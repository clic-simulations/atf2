# version 5.2 converted from MadX

# lattice options that can be set externally

if {! [info exist stripline]} {
    set stripline 3.0
}
if {! [info exist stripline2]} {
    set stripline2 3.0
}
if {! [info exist font_stripline_resolution]} {
    set font_stripline_resolution 0.5
}
if {! [info exist cband]} {
    set cband 0.2
}
if {! [info exist cband_noatt]} {
    set cband_noatt 0.2
}
if {! [info exist sband]} {
    set sband 1.0
}
if {! [info exist ipbpm]} {
    set ipbpm 0.1
}

# wakefields:
if {! [info exist cband_wake]} {
    set cband_wake ""
}
if {! [info exist sband_wake]} {
    set sband_wake ""
}

TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_0.dat };
Drift -name "ATF2MSTART" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1.dat };
Bpm -name "START" -length 0 -resolution $stripline
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_2.dat };
Quadrupole -name "KEX1MULT1" -synrad $quad_synrad -length 0 -strength [expr -0.01846929*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_3.dat };
Multipole -name "KEX1MULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -3.824591*$e0] -e0 $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_4.dat };
Sbend -name "KEX1A" -synrad $sbend_synrad -length 0.2500010417 -angle 0.0025 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.00635 -fint 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_5.dat };
#Bpm -name "RAUL" -length 0 -resolution $stripline 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_6.dat };
set e0 [expr $e0-14.1e-6*0.0025*0.0025/0.2500010417*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_7.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_8.dat };
Quadrupole -name "KEX1MULT1" -synrad $quad_synrad -length 0 -strength [expr -0.01846929*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_9.dat };
Multipole -name "KEX1MULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -3.824591*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_10.dat };
Drift -name "IP01" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_11.dat };
Quadrupole -name "KEX1MULT1" -synrad $quad_synrad -length 0 -strength [expr -0.01846929*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_12.dat };
Multipole -name "KEX1MULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -3.824591*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_13.dat };
Sbend -name "KEX1B" -synrad $sbend_synrad -length 0.2500010417 -angle 0.0025 -E1 0 -E2 0.005 -six_dim 1 -e0 $e0 -hgap 0.00635 -fintx 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_14.dat };
set e0 [expr $e0-14.1e-6*0.0025*0.0025/0.2500010417*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_15.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_16.dat };
Quadrupole -name "KEX1MULT1" -synrad $quad_synrad -length 0 -strength [expr -0.01846929*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_17.dat };
Multipole -name "KEX1MULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -3.824591*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_18.dat };
Drift -name "L001" -length 0.9231390392 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_19.dat };
Sbend -name "QM6RX" -synrad $sbend_synrad -length 0.09937458845 -angle 0.0023109657 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -K [expr -0.3644313055*$e0] 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_20.dat };
set e0 [expr $e0-14.1e-6*0.0023109657*0.0023109657/0.09937458845*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_21.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_22.dat };
Sbend -name "QM6RX" -synrad $sbend_synrad -length 0.09937458845 -angle 0.0023109657 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -K [expr -0.3644313055*$e0] 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_23.dat };
set e0 [expr $e0-14.1e-6*0.0023109657*0.0023109657/0.09937458845*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_24.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_25.dat };
Drift -name "L002A" -length 0.04685466892 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_26.dat };
HCORRECTOR -name "ZH100RX" -length 0.24955 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_27.dat };
Drift -name "L002B" -length 0.7028333967 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_28.dat };
HCORRECTOR -name "ZH101RX" -length 0.1679 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_29.dat };
Drift -name "L002C" -length 0.05068234611 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_30.dat };
VCORRECTOR -name "ZV100RX" -length 0.13874 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_31.dat };
Drift -name "L002D" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_32.dat };
#BPM MB1X not instrumented but present (button BPM)
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_33.dat };
Bpm -name "MB1X" -length 0 -resolution $stripline
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_34.dat };
Drift -name "L002E" -length 0.1894692706 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_35.dat };
Sbend -name "QM7RX" -synrad $sbend_synrad -length 0.0421716479 -angle -0.0045873988 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -K [expr 0.20411*$e0] 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_36.dat };
set e0 [expr $e0-14.1e-6*-0.0045873988*-0.0045873988/0.0421716479*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_37.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_38.dat };
Sbend -name "QM7RX" -synrad $sbend_synrad -length 0.0421716479 -angle -0.0045873988 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -K [expr 0.20411*$e0] 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_39.dat };
set e0 [expr $e0-14.1e-6*-0.0045873988*-0.0045873988/0.0421716479*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_40.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_41.dat };
Drift -name "L003" -length 0.1478305058 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_42.dat };
Sbend -name "BS1XA" -synrad $sbend_synrad -length 0.3 -angle 0.01401783271 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_43.dat };
set e0 [expr $e0-14.1e-6*0.01401783271*0.01401783271/0.3*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_44.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_45.dat };
Sbend -name "BS1XB" -synrad $sbend_synrad -length 0.3 -angle 0.01401783271 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_46.dat };
set e0 [expr $e0-14.1e-6*0.01401783271*0.01401783271/0.3*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_47.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_48.dat };
Drift -name "L004" -length 0.2 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_49.dat };
Sbend -name "BS2XA" -synrad $sbend_synrad -length 0.4 -angle 0.03717168307 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_50.dat };
set e0 [expr $e0-14.1e-6*0.03717168307*0.03717168307/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_51.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_52.dat };
Sbend -name "BS2XB" -synrad $sbend_synrad -length 0.4 -angle 0.03717168307 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_53.dat };
set e0 [expr $e0-14.1e-6*0.03717168307*0.03717168307/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_54.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_55.dat };
Drift -name "L005A" -length 0.1 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_56.dat };
#BPM MB2X present (button BPM) but callibration very dificult in Y
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_57.dat };
Bpm -name "MB2X" -length 0 -resolution $stripline
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_58.dat };
Drift -name "L005B" -length 0.1 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_59.dat };
Sbend -name "BS3XA" -synrad $sbend_synrad -length 0.5 -angle 0.1175110129 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_60.dat };
set e0 [expr $e0-14.1e-6*0.1175110129*0.1175110129/0.5*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_61.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_62.dat };
Sbend -name "BS3XB" -synrad $sbend_synrad -length 0.5 -angle 0.1175110129 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_63.dat };
set e0 [expr $e0-14.1e-6*0.1175110129*0.1175110129/0.5*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_64.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_65.dat };
Drift -name "L006A" -length 0.4782345 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_66.dat };
VCORRECTOR -name "ZV1X" -length 0.128141 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_67.dat };
Drift -name "L006B" -length 0.0335945 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_68.dat };
Quadrupole -name "QS1X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_69.dat };
Quadrupole -name "QS1X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_70.dat };
Drift -name "L006C" -length 0.08136 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_71.dat };
Quadrupole -name "QF1X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.5379118485*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_72.dat };
Quadrupole -name "QF1X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.5379118485*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_73.dat };
Drift -name "L007A" -length 0.014695 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_74.dat };
Bpm -name "MQF1X" -length 0 -resolution $stripline2
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_75.dat };
Drift -name "L007B" -length 0.214 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_76.dat };
Drift -name "MS1X" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_77.dat };
Drift -name "L007C" -length 0.271305 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_78.dat };
Multipole -name "BH1XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.1839837463*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_79.dat };
Sbend -name "BH1XA" -synrad $sbend_synrad -length 0.4 -angle 0.0941996781 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_80.dat };
set e0 [expr $e0-14.1e-6*0.0941996781*0.0941996781/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_81.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_82.dat };
Multipole -name "BH1XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.1839837463*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_83.dat };
HCORRECTOR -name "ZX1X" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_84.dat };
Multipole -name "BH1XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.1839837463*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_85.dat };
Sbend -name "BH1XB" -synrad $sbend_synrad -length 0.4 -angle 0.0941996781 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_86.dat };
set e0 [expr $e0-14.1e-6*0.0941996781*0.0941996781/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_87.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_88.dat };
Multipole -name "BH1XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.1839837463*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_89.dat };
Drift -name "L008A" -length 0.2132345 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_90.dat };
VCORRECTOR -name "ZV2X" -length 0.128141 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_91.dat };
Drift -name "L008B" -length 0.1586245 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_92.dat };
Quadrupole -name "QD2X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.4708418816*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_93.dat };
Quadrupole -name "QD2X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.4708418816*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_94.dat };
Drift -name "L009A" -length 0.014695 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_95.dat };
Bpm -name "MQD2X" -length 0 -resolution $stripline2
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_96.dat };
Drift -name "L009B" -length 0.7578050808 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_97.dat };
Quadrupole -name "QF3X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3353340117*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_98.dat };
Quadrupole -name "QF3X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3353340117*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_99.dat };
Drift -name "L010A" -length 0.026695 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_100.dat };
Bpm -name "MQF3X" -length 0 -resolution $stripline2
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_101.dat };
Drift -name "L010B" -length 0.138725 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_102.dat };
HCORRECTOR -name "ZH1X" -length 0.11455 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_103.dat };
Drift -name "L010C" -length 0.1396545 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_104.dat };
VCORRECTOR -name "ZV3X" -length 0.128141 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_105.dat };
Drift -name "L010D" -length 1.505663876 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_106.dat };
Drift -name "MID" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_107.dat };
Drift -name "L010E" -length 2.053429376 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_108.dat };
Quadrupole -name "QF4X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3393545966*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_109.dat };
Quadrupole -name "QF4X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3393545966*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_110.dat };
Drift -name "L011A" -length 0.026695 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_111.dat };
Bpm -name "MQF4X" -length 0 -resolution $stripline2
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_112.dat };
Drift -name "L011B" -length 0.134725 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_113.dat };
HCORRECTOR -name "ZH2X" -length 0.11455 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_114.dat };
Drift -name "L011C" -length 0.4698350808 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_115.dat };
Bpm -name "MQD5X" -length 0 -resolution $stripline2
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_116.dat };
Drift -name "L011D" -length 0.026695 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_117.dat };
Quadrupole -name "QD5X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.4607728075*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_118.dat };
Quadrupole -name "QD5X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.4607728075*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_119.dat };
Drift -name "L012A" -length 0.1576245 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_120.dat };
VCORRECTOR -name "ZV4X" -length 0.128141 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_121.dat };
Drift -name "L012B" -length 0.2142345 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_122.dat };
Multipole -name "BH2XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.1839837463*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_123.dat };
Sbend -name "BH2XA" -synrad $sbend_synrad -length 0.4 -angle -0.0941996781 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_124.dat };
set e0 [expr $e0-14.1e-6*-0.0941996781*-0.0941996781/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_125.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_126.dat };
Multipole -name "BH2XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.1839837463*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_127.dat };
HCORRECTOR -name "ZX2X" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_128.dat };
Multipole -name "BH2XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.1839837463*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_129.dat };
Sbend -name "BH2XB" -synrad $sbend_synrad -length 0.4 -angle -0.0941996781 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_130.dat };
set e0 [expr $e0-14.1e-6*-0.0941996781*-0.0941996781/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_131.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_132.dat };
Multipole -name "BH2XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.1839837463*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_133.dat };
Drift -name "L013A" -length 0.2142345 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_134.dat };
VCORRECTOR -name "ZV5X" -length 0.128141 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_135.dat };
Drift -name "L013B" -length 0.1429295 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_136.dat };
Bpm -name "MQF6X" -length 0 -resolution $stripline
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_137.dat };
Drift -name "L013C" -length 0.014695 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_138.dat };
Quadrupole -name "QF6X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.5650485124*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_139.dat };
Quadrupole -name "QF6X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.5650485124*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_140.dat };
Drift -name "L014A" -length 0.00536 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_141.dat };
Quadrupole -name "QS2X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_142.dat };
Quadrupole -name "QS2X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_143.dat };
Drift -name "L014B" -length 0.334665 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_144.dat };
Drift -name "UDDX1" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_145.dat };
Drift -name "L014C" -length 0.5875079196 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_146.dat };
Sbend -name "BH3XA" -synrad $sbend_synrad -length 0.675 -angle -0.1714240956 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_147.dat };
set e0 [expr $e0-14.1e-6*-0.1714240956*-0.1714240956/0.675*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_148.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_149.dat };
Sbend -name "BH3XB" -synrad $sbend_synrad -length 0.675 -angle -0.1714240956 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_150.dat };
set e0 [expr $e0-14.1e-6*-0.1714240956*-0.1714240956/0.675*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_151.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_152.dat };
Drift -name "L015" -length 0.7650382419 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_153.dat };
Quadrupole -name "QF7X" -synrad $quad_synrad -length 0.039335 -strength [expr 0.1915156418*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_154.dat };
Quadrupole -name "QF7X" -synrad $quad_synrad -length 0.039335 -strength [expr 0.1915156418*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_155.dat };
Drift -name "L016A" -length 0.026665 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_156.dat };
Bpm -name "MQF7X" -length 0 -resolution $stripline
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_157.dat };
Drift -name "L016B" -length 1.767246207 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_158.dat };
HCORRECTOR -name "ZH3X" -length 0.11455 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_159.dat };
Drift -name "L016C" -length 0.16342 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_160.dat };
Quadrupole -name "QD8X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.2950188224*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_161.dat };
Quadrupole -name "QD8X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.2950188224*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_162.dat };
Drift -name "L017A" -length 0.014695 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_163.dat };
Bpm -name "MQD8X" -length 0 -resolution $stripline
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_164.dat };
Drift -name "L017B" -length 0.1389295 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_165.dat };
VCORRECTOR -name "ZV6X" -length 0.128141 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_166.dat };
Drift -name "L017C" -length 0.6855607408 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_167.dat };
Drift -name "KEX2A" -length 0.2500010417 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_168.dat };
Drift -name "IP02" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_169.dat };
Drift -name "KEX2B" -length 0.2500010417 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_170.dat };
Drift -name "MDISP" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_171.dat };
Drift -name "L101A" -length 0.06027549974 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_172.dat };
Sbend -name "BKXA" -synrad $sbend_synrad -length 0.1 -angle 0.0025 -E1 0.005 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.05 -fint 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_173.dat };
set e0 [expr $e0-14.1e-6*0.0025*0.0025/0.1*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_174.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_175.dat };
Sbend -name "BKXB" -synrad $sbend_synrad -length 0.1 -angle 0.0025 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.05 -fintx 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_176.dat };
set e0 [expr $e0-14.1e-6*0.0025*0.0025/0.1*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_177.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_178.dat };
Drift -name "L101B" -length 0.166 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_179.dat };
Bpm -name "MQF9X" -length 0 -resolution $stripline
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_180.dat };
Drift -name "L101C" -length 0.054695 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_181.dat };
Quadrupole -name "QF9X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3814832824*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_182.dat };
Quadrupole -name "QF9X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3814832824*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_183.dat };
Drift -name "L102A" -length 0.15742 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_184.dat };
HCORRECTOR -name "ZH4X" -length 0.11455 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_185.dat };
Drift -name "L102B" -length 1.339476477 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_186.dat };
Drift -name "FONTK1" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_187.dat };
Drift -name "L102C" -length 0.3339295 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_188.dat };
VCORRECTOR -name "ZV7X" -length 0.128141 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_189.dat };
Drift -name "L102D" -length 0.0859295 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_190.dat };
Bpm -name "FONTP1" -length 0 -resolution $font_stripline_resolution
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_191.dat };
Drift -name "L102E" -length 0.125665 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_192.dat };
Quadrupole -name "QK1X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_193.dat };
Quadrupole -name "QK1X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_194.dat };
Drift -name "L102F" -length 0.10242 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_195.dat };
Multipole -name "QD10XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.061259524*$e0] -e0 $e0 -tilt 0.2728979485 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_196.dat };
Multipole -name "QD10XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-7.416610409*$e0] -e0 $e0 -tilt -0.4510025096 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_197.dat };
Quadrupole -name "QD10X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5833119514*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_198.dat };
Multipole -name "QD10XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.061259524*$e0] -e0 $e0 -tilt 0.2728979485 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_199.dat };
Multipole -name "QD10XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-7.416610409*$e0] -e0 $e0 -tilt -0.4510025096 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_200.dat };
Multipole -name "QD10XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.061259524*$e0] -e0 $e0 -tilt 0.2728979485 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_201.dat };
Multipole -name "QD10XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-7.416610409*$e0] -e0 $e0 -tilt -0.4510025096 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_202.dat };
Quadrupole -name "QD10X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5833119514*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_203.dat };
Multipole -name "QD10XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.061259524*$e0] -e0 $e0 -tilt 0.2728979485 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_204.dat };
Multipole -name "QD10XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-7.416610409*$e0] -e0 $e0 -tilt -0.4510025096 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_205.dat };
Drift -name "L103A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_206.dat };
Bpm -name "MQD10X" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_207.dat };
Drift -name "L103B" -length 0.9566 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_208.dat };
HCORRECTOR -name "ZH5X" -length 0.1248 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_209.dat };
Drift -name "L103C" -length 0.150355 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_210.dat };
Multipole -name "QF11XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02909535705*$e0] -e0 $e0 -tilt -0.2256002597 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_211.dat };
Multipole -name "QF11XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*9.212898679*$e0] -e0 $e0 -tilt 0.1224996226 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_212.dat };
Quadrupole -name "QF11X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.5116656769*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_213.dat };
Multipole -name "QF11XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02909535705*$e0] -e0 $e0 -tilt -0.2256002597 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_214.dat };
Multipole -name "QF11XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*9.212898679*$e0] -e0 $e0 -tilt 0.1224996226 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_215.dat };
Drift -name "MSKEW" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_216.dat };
Multipole -name "QF11XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02909535705*$e0] -e0 $e0 -tilt -0.2256002597 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_217.dat };
Multipole -name "QF11XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*9.212898679*$e0] -e0 $e0 -tilt 0.1224996226 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_218.dat };
Quadrupole -name "QF11X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.5116656769*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_219.dat };
Multipole -name "QF11XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02909535705*$e0] -e0 $e0 -tilt -0.2256002597 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_220.dat };
Multipole -name "QF11XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*9.212898679*$e0] -e0 $e0 -tilt 0.1224996226 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_221.dat };
Drift -name "L104A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_222.dat };
Bpm -name "MQF11X" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_223.dat };
Drift -name "L104B" -length 0.403 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_224.dat };
Drift -name "FONTK2" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_225.dat };
Drift -name "L104C" -length 0.3069295 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_226.dat };
VCORRECTOR -name "ZV8X" -length 0.128141 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_227.dat };
Drift -name "L104D" -length 0.0899295 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_228.dat };
#Bpm -name "FONTP2" -length 0 -resolution $font_stripline_resolution
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_229.dat };
Drift -name "L104F" -length 0.12433 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_230.dat };
Quadrupole -name "QK2X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_231.dat };
Quadrupole -name "QK2X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_232.dat };
Drift -name "L104G" -length 0.100755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_233.dat };
Multipole -name "QD12XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.07128526211*$e0] -e0 $e0 -tilt 0.1714028079 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_234.dat };
Multipole -name "QD12XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-5.00930931*$e0] -e0 $e0 -tilt 0.643546056 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_235.dat };
Quadrupole -name "QD12X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5116656769*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_236.dat };
Multipole -name "QD12XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.07128526211*$e0] -e0 $e0 -tilt 0.1714028079 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_237.dat };
Multipole -name "QD12XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-5.00930931*$e0] -e0 $e0 -tilt 0.643546056 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_238.dat };
Multipole -name "QD12XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.07128526211*$e0] -e0 $e0 -tilt 0.1714028079 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_239.dat };
Multipole -name "QD12XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-5.00930931*$e0] -e0 $e0 -tilt 0.643546056 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_240.dat };
Quadrupole -name "QD12X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5116656769*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_241.dat };
Multipole -name "QD12XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.07128526211*$e0] -e0 $e0 -tilt 0.1714028079 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_242.dat };
Multipole -name "QD12XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-5.00930931*$e0] -e0 $e0 -tilt 0.643546056 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_243.dat };
Drift -name "L105A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_244.dat };
Bpm -name "MQD12X" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_245.dat };
Drift -name "L105B" -length 0.5351231519 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_246.dat };
HCORRECTOR -name "ZH6X" -length 0.11455 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_247.dat };
Drift -name "L105C" -length 0.1653525 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_248.dat };
Quadrupole -name "QF13X" -synrad $quad_synrad -length 0.0993725 -strength [expr 0.6841194487*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_249.dat };
Quadrupole -name "QF13X" -synrad $quad_synrad -length 0.0993725 -strength [expr 0.6841194487*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_250.dat };
Drift -name "L106A" -length 0.0546275 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_251.dat };
Bpm -name "MQF13X" -length 0 -resolution $stripline
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_252.dat };
Drift -name "L106B" -length 0.7821670781 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_253.dat };
Quadrupole -name "QD14X" -synrad $quad_synrad -length 0.0993725 -strength [expr -0.507718235*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_254.dat };
Drift -name "FIT180" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_255.dat };
Quadrupole -name "QD14X" -synrad $quad_synrad -length 0.0993725 -strength [expr -0.507718235*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_256.dat };
Drift -name "L107A" -length 0.0546275 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_257.dat };
Bpm -name "MQD14X" -length 0 -resolution $stripline
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_258.dat };
Drift -name "L107B" -length 0.276 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_259.dat };
#Bpm -name "FONTP3" -length 0 -resolution $font_stripline_resolution
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_260.dat };
Drift -name "L107C" -length 0.2282645781 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_261.dat };
HCORRECTOR -name "ZH7X" -length 0.11455 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_262.dat };
Drift -name "L107D" -length 0.1633525 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_263.dat };
Quadrupole -name "QF15X" -synrad $quad_synrad -length 0.0993725 -strength [expr 0.6841194487*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_264.dat };
Quadrupole -name "QF15X" -synrad $quad_synrad -length 0.0993725 -strength [expr 0.6841194487*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_265.dat };
Drift -name "L108A" -length 0.0546275 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_266.dat };
#Bpm -name "MQF15X" -length 0 -resolution $stripline
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_267.dat };
Drift -name "L108B" -length 0.3156 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_268.dat };
VCORRECTOR -name "ZV9X" -length 0.1248 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_269.dat };
Drift -name "L108D" -length 0.1603281519 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_270.dat };
Quadrupole -name "QK3X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_271.dat };
Quadrupole -name "QK3X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_272.dat };
Drift -name "L108E" -length 0.100755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_273.dat };
Multipole -name "QD16XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01998975467*$e0] -e0 $e0 -tilt -0.2319377413 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_274.dat };
Multipole -name "QD16XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-2.427444304*$e0] -e0 $e0 -tilt -0.6034986504 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_275.dat };
Quadrupole -name "QD16X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5116656769*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_276.dat };
Multipole -name "QD16XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01998975467*$e0] -e0 $e0 -tilt -0.2319377413 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_277.dat };
Multipole -name "QD16XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-2.427444304*$e0] -e0 $e0 -tilt -0.6034986504 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_278.dat };
Multipole -name "QD16XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01998975467*$e0] -e0 $e0 -tilt -0.2319377413 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_279.dat };
Multipole -name "QD16XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-2.427444304*$e0] -e0 $e0 -tilt -0.6034986504 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_280.dat };
Quadrupole -name "QD16X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5116656769*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_281.dat };
Multipole -name "QD16XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01998975467*$e0] -e0 $e0 -tilt -0.2319377413 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_282.dat };
Multipole -name "QD16XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-2.427444304*$e0] -e0 $e0 -tilt -0.6034986504 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_283.dat };
Drift -name "L109A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_284.dat };
Bpm -name "MQD16X" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_285.dat };
Drift -name "L109B" -length 0.9546 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_286.dat };
HCORRECTOR -name "ZH8X" -length 0.1248 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_287.dat };
Drift -name "L109C" -length 0.152355 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_288.dat };
Multipole -name "QF17XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.005940438509*$e0] -e0 $e0 -tilt 0.6748486461 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_289.dat };
Multipole -name "QF17XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.753578805*$e0] -e0 $e0 -tilt 0.09200095134 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_290.dat };
Quadrupole -name "QF17X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.5116656769*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_291.dat };
Multipole -name "QF17XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.005940438509*$e0] -e0 $e0 -tilt 0.6748486461 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_292.dat };
Multipole -name "QF17XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.753578805*$e0] -e0 $e0 -tilt 0.09200095134 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_293.dat };
Multipole -name "QF17XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.005940438509*$e0] -e0 $e0 -tilt 0.6748486461 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_294.dat };
Multipole -name "QF17XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.753578805*$e0] -e0 $e0 -tilt 0.09200095134 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_295.dat };
Quadrupole -name "QF17X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.5116656769*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_296.dat };
Multipole -name "QF17XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.005940438509*$e0] -e0 $e0 -tilt 0.6748486461 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_297.dat };
Multipole -name "QF17XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.753578805*$e0] -e0 $e0 -tilt 0.09200095134 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_298.dat };
Drift -name "L110A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_299.dat };
Bpm -name "MQF17X" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_300.dat };
Drift -name "L110B" -length 0.211 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_301.dat };
Drift -name "MREF1X" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_302.dat };
Drift -name "L110C" -length 0.8401649959 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_303.dat };
Quadrupole -name "QK4X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_304.dat };
Quadrupole -name "QK4X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_305.dat };
Drift -name "L110D" -length 0.6575945 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_306.dat };
VCORRECTOR -name "ZV10X" -length 0.128141 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_307.dat };
Drift -name "L110E" -length 0.2486845 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_308.dat };
Multipole -name "QD18XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.008575901695*$e0] -e0 $e0 -tilt 0.530866035 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_309.dat };
Multipole -name "QD18XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.348131746*$e0] -e0 $e0 -tilt 0.5677030286 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_310.dat };
Quadrupole -name "QD18X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.3430360678*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_311.dat };
Multipole -name "QD18XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.008575901695*$e0] -e0 $e0 -tilt 0.530866035 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_312.dat };
Multipole -name "QD18XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.348131746*$e0] -e0 $e0 -tilt 0.5677030286 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_313.dat };
Multipole -name "QD18XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.008575901695*$e0] -e0 $e0 -tilt 0.530866035 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_314.dat };
Multipole -name "QD18XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.348131746*$e0] -e0 $e0 -tilt 0.5677030286 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_315.dat };
Quadrupole -name "QD18X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.3430360678*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_316.dat };
Multipole -name "QD18XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.008575901695*$e0] -e0 $e0 -tilt 0.530866035 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_317.dat };
Multipole -name "QD18XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.348131746*$e0] -e0 $e0 -tilt 0.5677030286 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_318.dat };
Drift -name "L111A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_319.dat };
Bpm -name "MQD18X" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_320.dat };
Drift -name "L111B" -length 0.276 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_321.dat };
Drift -name "MW0X" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_322.dat };
Drift -name "L111C" -length 0.4046498952 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_323.dat };
Drift -name "OTR0X" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_324.dat };
Drift -name "L111D" -length 0.2716 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_325.dat };
HCORRECTOR -name "ZH9X" -length 0.1248 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_326.dat };
Drift -name "L111E" -length 0.149355 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_327.dat };
Multipole -name "QF19XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002165323765*$e0] -e0 $e0 -tilt 0.4851101624 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_328.dat };
Multipole -name "QF19XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.700254151*$e0] -e0 $e0 -tilt -0.3060050931 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_329.dat };
Quadrupole -name "QF19X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.3275830205*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_330.dat };
Multipole -name "QF19XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002165323765*$e0] -e0 $e0 -tilt 0.4851101624 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_331.dat };
Multipole -name "QF19XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.700254151*$e0] -e0 $e0 -tilt -0.3060050931 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_332.dat };
Multipole -name "QF19XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002165323765*$e0] -e0 $e0 -tilt 0.4851101624 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_333.dat };
Multipole -name "QF19XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.700254151*$e0] -e0 $e0 -tilt -0.3060050931 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_334.dat };
Quadrupole -name "QF19X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.3275830205*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_335.dat };
Multipole -name "QF19XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002165323765*$e0] -e0 $e0 -tilt 0.4851101624 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_336.dat };
Multipole -name "QF19XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.700254151*$e0] -e0 $e0 -tilt -0.3060050931 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_337.dat };
Drift -name "L112A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_338.dat };
Bpm -name "MQF19X" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_339.dat };
Drift -name "L112B" -length 0.265 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_340.dat };
Drift -name "MW1X" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_341.dat };
Drift -name "L112C" -length 0.386 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_342.dat };
Drift -name "OTR1X" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_343.dat };
Drift -name "L112D" -length 0.309 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_344.dat };
Drift -name "WCM" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_345.dat };
Drift -name "L112E" -length 0.4849295 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_346.dat };
VCORRECTOR -name "ZV11X" -length 0.128141 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_347.dat };
Drift -name "L112F" -length 1.4372645 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_348.dat };
Drift -name "PPMW" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_349.dat };
Drift -name "L112G" -length 0.469 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_350.dat };
Drift -name "MW2X" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_351.dat };
Drift -name "L112H" -length 0.282 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_352.dat };
Bpm -name "MQD20X" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_353.dat };
Drift -name "L112I" -length 0.019665 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_354.dat };
Quadrupole -name "QD20X" -synrad $quad_synrad -length 0.039335 -strength [expr -0.1511348642*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_355.dat };
Drift -name "MEMIT" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_356.dat };
Quadrupole -name "QD20X" -synrad $quad_synrad -length 0.039335 -strength [expr -0.1511348642*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_357.dat };
Drift -name "L113A" -length 0.466665 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_358.dat };
Drift -name "OTR2X" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_359.dat };
Drift -name "L113B" -length 1.861065 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_360.dat };
HCORRECTOR -name "ZH10X" -length 0.11921 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_361.dat };
Drift -name "L113C" -length 0.937395 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_362.dat };
Drift -name "ICT1X" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_363.dat };
Drift -name "L113D" -length 0.415665 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_364.dat };
Quadrupole -name "QF21X" -synrad $quad_synrad -length 0.039335 -strength [expr 0.2223564783*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_365.dat };
Quadrupole -name "QF21X" -synrad $quad_synrad -length 0.039335 -strength [expr 0.2223564783*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_366.dat };
Drift -name "L114A" -length 0.019665 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_367.dat };
Bpm -name "MQF21X" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_368.dat };
Drift -name "L114B" -length 0.274 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_369.dat };
Drift -name "MW3X" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_370.dat };
Drift -name "L114C" -length 0.502 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_371.dat };
Drift -name "OTR3X" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_372.dat };
Drift -name "L114D" -length 0.35 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_373.dat };
Drift -name "TILTM" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_374.dat };
Drift -name "L114E" -length 0.54 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_375.dat };
#not present anymore
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_376.dat };
#Bpm -name "IPBPM" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_377.dat };
Drift -name "ATIPBPM" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_378.dat };
Drift -name "L114F" -length 1.813335 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_379.dat };
Drift -name "MW4X" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_380.dat };
Drift -name "L114G" -length 0.282 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_381.dat };
Drift -name "BEGFF" -length 0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_382.dat };
if {[info exists use_dispersion_ff] && $use_dispersion_ff} {
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_383.dat };
    TclCall -name "ADDDISPERSIONFFSTART" -script "add_dispersion_ff_start"
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_384.dat };
}
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_385.dat };
if {[info exists save_beam_ff_start] && $save_beam_ff_start} {
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_386.dat };
    TclCall -name "SAVEBEAMFFSTART" -script "save_beam_ff_start"
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_387.dat };
}
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_388.dat };
Bpm -name "MQM16FF" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_389.dat };
Drift -name "L200" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_390.dat };
Multipole -name "QM16FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003590382417*$e0] -e0 $e0 -tilt -0.3676020139 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_391.dat };
Multipole -name "QM16FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.145971342*$e0] -e0 $e0 -tilt -0.6634969671 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_392.dat };
Quadrupole -name "QM16FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_393.dat };
Multipole -name "QM16FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003590382417*$e0] -e0 $e0 -tilt -0.3676020139 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_394.dat };
Multipole -name "QM16FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.145971342*$e0] -e0 $e0 -tilt -0.6634969671 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_395.dat };
Drift -name "AQM16FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_396.dat };
Multipole -name "QM16FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003590382417*$e0] -e0 $e0 -tilt -0.3676020139 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_397.dat };
Multipole -name "QM16FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.145971342*$e0] -e0 $e0 -tilt -0.6634969671 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_398.dat };
Quadrupole -name "QM16FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_399.dat };
Multipole -name "QM16FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003590382417*$e0] -e0 $e0 -tilt -0.3676020139 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_400.dat };
Multipole -name "QM16FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.145971342*$e0] -e0 $e0 -tilt -0.6634969671 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_401.dat };
Drift -name "L201A" -length 0.76315 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_402.dat };
HCORRECTOR -name "ZH1FF" -length 0.11921 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_403.dat };
Drift -name "L201B" -length 0.1353245 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_404.dat };
VCORRECTOR -name "ZV1FF" -length 0.128141 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_405.dat };
Drift -name "L201C" -length 0.1556845 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_406.dat };
Multipole -name "QM15FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0009094236936*$e0] -e0 $e0 -tilt 0.507224233 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_407.dat };
Multipole -name "QM15FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.4632096203*$e0] -e0 $e0 -tilt 0.1838965077 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_408.dat };
Quadrupole -name "QM15FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.3199356679*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_409.dat };
Multipole -name "QM15FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0009094236936*$e0] -e0 $e0 -tilt 0.507224233 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_410.dat };
Multipole -name "QM15FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.4632096203*$e0] -e0 $e0 -tilt 0.1838965077 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_411.dat };
Drift -name "AQM15FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_412.dat };
Multipole -name "QM15FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0009094236936*$e0] -e0 $e0 -tilt 0.507224233 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_413.dat };
Multipole -name "QM15FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.4632096203*$e0] -e0 $e0 -tilt 0.1838965077 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_414.dat };
Quadrupole -name "QM15FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.3199356679*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_415.dat };
Multipole -name "QM15FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0009094236936*$e0] -e0 $e0 -tilt 0.507224233 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_416.dat };
Multipole -name "QM15FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.4632096203*$e0] -e0 $e0 -tilt 0.1838965077 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_417.dat };
Drift -name "L202A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_418.dat };
Bpm -name "MQM15FF" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_419.dat };
Drift -name "L202B" -length 1.281755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_420.dat };
Multipole -name "QM14FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03177746312*$e0] -e0 $e0 -tilt -0.8477670492 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_421.dat };
Multipole -name "QM14FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-4.818911977*$e0] -e0 $e0 -tilt 0.1220007465 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_422.dat };
Quadrupole -name "QM14FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.758062188*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_423.dat };
Multipole -name "QM14FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03177746312*$e0] -e0 $e0 -tilt -0.8477670492 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_424.dat };
Multipole -name "QM14FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-4.818911977*$e0] -e0 $e0 -tilt 0.1220007465 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_425.dat };
Drift -name "AQM14FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_426.dat };
Multipole -name "QM14FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03177746312*$e0] -e0 $e0 -tilt -0.8477670492 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_427.dat };
Multipole -name "QM14FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-4.818911977*$e0] -e0 $e0 -tilt 0.1220007465 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_428.dat };
Quadrupole -name "QM14FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.758062188*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_429.dat };
Multipole -name "QM14FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03177746312*$e0] -e0 $e0 -tilt -0.8477670492 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_430.dat };
Multipole -name "QM14FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-4.818911977*$e0] -e0 $e0 -tilt 0.1220007465 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_431.dat };
Drift -name "L203A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_432.dat };
Bpm -name "MQM14FF" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_433.dat };
if {[info exist use_mfb2ffkicker] } {
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_434.dat };
    Dipole -name "MFB2FFKICKER" -length 0.0 -strength_x 0.0 -strength_y 0.0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_435.dat };
}
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_436.dat };
Drift -name "L203B" -length 0.557455 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_437.dat };
Drift -name "LWML1" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_438.dat };
Drift -name "L203C" -length 0.0215 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_439.dat };
Drift -name "LW1FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_440.dat };
Drift -name "L203D" -length 0.0215 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_441.dat };
Drift -name "LWML2" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_442.dat };
Drift -name "L203E" -length 0.2035 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_443.dat };
if {[info exists save_beam_mfb2ff] && $save_beam_mfb2ff} {
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_444.dat };
    TclCall -name "SAVEBEAMMFB2FF" -script "save_beam_mfb2ff"
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_445.dat };
}
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_446.dat };
Bpm -name "MFB2FF" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_447.dat };
Drift -name "AFB2FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_448.dat };
Drift -name "L203F" -length 0.4778 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_449.dat };
Multipole -name "QM13FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002564596147*$e0] -e0 $e0 -tilt 0.4942315471 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_450.dat };
Multipole -name "QM13FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*6.191676439*$e0] -e0 $e0 -tilt 0.040266308 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_451.dat };
Quadrupole -name "QM13FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.4629159803*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_452.dat };
Multipole -name "QM13FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002564596147*$e0] -e0 $e0 -tilt 0.4942315471 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_453.dat };
Multipole -name "QM13FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*6.191676439*$e0] -e0 $e0 -tilt 0.040266308 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_454.dat };
Drift -name "AQM13FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_455.dat };
Multipole -name "QM13FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002564596147*$e0] -e0 $e0 -tilt 0.4942315471 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_456.dat };
Multipole -name "QM13FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*6.191676439*$e0] -e0 $e0 -tilt 0.040266308 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_457.dat };
Quadrupole -name "QM13FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.4629159803*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_458.dat };
Multipole -name "QM13FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002564596147*$e0] -e0 $e0 -tilt 0.4942315471 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_459.dat };
Multipole -name "QM13FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*6.191676439*$e0] -e0 $e0 -tilt 0.040266308 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_460.dat };
Drift -name "L204A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_461.dat };
Bpm -name "MQM13FF" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_462.dat };
Drift -name "L204B" -length 1.281755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_463.dat };
Multipole -name "QM12FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.009392199386*$e0] -e0 $e0 -tilt 1.041419048 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_464.dat };
Multipole -name "QM12FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2866941739*$e0] -e0 $e0 -tilt -0.6337616359 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_465.dat };
Quadrupole -name "QM12FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1164254228*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_466.dat };
Multipole -name "QM12FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.009392199386*$e0] -e0 $e0 -tilt 1.041419048 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_467.dat };
Multipole -name "QM12FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2866941739*$e0] -e0 $e0 -tilt -0.6337616359 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_468.dat };
Drift -name "AQM12FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_469.dat };
Multipole -name "QM12FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.009392199386*$e0] -e0 $e0 -tilt 1.041419048 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_470.dat };
Multipole -name "QM12FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2866941739*$e0] -e0 $e0 -tilt -0.6337616359 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_471.dat };
Quadrupole -name "QM12FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1164254228*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_472.dat };
Multipole -name "QM12FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.009392199386*$e0] -e0 $e0 -tilt 1.041419048 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_473.dat };
Multipole -name "QM12FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2866941739*$e0] -e0 $e0 -tilt -0.6337616359 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_474.dat };
Drift -name "L205A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_475.dat };
Bpm -name "MQM12FF" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_476.dat };
Drift -name "L205B" -length 0.715755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_477.dat };
# not present
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_478.dat };
#Bpm -name "MFB1FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_479.dat };
Drift -name "L205C" -length 0.666 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_480.dat };
Drift -name "QM11FFMULT2" -length 0 -tilt 0.4292947267 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_481.dat };
Drift -name "QM11FFMULT3" -length 0 -tilt 0.7639750205 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_482.dat };
Quadrupole -name "QM11FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.08000716598*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_483.dat };
Drift -name "QM11FFMULT2" -length 0 -tilt 0.4292947267 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_484.dat };
Drift -name "QM11FFMULT3" -length 0 -tilt 0.7639750205 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_485.dat };
Drift -name "AQM11FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_486.dat };
Drift -name "QM11FFMULT2" -length 0 -tilt 0.4292947267 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_487.dat };
Drift -name "QM11FFMULT3" -length 0 -tilt 0.7639750205 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_488.dat };
Quadrupole -name "QM11FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.08000716598*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_489.dat };
Drift -name "QM11FFMULT2" -length 0 -tilt 0.4292947267 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_490.dat };
Drift -name "QM11FFMULT3" -length 0 -tilt 0.7639750205 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_491.dat };
Drift -name "L206A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_492.dat };
Bpm -name "MQM11FF" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_493.dat };
Drift -name "L206BA" -length 0.8408775 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_494.dat };
# collimator cavity not connected
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_495.dat };
#Bpm -name "COLLBY" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_496.dat };
Drift -name "L206BB" -length 0.4408775 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_497.dat };
Multipole -name "QD10BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003197634999*$e0] -e0 $e0 -tilt -0.882813608 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_498.dat };
Multipole -name "QD10BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.407797338*$e0] -e0 $e0 -tilt -0.7077528722 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_499.dat };
Quadrupole -name "QD10BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1450096657*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_500.dat };
Multipole -name "QD10BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003197634999*$e0] -e0 $e0 -tilt -0.882813608 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_501.dat };
Multipole -name "QD10BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.407797338*$e0] -e0 $e0 -tilt -0.7077528722 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_502.dat };
Drift -name "AQD10BFF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_503.dat };
Multipole -name "QD10BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003197634999*$e0] -e0 $e0 -tilt -0.882813608 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_504.dat };
Multipole -name "QD10BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.407797338*$e0] -e0 $e0 -tilt -0.7077528722 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_505.dat };
Quadrupole -name "QD10BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1450096657*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_506.dat };
Multipole -name "QD10BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003197634999*$e0] -e0 $e0 -tilt -0.882813608 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_507.dat };
Multipole -name "QD10BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.407797338*$e0] -e0 $e0 -tilt -0.7077528722 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_508.dat };
Drift -name "L207A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_509.dat };
#Bpm -name "MQD10BFF" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake #REMOVED
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_510.dat };
Drift -name "L207B" -length 0.477 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_511.dat };
# collimator cavity not connected
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_512.dat };
#Bpm -name "COLLB" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_513.dat };
Drift -name "MREF3FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_514.dat };
# wakeFieldSetup on mover
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_515.dat };
if {[info exist wakeFieldSetup] && $wakeFieldSetup} {
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_516.dat };
    source ${lattice_dir}/ATF2_wakeFieldSetup.tcl
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_517.dat };
}
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_518.dat };
Drift -name "L207C" -length 0.204755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_519.dat };
Multipole -name "QD10AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.005931299368*$e0] -e0 $e0 -tilt -0.8561060094 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_520.dat };
Multipole -name "QD10AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6223524834*$e0] -e0 $e0 -tilt -0.0202933426 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_521.dat };
Quadrupole -name "QD10AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1450096657*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_522.dat };
Multipole -name "QD10AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.005931299368*$e0] -e0 $e0 -tilt -0.8561060094 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_523.dat };
Multipole -name "QD10AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6223524834*$e0] -e0 $e0 -tilt -0.0202933426 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_524.dat };
Drift -name "AQD10AFF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_525.dat };
Multipole -name "QD10AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.005931299368*$e0] -e0 $e0 -tilt -0.8561060094 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_526.dat };
Multipole -name "QD10AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6223524834*$e0] -e0 $e0 -tilt -0.0202933426 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_527.dat };
Quadrupole -name "QD10AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1450096657*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_528.dat };
Multipole -name "QD10AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.005931299368*$e0] -e0 $e0 -tilt -0.8561060094 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_529.dat };
Multipole -name "QD10AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6223524834*$e0] -e0 $e0 -tilt -0.0202933426 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_530.dat };
Drift -name "L208A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_531.dat };
Bpm -name "MQD10AFF" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_532.dat };
Drift -name "L208B" -length 0.289 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_533.dat };
Drift -name "MS1FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_534.dat };
Drift -name "L208C" -length 0.792755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_535.dat };
Multipole -name "QF9BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.03025071081*$e0] -e0 $e0 -tilt 0.9789002044 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_536.dat };
Multipole -name "QF9BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.4884584584*$e0] -e0 $e0 -tilt -0.5935711555 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_537.dat };
Quadrupole -name "QF9BFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1893249839*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_538.dat };
Multipole -name "QF9BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.03025071081*$e0] -e0 $e0 -tilt 0.9789002044 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_539.dat };
Multipole -name "QF9BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.4884584584*$e0] -e0 $e0 -tilt -0.5935711555 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_540.dat };
Drift -name "AQF9BFF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_541.dat };
Multipole -name "QF9BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.03025071081*$e0] -e0 $e0 -tilt 0.9789002044 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_542.dat };
Multipole -name "QF9BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.4884584584*$e0] -e0 $e0 -tilt -0.5935711555 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_543.dat };
Quadrupole -name "QF9BFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1893249839*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_544.dat };
Multipole -name "QF9BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.03025071081*$e0] -e0 $e0 -tilt 0.9789002044 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_545.dat };
Multipole -name "QF9BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.4884584584*$e0] -e0 $e0 -tilt -0.5935711555 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_546.dat };
Drift -name "L209A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_547.dat };
#Bpm -name "MQF9BFF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_548.dat };
Drift -name "L209B" -length 0.390960984 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_549.dat };
Multipole -name "SF6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.9710366071*$e0] -e0 $e0 -tilt -0.5520476424 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_550.dat };
Multipole -name "SF6FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 80.25517652*$e0] -e0 $e0 -tilt -0.2727949621 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_551.dat };
Drift -name "SF6FFMULT5" -length 0 -tilt -0.1902408885 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_552.dat };
Drift -name "SF6FFMULT6" -length 0 -tilt 0.207694181 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_553.dat };
Drift -name "SF6FFMULT7" -length 0 -tilt 0.01361356817 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_554.dat };
Drift -name "SF6FFMULT8" -length 0 -tilt -0.0005235987756 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_555.dat };
Drift -name "SF6FFMULT9" -length 0 -tilt 0.2935643802 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_556.dat };
Multipole -name "SF6FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr 4.8828277*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_557.dat };
Multipole -name "SF6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.9710366071*$e0] -e0 $e0 -tilt -0.5520476424 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_558.dat };
Multipole -name "SF6FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 80.25517652*$e0] -e0 $e0 -tilt -0.2727949621 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_559.dat };
Drift -name "SF6FFMULT5" -length 0 -tilt -0.1902408885 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_560.dat };
Drift -name "SF6FFMULT6" -length 0 -tilt 0.207694181 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_561.dat };
Drift -name "SF6FFMULT7" -length 0 -tilt 0.01361356817 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_562.dat };
Drift -name "SF6FFMULT8" -length 0 -tilt -0.0005235987756 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_563.dat };
Drift -name "SF6FFMULT9" -length 0 -tilt 0.2935643802 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_564.dat };
Drift -name "ASF6FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_565.dat };
Multipole -name "SF6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.9710366071*$e0] -e0 $e0 -tilt -0.5520476424 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_566.dat };
Multipole -name "SF6FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 80.25517652*$e0] -e0 $e0 -tilt -0.2727949621 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_567.dat };
Drift -name "SF6FFMULT5" -length 0 -tilt -0.1902408885 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_568.dat };
Drift -name "SF6FFMULT6" -length 0 -tilt 0.207694181 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_569.dat };
Drift -name "SF6FFMULT7" -length 0 -tilt 0.01361356817 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_570.dat };
Drift -name "SF6FFMULT8" -length 0 -tilt -0.0005235987756 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_571.dat };
Drift -name "SF6FFMULT9" -length 0 -tilt 0.2935643802 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_572.dat };
Multipole -name "SF6FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr 4.8828277*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_573.dat };
Multipole -name "SF6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.9710366071*$e0] -e0 $e0 -tilt -0.5520476424 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_574.dat };
Multipole -name "SF6FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 80.25517652*$e0] -e0 $e0 -tilt -0.2727949621 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_575.dat };
Drift -name "SF6FFMULT5" -length 0 -tilt -0.1902408885 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_576.dat };
Drift -name "SF6FFMULT6" -length 0 -tilt 0.207694181 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_577.dat };
Drift -name "SF6FFMULT7" -length 0 -tilt 0.01361356817 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_578.dat };
Drift -name "SF6FFMULT8" -length 0 -tilt -0.0005235987756 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_579.dat };
Drift -name "SF6FFMULT9" -length 0 -tilt 0.2935643802 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_580.dat };
Drift -name "L210A" -length 0.099791984 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_581.dat };
#Bpm -name "MSF6FF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_582.dat };
Drift -name "L210B" -length 0.310924 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_583.dat };
Multipole -name "QF9AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02552335583*$e0] -e0 $e0 -tilt 0.297225583 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_584.dat };
Multipole -name "QF9AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*2.57883347*$e0] -e0 $e0 -tilt 0.7565372179 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_585.dat };
Quadrupole -name "QF9AFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1893249839*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_586.dat };
Multipole -name "QF9AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02552335583*$e0] -e0 $e0 -tilt 0.297225583 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_587.dat };
Multipole -name "QF9AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*2.57883347*$e0] -e0 $e0 -tilt 0.7565372179 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_588.dat };
Drift -name "AQF9AFF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_589.dat };
Multipole -name "QF9AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02552335583*$e0] -e0 $e0 -tilt 0.297225583 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_590.dat };
Multipole -name "QF9AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*2.57883347*$e0] -e0 $e0 -tilt 0.7565372179 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_591.dat };
Quadrupole -name "QF9AFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1893249839*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_592.dat };
Multipole -name "QF9AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02552335583*$e0] -e0 $e0 -tilt 0.297225583 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_593.dat };
Multipole -name "QF9AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*2.57883347*$e0] -e0 $e0 -tilt 0.7565372179 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_594.dat };
Drift -name "L211A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_595.dat };
Bpm -name "MQF9AFF" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_596.dat };
Drift -name "L211B" -length 0.266 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_597.dat };
Multipole -name "SK4FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_598.dat };
Multipole -name "SK4FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_599.dat };
Drift -name "L211C" -length 1.085755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_600.dat };
Multipole -name "QD8FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02084036463*$e0] -e0 $e0 -tilt 0.636230717 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_601.dat };
Multipole -name "QD8FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-6.229332836*$e0] -e0 $e0 -tilt 0.0402518337 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_602.dat };
Quadrupole -name "QD8FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.3021776993*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_603.dat };
Multipole -name "QD8FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02084036463*$e0] -e0 $e0 -tilt 0.636230717 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_604.dat };
Multipole -name "QD8FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-6.229332836*$e0] -e0 $e0 -tilt 0.0402518337 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_605.dat };
Drift -name "AQD8FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_606.dat };
Multipole -name "QD8FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02084036463*$e0] -e0 $e0 -tilt 0.636230717 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_607.dat };
Multipole -name "QD8FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-6.229332836*$e0] -e0 $e0 -tilt 0.0402518337 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_608.dat };
Quadrupole -name "QD8FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.3021776993*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_609.dat };
Multipole -name "QD8FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02084036463*$e0] -e0 $e0 -tilt 0.636230717 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_610.dat };
Multipole -name "QD8FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-6.229332836*$e0] -e0 $e0 -tilt 0.0402518337 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_611.dat };
Drift -name "L212A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_612.dat };
Bpm -name "MQD8FF" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_613.dat };
Drift -name "L212B" -length 1.681755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_614.dat };
Multipole -name "QF7FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02376281692*$e0] -e0 $e0 -tilt 0.03989982269 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_615.dat };
Multipole -name "QF7FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.720636134*$e0] -e0 $e0 -tilt -0.7129998768 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_616.dat };
Quadrupole -name "QF7FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.2750809185*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_617.dat };
Multipole -name "QF7FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02376281692*$e0] -e0 $e0 -tilt 0.03989982269 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_618.dat };
Multipole -name "QF7FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.720636134*$e0] -e0 $e0 -tilt -0.7129998768 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_619.dat };
Drift -name "AQF7FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_620.dat };
Multipole -name "QF7FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02376281692*$e0] -e0 $e0 -tilt 0.03989982269 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_621.dat };
Multipole -name "QF7FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.720636134*$e0] -e0 $e0 -tilt -0.7129998768 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_622.dat };
Quadrupole -name "QF7FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.2750809185*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_623.dat };
Multipole -name "QF7FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02376281692*$e0] -e0 $e0 -tilt 0.03989982269 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_624.dat };
Multipole -name "QF7FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.720636134*$e0] -e0 $e0 -tilt -0.7129998768 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_625.dat };
Drift -name "L213A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_626.dat };
Bpm -name "MQF7FF" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_627.dat };
Drift -name "L213B" -length 0.4246097102 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_628.dat };
Quadrupole -name "B5FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.995837329e-05*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_629.dat };
Multipole -name "B5FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.05117313872*$e0] -e0 $e0 -tilt 0.01137769317 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_630.dat };
Multipole -name "B5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6735950984*$e0] -e0 $e0 -tilt 0.7853981634 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_631.dat };
Multipole -name "B5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -868.1892379*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_632.dat };
Sbend -name "B5FFA" -synrad $sbend_synrad -length 0.3064317863 -angle -0.02494796661 -E1 -0.02494796661 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.01905 -fint 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_633.dat };
set e0 [expr $e0-14.1e-6*-0.02494796661*-0.02494796661/0.3064317863*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_634.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_635.dat };
Quadrupole -name "B5FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.995837329e-05*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_636.dat };
Multipole -name "B5FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.05117313872*$e0] -e0 $e0 -tilt 0.01137769317 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_637.dat };
Multipole -name "B5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6735950984*$e0] -e0 $e0 -tilt 0.7853981634 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_638.dat };
Multipole -name "B5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -868.1892379*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_639.dat };
Quadrupole -name "B5FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.995837329e-05*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_640.dat };
Multipole -name "B5FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.05117313872*$e0] -e0 $e0 -tilt 0.01137769317 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_641.dat };
Multipole -name "B5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6735950984*$e0] -e0 $e0 -tilt 0.7853981634 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_642.dat };
Multipole -name "B5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -868.1892379*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_643.dat };
Sbend -name "B5FFB" -synrad $sbend_synrad -length 0.3064317863 -angle -0.02494796661 -E1 0 -E2 -0.02494796661 -six_dim 1 -e0 $e0 -hgap 0.01905 -fintx 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_644.dat };
set e0 [expr $e0-14.1e-6*-0.02494796661*-0.02494796661/0.3064317863*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_645.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_646.dat };
Quadrupole -name "B5FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.995837329e-05*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_647.dat };
Multipole -name "B5FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.05117313872*$e0] -e0 $e0 -tilt 0.01137769317 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_648.dat };
Multipole -name "B5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6735950984*$e0] -e0 $e0 -tilt 0.7853981634 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_649.dat };
Multipole -name "B5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -868.1892379*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_650.dat };
Drift -name "L214" -length 0.6443647102 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_651.dat };
Multipole -name "QD6FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04580065396*$e0] -e0 $e0 -tilt 0.862729249 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_652.dat };
Multipole -name "QD6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-9.14694104*$e0] -e0 $e0 -tilt -0.008496430778 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_653.dat };
Quadrupole -name "QD6FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.301163606*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_654.dat };
Multipole -name "QD6FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04580065396*$e0] -e0 $e0 -tilt 0.862729249 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_655.dat };
Multipole -name "QD6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-9.14694104*$e0] -e0 $e0 -tilt -0.008496430778 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_656.dat };
Drift -name "AQD6FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_657.dat };
Multipole -name "QD6FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04580065396*$e0] -e0 $e0 -tilt 0.862729249 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_658.dat };
Multipole -name "QD6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-9.14694104*$e0] -e0 $e0 -tilt -0.008496430778 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_659.dat };
Quadrupole -name "QD6FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.301163606*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_660.dat };
Multipole -name "QD6FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04580065396*$e0] -e0 $e0 -tilt 0.862729249 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_661.dat };
Multipole -name "QD6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-9.14694104*$e0] -e0 $e0 -tilt -0.008496430778 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_662.dat };
Drift -name "L215A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_663.dat };
#Bpm -name "MQD6FF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_664.dat };
Drift -name "L215B" -length 1.066 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_665.dat };
Multipole -name "SK3FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_666.dat };
Multipole -name "SK3FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_667.dat };
Drift -name "L215C" -length 0.285755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_668.dat };
Multipole -name "QF5BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.01745259381*$e0] -e0 $e0 -tilt 0.8505662066 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_669.dat };
Multipole -name "QF5BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.762728669*$e0] -e0 $e0 -tilt 0.01199774474 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_670.dat };
Quadrupole -name "QF5BFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1880283172*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_671.dat };
Multipole -name "QF5BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.01745259381*$e0] -e0 $e0 -tilt 0.8505662066 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_672.dat };
Multipole -name "QF5BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.762728669*$e0] -e0 $e0 -tilt 0.01199774474 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_673.dat };
Drift -name "AQF5BFF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_674.dat };
Multipole -name "QF5BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.01745259381*$e0] -e0 $e0 -tilt 0.8505662066 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_675.dat };
Multipole -name "QF5BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.762728669*$e0] -e0 $e0 -tilt 0.01199774474 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_676.dat };
Quadrupole -name "QF5BFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1880283172*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_677.dat };
Multipole -name "QF5BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.01745259381*$e0] -e0 $e0 -tilt 0.8505662066 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_678.dat };
Multipole -name "QF5BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.762728669*$e0] -e0 $e0 -tilt 0.01199774474 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_679.dat };
Drift -name "L216A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_680.dat };
Bpm -name "MQF5BFF" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_681.dat };
Drift -name "L216B" -length 0.390960984 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_682.dat };
Multipole -name "SF5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.002305870916*$e0] -e0 $e0 -tilt -0.04939281783 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_683.dat };
Multipole -name "SF5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -10.79174071*$e0] -e0 $e0 -tilt 0.5820673055 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_684.dat };
Drift -name "SF5FFMULT5" -length 0 -tilt 0.3670427417 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_685.dat };
Drift -name "SF5FFMULT6" -length 0 -tilt -0.3497390488 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_686.dat };
Drift -name "SF5FFMULT7" -length 0 -tilt -0.08360127117 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_687.dat };
Drift -name "SF5FFMULT8" -length 0 -tilt 0.003839724354 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_688.dat };
Drift -name "SF5FFMULT9" -length 0 -tilt 0.2176425577 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_689.dat };
Multipole -name "SF5FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr -0.3954341426*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_690.dat };
Multipole -name "SF5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.002305870916*$e0] -e0 $e0 -tilt -0.04939281783 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_691.dat };
Multipole -name "SF5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -10.79174071*$e0] -e0 $e0 -tilt 0.5820673055 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_692.dat };
Drift -name "SF5FFMULT5" -length 0 -tilt 0.3670427417 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_693.dat };
Drift -name "SF5FFMULT6" -length 0 -tilt -0.3497390488 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_694.dat };
Drift -name "SF5FFMULT7" -length 0 -tilt -0.08360127117 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_695.dat };
Drift -name "SF5FFMULT8" -length 0 -tilt 0.003839724354 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_696.dat };
Drift -name "SF5FFMULT9" -length 0 -tilt 0.2176425577 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_697.dat };
Drift -name "ASF5FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_698.dat };
Multipole -name "SF5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.002305870916*$e0] -e0 $e0 -tilt -0.04939281783 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_699.dat };
Multipole -name "SF5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -10.79174071*$e0] -e0 $e0 -tilt 0.5820673055 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_700.dat };
Drift -name "SF5FFMULT5" -length 0 -tilt 0.3670427417 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_701.dat };
Drift -name "SF5FFMULT6" -length 0 -tilt -0.3497390488 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_702.dat };
Drift -name "SF5FFMULT7" -length 0 -tilt -0.08360127117 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_703.dat };
Drift -name "SF5FFMULT8" -length 0 -tilt 0.003839724354 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_704.dat };
Drift -name "SF5FFMULT9" -length 0 -tilt 0.2176425577 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_705.dat };
Multipole -name "SF5FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr -0.3954341426*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_706.dat };
Multipole -name "SF5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.002305870916*$e0] -e0 $e0 -tilt -0.04939281783 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_707.dat };
Multipole -name "SF5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -10.79174071*$e0] -e0 $e0 -tilt 0.5820673055 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_708.dat };
Drift -name "SF5FFMULT5" -length 0 -tilt 0.3670427417 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_709.dat };
Drift -name "SF5FFMULT6" -length 0 -tilt -0.3497390488 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_710.dat };
Drift -name "SF5FFMULT7" -length 0 -tilt -0.08360127117 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_711.dat };
Drift -name "SF5FFMULT8" -length 0 -tilt 0.003839724354 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_712.dat };
Drift -name "SF5FFMULT9" -length 0 -tilt 0.2176425577 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_713.dat };
Drift -name "L217A" -length 0.099791984 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_714.dat };
#Bpm -name "MSF5FF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_715.dat };
Drift -name "L217B" -length 0.310924 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_716.dat };
Multipole -name "QF5AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.007439676528*$e0] -e0 $e0 -tilt 0.3292327923 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_717.dat };
Multipole -name "QF5AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.246966194*$e0] -e0 $e0 -tilt 0.512545994 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_718.dat };
Quadrupole -name "QF5AFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1880283172*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_719.dat };
Multipole -name "QF5AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.007439676528*$e0] -e0 $e0 -tilt 0.3292327923 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_720.dat };
Multipole -name "QF5AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.246966194*$e0] -e0 $e0 -tilt 0.512545994 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_721.dat };
Drift -name "AQF5AFF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_722.dat };
Multipole -name "QF5AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.007439676528*$e0] -e0 $e0 -tilt 0.3292327923 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_723.dat };
Multipole -name "QF5AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.246966194*$e0] -e0 $e0 -tilt 0.512545994 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_724.dat };
Quadrupole -name "QF5AFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1880283172*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_725.dat };
Multipole -name "QF5AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.007439676528*$e0] -e0 $e0 -tilt 0.3292327923 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_726.dat };
Multipole -name "QF5AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.246966194*$e0] -e0 $e0 -tilt 0.512545994 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_727.dat };
Drift -name "L218A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_728.dat };
#Bpm -name "MQF5AFF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_729.dat };
Drift -name "L218B" -length 0.223 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_730.dat };
#Drift -name "MREF2FF" -length 0  #REMOVED
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_731.dat };
Drift -name "L218C" -length 0.858755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_732.dat };
Multipole -name "QD4BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03539761723*$e0] -e0 $e0 -tilt -0.06709968635 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_733.dat };
Multipole -name "QD4BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.41320609*$e0] -e0 $e0 -tilt -0.1275036164 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_734.dat };
Quadrupole -name "QD4BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1483961368*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_735.dat };
Multipole -name "QD4BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03539761723*$e0] -e0 $e0 -tilt -0.06709968635 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_736.dat };
Multipole -name "QD4BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.41320609*$e0] -e0 $e0 -tilt -0.1275036164 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_737.dat };
Drift -name "AQD4BFF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_738.dat };
Multipole -name "QD4BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03539761723*$e0] -e0 $e0 -tilt -0.06709968635 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_739.dat };
Multipole -name "QD4BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.41320609*$e0] -e0 $e0 -tilt -0.1275036164 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_740.dat };
Quadrupole -name "QD4BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1483961368*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_741.dat };
Multipole -name "QD4BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03539761723*$e0] -e0 $e0 -tilt -0.06709968635 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_742.dat };
Multipole -name "QD4BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.41320609*$e0] -e0 $e0 -tilt -0.1275036164 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_743.dat };
Drift -name "L219A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_744.dat };
Bpm -name "MQD4BFF" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_745.dat };
Drift -name "L219B" -length 0.390960984 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_746.dat };
Multipole -name "SD4FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.441469053*$e0] -e0 $e0 -tilt -0.5440191278 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_747.dat };
Multipole -name "SD4FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 34.2076517*$e0] -e0 $e0 -tilt -0.3703588673 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_748.dat };
Drift -name "SD4FFMULT5" -length 0 -tilt -0.05707226654 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_749.dat };
Drift -name "SD4FFMULT6" -length 0 -tilt -0.323733643 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_750.dat };
Drift -name "SD4FFMULT7" -length 0 -tilt -0.347669587 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_751.dat };
Drift -name "SD4FFMULT8" -length 0 -tilt -0.001919862177 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_752.dat };
Drift -name "SD4FFMULT9" -length 0 -tilt 0.02408554368 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_753.dat };
Multipole -name "SD4FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr 7.817968185*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_754.dat };
Multipole -name "SD4FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.441469053*$e0] -e0 $e0 -tilt -0.5440191278 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_755.dat };
Multipole -name "SD4FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 34.2076517*$e0] -e0 $e0 -tilt -0.3703588673 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_756.dat };
Drift -name "SD4FFMULT5" -length 0 -tilt -0.05707226654 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_757.dat };
Drift -name "SD4FFMULT6" -length 0 -tilt -0.323733643 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_758.dat };
Drift -name "SD4FFMULT7" -length 0 -tilt -0.347669587 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_759.dat };
Drift -name "SD4FFMULT8" -length 0 -tilt -0.001919862177 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_760.dat };
Drift -name "SD4FFMULT9" -length 0 -tilt 0.02408554368 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_761.dat };
Drift -name "ASD4FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_762.dat };
Multipole -name "SD4FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.441469053*$e0] -e0 $e0 -tilt -0.5440191278 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_763.dat };
Multipole -name "SD4FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 34.2076517*$e0] -e0 $e0 -tilt -0.3703588673 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_764.dat };
Drift -name "SD4FFMULT5" -length 0 -tilt -0.05707226654 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_765.dat };
Drift -name "SD4FFMULT6" -length 0 -tilt -0.323733643 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_766.dat };
Drift -name "SD4FFMULT7" -length 0 -tilt -0.347669587 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_767.dat };
Drift -name "SD4FFMULT8" -length 0 -tilt -0.001919862177 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_768.dat };
Drift -name "SD4FFMULT9" -length 0 -tilt 0.02408554368 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_769.dat };
Multipole -name "SD4FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr 7.817968185*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_770.dat };
Multipole -name "SD4FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.441469053*$e0] -e0 $e0 -tilt -0.5440191278 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_771.dat };
Multipole -name "SD4FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 34.2076517*$e0] -e0 $e0 -tilt -0.3703588673 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_772.dat };
Drift -name "SD4FFMULT5" -length 0 -tilt -0.05707226654 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_773.dat };
Drift -name "SD4FFMULT6" -length 0 -tilt -0.323733643 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_774.dat };
Drift -name "SD4FFMULT7" -length 0 -tilt -0.347669587 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_775.dat };
Drift -name "SD4FFMULT8" -length 0 -tilt -0.001919862177 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_776.dat };
Drift -name "SD4FFMULT9" -length 0 -tilt 0.02408554368 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_777.dat };
Drift -name "L220A" -length 0.099791984 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_778.dat };
#Bpm -name "MSD4FF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_779.dat };
Drift -name "L220B" -length 0.310924 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_780.dat };
Multipole -name "QD4AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03253817197*$e0] -e0 $e0 -tilt -0.2852693742 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_781.dat };
Multipole -name "QD4AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1914310165*$e0] -e0 $e0 -tilt 0.1117542814 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_782.dat };
Quadrupole -name "QD4AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1483961368*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_783.dat };
Multipole -name "QD4AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03253817197*$e0] -e0 $e0 -tilt -0.2852693742 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_784.dat };
Multipole -name "QD4AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1914310165*$e0] -e0 $e0 -tilt 0.1117542814 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_785.dat };
Drift -name "AQD4AFF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_786.dat };
Multipole -name "QD4AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03253817197*$e0] -e0 $e0 -tilt -0.2852693742 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_787.dat };
Multipole -name "QD4AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1914310165*$e0] -e0 $e0 -tilt 0.1117542814 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_788.dat };
Quadrupole -name "QD4AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1483961368*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_789.dat };
Multipole -name "QD4AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03253817197*$e0] -e0 $e0 -tilt -0.2852693742 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_790.dat };
Multipole -name "QD4AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1914310165*$e0] -e0 $e0 -tilt 0.1117542814 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_791.dat };
Drift -name "L221A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_792.dat };
#Bpm -name "MQD4AFF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_793.dat };
Drift -name "L221B" -length 0.266 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_794.dat };
Multipole -name "SK2FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_795.dat };
Multipole -name "SK2FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_796.dat };
Drift -name "L221C" -length 1.028629871 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_797.dat };
Quadrupole -name "B2FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.508793732e-05*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_798.dat };
Multipole -name "B2FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00495539408*$e0] -e0 $e0 -tilt 0.2883807683 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_799.dat };
Multipole -name "B2FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.2263190599*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_800.dat };
Multipole -name "B2FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -434.5325949*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_801.dat };
Sbend -name "B2FFA" -synrad $sbend_synrad -length 0.3064116254 -angle -0.01508793732 -E1 -0.01508793732 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.01905 -fint 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_802.dat };
set e0 [expr $e0-14.1e-6*-0.01508793732*-0.01508793732/0.3064116254*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_803.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_804.dat };
Quadrupole -name "B2FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.508793732e-05*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_805.dat };
Multipole -name "B2FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00495539408*$e0] -e0 $e0 -tilt 0.2883807683 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_806.dat };
Multipole -name "B2FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.2263190599*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_807.dat };
Multipole -name "B2FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -434.5325949*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_808.dat };
Quadrupole -name "B2FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.508793732e-05*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_809.dat };
Multipole -name "B2FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00495539408*$e0] -e0 $e0 -tilt 0.2883807683 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_810.dat };
Multipole -name "B2FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.2263190599*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_811.dat };
Multipole -name "B2FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -434.5325949*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_812.dat };
Sbend -name "B2FFB" -synrad $sbend_synrad -length 0.3064116254 -angle -0.01508793732 -E1 0 -E2 -0.01508793732 -six_dim 1 -e0 $e0 -hgap 0.01905 -fintx 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_813.dat };
set e0 [expr $e0-14.1e-6*-0.01508793732*-0.01508793732/0.3064116254*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_814.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_815.dat };
Quadrupole -name "B2FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.508793732e-05*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_816.dat };
Multipole -name "B2FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00495539408*$e0] -e0 $e0 -tilt 0.2883807683 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_817.dat };
Multipole -name "B2FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.2263190599*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_818.dat };
Multipole -name "B2FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -434.5325949*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_819.dat };
Drift -name "L222" -length 0.444384871 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_820.dat };
Multipole -name "QF3FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0432486468*$e0] -e0 $e0 -tilt -0.6574387986 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_821.dat };
Multipole -name "QF3FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.770240438*$e0] -e0 $e0 -tilt 0.04499189406 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_822.dat };
Quadrupole -name "QF3FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.2763599368*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_823.dat };
Multipole -name "QF3FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0432486468*$e0] -e0 $e0 -tilt -0.6574387986 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_824.dat };
Multipole -name "QF3FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.770240438*$e0] -e0 $e0 -tilt 0.04499189406 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_825.dat };
Drift -name "AQF3FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_826.dat };
Multipole -name "QF3FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0432486468*$e0] -e0 $e0 -tilt -0.6574387986 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_827.dat };
Multipole -name "QF3FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.770240438*$e0] -e0 $e0 -tilt 0.04499189406 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_828.dat };
Quadrupole -name "QF3FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.2763599368*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_829.dat };
Multipole -name "QF3FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0432486468*$e0] -e0 $e0 -tilt -0.6574387986 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_830.dat };
Multipole -name "QF3FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.770240438*$e0] -e0 $e0 -tilt 0.04499189406 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_831.dat };
Drift -name "L223A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_832.dat };
Bpm -name "MQF3FF" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_833.dat };
Drift -name "L223B" -length 0.4246045352 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_834.dat };
Quadrupole -name "B1FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -3.093740393e-05*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_835.dat };
Multipole -name "B1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04862213291*$e0] -e0 $e0 -tilt 0.04625572479 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_836.dat };
Multipole -name "B1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.08070627111*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_837.dat };
Multipole -name "B1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1097.605287*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_838.dat };
Sbend -name "B1FFA" -synrad $sbend_synrad -length 0.3064369612 -angle -0.02690209037 -E1 -0.02690209037 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.01905 -fint 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_839.dat };
set e0 [expr $e0-14.1e-6*-0.02690209037*-0.02690209037/0.3064369612*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_840.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_841.dat };
Quadrupole -name "B1FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -3.093740393e-05*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_842.dat };
Multipole -name "B1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04862213291*$e0] -e0 $e0 -tilt 0.04625572479 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_843.dat };
Multipole -name "B1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.08070627111*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_844.dat };
Multipole -name "B1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1097.605287*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_845.dat };
Quadrupole -name "B1FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -3.093740393e-05*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_846.dat };
Multipole -name "B1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04862213291*$e0] -e0 $e0 -tilt 0.04625572479 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_847.dat };
Multipole -name "B1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.08070627111*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_848.dat };
Multipole -name "B1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1097.605287*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_849.dat };
Sbend -name "B1FFB" -synrad $sbend_synrad -length 0.3064369612 -angle -0.02690209037 -E1 0 -E2 -0.02690209037 -six_dim 1 -e0 $e0 -hgap 0.01905 -fintx 0.5 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_850.dat };
set e0 [expr $e0-14.1e-6*-0.02690209037*-0.02690209037/0.3064369612*$e0*$e0*$e0*$e0*$sbend_synrad]
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_851.dat };
SetReferenceEnergy $e0
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_852.dat };
Quadrupole -name "B1FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -3.093740393e-05*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_853.dat };
Multipole -name "B1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04862213291*$e0] -e0 $e0 -tilt 0.04625572479 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_854.dat };
Multipole -name "B1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.08070627111*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_855.dat };
Multipole -name "B1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1097.605287*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_856.dat };
Drift -name "L224A" -length 0.2406045352 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_857.dat };
Drift -name "MREF1FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_858.dat };
Drift -name "L224B" -length 0.203755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_859.dat };
Multipole -name "QD2BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01829512447*$e0] -e0 $e0 -tilt 0.006571980611 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_860.dat };
Multipole -name "QD2BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.488201167*$e0] -e0 $e0 -tilt 0.6925477838 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_861.dat };
Quadrupole -name "QD2BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.09935648446*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_862.dat };
Multipole -name "QD2BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01829512447*$e0] -e0 $e0 -tilt 0.006571980611 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_863.dat };
Multipole -name "QD2BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.488201167*$e0] -e0 $e0 -tilt 0.6925477838 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_864.dat };
Drift -name "AQD2BFF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_865.dat };
Multipole -name "QD2BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01829512447*$e0] -e0 $e0 -tilt 0.006571980611 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_866.dat };
Multipole -name "QD2BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.488201167*$e0] -e0 $e0 -tilt 0.6925477838 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_867.dat };
Quadrupole -name "QD2BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.09935648446*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_868.dat };
Multipole -name "QD2BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01829512447*$e0] -e0 $e0 -tilt 0.006571980611 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_869.dat };
Multipole -name "QD2BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.488201167*$e0] -e0 $e0 -tilt 0.6925477838 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_870.dat };
Drift -name "L225A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_871.dat };
Bpm -name "MQD2BFF" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_872.dat };
Drift -name "L225B" -length 0.354 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_873.dat };
Drift -name "MT1X" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_874.dat };
Drift -name "L225C" -length 0.137 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_875.dat };
Drift -name "FFML1" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_876.dat };
Drift -name "L225D" -length 0.7235 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_877.dat };
Drift -name "MREFIPFF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_878.dat };
Drift -name "L225E" -length 0.267255 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_879.dat };
Multipole -name "QD2AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.007820933842*$e0] -e0 $e0 -tilt 0.4505632088 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_880.dat };
Multipole -name "QD2AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-3.237581097*$e0] -e0 $e0 -tilt -0.009734334147 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_881.dat };
Quadrupole -name "QD2AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1448640481*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_882.dat };
Multipole -name "QD2AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.007820933842*$e0] -e0 $e0 -tilt 0.4505632088 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_883.dat };
Multipole -name "QD2AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-3.237581097*$e0] -e0 $e0 -tilt -0.009734334147 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_884.dat };
Drift -name "AQD2AFF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_885.dat };
Multipole -name "QD2AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.007820933842*$e0] -e0 $e0 -tilt 0.4505632088 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_886.dat };
Multipole -name "QD2AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-3.237581097*$e0] -e0 $e0 -tilt -0.009734334147 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_887.dat };
Quadrupole -name "QD2AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1448640481*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_888.dat };
Multipole -name "QD2AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.007820933842*$e0] -e0 $e0 -tilt 0.4505632088 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_889.dat };
Multipole -name "QD2AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-3.237581097*$e0] -e0 $e0 -tilt -0.009734334147 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_890.dat };
Drift -name "L226A" -length 0.019755 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_891.dat };
Bpm -name "MQD2AFF" -length 0 -resolution $cband -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_892.dat };
Drift -name "L226B" -length 0.391 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_893.dat };
Drift -name "MREFSFF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_894.dat };
Drift -name "L226C" -length 3.825 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_895.dat };
Multipole -name "SK1FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_896.dat };
Multipole -name "SK1FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_897.dat };
Drift -name "L226D" -length 0.161 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_898.dat };
Bpm -name "MSF1FF" -length 0 -resolution $sband -short_range_wake $sband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_899.dat };
Drift -name "L226E" -length 0.099 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_900.dat };
Multipole -name "SF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1499119875*$e0] -e0 $e0 -tilt -0.4398229715 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_901.dat };
Multipole -name "SF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -64.19233541*$e0] -e0 $e0 -tilt 0.6283185307 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_902.dat };
Drift -name "SF1FFMULT5" -length 0 -tilt -0.2705260341 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_903.dat };
Drift -name "SF1FFMULT6" -length 0 -tilt 0.8459860217 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_904.dat };
Drift -name "SF1FFMULT7" -length 0 -tilt 4.196644186 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_905.dat };
Drift -name "SF1FFMULT8" -length 0 -tilt 3.380702761 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_906.dat };
Drift -name "SF1FFMULT9" -length 0 -tilt 1.082104136 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_907.dat };
Multipole -name "SF1FF" -synrad $mult_synrad -type 3 -length 0.05 -strength [expr -1.505168272*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_908.dat };
Multipole -name "SF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1499119875*$e0] -e0 $e0 -tilt -0.4398229715 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_909.dat };
Multipole -name "SF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -64.19233541*$e0] -e0 $e0 -tilt 0.6283185307 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_910.dat };
Drift -name "SF1FFMULT5" -length 0 -tilt -0.2705260341 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_911.dat };
Drift -name "SF1FFMULT6" -length 0 -tilt 0.8459860217 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_912.dat };
Drift -name "SF1FFMULT7" -length 0 -tilt 4.196644186 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_913.dat };
Drift -name "SF1FFMULT8" -length 0 -tilt 3.380702761 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_914.dat };
Drift -name "SF1FFMULT9" -length 0 -tilt 1.082104136 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_915.dat };
Drift -name "ASF1FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_916.dat };
Multipole -name "SF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1499119875*$e0] -e0 $e0 -tilt -0.4398229715 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_917.dat };
Multipole -name "SF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -64.19233541*$e0] -e0 $e0 -tilt 0.6283185307 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_918.dat };
Drift -name "SF1FFMULT5" -length 0 -tilt -0.2705260341 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_919.dat };
Drift -name "SF1FFMULT6" -length 0 -tilt 0.8459860217 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_920.dat };
Drift -name "SF1FFMULT7" -length 0 -tilt 4.196644186 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_921.dat };
Drift -name "SF1FFMULT8" -length 0 -tilt 3.380702761 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_922.dat };
Drift -name "SF1FFMULT9" -length 0 -tilt 1.082104136 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_923.dat };
Multipole -name "SF1FF" -synrad $mult_synrad -type 3 -length 0.05 -strength [expr -1.505168272*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_924.dat };
Multipole -name "SF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1499119875*$e0] -e0 $e0 -tilt -0.4398229715 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_925.dat };
Multipole -name "SF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -64.19233541*$e0] -e0 $e0 -tilt 0.6283185307 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_926.dat };
Drift -name "SF1FFMULT5" -length 0 -tilt -0.2705260341 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_927.dat };
Drift -name "SF1FFMULT6" -length 0 -tilt 0.8459860217 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_928.dat };
Drift -name "SF1FFMULT7" -length 0 -tilt 4.196644186 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_929.dat };
Drift -name "SF1FFMULT8" -length 0 -tilt 3.380702761 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_930.dat };
Drift -name "SF1FFMULT9" -length 0 -tilt 1.082104136 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_931.dat };
Drift -name "L227" -length 0.30295 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_932.dat };
Multipole -name "QF1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0004336394638*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_933.dat };
Multipole -name "QF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.02131894177*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_934.dat };
Multipole -name "QF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -0.6484970075*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_935.dat };
Drift -name "QF1FFMULT5" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_936.dat };
Drift -name "QF1FFMULT6" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_937.dat };
Drift -name "QF1FFMULT7" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_938.dat };
Drift -name "QF1FFMULT8" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_939.dat };
Drift -name "QF1FFMULT9" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_940.dat };
Drift -name "QF1FFMULT2S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_941.dat };
Drift -name "QF1FFMULT3S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_942.dat };
Drift -name "QF1FFMULT4S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_943.dat };
Drift -name "QF1FFMULT5S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_944.dat };
Drift -name "QF1FFMULT6S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_945.dat };
Drift -name "QF1FFMULT7S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_946.dat };
Drift -name "QF1FFMULT8S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_947.dat };
Drift -name "QF1FFMULT9S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_948.dat };
Quadrupole -name "QF1FF" -synrad $quad_synrad -length 0.22205 -strength [expr 0.369493027*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_949.dat };
Multipole -name "QF1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0004336394638*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_950.dat };
Multipole -name "QF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.02131894177*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_951.dat };
Multipole -name "QF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -0.6484970075*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_952.dat };
Drift -name "QF1FFMULT5" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_953.dat };
Drift -name "QF1FFMULT6" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_954.dat };
Drift -name "QF1FFMULT7" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_955.dat };
Drift -name "QF1FFMULT8" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_956.dat };
Drift -name "QF1FFMULT9" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_957.dat };
Drift -name "QF1FFMULT2S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_958.dat };
Drift -name "QF1FFMULT3S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_959.dat };
Drift -name "QF1FFMULT4S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_960.dat };
Drift -name "QF1FFMULT5S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_961.dat };
Drift -name "QF1FFMULT6S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_962.dat };
Drift -name "QF1FFMULT7S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_963.dat };
Drift -name "QF1FFMULT8S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_964.dat };
Drift -name "QF1FFMULT9S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_965.dat };
Drift -name "AQF1FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_966.dat };
Multipole -name "QF1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0004336394638*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_967.dat };
Multipole -name "QF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.02131894177*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_968.dat };
Multipole -name "QF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -0.6484970075*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_969.dat };
Drift -name "QF1FFMULT5" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_970.dat };
Drift -name "QF1FFMULT6" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_971.dat };
Drift -name "QF1FFMULT7" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_972.dat };
Drift -name "QF1FFMULT8" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_973.dat };
Drift -name "QF1FFMULT9" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_974.dat };
Drift -name "QF1FFMULT2S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_975.dat };
Drift -name "QF1FFMULT3S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_976.dat };
Drift -name "QF1FFMULT4S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_977.dat };
Drift -name "QF1FFMULT5S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_978.dat };
Drift -name "QF1FFMULT6S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_979.dat };
Drift -name "QF1FFMULT7S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_980.dat };
Drift -name "QF1FFMULT8S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_981.dat };
Drift -name "QF1FFMULT9S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_982.dat };
Quadrupole -name "QF1FF" -synrad $quad_synrad -length 0.22205 -strength [expr 0.369493027*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_983.dat };
Multipole -name "QF1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0004336394638*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_984.dat };
Multipole -name "QF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.02131894177*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_985.dat };
Multipole -name "QF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -0.6484970075*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_986.dat };

TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_987.dat };
Drift -name "QF1FFMULT5" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_988.dat };
Drift -name "QF1FFMULT6" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_989.dat };
Drift -name "QF1FFMULT7" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_990.dat };
Drift -name "QF1FFMULT8" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_991.dat };
Drift -name "QF1FFMULT9" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_992.dat };
Drift -name "QF1FFMULT2S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_993.dat };
Drift -name "QF1FFMULT3S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_994.dat };
Drift -name "QF1FFMULT4S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_995.dat };
Drift -name "QF1FFMULT5S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_996.dat };
Drift -name "QF1FFMULT6S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_997.dat };
Drift -name "QF1FFMULT7S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_998.dat };
Drift -name "QF1FFMULT8S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_999.dat };
Drift -name "QF1FFMULT9S" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1000.dat };
Drift -name "L228A" -length 0.12195 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1001.dat };
# not present anymore
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1002.dat };
#Bpm -name "MQF1FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1003.dat };
Drift -name "L228B" -length 0.297 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1004.dat };
#Bpm -name "MSD0FF" -length 0 -resolution $sband -short_range_wake $sband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1005.dat };
Drift -name "L228C" -length 0.099 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1006.dat };
Multipole -name "SD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2963503459*$e0] -e0 $e0 -tilt 0.07679448709 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1007.dat };
Multipole -name "SD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 78.90731241*$e0] -e0 $e0 -tilt 0.6283185307 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1008.dat };
Drift -name "SD0FFMULT5" -length 0 -tilt 0.2391101075 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1009.dat };
Drift -name "SD0FFMULT6" -length 0 -tilt 0.5667333414 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1010.dat };
Drift -name "SD0FFMULT7" -length 0 -tilt -0.3447025273 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1011.dat };
Drift -name "SD0FFMULT8" -length 0 -tilt -0.2862339973 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1012.dat };
Drift -name "SD0FFMULT9" -length 0 -tilt 0.2513274123 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1013.dat };
Multipole -name "SD0FF" -synrad $mult_synrad -type 3 -length 0.05 -strength [expr 2.250119161*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1014.dat };
Multipole -name "SD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2963503459*$e0] -e0 $e0 -tilt 0.07679448709 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1015.dat };
Multipole -name "SD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 78.90731241*$e0] -e0 $e0 -tilt 0.6283185307 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1016.dat };
Drift -name "SD0FFMULT5" -length 0 -tilt 0.2391101075 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1017.dat };
Drift -name "SD0FFMULT6" -length 0 -tilt 0.5667333414 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1018.dat };
Drift -name "SD0FFMULT7" -length 0 -tilt -0.3447025273 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1019.dat };
Drift -name "SD0FFMULT8" -length 0 -tilt -0.2862339973 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1020.dat };
Drift -name "SD0FFMULT9" -length 0 -tilt 0.2513274123 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1021.dat };
Drift -name "ASD0FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1022.dat };
Multipole -name "SD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2963503459*$e0] -e0 $e0 -tilt 0.07679448709 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1023.dat };
Multipole -name "SD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 78.90731241*$e0] -e0 $e0 -tilt 0.6283185307 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1024.dat };
Drift -name "SD0FFMULT5" -length 0 -tilt 0.2391101075 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1025.dat };
Drift -name "SD0FFMULT6" -length 0 -tilt 0.5667333414 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1026.dat };
Drift -name "SD0FFMULT7" -length 0 -tilt -0.3447025273 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1027.dat };
Drift -name "SD0FFMULT8" -length 0 -tilt -0.2862339973 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1028.dat };
Drift -name "SD0FFMULT9" -length 0 -tilt 0.2513274123 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1029.dat };
Multipole -name "SD0FF" -synrad $mult_synrad -type 3 -length 0.05 -strength [expr 2.250119161*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1030.dat };
Multipole -name "SD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2963503459*$e0] -e0 $e0 -tilt 0.07679448709 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1031.dat };
Multipole -name "SD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 78.90731241*$e0] -e0 $e0 -tilt 0.6283185307 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1032.dat };
Drift -name "SD0FFMULT5" -length 0 -tilt 0.2391101075 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1033.dat };
Drift -name "SD0FFMULT6" -length 0 -tilt 0.5667333414 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1034.dat };
Drift -name "SD0FFMULT7" -length 0 -tilt -0.3447025273 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1035.dat };
Drift -name "SD0FFMULT8" -length 0 -tilt -0.2862339973 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1036.dat };
Drift -name "SD0FFMULT9" -length 0 -tilt 0.2513274123 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1037.dat };
Drift -name "L229" -length 0.2875 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1038.dat };
Multipole -name "QD0FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01735176498*$e0] -e0 $e0 -tilt 0.2541199391 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1039.dat };
Multipole -name "QD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.072664521*$e0] -e0 $e0 -tilt 0.641757566 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1040.dat };
Multipole -name "QD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -585.4643194*$e0] -e0 $e0 -tilt 0.5410520681 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1041.dat };
Drift -name "QD0FFMULT5" -length 0 -tilt 0.5134758659 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1042.dat };
Drift -name "QD0FFMULT6" -length 0 -tilt 0.1261374384 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1043.dat };
Drift -name "QD0FFMULT7" -length 0 -tilt 0.3644247478 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1044.dat };
Drift -name "QD0FFMULT8" -length 0 -tilt -0.01239183769 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1045.dat };
Drift -name "QD0FFMULT9" -length 0 -tilt -0.001047197551 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1046.dat };
Quadrupole -name "QD0FF" -synrad $quad_synrad -length 0.2375 -strength [expr -0.6811853675*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1047.dat };
Multipole -name "QD0FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01735176498*$e0] -e0 $e0 -tilt 0.2541199391 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1048.dat };
Multipole -name "QD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.072664521*$e0] -e0 $e0 -tilt 0.641757566 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1049.dat };
Multipole -name "QD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -585.4643194*$e0] -e0 $e0 -tilt 0.5410520681 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1050.dat };
Drift -name "QD0FFMULT5" -length 0 -tilt 0.5134758659 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1051.dat };
Drift -name "QD0FFMULT6" -length 0 -tilt 0.1261374384 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1052.dat };
Drift -name "QD0FFMULT7" -length 0 -tilt 0.3644247478 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1053.dat };
Drift -name "QD0FFMULT8" -length 0 -tilt -0.01239183769 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1054.dat };
Drift -name "QD0FFMULT9" -length 0 -tilt -0.001047197551 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1055.dat };
Drift -name "AQD0FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1056.dat };
Multipole -name "QD0FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01735176498*$e0] -e0 $e0 -tilt 0.2541199391 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1057.dat };
Multipole -name "QD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.072664521*$e0] -e0 $e0 -tilt 0.641757566 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1058.dat };
Multipole -name "QD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -585.4643194*$e0] -e0 $e0 -tilt 0.5410520681 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1059.dat };
Drift -name "QD0FFMULT5" -length 0 -tilt 0.5134758659 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1060.dat };
Drift -name "QD0FFMULT6" -length 0 -tilt 0.1261374384 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1061.dat };
Drift -name "QD0FFMULT7" -length 0 -tilt 0.3644247478 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1062.dat };
Drift -name "QD0FFMULT8" -length 0 -tilt -0.01239183769 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1063.dat };
Drift -name "QD0FFMULT9" -length 0 -tilt -0.001047197551 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1064.dat };
Quadrupole -name "QD0FF" -synrad $quad_synrad -length 0.2375 -strength [expr -0.6811853675*$e0] -e0 $e0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1065.dat };
Multipole -name "QD0FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01735176498*$e0] -e0 $e0 -tilt 0.2541199391 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1066.dat };
Multipole -name "QD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.072664521*$e0] -e0 $e0 -tilt 0.641757566 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1067.dat };
Multipole -name "QD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -585.4643194*$e0] -e0 $e0 -tilt 0.5410520681 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1068.dat };
Drift -name "QD0FFMULT5" -length 0 -tilt 0.5134758659 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1069.dat };
Drift -name "QD0FFMULT6" -length 0 -tilt 0.1261374384 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1070.dat };
Drift -name "QD0FFMULT7" -length 0 -tilt 0.3644247478 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1071.dat };
Drift -name "QD0FFMULT8" -length 0 -tilt -0.01239183769 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1072.dat };
Drift -name "QD0FFMULT9" -length 0 -tilt -0.001047197551 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1073.dat };
Drift -name "L230A" -length 0.1065 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1074.dat };
# not present anymore
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1075.dat };
#Bpm -name "MQD0FF" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1076.dat };
Drift -name "L230B" -length 0.286 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1077.dat };
VCORRECTOR -name "IPKICK" -length 0.12 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1078.dat };
Drift -name "L230C" -length 0.048 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1079.dat };
Bpm -name "MPREIP" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1080.dat };
Drift -name "L230D" -length 0.2758 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1081.dat };
Bpm -name "IPBPMA" -length 0 -resolution $ipbpm
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1082.dat };
Drift -name "L230E" -length 0.081 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1083.dat };
Bpm -name "IPBPMB" -length 0 -resolution $ipbpm
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1084.dat };
Drift -name "L230F" -length 0.0702 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1085.dat };
Drift -name "MS1IP" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1086.dat };
Drift -name "MBS1IP" -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1087.dat };
Quadrupole -name "IP" -strength 0 -length 0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1088.dat };

TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1089.dat };

TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1090.dat };
if {[info exist add_bpm_at_ip] && $add_bpm_at_ip} {
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1091.dat };
    CavityBpm -name "BPMIP" -length 0 -resolution $ipbpm
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1092.dat };
}
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1093.dat };
TclCall -name "SAVEBEAMIP" -script { BeamDump -file ip_beam.dat }
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1094.dat };

TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1095.dat };
Drift -name "L231" -length 0.0882 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1096.dat };
Bpm -name "IPBPMC" -length 0 -resolution $ipbpm
TclCall -name "SAVEBEAMIP" -script { BeamDump -file video_beam_1097.dat };

#Drift -name "ENDFF" -length 0 
#Drift -name "L301A" -length 0.3338 
#Drift -name "MW1IP" -length 0 
#Drift -name "L301B" -length 0.203 
# not sure about resolution of MPIP - setting to stripline
#Bpm -name "MPIP" -length 0 -resolution $stripline
#Drift -name "L301C" -length 0.302 
#Drift -name "MSPIP" -length 0 
#Drift -name "L301D" -length 0.573 
#Drift -name "MBDUMPA" -length 0 
#Sbend -name "BDUMPA" -synrad $sbend_synrad -length 0.6784391641 -angle 0.1745329252 -E1 0.1745329252 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
#set e0 [expr $e0-14.1e-6*0.1745329252*0.1745329252/0.6784391641*$e0*$e0*$e0*$e0*$sbend_synrad]
#SetReferenceEnergy $e0
#Sbend -name "BDUMPB" -synrad $sbend_synrad -length 0.6784391641 -angle 0.1745329252 -E1 0 -E2 0.1745329252 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
#set e0 [expr $e0-14.1e-6*0.1745329252*0.1745329252/0.6784391641*$e0*$e0*$e0*$e0*$sbend_synrad]
#SetReferenceEnergy $e0
#Drift -name "MBDUMPB" -length 0 
#Drift -name "L302A" -length 0.6345608359 
#Drift -name "MDUMP" -length 0 
#Drift -name "L302B" -length 0.146 
#Drift -name "ICTDUMP" -length 0 
#Drift -name "DIAMOND" -length 0 
#Drift -name "L302C" -length 1.219439164 
#Drift -name "DUMPH" -length 1.15 
#Drift -name "DUMPH" -length 1.15 
#Drift -name "ATF2MEND" -length 0 
