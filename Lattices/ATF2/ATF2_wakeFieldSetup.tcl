# set oneRefCavity 0
# set twoRefCavity 0
# set bellowWakeStudy 0
# set oneBellow 0
# set oneBellowShielded 0

# set bellow_wake ""
# set cbandref_wake ""

Drift -name "BELLOW_MREF_1" -short_range_wake $bellow_wake

if {$wakeFieldDipole} {
    puts "Wake Field Setup: Dipole"
    Dipole -name "WAKE_DIPOLE"
}

Drift -name "FLANGE_MREF_1" -short_range_wake $flange_wake
if {$oneRefCavity} {
    puts "Wake Field Setup: MREF3FF"
    Drift -name "APERTURESTEP_1" -short_range_wake $aperture_2024_wake
    Drift -name "MREF3FF" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1 -short_range_wake $cbandref_wake
    Drift -name "APERTURESTEP_2" -short_range_wake $aperture_2420_wake
} elseif {$twoRefCavity} {
    puts "Wake Field Setup: MREF2FF and MREF3FF"
    Drift -name "APERTURESTEP_1" -short_range_wake $aperture_2024_wake
    Drift -name "MREF2FF" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1 -short_range_wake $cbandref_wake
    Drift -name "APERTURESTEP_2" -short_range_wake $aperture_2420_wake
    Drift -name "FLANGE_MREF_15" -short_range_wake $flange_wake
    Drift -name "APERTURESTEP_3" -short_range_wake $aperture_2024_wake
    Drift -name "MREF3FF" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1 -short_range_wake $cbandref_wake
    Drift -name "APERTURESTEP_4" -short_range_wake $aperture_2420_wake
} elseif {$bellowWakeStudy} {
    # 3 Bellows setup: http://atf.kek.jp/twiki/bin/view/ATFlogbook/Log20130419s
    puts "Wake Field Setup: 3 Bellows (with masks)"
    Drift -name "BELLOW_MOVER_1" -short_range_wake $bellow_wake
    Drift -name "BELLOW_MOVER_2" -short_range_wake $bellow_wake
    Drift -name "BELLOW_MOVER_3" -short_range_wake $bellow_wake
} elseif {$oneBellow} {
    puts "Wake Field Setup: 1 Bellow (no mask)"
    Drift -name "BELLOW_MOVER_1" -short_range_wake $bellow_wake
} elseif {$oneBellowShielded} {
    puts "Wake Field Setup: 1 Bellow (with mask)"
    # with mask, assume no wakefield, no simulation done yet
    Drift -name "BELLOW_MOVER_1" -short_range_wake ""
} else {
    puts "WARNING: nothing on wakefield setup!"
}
Drift -name "FLANGE_MREF_2" -short_range_wake $flange_wake

Drift -name "BELLOW_MREF_2" -short_range_wake $bellow_wake
