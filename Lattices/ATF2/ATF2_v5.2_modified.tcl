# copied and adapted from v5.1
#
# changes wrt 5.1:
# POSTIP replaced by IPBPM C
# IPKICKER position changed
#

# options for lattice, should be loaded via external settings file:
#

#BPM resolutions [um]
#set stripline 3.0
#set stripline2 3.0
#set font_stripline_resolution 0.5
#set cband 0.2
#set cband_noatt 0.05 
#set sband 1.0
## nominal 0.002 for ipbpm, observed <0.1
#set ipbpm 0.1

# Add wakefields to cavity BPMs
#set cband_wake ""
#set sband_wake ""

# Options to save beam
#set save_beam_ip 0
#set save_beam_ff_start 0
#set save_beam_mfb2ff 0

# Add possibility to add dispersion
#set use_dispersion_ff 0

# Kicker before MFB2FF
#set use_mfb2ffkicker 0

# Wakefield experiment setup
#set wakeFieldSetup 0

if {! [info exist stripline]} {
    set stripline 3.0
}
if {! [info exist stripline2]} {
    set stripline2 3.0
}
if {! [info exist font_stripline_resolution]} {
    set font_stripline_resolution 0.5
}
if {! [info exist cband]} {
    set cband 0.2
}
if {! [info exist cband_noatt]} {
    set cband_noatt 0.2
}
if {! [info exist sband]} {
    set sband 1.0
}
if {! [info exist ipbpm]} {
    set ipbpm 0.1
}

# wakefields:
if {! [info exist cband_wake]} {
    set cband_wake ""
}
if {! [info exist sband_wake]} {
    set sband_wake ""
}

Girder
Quadrupole -name "KEX1MULT" -synrad $quad_synrad -length 0 -strength [expr -0.01846929*$e0] -e0 $e0 
Sbend -name "KEX1A" -synrad $sbend_synrad -length 0.2500010417 -angle 0.0025 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.00635 -fint 0.5 
set e0 [expr $e0-14.1e-6*0.0025*0.0025/0.2500010417*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Quadrupole -name "KEX1MULT" -synrad $quad_synrad -length 0 -strength [expr -0.01846929*$e0] -e0 $e0 
Drift -name "IP01" -length 0 
Quadrupole -name "KEX1MULT" -synrad $quad_synrad -length 0 -strength [expr -0.01846929*$e0] -e0 $e0 
Sbend -name "KEX1B" -synrad $sbend_synrad -length 0.2500010417 -angle 0.0025 -E1 0 -E2 0.005 -six_dim 1 -e0 $e0 -hgap 0.00635 -fintx 0.5 
set e0 [expr $e0-14.1e-6*0.0025*0.0025/0.2500010417*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Quadrupole -name "KEX1MULT" -synrad $quad_synrad -length 0 -strength [expr -0.01846929*$e0] -e0 $e0 
Drift -name "L001" -length 0.9231390392 
Sbend -name "QM6RX" -synrad $sbend_synrad -length 0.09937458845 -angle 0.0023109657 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -K [expr -0.3644313055*$e0] 
set e0 [expr $e0-14.1e-6*0.0023109657*0.0023109657/0.09937458845*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "QM6RX" -synrad $sbend_synrad -length 0.09937458845 -angle 0.0023109657 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -K [expr -0.3644313055*$e0] 
set e0 [expr $e0-14.1e-6*0.0023109657*0.0023109657/0.09937458845*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L002A" -length 0.04685466892 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH100RX" -length 0.24955 
Drift -name "L002B" -length 0.7028333967 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH101RX" -length 0.1679 
Drift -name "L002C" -length 0.05068234611 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV100RX" -length 0.13874 
Drift -name "L002D" -length 0 
Bpm -name "MB1X" -length 0 
Drift -name "L002E" -length 0.1894692706 
Sbend -name "QM7RX" -synrad $sbend_synrad -length 0.0421716479 -angle -0.0045873988 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -K [expr 0.20411*$e0] 
set e0 [expr $e0-14.1e-6*-0.0045873988*-0.0045873988/0.0421716479*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "QM7RX" -synrad $sbend_synrad -length 0.0421716479 -angle -0.0045873988 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -K [expr 0.20411*$e0] 
set e0 [expr $e0-14.1e-6*-0.0045873988*-0.0045873988/0.0421716479*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L003" -length 0.1478305058 
Sbend -name "BS1XA" -synrad $sbend_synrad -length 0.3 -angle 0.01401783271 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
set e0 [expr $e0-14.1e-6*0.01401783271*0.01401783271/0.3*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BS1XB" -synrad $sbend_synrad -length 0.3 -angle 0.01401783271 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
set e0 [expr $e0-14.1e-6*0.01401783271*0.01401783271/0.3*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L004" -length 0.2 
Sbend -name "BS2XA" -synrad $sbend_synrad -length 0.4 -angle 0.03717168307 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
set e0 [expr $e0-14.1e-6*0.03717168307*0.03717168307/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BS2XB" -synrad $sbend_synrad -length 0.4 -angle 0.03717168307 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
set e0 [expr $e0-14.1e-6*0.03717168307*0.03717168307/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L005A" -length 0.1 
Bpm -name "MB2X" -length 0 
Drift -name "L005B" -length 0.1 
Sbend -name "BS3XA" -synrad $sbend_synrad -length 0.5 -angle 0.1175110129 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
set e0 [expr $e0-14.1e-6*0.1175110129*0.1175110129/0.5*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BS3XB" -synrad $sbend_synrad -length 0.5 -angle 0.1175110129 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
set e0 [expr $e0-14.1e-6*0.1175110129*0.1175110129/0.5*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L006A" -length 0.4782345 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV1X" -length 0.128141 
Drift -name "L006B" -length 0.0335945 
Quadrupole -name "QS1X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Quadrupole -name "QS1X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Drift -name "L006C" -length 0.08136 
Quadrupole -name "QF1X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.5379118485*$e0] -e0 $e0 
Quadrupole -name "QF1X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.5379118485*$e0] -e0 $e0 
Drift -name "L007A" -length 0.014695 
Bpm -name "MQF1X" -length 0 
Drift -name "L007B" -length 0.214 
Drift -name "MS1X" -length 0 
Drift -name "L007C" -length 0.271305 
Multipole -name "BH1XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.1839837463*$e0] -e0 $e0 
Sbend -name "BH1XA" -synrad $sbend_synrad -length 0.4 -angle 0.0941996781 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
set e0 [expr $e0-14.1e-6*0.0941996781*0.0941996781/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Multipole -name "BH1XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.1839837463*$e0] -e0 $e0 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZX1X" -length 0 
Multipole -name "BH1XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.1839837463*$e0] -e0 $e0 
Sbend -name "BH1XB" -synrad $sbend_synrad -length 0.4 -angle 0.0941996781 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
set e0 [expr $e0-14.1e-6*0.0941996781*0.0941996781/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Multipole -name "BH1XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.1839837463*$e0] -e0 $e0 
Drift -name "L008A" -length 0.2132345 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV2X" -length 0.128141 
Drift -name "L008B" -length 0.1586245 
Quadrupole -name "QD2X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.4708418816*$e0] -e0 $e0 
Quadrupole -name "QD2X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.4708418816*$e0] -e0 $e0 
Drift -name "L009A" -length 0.014695 
Bpm -name "MQD2X" -length 0 
Drift -name "L009B" -length 0.7578050808 
Quadrupole -name "QF3X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3353340117*$e0] -e0 $e0 
Quadrupole -name "QF3X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3353340117*$e0] -e0 $e0 
Drift -name "L010A" -length 0.026695 
Bpm -name "MQF3X" -length 0 
Drift -name "L010B" -length 0.138725 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH1X" -length 0.11455 
Drift -name "L010C" -length 0.1396545 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV3X" -length 0.128141 
Drift -name "L010D" -length 1.505663876 
Drift -name "MID" -length 0 
Drift -name "L010E" -length 2.053429376 
Quadrupole -name "QF4X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3393545966*$e0] -e0 $e0 
Quadrupole -name "QF4X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3393545966*$e0] -e0 $e0 
Drift -name "L011A" -length 0.026695 
Bpm -name "MQF4X" -length 0 
Drift -name "L011B" -length 0.134725 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH2X" -length 0.11455 
Drift -name "L011C" -length 0.4698350808 
Bpm -name "MQD5X" -length 0 
Drift -name "L011D" -length 0.026695 
Quadrupole -name "QD5X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.4607728075*$e0] -e0 $e0 
Quadrupole -name "QD5X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.4607728075*$e0] -e0 $e0 
Drift -name "L012A" -length 0.1576245 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV4X" -length 0.128141 
Drift -name "L012B" -length 0.2142345 
Multipole -name "BH2XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.1839837463*$e0] -e0 $e0 
Sbend -name "BH2XA" -synrad $sbend_synrad -length 0.4 -angle -0.0941996781 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
set e0 [expr $e0-14.1e-6*-0.0941996781*-0.0941996781/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Multipole -name "BH2XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.1839837463*$e0] -e0 $e0 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZX2X" -length 0 
Multipole -name "BH2XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.1839837463*$e0] -e0 $e0 
Sbend -name "BH2XB" -synrad $sbend_synrad -length 0.4 -angle -0.0941996781 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
set e0 [expr $e0-14.1e-6*-0.0941996781*-0.0941996781/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Multipole -name "BH2XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.1839837463*$e0] -e0 $e0 
Drift -name "L013A" -length 0.2142345 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV5X" -length 0.128141 
Drift -name "L013B" -length 0.1429295 
Bpm -name "MQF6X" -length 0 
Drift -name "L013C" -length 0.014695 
Quadrupole -name "QF6X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.5650485124*$e0] -e0 $e0 
Quadrupole -name "QF6X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.5650485124*$e0] -e0 $e0 
Drift -name "L014A" -length 0.00536 
Quadrupole -name "QS2X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Quadrupole -name "QS2X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Drift -name "L014B" -length 0.334665 
Drift -name "UDDX1" -length 0 
Drift -name "L014C" -length 0.5875079196 
Sbend -name "BH3XA" -synrad $sbend_synrad -length 0.675 -angle -0.1714240956 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
set e0 [expr $e0-14.1e-6*-0.1714240956*-0.1714240956/0.675*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BH3XB" -synrad $sbend_synrad -length 0.675 -angle -0.1714240956 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
set e0 [expr $e0-14.1e-6*-0.1714240956*-0.1714240956/0.675*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L015" -length 0.7650382419 
Quadrupole -name "QF7X" -synrad $quad_synrad -length 0.039335 -strength [expr 0.1915156418*$e0] -e0 $e0 
Quadrupole -name "QF7X" -synrad $quad_synrad -length 0.039335 -strength [expr 0.1915156418*$e0] -e0 $e0 
Drift -name "L016A" -length 0.026665 
Bpm -name "MQF7X" -length 0 
Drift -name "L016B" -length 1.767246207 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH3X" -length 0.11455 
Drift -name "L016C" -length 0.16342 
Quadrupole -name "QD8X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.2950188224*$e0] -e0 $e0 
Quadrupole -name "QD8X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.2950188224*$e0] -e0 $e0 
Drift -name "L017A" -length 0.014695 
Bpm -name "MQD8X" -length 0 
Drift -name "L017B" -length 0.1389295 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV6X" -length 0.128141 
Drift -name "L017C" -length 0.6855607408 
Drift -name "KEX2A" -length 0.2500010417 
Drift -name "IP02" -length 0 
Drift -name "KEX2B" -length 0.2500010417 
Drift -name "MDISP" -length 0 
Drift -name "L101A" -length 0.06027549974 
Sbend -name "BKXA" -synrad $sbend_synrad -length 0.1 -angle 0.0025 -E1 0.005 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.05 -fint 0.5 
set e0 [expr $e0-14.1e-6*0.0025*0.0025/0.1*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BKXB" -synrad $sbend_synrad -length 0.1 -angle 0.0025 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.05 -fintx 0.5 
set e0 [expr $e0-14.1e-6*0.0025*0.0025/0.1*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L101B" -length 0.166 
Bpm -name "MQF9X" -length 0 
Drift -name "L101C" -length 0.054695 
Quadrupole -name "QF9X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3814832824*$e0] -e0 $e0 
Quadrupole -name "QF9X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3814832824*$e0] -e0 $e0 
Drift -name "L102A" -length 0.15742 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH4X" -length 0.11455 
Drift -name "L102B" -length 1.339476477 
Drift -name "FONTK1" -length 0 
Drift -name "L102C" -length 0.3339295 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV7X" -length 0.128141 
Drift -name "L102D" -length 0.0859295 
Bpm -name "FONTP1" -length 0 
Drift -name "L102E" -length 0.125665 
Quadrupole -name "QK1X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Quadrupole -name "QK1X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Drift -name "L102F" -length 0.10242 
Multipole -name "QD10XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03931007799*$e0] -e0 $e0 
Quadrupole -name "QD10X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5833119514*$e0] -e0 $e0 
Multipole -name "QD10XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03931007799*$e0] -e0 $e0 
Multipole -name "QD10XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03931007799*$e0] -e0 $e0 
Quadrupole -name "QD10X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5833119514*$e0] -e0 $e0 
Multipole -name "QD10XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03931007799*$e0] -e0 $e0 
Drift -name "L103A" -length 0.019755 
Bpm -name "MQD10X" -length 0 
Drift -name "L103B" -length 0.9566 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH5X" -length 0.1248 
Drift -name "L103C" -length 0.150355 
Multipole -name "QF11XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02268216962*$e0] -e0 $e0 
Quadrupole -name "QF11X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.5116656769*$e0] -e0 $e0 
Multipole -name "QF11XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02268216962*$e0] -e0 $e0 
Drift -name "MSKEW" -length 0 
Multipole -name "QF11XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02268216962*$e0] -e0 $e0 
Quadrupole -name "QF11X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.5116656769*$e0] -e0 $e0 
Multipole -name "QF11XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02268216962*$e0] -e0 $e0 
Drift -name "L104A" -length 0.019755 
Bpm -name "MQF11X" -length 0 
Drift -name "L104B" -length 0.403 
Drift -name "FONTK2" -length 0 
Drift -name "L104C" -length 0.3069295 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV8X" -length 0.128141 
Drift -name "L104D" -length 0.0899295 
Bpm -name "FONTP2" -length 0 
Drift -name "L104F" -length 0.12433 
Quadrupole -name "QK2X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Quadrupole -name "QK2X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Drift -name "L104G" -length 0.100755 
Multipole -name "QD12XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.062066818*$e0] -e0 $e0 
Quadrupole -name "QD12X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5116656769*$e0] -e0 $e0 
Multipole -name "QD12XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.062066818*$e0] -e0 $e0 
Multipole -name "QD12XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.062066818*$e0] -e0 $e0 
Quadrupole -name "QD12X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5116656769*$e0] -e0 $e0 
Multipole -name "QD12XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.062066818*$e0] -e0 $e0 
Drift -name "L105A" -length 0.019755 
Bpm -name "MQD12X" -length 0 
Drift -name "L105B" -length 0.5351231519 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH6X" -length 0.11455 
Drift -name "L105C" -length 0.1653525 
Quadrupole -name "QF13X" -synrad $quad_synrad -length 0.0993725 -strength [expr 0.6841194487*$e0] -e0 $e0 
Quadrupole -name "QF13X" -synrad $quad_synrad -length 0.0993725 -strength [expr 0.6841194487*$e0] -e0 $e0 
Drift -name "L106A" -length 0.0546275 
Bpm -name "MQF13X" -length 0 
Drift -name "L106B" -length 0.7821670781 
Quadrupole -name "QD14X" -synrad $quad_synrad -length 0.0993725 -strength [expr -0.507718235*$e0] -e0 $e0 
Drift -name "FIT180" -length 0 
Quadrupole -name "QD14X" -synrad $quad_synrad -length 0.0993725 -strength [expr -0.507718235*$e0] -e0 $e0 
Drift -name "L107A" -length 0.0546275 
Bpm -name "MQD14X" -length 0 
Drift -name "L107B" -length 0.276 
Bpm -name "FONTP3" -length 0 
Drift -name "L107C" -length 0.2282645781 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH7X" -length 0.11455 
Drift -name "L107D" -length 0.1633525 
Quadrupole -name "QF15X" -synrad $quad_synrad -length 0.0993725 -strength [expr 0.6841194487*$e0] -e0 $e0 
Quadrupole -name "QF15X" -synrad $quad_synrad -length 0.0993725 -strength [expr 0.6841194487*$e0] -e0 $e0 
Drift -name "L108A" -length 0.0546275 
Bpm -name "MQF15X" -length 0 
Drift -name "L108B" -length 0.3156 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV9X" -length 0.1248 
Drift -name "L108D" -length 0.1603281519 
Quadrupole -name "QK3X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Quadrupole -name "QK3X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Drift -name "L108E" -length 0.100755 
Multipole -name "QD16XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01534278979*$e0] -e0 $e0 
Quadrupole -name "QD16X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5116656769*$e0] -e0 $e0 
Multipole -name "QD16XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01534278979*$e0] -e0 $e0 
Multipole -name "QD16XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01534278979*$e0] -e0 $e0 
Quadrupole -name "QD16X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5116656769*$e0] -e0 $e0 
Multipole -name "QD16XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01534278979*$e0] -e0 $e0 
Drift -name "L109A" -length 0.019755 
Bpm -name "MQD16X" -length 0 
Drift -name "L109B" -length 0.9546 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH8X" -length 0.1248 
Drift -name "L109C" -length 0.152355 
Multipole -name "QF17XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.002603924659*$e0] -e0 $e0 
Quadrupole -name "QF17X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.5116656769*$e0] -e0 $e0 
Multipole -name "QF17XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.002603924659*$e0] -e0 $e0 
Multipole -name "QF17XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.002603924659*$e0] -e0 $e0 
Quadrupole -name "QF17X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.5116656769*$e0] -e0 $e0 
Multipole -name "QF17XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.002603924659*$e0] -e0 $e0 
Drift -name "L110A" -length 0.019755 
Bpm -name "MQF17X" -length 0 
Drift -name "L110B" -length 0.211 
Drift -name "MREF1X" -length 0 
Drift -name "L110C" -length 0.8401649959 
Quadrupole -name "QK4X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Quadrupole -name "QK4X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Drift -name "L110D" -length 0.6575945 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV10X" -length 0.128141 
Drift -name "L110E" -length 0.2486845 
Multipole -name "QD18XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0001869550965*$e0] -e0 $e0 
Quadrupole -name "QD18X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.3430360678*$e0] -e0 $e0 
Multipole -name "QD18XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0001869550965*$e0] -e0 $e0 
Multipole -name "QD18XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0001869550965*$e0] -e0 $e0 
Quadrupole -name "QD18X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.3430360678*$e0] -e0 $e0 
Multipole -name "QD18XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0001869550965*$e0] -e0 $e0 
Drift -name "L111A" -length 0.019755 
Bpm -name "MQD18X" -length 0 
Drift -name "L111B" -length 0.276 
# UNKNOWN: "MW0X" "WIRE" 38.89174582 3.037818637 -2.906560779 2.860909441 6.993136702 2.591080557 2.146200743 0 0 0 0 0 0 2.668991026e-08 3.470351723e-08 0 0 0 0 0 0 0 0 0 0 0 0 -0 -0 -0 -0 1.3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "WIRE" 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.002126837178 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Drift -name "MW0X" -length 0 
Drift -name "L111C" -length 0.4046498952 
Drift -name "OTR0X" -length 0 
Drift -name "L111D" -length 0.2716 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH9X" -length 0.1248 
Drift -name "L111E" -length 0.149355 
Multipole -name "QF19XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0002494657355*$e0] -e0 $e0 
Quadrupole -name "QF19X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.3275830205*$e0] -e0 $e0 
Multipole -name "QF19XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0002494657355*$e0] -e0 $e0 
Multipole -name "QF19XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0002494657355*$e0] -e0 $e0 
Quadrupole -name "QF19X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.3275830205*$e0] -e0 $e0 
Multipole -name "QF19XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0002494657355*$e0] -e0 $e0 
Drift -name "L112A" -length 0.019755 
Bpm -name "MQF19X" -length 0 
Drift -name "L112B" -length 0.265 
# UNKNOWN: "MW1X" "WIRE" 40.32539572 11.17809797 1.7498406 2.893262829 3.156971659 -0.5839668552 2.20513336 0 0 0 0 0 0 6.092766777e-08 -5.78203431e-09 0 0 0 0 0 0 0 0 0 0 0 0 -0 -0 -0 -0 1.3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "WIRE" 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.002126837149 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Drift -name "MW1X" -length 0 
Drift -name "L112C" -length 0.386 
Drift -name "OTR1X" -length 0 
Drift -name "L112D" -length 0.309 
Drift -name "WCM" -length 0 
Drift -name "L112E" -length 0.4849295 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV11X" -length 0.128141 
Drift -name "L112F" -length 1.4372645 
Drift -name "PPMW" -length 0 
Drift -name "L112G" -length 0.469 
# UNKNOWN: "MW2X" "WIRE" 43.53973072 3.683415493 0.5818026407 2.976768142 11.29990366 -1.949350923 2.295575194 0 0 0 0 0 0 4.234227252e-08 -5.78203431e-09 0 0 0 0 0 0 0 0 0 0 0 0 -0 -0 -0 -0 1.3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "WIRE" 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.002126837147 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Drift -name "MW2X" -length 0 
Drift -name "L112H" -length 0.282 
Bpm -name "MQD20X" -length 0 
Drift -name "L112I" -length 0.019665 
Quadrupole -name "QD20X" -synrad $quad_synrad -length 0.039335 -strength [expr -0.1511348642*$e0] -e0 $e0 
Drift -name "MEMIT" -length 0 
Quadrupole -name "QD20X" -synrad $quad_synrad -length 0.039335 -strength [expr -0.1511348642*$e0] -e0 $e0 
Drift -name "L113A" -length 0.466665 
Drift -name "OTR2X" -length 0 
Drift -name "L113B" -length 1.861065 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH10X" -length 0.11921 
Drift -name "L113C" -length 0.937395 
Drift -name "ICT1X" -length 0 
Drift -name "L113D" -length 0.415665 
Quadrupole -name "QF21X" -synrad $quad_synrad -length 0.039335 -strength [expr 0.2223564783*$e0] -e0 $e0 
Quadrupole -name "QF21X" -synrad $quad_synrad -length 0.039335 -strength [expr 0.2223564783*$e0] -e0 $e0 
Drift -name "L114A" -length 0.019665 
Bpm -name "MQF21X" -length 0 
Drift -name "L114B" -length 0.274 
# UNKNOWN: "MW3X" "WIRE" 48.09240072 11.04571413 3.501010163 3.094777926 4.95419008 -1.510622213 2.403375522 0 0 0 0 0 0 5.797734586e-08 -2.247376471e-08 0 0 0 0 0 0 0 0 0 0 0 0 -0 -0 -0 -0 1.3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "WIRE" 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.002126837136 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Drift -name "MW3X" -length 0 
Drift -name "L114C" -length 0.502 
Drift -name "OTR3X" -length 0 
Drift -name "L114D" -length 0.35 
Drift -name "TILTM" -length 0 
Drift -name "L114E" -length 0.54 
Bpm -name "IPBPM" -length 0 
Drift -name "ATIPBPM" -length 0 
Drift -name "L114F" -length 1.813335 
# UNKNOWN: "MW4X" "WIRE" 51.29773572 0.9329607361 -0.3460346615 3.353517828 21.44457427 -3.634045719 2.453703643 0 0 0 0 0 0 -1.405859873e-08 -2.247376471e-08 0 0 0 0 0 0 0 0 0 0 0 0 -0 -0 -0 -0 1.3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "WIRE" 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.002126837128 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Drift -name "MW4X" -length 0 
Drift -name "L114G" -length 0.282 
Drift -name "BEGFF" -length 0 
Bpm -name "MQM16FF" -length 0 
Drift -name "L200" -length 0.019755 
# WARNING: Multipole options not defined
Drift -name "QM16FFMULT" -length 0 
Quadrupole -name "QM16FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0*$e0] -e0 $e0 
# WARNING: Multipole options not defined
Drift -name "QM16FFMULT" -length 0 
Drift -name "AQM16FF" -length 0 
# WARNING: Multipole options not defined
Drift -name "QM16FFMULT" -length 0 
Quadrupole -name "QM16FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0*$e0] -e0 $e0 
# WARNING: Multipole options not defined
Drift -name "QM16FFMULT" -length 0 
Drift -name "L201A" -length 0.76315 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH1FF" -length 0.11921 
Drift -name "L201B" -length 0.1353245 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV1FF" -length 0.128141 
Drift -name "L201C" -length 0.1556845 
Multipole -name "QM15FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0001505025711*$e0] -e0 $e0 
Quadrupole -name "QM15FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.3199356679*$e0] -e0 $e0 
Multipole -name "QM15FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0001505025711*$e0] -e0 $e0 
Drift -name "AQM15FF" -length 0 
Multipole -name "QM15FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0001505025711*$e0] -e0 $e0 
Quadrupole -name "QM15FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.3199356679*$e0] -e0 $e0 
Multipole -name "QM15FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0001505025711*$e0] -e0 $e0 
Drift -name "L202A" -length 0.019755 
Bpm -name "MQM15FF" -length 0 
Drift -name "L202B" -length 1.281755 
Multipole -name "QM14FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02622302779*$e0] -e0 $e0 
Quadrupole -name "QM14FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.758062188*$e0] -e0 $e0 
Multipole -name "QM14FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02622302779*$e0] -e0 $e0 
Drift -name "AQM14FF" -length 0 
Multipole -name "QM14FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02622302779*$e0] -e0 $e0 
Quadrupole -name "QM14FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.758062188*$e0] -e0 $e0 
Multipole -name "QM14FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02622302779*$e0] -e0 $e0 
Drift -name "L203A" -length 0.019755 
Bpm -name "MQM14FF" -length 0 
Drift -name "L203B" -length 0.557455 
Drift -name "LWML1" -length 0 
Drift -name "L203C" -length 0.0215 
# UNKNOWN: "LW1FF" "WIRE" 55.39669072 12.24133741 -8.653180062 3.56324202 10.50486257 46.7893425 2.469846489 0 0 0 0 0 0 -6.921954167e-08 -4.609546268e-08 0 0 0 0 0 0 0 0 0 0 0 0 -0 -0 -0 -0 1.3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "WIRE" 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.002126837104 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Drift -name "LW1FF" -length 0 
Drift -name "L203D" -length 0.0215 
Drift -name "LWML2" -length 0 
Drift -name "L203E" -length 0.2035 
Bpm -name "MFB2FF" -length 0 
Drift -name "AFB2FF" -length 0 
Drift -name "L203F" -length 0.4778 
Multipole -name "QM13FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0002276859474*$e0] -e0 $e0 
Quadrupole -name "QM13FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.4629159803*$e0] -e0 $e0 
Multipole -name "QM13FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0002276859474*$e0] -e0 $e0 
Drift -name "AQM13FF" -length 0 
Multipole -name "QM13FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0002276859474*$e0] -e0 $e0 
Quadrupole -name "QM13FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.4629159803*$e0] -e0 $e0 
Multipole -name "QM13FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0002276859474*$e0] -e0 $e0 
Drift -name "L204A" -length 0.019755 
Bpm -name "MQM13FF" -length 0 
Drift -name "L204B" -length 1.281755 
Multipole -name "QM12FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.009401453585*$e0] -e0 $e0 
Quadrupole -name "QM12FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1164254228*$e0] -e0 $e0 
Multipole -name "QM12FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.009401453585*$e0] -e0 $e0 
Drift -name "AQM12FF" -length 0 
Multipole -name "QM12FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.009401453585*$e0] -e0 $e0 
Quadrupole -name "QM12FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1164254228*$e0] -e0 $e0 
Multipole -name "QM12FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.009401453585*$e0] -e0 $e0 
Drift -name "L205A" -length 0.019755 
Bpm -name "MQM12FF" -length 0 
Drift -name "L205B" -length 0.715755 
Bpm -name "MFB1FF" -length 0 
Drift -name "L205C" -length 0.666 
Multipole -name "QM11FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0003312393654*$e0] -e0 $e0 
Quadrupole -name "QM11FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.08000716598*$e0] -e0 $e0 
Multipole -name "QM11FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0003312393654*$e0] -e0 $e0 
Drift -name "AQM11FF" -length 0 
Multipole -name "QM11FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0003312393654*$e0] -e0 $e0 
Quadrupole -name "QM11FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.08000716598*$e0] -e0 $e0 
Multipole -name "QM11FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0003312393654*$e0] -e0 $e0 
Drift -name "L206A" -length 0.019755 
Bpm -name "MQM11FF" -length 0 
Drift -name "L206B" -length 1.281755 
Multipole -name "QD10BFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002695636294*$e0] -e0 $e0 
Quadrupole -name "QD10BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1450096657*$e0] -e0 $e0 
Multipole -name "QD10BFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002695636294*$e0] -e0 $e0 
Drift -name "AQD10BFF" -length 0 
Multipole -name "QD10BFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002695636294*$e0] -e0 $e0 
Quadrupole -name "QD10BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1450096657*$e0] -e0 $e0 
Multipole -name "QD10BFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002695636294*$e0] -e0 $e0 
Drift -name "L207A" -length 0.019755 
Bpm -name "MQD10BFF" -length 0 
Drift -name "L207B" -length 0.477 
Drift -name "MREF3FF" -length 0 
Drift -name "L207C" -length 0.204755 
Multipole -name "QD10AFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.004934963228*$e0] -e0 $e0 
Quadrupole -name "QD10AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1450096657*$e0] -e0 $e0 
Multipole -name "QD10AFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.004934963228*$e0] -e0 $e0 
Drift -name "AQD10AFF" -length 0 
Multipole -name "QD10AFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.004934963228*$e0] -e0 $e0 
Quadrupole -name "QD10AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1450096657*$e0] -e0 $e0 
Multipole -name "QD10AFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.004934963228*$e0] -e0 $e0 
Drift -name "L208A" -length 0.019755 
Bpm -name "MQD10AFF" -length 0 
Drift -name "L208B" -length 0.289 
Drift -name "MS1FF" -length 0 
Drift -name "L208C" -length 0.792755 
Multipole -name "QF9BFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02953233026*$e0] -e0 $e0 
Quadrupole -name "QF9BFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1893249839*$e0] -e0 $e0 
Multipole -name "QF9BFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02953233026*$e0] -e0 $e0 
Drift -name "AQF9BFF" -length 0 
Multipole -name "QF9BFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02953233026*$e0] -e0 $e0 
Quadrupole -name "QF9BFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1893249839*$e0] -e0 $e0 
Multipole -name "QF9BFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02953233026*$e0] -e0 $e0 
Drift -name "L209A" -length 0.019755 
Bpm -name "MQF9BFF" -length 0 
Drift -name "L209B" -length 0.390960984 
Multipole -name "SF6FFMULT" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6180456752*$e0] -e0 $e0 
Multipole -name "SF6FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr 4.8828277*$e0] -e0 $e0 
Multipole -name "SF6FFMULT" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6180456752*$e0] -e0 $e0 
Drift -name "ASF6FF" -length 0 
Multipole -name "SF6FFMULT" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6180456752*$e0] -e0 $e0 
Multipole -name "SF6FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr 4.8828277*$e0] -e0 $e0 
Multipole -name "SF6FFMULT" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6180456752*$e0] -e0 $e0 
Drift -name "L210A" -length 0.099791984 
Bpm -name "MSF6FF" -length 0 
Drift -name "L210B" -length 0.310924 
Multipole -name "QF9AFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0158420658*$e0] -e0 $e0 
Quadrupole -name "QF9AFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1893249839*$e0] -e0 $e0 
Multipole -name "QF9AFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0158420658*$e0] -e0 $e0 
Drift -name "AQF9AFF" -length 0 
Multipole -name "QF9AFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0158420658*$e0] -e0 $e0 
Quadrupole -name "QF9AFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1893249839*$e0] -e0 $e0 
Multipole -name "QF9AFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0158420658*$e0] -e0 $e0 
Drift -name "L211A" -length 0.019755 
Bpm -name "MQF9AFF" -length 0 
Drift -name "L211B" -length 0.266 
Multipole -name "SK4FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Multipole -name "SK4FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Drift -name "L211C" -length 1.085755 
Multipole -name "QD8FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0070969355*$e0] -e0 $e0 
Quadrupole -name "QD8FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.3021776993*$e0] -e0 $e0 
Multipole -name "QD8FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0070969355*$e0] -e0 $e0 
Drift -name "AQD8FF" -length 0 
Multipole -name "QD8FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0070969355*$e0] -e0 $e0 
Quadrupole -name "QD8FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.3021776993*$e0] -e0 $e0 
Multipole -name "QD8FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0070969355*$e0] -e0 $e0 
Drift -name "L212A" -length 0.019755 
Bpm -name "MQD8FF" -length 0 
Drift -name "L212B" -length 1.681755 
Multipole -name "QF7FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.04450369748*$e0] -e0 $e0 
Quadrupole -name "QF7FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.2750809185*$e0] -e0 $e0 
Multipole -name "QF7FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.04450369748*$e0] -e0 $e0 
Drift -name "AQF7FF" -length 0 
Multipole -name "QF7FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.04450369748*$e0] -e0 $e0 
Quadrupole -name "QF7FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.2750809185*$e0] -e0 $e0 
Multipole -name "QF7FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.04450369748*$e0] -e0 $e0 
Drift -name "L213A" -length 0.019755 
Bpm -name "MQF7FF" -length 0 
Drift -name "L213B" -length 0.4246097102 
Quadrupole -name "B5FFMULT" -synrad $quad_synrad -length 0 -strength [expr -1.995837329e-05*$e0] -e0 $e0 
Sbend -name "B5FFA" -synrad $sbend_synrad -length 0.3064317863 -angle -0.02494796661 -E1 -0.02494796661 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.01905 -fint 0.5 
set e0 [expr $e0-14.1e-6*-0.02494796661*-0.02494796661/0.3064317863*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Quadrupole -name "B5FFMULT" -synrad $quad_synrad -length 0 -strength [expr -1.995837329e-05*$e0] -e0 $e0 
Quadrupole -name "B5FFMULT" -synrad $quad_synrad -length 0 -strength [expr -1.995837329e-05*$e0] -e0 $e0 
Sbend -name "B5FFB" -synrad $sbend_synrad -length 0.3064317863 -angle -0.02494796661 -E1 0 -E2 -0.02494796661 -six_dim 1 -e0 $e0 -hgap 0.01905 -fintx 0.5 
set e0 [expr $e0-14.1e-6*-0.02494796661*-0.02494796661/0.3064317863*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Quadrupole -name "B5FFMULT" -synrad $quad_synrad -length 0 -strength [expr -1.995837329e-05*$e0] -e0 $e0 
Drift -name "L214" -length 0.6443647102 
Multipole -name "QD6FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.04035337279*$e0] -e0 $e0 
Quadrupole -name "QD6FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.301163606*$e0] -e0 $e0 
Multipole -name "QD6FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.04035337279*$e0] -e0 $e0 
Drift -name "AQD6FF" -length 0 
Multipole -name "QD6FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.04035337279*$e0] -e0 $e0 
Quadrupole -name "QD6FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.301163606*$e0] -e0 $e0 
Multipole -name "QD6FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.04035337279*$e0] -e0 $e0 
Drift -name "L215A" -length 0.019755 
Bpm -name "MQD6FF" -length 0 
Drift -name "L215B" -length 1.066 
Multipole -name "SK3FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Multipole -name "SK3FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Drift -name "L215C" -length 0.285755 
Multipole -name "QF5BFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01473232534*$e0] -e0 $e0 
Quadrupole -name "QF5BFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1880283172*$e0] -e0 $e0 
Multipole -name "QF5BFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01473232534*$e0] -e0 $e0 
Drift -name "AQF5BFF" -length 0 
Multipole -name "QF5BFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01473232534*$e0] -e0 $e0 
Quadrupole -name "QF5BFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1880283172*$e0] -e0 $e0 
Multipole -name "QF5BFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01473232534*$e0] -e0 $e0 
Drift -name "L216A" -length 0.019755 
Bpm -name "MQF5BFF" -length 0 
Drift -name "L216B" -length 0.390960984 
Multipole -name "SF5FFMULT" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.09259852829*$e0] -e0 $e0 
Multipole -name "SF5FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr -0.3954341426*$e0] -e0 $e0 
Multipole -name "SF5FFMULT" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.09259852829*$e0] -e0 $e0 
Drift -name "ASF5FF" -length 0 
Multipole -name "SF5FFMULT" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.09259852829*$e0] -e0 $e0 
Multipole -name "SF5FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr -0.3954341426*$e0] -e0 $e0 
Multipole -name "SF5FFMULT" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.09259852829*$e0] -e0 $e0 
Drift -name "L217A" -length 0.099791984 
Bpm -name "MSF5FF" -length 0 
Drift -name "L217B" -length 0.310924 
Multipole -name "QF5AFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.004121559746*$e0] -e0 $e0 
Quadrupole -name "QF5AFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1880283172*$e0] -e0 $e0 
Multipole -name "QF5AFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.004121559746*$e0] -e0 $e0 
Drift -name "AQF5AFF" -length 0 
Multipole -name "QF5AFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.004121559746*$e0] -e0 $e0 
Quadrupole -name "QF5AFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1880283172*$e0] -e0 $e0 
Multipole -name "QF5AFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.004121559746*$e0] -e0 $e0 
Drift -name "L218A" -length 0.019755 
Bpm -name "MQF5AFF" -length 0 
Drift -name "L218B" -length 0.223 
Drift -name "MREF2FF" -length 0 
Drift -name "L218C" -length 0.858755 
Multipole -name "QD4BFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03495408132*$e0] -e0 $e0 
Quadrupole -name "QD4BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1483961368*$e0] -e0 $e0 
Multipole -name "QD4BFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03495408132*$e0] -e0 $e0 
Drift -name "AQD4BFF" -length 0 
Multipole -name "QD4BFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03495408132*$e0] -e0 $e0 
Quadrupole -name "QD4BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1483961368*$e0] -e0 $e0 
Multipole -name "QD4BFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03495408132*$e0] -e0 $e0 
Drift -name "L219A" -length 0.019755 
Bpm -name "MQD4BFF" -length 0 
Drift -name "L219B" -length 0.390960984 
Multipole -name "SD4FFMULT" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.9169933774*$e0] -e0 $e0 
Multipole -name "SD4FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr 7.817968185*$e0] -e0 $e0 
Multipole -name "SD4FFMULT" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.9169933774*$e0] -e0 $e0 
Drift -name "ASD4FF" -length 0 
Multipole -name "SD4FFMULT" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.9169933774*$e0] -e0 $e0 
Multipole -name "SD4FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr 7.817968185*$e0] -e0 $e0 
Multipole -name "SD4FFMULT" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.9169933774*$e0] -e0 $e0 
Drift -name "L220A" -length 0.099791984 
Bpm -name "MSD4FF" -length 0 
Drift -name "L220B" -length 0.310924 
Multipole -name "QD4AFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02222682087*$e0] -e0 $e0 
Quadrupole -name "QD4AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1483961368*$e0] -e0 $e0 
Multipole -name "QD4AFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02222682087*$e0] -e0 $e0 
Drift -name "AQD4AFF" -length 0 
Multipole -name "QD4AFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02222682087*$e0] -e0 $e0 
Quadrupole -name "QD4AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1483961368*$e0] -e0 $e0 
Multipole -name "QD4AFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02222682087*$e0] -e0 $e0 
Drift -name "L221A" -length 0.019755 
Bpm -name "MQD4AFF" -length 0 
Drift -name "L221B" -length 0.266 
Multipole -name "SK2FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Multipole -name "SK2FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Drift -name "L221C" -length 1.028629871 
Quadrupole -name "B2FFMULT" -synrad $quad_synrad -length 0 -strength [expr -1.508793732e-05*$e0] -e0 $e0 
Sbend -name "B2FFA" -synrad $sbend_synrad -length 0.3064116254 -angle -0.01508793732 -E1 -0.01508793732 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.01905 -fint 0.5 
set e0 [expr $e0-14.1e-6*-0.01508793732*-0.01508793732/0.3064116254*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Quadrupole -name "B2FFMULT" -synrad $quad_synrad -length 0 -strength [expr -1.508793732e-05*$e0] -e0 $e0 
Quadrupole -name "B2FFMULT" -synrad $quad_synrad -length 0 -strength [expr -1.508793732e-05*$e0] -e0 $e0 
Sbend -name "B2FFB" -synrad $sbend_synrad -length 0.3064116254 -angle -0.01508793732 -E1 0 -E2 -0.01508793732 -six_dim 1 -e0 $e0 -hgap 0.01905 -fintx 0.5 
set e0 [expr $e0-14.1e-6*-0.01508793732*-0.01508793732/0.3064116254*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Quadrupole -name "B2FFMULT" -synrad $quad_synrad -length 0 -strength [expr -1.508793732e-05*$e0] -e0 $e0 
Drift -name "L222" -length 0.444384871 
Multipole -name "QF3FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01729614954*$e0] -e0 $e0 
Quadrupole -name "QF3FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.2763599368*$e0] -e0 $e0 
Multipole -name "QF3FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01729614954*$e0] -e0 $e0 
Drift -name "AQF3FF" -length 0 
Multipole -name "QF3FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01729614954*$e0] -e0 $e0 
Quadrupole -name "QF3FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.2763599368*$e0] -e0 $e0 
Multipole -name "QF3FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01729614954*$e0] -e0 $e0 
Drift -name "L223A" -length 0.019755 
Bpm -name "MQF3FF" -length 0 
Drift -name "L223B" -length 0.4246045352 
Quadrupole -name "B1FFMULT" -synrad $quad_synrad -length 0 -strength [expr -3.093740393e-05*$e0] -e0 $e0 
Sbend -name "B1FFA" -synrad $sbend_synrad -length 0.3064369612 -angle -0.02690209037 -E1 -0.02690209037 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.01905 -fint 0.5 
set e0 [expr $e0-14.1e-6*-0.02690209037*-0.02690209037/0.3064369612*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Quadrupole -name "B1FFMULT" -synrad $quad_synrad -length 0 -strength [expr -3.093740393e-05*$e0] -e0 $e0 
Quadrupole -name "B1FFMULT" -synrad $quad_synrad -length 0 -strength [expr -3.093740393e-05*$e0] -e0 $e0 
Sbend -name "B1FFB" -synrad $sbend_synrad -length 0.3064369612 -angle -0.02690209037 -E1 0 -E2 -0.02690209037 -six_dim 1 -e0 $e0 -hgap 0.01905 -fintx 0.5 
set e0 [expr $e0-14.1e-6*-0.02690209037*-0.02690209037/0.3064369612*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Quadrupole -name "B1FFMULT" -synrad $quad_synrad -length 0 -strength [expr -3.093740393e-05*$e0] -e0 $e0 
Drift -name "L224A" -length 0.2406045352 
Drift -name "MREF1FF" -length 0 
Drift -name "L224B" -length 0.203755 
Multipole -name "QD2BFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0149651453*$e0] -e0 $e0 
Quadrupole -name "QD2BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.09935648446*$e0] -e0 $e0 
Multipole -name "QD2BFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0149651453*$e0] -e0 $e0 
Drift -name "AQD2BFF" -length 0 
Multipole -name "QD2BFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0149651453*$e0] -e0 $e0 
Quadrupole -name "QD2BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.09935648446*$e0] -e0 $e0 
Multipole -name "QD2BFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0149651453*$e0] -e0 $e0 
Drift -name "L225A" -length 0.019755 
Bpm -name "MQD2BFF" -length 0 
Drift -name "L225B" -length 0.354 
Drift -name "MT1X" -length 0 
Drift -name "L225C" -length 0.137 
Drift -name "FFML1" -length 0 
Drift -name "L225D" -length 0.7235 
Drift -name "MREFIPFF" -length 0 
Drift -name "L225E" -length 0.267255 
Multipole -name "QD2AFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.002168909994*$e0] -e0 $e0 
Quadrupole -name "QD2AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1448640481*$e0] -e0 $e0 
Multipole -name "QD2AFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.002168909994*$e0] -e0 $e0 
Drift -name "AQD2AFF" -length 0 
Multipole -name "QD2AFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.002168909994*$e0] -e0 $e0 
Quadrupole -name "QD2AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1448640481*$e0] -e0 $e0 
Multipole -name "QD2AFFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.002168909994*$e0] -e0 $e0 
Drift -name "L226A" -length 0.019755 
Bpm -name "MQD2AFF" -length 0 
Drift -name "L226B" -length 0.391 
Drift -name "MREFSFF" -length 0 
Drift -name "L226C" -length 3.825 
Multipole -name "SK1FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Multipole -name "SK1FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Drift -name "L226D" -length 0.161 
Bpm -name "MSF1FF" -length 0 
Drift -name "L226E" -length 0.099 
Multipole -name "SF1FFMULT" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.04061381895*$e0] -e0 $e0 
Multipole -name "SF1FF" -synrad $mult_synrad -type 3 -length 0.05 -strength [expr -1.505168272*$e0] -e0 $e0 
Multipole -name "SF1FFMULT" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.04061381895*$e0] -e0 $e0 
Drift -name "ASF1FF" -length 0 
Multipole -name "SF1FFMULT" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.04061381895*$e0] -e0 $e0 
Multipole -name "SF1FF" -synrad $mult_synrad -type 3 -length 0.05 -strength [expr -1.505168272*$e0] -e0 $e0 
Multipole -name "SF1FFMULT" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.04061381895*$e0] -e0 $e0 
Drift -name "L227" -length 0.30295 
Multipole -name "QF1FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0006350714972*$e0] -e0 $e0 
Quadrupole -name "QF1FF" -synrad $quad_synrad -length 0.22205 -strength [expr 0.369493027*$e0] -e0 $e0 
Multipole -name "QF1FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0006350714972*$e0] -e0 $e0 
Drift -name "AQF1FF" -length 0 
Multipole -name "QF1FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0006350714972*$e0] -e0 $e0 
Quadrupole -name "QF1FF" -synrad $quad_synrad -length 0.22205 -strength [expr 0.369493027*$e0] -e0 $e0 
Multipole -name "QF1FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0006350714972*$e0] -e0 $e0 
Drift -name "L228A" -length 0.12195 
Bpm -name "MQF1FF" -length 0 
Drift -name "L228B" -length 0.297 
Bpm -name "MSD0FF" -length 0 
Drift -name "L228C" -length 0.099 
Multipole -name "SD0FFMULT" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.3056329435*$e0] -e0 $e0 
Multipole -name "SD0FF" -synrad $mult_synrad -type 3 -length 0.05 -strength [expr 2.250119161*$e0] -e0 $e0 
Multipole -name "SD0FFMULT" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.3056329435*$e0] -e0 $e0 
Drift -name "ASD0FF" -length 0 
Multipole -name "SD0FFMULT" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.3056329435*$e0] -e0 $e0 
Multipole -name "SD0FF" -synrad $mult_synrad -type 3 -length 0.05 -strength [expr 2.250119161*$e0] -e0 $e0 
Multipole -name "SD0FFMULT" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.3056329435*$e0] -e0 $e0 
Drift -name "L229" -length 0.2875 
Multipole -name "QD0FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01255704502*$e0] -e0 $e0 
Quadrupole -name "QD0FF" -synrad $quad_synrad -length 0.2375 -strength [expr -0.6811853675*$e0] -e0 $e0 
Multipole -name "QD0FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01255704502*$e0] -e0 $e0 
Drift -name "AQD0FF" -length 0 
Multipole -name "QD0FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01255704502*$e0] -e0 $e0 
Quadrupole -name "QD0FF" -synrad $quad_synrad -length 0.2375 -strength [expr -0.6811853675*$e0] -e0 $e0 
Multipole -name "QD0FFMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01255704502*$e0] -e0 $e0 
Drift -name "L230A" -length 0.1065 
Bpm -name "MQD0FF" -length 0 
Drift -name "L230B" -length 0.286 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "IPKICK" -length 0.12 
Drift -name "L230C" -length 0.048 
Bpm -name "MPREIP" -length 0 
Drift -name "L230D" -length 0.2758 
Bpm -name "IPBPMA" -length 0 
Drift -name "L230E" -length 0.081 
Bpm -name "IPBPMB" -length 0 
Drift -name "L230F" -length 0.0702 
Drift -name "MS1IP" -length 0 
Drift -name "MBS1IP" -length 0 
Bpm -name "IP" -length 0 
Drift -name "L231" -length 0.0882 
Bpm -name "IPBPMC" -length 0 
Drift -name "ENDFF" -length 0 
Girder
