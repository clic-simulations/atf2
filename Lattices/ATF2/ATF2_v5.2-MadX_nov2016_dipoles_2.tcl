# version 5.2 converted from MadX

# lattice options that can be set externally

if {! [info exist stripline]} {
    set stripline 3.0
}
if {! [info exist stripline2]} {
    set stripline2 3.0
}
if {! [info exist font_stripline_resolution]} {
    set font_stripline_resolution 0.5
}
if {! [info exist cband]} {
    set cband 0.2
}
if {! [info exist cband_noatt]} {
    set cband_noatt 0.2
}
if {! [info exist sband]} {
    set sband 1.0
}
if {! [info exist ipbpm]} {
    set ipbpm 0.1
}

# wakefields:
if {! [info exist cband_wake]} {
    set cband_wake ""
}
if {! [info exist sband_wake]} {
    set sband_wake ""
}

Dipole -name "DIPOLE_0" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "ATF2MSTART" -length 0 
Dipole -name "DIPOLE_1" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Bpm -name "START" -length 0 -resolution $stripline
Dipole -name "DIPOLE_2" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "KEX1MULT1" -synrad $quad_synrad -length 0 -strength [expr -0.01846929*$e0] -e0 $e0 
Dipole -name "DIPOLE_3" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "KEX1MULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -3.824591*$e0] -e0 $e0
Dipole -name "DIPOLE_4" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "KEX1A" -synrad $sbend_synrad -length 0.2500010417 -angle 0.0025 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.00635 -fint 0.5 
Dipole -name "DIPOLE_5" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Bpm -name "RAUL" -length 0 -resolution $stripline 
Dipole -name "DIPOLE_6" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*0.0025*0.0025/0.2500010417*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_7" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_8" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "KEX1MULT1" -synrad $quad_synrad -length 0 -strength [expr -0.01846929*$e0] -e0 $e0 
Dipole -name "DIPOLE_9" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "KEX1MULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -3.824591*$e0] -e0 $e0 
Dipole -name "DIPOLE_10" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "IP01" -length 0 
Dipole -name "DIPOLE_11" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "KEX1MULT1" -synrad $quad_synrad -length 0 -strength [expr -0.01846929*$e0] -e0 $e0 
Dipole -name "DIPOLE_12" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "KEX1MULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -3.824591*$e0] -e0 $e0 
Dipole -name "DIPOLE_13" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "KEX1B" -synrad $sbend_synrad -length 0.2500010417 -angle 0.0025 -E1 0 -E2 0.005 -six_dim 1 -e0 $e0 -hgap 0.00635 -fintx 0.5 
Dipole -name "DIPOLE_14" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*0.0025*0.0025/0.2500010417*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_15" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_16" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "KEX1MULT1" -synrad $quad_synrad -length 0 -strength [expr -0.01846929*$e0] -e0 $e0 
Dipole -name "DIPOLE_17" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "KEX1MULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -3.824591*$e0] -e0 $e0 
Dipole -name "DIPOLE_18" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L001" -length 0.9231390392 
Dipole -name "DIPOLE_19" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "QM6RX" -synrad $sbend_synrad -length 0.09937458845 -angle 0.0023109657 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -K [expr -0.3644313055*$e0] 
Dipole -name "DIPOLE_20" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*0.0023109657*0.0023109657/0.09937458845*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_21" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_22" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "QM6RX" -synrad $sbend_synrad -length 0.09937458845 -angle 0.0023109657 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -K [expr -0.3644313055*$e0] 
Dipole -name "DIPOLE_23" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*0.0023109657*0.0023109657/0.09937458845*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_24" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_25" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L002A" -length 0.04685466892 
Dipole -name "DIPOLE_26" -length 0.0 -strength_x 0.0 -strength_y 0.0 
HCORRECTOR -name "ZH100RX" -length 0.24955 
Dipole -name "DIPOLE_27" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L002B" -length 0.7028333967 
Dipole -name "DIPOLE_28" -length 0.0 -strength_x 0.0 -strength_y 0.0 
HCORRECTOR -name "ZH101RX" -length 0.1679 
Dipole -name "DIPOLE_29" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L002C" -length 0.05068234611 
Dipole -name "DIPOLE_30" -length 0.0 -strength_x 0.0 -strength_y 0.0 
VCORRECTOR -name "ZV100RX" -length 0.13874 
Dipole -name "DIPOLE_31" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L002D" -length 0 
Dipole -name "DIPOLE_32" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#BPM MB1X not instrumented but present (button BPM)
Dipole -name "DIPOLE_33" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MB1X" -length 0 -resolution $stripline
Dipole -name "DIPOLE_34" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L002E" -length 0.1894692706 
Dipole -name "DIPOLE_35" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "QM7RX" -synrad $sbend_synrad -length 0.0421716479 -angle -0.0045873988 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -K [expr 0.20411*$e0] 
Dipole -name "DIPOLE_36" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*-0.0045873988*-0.0045873988/0.0421716479*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_37" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_38" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "QM7RX" -synrad $sbend_synrad -length 0.0421716479 -angle -0.0045873988 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -K [expr 0.20411*$e0] 
Dipole -name "DIPOLE_39" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*-0.0045873988*-0.0045873988/0.0421716479*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_40" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_41" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L003" -length 0.1478305058 
Dipole -name "DIPOLE_42" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "BS1XA" -synrad $sbend_synrad -length 0.3 -angle 0.01401783271 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
Dipole -name "DIPOLE_43" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*0.01401783271*0.01401783271/0.3*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_44" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_45" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "BS1XB" -synrad $sbend_synrad -length 0.3 -angle 0.01401783271 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
Dipole -name "DIPOLE_46" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*0.01401783271*0.01401783271/0.3*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_47" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_48" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L004" -length 0.2 
Dipole -name "DIPOLE_49" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "BS2XA" -synrad $sbend_synrad -length 0.4 -angle 0.03717168307 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
Dipole -name "DIPOLE_50" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*0.03717168307*0.03717168307/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_51" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_52" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "BS2XB" -synrad $sbend_synrad -length 0.4 -angle 0.03717168307 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
Dipole -name "DIPOLE_53" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*0.03717168307*0.03717168307/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_54" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_55" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L005A" -length 0.1 
Dipole -name "DIPOLE_56" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#BPM MB2X present (button BPM) but callibration very dificult in Y
Dipole -name "DIPOLE_57" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MB2X" -length 0 -resolution $stripline
Dipole -name "DIPOLE_58" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L005B" -length 0.1 
Dipole -name "DIPOLE_59" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "BS3XA" -synrad $sbend_synrad -length 0.5 -angle 0.1175110129 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
Dipole -name "DIPOLE_60" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*0.1175110129*0.1175110129/0.5*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_61" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_62" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "BS3XB" -synrad $sbend_synrad -length 0.5 -angle 0.1175110129 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
Dipole -name "DIPOLE_63" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*0.1175110129*0.1175110129/0.5*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_64" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_65" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L006A" -length 0.4782345 
Dipole -name "DIPOLE_66" -length 0.0 -strength_x 0.0 -strength_y 0.0 
VCORRECTOR -name "ZV1X" -length 0.128141 
Dipole -name "DIPOLE_67" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L006B" -length 0.0335945 
Dipole -name "DIPOLE_68" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QS1X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Dipole -name "DIPOLE_69" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QS1X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Dipole -name "DIPOLE_70" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L006C" -length 0.08136 
Dipole -name "DIPOLE_71" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF1X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.5379118485*$e0] -e0 $e0 
Dipole -name "DIPOLE_72" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF1X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.5379118485*$e0] -e0 $e0 
Dipole -name "DIPOLE_73" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L007A" -length 0.014695 
Dipole -name "DIPOLE_74" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQF1X" -length 0 -resolution $stripline2
Dipole -name "DIPOLE_75" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L007B" -length 0.214 
Dipole -name "DIPOLE_76" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "MS1X" -length 0 
Dipole -name "DIPOLE_77" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L007C" -length 0.271305 
Dipole -name "DIPOLE_78" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "BH1XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.1839837463*$e0] -e0 $e0 
Dipole -name "DIPOLE_79" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "BH1XA" -synrad $sbend_synrad -length 0.4 -angle 0.0941996781 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
Dipole -name "DIPOLE_80" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*0.0941996781*0.0941996781/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_81" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_82" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "BH1XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.1839837463*$e0] -e0 $e0 
Dipole -name "DIPOLE_83" -length 0.0 -strength_x 0.0 -strength_y 0.0 
HCORRECTOR -name "ZX1X" -length 0 
Dipole -name "DIPOLE_84" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "BH1XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.1839837463*$e0] -e0 $e0 
Dipole -name "DIPOLE_85" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "BH1XB" -synrad $sbend_synrad -length 0.4 -angle 0.0941996781 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
Dipole -name "DIPOLE_86" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*0.0941996781*0.0941996781/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_87" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_88" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "BH1XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.1839837463*$e0] -e0 $e0 
Dipole -name "DIPOLE_89" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L008A" -length 0.2132345 
Dipole -name "DIPOLE_90" -length 0.0 -strength_x 0.0 -strength_y 0.0 
VCORRECTOR -name "ZV2X" -length 0.128141 
Dipole -name "DIPOLE_91" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L008B" -length 0.1586245 
Dipole -name "DIPOLE_92" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD2X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.4708418816*$e0] -e0 $e0 
Dipole -name "DIPOLE_93" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD2X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.4708418816*$e0] -e0 $e0 
Dipole -name "DIPOLE_94" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L009A" -length 0.014695 
Dipole -name "DIPOLE_95" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQD2X" -length 0 -resolution $stripline2
Dipole -name "DIPOLE_96" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L009B" -length 0.7578050808 
Dipole -name "DIPOLE_97" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF3X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3353340117*$e0] -e0 $e0 
Dipole -name "DIPOLE_98" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF3X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3353340117*$e0] -e0 $e0 
Dipole -name "DIPOLE_99" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L010A" -length 0.026695 
Dipole -name "DIPOLE_100" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQF3X" -length 0 -resolution $stripline2
Dipole -name "DIPOLE_101" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L010B" -length 0.138725 
Dipole -name "DIPOLE_102" -length 0.0 -strength_x 0.0 -strength_y 0.0 
HCORRECTOR -name "ZH1X" -length 0.11455 
Dipole -name "DIPOLE_103" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L010C" -length 0.1396545 
Dipole -name "DIPOLE_104" -length 0.0 -strength_x 0.0 -strength_y 0.0 
VCORRECTOR -name "ZV3X" -length 0.128141 
Dipole -name "DIPOLE_105" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L010D" -length 1.505663876 
Dipole -name "DIPOLE_106" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "MID" -length 0 
Dipole -name "DIPOLE_107" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L010E" -length 2.053429376 
Dipole -name "DIPOLE_108" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF4X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3393545966*$e0] -e0 $e0 
Dipole -name "DIPOLE_109" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF4X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3393545966*$e0] -e0 $e0 
Dipole -name "DIPOLE_110" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L011A" -length 0.026695 
Dipole -name "DIPOLE_111" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQF4X" -length 0 -resolution $stripline2
Dipole -name "DIPOLE_112" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L011B" -length 0.134725 
Dipole -name "DIPOLE_113" -length 0.0 -strength_x 0.0 -strength_y 0.0 
HCORRECTOR -name "ZH2X" -length 0.11455 
Dipole -name "DIPOLE_114" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L011C" -length 0.4698350808 
Dipole -name "DIPOLE_115" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQD5X" -length 0 -resolution $stripline2
Dipole -name "DIPOLE_116" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L011D" -length 0.026695 
Dipole -name "DIPOLE_117" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD5X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.4607728075*$e0] -e0 $e0 
Dipole -name "DIPOLE_118" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD5X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.4607728075*$e0] -e0 $e0 
Dipole -name "DIPOLE_119" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L012A" -length 0.1576245 
Dipole -name "DIPOLE_120" -length 0.0 -strength_x 0.0 -strength_y 0.0 
VCORRECTOR -name "ZV4X" -length 0.128141 
Dipole -name "DIPOLE_121" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L012B" -length 0.2142345 
Dipole -name "DIPOLE_122" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "BH2XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.1839837463*$e0] -e0 $e0 
Dipole -name "DIPOLE_123" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "BH2XA" -synrad $sbend_synrad -length 0.4 -angle -0.0941996781 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
Dipole -name "DIPOLE_124" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*-0.0941996781*-0.0941996781/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_125" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_126" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "BH2XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.1839837463*$e0] -e0 $e0 
Dipole -name "DIPOLE_127" -length 0.0 -strength_x 0.0 -strength_y 0.0 
HCORRECTOR -name "ZX2X" -length 0 
Dipole -name "DIPOLE_128" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "BH2XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.1839837463*$e0] -e0 $e0 
Dipole -name "DIPOLE_129" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "BH2XB" -synrad $sbend_synrad -length 0.4 -angle -0.0941996781 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
Dipole -name "DIPOLE_130" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*-0.0941996781*-0.0941996781/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_131" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_132" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "BH2XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.1839837463*$e0] -e0 $e0 
Dipole -name "DIPOLE_133" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L013A" -length 0.2142345 
Dipole -name "DIPOLE_134" -length 0.0 -strength_x 0.0 -strength_y 0.0 
VCORRECTOR -name "ZV5X" -length 0.128141 
Dipole -name "DIPOLE_135" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L013B" -length 0.1429295 
Dipole -name "DIPOLE_136" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQF6X" -length 0 -resolution $stripline
Dipole -name "DIPOLE_137" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L013C" -length 0.014695 
Dipole -name "DIPOLE_138" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF6X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.5650485124*$e0] -e0 $e0 
Dipole -name "DIPOLE_139" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF6X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.5650485124*$e0] -e0 $e0 
Dipole -name "DIPOLE_140" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L014A" -length 0.00536 
Dipole -name "DIPOLE_141" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QS2X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Dipole -name "DIPOLE_142" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QS2X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Dipole -name "DIPOLE_143" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L014B" -length 0.334665 
Dipole -name "DIPOLE_144" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "UDDX1" -length 0 
Dipole -name "DIPOLE_145" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L014C" -length 0.5875079196 
Dipole -name "DIPOLE_146" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "BH3XA" -synrad $sbend_synrad -length 0.675 -angle -0.1714240956 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
Dipole -name "DIPOLE_147" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*-0.1714240956*-0.1714240956/0.675*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_148" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_149" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "BH3XB" -synrad $sbend_synrad -length 0.675 -angle -0.1714240956 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
Dipole -name "DIPOLE_150" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*-0.1714240956*-0.1714240956/0.675*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_151" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_152" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L015" -length 0.7650382419 
Dipole -name "DIPOLE_153" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF7X" -synrad $quad_synrad -length 0.039335 -strength [expr 0.1915156418*$e0] -e0 $e0 
Dipole -name "DIPOLE_154" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF7X" -synrad $quad_synrad -length 0.039335 -strength [expr 0.1915156418*$e0] -e0 $e0 
Dipole -name "DIPOLE_155" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L016A" -length 0.026665 
Dipole -name "DIPOLE_156" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQF7X" -length 0 -resolution $stripline
Dipole -name "DIPOLE_157" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L016B" -length 1.767246207 
Dipole -name "DIPOLE_158" -length 0.0 -strength_x 0.0 -strength_y 0.0 
HCORRECTOR -name "ZH3X" -length 0.11455 
Dipole -name "DIPOLE_159" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L016C" -length 0.16342 
Dipole -name "DIPOLE_160" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD8X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.2950188224*$e0] -e0 $e0 
Dipole -name "DIPOLE_161" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD8X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.2950188224*$e0] -e0 $e0 
Dipole -name "DIPOLE_162" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L017A" -length 0.014695 
Dipole -name "DIPOLE_163" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQD8X" -length 0 -resolution $stripline
Dipole -name "DIPOLE_164" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L017B" -length 0.1389295 
Dipole -name "DIPOLE_165" -length 0.0 -strength_x 0.0 -strength_y 0.0 
VCORRECTOR -name "ZV6X" -length 0.128141 
Dipole -name "DIPOLE_166" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L017C" -length 0.6855607408 
Dipole -name "DIPOLE_167" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "KEX2A" -length 0.2500010417 
Dipole -name "DIPOLE_168" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "IP02" -length 0 
Dipole -name "DIPOLE_169" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "KEX2B" -length 0.2500010417 
Dipole -name "DIPOLE_170" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "MDISP" -length 0 
Dipole -name "DIPOLE_171" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L101A" -length 0.06027549974 
Dipole -name "DIPOLE_172" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "BKXA" -synrad $sbend_synrad -length 0.1 -angle 0.0025 -E1 0.005 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.05 -fint 0.5 
Dipole -name "DIPOLE_173" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*0.0025*0.0025/0.1*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_174" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_175" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "BKXB" -synrad $sbend_synrad -length 0.1 -angle 0.0025 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.05 -fintx 0.5 
Dipole -name "DIPOLE_176" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*0.0025*0.0025/0.1*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_177" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_178" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L101B" -length 0.166 
Dipole -name "DIPOLE_179" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQF9X" -length 0 -resolution $stripline
Dipole -name "DIPOLE_180" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L101C" -length 0.054695 
Dipole -name "DIPOLE_181" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF9X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3814832824*$e0] -e0 $e0 
Dipole -name "DIPOLE_182" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF9X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3814832824*$e0] -e0 $e0 
Dipole -name "DIPOLE_183" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L102A" -length 0.15742 
Dipole -name "DIPOLE_184" -length 0.0 -strength_x 0.0 -strength_y 0.0 
HCORRECTOR -name "ZH4X" -length 0.11455 
Dipole -name "DIPOLE_185" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L102B" -length 1.339476477 
Dipole -name "DIPOLE_186" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "FONTK1" -length 0 
Dipole -name "DIPOLE_187" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L102C" -length 0.3339295 
Dipole -name "DIPOLE_188" -length 0.0 -strength_x 0.0 -strength_y 0.0 
VCORRECTOR -name "ZV7X" -length 0.128141 
Dipole -name "DIPOLE_189" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L102D" -length 0.0859295 
Dipole -name "DIPOLE_190" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Bpm -name "FONTP1" -length 0 -resolution $font_stripline_resolution
Dipole -name "DIPOLE_191" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L102E" -length 0.125665 
Dipole -name "DIPOLE_192" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QK1X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Dipole -name "DIPOLE_193" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QK1X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Dipole -name "DIPOLE_194" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L102F" -length 0.10242 
Dipole -name "DIPOLE_195" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.061259524*$e0] -e0 $e0 -tilt 0.2728979485 
Dipole -name "DIPOLE_196" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-7.416610409*$e0] -e0 $e0 -tilt -0.4510025096 
Dipole -name "DIPOLE_197" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD10X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5833119514*$e0] -e0 $e0 
Dipole -name "DIPOLE_198" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.061259524*$e0] -e0 $e0 -tilt 0.2728979485 
Dipole -name "DIPOLE_199" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-7.416610409*$e0] -e0 $e0 -tilt -0.4510025096 
Dipole -name "DIPOLE_200" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.061259524*$e0] -e0 $e0 -tilt 0.2728979485 
Dipole -name "DIPOLE_201" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-7.416610409*$e0] -e0 $e0 -tilt -0.4510025096 
Dipole -name "DIPOLE_202" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD10X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5833119514*$e0] -e0 $e0 
Dipole -name "DIPOLE_203" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.061259524*$e0] -e0 $e0 -tilt 0.2728979485 
Dipole -name "DIPOLE_204" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-7.416610409*$e0] -e0 $e0 -tilt -0.4510025096 
Dipole -name "DIPOLE_205" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L103A" -length 0.019755 
Dipole -name "DIPOLE_206" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQD10X" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_207" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L103B" -length 0.9566 
Dipole -name "DIPOLE_208" -length 0.0 -strength_x 0.0 -strength_y 0.0 
HCORRECTOR -name "ZH5X" -length 0.1248 
Dipole -name "DIPOLE_209" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L103C" -length 0.150355 
Dipole -name "DIPOLE_210" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF11XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02909535705*$e0] -e0 $e0 -tilt -0.2256002597 
Dipole -name "DIPOLE_211" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF11XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*9.212898679*$e0] -e0 $e0 -tilt 0.1224996226 
Dipole -name "DIPOLE_212" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF11X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.5116656769*$e0] -e0 $e0 
Dipole -name "DIPOLE_213" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF11XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02909535705*$e0] -e0 $e0 -tilt -0.2256002597 
Dipole -name "DIPOLE_214" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF11XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*9.212898679*$e0] -e0 $e0 -tilt 0.1224996226 
Dipole -name "DIPOLE_215" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "MSKEW" -length 0 
Dipole -name "DIPOLE_216" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF11XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02909535705*$e0] -e0 $e0 -tilt -0.2256002597 
Dipole -name "DIPOLE_217" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF11XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*9.212898679*$e0] -e0 $e0 -tilt 0.1224996226 
Dipole -name "DIPOLE_218" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF11X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.5116656769*$e0] -e0 $e0 
Dipole -name "DIPOLE_219" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF11XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02909535705*$e0] -e0 $e0 -tilt -0.2256002597 
Dipole -name "DIPOLE_220" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF11XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*9.212898679*$e0] -e0 $e0 -tilt 0.1224996226 
Dipole -name "DIPOLE_221" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L104A" -length 0.019755 
Dipole -name "DIPOLE_222" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQF11X" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_223" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L104B" -length 0.403 
Dipole -name "DIPOLE_224" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "FONTK2" -length 0 
Dipole -name "DIPOLE_225" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L104C" -length 0.3069295 
Dipole -name "DIPOLE_226" -length 0.0 -strength_x 0.0 -strength_y 0.0 
VCORRECTOR -name "ZV8X" -length 0.128141 
Dipole -name "DIPOLE_227" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L104D" -length 0.0899295 
Dipole -name "DIPOLE_228" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Bpm -name "FONTP2" -length 0 -resolution $font_stripline_resolution
Dipole -name "DIPOLE_229" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L104F" -length 0.12433 
Dipole -name "DIPOLE_230" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QK2X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Dipole -name "DIPOLE_231" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QK2X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Dipole -name "DIPOLE_232" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L104G" -length 0.100755 
Dipole -name "DIPOLE_233" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD12XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.07128526211*$e0] -e0 $e0 -tilt 0.1714028079 
Dipole -name "DIPOLE_234" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD12XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-5.00930931*$e0] -e0 $e0 -tilt 0.643546056 
Dipole -name "DIPOLE_235" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD12X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5116656769*$e0] -e0 $e0 
Dipole -name "DIPOLE_236" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD12XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.07128526211*$e0] -e0 $e0 -tilt 0.1714028079 
Dipole -name "DIPOLE_237" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD12XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-5.00930931*$e0] -e0 $e0 -tilt 0.643546056 
Dipole -name "DIPOLE_238" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD12XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.07128526211*$e0] -e0 $e0 -tilt 0.1714028079 
Dipole -name "DIPOLE_239" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD12XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-5.00930931*$e0] -e0 $e0 -tilt 0.643546056 
Dipole -name "DIPOLE_240" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD12X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5116656769*$e0] -e0 $e0 
Dipole -name "DIPOLE_241" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD12XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.07128526211*$e0] -e0 $e0 -tilt 0.1714028079 
Dipole -name "DIPOLE_242" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD12XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-5.00930931*$e0] -e0 $e0 -tilt 0.643546056 
Dipole -name "DIPOLE_243" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L105A" -length 0.019755 
Dipole -name "DIPOLE_244" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQD12X" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_245" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L105B" -length 0.5351231519 
Dipole -name "DIPOLE_246" -length 0.0 -strength_x 0.0 -strength_y 0.0 
HCORRECTOR -name "ZH6X" -length 0.11455 
Dipole -name "DIPOLE_247" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L105C" -length 0.1653525 
Dipole -name "DIPOLE_248" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF13X" -synrad $quad_synrad -length 0.0993725 -strength [expr 0.6841194487*$e0] -e0 $e0 
Dipole -name "DIPOLE_249" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF13X" -synrad $quad_synrad -length 0.0993725 -strength [expr 0.6841194487*$e0] -e0 $e0 
Dipole -name "DIPOLE_250" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L106A" -length 0.0546275 
Dipole -name "DIPOLE_251" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQF13X" -length 0 -resolution $stripline
Dipole -name "DIPOLE_252" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L106B" -length 0.7821670781 
Dipole -name "DIPOLE_253" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD14X" -synrad $quad_synrad -length 0.0993725 -strength [expr -0.507718235*$e0] -e0 $e0 
Dipole -name "DIPOLE_254" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "FIT180" -length 0 
Dipole -name "DIPOLE_255" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD14X" -synrad $quad_synrad -length 0.0993725 -strength [expr -0.507718235*$e0] -e0 $e0 
Dipole -name "DIPOLE_256" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L107A" -length 0.0546275 
Dipole -name "DIPOLE_257" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQD14X" -length 0 -resolution $stripline
Dipole -name "DIPOLE_258" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L107B" -length 0.276 
Dipole -name "DIPOLE_259" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Bpm -name "FONTP3" -length 0 -resolution $font_stripline_resolution
Dipole -name "DIPOLE_260" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L107C" -length 0.2282645781 
Dipole -name "DIPOLE_261" -length 0.0 -strength_x 0.0 -strength_y 0.0 
HCORRECTOR -name "ZH7X" -length 0.11455 
Dipole -name "DIPOLE_262" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L107D" -length 0.1633525 
Dipole -name "DIPOLE_263" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF15X" -synrad $quad_synrad -length 0.0993725 -strength [expr 0.6841194487*$e0] -e0 $e0 
Dipole -name "DIPOLE_264" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF15X" -synrad $quad_synrad -length 0.0993725 -strength [expr 0.6841194487*$e0] -e0 $e0 
Dipole -name "DIPOLE_265" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L108A" -length 0.0546275 
Dipole -name "DIPOLE_266" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Bpm -name "MQF15X" -length 0 -resolution $stripline
Dipole -name "DIPOLE_267" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L108B" -length 0.3156 
Dipole -name "DIPOLE_268" -length 0.0 -strength_x 0.0 -strength_y 0.0 
VCORRECTOR -name "ZV9X" -length 0.1248 
Dipole -name "DIPOLE_269" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L108D" -length 0.1603281519 
Dipole -name "DIPOLE_270" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QK3X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Dipole -name "DIPOLE_271" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QK3X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Dipole -name "DIPOLE_272" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L108E" -length 0.100755 
Dipole -name "DIPOLE_273" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD16XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01998975467*$e0] -e0 $e0 -tilt -0.2319377413 
Dipole -name "DIPOLE_274" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD16XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-2.427444304*$e0] -e0 $e0 -tilt -0.6034986504 
Dipole -name "DIPOLE_275" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD16X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5116656769*$e0] -e0 $e0 
Dipole -name "DIPOLE_276" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD16XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01998975467*$e0] -e0 $e0 -tilt -0.2319377413 
Dipole -name "DIPOLE_277" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD16XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-2.427444304*$e0] -e0 $e0 -tilt -0.6034986504 
Dipole -name "DIPOLE_278" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD16XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01998975467*$e0] -e0 $e0 -tilt -0.2319377413 
Dipole -name "DIPOLE_279" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD16XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-2.427444304*$e0] -e0 $e0 -tilt -0.6034986504 
Dipole -name "DIPOLE_280" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD16X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5116656769*$e0] -e0 $e0 
Dipole -name "DIPOLE_281" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD16XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01998975467*$e0] -e0 $e0 -tilt -0.2319377413 
Dipole -name "DIPOLE_282" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD16XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-2.427444304*$e0] -e0 $e0 -tilt -0.6034986504 
Dipole -name "DIPOLE_283" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L109A" -length 0.019755 
Dipole -name "DIPOLE_284" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQD16X" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_285" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L109B" -length 0.9546 
Dipole -name "DIPOLE_286" -length 0.0 -strength_x 0.0 -strength_y 0.0 
HCORRECTOR -name "ZH8X" -length 0.1248 
Dipole -name "DIPOLE_287" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L109C" -length 0.152355 
Dipole -name "DIPOLE_288" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF17XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.005940438509*$e0] -e0 $e0 -tilt 0.6748486461 
Dipole -name "DIPOLE_289" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF17XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.753578805*$e0] -e0 $e0 -tilt 0.09200095134 
Dipole -name "DIPOLE_290" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF17X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.5116656769*$e0] -e0 $e0 
Dipole -name "DIPOLE_291" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF17XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.005940438509*$e0] -e0 $e0 -tilt 0.6748486461 
Dipole -name "DIPOLE_292" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF17XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.753578805*$e0] -e0 $e0 -tilt 0.09200095134 
Dipole -name "DIPOLE_293" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF17XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.005940438509*$e0] -e0 $e0 -tilt 0.6748486461 
Dipole -name "DIPOLE_294" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF17XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.753578805*$e0] -e0 $e0 -tilt 0.09200095134 
Dipole -name "DIPOLE_295" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF17X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.5116656769*$e0] -e0 $e0 
Dipole -name "DIPOLE_296" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF17XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.005940438509*$e0] -e0 $e0 -tilt 0.6748486461 
Dipole -name "DIPOLE_297" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF17XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.753578805*$e0] -e0 $e0 -tilt 0.09200095134 
Dipole -name "DIPOLE_298" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L110A" -length 0.019755 
Dipole -name "DIPOLE_299" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQF17X" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_300" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L110B" -length 0.211 
Dipole -name "DIPOLE_301" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "MREF1X" -length 0 
Dipole -name "DIPOLE_302" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L110C" -length 0.8401649959 
Dipole -name "DIPOLE_303" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QK4X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Dipole -name "DIPOLE_304" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QK4X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Dipole -name "DIPOLE_305" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L110D" -length 0.6575945 
Dipole -name "DIPOLE_306" -length 0.0 -strength_x 0.0 -strength_y 0.0 
VCORRECTOR -name "ZV10X" -length 0.128141 
Dipole -name "DIPOLE_307" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L110E" -length 0.2486845 
Dipole -name "DIPOLE_308" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD18XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.008575901695*$e0] -e0 $e0 -tilt 0.530866035 
Dipole -name "DIPOLE_309" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD18XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.348131746*$e0] -e0 $e0 -tilt 0.5677030286 
Dipole -name "DIPOLE_310" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD18X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.3430360678*$e0] -e0 $e0 
Dipole -name "DIPOLE_311" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD18XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.008575901695*$e0] -e0 $e0 -tilt 0.530866035 
Dipole -name "DIPOLE_312" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD18XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.348131746*$e0] -e0 $e0 -tilt 0.5677030286 
Dipole -name "DIPOLE_313" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD18XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.008575901695*$e0] -e0 $e0 -tilt 0.530866035 
Dipole -name "DIPOLE_314" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD18XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.348131746*$e0] -e0 $e0 -tilt 0.5677030286 
Dipole -name "DIPOLE_315" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD18X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.3430360678*$e0] -e0 $e0 
Dipole -name "DIPOLE_316" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD18XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.008575901695*$e0] -e0 $e0 -tilt 0.530866035 
Dipole -name "DIPOLE_317" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD18XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.348131746*$e0] -e0 $e0 -tilt 0.5677030286 
Dipole -name "DIPOLE_318" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L111A" -length 0.019755 
Dipole -name "DIPOLE_319" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQD18X" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_320" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L111B" -length 0.276 
Dipole -name "DIPOLE_321" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "MW0X" -length 0 
Dipole -name "DIPOLE_322" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L111C" -length 0.4046498952 
Dipole -name "DIPOLE_323" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "OTR0X" -length 0 
Dipole -name "DIPOLE_324" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L111D" -length 0.2716 
Dipole -name "DIPOLE_325" -length 0.0 -strength_x 0.0 -strength_y 0.0 
HCORRECTOR -name "ZH9X" -length 0.1248 
Dipole -name "DIPOLE_326" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L111E" -length 0.149355 
Dipole -name "DIPOLE_327" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF19XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002165323765*$e0] -e0 $e0 -tilt 0.4851101624 
Dipole -name "DIPOLE_328" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF19XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.700254151*$e0] -e0 $e0 -tilt -0.3060050931 
Dipole -name "DIPOLE_329" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF19X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.3275830205*$e0] -e0 $e0 
Dipole -name "DIPOLE_330" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF19XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002165323765*$e0] -e0 $e0 -tilt 0.4851101624 
Dipole -name "DIPOLE_331" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF19XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.700254151*$e0] -e0 $e0 -tilt -0.3060050931 
Dipole -name "DIPOLE_332" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF19XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002165323765*$e0] -e0 $e0 -tilt 0.4851101624 
Dipole -name "DIPOLE_333" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF19XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.700254151*$e0] -e0 $e0 -tilt -0.3060050931 
Dipole -name "DIPOLE_334" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF19X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.3275830205*$e0] -e0 $e0 
Dipole -name "DIPOLE_335" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF19XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002165323765*$e0] -e0 $e0 -tilt 0.4851101624 
Dipole -name "DIPOLE_336" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF19XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.700254151*$e0] -e0 $e0 -tilt -0.3060050931 
Dipole -name "DIPOLE_337" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L112A" -length 0.019755 
Dipole -name "DIPOLE_338" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQF19X" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_339" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L112B" -length 0.265 
Dipole -name "DIPOLE_340" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "MW1X" -length 0 
Dipole -name "DIPOLE_341" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L112C" -length 0.386 
Dipole -name "DIPOLE_342" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "OTR1X" -length 0 
Dipole -name "DIPOLE_343" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L112D" -length 0.309 
Dipole -name "DIPOLE_344" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "WCM" -length 0 
Dipole -name "DIPOLE_345" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L112E" -length 0.4849295 
Dipole -name "DIPOLE_346" -length 0.0 -strength_x 0.0 -strength_y 0.0 
VCORRECTOR -name "ZV11X" -length 0.128141 
Dipole -name "DIPOLE_347" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L112F" -length 1.4372645 
Dipole -name "DIPOLE_348" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "PPMW" -length 0 
Dipole -name "DIPOLE_349" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L112G" -length 0.469 
Dipole -name "DIPOLE_350" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "MW2X" -length 0 
Dipole -name "DIPOLE_351" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L112H" -length 0.282 
Dipole -name "DIPOLE_352" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQD20X" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_353" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L112I" -length 0.019665 
Dipole -name "DIPOLE_354" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD20X" -synrad $quad_synrad -length 0.039335 -strength [expr -0.1511348642*$e0] -e0 $e0 
Dipole -name "DIPOLE_355" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "MEMIT" -length 0 
Dipole -name "DIPOLE_356" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD20X" -synrad $quad_synrad -length 0.039335 -strength [expr -0.1511348642*$e0] -e0 $e0 
Dipole -name "DIPOLE_357" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L113A" -length 0.466665 
Dipole -name "DIPOLE_358" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "OTR2X" -length 0 
Dipole -name "DIPOLE_359" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L113B" -length 1.861065 
Dipole -name "DIPOLE_360" -length 0.0 -strength_x 0.0 -strength_y 0.0 
HCORRECTOR -name "ZH10X" -length 0.11921 
Dipole -name "DIPOLE_361" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L113C" -length 0.937395 
Dipole -name "DIPOLE_362" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "ICT1X" -length 0 
Dipole -name "DIPOLE_363" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L113D" -length 0.415665 
Dipole -name "DIPOLE_364" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF21X" -synrad $quad_synrad -length 0.039335 -strength [expr 0.2223564783*$e0] -e0 $e0 
Dipole -name "DIPOLE_365" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF21X" -synrad $quad_synrad -length 0.039335 -strength [expr 0.2223564783*$e0] -e0 $e0 
Dipole -name "DIPOLE_366" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L114A" -length 0.019665 
Dipole -name "DIPOLE_367" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQF21X" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_368" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L114B" -length 0.274 
Dipole -name "DIPOLE_369" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "MW3X" -length 0 
Dipole -name "DIPOLE_370" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L114C" -length 0.502 
Dipole -name "DIPOLE_371" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "OTR3X" -length 0 
Dipole -name "DIPOLE_372" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L114D" -length 0.35 
Dipole -name "DIPOLE_373" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "TILTM" -length 0 
Dipole -name "DIPOLE_374" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L114E" -length 0.54 
Dipole -name "DIPOLE_375" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#not present anymore
Dipole -name "DIPOLE_376" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Bpm -name "IPBPM" -length 0 
Dipole -name "DIPOLE_377" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "ATIPBPM" -length 0 
Dipole -name "DIPOLE_378" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L114F" -length 1.813335 
Dipole -name "DIPOLE_379" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "MW4X" -length 0 
Dipole -name "DIPOLE_380" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L114G" -length 0.282 
Dipole -name "DIPOLE_381" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "BEGFF" -length 0
Dipole -name "DIPOLE_382" -length 0.0 -strength_x 0.0 -strength_y 0.0 
if {[info exists use_dispersion_ff] && $use_dispersion_ff} {
Dipole -name "DIPOLE_383" -length 0.0 -strength_x 0.0 -strength_y 0.0 
    TclCall -name "ADDDISPERSIONFFSTART" -script "add_dispersion_ff_start"
Dipole -name "DIPOLE_384" -length 0.0 -strength_x 0.0 -strength_y 0.0 
}
Dipole -name "DIPOLE_385" -length 0.0 -strength_x 0.0 -strength_y 0.0 
if {[info exists save_beam_ff_start] && $save_beam_ff_start} {
Dipole -name "DIPOLE_386" -length 0.0 -strength_x 0.0 -strength_y 0.0 
    TclCall -name "SAVEBEAMFFSTART" -script "save_beam_ff_start"
Dipole -name "DIPOLE_387" -length 0.0 -strength_x 0.0 -strength_y 0.0 
}
Dipole -name "DIPOLE_388" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQM16FF" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
Dipole -name "DIPOLE_389" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L200" -length 0.019755 
Dipole -name "DIPOLE_390" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM16FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003590382417*$e0] -e0 $e0 -tilt -0.3676020139 
Dipole -name "DIPOLE_391" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM16FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.145971342*$e0] -e0 $e0 -tilt -0.6634969671 
Dipole -name "DIPOLE_392" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QM16FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0*$e0] -e0 $e0 
Dipole -name "DIPOLE_393" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM16FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003590382417*$e0] -e0 $e0 -tilt -0.3676020139 
Dipole -name "DIPOLE_394" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM16FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.145971342*$e0] -e0 $e0 -tilt -0.6634969671 
Dipole -name "DIPOLE_395" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQM16FF" -length 0 
Dipole -name "DIPOLE_396" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM16FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003590382417*$e0] -e0 $e0 -tilt -0.3676020139 
Dipole -name "DIPOLE_397" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM16FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.145971342*$e0] -e0 $e0 -tilt -0.6634969671 
Dipole -name "DIPOLE_398" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QM16FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0*$e0] -e0 $e0 
Dipole -name "DIPOLE_399" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM16FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003590382417*$e0] -e0 $e0 -tilt -0.3676020139 
Dipole -name "DIPOLE_400" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM16FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.145971342*$e0] -e0 $e0 -tilt -0.6634969671 
Dipole -name "DIPOLE_401" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L201A" -length 0.76315 
Dipole -name "DIPOLE_402" -length 0.0 -strength_x 0.0 -strength_y 0.0 
HCORRECTOR -name "ZH1FF" -length 0.11921 
Dipole -name "DIPOLE_403" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L201B" -length 0.1353245 
Dipole -name "DIPOLE_404" -length 0.0 -strength_x 0.0 -strength_y 0.0 
VCORRECTOR -name "ZV1FF" -length 0.128141 
Dipole -name "DIPOLE_405" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L201C" -length 0.1556845 
Dipole -name "DIPOLE_406" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM15FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0009094236936*$e0] -e0 $e0 -tilt 0.507224233 
Dipole -name "DIPOLE_407" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM15FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.4632096203*$e0] -e0 $e0 -tilt 0.1838965077 
Dipole -name "DIPOLE_408" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QM15FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.3199356679*$e0] -e0 $e0 
Dipole -name "DIPOLE_409" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM15FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0009094236936*$e0] -e0 $e0 -tilt 0.507224233 
Dipole -name "DIPOLE_410" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM15FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.4632096203*$e0] -e0 $e0 -tilt 0.1838965077 
Dipole -name "DIPOLE_411" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQM15FF" -length 0 
Dipole -name "DIPOLE_412" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM15FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0009094236936*$e0] -e0 $e0 -tilt 0.507224233 
Dipole -name "DIPOLE_413" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM15FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.4632096203*$e0] -e0 $e0 -tilt 0.1838965077 
Dipole -name "DIPOLE_414" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QM15FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.3199356679*$e0] -e0 $e0 
Dipole -name "DIPOLE_415" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM15FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0009094236936*$e0] -e0 $e0 -tilt 0.507224233 
Dipole -name "DIPOLE_416" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM15FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.4632096203*$e0] -e0 $e0 -tilt 0.1838965077 
Dipole -name "DIPOLE_417" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L202A" -length 0.019755 
Dipole -name "DIPOLE_418" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQM15FF" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
Dipole -name "DIPOLE_419" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L202B" -length 1.281755 
Dipole -name "DIPOLE_420" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM14FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03177746312*$e0] -e0 $e0 -tilt -0.8477670492 
Dipole -name "DIPOLE_421" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM14FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-4.818911977*$e0] -e0 $e0 -tilt 0.1220007465 
Dipole -name "DIPOLE_422" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QM14FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.758062188*$e0] -e0 $e0 
Dipole -name "DIPOLE_423" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM14FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03177746312*$e0] -e0 $e0 -tilt -0.8477670492 
Dipole -name "DIPOLE_424" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM14FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-4.818911977*$e0] -e0 $e0 -tilt 0.1220007465 
Dipole -name "DIPOLE_425" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQM14FF" -length 0 
Dipole -name "DIPOLE_426" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM14FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03177746312*$e0] -e0 $e0 -tilt -0.8477670492 
Dipole -name "DIPOLE_427" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM14FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-4.818911977*$e0] -e0 $e0 -tilt 0.1220007465 
Dipole -name "DIPOLE_428" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QM14FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.758062188*$e0] -e0 $e0 
Dipole -name "DIPOLE_429" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM14FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03177746312*$e0] -e0 $e0 -tilt -0.8477670492 
Dipole -name "DIPOLE_430" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM14FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-4.818911977*$e0] -e0 $e0 -tilt 0.1220007465 
Dipole -name "DIPOLE_431" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L203A" -length 0.019755 
Dipole -name "DIPOLE_432" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQM14FF" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
Dipole -name "DIPOLE_433" -length 0.0 -strength_x 0.0 -strength_y 0.0 
if {[info exist use_mfb2ffkicker] } {
Dipole -name "DIPOLE_434" -length 0.0 -strength_x 0.0 -strength_y 0.0 
    Dipole -name "MFB2FFKICKER" -length 0.0 -strength_x 0.0 -strength_y 0.0
Dipole -name "DIPOLE_435" -length 0.0 -strength_x 0.0 -strength_y 0.0 
}
Dipole -name "DIPOLE_436" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L203B" -length 0.557455 
Dipole -name "DIPOLE_437" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "LWML1" -length 0 
Dipole -name "DIPOLE_438" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L203C" -length 0.0215 
Dipole -name "DIPOLE_439" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "LW1FF" -length 0 
Dipole -name "DIPOLE_440" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L203D" -length 0.0215 
Dipole -name "DIPOLE_441" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "LWML2" -length 0 
Dipole -name "DIPOLE_442" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L203E" -length 0.2035 
Dipole -name "DIPOLE_443" -length 0.0 -strength_x 0.0 -strength_y 0.0 
if {[info exists save_beam_mfb2ff] && $save_beam_mfb2ff} {
Dipole -name "DIPOLE_444" -length 0.0 -strength_x 0.0 -strength_y 0.0 
    TclCall -name "SAVEBEAMMFB2FF" -script "save_beam_mfb2ff"
Dipole -name "DIPOLE_445" -length 0.0 -strength_x 0.0 -strength_y 0.0 
}
Dipole -name "DIPOLE_446" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Bpm -name "MFB2FF" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
Dipole -name "DIPOLE_447" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AFB2FF" -length 0 
Dipole -name "DIPOLE_448" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L203F" -length 0.4778 
Dipole -name "DIPOLE_449" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM13FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002564596147*$e0] -e0 $e0 -tilt 0.4942315471 
Dipole -name "DIPOLE_450" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM13FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*6.191676439*$e0] -e0 $e0 -tilt 0.040266308 
Dipole -name "DIPOLE_451" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QM13FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.4629159803*$e0] -e0 $e0 
Dipole -name "DIPOLE_452" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM13FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002564596147*$e0] -e0 $e0 -tilt 0.4942315471 
Dipole -name "DIPOLE_453" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM13FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*6.191676439*$e0] -e0 $e0 -tilt 0.040266308 
Dipole -name "DIPOLE_454" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQM13FF" -length 0 
Dipole -name "DIPOLE_455" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM13FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002564596147*$e0] -e0 $e0 -tilt 0.4942315471 
Dipole -name "DIPOLE_456" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM13FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*6.191676439*$e0] -e0 $e0 -tilt 0.040266308 
Dipole -name "DIPOLE_457" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QM13FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.4629159803*$e0] -e0 $e0 
Dipole -name "DIPOLE_458" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM13FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002564596147*$e0] -e0 $e0 -tilt 0.4942315471 
Dipole -name "DIPOLE_459" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM13FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*6.191676439*$e0] -e0 $e0 -tilt 0.040266308 
Dipole -name "DIPOLE_460" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L204A" -length 0.019755 
Dipole -name "DIPOLE_461" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQM13FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_462" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L204B" -length 1.281755 
Dipole -name "DIPOLE_463" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM12FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.009392199386*$e0] -e0 $e0 -tilt 1.041419048 
Dipole -name "DIPOLE_464" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM12FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2866941739*$e0] -e0 $e0 -tilt -0.6337616359 
Dipole -name "DIPOLE_465" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QM12FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1164254228*$e0] -e0 $e0 
Dipole -name "DIPOLE_466" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM12FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.009392199386*$e0] -e0 $e0 -tilt 1.041419048 
Dipole -name "DIPOLE_467" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM12FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2866941739*$e0] -e0 $e0 -tilt -0.6337616359 
Dipole -name "DIPOLE_468" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQM12FF" -length 0 
Dipole -name "DIPOLE_469" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM12FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.009392199386*$e0] -e0 $e0 -tilt 1.041419048 
Dipole -name "DIPOLE_470" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM12FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2866941739*$e0] -e0 $e0 -tilt -0.6337616359 
Dipole -name "DIPOLE_471" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QM12FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1164254228*$e0] -e0 $e0 
Dipole -name "DIPOLE_472" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM12FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.009392199386*$e0] -e0 $e0 -tilt 1.041419048 
Dipole -name "DIPOLE_473" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QM12FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2866941739*$e0] -e0 $e0 -tilt -0.6337616359 
Dipole -name "DIPOLE_474" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L205A" -length 0.019755 
Dipole -name "DIPOLE_475" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQM12FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_476" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L205B" -length 0.715755 
Dipole -name "DIPOLE_477" -length 0.0 -strength_x 0.0 -strength_y 0.0 
# not present
Dipole -name "DIPOLE_478" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Bpm -name "MFB1FF" -length 0 
Dipole -name "DIPOLE_479" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L205C" -length 0.666 
Dipole -name "DIPOLE_480" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QM11FFMULT2" -length 0 -tilt 0.4292947267 
Dipole -name "DIPOLE_481" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QM11FFMULT3" -length 0 -tilt 0.7639750205 
Dipole -name "DIPOLE_482" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QM11FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.08000716598*$e0] -e0 $e0 
Dipole -name "DIPOLE_483" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QM11FFMULT2" -length 0 -tilt 0.4292947267 
Dipole -name "DIPOLE_484" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QM11FFMULT3" -length 0 -tilt 0.7639750205 
Dipole -name "DIPOLE_485" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQM11FF" -length 0 
Dipole -name "DIPOLE_486" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QM11FFMULT2" -length 0 -tilt 0.4292947267 
Dipole -name "DIPOLE_487" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QM11FFMULT3" -length 0 -tilt 0.7639750205 
Dipole -name "DIPOLE_488" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QM11FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.08000716598*$e0] -e0 $e0 
Dipole -name "DIPOLE_489" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QM11FFMULT2" -length 0 -tilt 0.4292947267 
Dipole -name "DIPOLE_490" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QM11FFMULT3" -length 0 -tilt 0.7639750205 
Dipole -name "DIPOLE_491" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L206A" -length 0.019755 
Dipole -name "DIPOLE_492" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQM11FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_493" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L206BA" -length 0.8408775 
Dipole -name "DIPOLE_494" -length 0.0 -strength_x 0.0 -strength_y 0.0 
# collimator cavity not connected
Dipole -name "DIPOLE_495" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Bpm -name "COLLBY" -length 0 
Dipole -name "DIPOLE_496" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L206BB" -length 0.4408775 
Dipole -name "DIPOLE_497" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003197634999*$e0] -e0 $e0 -tilt -0.882813608 
Dipole -name "DIPOLE_498" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.407797338*$e0] -e0 $e0 -tilt -0.7077528722 
Dipole -name "DIPOLE_499" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD10BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1450096657*$e0] -e0 $e0 
Dipole -name "DIPOLE_500" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003197634999*$e0] -e0 $e0 -tilt -0.882813608 
Dipole -name "DIPOLE_501" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.407797338*$e0] -e0 $e0 -tilt -0.7077528722 
Dipole -name "DIPOLE_502" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQD10BFF" -length 0 
Dipole -name "DIPOLE_503" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003197634999*$e0] -e0 $e0 -tilt -0.882813608 
Dipole -name "DIPOLE_504" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.407797338*$e0] -e0 $e0 -tilt -0.7077528722 
Dipole -name "DIPOLE_505" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD10BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1450096657*$e0] -e0 $e0 
Dipole -name "DIPOLE_506" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003197634999*$e0] -e0 $e0 -tilt -0.882813608 
Dipole -name "DIPOLE_507" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.407797338*$e0] -e0 $e0 -tilt -0.7077528722 
Dipole -name "DIPOLE_508" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L207A" -length 0.019755 
Dipole -name "DIPOLE_509" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQD10BFF" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
Dipole -name "DIPOLE_510" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L207B" -length 0.477 
Dipole -name "DIPOLE_511" -length 0.0 -strength_x 0.0 -strength_y 0.0 
# collimator cavity not connected
Dipole -name "DIPOLE_512" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Bpm -name "COLLB" -length 0 
Dipole -name "DIPOLE_513" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "MREF3FF" -length 0 
Dipole -name "DIPOLE_514" -length 0.0 -strength_x 0.0 -strength_y 0.0 
# wakeFieldSetup on mover
Dipole -name "DIPOLE_515" -length 0.0 -strength_x 0.0 -strength_y 0.0 
if {[info exist wakeFieldSetup] && $wakeFieldSetup} {
Dipole -name "DIPOLE_516" -length 0.0 -strength_x 0.0 -strength_y 0.0 
    source ${lattice_dir}/ATF2_wakeFieldSetup.tcl
Dipole -name "DIPOLE_517" -length 0.0 -strength_x 0.0 -strength_y 0.0 
}
Dipole -name "DIPOLE_518" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L207C" -length 0.204755 
Dipole -name "DIPOLE_519" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.005931299368*$e0] -e0 $e0 -tilt -0.8561060094 
Dipole -name "DIPOLE_520" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6223524834*$e0] -e0 $e0 -tilt -0.0202933426 
Dipole -name "DIPOLE_521" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD10AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1450096657*$e0] -e0 $e0 
Dipole -name "DIPOLE_522" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.005931299368*$e0] -e0 $e0 -tilt -0.8561060094 
Dipole -name "DIPOLE_523" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6223524834*$e0] -e0 $e0 -tilt -0.0202933426 
Dipole -name "DIPOLE_524" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQD10AFF" -length 0 
Dipole -name "DIPOLE_525" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.005931299368*$e0] -e0 $e0 -tilt -0.8561060094 
Dipole -name "DIPOLE_526" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6223524834*$e0] -e0 $e0 -tilt -0.0202933426 
Dipole -name "DIPOLE_527" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD10AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1450096657*$e0] -e0 $e0 
Dipole -name "DIPOLE_528" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.005931299368*$e0] -e0 $e0 -tilt -0.8561060094 
Dipole -name "DIPOLE_529" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD10AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6223524834*$e0] -e0 $e0 -tilt -0.0202933426 
Dipole -name "DIPOLE_530" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L208A" -length 0.019755 
Dipole -name "DIPOLE_531" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQD10AFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_532" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L208B" -length 0.289 
Dipole -name "DIPOLE_533" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "MS1FF" -length 0 
Dipole -name "DIPOLE_534" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L208C" -length 0.792755 
Dipole -name "DIPOLE_535" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF9BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.03025071081*$e0] -e0 $e0 -tilt 0.9789002044 
Dipole -name "DIPOLE_536" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF9BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.4884584584*$e0] -e0 $e0 -tilt -0.5935711555 
Dipole -name "DIPOLE_537" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF9BFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1893249839*$e0] -e0 $e0 
Dipole -name "DIPOLE_538" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF9BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.03025071081*$e0] -e0 $e0 -tilt 0.9789002044 
Dipole -name "DIPOLE_539" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF9BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.4884584584*$e0] -e0 $e0 -tilt -0.5935711555 
Dipole -name "DIPOLE_540" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQF9BFF" -length 0 
Dipole -name "DIPOLE_541" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF9BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.03025071081*$e0] -e0 $e0 -tilt 0.9789002044 
Dipole -name "DIPOLE_542" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF9BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.4884584584*$e0] -e0 $e0 -tilt -0.5935711555 
Dipole -name "DIPOLE_543" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF9BFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1893249839*$e0] -e0 $e0 
Dipole -name "DIPOLE_544" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF9BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.03025071081*$e0] -e0 $e0 -tilt 0.9789002044 
Dipole -name "DIPOLE_545" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF9BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.4884584584*$e0] -e0 $e0 -tilt -0.5935711555 
Dipole -name "DIPOLE_546" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L209A" -length 0.019755 
Dipole -name "DIPOLE_547" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQF9BFF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
Dipole -name "DIPOLE_548" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L209B" -length 0.390960984 
Dipole -name "DIPOLE_549" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.9710366071*$e0] -e0 $e0 -tilt -0.5520476424 
Dipole -name "DIPOLE_550" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF6FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 80.25517652*$e0] -e0 $e0 -tilt -0.2727949621 
Dipole -name "DIPOLE_551" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF6FFMULT5" -length 0 -tilt -0.1902408885 
Dipole -name "DIPOLE_552" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF6FFMULT6" -length 0 -tilt 0.207694181 
Dipole -name "DIPOLE_553" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF6FFMULT7" -length 0 -tilt 0.01361356817 
Dipole -name "DIPOLE_554" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF6FFMULT8" -length 0 -tilt -0.0005235987756 
Dipole -name "DIPOLE_555" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF6FFMULT9" -length 0 -tilt 0.2935643802 
Dipole -name "DIPOLE_556" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF6FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr 4.8828277*$e0] -e0 $e0 
Dipole -name "DIPOLE_557" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.9710366071*$e0] -e0 $e0 -tilt -0.5520476424 
Dipole -name "DIPOLE_558" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF6FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 80.25517652*$e0] -e0 $e0 -tilt -0.2727949621 
Dipole -name "DIPOLE_559" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF6FFMULT5" -length 0 -tilt -0.1902408885 
Dipole -name "DIPOLE_560" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF6FFMULT6" -length 0 -tilt 0.207694181 
Dipole -name "DIPOLE_561" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF6FFMULT7" -length 0 -tilt 0.01361356817 
Dipole -name "DIPOLE_562" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF6FFMULT8" -length 0 -tilt -0.0005235987756 
Dipole -name "DIPOLE_563" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF6FFMULT9" -length 0 -tilt 0.2935643802 
Dipole -name "DIPOLE_564" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "ASF6FF" -length 0 
Dipole -name "DIPOLE_565" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.9710366071*$e0] -e0 $e0 -tilt -0.5520476424 
Dipole -name "DIPOLE_566" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF6FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 80.25517652*$e0] -e0 $e0 -tilt -0.2727949621 
Dipole -name "DIPOLE_567" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF6FFMULT5" -length 0 -tilt -0.1902408885 
Dipole -name "DIPOLE_568" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF6FFMULT6" -length 0 -tilt 0.207694181 
Dipole -name "DIPOLE_569" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF6FFMULT7" -length 0 -tilt 0.01361356817 
Dipole -name "DIPOLE_570" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF6FFMULT8" -length 0 -tilt -0.0005235987756 
Dipole -name "DIPOLE_571" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF6FFMULT9" -length 0 -tilt 0.2935643802 
Dipole -name "DIPOLE_572" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF6FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr 4.8828277*$e0] -e0 $e0 
Dipole -name "DIPOLE_573" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.9710366071*$e0] -e0 $e0 -tilt -0.5520476424 
Dipole -name "DIPOLE_574" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF6FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 80.25517652*$e0] -e0 $e0 -tilt -0.2727949621 
Dipole -name "DIPOLE_575" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF6FFMULT5" -length 0 -tilt -0.1902408885 
Dipole -name "DIPOLE_576" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF6FFMULT6" -length 0 -tilt 0.207694181 
Dipole -name "DIPOLE_577" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF6FFMULT7" -length 0 -tilt 0.01361356817 
Dipole -name "DIPOLE_578" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF6FFMULT8" -length 0 -tilt -0.0005235987756 
Dipole -name "DIPOLE_579" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF6FFMULT9" -length 0 -tilt 0.2935643802 
Dipole -name "DIPOLE_580" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L210A" -length 0.099791984 
Dipole -name "DIPOLE_581" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MSF6FF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
Dipole -name "DIPOLE_582" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L210B" -length 0.310924 
Dipole -name "DIPOLE_583" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF9AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02552335583*$e0] -e0 $e0 -tilt 0.297225583 
Dipole -name "DIPOLE_584" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF9AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*2.57883347*$e0] -e0 $e0 -tilt 0.7565372179 
Dipole -name "DIPOLE_585" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF9AFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1893249839*$e0] -e0 $e0 
Dipole -name "DIPOLE_586" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF9AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02552335583*$e0] -e0 $e0 -tilt 0.297225583 
Dipole -name "DIPOLE_587" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF9AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*2.57883347*$e0] -e0 $e0 -tilt 0.7565372179 
Dipole -name "DIPOLE_588" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQF9AFF" -length 0 
Dipole -name "DIPOLE_589" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF9AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02552335583*$e0] -e0 $e0 -tilt 0.297225583 
Dipole -name "DIPOLE_590" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF9AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*2.57883347*$e0] -e0 $e0 -tilt 0.7565372179 
Dipole -name "DIPOLE_591" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF9AFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1893249839*$e0] -e0 $e0 
Dipole -name "DIPOLE_592" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF9AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02552335583*$e0] -e0 $e0 -tilt 0.297225583 
Dipole -name "DIPOLE_593" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF9AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*2.57883347*$e0] -e0 $e0 -tilt 0.7565372179 
Dipole -name "DIPOLE_594" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L211A" -length 0.019755 
Dipole -name "DIPOLE_595" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQF9AFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_596" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L211B" -length 0.266 
Dipole -name "DIPOLE_597" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SK4FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Dipole -name "DIPOLE_598" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SK4FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Dipole -name "DIPOLE_599" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L211C" -length 1.085755 
Dipole -name "DIPOLE_600" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD8FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02084036463*$e0] -e0 $e0 -tilt 0.636230717 
Dipole -name "DIPOLE_601" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD8FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-6.229332836*$e0] -e0 $e0 -tilt 0.0402518337 
Dipole -name "DIPOLE_602" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD8FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.3021776993*$e0] -e0 $e0 
Dipole -name "DIPOLE_603" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD8FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02084036463*$e0] -e0 $e0 -tilt 0.636230717 
Dipole -name "DIPOLE_604" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD8FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-6.229332836*$e0] -e0 $e0 -tilt 0.0402518337 
Dipole -name "DIPOLE_605" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQD8FF" -length 0 
Dipole -name "DIPOLE_606" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD8FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02084036463*$e0] -e0 $e0 -tilt 0.636230717 
Dipole -name "DIPOLE_607" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD8FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-6.229332836*$e0] -e0 $e0 -tilt 0.0402518337 
Dipole -name "DIPOLE_608" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD8FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.3021776993*$e0] -e0 $e0 
Dipole -name "DIPOLE_609" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD8FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02084036463*$e0] -e0 $e0 -tilt 0.636230717 
Dipole -name "DIPOLE_610" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD8FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-6.229332836*$e0] -e0 $e0 -tilt 0.0402518337 
Dipole -name "DIPOLE_611" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L212A" -length 0.019755 
Dipole -name "DIPOLE_612" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQD8FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_613" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L212B" -length 1.681755 
Dipole -name "DIPOLE_614" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF7FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02376281692*$e0] -e0 $e0 -tilt 0.03989982269 
Dipole -name "DIPOLE_615" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF7FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.720636134*$e0] -e0 $e0 -tilt -0.7129998768 
Dipole -name "DIPOLE_616" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF7FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.2750809185*$e0] -e0 $e0 
Dipole -name "DIPOLE_617" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF7FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02376281692*$e0] -e0 $e0 -tilt 0.03989982269 
Dipole -name "DIPOLE_618" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF7FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.720636134*$e0] -e0 $e0 -tilt -0.7129998768 
Dipole -name "DIPOLE_619" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQF7FF" -length 0 
Dipole -name "DIPOLE_620" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF7FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02376281692*$e0] -e0 $e0 -tilt 0.03989982269 
Dipole -name "DIPOLE_621" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF7FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.720636134*$e0] -e0 $e0 -tilt -0.7129998768 
Dipole -name "DIPOLE_622" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF7FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.2750809185*$e0] -e0 $e0 
Dipole -name "DIPOLE_623" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF7FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02376281692*$e0] -e0 $e0 -tilt 0.03989982269 
Dipole -name "DIPOLE_624" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF7FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.720636134*$e0] -e0 $e0 -tilt -0.7129998768 
Dipole -name "DIPOLE_625" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L213A" -length 0.019755 
Dipole -name "DIPOLE_626" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQF7FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_627" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L213B" -length 0.4246097102 
Dipole -name "DIPOLE_628" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "B5FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.995837329e-05*$e0] -e0 $e0 
Dipole -name "DIPOLE_629" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B5FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.05117313872*$e0] -e0 $e0 -tilt 0.01137769317 
Dipole -name "DIPOLE_630" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6735950984*$e0] -e0 $e0 -tilt 0.7853981634 
Dipole -name "DIPOLE_631" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -868.1892379*$e0] -e0 $e0 
Dipole -name "DIPOLE_632" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "B5FFA" -synrad $sbend_synrad -length 0.3064317863 -angle -0.02494796661 -E1 -0.02494796661 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.01905 -fint 0.5 
Dipole -name "DIPOLE_633" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*-0.02494796661*-0.02494796661/0.3064317863*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_634" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_635" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "B5FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.995837329e-05*$e0] -e0 $e0 
Dipole -name "DIPOLE_636" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B5FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.05117313872*$e0] -e0 $e0 -tilt 0.01137769317 
Dipole -name "DIPOLE_637" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6735950984*$e0] -e0 $e0 -tilt 0.7853981634 
Dipole -name "DIPOLE_638" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -868.1892379*$e0] -e0 $e0 
Dipole -name "DIPOLE_639" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "B5FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.995837329e-05*$e0] -e0 $e0 
Dipole -name "DIPOLE_640" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B5FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.05117313872*$e0] -e0 $e0 -tilt 0.01137769317 
Dipole -name "DIPOLE_641" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6735950984*$e0] -e0 $e0 -tilt 0.7853981634 
Dipole -name "DIPOLE_642" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -868.1892379*$e0] -e0 $e0 
Dipole -name "DIPOLE_643" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "B5FFB" -synrad $sbend_synrad -length 0.3064317863 -angle -0.02494796661 -E1 0 -E2 -0.02494796661 -six_dim 1 -e0 $e0 -hgap 0.01905 -fintx 0.5 
Dipole -name "DIPOLE_644" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*-0.02494796661*-0.02494796661/0.3064317863*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_645" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_646" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "B5FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.995837329e-05*$e0] -e0 $e0 
Dipole -name "DIPOLE_647" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B5FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.05117313872*$e0] -e0 $e0 -tilt 0.01137769317 
Dipole -name "DIPOLE_648" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6735950984*$e0] -e0 $e0 -tilt 0.7853981634 
Dipole -name "DIPOLE_649" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -868.1892379*$e0] -e0 $e0 
Dipole -name "DIPOLE_650" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L214" -length 0.6443647102 
Dipole -name "DIPOLE_651" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD6FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04580065396*$e0] -e0 $e0 -tilt 0.862729249 
Dipole -name "DIPOLE_652" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-9.14694104*$e0] -e0 $e0 -tilt -0.008496430778 
Dipole -name "DIPOLE_653" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD6FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.301163606*$e0] -e0 $e0 
Dipole -name "DIPOLE_654" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD6FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04580065396*$e0] -e0 $e0 -tilt 0.862729249 
Dipole -name "DIPOLE_655" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-9.14694104*$e0] -e0 $e0 -tilt -0.008496430778 
Dipole -name "DIPOLE_656" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQD6FF" -length 0 
Dipole -name "DIPOLE_657" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD6FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04580065396*$e0] -e0 $e0 -tilt 0.862729249 
Dipole -name "DIPOLE_658" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-9.14694104*$e0] -e0 $e0 -tilt -0.008496430778 
Dipole -name "DIPOLE_659" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD6FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.301163606*$e0] -e0 $e0 
Dipole -name "DIPOLE_660" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD6FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04580065396*$e0] -e0 $e0 -tilt 0.862729249 
Dipole -name "DIPOLE_661" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-9.14694104*$e0] -e0 $e0 -tilt -0.008496430778 
Dipole -name "DIPOLE_662" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L215A" -length 0.019755 
Dipole -name "DIPOLE_663" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQD6FF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
Dipole -name "DIPOLE_664" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L215B" -length 1.066 
Dipole -name "DIPOLE_665" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SK3FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Dipole -name "DIPOLE_666" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SK3FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Dipole -name "DIPOLE_667" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L215C" -length 0.285755 
Dipole -name "DIPOLE_668" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF5BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.01745259381*$e0] -e0 $e0 -tilt 0.8505662066 
Dipole -name "DIPOLE_669" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF5BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.762728669*$e0] -e0 $e0 -tilt 0.01199774474 
Dipole -name "DIPOLE_670" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF5BFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1880283172*$e0] -e0 $e0 
Dipole -name "DIPOLE_671" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF5BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.01745259381*$e0] -e0 $e0 -tilt 0.8505662066 
Dipole -name "DIPOLE_672" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF5BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.762728669*$e0] -e0 $e0 -tilt 0.01199774474 
Dipole -name "DIPOLE_673" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQF5BFF" -length 0 
Dipole -name "DIPOLE_674" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF5BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.01745259381*$e0] -e0 $e0 -tilt 0.8505662066 
Dipole -name "DIPOLE_675" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF5BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.762728669*$e0] -e0 $e0 -tilt 0.01199774474 
Dipole -name "DIPOLE_676" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF5BFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1880283172*$e0] -e0 $e0 
Dipole -name "DIPOLE_677" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF5BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.01745259381*$e0] -e0 $e0 -tilt 0.8505662066 
Dipole -name "DIPOLE_678" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF5BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.762728669*$e0] -e0 $e0 -tilt 0.01199774474 
Dipole -name "DIPOLE_679" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L216A" -length 0.019755 
Dipole -name "DIPOLE_680" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQF5BFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_681" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L216B" -length 0.390960984 
Dipole -name "DIPOLE_682" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.002305870916*$e0] -e0 $e0 -tilt -0.04939281783 
Dipole -name "DIPOLE_683" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -10.79174071*$e0] -e0 $e0 -tilt 0.5820673055 
Dipole -name "DIPOLE_684" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF5FFMULT5" -length 0 -tilt 0.3670427417 
Dipole -name "DIPOLE_685" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF5FFMULT6" -length 0 -tilt -0.3497390488 
Dipole -name "DIPOLE_686" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF5FFMULT7" -length 0 -tilt -0.08360127117 
Dipole -name "DIPOLE_687" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF5FFMULT8" -length 0 -tilt 0.003839724354 
Dipole -name "DIPOLE_688" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF5FFMULT9" -length 0 -tilt 0.2176425577 
Dipole -name "DIPOLE_689" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF5FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr -0.3954341426*$e0] -e0 $e0 
Dipole -name "DIPOLE_690" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.002305870916*$e0] -e0 $e0 -tilt -0.04939281783 
Dipole -name "DIPOLE_691" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -10.79174071*$e0] -e0 $e0 -tilt 0.5820673055 
Dipole -name "DIPOLE_692" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF5FFMULT5" -length 0 -tilt 0.3670427417 
Dipole -name "DIPOLE_693" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF5FFMULT6" -length 0 -tilt -0.3497390488 
Dipole -name "DIPOLE_694" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF5FFMULT7" -length 0 -tilt -0.08360127117 
Dipole -name "DIPOLE_695" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF5FFMULT8" -length 0 -tilt 0.003839724354 
Dipole -name "DIPOLE_696" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF5FFMULT9" -length 0 -tilt 0.2176425577 
Dipole -name "DIPOLE_697" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "ASF5FF" -length 0 
Dipole -name "DIPOLE_698" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.002305870916*$e0] -e0 $e0 -tilt -0.04939281783 
Dipole -name "DIPOLE_699" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -10.79174071*$e0] -e0 $e0 -tilt 0.5820673055 
Dipole -name "DIPOLE_700" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF5FFMULT5" -length 0 -tilt 0.3670427417 
Dipole -name "DIPOLE_701" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF5FFMULT6" -length 0 -tilt -0.3497390488 
Dipole -name "DIPOLE_702" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF5FFMULT7" -length 0 -tilt -0.08360127117 
Dipole -name "DIPOLE_703" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF5FFMULT8" -length 0 -tilt 0.003839724354 
Dipole -name "DIPOLE_704" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF5FFMULT9" -length 0 -tilt 0.2176425577 
Dipole -name "DIPOLE_705" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF5FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr -0.3954341426*$e0] -e0 $e0 
Dipole -name "DIPOLE_706" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.002305870916*$e0] -e0 $e0 -tilt -0.04939281783 
Dipole -name "DIPOLE_707" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -10.79174071*$e0] -e0 $e0 -tilt 0.5820673055 
Dipole -name "DIPOLE_708" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF5FFMULT5" -length 0 -tilt 0.3670427417 
Dipole -name "DIPOLE_709" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF5FFMULT6" -length 0 -tilt -0.3497390488 
Dipole -name "DIPOLE_710" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF5FFMULT7" -length 0 -tilt -0.08360127117 
Dipole -name "DIPOLE_711" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF5FFMULT8" -length 0 -tilt 0.003839724354 
Dipole -name "DIPOLE_712" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF5FFMULT9" -length 0 -tilt 0.2176425577 
Dipole -name "DIPOLE_713" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L217A" -length 0.099791984 
Dipole -name "DIPOLE_714" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Bpm -name "MSF5FF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
Dipole -name "DIPOLE_715" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L217B" -length 0.310924 
Dipole -name "DIPOLE_716" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF5AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.007439676528*$e0] -e0 $e0 -tilt 0.3292327923 
Dipole -name "DIPOLE_717" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF5AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.246966194*$e0] -e0 $e0 -tilt 0.512545994 
Dipole -name "DIPOLE_718" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF5AFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1880283172*$e0] -e0 $e0 
Dipole -name "DIPOLE_719" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF5AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.007439676528*$e0] -e0 $e0 -tilt 0.3292327923 
Dipole -name "DIPOLE_720" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF5AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.246966194*$e0] -e0 $e0 -tilt 0.512545994 
Dipole -name "DIPOLE_721" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQF5AFF" -length 0 
Dipole -name "DIPOLE_722" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF5AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.007439676528*$e0] -e0 $e0 -tilt 0.3292327923 
Dipole -name "DIPOLE_723" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF5AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.246966194*$e0] -e0 $e0 -tilt 0.512545994 
Dipole -name "DIPOLE_724" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF5AFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1880283172*$e0] -e0 $e0 
Dipole -name "DIPOLE_725" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF5AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.007439676528*$e0] -e0 $e0 -tilt 0.3292327923 
Dipole -name "DIPOLE_726" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF5AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.246966194*$e0] -e0 $e0 -tilt 0.512545994 
Dipole -name "DIPOLE_727" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L218A" -length 0.019755 
Dipole -name "DIPOLE_728" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Bpm -name "MQF5AFF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
Dipole -name "DIPOLE_729" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L218B" -length 0.223 
Dipole -name "DIPOLE_730" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Drift -name "MREF2FF" -length 0  #REMOVED
Dipole -name "DIPOLE_731" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L218C" -length 0.858755 
Dipole -name "DIPOLE_732" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD4BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03539761723*$e0] -e0 $e0 -tilt -0.06709968635 
Dipole -name "DIPOLE_733" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD4BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.41320609*$e0] -e0 $e0 -tilt -0.1275036164 
Dipole -name "DIPOLE_734" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD4BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1483961368*$e0] -e0 $e0 
Dipole -name "DIPOLE_735" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD4BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03539761723*$e0] -e0 $e0 -tilt -0.06709968635 
Dipole -name "DIPOLE_736" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD4BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.41320609*$e0] -e0 $e0 -tilt -0.1275036164 
Dipole -name "DIPOLE_737" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQD4BFF" -length 0 
Dipole -name "DIPOLE_738" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD4BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03539761723*$e0] -e0 $e0 -tilt -0.06709968635 
Dipole -name "DIPOLE_739" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD4BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.41320609*$e0] -e0 $e0 -tilt -0.1275036164 
Dipole -name "DIPOLE_740" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD4BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1483961368*$e0] -e0 $e0 
Dipole -name "DIPOLE_741" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD4BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03539761723*$e0] -e0 $e0 -tilt -0.06709968635 
Dipole -name "DIPOLE_742" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD4BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.41320609*$e0] -e0 $e0 -tilt -0.1275036164 
Dipole -name "DIPOLE_743" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L219A" -length 0.019755 
Dipole -name "DIPOLE_744" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQD4BFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_745" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L219B" -length 0.390960984 
Dipole -name "DIPOLE_746" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SD4FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.441469053*$e0] -e0 $e0 -tilt -0.5440191278 
Dipole -name "DIPOLE_747" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SD4FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 34.2076517*$e0] -e0 $e0 -tilt -0.3703588673 
Dipole -name "DIPOLE_748" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD4FFMULT5" -length 0 -tilt -0.05707226654 
Dipole -name "DIPOLE_749" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD4FFMULT6" -length 0 -tilt -0.323733643 
Dipole -name "DIPOLE_750" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD4FFMULT7" -length 0 -tilt -0.347669587 
Dipole -name "DIPOLE_751" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD4FFMULT8" -length 0 -tilt -0.001919862177 
Dipole -name "DIPOLE_752" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD4FFMULT9" -length 0 -tilt 0.02408554368 
Dipole -name "DIPOLE_753" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SD4FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr 7.817968185*$e0] -e0 $e0 
Dipole -name "DIPOLE_754" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SD4FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.441469053*$e0] -e0 $e0 -tilt -0.5440191278 
Dipole -name "DIPOLE_755" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SD4FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 34.2076517*$e0] -e0 $e0 -tilt -0.3703588673 
Dipole -name "DIPOLE_756" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD4FFMULT5" -length 0 -tilt -0.05707226654 
Dipole -name "DIPOLE_757" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD4FFMULT6" -length 0 -tilt -0.323733643 
Dipole -name "DIPOLE_758" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD4FFMULT7" -length 0 -tilt -0.347669587 
Dipole -name "DIPOLE_759" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD4FFMULT8" -length 0 -tilt -0.001919862177 
Dipole -name "DIPOLE_760" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD4FFMULT9" -length 0 -tilt 0.02408554368 
Dipole -name "DIPOLE_761" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "ASD4FF" -length 0 
Dipole -name "DIPOLE_762" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SD4FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.441469053*$e0] -e0 $e0 -tilt -0.5440191278 
Dipole -name "DIPOLE_763" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SD4FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 34.2076517*$e0] -e0 $e0 -tilt -0.3703588673 
Dipole -name "DIPOLE_764" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD4FFMULT5" -length 0 -tilt -0.05707226654 
Dipole -name "DIPOLE_765" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD4FFMULT6" -length 0 -tilt -0.323733643 
Dipole -name "DIPOLE_766" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD4FFMULT7" -length 0 -tilt -0.347669587 
Dipole -name "DIPOLE_767" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD4FFMULT8" -length 0 -tilt -0.001919862177 
Dipole -name "DIPOLE_768" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD4FFMULT9" -length 0 -tilt 0.02408554368 
Dipole -name "DIPOLE_769" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SD4FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr 7.817968185*$e0] -e0 $e0 
Dipole -name "DIPOLE_770" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SD4FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.441469053*$e0] -e0 $e0 -tilt -0.5440191278 
Dipole -name "DIPOLE_771" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SD4FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 34.2076517*$e0] -e0 $e0 -tilt -0.3703588673 
Dipole -name "DIPOLE_772" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD4FFMULT5" -length 0 -tilt -0.05707226654 
Dipole -name "DIPOLE_773" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD4FFMULT6" -length 0 -tilt -0.323733643 
Dipole -name "DIPOLE_774" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD4FFMULT7" -length 0 -tilt -0.347669587 
Dipole -name "DIPOLE_775" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD4FFMULT8" -length 0 -tilt -0.001919862177 
Dipole -name "DIPOLE_776" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD4FFMULT9" -length 0 -tilt 0.02408554368 
Dipole -name "DIPOLE_777" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L220A" -length 0.099791984 
Dipole -name "DIPOLE_778" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MSD4FF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
Dipole -name "DIPOLE_779" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L220B" -length 0.310924 
Dipole -name "DIPOLE_780" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD4AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03253817197*$e0] -e0 $e0 -tilt -0.2852693742 
Dipole -name "DIPOLE_781" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD4AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1914310165*$e0] -e0 $e0 -tilt 0.1117542814 
Dipole -name "DIPOLE_782" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD4AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1483961368*$e0] -e0 $e0 
Dipole -name "DIPOLE_783" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD4AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03253817197*$e0] -e0 $e0 -tilt -0.2852693742 
Dipole -name "DIPOLE_784" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD4AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1914310165*$e0] -e0 $e0 -tilt 0.1117542814 
Dipole -name "DIPOLE_785" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQD4AFF" -length 0 
Dipole -name "DIPOLE_786" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD4AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03253817197*$e0] -e0 $e0 -tilt -0.2852693742 
Dipole -name "DIPOLE_787" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD4AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1914310165*$e0] -e0 $e0 -tilt 0.1117542814 
Dipole -name "DIPOLE_788" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD4AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1483961368*$e0] -e0 $e0 
Dipole -name "DIPOLE_789" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD4AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03253817197*$e0] -e0 $e0 -tilt -0.2852693742 
Dipole -name "DIPOLE_790" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD4AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1914310165*$e0] -e0 $e0 -tilt 0.1117542814 
Dipole -name "DIPOLE_791" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L221A" -length 0.019755 
Dipole -name "DIPOLE_792" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQD4AFF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
Dipole -name "DIPOLE_793" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L221B" -length 0.266 
Dipole -name "DIPOLE_794" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SK2FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Dipole -name "DIPOLE_795" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SK2FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Dipole -name "DIPOLE_796" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L221C" -length 1.028629871 
Dipole -name "DIPOLE_797" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "B2FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.508793732e-05*$e0] -e0 $e0 
Dipole -name "DIPOLE_798" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B2FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00495539408*$e0] -e0 $e0 -tilt 0.2883807683 
Dipole -name "DIPOLE_799" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B2FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.2263190599*$e0] -e0 $e0 
Dipole -name "DIPOLE_800" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B2FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -434.5325949*$e0] -e0 $e0 
Dipole -name "DIPOLE_801" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "B2FFA" -synrad $sbend_synrad -length 0.3064116254 -angle -0.01508793732 -E1 -0.01508793732 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.01905 -fint 0.5 
Dipole -name "DIPOLE_802" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*-0.01508793732*-0.01508793732/0.3064116254*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_803" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_804" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "B2FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.508793732e-05*$e0] -e0 $e0 
Dipole -name "DIPOLE_805" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B2FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00495539408*$e0] -e0 $e0 -tilt 0.2883807683 
Dipole -name "DIPOLE_806" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B2FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.2263190599*$e0] -e0 $e0 
Dipole -name "DIPOLE_807" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B2FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -434.5325949*$e0] -e0 $e0 
Dipole -name "DIPOLE_808" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "B2FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.508793732e-05*$e0] -e0 $e0 
Dipole -name "DIPOLE_809" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B2FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00495539408*$e0] -e0 $e0 -tilt 0.2883807683 
Dipole -name "DIPOLE_810" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B2FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.2263190599*$e0] -e0 $e0 
Dipole -name "DIPOLE_811" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B2FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -434.5325949*$e0] -e0 $e0 
Dipole -name "DIPOLE_812" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "B2FFB" -synrad $sbend_synrad -length 0.3064116254 -angle -0.01508793732 -E1 0 -E2 -0.01508793732 -six_dim 1 -e0 $e0 -hgap 0.01905 -fintx 0.5 
Dipole -name "DIPOLE_813" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*-0.01508793732*-0.01508793732/0.3064116254*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_814" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_815" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "B2FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.508793732e-05*$e0] -e0 $e0 
Dipole -name "DIPOLE_816" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B2FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00495539408*$e0] -e0 $e0 -tilt 0.2883807683 
Dipole -name "DIPOLE_817" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B2FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.2263190599*$e0] -e0 $e0 
Dipole -name "DIPOLE_818" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B2FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -434.5325949*$e0] -e0 $e0 
Dipole -name "DIPOLE_819" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L222" -length 0.444384871 
Dipole -name "DIPOLE_820" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF3FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0432486468*$e0] -e0 $e0 -tilt -0.6574387986 
Dipole -name "DIPOLE_821" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF3FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.770240438*$e0] -e0 $e0 -tilt 0.04499189406 
Dipole -name "DIPOLE_822" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF3FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.2763599368*$e0] -e0 $e0 
Dipole -name "DIPOLE_823" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF3FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0432486468*$e0] -e0 $e0 -tilt -0.6574387986 
Dipole -name "DIPOLE_824" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF3FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.770240438*$e0] -e0 $e0 -tilt 0.04499189406 
Dipole -name "DIPOLE_825" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQF3FF" -length 0 
Dipole -name "DIPOLE_826" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF3FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0432486468*$e0] -e0 $e0 -tilt -0.6574387986 
Dipole -name "DIPOLE_827" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF3FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.770240438*$e0] -e0 $e0 -tilt 0.04499189406 
Dipole -name "DIPOLE_828" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF3FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.2763599368*$e0] -e0 $e0 
Dipole -name "DIPOLE_829" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF3FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0432486468*$e0] -e0 $e0 -tilt -0.6574387986 
Dipole -name "DIPOLE_830" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF3FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.770240438*$e0] -e0 $e0 -tilt 0.04499189406 
Dipole -name "DIPOLE_831" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L223A" -length 0.019755 
Dipole -name "DIPOLE_832" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQF3FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_833" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L223B" -length 0.4246045352 
Dipole -name "DIPOLE_834" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "B1FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -3.093740393e-05*$e0] -e0 $e0 
Dipole -name "DIPOLE_835" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04862213291*$e0] -e0 $e0 -tilt 0.04625572479 
Dipole -name "DIPOLE_836" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.08070627111*$e0] -e0 $e0 
Dipole -name "DIPOLE_837" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1097.605287*$e0] -e0 $e0 
Dipole -name "DIPOLE_838" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "B1FFA" -synrad $sbend_synrad -length 0.3064369612 -angle -0.02690209037 -E1 -0.02690209037 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.01905 -fint 0.5 
Dipole -name "DIPOLE_839" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*-0.02690209037*-0.02690209037/0.3064369612*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_840" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_841" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "B1FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -3.093740393e-05*$e0] -e0 $e0 
Dipole -name "DIPOLE_842" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04862213291*$e0] -e0 $e0 -tilt 0.04625572479 
Dipole -name "DIPOLE_843" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.08070627111*$e0] -e0 $e0 
Dipole -name "DIPOLE_844" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1097.605287*$e0] -e0 $e0 
Dipole -name "DIPOLE_845" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "B1FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -3.093740393e-05*$e0] -e0 $e0 
Dipole -name "DIPOLE_846" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04862213291*$e0] -e0 $e0 -tilt 0.04625572479 
Dipole -name "DIPOLE_847" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.08070627111*$e0] -e0 $e0 
Dipole -name "DIPOLE_848" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1097.605287*$e0] -e0 $e0 
Dipole -name "DIPOLE_849" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Sbend -name "B1FFB" -synrad $sbend_synrad -length 0.3064369612 -angle -0.02690209037 -E1 0 -E2 -0.02690209037 -six_dim 1 -e0 $e0 -hgap 0.01905 -fintx 0.5 
Dipole -name "DIPOLE_850" -length 0.0 -strength_x 0.0 -strength_y 0.0 
set e0 [expr $e0-14.1e-6*-0.02690209037*-0.02690209037/0.3064369612*$e0*$e0*$e0*$e0*$sbend_synrad]
Dipole -name "DIPOLE_851" -length 0.0 -strength_x 0.0 -strength_y 0.0 
SetReferenceEnergy $e0
Dipole -name "DIPOLE_852" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "B1FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -3.093740393e-05*$e0] -e0 $e0 
Dipole -name "DIPOLE_853" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04862213291*$e0] -e0 $e0 -tilt 0.04625572479 
Dipole -name "DIPOLE_854" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.08070627111*$e0] -e0 $e0 
Dipole -name "DIPOLE_855" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "B1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1097.605287*$e0] -e0 $e0 
Dipole -name "DIPOLE_856" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L224A" -length 0.2406045352 
Dipole -name "DIPOLE_857" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "MREF1FF" -length 0 
Dipole -name "DIPOLE_858" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L224B" -length 0.203755 
Dipole -name "DIPOLE_859" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD2BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01829512447*$e0] -e0 $e0 -tilt 0.006571980611 
Dipole -name "DIPOLE_860" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD2BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.488201167*$e0] -e0 $e0 -tilt 0.6925477838 
Dipole -name "DIPOLE_861" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD2BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.09935648446*$e0] -e0 $e0 
Dipole -name "DIPOLE_862" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD2BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01829512447*$e0] -e0 $e0 -tilt 0.006571980611 
Dipole -name "DIPOLE_863" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD2BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.488201167*$e0] -e0 $e0 -tilt 0.6925477838 
Dipole -name "DIPOLE_864" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQD2BFF" -length 0 
Dipole -name "DIPOLE_865" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD2BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01829512447*$e0] -e0 $e0 -tilt 0.006571980611 
Dipole -name "DIPOLE_866" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD2BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.488201167*$e0] -e0 $e0 -tilt 0.6925477838 
Dipole -name "DIPOLE_867" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD2BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.09935648446*$e0] -e0 $e0 
Dipole -name "DIPOLE_868" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD2BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01829512447*$e0] -e0 $e0 -tilt 0.006571980611 
Dipole -name "DIPOLE_869" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD2BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.488201167*$e0] -e0 $e0 -tilt 0.6925477838 
Dipole -name "DIPOLE_870" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L225A" -length 0.019755 
Dipole -name "DIPOLE_871" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQD2BFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_872" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L225B" -length 0.354 
Dipole -name "DIPOLE_873" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "MT1X" -length 0 
Dipole -name "DIPOLE_874" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L225C" -length 0.137 
Dipole -name "DIPOLE_875" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "FFML1" -length 0 
Dipole -name "DIPOLE_876" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L225D" -length 0.7235 
Dipole -name "DIPOLE_877" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "MREFIPFF" -length 0 
Dipole -name "DIPOLE_878" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L225E" -length 0.267255 
Dipole -name "DIPOLE_879" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD2AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.007820933842*$e0] -e0 $e0 -tilt 0.4505632088 
Dipole -name "DIPOLE_880" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD2AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-3.237581097*$e0] -e0 $e0 -tilt -0.009734334147 
Dipole -name "DIPOLE_881" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD2AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1448640481*$e0] -e0 $e0 
Dipole -name "DIPOLE_882" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD2AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.007820933842*$e0] -e0 $e0 -tilt 0.4505632088 
Dipole -name "DIPOLE_883" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD2AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-3.237581097*$e0] -e0 $e0 -tilt -0.009734334147 
Dipole -name "DIPOLE_884" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQD2AFF" -length 0 
Dipole -name "DIPOLE_885" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD2AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.007820933842*$e0] -e0 $e0 -tilt 0.4505632088 
Dipole -name "DIPOLE_886" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD2AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-3.237581097*$e0] -e0 $e0 -tilt -0.009734334147 
Dipole -name "DIPOLE_887" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD2AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1448640481*$e0] -e0 $e0 
Dipole -name "DIPOLE_888" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD2AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.007820933842*$e0] -e0 $e0 -tilt 0.4505632088 
Dipole -name "DIPOLE_889" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD2AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-3.237581097*$e0] -e0 $e0 -tilt -0.009734334147 
Dipole -name "DIPOLE_890" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L226A" -length 0.019755 
Dipole -name "DIPOLE_891" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQD2AFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Dipole -name "DIPOLE_892" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L226B" -length 0.391 
Dipole -name "DIPOLE_893" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "MREFSFF" -length 0 
Dipole -name "DIPOLE_894" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L226C" -length 3.825 
Dipole -name "DIPOLE_895" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SK1FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Dipole -name "DIPOLE_896" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SK1FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Dipole -name "DIPOLE_897" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L226D" -length 0.161 
Dipole -name "DIPOLE_898" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MSF1FF" -length 0 -resolution $sband -short_range_wake $sband_wake
Dipole -name "DIPOLE_899" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L226E" -length 0.099 
Dipole -name "DIPOLE_900" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1499119875*$e0] -e0 $e0 -tilt -0.4398229715 
Dipole -name "DIPOLE_901" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -64.19233541*$e0] -e0 $e0 -tilt 0.6283185307 
Dipole -name "DIPOLE_902" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF1FFMULT5" -length 0 -tilt -0.2705260341 
Dipole -name "DIPOLE_903" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF1FFMULT6" -length 0 -tilt 0.8459860217 
Dipole -name "DIPOLE_904" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF1FFMULT7" -length 0 -tilt 4.196644186 
Dipole -name "DIPOLE_905" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF1FFMULT8" -length 0 -tilt 3.380702761 
Dipole -name "DIPOLE_906" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF1FFMULT9" -length 0 -tilt 1.082104136 
Dipole -name "DIPOLE_907" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF1FF" -synrad $mult_synrad -type 3 -length 0.05 -strength [expr -1.505168272*$e0] -e0 $e0 
Dipole -name "DIPOLE_908" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1499119875*$e0] -e0 $e0 -tilt -0.4398229715 
Dipole -name "DIPOLE_909" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -64.19233541*$e0] -e0 $e0 -tilt 0.6283185307 
Dipole -name "DIPOLE_910" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF1FFMULT5" -length 0 -tilt -0.2705260341 
Dipole -name "DIPOLE_911" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF1FFMULT6" -length 0 -tilt 0.8459860217 
Dipole -name "DIPOLE_912" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF1FFMULT7" -length 0 -tilt 4.196644186 
Dipole -name "DIPOLE_913" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF1FFMULT8" -length 0 -tilt 3.380702761 
Dipole -name "DIPOLE_914" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF1FFMULT9" -length 0 -tilt 1.082104136 
Dipole -name "DIPOLE_915" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "ASF1FF" -length 0 
Dipole -name "DIPOLE_916" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1499119875*$e0] -e0 $e0 -tilt -0.4398229715 
Dipole -name "DIPOLE_917" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -64.19233541*$e0] -e0 $e0 -tilt 0.6283185307 
Dipole -name "DIPOLE_918" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF1FFMULT5" -length 0 -tilt -0.2705260341 
Dipole -name "DIPOLE_919" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF1FFMULT6" -length 0 -tilt 0.8459860217 
Dipole -name "DIPOLE_920" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF1FFMULT7" -length 0 -tilt 4.196644186 
Dipole -name "DIPOLE_921" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF1FFMULT8" -length 0 -tilt 3.380702761 
Dipole -name "DIPOLE_922" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF1FFMULT9" -length 0 -tilt 1.082104136 
Dipole -name "DIPOLE_923" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF1FF" -synrad $mult_synrad -type 3 -length 0.05 -strength [expr -1.505168272*$e0] -e0 $e0 
Dipole -name "DIPOLE_924" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1499119875*$e0] -e0 $e0 -tilt -0.4398229715 
Dipole -name "DIPOLE_925" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -64.19233541*$e0] -e0 $e0 -tilt 0.6283185307 
Dipole -name "DIPOLE_926" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF1FFMULT5" -length 0 -tilt -0.2705260341 
Dipole -name "DIPOLE_927" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF1FFMULT6" -length 0 -tilt 0.8459860217 
Dipole -name "DIPOLE_928" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF1FFMULT7" -length 0 -tilt 4.196644186 
Dipole -name "DIPOLE_929" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF1FFMULT8" -length 0 -tilt 3.380702761 
Dipole -name "DIPOLE_930" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SF1FFMULT9" -length 0 -tilt 1.082104136 
Dipole -name "DIPOLE_931" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L227" -length 0.30295 
Dipole -name "DIPOLE_932" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0004336394638*$e0] -e0 $e0 
Dipole -name "DIPOLE_933" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.02131894177*$e0] -e0 $e0 
Dipole -name "DIPOLE_934" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -0.6484970075*$e0] -e0 $e0 
Dipole -name "DIPOLE_935" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT5" -length 0 
Dipole -name "DIPOLE_936" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT6" -length 0 
Dipole -name "DIPOLE_937" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT7" -length 0 
Dipole -name "DIPOLE_938" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT8" -length 0 
Dipole -name "DIPOLE_939" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT9" -length 0 
Dipole -name "DIPOLE_940" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT2S" -length 0 
Dipole -name "DIPOLE_941" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT3S" -length 0 
Dipole -name "DIPOLE_942" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT4S" -length 0 
Dipole -name "DIPOLE_943" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT5S" -length 0 
Dipole -name "DIPOLE_944" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT6S" -length 0 
Dipole -name "DIPOLE_945" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT7S" -length 0 
Dipole -name "DIPOLE_946" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT8S" -length 0 
Dipole -name "DIPOLE_947" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT9S" -length 0 
Dipole -name "DIPOLE_948" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF1FF" -synrad $quad_synrad -length 0.22205 -strength [expr 0.369493027*$e0] -e0 $e0 
Dipole -name "DIPOLE_949" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0004336394638*$e0] -e0 $e0 
Dipole -name "DIPOLE_950" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.02131894177*$e0] -e0 $e0 
Dipole -name "DIPOLE_951" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -0.6484970075*$e0] -e0 $e0 
Dipole -name "DIPOLE_952" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT5" -length 0 
Dipole -name "DIPOLE_953" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT6" -length 0 
Dipole -name "DIPOLE_954" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT7" -length 0 
Dipole -name "DIPOLE_955" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT8" -length 0 
Dipole -name "DIPOLE_956" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT9" -length 0 
Dipole -name "DIPOLE_957" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT2S" -length 0 
Dipole -name "DIPOLE_958" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT3S" -length 0 
Dipole -name "DIPOLE_959" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT4S" -length 0 
Dipole -name "DIPOLE_960" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT5S" -length 0 
Dipole -name "DIPOLE_961" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT6S" -length 0 
Dipole -name "DIPOLE_962" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT7S" -length 0 
Dipole -name "DIPOLE_963" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT8S" -length 0 
Dipole -name "DIPOLE_964" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT9S" -length 0 
Dipole -name "DIPOLE_965" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQF1FF" -length 0 
Dipole -name "DIPOLE_966" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0004336394638*$e0] -e0 $e0 
Dipole -name "DIPOLE_967" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.02131894177*$e0] -e0 $e0 
Dipole -name "DIPOLE_968" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -0.6484970075*$e0] -e0 $e0 
Dipole -name "DIPOLE_969" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT5" -length 0 
Dipole -name "DIPOLE_970" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT6" -length 0 
Dipole -name "DIPOLE_971" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT7" -length 0 
Dipole -name "DIPOLE_972" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT8" -length 0 
Dipole -name "DIPOLE_973" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT9" -length 0 
Dipole -name "DIPOLE_974" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT2S" -length 0 
Dipole -name "DIPOLE_975" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT3S" -length 0 
Dipole -name "DIPOLE_976" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT4S" -length 0 
Dipole -name "DIPOLE_977" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT5S" -length 0 
Dipole -name "DIPOLE_978" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT6S" -length 0 
Dipole -name "DIPOLE_979" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT7S" -length 0 
Dipole -name "DIPOLE_980" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT8S" -length 0 
Dipole -name "DIPOLE_981" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT9S" -length 0 
Dipole -name "DIPOLE_982" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QF1FF" -synrad $quad_synrad -length 0.22205 -strength [expr 0.369493027*$e0] -e0 $e0 
Dipole -name "DIPOLE_983" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0004336394638*$e0] -e0 $e0 
Dipole -name "DIPOLE_984" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.02131894177*$e0] -e0 $e0 
Dipole -name "DIPOLE_985" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -0.6484970075*$e0] -e0 $e0 
Dipole -name "DIPOLE_986" -length 0.0 -strength_x 0.0 -strength_y 0.0 

Dipole -name "DIPOLE_987" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT5" -length 0 
Dipole -name "DIPOLE_988" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT6" -length 0 
Dipole -name "DIPOLE_989" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT7" -length 0 
Dipole -name "DIPOLE_990" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT8" -length 0 
Dipole -name "DIPOLE_991" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT9" -length 0 
Dipole -name "DIPOLE_992" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT2S" -length 0 
Dipole -name "DIPOLE_993" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT3S" -length 0 
Dipole -name "DIPOLE_994" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT4S" -length 0 
Dipole -name "DIPOLE_995" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT5S" -length 0 
Dipole -name "DIPOLE_996" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT6S" -length 0 
Dipole -name "DIPOLE_997" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT7S" -length 0 
Dipole -name "DIPOLE_998" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT8S" -length 0 
Dipole -name "DIPOLE_999" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QF1FFMULT9S" -length 0 
Dipole -name "DIPOLE_1000" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L228A" -length 0.12195 
Dipole -name "DIPOLE_1001" -length 0.0 -strength_x 0.0 -strength_y 0.0 
# not present anymore
Dipole -name "DIPOLE_1002" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Bpm -name "MQF1FF" -length 0 
Dipole -name "DIPOLE_1003" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L228B" -length 0.297 
Dipole -name "DIPOLE_1004" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Bpm -name "MSD0FF" -length 0 -resolution $sband -short_range_wake $sband_wake
Dipole -name "DIPOLE_1005" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L228C" -length 0.099 
Dipole -name "DIPOLE_1006" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2963503459*$e0] -e0 $e0 -tilt 0.07679448709 
Dipole -name "DIPOLE_1007" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 78.90731241*$e0] -e0 $e0 -tilt 0.6283185307 
Dipole -name "DIPOLE_1008" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD0FFMULT5" -length 0 -tilt 0.2391101075 
Dipole -name "DIPOLE_1009" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD0FFMULT6" -length 0 -tilt 0.5667333414 
Dipole -name "DIPOLE_1010" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD0FFMULT7" -length 0 -tilt -0.3447025273 
Dipole -name "DIPOLE_1011" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD0FFMULT8" -length 0 -tilt -0.2862339973 
Dipole -name "DIPOLE_1012" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD0FFMULT9" -length 0 -tilt 0.2513274123 
Dipole -name "DIPOLE_1013" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SD0FF" -synrad $mult_synrad -type 3 -length 0.05 -strength [expr 2.250119161*$e0] -e0 $e0 
Dipole -name "DIPOLE_1014" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2963503459*$e0] -e0 $e0 -tilt 0.07679448709 
Dipole -name "DIPOLE_1015" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 78.90731241*$e0] -e0 $e0 -tilt 0.6283185307 
Dipole -name "DIPOLE_1016" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD0FFMULT5" -length 0 -tilt 0.2391101075 
Dipole -name "DIPOLE_1017" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD0FFMULT6" -length 0 -tilt 0.5667333414 
Dipole -name "DIPOLE_1018" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD0FFMULT7" -length 0 -tilt -0.3447025273 
Dipole -name "DIPOLE_1019" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD0FFMULT8" -length 0 -tilt -0.2862339973 
Dipole -name "DIPOLE_1020" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD0FFMULT9" -length 0 -tilt 0.2513274123 
Dipole -name "DIPOLE_1021" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "ASD0FF" -length 0 
Dipole -name "DIPOLE_1022" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2963503459*$e0] -e0 $e0 -tilt 0.07679448709 
Dipole -name "DIPOLE_1023" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 78.90731241*$e0] -e0 $e0 -tilt 0.6283185307 
Dipole -name "DIPOLE_1024" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD0FFMULT5" -length 0 -tilt 0.2391101075 
Dipole -name "DIPOLE_1025" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD0FFMULT6" -length 0 -tilt 0.5667333414 
Dipole -name "DIPOLE_1026" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD0FFMULT7" -length 0 -tilt -0.3447025273 
Dipole -name "DIPOLE_1027" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD0FFMULT8" -length 0 -tilt -0.2862339973 
Dipole -name "DIPOLE_1028" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD0FFMULT9" -length 0 -tilt 0.2513274123 
Dipole -name "DIPOLE_1029" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SD0FF" -synrad $mult_synrad -type 3 -length 0.05 -strength [expr 2.250119161*$e0] -e0 $e0 
Dipole -name "DIPOLE_1030" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2963503459*$e0] -e0 $e0 -tilt 0.07679448709 
Dipole -name "DIPOLE_1031" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "SD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 78.90731241*$e0] -e0 $e0 -tilt 0.6283185307 
Dipole -name "DIPOLE_1032" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD0FFMULT5" -length 0 -tilt 0.2391101075 
Dipole -name "DIPOLE_1033" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD0FFMULT6" -length 0 -tilt 0.5667333414 
Dipole -name "DIPOLE_1034" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD0FFMULT7" -length 0 -tilt -0.3447025273 
Dipole -name "DIPOLE_1035" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD0FFMULT8" -length 0 -tilt -0.2862339973 
Dipole -name "DIPOLE_1036" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "SD0FFMULT9" -length 0 -tilt 0.2513274123 
Dipole -name "DIPOLE_1037" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L229" -length 0.2875 
Dipole -name "DIPOLE_1038" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD0FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01735176498*$e0] -e0 $e0 -tilt 0.2541199391 
Dipole -name "DIPOLE_1039" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.072664521*$e0] -e0 $e0 -tilt 0.641757566 
Dipole -name "DIPOLE_1040" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -585.4643194*$e0] -e0 $e0 -tilt 0.5410520681 
Dipole -name "DIPOLE_1041" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QD0FFMULT5" -length 0 -tilt 0.5134758659 
Dipole -name "DIPOLE_1042" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QD0FFMULT6" -length 0 -tilt 0.1261374384 
Dipole -name "DIPOLE_1043" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QD0FFMULT7" -length 0 -tilt 0.3644247478 
Dipole -name "DIPOLE_1044" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QD0FFMULT8" -length 0 -tilt -0.01239183769 
Dipole -name "DIPOLE_1045" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QD0FFMULT9" -length 0 -tilt -0.001047197551 
Dipole -name "DIPOLE_1046" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD0FF" -synrad $quad_synrad -length 0.2375 -strength [expr -0.6811853675*$e0] -e0 $e0 
Dipole -name "DIPOLE_1047" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD0FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01735176498*$e0] -e0 $e0 -tilt 0.2541199391 
Dipole -name "DIPOLE_1048" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.072664521*$e0] -e0 $e0 -tilt 0.641757566 
Dipole -name "DIPOLE_1049" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -585.4643194*$e0] -e0 $e0 -tilt 0.5410520681 
Dipole -name "DIPOLE_1050" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QD0FFMULT5" -length 0 -tilt 0.5134758659 
Dipole -name "DIPOLE_1051" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QD0FFMULT6" -length 0 -tilt 0.1261374384 
Dipole -name "DIPOLE_1052" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QD0FFMULT7" -length 0 -tilt 0.3644247478 
Dipole -name "DIPOLE_1053" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QD0FFMULT8" -length 0 -tilt -0.01239183769 
Dipole -name "DIPOLE_1054" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QD0FFMULT9" -length 0 -tilt -0.001047197551 
Dipole -name "DIPOLE_1055" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "AQD0FF" -length 0 
Dipole -name "DIPOLE_1056" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD0FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01735176498*$e0] -e0 $e0 -tilt 0.2541199391 
Dipole -name "DIPOLE_1057" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.072664521*$e0] -e0 $e0 -tilt 0.641757566 
Dipole -name "DIPOLE_1058" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -585.4643194*$e0] -e0 $e0 -tilt 0.5410520681 
Dipole -name "DIPOLE_1059" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QD0FFMULT5" -length 0 -tilt 0.5134758659 
Dipole -name "DIPOLE_1060" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QD0FFMULT6" -length 0 -tilt 0.1261374384 
Dipole -name "DIPOLE_1061" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QD0FFMULT7" -length 0 -tilt 0.3644247478 
Dipole -name "DIPOLE_1062" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QD0FFMULT8" -length 0 -tilt -0.01239183769 
Dipole -name "DIPOLE_1063" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QD0FFMULT9" -length 0 -tilt -0.001047197551 
Dipole -name "DIPOLE_1064" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "QD0FF" -synrad $quad_synrad -length 0.2375 -strength [expr -0.6811853675*$e0] -e0 $e0 
Dipole -name "DIPOLE_1065" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD0FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01735176498*$e0] -e0 $e0 -tilt 0.2541199391 
Dipole -name "DIPOLE_1066" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.072664521*$e0] -e0 $e0 -tilt 0.641757566 
Dipole -name "DIPOLE_1067" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Multipole -name "QD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -585.4643194*$e0] -e0 $e0 -tilt 0.5410520681 
Dipole -name "DIPOLE_1068" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QD0FFMULT5" -length 0 -tilt 0.5134758659 
Dipole -name "DIPOLE_1069" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QD0FFMULT6" -length 0 -tilt 0.1261374384 
Dipole -name "DIPOLE_1070" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QD0FFMULT7" -length 0 -tilt 0.3644247478 
Dipole -name "DIPOLE_1071" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QD0FFMULT8" -length 0 -tilt -0.01239183769 
Dipole -name "DIPOLE_1072" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "QD0FFMULT9" -length 0 -tilt -0.001047197551 
Dipole -name "DIPOLE_1073" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L230A" -length 0.1065 
Dipole -name "DIPOLE_1074" -length 0.0 -strength_x 0.0 -strength_y 0.0 
# not present anymore
Dipole -name "DIPOLE_1075" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Bpm -name "MQD0FF" -length 0 
Dipole -name "DIPOLE_1076" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L230B" -length 0.286 
Dipole -name "DIPOLE_1077" -length 0.0 -strength_x 0.0 -strength_y 0.0 
VCORRECTOR -name "IPKICK" -length 0.12 
Dipole -name "DIPOLE_1078" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L230C" -length 0.048 
Dipole -name "DIPOLE_1079" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Bpm -name "MPREIP" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
Dipole -name "DIPOLE_1080" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L230D" -length 0.2758 
Dipole -name "DIPOLE_1081" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Bpm -name "IPBPMA" -length 0 -resolution $ipbpm
Dipole -name "DIPOLE_1082" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L230E" -length 0.081 
Dipole -name "DIPOLE_1083" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Bpm -name "IPBPMB" -length 0 -resolution $ipbpm
Dipole -name "DIPOLE_1084" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "L230F" -length 0.0702 
Dipole -name "DIPOLE_1085" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "MS1IP" -length 0 
Dipole -name "DIPOLE_1086" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Drift -name "MBS1IP" -length 0 
Dipole -name "DIPOLE_1087" -length 0.0 -strength_x 0.0 -strength_y 0.0 
Quadrupole -name "IP" -strength 0 -length 0 
Dipole -name "DIPOLE_1088" -length 0.0 -strength_x 0.0 -strength_y 0.0 

Dipole -name "DIPOLE_1089" -length 0.0 -strength_x 0.0 -strength_y 0.0 

Dipole -name "DIPOLE_1090" -length 0.0 -strength_x 0.0 -strength_y 0.0 
if {[info exist add_bpm_at_ip] && $add_bpm_at_ip} {
Dipole -name "DIPOLE_1091" -length 0.0 -strength_x 0.0 -strength_y 0.0 
    CavityBpm -name "BPMIP" -length 0 -resolution $ipbpm
Dipole -name "DIPOLE_1092" -length 0.0 -strength_x 0.0 -strength_y 0.0 
}
Dipole -name "DIPOLE_1093" -length 0.0 -strength_x 0.0 -strength_y 0.0 
TclCall -name "SAVEBEAMIP" -script { BeamDump -file ip_beam.dat }
Dipole -name "DIPOLE_1094" -length 0.0 -strength_x 0.0 -strength_y 0.0 

Dipole -name "DIPOLE_1095" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Drift -name "L231" -length 0.0882 
Dipole -name "DIPOLE_1096" -length 0.0 -strength_x 0.0 -strength_y 0.0 
#Bpm -name "IPBPMC" -length 0 -resolution $ipbpm
Dipole -name "DIPOLE_1097" -length 0.0 -strength_x 0.0 -strength_y 0.0 

Drift -name "ENDFF" -length 0 
Drift -name "L301A" -length 0.3338 
Drift -name "MW1IP" -length 0 
Drift -name "L301B" -length 0.203 
# not sure about resolution of MPIP - setting to stripline
Bpm -name "MPIP" -length 0 -resolution $stripline
#Drift -name "L301C" -length 0.302 
#Drift -name "MSPIP" -length 0 
#Drift -name "L301D" -length 0.573 
#Drift -name "MBDUMPA" -length 0 
#Sbend -name "BDUMPA" -synrad $sbend_synrad -length 0.6784391641 -angle 0.1745329252 -E1 0.1745329252 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
#set e0 [expr $e0-14.1e-6*0.1745329252*0.1745329252/0.6784391641*$e0*$e0*$e0*$e0*$sbend_synrad]
#SetReferenceEnergy $e0
#Sbend -name "BDUMPB" -synrad $sbend_synrad -length 0.6784391641 -angle 0.1745329252 -E1 0 -E2 0.1745329252 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
#set e0 [expr $e0-14.1e-6*0.1745329252*0.1745329252/0.6784391641*$e0*$e0*$e0*$e0*$sbend_synrad]
#SetReferenceEnergy $e0
#Drift -name "MBDUMPB" -length 0 
#Drift -name "L302A" -length 0.6345608359 
#Drift -name "MDUMP" -length 0 
#Drift -name "L302B" -length 0.146 
#Drift -name "ICTDUMP" -length 0 
#Drift -name "DIAMOND" -length 0 
#Drift -name "L302C" -length 1.219439164 
#Drift -name "DUMPH" -length 1.15 
#Drift -name "DUMPH" -length 1.15 
#Drift -name "ATF2MEND" -length 0 
