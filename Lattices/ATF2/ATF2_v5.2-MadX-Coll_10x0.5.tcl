# version 5.2 converted from MadX

# lattice options that can be set externally

if {! [info exist stripline]} {
    set stripline 3.0
}
if {! [info exist stripline2]} {
    set stripline2 3.0
}
if {! [info exist font_stripline_resolution]} {
    set font_stripline_resolution 0.5
}
if {! [info exist cband]} {
    set cband 0.2
}
if {! [info exist cband_noatt]} {
    set cband_noatt 0.2
}
if {! [info exist sband]} {
    set sband 1.0
}
if {! [info exist ipbpm]} {
    set ipbpm 0.1
}

# wakefields:
if {! [info exist cband_wake]} {
    set cband_wake ""
}
if {! [info exist sband_wake]} {
    set sband_wake ""
}

set collimator_wake "collimator_wake"

Drift -name "ATF2MSTART" -length 0 
Quadrupole -name "KEX1MULT1" -synrad $quad_synrad -length 0 -strength [expr -0.01846929*$e0] -e0 $e0 
Multipole -name "KEX1MULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -3.824591*$e0] -e0 $e0 
Sbend -name "KEX1A" -synrad $sbend_synrad -length 0.2500010417 -angle 0.0025 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.00635 -fint 0.5 
set e0 [expr $e0-14.1e-6*0.0025*0.0025/0.2500010417*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Quadrupole -name "KEX1MULT1" -synrad $quad_synrad -length 0 -strength [expr -0.01846929*$e0] -e0 $e0 -tilt 0.0
Multipole -name "KEX1MULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -3.824591*$e0] -e0 $e0 -tilt 0.0
Drift -name "IP01" -length 0 
Quadrupole -name "KEX1MULT1" -synrad $quad_synrad -length 0 -strength [expr -0.01846929*$e0] -e0 $e0 -tilt 0.0
Multipole -name "KEX1MULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -3.824591*$e0] -e0 $e0 -tilt 0.0
Sbend -name "KEX1B" -synrad $sbend_synrad -length 0.2500010417 -angle 0.0025 -E1 0 -E2 0.005 -six_dim 1 -e0 $e0 -hgap 0.00635 -fintx 0.5 
set e0 [expr $e0-14.1e-6*0.0025*0.0025/0.2500010417*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Quadrupole -name "KEX1MULT1" -synrad $quad_synrad -length 0 -strength [expr -0.01846929*$e0] -e0 $e0 -tilt 0.0
Multipole -name "KEX1MULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -3.824591*$e0] -e0 $e0 -tilt 0.0
Drift -name "L001" -length 0.9231390392 
Sbend -name "QM6RX" -synrad $sbend_synrad -length 0.09937458845 -angle 0.0023109657 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -K [expr -0.364431305488876*$e0] 
set e0 [expr $e0-14.1e-6*0.0023109657*0.0023109657/0.09937458845*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "QM6RX" -synrad $sbend_synrad -length 0.09937458845 -angle 0.0023109657 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -K [expr -0.364431305488876*$e0] 
set e0 [expr $e0-14.1e-6*0.0023109657*0.0023109657/0.09937458845*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L002A" -length 0.04685466892 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH100RX" -length 0.24955 
Drift -name "L002B" -length 0.7028333967 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH101RX" -length 0.1679 
Drift -name "L002C" -length 0.05068234611 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV100RX" -length 0.13874 
Drift -name "L002D" -length 0 
#BPM MB1X not instrumented but present (button BPM)
#Bpm -name "MB1X" -length 0 
Drift -name "L002E" -length 0.1894692706 
Sbend -name "QM7RX" -synrad $sbend_synrad -length 0.0421716479 -angle -0.0045873988 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -K [expr 0.204109999997787*$e0] 
set e0 [expr $e0-14.1e-6*-0.0045873988*-0.0045873988/0.0421716479*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "QM7RX" -synrad $sbend_synrad -length 0.0421716479 -angle -0.0045873988 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -K [expr 0.204109999997787*$e0] 
set e0 [expr $e0-14.1e-6*-0.0045873988*-0.0045873988/0.0421716479*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L003" -length 0.1478305058 
Sbend -name "BS1XA" -synrad $sbend_synrad -length 0.3 -angle 0.01401783271 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
set e0 [expr $e0-14.1e-6*0.01401783271*0.01401783271/0.3*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BS1XB" -synrad $sbend_synrad -length 0.3 -angle 0.01401783271 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
set e0 [expr $e0-14.1e-6*0.01401783271*0.01401783271/0.3*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L004" -length 0.2 
Sbend -name "BS2XA" -synrad $sbend_synrad -length 0.4 -angle 0.03717168307 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
set e0 [expr $e0-14.1e-6*0.03717168307*0.03717168307/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BS2XB" -synrad $sbend_synrad -length 0.4 -angle 0.03717168307 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
set e0 [expr $e0-14.1e-6*0.03717168307*0.03717168307/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L005A" -length 0.1 
#BPM MB2X present (button BPM) but callibration very dificult in Y
Bpm -name "MB2X" -length 0 -resolution $stripline
Drift -name "L005B" -length 0.1 
Sbend -name "BS3XA" -synrad $sbend_synrad -length 0.5 -angle 0.1175110129 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
set e0 [expr $e0-14.1e-6*0.1175110129*0.1175110129/0.5*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BS3XB" -synrad $sbend_synrad -length 0.5 -angle 0.1175110129 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
set e0 [expr $e0-14.1e-6*0.1175110129*0.1175110129/0.5*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L006A" -length 0.4782345 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV1X" -length 0.128141 
Drift -name "L006B" -length 0.0335945 
Quadrupole -name "QS1X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Quadrupole -name "QS1X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Drift -name "L006C" -length 0.08136 
Quadrupole -name "QF1X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.537911848523610*$e0] -e0 $e0 
Quadrupole -name "QF1X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.537911848523610*$e0] -e0 $e0 
Drift -name "L007A" -length 0.014695 
Bpm -name "MQF1X" -length 0 -resolution $stripline2
Drift -name "L007B" -length 0.214 
Drift -name "MS1X" -length 0 
Drift -name "L007C" -length 0.271305 
Multipole -name "BH1XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.183983746289*$e0] -e0 $e0 -tilt 0.0 
Sbend -name "BH1XA" -synrad $sbend_synrad -length 0.4 -angle 0.0941996781 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
set e0 [expr $e0-14.1e-6*0.0941996781*0.0941996781/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Multipole -name "BH1XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.183983746289*$e0] -e0 $e0 -tilt 0.0
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZX1X" -length 0 
Multipole -name "BH1XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.183983746289*$e0] -e0 $e0 -tilt 0.0
Sbend -name "BH1XB" -synrad $sbend_synrad -length 0.4 -angle 0.0941996781 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
set e0 [expr $e0-14.1e-6*0.0941996781*0.0941996781/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Multipole -name "BH1XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.183983746289*$e0] -e0 $e0 -tilt 0.0
Drift -name "L008A" -length 0.2132345 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV2X" -length 0.128141 
Drift -name "L008B" -length 0.1586245 
Quadrupole -name "QD2X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.470841881613835*$e0] -e0 $e0 
Quadrupole -name "QD2X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.470841881613835*$e0] -e0 $e0 
Drift -name "L009A" -length 0.014695 
Bpm -name "MQD2X" -length 0 -resolution $stripline2
Drift -name "L009B" -length 0.7578050808 
Quadrupole -name "QF3X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.335334011687395*$e0] -e0 $e0 
Quadrupole -name "QF3X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.335334011687395*$e0] -e0 $e0 
Drift -name "L010A" -length 0.026695 
Bpm -name "MQF3X" -length 0 -resolution $stripline2
Drift -name "L010B" -length 0.138725 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH1X" -length 0.11455 
Drift -name "L010C" -length 0.1396545 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV3X" -length 0.128141 
Drift -name "L010D" -length 1.505663876 
Drift -name "MID" -length 0 
Drift -name "L010E" -length 2.053429376 
Quadrupole -name "QF4X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.339354596559070*$e0] -e0 $e0 
Quadrupole -name "QF4X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.339354596559070*$e0] -e0 $e0 
Drift -name "L011A" -length 0.026695 
Bpm -name "MQF4X" -length 0 -resolution $stripline2
Drift -name "L011B" -length 0.134725 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH2X" -length 0.11455 
Drift -name "L011C" -length 0.4698350808 
Bpm -name "MQD5X" -length 0 -resolution $stripline2
Drift -name "L011D" -length 0.026695 
Quadrupole -name "QD5X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.460772807543940*$e0] -e0 $e0 
Quadrupole -name "QD5X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.460772807543940*$e0] -e0 $e0 
Drift -name "L012A" -length 0.1576245 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV4X" -length 0.128141 
Drift -name "L012B" -length 0.2142345 
Multipole -name "BH2XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.183983746289*$e0] -e0 $e0 -tilt 0.0
Sbend -name "BH2XA" -synrad $sbend_synrad -length 0.4 -angle -0.0941996781 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
set e0 [expr $e0-14.1e-6*-0.0941996781*-0.0941996781/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Multipole -name "BH2XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.183983746289*$e0] -e0 $e0 -tilt 0.0
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZX2X" -length 0 
Multipole -name "BH2XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.183983746289*$e0] -e0 $e0 -tilt 0.0
Sbend -name "BH2XB" -synrad $sbend_synrad -length 0.4 -angle -0.0941996781 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
set e0 [expr $e0-14.1e-6*-0.0941996781*-0.0941996781/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Multipole -name "BH2XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.183983746289*$e0] -e0 $e0 -tilt 0.0
Drift -name "L013A" -length 0.2142345 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV5X" -length 0.128141 
Drift -name "L013B" -length 0.1429295 
Bpm -name "MQF6X" -length 0 -resolution $stripline
Drift -name "L013C" -length 0.014695 
Quadrupole -name "QF6X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.565048512466895*$e0] -e0 $e0 
Quadrupole -name "QF6X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.565048512466895*$e0] -e0 $e0 
Drift -name "L014A" -length 0.00536 
Quadrupole -name "QS2X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Quadrupole -name "QS2X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Drift -name "L014B" -length 0.334665 
Drift -name "UDDX1" -length 0 
Drift -name "L014C" -length 0.5875079196 
Sbend -name "BH3XA" -synrad $sbend_synrad -length 0.675 -angle -0.1714240956 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
set e0 [expr $e0-14.1e-6*-0.1714240956*-0.1714240956/0.675*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BH3XB" -synrad $sbend_synrad -length 0.675 -angle -0.1714240956 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
set e0 [expr $e0-14.1e-6*-0.1714240956*-0.1714240956/0.675*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L015" -length 0.7650382419 
Quadrupole -name "QF7X" -synrad $quad_synrad -length 0.039335 -strength [expr 0.191515641796390*$e0] -e0 $e0 
Quadrupole -name "QF7X" -synrad $quad_synrad -length 0.039335 -strength [expr 0.191515641796390*$e0] -e0 $e0 
Drift -name "L016A" -length 0.026665 
Bpm -name "MQF7X" -length 0 -resolution $stripline
Drift -name "L016B" -length 1.767246207 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH3X" -length 0.11455 
Drift -name "L016C" -length 0.16342 
Quadrupole -name "QD8X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.295018822405955*$e0] -e0 $e0 
Quadrupole -name "QD8X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.295018822405955*$e0] -e0 $e0 
Drift -name "L017A" -length 0.014695 
Bpm -name "MQD8X" -length 0 -resolution $stripline
Drift -name "L017B" -length 0.1389295 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV6X" -length 0.128141 
Drift -name "L017C" -length 0.6855607408 
Drift -name "KEX2A" -length 0.2500010417 
Drift -name "IP02" -length 0 
Drift -name "KEX2B" -length 0.2500010417 
Drift -name "MDISP" -length 0 
Drift -name "L101A" -length 0.06027549974 
Sbend -name "BKXA" -synrad $sbend_synrad -length 0.1 -angle 0.0025 -E1 0.005 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.05 -fint 0.5 
set e0 [expr $e0-14.1e-6*0.0025*0.0025/0.1*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BKXB" -synrad $sbend_synrad -length 0.1 -angle 0.0025 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.05 -fintx 0.5 
set e0 [expr $e0-14.1e-6*0.0025*0.0025/0.1*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L101B" -length 0.166 
Bpm -name "MQF9X" -length 0 -resolution $stripline
Drift -name "L101C" -length 0.054695 
Quadrupole -name "QF9X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.381483282330435*$e0] -e0 $e0 
Quadrupole -name "QF9X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.381483282330435*$e0] -e0 $e0 
Drift -name "L102A" -length 0.15742 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH4X" -length 0.11455 
Drift -name "L102B" -length 1.339476477 
Drift -name "FONTK1" -length 0 
Drift -name "L102C" -length 0.3339295 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV7X" -length 0.128141 
Drift -name "L102D" -length 0.0859295 
Bpm -name "FONTP1" -length 0 -resolution $font_stripline_resolution
Drift -name "L102E" -length 0.125665 
Quadrupole -name "QK1X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Quadrupole -name "QK1X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Drift -name "L102F" -length 0.10242 

Multipole -name "QD10XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0418510129542*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD10XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.71401889224*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD10XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0447350197823*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD10XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -7.21583322997*$e0] -e0 $e0 -tilt 0.785398163397448 

Quadrupole -name "QD10X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.583311951378510*$e0] -e0 $e0 

Multipole -name "QD10XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0418510129542*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD10XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.71401889224*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD10XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0447350197823*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QD10XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -7.21583322997*$e0] -e0 $e0 -tilt 0.785398163397448
 
Multipole -name "QD10XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0418510129542*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD10XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.71401889224*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD10XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0447350197823*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QD10XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -7.21583322997*$e0] -e0 $e0 -tilt 0.785398163397448 

Quadrupole -name "QD10X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.583311951378510*$e0] -e0 $e0 

Multipole -name "QD10XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0418510129542*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD10XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.71401889224*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD10XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0447350197823*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QD10XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -7.21583322997*$e0] -e0 $e0 -tilt 0.785398163397448 

Drift -name "L103A" -length 0.019755 
Bpm -name "MQD10X" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L103B" -length 0.9566 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH5X" -length 0.1248 
Drift -name "L103C" -length 0.150355 

Multipole -name "QF11XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0226821696201*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF11XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 8.128849772959*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QF11XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0182224856497*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF11XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -4.33581635108*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QF11X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.511665676936205*$e0] -e0 $e0 

Multipole -name "QF11XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0226821696201*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF11XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 8.128849772959*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QF11XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0182224856497*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF11XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -4.33581635108*$e0] -e0 $e0 -tilt 0.785398163397448

Multipole -name "QF11XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0226821696201*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF11XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 8.128849772959*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QF11XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0182224856497*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF11XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -4.33581635108*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QF11X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.511665676936205*$e0] -e0 $e0 

Multipole -name "QF11XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0226821696201*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF11XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 8.128849772959*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QF11XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0182224856497*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF11XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -4.33581635108*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "L104A" -length 0.019755 
Bpm -name "MQF11X" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L104B" -length 0.403 
Drift -name "FONTK2" -length 0 
Drift -name "L104C" -length 0.3069295 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV8X" -length 0.128141 
Drift -name "L104D" -length 0.0899295 
Bpm -name "FONTP2" -length 0 -resolution $font_stripline_resolution
Drift -name "L104F" -length 0.12433 
Quadrupole -name "QK2X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Quadrupole -name "QK2X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Drift -name "L104G" -length 0.100755 

Multipole -name "QD12XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0620668180039*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD12XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 4.22433371422*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD12XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0350613561687*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD12XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 2.69224524033*$e0] -e0 $e0 -tilt 0.785398163397448  

Quadrupole -name "QD12X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.511665676936205*$e0] -e0 $e0 

Multipole -name "QD12XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0620668180039*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD12XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 4.22433371422*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD12XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0350613561687*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD12XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 2.69224524033*$e0] -e0 $e0 -tilt 0.785398163397448 

Multipole -name "QD12XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0620668180039*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD12XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 4.22433371422*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD12XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0350613561687*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD12XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 2.69224524033*$e0] -e0 $e0 -tilt 0.785398163397448 

Quadrupole -name "QD12X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.511665676936205*$e0] -e0 $e0 

Multipole -name "QD12XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0620668180039*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD12XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 4.22433371422*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD12XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0350613561687*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD12XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 2.69224524033*$e0] -e0 $e0 -tilt 0.785398163397448 


Drift -name "L105A" -length 0.019755 
Bpm -name "MQD12X" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L105B" -length 0.5351231519 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH6X" -length 0.11455 
Drift -name "L105C" -length 0.1653525 
Quadrupole -name "QF13X" -synrad $quad_synrad -length 0.0993725 -strength [expr 0.684119448627977*$e0] -e0 $e0 
Quadrupole -name "QF13X" -synrad $quad_synrad -length 0.0993725 -strength [expr 0.684119448627977*$e0] -e0 $e0 
Drift -name "L106A" -length 0.0546275 
Bpm -name "MQF13X" -length 0 -resolution $stripline
Drift -name "L106B" -length 0.7821670781 
Quadrupole -name "QD14X" -synrad $quad_synrad -length 0.0993725 -strength [expr -0.507718234912880*$e0] -e0 $e0 
Drift -name "FIT180" -length 0 
Quadrupole -name "QD14X" -synrad $quad_synrad -length 0.0993725 -strength [expr -0.507718234912880*$e0] -e0 $e0 
Drift -name "L107A" -length 0.0546275 
Bpm -name "MQD14X" -length 0 -resolution $stripline
Drift -name "L107B" -length 0.276 
Bpm -name "FONTP3" -length 0 -resolution $font_stripline_resolution
Drift -name "L107C" -length 0.2282645781 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH7X" -length 0.11455 
Drift -name "L107D" -length 0.1633525 
Quadrupole -name "QF15X" -synrad $quad_synrad -length 0.0993725 -strength [expr 0.684119448627977*$e0] -e0 $e0 
Quadrupole -name "QF15X" -synrad $quad_synrad -length 0.0993725 -strength [expr 0.684119448627977*$e0] -e0 $e0 
Drift -name "L108A" -length 0.0546275 
Bpm -name "MQF15X" -length 0 -resolution $stripline
Drift -name "L108B" -length 0.3156 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV9X" -length 0.1248 
Drift -name "L108D" -length 0.1603281519 
Quadrupole -name "QK3X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Quadrupole -name "QK3X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Drift -name "L108E" -length 0.100755 

Multipole -name "QD16XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0153427897931*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD16XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.8127523836*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD16XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0128136291866*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QD16XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.61443942174*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QD16X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.511665676936205*$e0] -e0 $e0 

Multipole -name "QD16XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0153427897931*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD16XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.8127523836*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD16XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0128136291866*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QD16XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.61443942174*$e0] -e0 $e0 -tilt 0.785398163397448

Multipole -name "QD16XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0153427897931*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD16XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.8127523836*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD16XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0128136291866*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QD16XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.61443942174*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QD16X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.511665676936205*$e0] -e0 $e0 

Multipole -name "QD16XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0153427897931*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD16XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.8127523836*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD16XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0128136291866*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QD16XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.61443942174*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "L109A" -length 0.019755 
Bpm -name "QD16X" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L109B" -length 0.9546 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH8X" -length 0.1248 
Drift -name "L109C" -length 0.152355 

Multipole -name "QF17XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00260392465947*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF17XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 4.43531407037*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF17XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0053393244933*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF17XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.7101171157*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QF17X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.511665676936205*$e0] -e0 $e0 

Multipole -name "QF17XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00260392465947*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF17XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 4.43531407037*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF17XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0053393244933*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF17XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.7101171157*$e0] -e0 $e0 -tilt 0.785398163397448

Multipole -name "QF17XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00260392465947*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF17XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 4.43531407037*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF17XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0053393244933*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF17XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.7101171157*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QF17X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.511665676936205*$e0] -e0 $e0 

Multipole -name "QF17XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00260392465947*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF17XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 4.43531407037*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF17XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0053393244933*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF17XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.7101171157*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "L110A" -length 0.019755 
Bpm -name "QF17X" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L110B" -length 0.211 
Drift -name "MREF1X" -length 0 
Drift -name "L110C" -length 0.8401649959 
Quadrupole -name "QK4X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Quadrupole -name "QK4X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Drift -name "L110D" -length 0.6575945 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV10X" -length 0.128141 
Drift -name "L110E" -length 0.2486845 
Multipole -name "QD18XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000186955096522*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD18XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.868506594619*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD18XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00857386363763*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QD18XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.03109432205] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QD18X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.343036067819360*$e0] -e0 $e0 

Multipole -name "QD18XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000186955096522*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD18XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.868506594619*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD18XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00857386363763*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QD18XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.03109432205] -e0 $e0 -tilt 0.785398163397448

Multipole -name "QD18XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000186955096522*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD18XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.868506594619*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD18XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00857386363763*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QD18XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.03109432205] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QD18X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.343036067819360*$e0] -e0 $e0 

Multipole -name "QD18XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000186955096522*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD18XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.868506594619*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD18XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00857386363763*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QD18XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.03109432205] -e0 $e0 -tilt 0.785398163397448

Drift -name "L111A" -length 0.019755 
Bpm -name "QD18X" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L111B" -length 0.276 
Drift -name "MW0X" -length 0 
Drift -name "L111C" -length 0.4046498952 
Drift -name "OTR0X" -length 0 
Drift -name "L111D" -length 0.2716 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH9X" -length 0.1248 
Drift -name "L111E" -length 0.149355 

Multipole -name "QF19XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000249465735489*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QF19XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.577861042839*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF19XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00215090535716*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF19XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.59904371225*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QF19X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.327583020490130*$e0] -e0 $e0 

Multipole -name "QF19XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000249465735489*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QF19XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.577861042839*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF19XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00215090535716*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF19XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.59904371225*$e0] -e0 $e0 -tilt 0.785398163397448 

Multipole -name "QF19XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000249465735489*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QF19XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.577861042839*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF19XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00215090535716*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF19XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.59904371225*$e0] -e0 $e0 -tilt 0.785398163397448 

Quadrupole -name "QF19X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.327583020490130*$e0] -e0 $e0 

Multipole -name "QF19XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000249465735489*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QF19XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.577861042839*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF19XMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00215090535716*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF19XMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.59904371225*$e0] -e0 $e0 -tilt 0.785398163397448 

Drift -name "L112A" -length 0.019755 
Bpm -name "QF19X" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L112B" -length 0.265 
Drift -name "MW1X" -length 0 
Drift -name "L112C" -length 0.386 
Drift -name "OTR1X" -length 0 
Drift -name "L112D" -length 0.309 
Drift -name "WCM" -length 0 
Drift -name "L112E" -length 0.4849295 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV11X" -length 0.128141 
Drift -name "L112F" -length 1.4372645 
Drift -name "PPMW" -length 0 
Drift -name "L112G" -length 0.469 
Drift -name "MW2X" -length 0 
Drift -name "L112H" -length 0.282 
Bpm -name "QD20X" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L112I" -length 0.019665 
Quadrupole -name "QD20X" -synrad $quad_synrad -length 0.039335 -strength [expr -0.151134864218325*$e0] -e0 $e0 
Drift -name "MEMIT" -length 0 
Quadrupole -name "QD20X" -synrad $quad_synrad -length 0.039335 -strength [expr -0.151134864218325*$e0] -e0 $e0 
Drift -name "L113A" -length 0.466665 
Drift -name "OTR2X" -length 0 
Drift -name "L113B" -length 1.861065 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH10X" -length 0.11921 
Drift -name "L113C" -length 0.937395 
Drift -name "ICT1X" -length 0 
Drift -name "L113D" -length 0.415665 
Quadrupole -name "QF21X" -synrad $quad_synrad -length 0.039335 -strength [expr 0.222356478341460*$e0] -e0 $e0 
Quadrupole -name "QF21X" -synrad $quad_synrad -length 0.039335 -strength [expr 0.222356478341460*$e0] -e0 $e0 
Drift -name "L114A" -length 0.019665 
Bpm -name "QF21X" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L114B" -length 0.274 
Drift -name "MW3X" -length 0 
Drift -name "L114C" -length 0.502 
Drift -name "OTR3X" -length 0 
Drift -name "L114D" -length 0.35 
Drift -name "TILTM" -length 0 
Drift -name "L114E" -length 0.54 
#not present anymore
#Bpm -name "IPBPM" -length 0 
Drift -name "ATIPBPM" -length 0 
Drift -name "L114F" -length 1.813335 
Drift -name "MW4X" -length 0 
Drift -name "L114G" -length 0.282 
Drift -name "BEGFF" -length 0 
TclCall -script {BeamDump -file BEGFF.out}
Bpm -name "QM16FF" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake

Drift -name "L200" -length 0.019755 
Multipole -name "QM16FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00161959843622*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM16FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -2.7793322179*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM16FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00320433250548*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QM16FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.47392269321*$e0] -e0 $e0 -tilt 0.785398163397448 

Quadrupole -name "QM16FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0*$e0] -e0 $e0 

Multipole -name "QM16FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00161959843622*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM16FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -2.7793322179*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM16FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00320433250548*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QM16FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.47392269321*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "AQM16FF" -length 0 

Multipole -name "QM16FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00161959843622*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM16FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -2.7793322179*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM16FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00320433250548*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QM16FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.47392269321*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QM16FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0*$e0] -e0 $e0 

Multipole -name "QM16FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00161959843622*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM16FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -2.7793322179*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM16FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00320433250548*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QM16FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.47392269321*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "L201A" -length 0.76315 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH1FF" -length 0.11921 
Drift -name "L201B" -length 0.1353245 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV1FF" -length 0.128141 
Drift -name "L201C" -length 0.1556845 

Multipole -name "QM15FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 4.46562257828e-05*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM15FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.343441048321*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM15FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.000908326635071*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QM15FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.310823742056*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QM15FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.330508748203380*$e0] -e0 $e0 

Multipole -name "QM15FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 4.46562257828e-05*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM15FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.343441048321*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM15FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.000908326635071*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QM15FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.310823742056*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "AQM15FF" -length 0 

Multipole -name "QM15FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 4.46562257828e-05*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM15FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.343441048321*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM15FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.000908326635071*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QM15FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.310823742056*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QM15FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.330508748203380*$e0] -e0 $e0 

Multipole -name "QM15FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 4.46562257828e-05*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM15FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.343441048321*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM15FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.000908326635071*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QM15FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.310823742056*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "L202A" -length 0.019755 
Bpm -name "QM15FF" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
Drift -name "L202B" -length 1.281755 
Multipole -name "QM14FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0262576891184*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM14FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -4.25640493619*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM14FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0178980704129*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QM14FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 2.25940914063*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QM14FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.905736310158185*$e0] -e0 $e0 

Multipole -name "QM14FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0262576891184*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM14FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -4.25640493619*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM14FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0178980704129*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QM14FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 2.25940914063*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "AQM14FF" -length 0 

Multipole -name "QM14FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0262576891184*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM14FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -4.25640493619*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM14FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0178980704129*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QM14FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 2.25940914063*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QM14FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.905736310158185*$e0] -e0 $e0 

Multipole -name "QM14FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0262576891184*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM14FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -4.25640493619*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM14FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0178980704129*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QM14FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 2.25940914063*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "L203A" -length 0.019755 
Bpm -name "QM14FF" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
Drift -name "L203B" -length 0.557455 
Drift -name "LWML1" -length 0 
Drift -name "L203C" -length 0.0215 
Drift -name "LW1FF" -length 0 
Drift -name "L203D" -length 0.0215 
Drift -name "LWML2" -length 0 
Drift -name "L203E" -length 0.2035 
Bpm -name "FB2FF" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
Drift -name "AFB2FF" -length 0 
Drift -name "L203F" -length 0.4778 
Multipole -name "QM13FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000225653062193*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM13FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 6.11153764811*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QM13FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00255464950487*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QM13FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.992957553787*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QM13FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.482255254347980*$e0] -e0 $e0 

Multipole -name "QM13FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000225653062193*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM13FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 6.11153764811*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QM13FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00255464950487*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QM13FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.992957553787*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "AQM13FF" -length 0 

Multipole -name "QM13FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000225653062193*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM13FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 6.11153764811*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QM13FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00255464950487*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QM13FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.992957553787*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QM13FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.482255254347980*$e0] -e0 $e0 

Multipole -name "QM13FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000225653062193*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM13FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 6.11153764811*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QM13FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00255464950487*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QM13FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.992957553787*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "L204A" -length 0.019755 
Bpm -name "QM13FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L204B" -length 1.281755 

Multipole -name "QM12FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00939078814993*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM12FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.235554167819*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM12FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.000162810409894*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QM12FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.1634251614*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QM12FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.164287207460155*$e0] -e0 $e0 

Multipole -name "QM12FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00939078814993*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM12FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.235554167819*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM12FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.000162810409894*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QM12FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.1634251614*$e0] -e0 $e0 -tilt 0.785398163397448 

Drift -name "AQM12FF" -length 0
 
Multipole -name "QM12FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00939078814993*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM12FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.235554167819*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM12FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.000162810409894*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QM12FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.1634251614*$e0] -e0 $e0 -tilt 0.785398163397448 

Quadrupole -name "QM12FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.164287207460155*$e0] -e0 $e0 

Multipole -name "QM12FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00939078814993*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM12FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.235554167819*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM12FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.000162810409894*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QM12FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.1634251614*$e0] -e0 $e0 -tilt 0.785398163397448
Drift -name "L205A" -length 0.019755 
Bpm -name "QM12FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L205B" -length 0.715755 
# not present
#Bpm -name "MFB1FF" -length 0 
Drift -name "L205C" -length 0.666 

Multipole -name "QM11FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000331239365424*$e0] -e0 $e0 -tilt 0.0  
Multipole -name "QM11FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.374398792874*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM11FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00113941541135*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QM11FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.0321619579745*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QM11FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.109031309971815*$e0] -e0 $e0 

Multipole -name "QM11FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000331239365424*$e0] -e0 $e0 -tilt 0.0  
Multipole -name "QM11FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.374398792874*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM11FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00113941541135*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QM11FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.0321619579745*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "AQM11FF" -length 0 

Multipole -name "QM11FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000331239365424*$e0] -e0 $e0 -tilt 0.0  
Multipole -name "QM11FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.374398792874*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM11FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00113941541135*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QM11FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.0321619579745*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QM11FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.109031309971815*$e0] -e0 $e0 

Multipole -name "QM11FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000331239365424*$e0] -e0 $e0 -tilt 0.0  
Multipole -name "QM11FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.374398792874*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QM11FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00113941541135*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QM11FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.0321619579745*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "L206A" -length 0.019755 
Bpm -name "QM11FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L206BA" -length 0.8408775 
#  Rectangular tapered collimator
#Girder
#Collimator -name "C3WAKE1" -vertical 1 -in_height $paramb -fin_height $parama -width $colwidth -taper_length $tlength -flat_length $flength -sigma $conduct -charge $match(charge) -correct_offset $offset -wake_method $wmetode -outputfile collimator.txt

# Second way to introduce the wakefield effect of the collimator
# collimator
TclCall -script {BeamDump -file Collparticles.out}
#set collimator_wake ""
#set bunch_length_mm [expr round(7/1000.)]
#SplineCreate "collimator_wake" -file "/Users/Nuria/Desktop/2015_work/PlacetBDSIM_WFS/ATF2/Frameworks/ATF2_studies/data/WakeData/Wake_coll_a5.txt"
#ShortRangeWake "collimator_wake" -wx "collimator_wake" -wy "collimator_wake" -type 1 
#set collimator_transverse_wake "collimator_wake"
#Bpm -name "C3WAKE1" -length 0 -short_range_wake $collimator_transverse_wake

#Bpm -name "COLLBY" -length 0 
Drift -name "L206BB" -length 0.4408775 

Multipole -name "QD10BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00281662126736*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD10BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.3404428505*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD10BFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00151377482575*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD10BFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.430239827068*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QD10BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.145009665727875*$e0] -e0 $e0 

Multipole -name "QD10BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00281662126736*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD10BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.3404428505*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD10BFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00151377482575*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD10BFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.430239827068*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "AQD10BFF" -length 0 

Multipole -name "QD10BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00281662126736*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD10BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.3404428505*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD10BFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00151377482575*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD10BFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.430239827068*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QD10BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.145009665727875*$e0] -e0 $e0 

Multipole -name "QD10BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00281662126736*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD10BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.3404428505*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD10BFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00151377482575*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD10BFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.430239827068*$e0] -e0 $e0 -tilt 0.785398163397448
Drift -name "L207A" -length 0.019755 
Bpm -name "QD10BFF" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
Drift -name "L207B" -length 0.477 
# collimator cavity not connected
#Bpm -name "COLLB" -length 0 
Drift -name "MREF3FF" -length 0 
Drift -name "L207C" -length 0.204755 

Multipole -name "QD10AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00498305881069*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD10AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.620303232595*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD10AFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00321705409996*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD10AFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.0504629882755*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QD10AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.145009665727875*$e0] -e0 $e0 

Multipole -name "QD10AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00498305881069*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD10AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.620303232595*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD10AFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00321705409996*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD10AFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.0504629882755*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "AQD10AFF" -length 0 

Multipole -name "QD10AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00498305881069*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD10AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.620303232595*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD10AFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00321705409996*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD10AFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.0504629882755*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QD10AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.145009665727875*$e0] -e0 $e0 

Multipole -name "QD10AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00498305881069*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD10AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.620303232595*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD10AFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00321705409996*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD10AFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.0504629882755*$e0] -e0 $e0 -tilt 0.785398163397448
Drift -name "L208A" -length 0.019755 
Bpm -name "QD10AFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L208B" -length 0.289 
Drift -name "MS1FF" -length 0 
Drift -name "L208C" -length 0.792755 
Multipole -name "QF9BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0296179553562*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF9BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.351583625662*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QF9BFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00615485377801*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF9BFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.339087923296*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QF9BFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.189324983874920*$e0] -e0 $e0 

Multipole -name "QF9BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0296179553562*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF9BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.351583625662*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QF9BFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00615485377801*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF9BFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.339087923296*$e0] -e0 $e0 -tilt 0.785398163397448 

Drift -name "AQF9BFF" -length 0 

Multipole -name "QF9BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0296179553562*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF9BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.351583625662*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QF9BFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00615485377801*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF9BFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.339087923296*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QF9BFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.189324983874920*$e0] -e0 $e0 

Multipole -name "QF9BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0296179553562*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF9BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.351583625662*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QF9BFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00615485377801*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF9BFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.339087923296*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "L209A" -length 0.019755 
Bpm -name "QF9BFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L209B" -length 0.390960984 

Multipole -name "SF6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.577867137359*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 16.4804162942*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 7135.7443425*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr 450082.115399*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr 591105477.008*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr 8.79062418012e+12*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr -1.71238705119e+14*$e0] -e0 $e0 -tilt 0.0 

Multipole -name "SF6FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.780372772391*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF6FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr 78.5448231121*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SF6FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr 15585.8174139*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF6FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr -3831366.44202*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF6FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr -64632175.5395*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF6FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr 41425147154.9*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF6FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -3.57736293212e+13*$e0] -e0 $e0 -tilt 0.785398163397448 

Multipole -name "SF6FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr  5.611721279039542*$e0] -e0 $e0 

Multipole -name "SF6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.577867137359*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 16.4804162942*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 7135.7443425*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr 450082.115399*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr 591105477.008*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr 8.79062418012e+12*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr -1.71238705119e+14*$e0] -e0 $e0 -tilt 0.0 

Multipole -name "SF6FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.780372772391*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF6FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr 78.5448231121*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SF6FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr 15585.8174139*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF6FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr -3831366.44202*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF6FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr -64632175.5395*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF6FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr 41425147154.9*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF6FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -3.57736293212e+13*$e0] -e0 $e0 -tilt 0.785398163397448  

Drift -name "ASF6FF" -length 0 

Multipole -name "SF6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.577867137359*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 16.4804162942*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 7135.7443425*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr 450082.115399*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr 591105477.008*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr 8.79062418012e+12*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr -1.71238705119e+14*$e0] -e0 $e0 -tilt 0.0 

Multipole -name "SF6FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.780372772391*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF6FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr 78.5448231121*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SF6FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr 15585.8174139*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF6FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr -3831366.44202*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF6FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr -64632175.5395*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF6FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr 41425147154.9*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF6FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -3.57736293212e+13*$e0] -e0 $e0 -tilt 0.785398163397448 

Multipole -name "SF6FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr  5.611721279039542*$e0] -e0 $e0 

Multipole -name "SF6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.577867137359*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 16.4804162942*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 7135.7443425*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr 450082.115399*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr 591105477.008*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr 8.79062418012e+12*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF6FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr -1.71238705119e+14*$e0] -e0 $e0 -tilt 0.0 

Multipole -name "SF6FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.780372772391*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF6FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr 78.5448231121*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SF6FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr 15585.8174139*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF6FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr -3831366.44202*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF6FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr -64632175.5395*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF6FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr 41425147154.9*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF6FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -3.57736293212e+13*$e0] -e0 $e0 -tilt 0.785398163397448 

Drift -name "L210A" -length 0.099791984 
Bpm -name "SF6FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L210B" -length 0.310924 

Multipole -name "QF9AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0160314287847*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF9AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -2.56166814597*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QF9AFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.019860387306*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF9AFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.297049452429*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QF9AFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.189324983874920*$e0] -e0 $e0 

Multipole -name "QF9AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0160314287847*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF9AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -2.56166814597*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QF9AFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.019860387306*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF9AFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.297049452429*$e0] -e0 $e0 -tilt 0.785398163397448 

Drift -name "AQF9AFF" -length 0 

Multipole -name "QF9AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0160314287847*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF9AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -2.56166814597*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QF9AFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.019860387306*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF9AFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.297049452429*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QF9AFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.189324983874920*$e0] -e0 $e0 

Multipole -name "QF9AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0160314287847*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF9AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -2.56166814597*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QF9AFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.019860387306*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF9AFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.297049452429*$e0] -e0 $e0 -tilt 0.785398163397448
 
Drift -name "L211A" -length 0.019755 
Bpm -name "QF9AFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L211B" -length 0.266 
Multipole -name "SK4FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr -0.070694580706000*$e0] -e0 $e0 -tilt -0.523598775598 
Multipole -name "SK4FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr -0.070694580706000*$e0] -e0 $e0 -tilt -0.523598775598 
Drift -name "L211C" -length 1.085755 

Multipole -name "QD8FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00690863576129*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD8FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -6.14876448704*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD8FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0196619314878*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QD8FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.998640505844*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QD8FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.302177699348405*$e0] -e0 $e0 

Multipole -name "QD8FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00690863576129*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD8FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -6.14876448704*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD8FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0196619314878*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD8FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.998640505844*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "AQD8FF" -length 0 

Multipole -name "QD8FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00690863576129*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD8FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -6.14876448704*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD8FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0196619314878*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD8FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.998640505844*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QD8FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.302177699348405*$e0] -e0 $e0 

Multipole -name "QD8FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00690863576129*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD8FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -6.14876448704*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD8FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0196619314878*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QD8FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.998640505844*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "L212A" -length 0.019755 
Bpm -name "QD8FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L212B" -length 1.681755 

Multipole -name "QF7FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0235927837079*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF7FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -4.52406949142*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF7FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00283760900847*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF7FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.348035883*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QF7FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.275080918478550*$e0] -e0 $e0
 
Multipole -name "QF7FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0235927837079*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF7FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -4.52406949142*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF7FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00283760900847*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF7FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.348035883*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "AQF7FF" -length 0 

Multipole -name "QF7FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0235927837079*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF7FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -4.52406949142*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF7FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00283760900847*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF7FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.348035883*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QF7FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.275080918478550*$e0] -e0 $e0 

Multipole -name "QF7FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0235927837079*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF7FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -4.52406949142*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF7FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00283760900847*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF7FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.348035883*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "L213A" -length 0.019755 
Bpm -name "QF7FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L213B" -length 0.4246097102 

Quadrupole -name "B5FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.99583732856e-05*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B5FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0511433315445*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "B5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.673595098389*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "B5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -868.189237924*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B5FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00174635766251*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "B5FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.20799493159e-12*$e0] -e0 $e0 -tilt 0.785398163397448


Sbend -name "B5FFA" -synrad $sbend_synrad -length 0.3064317863 -angle -0.02494796661 -E1 -0.02494796661 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.01905 -fint 0.5 
set e0 [expr $e0-14.1e-6*-0.02494796661*-0.02494796661/0.3064317863*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0

Quadrupole -name "B5FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.99583732856e-05*$e0] -e0 $e0 
Multipole -name "B5FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0511433315445*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "B5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.673595098389*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "B5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -868.189237924*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B5FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00174635766251*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "B5FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.20799493159e-12*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "B5FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.99583732856e-05*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B5FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0511433315445*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "B5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.673595098389*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "B5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -868.189237924*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B5FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00174635766251*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "B5FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.20799493159e-12*$e0] -e0 $e0 -tilt 0.785398163397448

Sbend -name "B5FFB" -synrad $sbend_synrad -length 0.3064317863 -angle -0.02494796661 -E1 0 -E2 -0.02494796661 -six_dim 1 -e0 $e0 -hgap 0.01905 -fintx 0.5 

set e0 [expr $e0-14.1e-6*-0.02494796661*-0.02494796661/0.3064317863*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0
Quadrupole -name "B5FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.99583732856e-05*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B5FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0511433315445*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "B5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.673595098389*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "B5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -868.189237924*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B5FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00174635766251*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "B5FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.20799493159e-12*$e0] -e0 $e0 -tilt 0.785398163397448
 
Drift -name "L214" -length 0.6443647102 

Multipole -name "QD6FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0389644429701*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -9.14165905593*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD6FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0240722264693*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD6FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.310805565847*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QD6FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.301163605998805*$e0] -e0 $e0 

Multipole -name "QD6FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0389644429701*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -9.14165905593*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD6FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0240722264693*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD6FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.310805565847*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "AQD6FF" -length 0 

Multipole -name "QD6FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0389644429701*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -9.14165905593*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD6FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0240722264693*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD6FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.310805565847*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QD6FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.301163605998805*$e0] -e0 $e0 

Multipole -name "QD6FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0389644429701*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -9.14165905593*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD6FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0240722264693*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD6FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.310805565847*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "L215A" -length 0.019755 
Bpm -name "QD6FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L215B" -length 1.066 

Multipole -name "SK3FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr -0.043231917420500*$e0] -e0 $e0 -tilt -0.523598775598 
Multipole -name "SK3FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr -0.043231917420500*$e0] -e0 $e0 -tilt -0.523598775598 

Drift -name "L215C" -length 0.285755 

Multipole -name "QF5BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0145030989982*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF5BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 3.75839646603*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF5BFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00970840615881*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF5BFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.180507724852*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QF5BFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.188028317183970*$e0] -e0 $e0 

Multipole -name "QF5BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0145030989982*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF5BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 3.75839646603*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF5BFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00970840615881*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF5BFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.180507724852*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "AQF5BFF" -length 0 

Multipole -name "QF5BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0145030989982*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF5BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 3.75839646603*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF5BFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00970840615881*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF5BFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.180507724852*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QF5BFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.188028317183970*$e0] -e0 $e0 

Multipole -name "QF5BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0145030989982*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF5BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 3.75839646603*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF5BFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00970840615881*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF5BFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.180507724852*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "L216A" -length 0.019755 
Bpm -name "QF5BFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L216B" -length 0.390960984 

Multipole -name "SF5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.00226101296598*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 10.50445657081*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 9.73003860997*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr 6243.1712849*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr -1478359.53968*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr -18606983912.8*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr 5.59472225864e+12*$e0] -e0 $e0 -tilt 0.0 


Multipole -name "SF5FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.00045261578508*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SF5FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr 2.4734711897*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF5FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr 13.3041535439*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF5FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr -5188.70511108*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF5FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr -1168376.15047*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF5FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr 643267292.028*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF5FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr 8.07980504623e+12*$e0] -e0 $e0 -tilt 0.785398163397448 

Multipole -name "SF5FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr 0.643396082373034*$e0] -e0 $e0 

Multipole -name "SF5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.00226101296598*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 10.50445657081*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 9.73003860997*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr 6243.1712849*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr -1478359.53968*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr -18606983912.8*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr 5.59472225864e+12*$e0] -e0 $e0 -tilt 0.0 


Multipole -name "SF5FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.00045261578508*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SF5FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr 2.4734711897*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF5FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr 13.3041535439*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF5FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr -5188.70511108*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF5FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr -1168376.15047*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF5FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr 643267292.028*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF5FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr 8.07980504623e+12*$e0] -e0 $e0 -tilt 0.785398163397448 

Drift -name "ASF5FF" -length 0 

Multipole -name "SF5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.00226101296598*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 10.50445657081*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 9.73003860997*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr 6243.1712849*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr -1478359.53968*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr -18606983912.8*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr 5.59472225864e+12*$e0] -e0 $e0 -tilt 0.0 


Multipole -name "SF5FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.00045261578508*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SF5FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr 2.4734711897*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF5FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr 13.3041535439*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF5FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr -5188.70511108*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF5FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr -1168376.15047*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF5FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr 643267292.028*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF5FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr 8.07980504623e+12*$e0] -e0 $e0 -tilt 0.785398163397448  


Multipole -name "SF5FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr 0.643396082373034*$e0] -e0 $e0 

Multipole -name "SF5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.00226101296598*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 10.50445657081*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 9.73003860997*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr 6243.1712849*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr -1478359.53968*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr -18606983912.8*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF5FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr 5.59472225864e+12*$e0] -e0 $e0 -tilt 0.0 


Multipole -name "SF5FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.00045261578508*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SF5FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr 2.4734711897*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF5FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr 13.3041535439*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF5FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr -5188.70511108*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF5FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr -1168376.15047*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF5FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr 643267292.028*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF5FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr 8.07980504623e+12*$e0] -e0 $e0 -tilt 0.785398163397448



Drift -name "L217A" -length 0.099791984 
Bpm -name "SF5FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L217B" -length 0.310924 

Multipole -name "QF5AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00409637980231*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF5AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.575145620756*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QF5AFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00621035098497] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF5AFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.10640508096*$e0] -e0 $e0 -tilt 0.785398163397448 

Quadrupole -name "QF5AFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.188028317183970*$e0] -e0 $e0 

Multipole -name "QF5AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00409637980231*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF5AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.575145620756*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QF5AFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00621035098497] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF5AFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.10640508096*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "AQF5AFF" -length 0

Multipole -name "QF5AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00409637980231*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF5AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.575145620756*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QF5AFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00621035098497] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF5AFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.10640508096*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QF5AFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.188028317183970*$e0] -e0 $e0 

Multipole -name "QF5AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00409637980231*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF5AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.575145620756*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QF5AFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00621035098497] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF5AFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.10640508096*$e0] -e0 $e0 -tilt 0.785398163397448 

Drift -name "L218A" -length 0.019755 
Bpm -name "QF5AFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L218B" -length 0.223 
Drift -name "MREF2FF" -length 0 
Drift -name "L218C" -length 0.858755 

Multipole -name "QD4BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0346828567824*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD4BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.23335787352*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD4BFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00707748196547*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD4BFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.689912899433*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QD4BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.148396136765705*$e0] -e0 $e0 

Multipole -name "QD4BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0346828567824*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD4BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.23335787352*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD4BFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00707748196547*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD4BFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.689912899433*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "AQD4BFF" -length 0 

Multipole -name "QD4BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0346828567824*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD4BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.23335787352*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD4BFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00707748196547*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD4BFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.689912899433*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QD4BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.148396136765705*$e0] -e0 $e0 

Multipole -name "QD4BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0346828567824*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD4BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.23335787352*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD4BFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00707748196547*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD4BFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.689912899433*$e0] -e0 $e0 -tilt 0.785398163397448
Drift -name "L219A" -length 0.019755 

Bpm -name "QD4BFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L219B" -length 0.390960984 

Multipole -name "SD4FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.820185089287*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -9.486283072*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 10070.5544685*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr -1031677.07471*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr -1358243588.84*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr 1.29647439312e+13*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr 3.07691114388e+14*$e0] -e0 $e0 -tilt 0.0 

Multipole -name "SD4FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.18538156307*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD4FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr 32.865998668*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD4FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr 3589.92464627*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD4FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr 1236500.94416*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD4FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr 511612686.928*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD4FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr 2.24036989866e+11*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD4FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -7.55761864256e+13*$e0] -e0 $e0 -tilt 0.785398163397448

Multipole -name "SD4FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr 7.478357276734121*$e0] -e0 $e0 

Multipole -name "SD4FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.820185089287*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -9.486283072*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 10070.5544685*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr -1031677.07471*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr -1358243588.84*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr 1.29647439312e+13*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr 3.07691114388e+14*$e0] -e0 $e0 -tilt 0.0 

Multipole -name "SD4FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.18538156307*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD4FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr 32.865998668*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD4FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr 3589.92464627*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD4FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr 1236500.94416*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD4FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr 511612686.928*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD4FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr 2.24036989866e+11*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD4FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -7.55761864256e+13*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "ASD4FF" -length 0 

Multipole -name "SD4FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.820185089287*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -9.486283072*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 10070.5544685*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr -1031677.07471*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr -1358243588.84*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr 1.29647439312e+13*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr 3.07691114388e+14*$e0] -e0 $e0 -tilt 0.0 

Multipole -name "SD4FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.18538156307*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD4FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr 32.865998668*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD4FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr 3589.92464627*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD4FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr 1236500.94416*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD4FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr 511612686.928*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD4FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr 2.24036989866e+11*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD4FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -7.55761864256e+13*$e0] -e0 $e0 -tilt 0.785398163397448

Multipole -name "SD4FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr 7.478357276734121*$e0] -e0 $e0
 
Multipole -name "SD4FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.820185089287*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -9.486283072*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 10070.5544685*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr -1031677.07471*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr -1358243588.84*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr 1.29647439312e+13*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD4FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr 3.07691114388e+14*$e0] -e0 $e0 -tilt 0.0 

Multipole -name "SD4FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.18538156307*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD4FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr 32.865998668*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD4FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr 3589.92464627*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD4FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr 1236500.94416*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD4FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr 511612686.928*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD4FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr 2.24036989866e+11*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD4FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -7.55761864256e+13*$e0] -e0 $e0 -tilt 0.785398163397448 


Drift -name "L220A" -length 0.099791984 
Bpm -name "SD4FF" -length 0 -resolution $cband -short_range_wake $cband_wake

Drift -name "L220B" -length 0.310924 

Multipole -name "QD4AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0213323024319*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD4AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.172621108569*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD4AFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0245696053714*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD4AFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.08275135617*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QD4AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.148396136765705*$e0] -e0 $e0 

Multipole -name "QD4AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0213323024319*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD4AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.172621108569*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD4AFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0245696053714*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QD4AFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.08275135617*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "AQD4AFF" -length 0
 
Multipole -name "QD4AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0213323024319*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD4AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.172621108569*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD4AFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0245696053714*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QD4AFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.08275135617*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QD4AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.148396136765705*$e0] -e0 $e0 

Multipole -name "QD4AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0213323024319*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD4AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.172621108569*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD4AFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0245696053714*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QD4AFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.08275135617*$e0] -e0 $e0 -tilt 0.785398163397448
Drift -name "L221A" -length 0.019755 
Bpm -name "QD4AFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L221B" -length 0.266
 
Multipole -name "SK2FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr -0.136470217445000*$e0] -e0 $e0 -tilt -0.5235987756 
Multipole -name "SK2FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr -0.136470217445000*$e0] -e0 $e0 -tilt -0.5235987756 

Drift -name "L221C" -length 1.028629871 

Quadrupole -name "B2FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.5087937324e-05*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B2FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00321373065001*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "B2FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.22631905986*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B2FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -434.532594931*$e0] -e0 $e0  -tilt 0.0
Multipole -name "B2FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.003771984331*$e0] -e0 $e0 -tilt 0.785398163397448

Sbend -name "B2FFA" -synrad $sbend_synrad -length 0.306411625424 -angle -0.01508793732 -E1 -0.01508793732 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.01905 -fint 0.5 
set e0 [expr $e0-14.1e-6*-0.01508793732*-0.01508793732/0.3064116254*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0

Quadrupole -name "B2FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.5087937324e-05*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B2FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00321373065001*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "B2FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.22631905986*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B2FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -434.532594931*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B2FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.003771984331*$e0] -e0 $e0 -tilt 0.785398163397448 

Quadrupole -name "B2FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.5087937324e-05*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B2FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00321373065001*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "B2FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.22631905986*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B2FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -434.532594931*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B2FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.003771984331*$e0] -e0 $e0 -tilt 0.785398163397448

Sbend -name "B2FFB" -synrad $sbend_synrad -length 0.306411625424 -angle -0.01508793732 -E1 0 -E2 -0.01508793732 -six_dim 1 -e0 $e0 -hgap 0.01905 -fintx 0.5 
set e0 [expr $e0-14.1e-6*-0.01508793732*-0.01508793732/0.3064116254*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0

Quadrupole -name "B2FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.5087937324e-05*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B2FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00321373065001*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "B2FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.22631905986*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B2FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -434.532594931*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B2FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.003771984331*$e0] -e0 $e0 -tilt 0.785398163397448 

Drift -name "L222" -length 0.444384871 

Multipole -name "QF3FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0169023482717*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF3FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 3.70934915919*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF3FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0398089948747*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF3FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.674864266689*$e0] -e0 $e0 -tilt 0.785398163397448 

Quadrupole -name "QF3FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.276359936828130*$e0] -e0 $e0 

Multipole -name "QF3FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0169023482717*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF3FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 3.70934915919*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF3FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0398089948747*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF3FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.674864266689*$e0] -e0 $e0 -tilt 0.785398163397448 

Drift -name "AQF3FF" -length 0 

Multipole -name "QF3FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0169023482717*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF3FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 3.70934915919*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF3FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0398089948747*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF3FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.674864266689*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QF3FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.276359936828130*$e0] -e0 $e0 

Multipole -name "QF3FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0169023482717*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF3FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 3.70934915919*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QF3FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0398089948747*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF3FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.674864266689*$e0] -e0 $e0 -tilt 0.785398163397448  

Drift -name "L223A" -length 0.019755 
Bpm -name "QF3FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L223B" -length 0.4246045352
 
Quadrupole -name "B1FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -3.09374039255e-05*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0481547417622*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "B1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.08070627111*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1097.605287*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B1FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00672552259246*$e0] -e0 $e0 -tilt 0.785398163397448

Sbend -name "B1FFA" -synrad $sbend_synrad -length 0.306436961215 -angle -0.02690209037 -E1 -0.02690209037 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.01905 -fint 0.5 

set e0 [expr $e0-14.1e-6*-0.02690209037*-0.02690209037/0.3064369612*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0

Quadrupole -name "B1FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -3.09374039255e-05*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0481547417622*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "B1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.08070627111*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1097.605287*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B1FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00672552259246*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "B1FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -3.09374039255e-05*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0481547417622*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "B1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.08070627111*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1097.605287*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B1FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00672552259246*$e0] -e0 $e0 -tilt 0.785398163397448

Sbend -name "B1FFB" -synrad $sbend_synrad -length 0.306436961215 -angle -0.02690209037 -E1 0 -E2 -0.02690209037 -six_dim 1 -e0 $e0 -hgap 0.01905 -fintx 0.5 

set e0 [expr $e0-14.1e-6*-0.02690209037*-0.02690209037/0.3064369612*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0
Quadrupole -name "B1FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -3.09374039255e-05*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0481547417622*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "B1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.08070627111*$e0] -e0 $e0 -tilt 0.0
Multipole -name "B1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1097.605287*$e0] -e0 $e0 -tilt 0.0

Multipole -name "B1FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00672552259246*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "L224A" -length 0.2406045352 
Drift -name "MREF1FF" -length 0 
Drift -name "L224B" -length 0.203755 
Multipole -name "QD2BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0182915687598*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD2BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.38673504499*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD2BFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000360682241512*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD2BFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.540100572072*$e0] -e0 $e0 -tilt 0.785398163397448 

Quadrupole -name "QD2BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.099356484488870*$e0] -e0 $e0 

Multipole -name "QD2BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0182915687598*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD2BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.38673504499*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD2BFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000360682241512*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD2BFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.540100572072*$e0] -e0 $e0 -tilt 0.785398163397448 

Drift -name "AQD2BFF" -length 0 

Multipole -name "QD2BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0182915687598*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD2BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.38673504499*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD2BFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000360682241512*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD2BFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.540100572072*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QD2BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.099356484488870*$e0] -e0 $e0 

Multipole -name "QD2BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0182915687598*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD2BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 1.38673504499*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD2BFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000360682241512*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD2BFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.540100572072*$e0] -e0 $e0 -tilt 0.785398163397448 

Drift -name "L225A" -length 0.019755 
Bpm -name "QD2BFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L225B" -length 0.354 
Drift -name "MT1X" -length 0 
Drift -name "L225C" -length 0.137 
Drift -name "FFML1" -length 0 
Drift -name "L225D" -length 0.7235 
Drift -name "MREFIPFF" -length 0 
Drift -name "L225E" -length 0.267255 

Multipole -name "QD2AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00169994071866*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD2AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -3.23512713281*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD2AFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00763395098976*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD2AFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.126030933023*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QD2AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.144864048104645*$e0] -e0 $e0 

Multipole -name "QD2AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00169994071866*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD2AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -3.23512713281*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD2AFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00763395098976*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD2AFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.126030933023*$e0] -e0 $e0 -tilt 0.785398163397448 

Drift -name "AQD2AFF" -length 0 

Multipole -name "QD2AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00169994071866*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD2AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -3.23512713281*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD2AFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00763395098976*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD2AFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.126030933023*$e0] -e0 $e0 -tilt 0.785398163397448 

Quadrupole -name "QD2AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.144864048104645*$e0] -e0 $e0 

Multipole -name "QD2AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00169994071866*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD2AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -3.23512713281*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD2AFFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.00763395098976*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD2AFFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.126030933023*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "L226A" -length 0.019755 
Bpm -name "QD2AFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L226B" -length 0.391 
Drift -name "MREFSFF" -length 0 
Drift -name "L226C" -length 3.825 

Multipole -name "SK1FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0.002264070190050*$e0] -e0 $e0 -tilt -0.523598775598 
Multipole -name "SK1FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0.002264070190050*$e0] -e0 $e0 -tilt -0.523598775598 

Drift -name "L226D" -length 0.161 
Bpm -name "SF1FF" -length 0 -resolution $sband -short_range_wake $sband_wake

TclCall -script {BeamDump -file L226E.out}

Drift -name "L226E" -length 0.099 

Multipole -name "SF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.0280907052902*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 64.192335406*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 277.871630814*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr -2454099.79413*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr 725900236.566*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr -2.70783785196e+12*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr 2.03061273258e+12*$e0] -e0 $e0 -tilt 0.0 

Multipole -name "SF1FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.147256634053*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SF1FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1.3276458521e-11*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SF1FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr -5302.10656927*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SF1FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr -927327.036952*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF1FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr 1092567688.29*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF1FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr -4.12228915787e+12*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF1FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -1.15161770726e+13*$e0] -e0 $e0 -tilt 0.785398163397448 

Multipole -name "SF1FF" -synrad $mult_synrad -type 3 -length 0.05 -strength [expr -1.431611355500000*$e0] -e0 $e0
 
Multipole -name "SF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.0280907052902*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 64.192335406*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 277.871630814*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr -2454099.79413*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr 725900236.566*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr -2.70783785196e+12*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr 2.03061273258e+12*$e0] -e0 $e0 -tilt 0.0 

Multipole -name "SF1FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.147256634053*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SF1FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1.3276458521e-11*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SF1FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr -5302.10656927*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SF1FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr -927327.036952*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF1FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr 1092567688.29*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF1FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr -4.12228915787e+12*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF1FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -1.15161770726e+13*$e0] -e0 $e0 -tilt 0.785398163397448 


Drift -name "ASF1FF" -length 0 

Multipole -name "SF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.0280907052902*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 64.192335406*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 277.871630814*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr -2454099.79413*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr 725900236.566*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr -2.70783785196e+12*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr 2.03061273258e+12*$e0] -e0 $e0 -tilt 0.0 

Multipole -name "SF1FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.147256634053*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SF1FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1.3276458521e-11*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SF1FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr -5302.10656927*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SF1FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr -927327.036952*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF1FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr 1092567688.29*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF1FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr -4.12228915787e+12*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF1FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -1.15161770726e+13*$e0] -e0 $e0 -tilt 0.785398163397448  

Multipole -name "SF1FF" -synrad $mult_synrad -type 3 -length 0.05 -strength [expr -1.431611355500000*$e0] -e0 $e0 

Multipole -name "SF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.0280907052902*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 64.192335406*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 277.871630814*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr -2454099.79413*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr 725900236.566*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr -2.70783785196e+12*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SF1FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr 2.03061273258e+12*$e0] -e0 $e0 -tilt 0.0 

Multipole -name "SF1FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.147256634053*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SF1FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1.3276458521e-11*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SF1FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr -5302.10656927*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SF1FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr -927327.036952*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF1FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr 1092567688.29*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF1FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr -4.12228915787e+12*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SF1FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -1.15161770726e+13*$e0] -e0 $e0 -tilt 0.785398163397448 


Drift -name "L227" -length 0.30295 

Multipole -name "QF1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000635071497181*$e0] -e0 $e0 
Multipole -name "QF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.0676215416559*$e0] -e0 $e0 
Multipole -name "QF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -0.331269280842*$e0] -e0 $e0 
Multipole -name "QF1FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 1983.46353926*$e0] -e0 $e0 
Multipole -name "QF1FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr 8342.26879472*$e0] -e0 $e0 
Multipole -name "QF1FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr -1499015.92339*$e0] -e0 $e0 
Multipole -name "QF1FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr 545924402.824*$e0] -e0 $e0 
Multipole -name "QF1FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr -4.99027591866e+11*$e0] -e0 $e0 

Multipole -name "QF1FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00100419568748*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF1FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.011534469196*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF1FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1.60669846825*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF1FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr -47.7820377319*$e0] -e0 $e0  -tilt 0.785398163397448
Multipole -name "QF1FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr -6218.36201899*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF1FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr -39776.732273*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF1FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr 787240116.805*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF1FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -56857012238.7*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QF1FF" -synrad $quad_synrad -length 0.22205 -strength [expr 0.369500390427450*$e0] -e0 $e0 

Multipole -name "QF1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000635071497181*$e0] -e0 $e0 
Multipole -name "QF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.0676215416559*$e0] -e0 $e0 
Multipole -name "QF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -0.331269280842*$e0] -e0 $e0 
Multipole -name "QF1FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 1983.46353926*$e0] -e0 $e0 
Multipole -name "QF1FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr 8342.26879472*$e0] -e0 $e0 
Multipole -name "QF1FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr -1499015.92339*$e0] -e0 $e0 
Multipole -name "QF1FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr 545924402.824*$e0] -e0 $e0 
Multipole -name "QF1FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr -4.99027591866e+11*$e0] -e0 $e0 

Multipole -name "QF1FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00100419568748*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF1FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.011534469196*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF1FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1.60669846825*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF1FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr -47.7820377319*$e0] -e0 $e0  -tilt 0.785398163397448
Multipole -name "QF1FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr -6218.36201899*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF1FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr -39776.732273*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF1FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr 787240116.805*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF1FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -56857012238.7*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "AQF1FF" -length 0 

Multipole -name "QF1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000635071497181*$e0] -e0 $e0 
Multipole -name "QF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.0676215416559*$e0] -e0 $e0 
Multipole -name "QF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -0.331269280842*$e0] -e0 $e0 
Multipole -name "QF1FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 1983.46353926*$e0] -e0 $e0 
Multipole -name "QF1FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr 8342.26879472*$e0] -e0 $e0 
Multipole -name "QF1FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr -1499015.92339*$e0] -e0 $e0 
Multipole -name "QF1FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr 545924402.824*$e0] -e0 $e0 
Multipole -name "QF1FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr -4.99027591866e+11*$e0] -e0 $e0 

Multipole -name "QF1FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00100419568748*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF1FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.011534469196*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF1FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1.60669846825*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF1FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr -47.7820377319*$e0] -e0 $e0  -tilt 0.785398163397448
Multipole -name "QF1FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr -6218.36201899*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF1FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr -39776.732273*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF1FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr 787240116.805*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF1FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -56857012238.7*$e0] -e0 $e0 -tilt 0.785398163397448

Quadrupole -name "QF1FF" -synrad $quad_synrad -length 0.22205 -strength [expr 0.369500390427450*$e0] -e0 $e0 

Multipole -name "QF1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.000635071497181*$e0] -e0 $e0 
Multipole -name "QF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.0676215416559*$e0] -e0 $e0 
Multipole -name "QF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -0.331269280842*$e0] -e0 $e0 
Multipole -name "QF1FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 1983.46353926*$e0] -e0 $e0 
Multipole -name "QF1FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr 8342.26879472*$e0] -e0 $e0 
Multipole -name "QF1FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr -1499015.92339*$e0] -e0 $e0 
Multipole -name "QF1FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr 545924402.824*$e0] -e0 $e0 
Multipole -name "QF1FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr -4.99027591866e+11*$e0] -e0 $e0 

Multipole -name "QF1FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00100419568748*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF1FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.011534469196*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF1FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1.60669846825*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF1FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr -47.7820377319*$e0] -e0 $e0  -tilt 0.785398163397448
Multipole -name "QF1FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr -6218.36201899*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF1FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr -39776.732273*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "QF1FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr 787240116.805*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QF1FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -56857012238.7*$e0] -e0 $e0 -tilt 0.785398163397448

TclCall -script {BeamDump -file L228A.out}

Drift -name "L228A" -length 0.12195 
# not present anymore
#Bpm -name "MQF1FF" -length 0 
Drift -name "L228B" -length 0.297 
Bpm -name "SD0FF" -length 0 -resolution $sband -short_range_wake $sband_wake
Drift -name "L228C" -length 0.099 

Multipole -name "SD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.282478384109*$e0] -e0 $e0 -tilt 0.0
Multipole -name "SD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -78.9073124118*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD0FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 762.050971552*$e0] -e0 $e0 -tilt 0.0
Multipole -name "SD0FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr -1015441.78218*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD0FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr -2672512006.28*$e0] -e0 $e0 -tilt 0.0
Multipole -name "SD0FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr -8.14179365321e+12*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD0FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr -6.32931694183e+13*$e0] -e0 $e0 -tilt 0.0 

Multipole -name "SD0FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.0896074217139*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD0FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr 1.63198558459e-11*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD0FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr -5563.10776166*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD0FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr 1100423.71939*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD0FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr 1079764939.4*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD0FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr -5.16693936896e+12*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD0FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -4.59851793148e+13*$e0] -e0 $e0 -tilt 0.785398163397448 

Multipole -name "SD0FF" -synrad $mult_synrad -type 3 -length 0.05 -strength [expr  2.168306738000000*$e0] -e0 $e0 

Multipole -name "SD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.282478384109*$e0] -e0 $e0 -tilt 0.0
Multipole -name "SD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -78.9073124118*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD0FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 762.050971552*$e0] -e0 $e0 -tilt 0.0
Multipole -name "SD0FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr -1015441.78218*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD0FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr -2672512006.28*$e0] -e0 $e0 -tilt 0.0
Multipole -name "SD0FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr -8.14179365321e+12*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD0FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr -6.32931694183e+13*$e0] -e0 $e0 -tilt 0.0 

Multipole -name "SD0FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.0896074217139*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD0FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr 1.63198558459e-11*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD0FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr -5563.10776166*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD0FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr 1100423.71939*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD0FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr 1079764939.4*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD0FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr -5.16693936896e+12*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD0FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -4.59851793148e+13*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "ASD0FF" -length 0
 
Multipole -name "SD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.282478384109*$e0] -e0 $e0 -tilt 0.0
Multipole -name "SD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -78.9073124118*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD0FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 762.050971552*$e0] -e0 $e0 -tilt 0.0
Multipole -name "SD0FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr -1015441.78218*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD0FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr -2672512006.28*$e0] -e0 $e0 -tilt 0.0
Multipole -name "SD0FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr -8.14179365321e+12*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD0FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr -6.32931694183e+13*$e0] -e0 $e0 -tilt 0.0 

Multipole -name "SD0FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.0896074217139*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD0FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr 1.63198558459e-11*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD0FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr -5563.10776166*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD0FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr 1100423.71939*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD0FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr 1079764939.4*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD0FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr -5.16693936896e+12*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD0FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -4.59851793148e+13*$e0] -e0 $e0 -tilt 0.785398163397448

Multipole -name "SD0FF" -synrad $mult_synrad -type 3 -length 0.05 -strength [expr 2.168306738000000*$e0] -e0 $e0 

Multipole -name "SD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.282478384109*$e0] -e0 $e0 -tilt 0.0
Multipole -name "SD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -78.9073124118*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD0FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 762.050971552*$e0] -e0 $e0 -tilt 0.0
Multipole -name "SD0FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr -1015441.78218*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD0FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr -2672512006.28*$e0] -e0 $e0 -tilt 0.0
Multipole -name "SD0FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr -8.14179365321e+12*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "SD0FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr -6.32931694183e+13*$e0] -e0 $e0 -tilt 0.0 

Multipole -name "SD0FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr -0.0896074217139*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD0FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr 1.63198558459e-11*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD0FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr -5563.10776166*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD0FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr 1100423.71939*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD0FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr 1079764939.4*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "SD0FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr -5.16693936896e+12*$e0] -e0 $e0 -tilt 0.785398163397448 
Multipole -name "SD0FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -4.59851793148e+13*$e0] -e0 $e0 -tilt 0.785398163397448

TclCall -script {BeamDump -file L229.out}


Drift -name "L229" -length 0.2875
 
Multipole -name "QD0FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0125489398514*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.900427003897*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 530.610871704*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD0FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 1468949.12081*$e0] -e0 $e0 -tilt 0.0  
Multipole -name "QD0FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr -8719489.49142*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD0FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr 424192589.083*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD0FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr -5111209249360*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD0FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr -33519383330200000*$e0] -e0 $e0 -tilt 0.0

Multipole -name "QD0FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0119836495496*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.582958305043*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr 247.427912977*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr 89330.1099209*$e0] -e0 $e0 -tilt 0.785398163397448  
Multipole -name "QD0FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr 10611503.6699*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr 97620699.2344*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr -572410695952*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -351026992977000*$e0] -e0 $e0 -tilt 0.785398163397448


Quadrupole -name "QD0FF" -synrad $quad_synrad -length 0.2375 -strength [expr -0.681447828800000*$e0] -e0 $e0 

Multipole -name "QD0FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0125489398514*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.900427003897*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 530.610871704*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD0FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 1468949.12081*$e0] -e0 $e0 -tilt 0.0  
Multipole -name "QD0FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr -8719489.49142*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD0FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr 424192589.083*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD0FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr -5111209249360*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD0FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr -33519383330200000*$e0] -e0 $e0 -tilt 0.0

Multipole -name "QD0FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0119836495496*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.582958305043*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr 247.427912977*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr 89330.1099209*$e0] -e0 $e0 -tilt 0.785398163397448  
Multipole -name "QD0FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr 10611503.6699*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr 97620699.2344*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr -572410695952*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -351026992977000*$e0] -e0 $e0 -tilt 0.785398163397448

Drift -name "AQD0FF" -length 0 

Multipole -name "QD0FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0125489398514*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.900427003897*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 530.610871704*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD0FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 1468949.12081*$e0] -e0 $e0 -tilt 0.0  
Multipole -name "QD0FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr -8719489.49142*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD0FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr 424192589.083*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD0FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr -5111209249360*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD0FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr -33519383330200000*$e0] -e0 $e0 -tilt 0.0

Multipole -name "QD0FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0119836495496*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.582958305043*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr 247.427912977*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr 89330.1099209*$e0] -e0 $e0 -tilt 0.785398163397448  
Multipole -name "QD0FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr 10611503.6699*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr 97620699.2344*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr -572410695952*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -351026992977000*$e0] -e0 $e0 -tilt 0.785398163397448


Quadrupole -name "QD0FF" -synrad $quad_synrad -length 0.2375 -strength [expr -0.681447828800000*$e0] -e0 $e0 

Multipole -name "QD0FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0125489398514*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.900427003897*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 530.610871704*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD0FFMULT5" -synrad $mult_synrad -type 6 -length 0 -strength [expr 1468949.12081*$e0] -e0 $e0 -tilt 0.0  
Multipole -name "QD0FFMULT6" -synrad $mult_synrad -type 7 -length 0 -strength [expr -8719489.49142*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD0FFMULT7" -synrad $mult_synrad -type 8 -length 0 -strength [expr 424192589.083*$e0] -e0 $e0 -tilt 0.0 
Multipole -name "QD0FFMULT8" -synrad $mult_synrad -type 9 -length 0 -strength [expr -5111209249360*$e0] -e0 $e0 -tilt 0.0
Multipole -name "QD0FFMULT9" -synrad $mult_synrad -type 10 -length 0 -strength [expr -33519383330200000*$e0] -e0 $e0 -tilt 0.0

Multipole -name "QD0FFMULT2S" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0119836495496*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT3S" -synrad $mult_synrad -type 4 -length 0 -strength [expr 0.582958305043*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT4S" -synrad $mult_synrad -type 5 -length 0 -strength [expr 247.427912977*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT5S" -synrad $mult_synrad -type 6 -length 0 -strength [expr 89330.1099209*$e0] -e0 $e0 -tilt 0.785398163397448  
Multipole -name "QD0FFMULT6S" -synrad $mult_synrad -type 7 -length 0 -strength [expr 10611503.6699*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT7S" -synrad $mult_synrad -type 8 -length 0 -strength [expr 97620699.2344*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT8S" -synrad $mult_synrad -type 9 -length 0 -strength [expr -572410695952*$e0] -e0 $e0 -tilt 0.785398163397448
Multipole -name "QD0FFMULT9S" -synrad $mult_synrad -type 10 -length 0 -strength [expr -351026992977000*$e0] -e0 $e0 -tilt 0.785398163397448


TclCall -script {BeamDump -file L230A.out}

Drift -name "L230A" -length 0.1065 
# not present anymore
#Bpm -name "MQD0FF" -length 0 
Drift -name "L230B" -length 0.286 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "IPKICK" -length 0.12 
Drift -name "L230C" -length 0.048 
Bpm -name "PREIP" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
Drift -name "L230D" -length 0.2758
TclCall -script {BeamDump -file IPBPMA.out}
Bpm -name "IPBPMA" -length 0 -resolution $ipbpm
Drift -name "L230E" -length 0.081 
Bpm -name "IPBPMB" -length 0 -resolution $ipbpm
TclCall -script {BeamDump -file IPBPMB.out}
Drift -name "L230F" -length 0.0702 

TclCall -script {BeamDump -file L230F.out}
Drift -name "MS1IP" -length 0 
Drift -name "MBS1IP" -length 0 
Quadrupole -name "IP" -strength 0 -length 0 
TclCall -script {BeamDump -file IPparticles.out}
Drift -name "L231" -length 0.0882 
Bpm -name "IPBPMC" -length 0 -resolution $ipbpm
TclCall -script {BeamDump -file IPBPMC.out}
Drift -name "ENDFF" -length 0 
Drift -name "L301A" -length 0.3338 
Drift -name "MW1IP" -length 0 
Drift -name "L301B" -length 0.203 
# not sure about resolution of MPIP - setting to stripline
Bpm -name "M-PIP" -length 0 -resolution $stripline
Drift -name "L301C" -length 0.302 
Drift -name "MSPIP" -length 0 
Drift -name "L301D" -length 0.573 
Drift -name "MBDUMPA" -length 0 
Sbend -name "BDUMPA" -synrad $sbend_synrad -length 0.6784391641 -angle 0.1745329252 -E1 0.1745329252 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
set e0 [expr $e0-14.1e-6*0.1745329252*0.1745329252/0.6784391641*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BDUMPB" -synrad $sbend_synrad -length 0.6784391641 -angle 0.1745329252 -E1 0 -E2 0.1745329252 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
set e0 [expr $e0-14.1e-6*0.1745329252*0.1745329252/0.6784391641*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "MBDUMPB" -length 0 
Drift -name "L302A" -length 0.6345608359 
Drift -name "MDUMP" -length 0 
Drift -name "L302B" -length 0.146 
Drift -name "ICTDUMP" -length 0 
Drift -name "DIAMOND" -length 0 
Drift -name "L302C" -length 1.219439164 
Drift -name "DUMPH" -length 1.15 
Drift -name "DUMPH" -length 1.15 
Drift -name "ATF2MEND" -length 0 
