# version 5.2 converted from MadX

# lattice options that can be set externally

if {! [info exist stripline]} {
    set stripline 3.0
}
if {! [info exist stripline2]} {
    set stripline2 3.0
}
if {! [info exist font_stripline_resolution]} {
    set font_stripline_resolution 0.5
}
if {! [info exist cband]} {
    set cband 0.2
}
if {! [info exist cband_noatt]} {
    set cband_noatt 0.2
}
if {! [info exist sband]} {
    set sband 1.0
}
if {! [info exist ipbpm]} {
    set ipbpm 0.1
}

# wakefields:
if {! [info exist cband_wake]} {
    set cband_wake ""
}
if {! [info exist sband_wake]} {
    set sband_wake ""
}

Drift -name "ATF2MSTART" -length 0 
Bpm -name "START" -length 0 -resolution $stripline
Quadrupole -name "KEX1MULT1" -synrad $quad_synrad -length 0 -strength [expr -0.01846929*$e0] -e0 $e0 
Multipole -name "KEX1MULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -3.824591*$e0] -e0 $e0
Sbend -name "KEX1A" -synrad $sbend_synrad -length 0.2500010417 -angle 0.0025 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.00635 -fint 0.5 
#Bpm -name "RAUL" -length 0 -resolution $stripline 
set e0 [expr $e0-14.1e-6*0.0025*0.0025/0.2500010417*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Quadrupole -name "KEX1MULT1" -synrad $quad_synrad -length 0 -strength [expr -0.01846929*$e0] -e0 $e0 
Multipole -name "KEX1MULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -3.824591*$e0] -e0 $e0 
Drift -name "IP01" -length 0 
Quadrupole -name "KEX1MULT1" -synrad $quad_synrad -length 0 -strength [expr -0.01846929*$e0] -e0 $e0 
Multipole -name "KEX1MULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -3.824591*$e0] -e0 $e0 
Sbend -name "KEX1B" -synrad $sbend_synrad -length 0.2500010417 -angle 0.0025 -E1 0 -E2 0.005 -six_dim 1 -e0 $e0 -hgap 0.00635 -fintx 0.5 
set e0 [expr $e0-14.1e-6*0.0025*0.0025/0.2500010417*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Quadrupole -name "KEX1MULT1" -synrad $quad_synrad -length 0 -strength [expr -0.01846929*$e0] -e0 $e0 
Multipole -name "KEX1MULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -3.824591*$e0] -e0 $e0 
Drift -name "L001" -length 0.9231390392 
Sbend -name "QM6RX" -synrad $sbend_synrad -length 0.09937458845 -angle 0.0023109657 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -K [expr -0.3644313055*$e0] 
set e0 [expr $e0-14.1e-6*0.0023109657*0.0023109657/0.09937458845*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "QM6RX" -synrad $sbend_synrad -length 0.09937458845 -angle 0.0023109657 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -K [expr -0.3644313055*$e0] 
set e0 [expr $e0-14.1e-6*0.0023109657*0.0023109657/0.09937458845*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L002A" -length 0.04685466892 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH100RX" -length 0.24955 
Drift -name "L002B" -length 0.7028333967 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH101RX" -length 0.1679 
Drift -name "L002C" -length 0.05068234611 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV100RX" -length 0.13874 
Drift -name "L002D" -length 0 
#BPM MB1X not instrumented but present (button BPM)
#Bpm -name "MB1X" -length 0 
Drift -name "L002E" -length 0.1894692706 
Sbend -name "QM7RX" -synrad $sbend_synrad -length 0.0421716479 -angle -0.0045873988 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -K [expr 0.20411*$e0] 
set e0 [expr $e0-14.1e-6*-0.0045873988*-0.0045873988/0.0421716479*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "QM7RX" -synrad $sbend_synrad -length 0.0421716479 -angle -0.0045873988 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -K [expr 0.20411*$e0] 
set e0 [expr $e0-14.1e-6*-0.0045873988*-0.0045873988/0.0421716479*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L003" -length 0.1478305058 
Sbend -name "BS1XA" -synrad $sbend_synrad -length 0.3 -angle 0.01401783271 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
set e0 [expr $e0-14.1e-6*0.01401783271*0.01401783271/0.3*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BS1XB" -synrad $sbend_synrad -length 0.3 -angle 0.01401783271 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
set e0 [expr $e0-14.1e-6*0.01401783271*0.01401783271/0.3*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L004" -length 0.2 
Sbend -name "BS2XA" -synrad $sbend_synrad -length 0.4 -angle 0.03717168307 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
set e0 [expr $e0-14.1e-6*0.03717168307*0.03717168307/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BS2XB" -synrad $sbend_synrad -length 0.4 -angle 0.03717168307 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
set e0 [expr $e0-14.1e-6*0.03717168307*0.03717168307/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L005A" -length 0.1 
#BPM MB2X present (button BPM) but callibration very dificult in Y
Bpm -name "MB2X" -length 0 -resolution $stripline
Drift -name "L005B" -length 0.1 
Sbend -name "BS3XA" -synrad $sbend_synrad -length 0.5 -angle 0.1175110129 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
set e0 [expr $e0-14.1e-6*0.1175110129*0.1175110129/0.5*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BS3XB" -synrad $sbend_synrad -length 0.5 -angle 0.1175110129 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
set e0 [expr $e0-14.1e-6*0.1175110129*0.1175110129/0.5*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L006A" -length 0.4782345 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV1X" -length 0.128141 
Drift -name "L006B" -length 0.0335945 
Quadrupole -name "QS1X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Quadrupole -name "QS1X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Drift -name "L006C" -length 0.08136 
Quadrupole -name "QF1X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.5379118485*$e0] -e0 $e0 
Quadrupole -name "QF1X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.5379118485*$e0] -e0 $e0 
Drift -name "L007A" -length 0.014695 
Bpm -name "MQF1X" -length 0 -resolution $stripline2
Drift -name "L007B" -length 0.214 
Drift -name "MS1X" -length 0 
Drift -name "L007C" -length 0.271305 
Multipole -name "BH1XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.1839837463*$e0] -e0 $e0 
Sbend -name "BH1XA" -synrad $sbend_synrad -length 0.4 -angle 0.0941996781 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
set e0 [expr $e0-14.1e-6*0.0941996781*0.0941996781/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Multipole -name "BH1XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.1839837463*$e0] -e0 $e0 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZX1X" -length 0 
Multipole -name "BH1XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.1839837463*$e0] -e0 $e0 
Sbend -name "BH1XB" -synrad $sbend_synrad -length 0.4 -angle 0.0941996781 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
set e0 [expr $e0-14.1e-6*0.0941996781*0.0941996781/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Multipole -name "BH1XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.1839837463*$e0] -e0 $e0 
Drift -name "L008A" -length 0.2132345 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV2X" -length 0.128141 
Drift -name "L008B" -length 0.1586245 
Quadrupole -name "QD2X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.4708418816*$e0] -e0 $e0 
Quadrupole -name "QD2X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.4708418816*$e0] -e0 $e0 
Drift -name "L009A" -length 0.014695 
Bpm -name "MQD2X" -length 0 -resolution $stripline2
Drift -name "L009B" -length 0.7578050808 
Quadrupole -name "QF3X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3353340117*$e0] -e0 $e0 
Quadrupole -name "QF3X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3353340117*$e0] -e0 $e0 
Drift -name "L010A" -length 0.026695 
Bpm -name "MQF3X" -length 0 -resolution $stripline2
Drift -name "L010B" -length 0.138725 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH1X" -length 0.11455 
Drift -name "L010C" -length 0.1396545 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV3X" -length 0.128141 
Drift -name "L010D" -length 1.505663876 
Drift -name "MID" -length 0 
Drift -name "L010E" -length 2.053429376 
Quadrupole -name "QF4X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3393545966*$e0] -e0 $e0 
Quadrupole -name "QF4X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3393545966*$e0] -e0 $e0 
Drift -name "L011A" -length 0.026695 
Bpm -name "MQF4X" -length 0 -resolution $stripline2
Drift -name "L011B" -length 0.134725 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH2X" -length 0.11455 
Drift -name "L011C" -length 0.4698350808 
Bpm -name "MQD5X" -length 0 -resolution $stripline2
Drift -name "L011D" -length 0.026695 
Quadrupole -name "QD5X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.4607728075*$e0] -e0 $e0 
Quadrupole -name "QD5X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.4607728075*$e0] -e0 $e0 
Drift -name "L012A" -length 0.1576245 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV4X" -length 0.128141 
Drift -name "L012B" -length 0.2142345 
Multipole -name "BH2XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.1839837463*$e0] -e0 $e0 
Sbend -name "BH2XA" -synrad $sbend_synrad -length 0.4 -angle -0.0941996781 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
set e0 [expr $e0-14.1e-6*-0.0941996781*-0.0941996781/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Multipole -name "BH2XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.1839837463*$e0] -e0 $e0 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZX2X" -length 0 
Multipole -name "BH2XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.1839837463*$e0] -e0 $e0 
Sbend -name "BH2XB" -synrad $sbend_synrad -length 0.4 -angle -0.0941996781 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
set e0 [expr $e0-14.1e-6*-0.0941996781*-0.0941996781/0.4*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Multipole -name "BH2XMULT" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.1839837463*$e0] -e0 $e0 
Drift -name "L013A" -length 0.2142345 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV5X" -length 0.128141 
Drift -name "L013B" -length 0.1429295 
Bpm -name "MQF6X" -length 0 -resolution $stripline
Drift -name "L013C" -length 0.014695 
Quadrupole -name "QF6X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.5650485124*$e0] -e0 $e0 
Quadrupole -name "QF6X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.5650485124*$e0] -e0 $e0 
Drift -name "L014A" -length 0.00536 
Quadrupole -name "QS2X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Quadrupole -name "QS2X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Drift -name "L014B" -length 0.334665 
Drift -name "UDDX1" -length 0 
Drift -name "L014C" -length 0.5875079196 
Sbend -name "BH3XA" -synrad $sbend_synrad -length 0.675 -angle -0.1714240956 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
set e0 [expr $e0-14.1e-6*-0.1714240956*-0.1714240956/0.675*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BH3XB" -synrad $sbend_synrad -length 0.675 -angle -0.1714240956 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
set e0 [expr $e0-14.1e-6*-0.1714240956*-0.1714240956/0.675*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L015" -length 0.7650382419 
Quadrupole -name "QF7X" -synrad $quad_synrad -length 0.039335 -strength [expr 0.1915156418*$e0] -e0 $e0 
Quadrupole -name "QF7X" -synrad $quad_synrad -length 0.039335 -strength [expr 0.1915156418*$e0] -e0 $e0 
Drift -name "L016A" -length 0.026665 
Bpm -name "MQF7X" -length 0 -resolution $stripline
Drift -name "L016B" -length 1.767246207 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH3X" -length 0.11455 
Drift -name "L016C" -length 0.16342 
Quadrupole -name "QD8X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.2950188224*$e0] -e0 $e0 
Quadrupole -name "QD8X" -synrad $quad_synrad -length 0.099305 -strength [expr -0.2950188224*$e0] -e0 $e0 
Drift -name "L017A" -length 0.014695 
Bpm -name "MQD8X" -length 0 -resolution $stripline
Drift -name "L017B" -length 0.1389295 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV6X" -length 0.128141 
Drift -name "L017C" -length 0.6855607408 
Drift -name "KEX2A" -length 0.2500010417 
Drift -name "IP02" -length 0 
Drift -name "KEX2B" -length 0.2500010417 
Drift -name "MDISP" -length 0 
Drift -name "L101A" -length 0.06027549974 
Sbend -name "BKXA" -synrad $sbend_synrad -length 0.1 -angle 0.0025 -E1 0.005 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.05 -fint 0.5 
set e0 [expr $e0-14.1e-6*0.0025*0.0025/0.1*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BKXB" -synrad $sbend_synrad -length 0.1 -angle 0.0025 -E1 0 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.05 -fintx 0.5 
set e0 [expr $e0-14.1e-6*0.0025*0.0025/0.1*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "L101B" -length 0.166 
Bpm -name "MQF9X" -length 0 -resolution $stripline
Drift -name "L101C" -length 0.054695 
Quadrupole -name "QF9X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3814832824*$e0] -e0 $e0 
Quadrupole -name "QF9X" -synrad $quad_synrad -length 0.099305 -strength [expr 0.3814832824*$e0] -e0 $e0 
Drift -name "L102A" -length 0.15742 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH4X" -length 0.11455 
Drift -name "L102B" -length 1.339476477 
Drift -name "FONTK1" -length 0 
Drift -name "L102C" -length 0.3339295 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV7X" -length 0.128141 
Drift -name "L102D" -length 0.0859295 
Bpm -name "FONTP1" -length 0 -resolution $font_stripline_resolution
Drift -name "L102E" -length 0.125665 
Quadrupole -name "QK1X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Quadrupole -name "QK1X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Drift -name "L102F" -length 0.10242 
Multipole -name "QD10XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.061259524*$e0] -e0 $e0 -tilt 0.2728979485 
Multipole -name "QD10XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-7.416610409*$e0] -e0 $e0 -tilt -0.4510025096 
Quadrupole -name "QD10X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5833119514*$e0] -e0 $e0 
Multipole -name "QD10XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.061259524*$e0] -e0 $e0 -tilt 0.2728979485 
Multipole -name "QD10XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-7.416610409*$e0] -e0 $e0 -tilt -0.4510025096 
Multipole -name "QD10XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.061259524*$e0] -e0 $e0 -tilt 0.2728979485 
Multipole -name "QD10XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-7.416610409*$e0] -e0 $e0 -tilt -0.4510025096 
Quadrupole -name "QD10X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5833119514*$e0] -e0 $e0 
Multipole -name "QD10XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.061259524*$e0] -e0 $e0 -tilt 0.2728979485 
Multipole -name "QD10XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-7.416610409*$e0] -e0 $e0 -tilt -0.4510025096 
Drift -name "L103A" -length 0.019755 
Bpm -name "MQD10X" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L103B" -length 0.9566 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH5X" -length 0.1248 
Drift -name "L103C" -length 0.150355 
Multipole -name "QF11XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02909535705*$e0] -e0 $e0 -tilt -0.2256002597 
Multipole -name "QF11XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*9.212898679*$e0] -e0 $e0 -tilt 0.1224996226 
Quadrupole -name "QF11X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.5116656769*$e0] -e0 $e0 
Multipole -name "QF11XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02909535705*$e0] -e0 $e0 -tilt -0.2256002597 
Multipole -name "QF11XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*9.212898679*$e0] -e0 $e0 -tilt 0.1224996226 
Drift -name "MSKEW" -length 0 
Multipole -name "QF11XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02909535705*$e0] -e0 $e0 -tilt -0.2256002597 
Multipole -name "QF11XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*9.212898679*$e0] -e0 $e0 -tilt 0.1224996226 
Quadrupole -name "QF11X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.5116656769*$e0] -e0 $e0 
Multipole -name "QF11XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02909535705*$e0] -e0 $e0 -tilt -0.2256002597 
Multipole -name "QF11XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*9.212898679*$e0] -e0 $e0 -tilt 0.1224996226 
Drift -name "L104A" -length 0.019755 
Bpm -name "MQF11X" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L104B" -length 0.403 
Drift -name "FONTK2" -length 0 
Drift -name "L104C" -length 0.3069295 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV8X" -length 0.128141 
Drift -name "L104D" -length 0.0899295 
Bpm -name "FONTP2" -length 0 -resolution $font_stripline_resolution
Drift -name "L104F" -length 0.12433 
Quadrupole -name "QK2X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Quadrupole -name "QK2X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Drift -name "L104G" -length 0.100755 
Multipole -name "QD12XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.07128526211*$e0] -e0 $e0 -tilt 0.1714028079 
Multipole -name "QD12XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-5.00930931*$e0] -e0 $e0 -tilt 0.643546056 
Quadrupole -name "QD12X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5116656769*$e0] -e0 $e0 
Multipole -name "QD12XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.07128526211*$e0] -e0 $e0 -tilt 0.1714028079 
Multipole -name "QD12XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-5.00930931*$e0] -e0 $e0 -tilt 0.643546056 
Multipole -name "QD12XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.07128526211*$e0] -e0 $e0 -tilt 0.1714028079 
Multipole -name "QD12XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-5.00930931*$e0] -e0 $e0 -tilt 0.643546056 
Quadrupole -name "QD12X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5116656769*$e0] -e0 $e0 
Multipole -name "QD12XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.07128526211*$e0] -e0 $e0 -tilt 0.1714028079 
Multipole -name "QD12XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-5.00930931*$e0] -e0 $e0 -tilt 0.643546056 
Drift -name "L105A" -length 0.019755 
Bpm -name "MQD12X" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L105B" -length 0.5351231519 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH6X" -length 0.11455 
Drift -name "L105C" -length 0.1653525 
Quadrupole -name "QF13X" -synrad $quad_synrad -length 0.0993725 -strength [expr 0.6841194487*$e0] -e0 $e0 
Quadrupole -name "QF13X" -synrad $quad_synrad -length 0.0993725 -strength [expr 0.6841194487*$e0] -e0 $e0 
Drift -name "L106A" -length 0.0546275 
Bpm -name "MQF13X" -length 0 -resolution $stripline
Drift -name "L106B" -length 0.7821670781 
Quadrupole -name "QD14X" -synrad $quad_synrad -length 0.0993725 -strength [expr -0.507718235*$e0] -e0 $e0 
Drift -name "FIT180" -length 0 
Quadrupole -name "QD14X" -synrad $quad_synrad -length 0.0993725 -strength [expr -0.507718235*$e0] -e0 $e0 
Drift -name "L107A" -length 0.0546275 
Bpm -name "MQD14X" -length 0 -resolution $stripline
Drift -name "L107B" -length 0.276 
Bpm -name "FONTP3" -length 0 -resolution $font_stripline_resolution
Drift -name "L107C" -length 0.2282645781 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH7X" -length 0.11455 
Drift -name "L107D" -length 0.1633525 
Quadrupole -name "QF15X" -synrad $quad_synrad -length 0.0993725 -strength [expr 0.6841194487*$e0] -e0 $e0 
Quadrupole -name "QF15X" -synrad $quad_synrad -length 0.0993725 -strength [expr 0.6841194487*$e0] -e0 $e0 
Drift -name "L108A" -length 0.0546275 
Bpm -name "MQF15X" -length 0 -resolution $stripline
Drift -name "L108B" -length 0.3156 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV9X" -length 0.1248 
Drift -name "L108D" -length 0.1603281519 
Quadrupole -name "QK3X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Quadrupole -name "QK3X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Drift -name "L108E" -length 0.100755 
Multipole -name "QD16XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01998975467*$e0] -e0 $e0 -tilt -0.2319377413 
Multipole -name "QD16XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-2.427444304*$e0] -e0 $e0 -tilt -0.6034986504 
Quadrupole -name "QD16X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5116656769*$e0] -e0 $e0 
Multipole -name "QD16XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01998975467*$e0] -e0 $e0 -tilt -0.2319377413 
Multipole -name "QD16XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-2.427444304*$e0] -e0 $e0 -tilt -0.6034986504 
Multipole -name "QD16XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01998975467*$e0] -e0 $e0 -tilt -0.2319377413 
Multipole -name "QD16XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-2.427444304*$e0] -e0 $e0 -tilt -0.6034986504 
Quadrupole -name "QD16X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.5116656769*$e0] -e0 $e0 
Multipole -name "QD16XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01998975467*$e0] -e0 $e0 -tilt -0.2319377413 
Multipole -name "QD16XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-2.427444304*$e0] -e0 $e0 -tilt -0.6034986504 
Drift -name "L109A" -length 0.019755 
Bpm -name "MQD16X" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L109B" -length 0.9546 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH8X" -length 0.1248 
Drift -name "L109C" -length 0.152355 
Multipole -name "QF17XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.005940438509*$e0] -e0 $e0 -tilt 0.6748486461 
Multipole -name "QF17XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.753578805*$e0] -e0 $e0 -tilt 0.09200095134 
Quadrupole -name "QF17X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.5116656769*$e0] -e0 $e0 
Multipole -name "QF17XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.005940438509*$e0] -e0 $e0 -tilt 0.6748486461 
Multipole -name "QF17XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.753578805*$e0] -e0 $e0 -tilt 0.09200095134 
Multipole -name "QF17XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.005940438509*$e0] -e0 $e0 -tilt 0.6748486461 
Multipole -name "QF17XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.753578805*$e0] -e0 $e0 -tilt 0.09200095134 
Quadrupole -name "QF17X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.5116656769*$e0] -e0 $e0 
Multipole -name "QF17XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.005940438509*$e0] -e0 $e0 -tilt 0.6748486461 
Multipole -name "QF17XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.753578805*$e0] -e0 $e0 -tilt 0.09200095134 
Drift -name "L110A" -length 0.019755 
Bpm -name "MQF17X" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L110B" -length 0.211 
Drift -name "MREF1X" -length 0 
Drift -name "L110C" -length 0.8401649959 
Quadrupole -name "QK4X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Quadrupole -name "QK4X" -synrad $quad_synrad -length 0.039335 -strength [expr 0*$e0] -e0 $e0 -tilt 0.7853981634 
Drift -name "L110D" -length 0.6575945 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV10X" -length 0.128141 
Drift -name "L110E" -length 0.2486845 
Multipole -name "QD18XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.008575901695*$e0] -e0 $e0 -tilt 0.530866035 
Multipole -name "QD18XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.348131746*$e0] -e0 $e0 -tilt 0.5677030286 
Quadrupole -name "QD18X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.3430360678*$e0] -e0 $e0 
Multipole -name "QD18XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.008575901695*$e0] -e0 $e0 -tilt 0.530866035 
Multipole -name "QD18XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.348131746*$e0] -e0 $e0 -tilt 0.5677030286 
Multipole -name "QD18XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.008575901695*$e0] -e0 $e0 -tilt 0.530866035 
Multipole -name "QD18XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.348131746*$e0] -e0 $e0 -tilt 0.5677030286 
Quadrupole -name "QD18X" -synrad $quad_synrad -length 0.099245 -strength [expr -0.3430360678*$e0] -e0 $e0 
Multipole -name "QD18XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.008575901695*$e0] -e0 $e0 -tilt 0.530866035 
Multipole -name "QD18XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.348131746*$e0] -e0 $e0 -tilt 0.5677030286 
Drift -name "L111A" -length 0.019755 
Bpm -name "MQD18X" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L111B" -length 0.276 
Drift -name "MW0X" -length 0 
Drift -name "L111C" -length 0.4046498952 
Drift -name "OTR0X" -length 0 
Drift -name "L111D" -length 0.2716 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH9X" -length 0.1248 
Drift -name "L111E" -length 0.149355 
Multipole -name "QF19XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002165323765*$e0] -e0 $e0 -tilt 0.4851101624 
Multipole -name "QF19XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.700254151*$e0] -e0 $e0 -tilt -0.3060050931 
Quadrupole -name "QF19X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.3275830205*$e0] -e0 $e0 
Multipole -name "QF19XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002165323765*$e0] -e0 $e0 -tilt 0.4851101624 
Multipole -name "QF19XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.700254151*$e0] -e0 $e0 -tilt -0.3060050931 
Multipole -name "QF19XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002165323765*$e0] -e0 $e0 -tilt 0.4851101624 
Multipole -name "QF19XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.700254151*$e0] -e0 $e0 -tilt -0.3060050931 
Quadrupole -name "QF19X" -synrad $quad_synrad -length 0.099245 -strength [expr 0.3275830205*$e0] -e0 $e0 
Multipole -name "QF19XMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002165323765*$e0] -e0 $e0 -tilt 0.4851101624 
Multipole -name "QF19XMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.700254151*$e0] -e0 $e0 -tilt -0.3060050931 
Drift -name "L112A" -length 0.019755 
Bpm -name "MQF19X" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L112B" -length 0.265 
Drift -name "MW1X" -length 0 
Drift -name "L112C" -length 0.386 
Drift -name "OTR1X" -length 0 
Drift -name "L112D" -length 0.309 
Drift -name "WCM" -length 0 
Drift -name "L112E" -length 0.4849295 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV11X" -length 0.128141 
Drift -name "L112F" -length 1.4372645 
Drift -name "PPMW" -length 0 
Drift -name "L112G" -length 0.469 
Drift -name "MW2X" -length 0 
Drift -name "L112H" -length 0.282 
Bpm -name "MQD20X" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L112I" -length 0.019665 
Quadrupole -name "QD20X" -synrad $quad_synrad -length 0.039335 -strength [expr -0.1511348642*$e0] -e0 $e0 
Drift -name "MEMIT" -length 0 
Quadrupole -name "QD20X" -synrad $quad_synrad -length 0.039335 -strength [expr -0.1511348642*$e0] -e0 $e0 
Drift -name "L113A" -length 0.466665 
Drift -name "OTR2X" -length 0 
Drift -name "L113B" -length 1.861065 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH10X" -length 0.11921 
Drift -name "L113C" -length 0.937395 
Drift -name "ICT1X" -length 0 
Drift -name "L113D" -length 0.415665 
Quadrupole -name "QF21X" -synrad $quad_synrad -length 0.039335 -strength [expr 0.2223564783*$e0] -e0 $e0 
Quadrupole -name "QF21X" -synrad $quad_synrad -length 0.039335 -strength [expr 0.2223564783*$e0] -e0 $e0 
Drift -name "L114A" -length 0.019665 
Bpm -name "MQF21X" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L114B" -length 0.274 
Drift -name "MW3X" -length 0 
Drift -name "L114C" -length 0.502 
Drift -name "OTR3X" -length 0 
Drift -name "L114D" -length 0.35 
Drift -name "TILTM" -length 0 
Drift -name "L114E" -length 0.54 
#not present anymore
#Bpm -name "IPBPM" -length 0 
Drift -name "ATIPBPM" -length 0 
Drift -name "L114F" -length 1.813335 
Drift -name "MW4X" -length 0 
Drift -name "L114G" -length 0.282 
Drift -name "BEGFF" -length 0
if {[info exists use_dispersion_ff] && $use_dispersion_ff} {
    TclCall -name "ADDDISPERSIONFFSTART" -script "add_dispersion_ff_start"
}
if {[info exists save_beam_ff_start] && $save_beam_ff_start} {
    TclCall -name "SAVEBEAMFFSTART" -script "save_beam_ff_start"
}
Bpm -name "MQM16FF" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
Drift -name "L200" -length 0.019755 
Multipole -name "QM16FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003590382417*$e0] -e0 $e0 -tilt -0.3676020139 
Multipole -name "QM16FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.145971342*$e0] -e0 $e0 -tilt -0.6634969671 
Quadrupole -name "QM16FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0*$e0] -e0 $e0 
Multipole -name "QM16FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003590382417*$e0] -e0 $e0 -tilt -0.3676020139 
Multipole -name "QM16FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.145971342*$e0] -e0 $e0 -tilt -0.6634969671 
Drift -name "AQM16FF" -length 0 
Multipole -name "QM16FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003590382417*$e0] -e0 $e0 -tilt -0.3676020139 
Multipole -name "QM16FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.145971342*$e0] -e0 $e0 -tilt -0.6634969671 
Quadrupole -name "QM16FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0*$e0] -e0 $e0 
Multipole -name "QM16FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003590382417*$e0] -e0 $e0 -tilt -0.3676020139 
Multipole -name "QM16FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.145971342*$e0] -e0 $e0 -tilt -0.6634969671 
Drift -name "L201A" -length 0.76315 
# WARNING: HCORRECTOR needs to be defined, no PLACET element
HCORRECTOR -name "ZH1FF" -length 0.11921 
Drift -name "L201B" -length 0.1353245 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "ZV1FF" -length 0.128141 
Drift -name "L201C" -length 0.1556845 
Multipole -name "QM15FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0009094236936*$e0] -e0 $e0 -tilt 0.507224233 
Multipole -name "QM15FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.4632096203*$e0] -e0 $e0 -tilt 0.1838965077 
Quadrupole -name "QM15FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.3199356679*$e0] -e0 $e0 
Multipole -name "QM15FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0009094236936*$e0] -e0 $e0 -tilt 0.507224233 
Multipole -name "QM15FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.4632096203*$e0] -e0 $e0 -tilt 0.1838965077 
Drift -name "AQM15FF" -length 0 
Multipole -name "QM15FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0009094236936*$e0] -e0 $e0 -tilt 0.507224233 
Multipole -name "QM15FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.4632096203*$e0] -e0 $e0 -tilt 0.1838965077 
Quadrupole -name "QM15FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.3199356679*$e0] -e0 $e0 
Multipole -name "QM15FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0009094236936*$e0] -e0 $e0 -tilt 0.507224233 
Multipole -name "QM15FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.4632096203*$e0] -e0 $e0 -tilt 0.1838965077 
Drift -name "L202A" -length 0.019755 
Bpm -name "MQM15FF" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
Drift -name "L202B" -length 1.281755 
Multipole -name "QM14FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03177746312*$e0] -e0 $e0 -tilt -0.8477670492 
Multipole -name "QM14FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-4.818911977*$e0] -e0 $e0 -tilt 0.1220007465 
Quadrupole -name "QM14FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.758062188*$e0] -e0 $e0 
Multipole -name "QM14FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03177746312*$e0] -e0 $e0 -tilt -0.8477670492 
Multipole -name "QM14FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-4.818911977*$e0] -e0 $e0 -tilt 0.1220007465 
Drift -name "AQM14FF" -length 0 
Multipole -name "QM14FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03177746312*$e0] -e0 $e0 -tilt -0.8477670492 
Multipole -name "QM14FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-4.818911977*$e0] -e0 $e0 -tilt 0.1220007465 
Quadrupole -name "QM14FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.758062188*$e0] -e0 $e0 
Multipole -name "QM14FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03177746312*$e0] -e0 $e0 -tilt -0.8477670492 
Multipole -name "QM14FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-4.818911977*$e0] -e0 $e0 -tilt 0.1220007465 
Drift -name "L203A" -length 0.019755 
Bpm -name "MQM14FF" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
if {[info exist use_mfb2ffkicker] } {
    Dipole -name "MFB2FFKICKER" -length 0.0 -strength_x 0.0 -strength_y 0.0
}
Drift -name "L203B" -length 0.557455 
Drift -name "LWML1" -length 0 
Drift -name "L203C" -length 0.0215 
Drift -name "LW1FF" -length 0 
Drift -name "L203D" -length 0.0215 
Drift -name "LWML2" -length 0 
Drift -name "L203E" -length 0.2035 
if {[info exists save_beam_mfb2ff] && $save_beam_mfb2ff} {
    TclCall -name "SAVEBEAMMFB2FF" -script "save_beam_mfb2ff"
}
Bpm -name "MFB2FF" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
Drift -name "AFB2FF" -length 0 
Drift -name "L203F" -length 0.4778 
Multipole -name "QM13FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002564596147*$e0] -e0 $e0 -tilt 0.4942315471 
Multipole -name "QM13FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*6.191676439*$e0] -e0 $e0 -tilt 0.040266308 
Quadrupole -name "QM13FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.4629159803*$e0] -e0 $e0 
Multipole -name "QM13FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002564596147*$e0] -e0 $e0 -tilt 0.4942315471 
Multipole -name "QM13FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*6.191676439*$e0] -e0 $e0 -tilt 0.040266308 
Drift -name "AQM13FF" -length 0 
Multipole -name "QM13FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002564596147*$e0] -e0 $e0 -tilt 0.4942315471 
Multipole -name "QM13FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*6.191676439*$e0] -e0 $e0 -tilt 0.040266308 
Quadrupole -name "QM13FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.4629159803*$e0] -e0 $e0 
Multipole -name "QM13FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.002564596147*$e0] -e0 $e0 -tilt 0.4942315471 
Multipole -name "QM13FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*6.191676439*$e0] -e0 $e0 -tilt 0.040266308 
Drift -name "L204A" -length 0.019755 
Bpm -name "MQM13FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L204B" -length 1.281755 
Multipole -name "QM12FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.009392199386*$e0] -e0 $e0 -tilt 1.041419048 
Multipole -name "QM12FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2866941739*$e0] -e0 $e0 -tilt -0.6337616359 
Quadrupole -name "QM12FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1164254228*$e0] -e0 $e0 
Multipole -name "QM12FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.009392199386*$e0] -e0 $e0 -tilt 1.041419048 
Multipole -name "QM12FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2866941739*$e0] -e0 $e0 -tilt -0.6337616359 
Drift -name "AQM12FF" -length 0 
Multipole -name "QM12FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.009392199386*$e0] -e0 $e0 -tilt 1.041419048 
Multipole -name "QM12FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2866941739*$e0] -e0 $e0 -tilt -0.6337616359 
Quadrupole -name "QM12FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1164254228*$e0] -e0 $e0 
Multipole -name "QM12FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.009392199386*$e0] -e0 $e0 -tilt 1.041419048 
Multipole -name "QM12FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2866941739*$e0] -e0 $e0 -tilt -0.6337616359 
Drift -name "L205A" -length 0.019755 
Bpm -name "MQM12FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L205B" -length 0.715755 
# not present
#Bpm -name "MFB1FF" -length 0 
Drift -name "L205C" -length 0.666 
# WARNING: Multipole options not defined
Drift -name "QM11FFMULT2" -length 0 -tilt 0.4292947267 
# WARNING: Multipole options not defined
Drift -name "QM11FFMULT3" -length 0 -tilt 0.7639750205 
Quadrupole -name "QM11FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.08000716598*$e0] -e0 $e0 
# WARNING: Multipole options not defined
Drift -name "QM11FFMULT2" -length 0 -tilt 0.4292947267 
# WARNING: Multipole options not defined
Drift -name "QM11FFMULT3" -length 0 -tilt 0.7639750205 
Drift -name "AQM11FF" -length 0 
# WARNING: Multipole options not defined
Drift -name "QM11FFMULT2" -length 0 -tilt 0.4292947267 
# WARNING: Multipole options not defined
Drift -name "QM11FFMULT3" -length 0 -tilt 0.7639750205 
Quadrupole -name "QM11FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.08000716598*$e0] -e0 $e0 
# WARNING: Multipole options not defined
Drift -name "QM11FFMULT2" -length 0 -tilt 0.4292947267 
# WARNING: Multipole options not defined
Drift -name "QM11FFMULT3" -length 0 -tilt 0.7639750205 
Drift -name "L206A" -length 0.019755 
Bpm -name "MQM11FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L206BA" -length 0.8408775 
# collimator cavity not connected
#Bpm -name "COLLBY" -length 0 
Drift -name "L206BB" -length 0.4408775 
Multipole -name "QD10BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003197634999*$e0] -e0 $e0 -tilt -0.882813608 
Multipole -name "QD10BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.407797338*$e0] -e0 $e0 -tilt -0.7077528722 
Quadrupole -name "QD10BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1450096657*$e0] -e0 $e0 
Multipole -name "QD10BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003197634999*$e0] -e0 $e0 -tilt -0.882813608 
Multipole -name "QD10BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.407797338*$e0] -e0 $e0 -tilt -0.7077528722 
Drift -name "AQD10BFF" -length 0 
Multipole -name "QD10BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003197634999*$e0] -e0 $e0 -tilt -0.882813608 
Multipole -name "QD10BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.407797338*$e0] -e0 $e0 -tilt -0.7077528722 
Quadrupole -name "QD10BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1450096657*$e0] -e0 $e0 
Multipole -name "QD10BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.003197634999*$e0] -e0 $e0 -tilt -0.882813608 
Multipole -name "QD10BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.407797338*$e0] -e0 $e0 -tilt -0.7077528722 
Drift -name "L207A" -length 0.019755 
#Bpm -name "MQD10BFF" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake #REMOVED
Drift -name "L207B" -length 0.477 
# collimator cavity not connected
#Bpm -name "COLLB" -length 0 
Drift -name "MREF3FF" -length 0 
# wakeFieldSetup on mover
if {[info exist wakeFieldSetup] && $wakeFieldSetup} {
    source ${lattice_dir}/ATF2_wakeFieldSetup.tcl
}
Drift -name "L207C" -length 0.204755 
Multipole -name "QD10AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.005931299368*$e0] -e0 $e0 -tilt -0.8561060094 
Multipole -name "QD10AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6223524834*$e0] -e0 $e0 -tilt -0.0202933426 
Quadrupole -name "QD10AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1450096657*$e0] -e0 $e0 
Multipole -name "QD10AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.005931299368*$e0] -e0 $e0 -tilt -0.8561060094 
Multipole -name "QD10AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6223524834*$e0] -e0 $e0 -tilt -0.0202933426 
Drift -name "AQD10AFF" -length 0 
Multipole -name "QD10AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.005931299368*$e0] -e0 $e0 -tilt -0.8561060094 
Multipole -name "QD10AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6223524834*$e0] -e0 $e0 -tilt -0.0202933426 
Quadrupole -name "QD10AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1450096657*$e0] -e0 $e0 
Multipole -name "QD10AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.005931299368*$e0] -e0 $e0 -tilt -0.8561060094 
Multipole -name "QD10AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6223524834*$e0] -e0 $e0 -tilt -0.0202933426 
Drift -name "L208A" -length 0.019755 
Bpm -name "MQD10AFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L208B" -length 0.289 
Drift -name "MS1FF" -length 0 
Drift -name "L208C" -length 0.792755 
Multipole -name "QF9BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.03025071081*$e0] -e0 $e0 -tilt 0.9789002044 
Multipole -name "QF9BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.4884584584*$e0] -e0 $e0 -tilt -0.5935711555 
Quadrupole -name "QF9BFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1893249839*$e0] -e0 $e0 
Multipole -name "QF9BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.03025071081*$e0] -e0 $e0 -tilt 0.9789002044 
Multipole -name "QF9BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.4884584584*$e0] -e0 $e0 -tilt -0.5935711555 
Drift -name "AQF9BFF" -length 0 
Multipole -name "QF9BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.03025071081*$e0] -e0 $e0 -tilt 0.9789002044 
Multipole -name "QF9BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.4884584584*$e0] -e0 $e0 -tilt -0.5935711555 
Quadrupole -name "QF9BFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1893249839*$e0] -e0 $e0 
Multipole -name "QF9BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.03025071081*$e0] -e0 $e0 -tilt 0.9789002044 
Multipole -name "QF9BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.4884584584*$e0] -e0 $e0 -tilt -0.5935711555 
Drift -name "L209A" -length 0.019755 
#Bpm -name "MQF9BFF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
Drift -name "L209B" -length 0.390960984 
Multipole -name "SF6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.9710366071*$e0] -e0 $e0 -tilt -0.5520476424 
Multipole -name "SF6FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 80.25517652*$e0] -e0 $e0 -tilt -0.2727949621 
# WARNING: Multipole options not defined
Drift -name "SF6FFMULT5" -length 0 -tilt -0.1902408885 
# WARNING: Multipole options not defined
Drift -name "SF6FFMULT6" -length 0 -tilt 0.207694181 
# WARNING: Multipole options not defined
Drift -name "SF6FFMULT7" -length 0 -tilt 0.01361356817 
# WARNING: Multipole options not defined
Drift -name "SF6FFMULT8" -length 0 -tilt -0.0005235987756 
# WARNING: Multipole options not defined
Drift -name "SF6FFMULT9" -length 0 -tilt 0.2935643802 
Multipole -name "SF6FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr 4.8828277*$e0] -e0 $e0 
Multipole -name "SF6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.9710366071*$e0] -e0 $e0 -tilt -0.5520476424 
Multipole -name "SF6FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 80.25517652*$e0] -e0 $e0 -tilt -0.2727949621 
# WARNING: Multipole options not defined
Drift -name "SF6FFMULT5" -length 0 -tilt -0.1902408885 
# WARNING: Multipole options not defined
Drift -name "SF6FFMULT6" -length 0 -tilt 0.207694181 
# WARNING: Multipole options not defined
Drift -name "SF6FFMULT7" -length 0 -tilt 0.01361356817 
# WARNING: Multipole options not defined
Drift -name "SF6FFMULT8" -length 0 -tilt -0.0005235987756 
# WARNING: Multipole options not defined
Drift -name "SF6FFMULT9" -length 0 -tilt 0.2935643802 
Drift -name "ASF6FF" -length 0 
Multipole -name "SF6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.9710366071*$e0] -e0 $e0 -tilt -0.5520476424 
Multipole -name "SF6FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 80.25517652*$e0] -e0 $e0 -tilt -0.2727949621 
# WARNING: Multipole options not defined
Drift -name "SF6FFMULT5" -length 0 -tilt -0.1902408885 
# WARNING: Multipole options not defined
Drift -name "SF6FFMULT6" -length 0 -tilt 0.207694181 
# WARNING: Multipole options not defined
Drift -name "SF6FFMULT7" -length 0 -tilt 0.01361356817 
# WARNING: Multipole options not defined
Drift -name "SF6FFMULT8" -length 0 -tilt -0.0005235987756 
# WARNING: Multipole options not defined
Drift -name "SF6FFMULT9" -length 0 -tilt 0.2935643802 
Multipole -name "SF6FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr 4.8828277*$e0] -e0 $e0 
Multipole -name "SF6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.9710366071*$e0] -e0 $e0 -tilt -0.5520476424 
Multipole -name "SF6FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 80.25517652*$e0] -e0 $e0 -tilt -0.2727949621 
# WARNING: Multipole options not defined
Drift -name "SF6FFMULT5" -length 0 -tilt -0.1902408885 
# WARNING: Multipole options not defined
Drift -name "SF6FFMULT6" -length 0 -tilt 0.207694181 
# WARNING: Multipole options not defined
Drift -name "SF6FFMULT7" -length 0 -tilt 0.01361356817 
# WARNING: Multipole options not defined
Drift -name "SF6FFMULT8" -length 0 -tilt -0.0005235987756 
# WARNING: Multipole options not defined
Drift -name "SF6FFMULT9" -length 0 -tilt 0.2935643802 
Drift -name "L210A" -length 0.099791984 
#Bpm -name "MSF6FF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
Drift -name "L210B" -length 0.310924 
Multipole -name "QF9AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02552335583*$e0] -e0 $e0 -tilt 0.297225583 
Multipole -name "QF9AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*2.57883347*$e0] -e0 $e0 -tilt 0.7565372179 
Quadrupole -name "QF9AFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1893249839*$e0] -e0 $e0 
Multipole -name "QF9AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02552335583*$e0] -e0 $e0 -tilt 0.297225583 
Multipole -name "QF9AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*2.57883347*$e0] -e0 $e0 -tilt 0.7565372179 
Drift -name "AQF9AFF" -length 0 
Multipole -name "QF9AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02552335583*$e0] -e0 $e0 -tilt 0.297225583 
Multipole -name "QF9AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*2.57883347*$e0] -e0 $e0 -tilt 0.7565372179 
Quadrupole -name "QF9AFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1893249839*$e0] -e0 $e0 
Multipole -name "QF9AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.02552335583*$e0] -e0 $e0 -tilt 0.297225583 
Multipole -name "QF9AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*2.57883347*$e0] -e0 $e0 -tilt 0.7565372179 
Drift -name "L211A" -length 0.019755 
Bpm -name "MQF9AFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L211B" -length 0.266 
Multipole -name "SK4FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Multipole -name "SK4FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Drift -name "L211C" -length 1.085755 
Multipole -name "QD8FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02084036463*$e0] -e0 $e0 -tilt 0.636230717 
Multipole -name "QD8FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-6.229332836*$e0] -e0 $e0 -tilt 0.0402518337 
Quadrupole -name "QD8FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.3021776993*$e0] -e0 $e0 
Multipole -name "QD8FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02084036463*$e0] -e0 $e0 -tilt 0.636230717 
Multipole -name "QD8FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-6.229332836*$e0] -e0 $e0 -tilt 0.0402518337 
Drift -name "AQD8FF" -length 0 
Multipole -name "QD8FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02084036463*$e0] -e0 $e0 -tilt 0.636230717 
Multipole -name "QD8FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-6.229332836*$e0] -e0 $e0 -tilt 0.0402518337 
Quadrupole -name "QD8FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.3021776993*$e0] -e0 $e0 
Multipole -name "QD8FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02084036463*$e0] -e0 $e0 -tilt 0.636230717 
Multipole -name "QD8FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-6.229332836*$e0] -e0 $e0 -tilt 0.0402518337 
Drift -name "L212A" -length 0.019755 
Bpm -name "MQD8FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L212B" -length 1.681755 
Multipole -name "QF7FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02376281692*$e0] -e0 $e0 -tilt 0.03989982269 
Multipole -name "QF7FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.720636134*$e0] -e0 $e0 -tilt -0.7129998768 
Quadrupole -name "QF7FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.2750809185*$e0] -e0 $e0 
Multipole -name "QF7FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02376281692*$e0] -e0 $e0 -tilt 0.03989982269 
Multipole -name "QF7FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.720636134*$e0] -e0 $e0 -tilt -0.7129998768 
Drift -name "AQF7FF" -length 0 
Multipole -name "QF7FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02376281692*$e0] -e0 $e0 -tilt 0.03989982269 
Multipole -name "QF7FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.720636134*$e0] -e0 $e0 -tilt -0.7129998768 
Quadrupole -name "QF7FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.2750809185*$e0] -e0 $e0 
Multipole -name "QF7FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.02376281692*$e0] -e0 $e0 -tilt 0.03989982269 
Multipole -name "QF7FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*4.720636134*$e0] -e0 $e0 -tilt -0.7129998768 
Drift -name "L213A" -length 0.019755 
Bpm -name "MQF7FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L213B" -length 0.4246097102 
Quadrupole -name "B5FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.995837329e-05*$e0] -e0 $e0 
Multipole -name "B5FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.05117313872*$e0] -e0 $e0 -tilt 0.01137769317 
Multipole -name "B5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6735950984*$e0] -e0 $e0 -tilt 0.7853981634 
Multipole -name "B5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -868.1892379*$e0] -e0 $e0 
Sbend -name "B5FFA" -synrad $sbend_synrad -length 0.3064317863 -angle -0.02494796661 -E1 -0.02494796661 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.01905 -fint 0.5 
set e0 [expr $e0-14.1e-6*-0.02494796661*-0.02494796661/0.3064317863*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Quadrupole -name "B5FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.995837329e-05*$e0] -e0 $e0 
Multipole -name "B5FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.05117313872*$e0] -e0 $e0 -tilt 0.01137769317 
Multipole -name "B5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6735950984*$e0] -e0 $e0 -tilt 0.7853981634 
Multipole -name "B5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -868.1892379*$e0] -e0 $e0 
Quadrupole -name "B5FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.995837329e-05*$e0] -e0 $e0 
Multipole -name "B5FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.05117313872*$e0] -e0 $e0 -tilt 0.01137769317 
Multipole -name "B5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6735950984*$e0] -e0 $e0 -tilt 0.7853981634 
Multipole -name "B5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -868.1892379*$e0] -e0 $e0 
Sbend -name "B5FFB" -synrad $sbend_synrad -length 0.3064317863 -angle -0.02494796661 -E1 0 -E2 -0.02494796661 -six_dim 1 -e0 $e0 -hgap 0.01905 -fintx 0.5 
set e0 [expr $e0-14.1e-6*-0.02494796661*-0.02494796661/0.3064317863*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Quadrupole -name "B5FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.995837329e-05*$e0] -e0 $e0 
Multipole -name "B5FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.05117313872*$e0] -e0 $e0 -tilt 0.01137769317 
Multipole -name "B5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.6735950984*$e0] -e0 $e0 -tilt 0.7853981634 
Multipole -name "B5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -868.1892379*$e0] -e0 $e0 
Drift -name "L214" -length 0.6443647102 
Multipole -name "QD6FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04580065396*$e0] -e0 $e0 -tilt 0.862729249 
Multipole -name "QD6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-9.14694104*$e0] -e0 $e0 -tilt -0.008496430778 
Quadrupole -name "QD6FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.301163606*$e0] -e0 $e0 
Multipole -name "QD6FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04580065396*$e0] -e0 $e0 -tilt 0.862729249 
Multipole -name "QD6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-9.14694104*$e0] -e0 $e0 -tilt -0.008496430778 
Drift -name "AQD6FF" -length 0 
Multipole -name "QD6FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04580065396*$e0] -e0 $e0 -tilt 0.862729249 
Multipole -name "QD6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-9.14694104*$e0] -e0 $e0 -tilt -0.008496430778 
Quadrupole -name "QD6FF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.301163606*$e0] -e0 $e0 
Multipole -name "QD6FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04580065396*$e0] -e0 $e0 -tilt 0.862729249 
Multipole -name "QD6FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-9.14694104*$e0] -e0 $e0 -tilt -0.008496430778 
Drift -name "L215A" -length 0.019755 
#Bpm -name "MQD6FF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
Drift -name "L215B" -length 1.066 
Multipole -name "SK3FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Multipole -name "SK3FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Drift -name "L215C" -length 0.285755 
Multipole -name "QF5BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.01745259381*$e0] -e0 $e0 -tilt 0.8505662066 
Multipole -name "QF5BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.762728669*$e0] -e0 $e0 -tilt 0.01199774474 
Quadrupole -name "QF5BFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1880283172*$e0] -e0 $e0 
Multipole -name "QF5BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.01745259381*$e0] -e0 $e0 -tilt 0.8505662066 
Multipole -name "QF5BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.762728669*$e0] -e0 $e0 -tilt 0.01199774474 
Drift -name "AQF5BFF" -length 0 
Multipole -name "QF5BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.01745259381*$e0] -e0 $e0 -tilt 0.8505662066 
Multipole -name "QF5BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.762728669*$e0] -e0 $e0 -tilt 0.01199774474 
Quadrupole -name "QF5BFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1880283172*$e0] -e0 $e0 
Multipole -name "QF5BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.01745259381*$e0] -e0 $e0 -tilt 0.8505662066 
Multipole -name "QF5BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.762728669*$e0] -e0 $e0 -tilt 0.01199774474 
Drift -name "L216A" -length 0.019755 
Bpm -name "MQF5BFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L216B" -length 0.390960984 
Multipole -name "SF5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.002305870916*$e0] -e0 $e0 -tilt -0.04939281783 
Multipole -name "SF5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -10.79174071*$e0] -e0 $e0 -tilt 0.5820673055 
# WARNING: Multipole options not defined
Drift -name "SF5FFMULT5" -length 0 -tilt 0.3670427417 
# WARNING: Multipole options not defined
Drift -name "SF5FFMULT6" -length 0 -tilt -0.3497390488 
# WARNING: Multipole options not defined
Drift -name "SF5FFMULT7" -length 0 -tilt -0.08360127117 
# WARNING: Multipole options not defined
Drift -name "SF5FFMULT8" -length 0 -tilt 0.003839724354 
# WARNING: Multipole options not defined
Drift -name "SF5FFMULT9" -length 0 -tilt 0.2176425577 
Multipole -name "SF5FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr -0.3954341426*$e0] -e0 $e0 
Multipole -name "SF5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.002305870916*$e0] -e0 $e0 -tilt -0.04939281783 
Multipole -name "SF5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -10.79174071*$e0] -e0 $e0 -tilt 0.5820673055 
# WARNING: Multipole options not defined
Drift -name "SF5FFMULT5" -length 0 -tilt 0.3670427417 
# WARNING: Multipole options not defined
Drift -name "SF5FFMULT6" -length 0 -tilt -0.3497390488 
# WARNING: Multipole options not defined
Drift -name "SF5FFMULT7" -length 0 -tilt -0.08360127117 
# WARNING: Multipole options not defined
Drift -name "SF5FFMULT8" -length 0 -tilt 0.003839724354 
# WARNING: Multipole options not defined
Drift -name "SF5FFMULT9" -length 0 -tilt 0.2176425577 
Drift -name "ASF5FF" -length 0 
Multipole -name "SF5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.002305870916*$e0] -e0 $e0 -tilt -0.04939281783 
Multipole -name "SF5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -10.79174071*$e0] -e0 $e0 -tilt 0.5820673055 
# WARNING: Multipole options not defined
Drift -name "SF5FFMULT5" -length 0 -tilt 0.3670427417 
# WARNING: Multipole options not defined
Drift -name "SF5FFMULT6" -length 0 -tilt -0.3497390488 
# WARNING: Multipole options not defined
Drift -name "SF5FFMULT7" -length 0 -tilt -0.08360127117 
# WARNING: Multipole options not defined
Drift -name "SF5FFMULT8" -length 0 -tilt 0.003839724354 
# WARNING: Multipole options not defined
Drift -name "SF5FFMULT9" -length 0 -tilt 0.2176425577 
Multipole -name "SF5FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr -0.3954341426*$e0] -e0 $e0 
Multipole -name "SF5FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.002305870916*$e0] -e0 $e0 -tilt -0.04939281783 
Multipole -name "SF5FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -10.79174071*$e0] -e0 $e0 -tilt 0.5820673055 
# WARNING: Multipole options not defined
Drift -name "SF5FFMULT5" -length 0 -tilt 0.3670427417 
# WARNING: Multipole options not defined
Drift -name "SF5FFMULT6" -length 0 -tilt -0.3497390488 
# WARNING: Multipole options not defined
Drift -name "SF5FFMULT7" -length 0 -tilt -0.08360127117 
# WARNING: Multipole options not defined
Drift -name "SF5FFMULT8" -length 0 -tilt 0.003839724354 
# WARNING: Multipole options not defined
Drift -name "SF5FFMULT9" -length 0 -tilt 0.2176425577 
Drift -name "L217A" -length 0.099791984 
#Bpm -name "MSF5FF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
Drift -name "L217B" -length 0.310924 
Multipole -name "QF5AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.007439676528*$e0] -e0 $e0 -tilt 0.3292327923 
Multipole -name "QF5AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.246966194*$e0] -e0 $e0 -tilt 0.512545994 
Quadrupole -name "QF5AFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1880283172*$e0] -e0 $e0 
Multipole -name "QF5AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.007439676528*$e0] -e0 $e0 -tilt 0.3292327923 
Multipole -name "QF5AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.246966194*$e0] -e0 $e0 -tilt 0.512545994 
Drift -name "AQF5AFF" -length 0 
Multipole -name "QF5AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.007439676528*$e0] -e0 $e0 -tilt 0.3292327923 
Multipole -name "QF5AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.246966194*$e0] -e0 $e0 -tilt 0.512545994 
Quadrupole -name "QF5AFF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.1880283172*$e0] -e0 $e0 
Multipole -name "QF5AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.007439676528*$e0] -e0 $e0 -tilt 0.3292327923 
Multipole -name "QF5AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.246966194*$e0] -e0 $e0 -tilt 0.512545994 
Drift -name "L218A" -length 0.019755 
#Bpm -name "MQF5AFF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
Drift -name "L218B" -length 0.223 
#Drift -name "MREF2FF" -length 0  #REMOVED
Drift -name "L218C" -length 0.858755 
Multipole -name "QD4BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03539761723*$e0] -e0 $e0 -tilt -0.06709968635 
Multipole -name "QD4BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.41320609*$e0] -e0 $e0 -tilt -0.1275036164 
Quadrupole -name "QD4BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1483961368*$e0] -e0 $e0 
Multipole -name "QD4BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03539761723*$e0] -e0 $e0 -tilt -0.06709968635 
Multipole -name "QD4BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.41320609*$e0] -e0 $e0 -tilt -0.1275036164 
Drift -name "AQD4BFF" -length 0 
Multipole -name "QD4BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03539761723*$e0] -e0 $e0 -tilt -0.06709968635 
Multipole -name "QD4BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.41320609*$e0] -e0 $e0 -tilt -0.1275036164 
Quadrupole -name "QD4BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1483961368*$e0] -e0 $e0 
Multipole -name "QD4BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03539761723*$e0] -e0 $e0 -tilt -0.06709968635 
Multipole -name "QD4BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.41320609*$e0] -e0 $e0 -tilt -0.1275036164 
Drift -name "L219A" -length 0.019755 
Bpm -name "MQD4BFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L219B" -length 0.390960984 
Multipole -name "SD4FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.441469053*$e0] -e0 $e0 -tilt -0.5440191278 
Multipole -name "SD4FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 34.2076517*$e0] -e0 $e0 -tilt -0.3703588673 
# WARNING: Multipole options not defined
Drift -name "SD4FFMULT5" -length 0 -tilt -0.05707226654 
# WARNING: Multipole options not defined
Drift -name "SD4FFMULT6" -length 0 -tilt -0.323733643 
# WARNING: Multipole options not defined
Drift -name "SD4FFMULT7" -length 0 -tilt -0.347669587 
# WARNING: Multipole options not defined
Drift -name "SD4FFMULT8" -length 0 -tilt -0.001919862177 
# WARNING: Multipole options not defined
Drift -name "SD4FFMULT9" -length 0 -tilt 0.02408554368 
Multipole -name "SD4FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr 7.817968185*$e0] -e0 $e0 
Multipole -name "SD4FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.441469053*$e0] -e0 $e0 -tilt -0.5440191278 
Multipole -name "SD4FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 34.2076517*$e0] -e0 $e0 -tilt -0.3703588673 
# WARNING: Multipole options not defined
Drift -name "SD4FFMULT5" -length 0 -tilt -0.05707226654 
# WARNING: Multipole options not defined
Drift -name "SD4FFMULT6" -length 0 -tilt -0.323733643 
# WARNING: Multipole options not defined
Drift -name "SD4FFMULT7" -length 0 -tilt -0.347669587 
# WARNING: Multipole options not defined
Drift -name "SD4FFMULT8" -length 0 -tilt -0.001919862177 
# WARNING: Multipole options not defined
Drift -name "SD4FFMULT9" -length 0 -tilt 0.02408554368 
Drift -name "ASD4FF" -length 0 
Multipole -name "SD4FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.441469053*$e0] -e0 $e0 -tilt -0.5440191278 
Multipole -name "SD4FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 34.2076517*$e0] -e0 $e0 -tilt -0.3703588673 
# WARNING: Multipole options not defined
Drift -name "SD4FFMULT5" -length 0 -tilt -0.05707226654 
# WARNING: Multipole options not defined
Drift -name "SD4FFMULT6" -length 0 -tilt -0.323733643 
# WARNING: Multipole options not defined
Drift -name "SD4FFMULT7" -length 0 -tilt -0.347669587 
# WARNING: Multipole options not defined
Drift -name "SD4FFMULT8" -length 0 -tilt -0.001919862177 
# WARNING: Multipole options not defined
Drift -name "SD4FFMULT9" -length 0 -tilt 0.02408554368 
Multipole -name "SD4FF" -synrad $mult_synrad -type 3 -length 0.050039016 -strength [expr 7.817968185*$e0] -e0 $e0 
Multipole -name "SD4FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*1.441469053*$e0] -e0 $e0 -tilt -0.5440191278 
Multipole -name "SD4FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 34.2076517*$e0] -e0 $e0 -tilt -0.3703588673 
# WARNING: Multipole options not defined
Drift -name "SD4FFMULT5" -length 0 -tilt -0.05707226654 
# WARNING: Multipole options not defined
Drift -name "SD4FFMULT6" -length 0 -tilt -0.323733643 
# WARNING: Multipole options not defined
Drift -name "SD4FFMULT7" -length 0 -tilt -0.347669587 
# WARNING: Multipole options not defined
Drift -name "SD4FFMULT8" -length 0 -tilt -0.001919862177 
# WARNING: Multipole options not defined
Drift -name "SD4FFMULT9" -length 0 -tilt 0.02408554368 
Drift -name "L220A" -length 0.099791984 
#Bpm -name "MSD4FF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
Drift -name "L220B" -length 0.310924 
Multipole -name "QD4AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03253817197*$e0] -e0 $e0 -tilt -0.2852693742 
Multipole -name "QD4AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1914310165*$e0] -e0 $e0 -tilt 0.1117542814 
Quadrupole -name "QD4AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1483961368*$e0] -e0 $e0 
Multipole -name "QD4AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03253817197*$e0] -e0 $e0 -tilt -0.2852693742 
Multipole -name "QD4AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1914310165*$e0] -e0 $e0 -tilt 0.1117542814 
Drift -name "AQD4AFF" -length 0 
Multipole -name "QD4AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03253817197*$e0] -e0 $e0 -tilt -0.2852693742 
Multipole -name "QD4AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1914310165*$e0] -e0 $e0 -tilt 0.1117542814 
Quadrupole -name "QD4AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1483961368*$e0] -e0 $e0 
Multipole -name "QD4AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.03253817197*$e0] -e0 $e0 -tilt -0.2852693742 
Multipole -name "QD4AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1914310165*$e0] -e0 $e0 -tilt 0.1117542814 
Drift -name "L221A" -length 0.019755 
#Bpm -name "MQD4AFF" -length 0 -resolution $cband -short_range_wake $cband_wake #REMOVED
Drift -name "L221B" -length 0.266 
Multipole -name "SK2FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Multipole -name "SK2FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Drift -name "L221C" -length 1.028629871 
Quadrupole -name "B2FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.508793732e-05*$e0] -e0 $e0 
Multipole -name "B2FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00495539408*$e0] -e0 $e0 -tilt 0.2883807683 
Multipole -name "B2FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.2263190599*$e0] -e0 $e0 
Multipole -name "B2FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -434.5325949*$e0] -e0 $e0 
Sbend -name "B2FFA" -synrad $sbend_synrad -length 0.3064116254 -angle -0.01508793732 -E1 -0.01508793732 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.01905 -fint 0.5 
set e0 [expr $e0-14.1e-6*-0.01508793732*-0.01508793732/0.3064116254*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Quadrupole -name "B2FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.508793732e-05*$e0] -e0 $e0 
Multipole -name "B2FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00495539408*$e0] -e0 $e0 -tilt 0.2883807683 
Multipole -name "B2FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.2263190599*$e0] -e0 $e0 
Multipole -name "B2FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -434.5325949*$e0] -e0 $e0 
Quadrupole -name "B2FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.508793732e-05*$e0] -e0 $e0 
Multipole -name "B2FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00495539408*$e0] -e0 $e0 -tilt 0.2883807683 
Multipole -name "B2FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.2263190599*$e0] -e0 $e0 
Multipole -name "B2FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -434.5325949*$e0] -e0 $e0 
Sbend -name "B2FFB" -synrad $sbend_synrad -length 0.3064116254 -angle -0.01508793732 -E1 0 -E2 -0.01508793732 -six_dim 1 -e0 $e0 -hgap 0.01905 -fintx 0.5 
set e0 [expr $e0-14.1e-6*-0.01508793732*-0.01508793732/0.3064116254*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Quadrupole -name "B2FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -1.508793732e-05*$e0] -e0 $e0 
Multipole -name "B2FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.00495539408*$e0] -e0 $e0 -tilt 0.2883807683 
Multipole -name "B2FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.2263190599*$e0] -e0 $e0 
Multipole -name "B2FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -434.5325949*$e0] -e0 $e0 
Drift -name "L222" -length 0.444384871 
Multipole -name "QF3FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0432486468*$e0] -e0 $e0 -tilt -0.6574387986 
Multipole -name "QF3FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.770240438*$e0] -e0 $e0 -tilt 0.04499189406 
Quadrupole -name "QF3FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.2763599368*$e0] -e0 $e0 
Multipole -name "QF3FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0432486468*$e0] -e0 $e0 -tilt -0.6574387986 
Multipole -name "QF3FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.770240438*$e0] -e0 $e0 -tilt 0.04499189406 
Drift -name "AQF3FF" -length 0 
Multipole -name "QF3FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0432486468*$e0] -e0 $e0 -tilt -0.6574387986 
Multipole -name "QF3FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.770240438*$e0] -e0 $e0 -tilt 0.04499189406 
Quadrupole -name "QF3FF" -synrad $quad_synrad -length 0.099245 -strength [expr 0.2763599368*$e0] -e0 $e0 
Multipole -name "QF3FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr 0.0432486468*$e0] -e0 $e0 -tilt -0.6574387986 
Multipole -name "QF3FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*3.770240438*$e0] -e0 $e0 -tilt 0.04499189406 
Drift -name "L223A" -length 0.019755 
Bpm -name "MQF3FF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L223B" -length 0.4246045352 
Quadrupole -name "B1FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -3.093740393e-05*$e0] -e0 $e0 
Multipole -name "B1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04862213291*$e0] -e0 $e0 -tilt 0.04625572479 
Multipole -name "B1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.08070627111*$e0] -e0 $e0 
Multipole -name "B1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1097.605287*$e0] -e0 $e0 
Sbend -name "B1FFA" -synrad $sbend_synrad -length 0.3064369612 -angle -0.02690209037 -E1 -0.02690209037 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.01905 -fint 0.5 
set e0 [expr $e0-14.1e-6*-0.02690209037*-0.02690209037/0.3064369612*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Quadrupole -name "B1FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -3.093740393e-05*$e0] -e0 $e0 
Multipole -name "B1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04862213291*$e0] -e0 $e0 -tilt 0.04625572479 
Multipole -name "B1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.08070627111*$e0] -e0 $e0 
Multipole -name "B1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1097.605287*$e0] -e0 $e0 
Quadrupole -name "B1FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -3.093740393e-05*$e0] -e0 $e0 
Multipole -name "B1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04862213291*$e0] -e0 $e0 -tilt 0.04625572479 
Multipole -name "B1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.08070627111*$e0] -e0 $e0 
Multipole -name "B1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1097.605287*$e0] -e0 $e0 
Sbend -name "B1FFB" -synrad $sbend_synrad -length 0.3064369612 -angle -0.02690209037 -E1 0 -E2 -0.02690209037 -six_dim 1 -e0 $e0 -hgap 0.01905 -fintx 0.5 
set e0 [expr $e0-14.1e-6*-0.02690209037*-0.02690209037/0.3064369612*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Quadrupole -name "B1FFMULT1" -synrad $quad_synrad -length 0 -strength [expr -3.093740393e-05*$e0] -e0 $e0 
Multipole -name "B1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.04862213291*$e0] -e0 $e0 -tilt 0.04625572479 
Multipole -name "B1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.08070627111*$e0] -e0 $e0 
Multipole -name "B1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -1097.605287*$e0] -e0 $e0 
Drift -name "L224A" -length 0.2406045352 
Drift -name "MREF1FF" -length 0 
Drift -name "L224B" -length 0.203755 
Multipole -name "QD2BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01829512447*$e0] -e0 $e0 -tilt 0.006571980611 
Multipole -name "QD2BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.488201167*$e0] -e0 $e0 -tilt 0.6925477838 
Quadrupole -name "QD2BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.09935648446*$e0] -e0 $e0 
Multipole -name "QD2BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01829512447*$e0] -e0 $e0 -tilt 0.006571980611 
Multipole -name "QD2BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.488201167*$e0] -e0 $e0 -tilt 0.6925477838 
Drift -name "AQD2BFF" -length 0 
Multipole -name "QD2BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01829512447*$e0] -e0 $e0 -tilt 0.006571980611 
Multipole -name "QD2BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.488201167*$e0] -e0 $e0 -tilt 0.6925477838 
Quadrupole -name "QD2BFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.09935648446*$e0] -e0 $e0 
Multipole -name "QD2BFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01829512447*$e0] -e0 $e0 -tilt 0.006571980611 
Multipole -name "QD2BFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.488201167*$e0] -e0 $e0 -tilt 0.6925477838 
Drift -name "L225A" -length 0.019755 
Bpm -name "MQD2BFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L225B" -length 0.354 
Drift -name "MT1X" -length 0 
Drift -name "L225C" -length 0.137 
Drift -name "FFML1" -length 0 
Drift -name "L225D" -length 0.7235 
Drift -name "MREFIPFF" -length 0 
Drift -name "L225E" -length 0.267255 
Multipole -name "QD2AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.007820933842*$e0] -e0 $e0 -tilt 0.4505632088 
Multipole -name "QD2AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-3.237581097*$e0] -e0 $e0 -tilt -0.009734334147 
Quadrupole -name "QD2AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1448640481*$e0] -e0 $e0 
Multipole -name "QD2AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.007820933842*$e0] -e0 $e0 -tilt 0.4505632088 
Multipole -name "QD2AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-3.237581097*$e0] -e0 $e0 -tilt -0.009734334147 
Drift -name "AQD2AFF" -length 0 
Multipole -name "QD2AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.007820933842*$e0] -e0 $e0 -tilt 0.4505632088 
Multipole -name "QD2AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-3.237581097*$e0] -e0 $e0 -tilt -0.009734334147 
Quadrupole -name "QD2AFF" -synrad $quad_synrad -length 0.099245 -strength [expr -0.1448640481*$e0] -e0 $e0 
Multipole -name "QD2AFFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.007820933842*$e0] -e0 $e0 -tilt 0.4505632088 
Multipole -name "QD2AFFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-3.237581097*$e0] -e0 $e0 -tilt -0.009734334147 
Drift -name "L226A" -length 0.019755 
Bpm -name "MQD2AFF" -length 0 -resolution $cband -short_range_wake $cband_wake
Drift -name "L226B" -length 0.391 
Drift -name "MREFSFF" -length 0 
Drift -name "L226C" -length 3.825 
Multipole -name "SK1FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Multipole -name "SK1FF" -synrad $mult_synrad -type 3 -length 0.115 -strength [expr 0*$e0] -e0 $e0 -tilt -0.5235987756 
Drift -name "L226D" -length 0.161 
Bpm -name "MSF1FF" -length 0 -resolution $sband -short_range_wake $sband_wake
Drift -name "L226E" -length 0.099 
Multipole -name "SF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1499119875*$e0] -e0 $e0 -tilt -0.4398229715 
Multipole -name "SF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -64.19233541*$e0] -e0 $e0 -tilt 0.6283185307 
# WARNING: Multipole options not defined
Drift -name "SF1FFMULT5" -length 0 -tilt -0.2705260341 
# WARNING: Multipole options not defined
Drift -name "SF1FFMULT6" -length 0 -tilt 0.8459860217 
# WARNING: Multipole options not defined
Drift -name "SF1FFMULT7" -length 0 -tilt 4.196644186 
# WARNING: Multipole options not defined
Drift -name "SF1FFMULT8" -length 0 -tilt 3.380702761 
# WARNING: Multipole options not defined
Drift -name "SF1FFMULT9" -length 0 -tilt 1.082104136 
Multipole -name "SF1FF" -synrad $mult_synrad -type 3 -length 0.05 -strength [expr -1.505168272*$e0] -e0 $e0 
Multipole -name "SF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1499119875*$e0] -e0 $e0 -tilt -0.4398229715 
Multipole -name "SF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -64.19233541*$e0] -e0 $e0 -tilt 0.6283185307 
# WARNING: Multipole options not defined
Drift -name "SF1FFMULT5" -length 0 -tilt -0.2705260341 
# WARNING: Multipole options not defined
Drift -name "SF1FFMULT6" -length 0 -tilt 0.8459860217 
# WARNING: Multipole options not defined
Drift -name "SF1FFMULT7" -length 0 -tilt 4.196644186 
# WARNING: Multipole options not defined
Drift -name "SF1FFMULT8" -length 0 -tilt 3.380702761 
# WARNING: Multipole options not defined
Drift -name "SF1FFMULT9" -length 0 -tilt 1.082104136 
Drift -name "ASF1FF" -length 0 
Multipole -name "SF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1499119875*$e0] -e0 $e0 -tilt -0.4398229715 
Multipole -name "SF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -64.19233541*$e0] -e0 $e0 -tilt 0.6283185307 
# WARNING: Multipole options not defined
Drift -name "SF1FFMULT5" -length 0 -tilt -0.2705260341 
# WARNING: Multipole options not defined
Drift -name "SF1FFMULT6" -length 0 -tilt 0.8459860217 
# WARNING: Multipole options not defined
Drift -name "SF1FFMULT7" -length 0 -tilt 4.196644186 
# WARNING: Multipole options not defined
Drift -name "SF1FFMULT8" -length 0 -tilt 3.380702761 
# WARNING: Multipole options not defined
Drift -name "SF1FFMULT9" -length 0 -tilt 1.082104136 
Multipole -name "SF1FF" -synrad $mult_synrad -type 3 -length 0.05 -strength [expr -1.505168272*$e0] -e0 $e0 
Multipole -name "SF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1499119875*$e0] -e0 $e0 -tilt -0.4398229715 
Multipole -name "SF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -64.19233541*$e0] -e0 $e0 -tilt 0.6283185307 
# WARNING: Multipole options not defined
Drift -name "SF1FFMULT5" -length 0 -tilt -0.2705260341 
# WARNING: Multipole options not defined
Drift -name "SF1FFMULT6" -length 0 -tilt 0.8459860217 
# WARNING: Multipole options not defined
Drift -name "SF1FFMULT7" -length 0 -tilt 4.196644186 
# WARNING: Multipole options not defined
Drift -name "SF1FFMULT8" -length 0 -tilt 3.380702761 
# WARNING: Multipole options not defined
Drift -name "SF1FFMULT9" -length 0 -tilt 1.082104136 
Drift -name "L227" -length 0.30295 
Multipole -name "QF1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0004336394638*$e0] -e0 $e0 
Multipole -name "QF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.02131894177*$e0] -e0 $e0 
Multipole -name "QF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -0.6484970075*$e0] -e0 $e0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT5" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT6" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT7" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT8" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT9" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT2S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT3S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT4S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT5S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT6S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT7S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT8S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT9S" -length 0 
Quadrupole -name "QF1FF" -synrad $quad_synrad -length 0.22205 -strength [expr 0.369493027*$e0] -e0 $e0 
Multipole -name "QF1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0004336394638*$e0] -e0 $e0 
Multipole -name "QF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.02131894177*$e0] -e0 $e0 
Multipole -name "QF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -0.6484970075*$e0] -e0 $e0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT5" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT6" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT7" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT8" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT9" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT2S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT3S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT4S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT5S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT6S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT7S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT8S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT9S" -length 0 
Drift -name "AQF1FF" -length 0 
Multipole -name "QF1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0004336394638*$e0] -e0 $e0 
Multipole -name "QF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.02131894177*$e0] -e0 $e0 
Multipole -name "QF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -0.6484970075*$e0] -e0 $e0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT5" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT6" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT7" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT8" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT9" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT2S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT3S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT4S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT5S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT6S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT7S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT8S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT9S" -length 0 
Quadrupole -name "QF1FF" -synrad $quad_synrad -length 0.22205 -strength [expr 0.369493027*$e0] -e0 $e0 
Multipole -name "QF1FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.0004336394638*$e0] -e0 $e0 
Multipole -name "QF1FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.02131894177*$e0] -e0 $e0 
Multipole -name "QF1FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -0.6484970075*$e0] -e0 $e0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT5" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT6" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT7" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT8" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT9" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT2S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT3S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT4S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT5S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT6S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT7S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT8S" -length 0 
# WARNING: Multipole options not defined
Drift -name "QF1FFMULT9S" -length 0 
Drift -name "L228A" -length 0.12195 
# not present anymore
#Bpm -name "MQF1FF" -length 0 
Drift -name "L228B" -length 0.297 
Bpm -name "MSD0FF" -length 0 -resolution $sband -short_range_wake $sband_wake
Drift -name "L228C" -length 0.099 
Multipole -name "SD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2963503459*$e0] -e0 $e0 -tilt 0.07679448709 
Multipole -name "SD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 78.90731241*$e0] -e0 $e0 -tilt 0.6283185307 
# WARNING: Multipole options not defined
Drift -name "SD0FFMULT5" -length 0 -tilt 0.2391101075 
# WARNING: Multipole options not defined
Drift -name "SD0FFMULT6" -length 0 -tilt 0.5667333414 
# WARNING: Multipole options not defined
Drift -name "SD0FFMULT7" -length 0 -tilt -0.3447025273 
# WARNING: Multipole options not defined
Drift -name "SD0FFMULT8" -length 0 -tilt -0.2862339973 
# WARNING: Multipole options not defined
Drift -name "SD0FFMULT9" -length 0 -tilt 0.2513274123 
Multipole -name "SD0FF" -synrad $mult_synrad -type 3 -length 0.05 -strength [expr 2.250119161*$e0] -e0 $e0 
Multipole -name "SD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2963503459*$e0] -e0 $e0 -tilt 0.07679448709 
Multipole -name "SD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 78.90731241*$e0] -e0 $e0 -tilt 0.6283185307 
# WARNING: Multipole options not defined
Drift -name "SD0FFMULT5" -length 0 -tilt 0.2391101075 
# WARNING: Multipole options not defined
Drift -name "SD0FFMULT6" -length 0 -tilt 0.5667333414 
# WARNING: Multipole options not defined
Drift -name "SD0FFMULT7" -length 0 -tilt -0.3447025273 
# WARNING: Multipole options not defined
Drift -name "SD0FFMULT8" -length 0 -tilt -0.2862339973 
# WARNING: Multipole options not defined
Drift -name "SD0FFMULT9" -length 0 -tilt 0.2513274123 
Drift -name "ASD0FF" -length 0 
Multipole -name "SD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2963503459*$e0] -e0 $e0 -tilt 0.07679448709 
Multipole -name "SD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 78.90731241*$e0] -e0 $e0 -tilt 0.6283185307 
# WARNING: Multipole options not defined
Drift -name "SD0FFMULT5" -length 0 -tilt 0.2391101075 
# WARNING: Multipole options not defined
Drift -name "SD0FFMULT6" -length 0 -tilt 0.5667333414 
# WARNING: Multipole options not defined
Drift -name "SD0FFMULT7" -length 0 -tilt -0.3447025273 
# WARNING: Multipole options not defined
Drift -name "SD0FFMULT8" -length 0 -tilt -0.2862339973 
# WARNING: Multipole options not defined
Drift -name "SD0FFMULT9" -length 0 -tilt 0.2513274123 
Multipole -name "SD0FF" -synrad $mult_synrad -type 3 -length 0.05 -strength [expr 2.250119161*$e0] -e0 $e0 
Multipole -name "SD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0.2963503459*$e0] -e0 $e0 -tilt 0.07679448709 
Multipole -name "SD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr 78.90731241*$e0] -e0 $e0 -tilt 0.6283185307 
# WARNING: Multipole options not defined
Drift -name "SD0FFMULT5" -length 0 -tilt 0.2391101075 
# WARNING: Multipole options not defined
Drift -name "SD0FFMULT6" -length 0 -tilt 0.5667333414 
# WARNING: Multipole options not defined
Drift -name "SD0FFMULT7" -length 0 -tilt -0.3447025273 
# WARNING: Multipole options not defined
Drift -name "SD0FFMULT8" -length 0 -tilt -0.2862339973 
# WARNING: Multipole options not defined
Drift -name "SD0FFMULT9" -length 0 -tilt 0.2513274123 
Drift -name "L229" -length 0.2875 
Multipole -name "QD0FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01735176498*$e0] -e0 $e0 -tilt 0.2541199391 
Multipole -name "QD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.072664521*$e0] -e0 $e0 -tilt 0.641757566 
Multipole -name "QD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -585.4643194*$e0] -e0 $e0 -tilt 0.5410520681 
# WARNING: Multipole options not defined
Drift -name "QD0FFMULT5" -length 0 -tilt 0.5134758659 
# WARNING: Multipole options not defined
Drift -name "QD0FFMULT6" -length 0 -tilt 0.1261374384 
# WARNING: Multipole options not defined
Drift -name "QD0FFMULT7" -length 0 -tilt 0.3644247478 
# WARNING: Multipole options not defined
Drift -name "QD0FFMULT8" -length 0 -tilt -0.01239183769 
# WARNING: Multipole options not defined
Drift -name "QD0FFMULT9" -length 0 -tilt -0.001047197551 
Quadrupole -name "QD0FF" -synrad $quad_synrad -length 0.2375 -strength [expr -0.6811853675*$e0] -e0 $e0 
Multipole -name "QD0FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01735176498*$e0] -e0 $e0 -tilt 0.2541199391 
Multipole -name "QD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.072664521*$e0] -e0 $e0 -tilt 0.641757566 
Multipole -name "QD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -585.4643194*$e0] -e0 $e0 -tilt 0.5410520681 
# WARNING: Multipole options not defined
Drift -name "QD0FFMULT5" -length 0 -tilt 0.5134758659 
# WARNING: Multipole options not defined
Drift -name "QD0FFMULT6" -length 0 -tilt 0.1261374384 
# WARNING: Multipole options not defined
Drift -name "QD0FFMULT7" -length 0 -tilt 0.3644247478 
# WARNING: Multipole options not defined
Drift -name "QD0FFMULT8" -length 0 -tilt -0.01239183769 
# WARNING: Multipole options not defined
Drift -name "QD0FFMULT9" -length 0 -tilt -0.001047197551 
Drift -name "AQD0FF" -length 0 
Multipole -name "QD0FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01735176498*$e0] -e0 $e0 -tilt 0.2541199391 
Multipole -name "QD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.072664521*$e0] -e0 $e0 -tilt 0.641757566 
Multipole -name "QD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -585.4643194*$e0] -e0 $e0 -tilt 0.5410520681 
# WARNING: Multipole options not defined
Drift -name "QD0FFMULT5" -length 0 -tilt 0.5134758659 
# WARNING: Multipole options not defined
Drift -name "QD0FFMULT6" -length 0 -tilt 0.1261374384 
# WARNING: Multipole options not defined
Drift -name "QD0FFMULT7" -length 0 -tilt 0.3644247478 
# WARNING: Multipole options not defined
Drift -name "QD0FFMULT8" -length 0 -tilt -0.01239183769 
# WARNING: Multipole options not defined
Drift -name "QD0FFMULT9" -length 0 -tilt -0.001047197551 
Quadrupole -name "QD0FF" -synrad $quad_synrad -length 0.2375 -strength [expr -0.6811853675*$e0] -e0 $e0 
Multipole -name "QD0FFMULT2" -synrad $mult_synrad -type 3 -length 0 -strength [expr -0.01735176498*$e0] -e0 $e0 -tilt 0.2541199391 
Multipole -name "QD0FFMULT3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-1.072664521*$e0] -e0 $e0 -tilt 0.641757566 
Multipole -name "QD0FFMULT4" -synrad $mult_synrad -type 5 -length 0 -strength [expr -585.4643194*$e0] -e0 $e0 -tilt 0.5410520681 
# WARNING: Multipole options not defined
Drift -name "QD0FFMULT5" -length 0 -tilt 0.5134758659 
# WARNING: Multipole options not defined
Drift -name "QD0FFMULT6" -length 0 -tilt 0.1261374384 
# WARNING: Multipole options not defined
Drift -name "QD0FFMULT7" -length 0 -tilt 0.3644247478 
# WARNING: Multipole options not defined
Drift -name "QD0FFMULT8" -length 0 -tilt -0.01239183769 
# WARNING: Multipole options not defined
Drift -name "QD0FFMULT9" -length 0 -tilt -0.001047197551 
Drift -name "L230A" -length 0.1065 
# not present anymore
#Bpm -name "MQD0FF" -length 0 
Drift -name "L230B" -length 0.286 
# WARNING: VCORRECTOR needs to be defined, no PLACET element
VCORRECTOR -name "IPKICK" -length 0.12 
Drift -name "L230C" -length 0.048 
Bpm -name "MPREIP" -length 0 -resolution $cband_noatt -short_range_wake $cband_wake
Drift -name "L230D" -length 0.2758 
Bpm -name "IPBPMA" -length 0 -resolution $ipbpm
Drift -name "L230E" -length 0.081 
Bpm -name "IPBPMB" -length 0 -resolution $ipbpm
Drift -name "L230F" -length 0.0702 
Drift -name "MS1IP" -length 0 
Drift -name "MBS1IP" -length 0 
Quadrupole -name "IP" -strength 0 -length 0 

if {[info exist add_bpm_at_ip] && $add_bpm_at_ip} {
    CavityBpm -name "BPMIP" -length 0 -resolution $ipbpm
}
if {[info exist save_beam_ip] && $save_beam_ip} {
    TclCall -name "SAVEBEAMIP" -script "save_beam_ip"
}

Drift -name "L231" -length 0.0882 
Bpm -name "IPBPMC" -length 0 -resolution $ipbpm
Drift -name "ENDFF" -length 0 
Drift -name "L301A" -length 0.3338 
Drift -name "MW1IP" -length 0 
Drift -name "L301B" -length 0.203 
# not sure about resolution of MPIP - setting to stripline
Bpm -name "MPIP" -length 0 -resolution $stripline
Drift -name "L301C" -length 0.302 
Drift -name "MSPIP" -length 0 
Drift -name "L301D" -length 0.573 
Drift -name "MBDUMPA" -length 0 
Sbend -name "BDUMPA" -synrad $sbend_synrad -length 0.6784391641 -angle 0.1745329252 -E1 0.1745329252 -E2 0 -six_dim 1 -e0 $e0 -hgap 0.016 -fint 0.5 
set e0 [expr $e0-14.1e-6*0.1745329252*0.1745329252/0.6784391641*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BDUMPB" -synrad $sbend_synrad -length 0.6784391641 -angle 0.1745329252 -E1 0 -E2 0.1745329252 -six_dim 1 -e0 $e0 -hgap 0.016 -fintx 0.5 
set e0 [expr $e0-14.1e-6*0.1745329252*0.1745329252/0.6784391641*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "MBDUMPB" -length 0 
Drift -name "L302A" -length 0.6345608359 
Drift -name "MDUMP" -length 0 
Drift -name "L302B" -length 0.146 
Drift -name "ICTDUMP" -length 0 
Drift -name "DIAMOND" -length 0 
Drift -name "L302C" -length 1.219439164 
Drift -name "DUMPH" -length 1.15 
Drift -name "DUMPH" -length 1.15 
Drift -name "ATF2MEND" -length 0 
