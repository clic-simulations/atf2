TclCall -name "NanoBPMAddBeamOffset" -script "nanobpmsetup_add_beam_offset"

Drift -name "L114B" -length 0.274 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "L114C" -length 0.502 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "L114D" -length 0.35 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "L114E" -length 0.54 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
#Drift -name "L114F" -length 1.813335 -aperture_shape elliptic -aperture_x 1 -aperture_y 1

Drift -name "L114F" -length 0.763335 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Bpm -name "IPTBPM1"  -resolution $ipbpm
Drift -name "L114G" -length 0.3 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Bpm -name "IPTBPM2"  -resolution $ipbpm
Drift -name "L114H" -length 0.3 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Bpm -name "IPTBPM3"  -resolution $ipbpm
Drift -name "L114I" -length 0.45 -aperture_shape elliptic -aperture_x 1 -aperture_y 1

Drift -name "L114J" -length 0.282 -aperture_shape elliptic -aperture_x 1 -aperture_y 1