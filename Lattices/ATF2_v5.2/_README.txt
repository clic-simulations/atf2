++COMMENTS
Hi,

this is a version taken from Mark Woodley's Flight simulator
https://confluence.slac.stanford.edu/display/ATF/ATF2+Flight+Simulator
I took the .saveline MAD file from here (v5.2)
https://code.google.com/p/atf2flightsim/source/browse/#svn%2Ftrunk%
2FATF2%2FFlightSim%2FlatticeFiles%2Fsrc%2Fv5.2
and I used this code to translate from MAD to MADX
https://github.com/orblancog/mad2madx.git

There are 4 lines declared:
  ATFDR
  EXT
  FF
  POST
I guess it is 10BX1BY1 from the name.



++LOG

ATF2lat_BX10BY1nl.saveline.madx
  initial translated file using
  https://github.com/orblancog/mad2madx.git
    tag v0.1beta
  Bugs in multipole components!!! This file remains hidden in this dir

ATF2lat_BX10BY1nl.r01.madx
  initial translated file using
  https://github.com/orblancog/mad2madx.git
    tag v0.2beta
  Multipole components seem OK. Need to be checked.



++AUTHOR
Oscar BLANCO
oblancog
oscar.roberto.blanco.garcia@cern.ch
