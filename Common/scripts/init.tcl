#
# Define wavelength
#

set lambda [expr 0.3/1.3]

#
# Define gradient
#

set gradient [expr 31.5e-3]

#
# Define longrange wakefield
# uncomment next command if single bunch only
#

#
# Define longrange wakefield
# uncomment next command if single bunch only
#

set CAV_modes {
    {1.6506 19.98 7e4}
    {1.6991 301.86 5e4}
    {1.7252 423.41 2e4}
    {1.7545 59.86 2e4}
    {1.7831 49.2 7.5e3}
    {1.7949 21.70 1e4}
    {1.8342 13.28 5e4}
    {1.8509 11.26 2.5e4}
    {1.8643 191.56 5e4}
    {1.8731 255.71 7e4}
    {1.8795 50.8 1e5}
    {2.5630 42.41 1e5}
    {2.5704 20.05 1e5}
    {2.5751 961.28 5e4}
}

#
# Define element parameters
#

set L_cav 1.036
set L_dcme 0.191
set L_dics 0.283
set L_duqp 0.247
set L_ddqp 0.171
set L_dims 0.382
set D_cav 0.28
set L_quad 0.666

#
# Calculate resulting parameters
#

set R_earth 6.37e6
set L_qmod [expr 2*$L_dcme+6*$L_dics+$L_duqp+$L_quad+$L_ddqp+8*$L_cav+$L_dims]
set L_mod [expr 2*$L_dcme+7*$L_dics+8*$L_cav+$L_dims]
set L_halfcell [expr ($L_qmod+2*$L_mod)*1.0000]

#
# Define normalised focal strengths for the two sectors
#

set Kf [expr $L_quad*0.0524215]
set Kd [expr $L_quad*-0.0470696]

#
# Find initial twiss parameters
#
#puts [MatchFodo -K1 [expr $Kd] -K2 [expr $Kf] -l1 0.666 -l2 0.666 -L 33.9]
#array set match {beta_x 28.9658 alpha_x 0 beta_y 124.146 alpha_y 0}
#array set match {beta_x 35.1243 alpha_x 0.683836 beta_y 103.839 alpha_y -1.74809}

# Flag to use straight linac
if {![info exists curved]} {
    set curved 0
}

# Flag indicating last part of linac
set last_part 0

#
# Prepare a list of the modes
#

set cav_modes {}
set N_mode 0
set scale [expr 1*0.3/(2.0*acos(-1.0)*1.3)/1.0]
foreach mode $CAV_modes {
    lappend cav_modes [expr 0.3/[lindex $mode 0]]
    lappend cav_modes [expr [lindex $mode 1]*$scale]
    lappend cav_modes [expr [lindex $mode 2]]
    incr N_mode
}

#
# Use this list to create fields
#

WakeSet wakelong $cav_modes

#
# Define accelerating structure
#

InjectorCavityDefine -lambda $lambda \
	-wakelong wakelong
