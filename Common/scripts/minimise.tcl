proc brack {funct x1 x2 f1} {
    set gold 1.618034
    set limit 10.0
#    set x1 0.0
#    set x2 1.0
#    set f1 [$funct $x1]
    set f2 [$funct $x2]
    if {$f1<$f2} {
        set tmp $x1
        set x1 $x2
        set x2 $tmp
        set tmp $f1
        set f1 $f2
        set f2 $tmp
    }
    set x3 [expr $x2+($x2-$x1)*$gold]
    set f3 [$funct $x3]
    while {$f2>$f3} {
        set r [expr ($x2-$x1)*($f2-$f3)]
        set q [expr ($x2-$x3)*($f2-$f1)]
        set qr [expr $q-$r]
        if {abs($qr)<1e-20} {
            if {$qr<0.0} {
                set qr -1e-20
            } {
                set qr 1e-20
            }
        }
        set xt [expr $x2-($q*($x2-$x3)-$r*($x2-$x1))/(2.0*$qr)]
        if {($x2-$xt)*($xt-$x3)>0.0} {
            set ft [$funct $xt]
            if {$ft<$f3} {
                return "$x2 $f2 $xt $ft $x3 $f3"
            }
            if {$ft>$f2} {
                return "$x1 $f1 $x2 $f2 $xt $ft"
            }
            set xt [expr $x3+$gold*($x3-$x2)]
            set ft [$funct $xt]
        } {
            set xlimit [expr $x2+$limit*($x3-$x2)]
            if {($x3-$xt)*($xt-$xlimit)>0.0} {
                set ft [$funct $xt]
                if {$ft<$f3} {
                    set x2 $x3
                    set x3 $xt
                    set f2 $f3
                    set f3 $ft
                    set xt [expr $x3+$gold*($x3-$x2)]
                    set ft [$funct $xt]
                }
            } {
                if {($xt-$xlimit)*($xlimit-$x3)>0.0} {
                    set xt $xlimit
                    set ft [$funct $xt]
                } {
                    set xt [expr $x3+$gold*($x3-$x2)]
                    set ft [$funct $xt]
                }
            }
        }
        set x1 $x2
        set x2 $x3
        set x3 $xt
        set f1 $f2
        set f2 $f3
        set f3 $ft
    }
    return "$x1 $f1 $x2 $f2 $x3 $f3"
}

proc minimum {list} {
    set x1 [lindex $list 0]
    set f1 [lindex $list 1]
    set x2 [lindex $list 2]
    set f2 [lindex $list 3]
    set x3 [lindex $list 4]
    set f3 [lindex $list 5]
    set tmp [expr ($x2-$x1)*abs($f2-$f3)-($x2-$x3)*abs($f2-$f1)]
    set tmp2 [expr 0.5*(($x2-$x1)*($x2-$x1)*abs($f2-$f3)-($x2-$x3)*($x2-$x3)*abs($f2-$f1))]
    if {(abs($tmp)<1e-30*abs($tmp2))||($tmp==0.0)} {
	return $x2
    }
    return [expr $x2-$tmp2/$tmp]
}

proc minimise {f x0 xp f0} {
    set res [brack $f $x0 $xp $f0]
    set x1 [lindex $res 0]
    set f1 [lindex $res 1]
    set x2 [lindex $res 2]
    set f2 [lindex $res 3]
    set x3 [lindex $res 4]
    set f3 [lindex $res 5]
    if {$x1>$x3} {
	set tmp $x1
	set x1 $x3
	set x3 $tmp
	set tmp $f1
	set f1 $f3
	set f3 $tmp
    }
    for {set i 0} {$i<3} {incr i} {
	set xtry [minimum "$x1 $f1 $x2 $f2 $x3 $f3"]
	if {$xtry<$x1} {
	    return "$x2 $f2"
	}
	if {$xtry>$x3} {
	    return "$x2 $f2"
	}
	if {$xtry==$x2} {
	    return "$x2 $f2"
	}
	set ftry [$f $xtry]
	if {$xtry<$x2} {
	    if {$ftry<$f2} {
		set x3 $x2
		set f3 $f2
		set x2 $xtry
		set f2 $ftry
	    } {
		set x1 $xtry
		set f1 $ftry
	    }
	} {
	    if {$ftry<$f2} {
		set x1 $x2
		set f1 $f2
		set x2 $xtry
		set f2 $ftry
	    } {
		set x3 $xtry
		set f3 $ftry
	    }
	}
    }
    return "$x2 $f2"
}

proc zbrack {f x1 x2 f1} {
    set factor 2.6
    set f2 [$f $x2]
    if {$f1*$f2>0.0} {
	while {$f1*$f2>0.0} {
	    if {abs($f1)>abs($f2)} {
		set x3 $x1
		set f3 $f1
		set x1 [expr $x2-($x1-$x2)*$factor]
		set f1 [$f $x1]
	    } {
		set x3 $x2
		set f3 $f2
		set x2 [expr $x1-($x2-$x1)*$factor]
		set f2 [$f $x2]
	    }
	    puts "$x1 $f1 $x2 $f2 $x3 $f3"
	    if {abs($x3-$x1)>10000.0} {
		return "$x1 $f1 $x2 $f2"
	    }
	}
	return "$x1 $f1 $x2 $f2 $x3 $f3"
    }
    if {$f1==0} {
	set tmp $x1
	set x1 $x2
	set x2 $tmp
	set tmp $f1
	set f1 $f2
	set f2 $tmp
    }
    return "$x1 $f1 $x2 $f2"
}

proc zfind {f x1 f1 x2 f2} {
    if {abs($f2-$f1)<1e-6} {
	if {abs($f1)<abs($f2)} {
	    set x3 [expr $x1-1.0]
	} {
	    set x3 [expr $x2+1.0]
	}
    } {
	puts "$x1 $x2 $f1 $f2"
	set x3 [expr $x1-$f1/($f2-$f1)*($x2-$x1)]
    }
    set f3 [$f $x3]
    while {(abs($x2-$x1)>0.01)&&(abs($f3)>0.1)} {
	puts "$x3 $f3"
	if {$f3*$f1>0.0} {
	    set x1 $x3
	    set f1 $f3
	} {
	    set x2 $x3
	    set f2 $f3
	}
	puts "$x1 $x2 $f1 $f2"
	set x3 [expr $x1-$f1/($f2-$f1)*($x2-$x1)]
	set f3 [$f $x3]
    }
    return "$x3 $f3"
}

proc root {f x1 x2 f1} {
    set l [zbrack $f $x1 $x2 $f1]
    puts "found $l"
    return [zfind $f [lindex $l 0] [lindex $l 1] [lindex $l 2] [lindex $l 3]]
}

