% File to make different checks about the different time stamps and
% signal recorded with the BPM measurement via EPICS and the ground
% motion measurement with the NI hardware. 

%%%%%%%%%%%%%
% Parameter %
%%%%%%%%%%%%%

%BPMfilename = './data/sync_test2_20140228_0045.dat';
%BPM_ts_filename = './data/time_stamp_sync_test2_20140228_0045.mat';
%BPM_ts_FONT_filename = './data/time_stamp_FONT_sync_test2_20140228_0045.mat';
%BPM_ts_FONT_local_filename = './data/time_stamp_FONT_local_sync_test2_20140228_0045.mat';
%Beam_on_filename = './data/sync_test2_20140228_0045_beam_on.dat';
%Beam_on_ts_filename = './data/time_stamp_beam_on_sync_test2_20140228_0045.mat';
%GMfilename = './data/ATF2_2014-02-28_00h44m03s.428.mat';

%BPMfilename = './data/sync_test1_20140227_2340.dat';
%BPM_ts_filename = './data/time_stamp_sync_test1_20140227_2340.mat';
%BPM_ts_FONT_filename = './data/time_stamp_FONT_sync_test1_20140227_2340.mat';
%BPM_ts_FONT_local_filename = './data/time_stamp_FONT_local_sync_test1_20140227_2340.mat';
%BPM_ts_FONT_local_filename = './data/time_stamp_FONT_local_sync_test2_20140228_0045.mat';
%Beam_on_filename = './data/sync_test1_20140227_2340_beam_on.dat';
%Beam_on_ts_filename = './data/time_stamp_beam_on_sync_test1_20140227_2340.mat';
%GMfilename = './data/ATF2_2014-02-27_23h34m50s.454.mat';

%BPMfilename = './data/data_20140304/bigbuffer20140304T221314.mat';
%Beam_on_filename = './data/data_20140304/sync_test5_20140304_2205_beam_on.dat';
%Beam_on_ts_filename = './data/data_20140304/time_stamp_beam_on_sync_test5_20140304_2205.mat';
%GMfilename = './data/ATF2_2014-03-04_22h02m57s.060.mat';

%BPMfilename = './data/test/test1.dat';
%BPM_ts_filename = './data/test/time_stamp_test1.mat';
%Beam_on_filename = './data/test/test1_beam_on.dat';
%Beam_on_ts_filename = './data/test/time_stamp_beam_on_test1.mat';
%GMfilename = './data/ATF2_2014-03-04_22h02m57s.060.mat';

BPMfilename = './data/data_20140307/beam_on_off_20140307_0155.mat';
GMfilename = './data/ATF2_2014-03-07_01h52m09s.100/ATF2_2014-03-07_01h52m09s.100.mat';

use_epics_time_stamp = 0;
use_bigbuffer = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load and convert the data %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

GM = load(GMfilename);
beam_on_gm = GM.transition_array;
ts_beam_on_gm = beam_on_gm(:,1);
time_stamp_gm = GM.t0+GM.time_transitions/24/60/60;
time_stamp_beam_on_gm = GM.t0+ts_beam_on_gm/24/60/60;
size(time_stamp_gm)

load(BPMfilename);
%xread = read(1:(end/2),:);
%yread = read((end/2+1):end,:);

%beam_on = load(Beam_on_filename);
%ts_beam_on = load(Beam_on_ts_filename);

%ts = load(BPM_ts_filename);
%ts_font = load(BPM_ts_FONT_filename);
%ts_font = ts_font.save_data;
%ts_font_local = load(BPM_ts_FONT_local_filename);
%ts_font_local = ts_font_local.save_data;
%ts_beam_on = load(Beam_on_ts_filename);
%ts_beam_on = ts_beam_on.time_stamp_beam_on;

%ts = ts';
%xread = xread';
%yread = yread';
%if(use_big_buffer == 1)
%    [ts, stripline_index, cav_index] = get_bigbuffer_atf2_data(ts); 
%end
%ts = ts.save_data;
%ts=ts((end/2+1):end,:);
%ts_font=ts_font((end/2+1):end,:);
if(use_epics_time_stamp == 1)
    ts = epicsts2mat(ts);
    ts_beam_on = epicsts2mat(ts_beam_on);

    [nr_pulse, nr_font_bpm] = size(ts_font);
    ts_font = epicsts2mat(ts_font);
    ts_font_local_temp = zeros(nr_pulse, 1);
    for i=1:nr_pulse
        ts_font_local_temp(i)=datenum(ts_font_local{i});
    end
    ts_font_local = ts_font_local_temp;
end

size(x_data)
size(y_data)
size(time_stamp_y)
size(time_stamp_beam_on)
size(beam_on)

% The Matab time stamp is the just the number of days from a certain start date
% So we define one time as the basis and start from there. 
% First we have to move the data to the Japan time zone. That is kind of a mess here

time_stamp_y = time_stamp_y + 16/24;
time_stamp_beam_on = time_stamp_beam_on + 16/24;
%ts_font = ts_font + 16/24;
time_stamp_gm = time_stamp_gm + 9/24;
time_stamp_beam_on_gm = time_stamp_beam_on_gm + 9/24;

time_stamp_ref = time_stamp_y(1,20);
delta_ts = (time_stamp_y - time_stamp_ref)*24*60*60;
%delta_ts_font = (ts_font - ts_ref)*24*60*60;
%delta_ts_font_local = (ts_font_local - ts_ref)*24*60*60;
delta_ts_beam_on = (time_stamp_beam_on - time_stamp_ref)*24*60*60;
delta_ts_gm = (time_stamp_gm - time_stamp_ref)*24*60*60;
delta_ts_beam_on_gm = (time_stamp_beam_on_gm - time_stamp_ref)*24*60*60;

%%%%%%%%%%%%%%%%%%
% Perform checks %
%%%%%%%%%%%%%%%%%%

% check the starting time stamps approximately 
fprintf(1, 'Starting time stamps of the different measurements\n')

fprintf(1, 'BPM stripline:  %s\n', datestr(time_stamp_y(1,1)));
fprintf(1, 'BPM stripline:  %s\n', datestr(time_stamp_y(1,20)));
%fprintf(1, 'BPM FONT:       %s\n', datestr(time_stamp_font(1,1)));
%fprintf(1, 'BPM FONT local: %s\n', datestr(ts_font_local(1,1)));
fprintf(1, 'Beam on:        %s\n', datestr(time_stamp_beam_on(1)));
fprintf(1, 'Ground motion:  %s\n', datestr(time_stamp_gm(1)));

% Check how consistent the time stamps are 
figure(1);
plot(delta_ts_gm(2:end) - delta_ts_gm(1:end-1),'-b');
grid on;
hold on;
plot([1 length(delta_ts_gm)-1], [1/3.12, 1/3.12], '-r');
title('delta ground motion time stamp');

figure(2);
plot(delta_ts_beam_on(2:end) - delta_ts_beam_on(1:end-1),'-xb');
grid on;
hold on;
plot([1 length(delta_ts_beam_on)-1], [1/3.12, 1/3.12], '-r');
title('delta beam on time stamp');

element_number1 = 20;
element_number2 = 21;
figure(3);
plot(delta_ts(2:end, element_number1) - delta_ts(1:end-1, element_number1),'-xb');
grid on;
hold on;
plot(delta_ts(2:end, element_number2) - delta_ts(1:end-1, element_number2),'-xg');
plot([1 length(delta_ts(2:end, element_number1))], [1/3.12, 1/3.12], '-r');
title('delta beam on time stamp');

figure(4);
plot(x_data(2:end, element_number1) - x_data(1:end-1, element_number1),'-xb');
grid on;
hold on;
plot(x_data(2:end, element_number2) - x_data(1:end-1, element_number2),'-xg');
title('delta beam on time stamp');

% Plot the synchronisation signal 
figure(10);
plot(delta_ts_beam_on, beam_on, '-xr');
grid on;
hold on;
stem(delta_ts_beam_on_gm, 1.5*ones(size(delta_ts_beam_on_gm)));
xlabel('pulse number');
ylabel('signal level');
legend('with BPM', 'with PXI');

