function [gm_index_okay, bpm_index_okay] = preprocess_gm_data(gm_ts, bpm_ts)

%%%%%%%%%%%%%
% Parameter %
%%%%%%%%%%%%%

delta_bpm = 1/3.1243;
epsilon_bpm = delta_bpm/5; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cut the data to the same time window %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Here we assume that all BPMs in x/y have the same time stamps. That was correct when 
% checking them;
bpm_ts = bpm_ts(:,20);

min_ts_common = max([gm_ts(1), bpm_ts(1)]);
max_ts_common = min([gm_ts(end), bpm_ts(end)]);

if(min_ts_common > max_ts_common)
  fprintf(1, 'No common data interval found\n');
  gm_index_okay = [];
  bpm_index_okay = []; 
  return;
end

bpm_indices_okay = (bpm_ts <= max_ts_common) & (bpm_ts >=min_ts_common);
bpm_ts = bpm_ts(bpm_indices_okay);
nr_ts_bpm = sum(bpm_indices_okay);

gm_indices_okay = (gm_ts <= max_ts_common) & (gm_ts >= min_ts_common);
nr_ts_gm = sum(gm_indices_okay);

size(bpm_indices_okay)
size(gm_indices_okay)

% Find the GM index that fits best to the first BPM indices.
% For that we assume that there is no step lost on the first 20 points (let's hope so)

% Check if there is a too large offset between ground motion and BPM data
index_gm_first = find(gm_indices_okay>0.5,1);
first_step_index_bpm = get_first_step_index(bpm_ts);
avg_time_diff = mean(gm_ts(index_gm_first:index_gm_first+first_step_index_bpm-2)-bpm_ts(1:first_step_index_bpm-1))
time_steps_offset = round(avg_time_diff./delta_bpm)
gm_ts = gm_ts(index_gm_first-time_steps_offset:end);

% Debug
avg_time_diff = mean(gm_ts(1:first_step_index_bpm-1)-bpm_ts(1:first_step_index_bpm-1))
time_steps_offset = round(avg_time_diff./delta_bpm)
% dEBUG


fprintf(1, 'Data points overlapping: GM = %i, BPM = %i\n', nr_ts_gm, nr_ts_bpm);

% Debug
%gm_ts_should_be = (0:(length(gm_ts)-1)).*delta_bpm+gm_ts(1)
%figure(40);
%plot(gm_ts' - gm_ts_should_be, '-b');
%grid on;
%hold on;

%figure(41);
%plot(gm_ts(2:end)-gm_ts(1:end-1), '-b');
%grid on;
%hold on;
% dEBUG

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find the proper indices %
%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The idea is the following: We want to remove BPM readings were the time between
% two time stamps was larger than the repetition time. Data which have been noisy 
% (most likely no beam) have been removed already. So we go through all BPM 
% readings and see, if this time stamp condition is fulfilled. If yes, we take it. 
%
% To find the according GM data, we apply the following logic/strategy. The basic 
% assumption is that the BPM data are in order and the time stamps are correct. The
% same has to be true for the GM data. So we have two consitant sets of data, were
% there can only be a constant offset. So if BPM data are missing (removal due to 
% spike removal function, or similar), then we also can remove the according 
% ground motion time step. 

gm_index_okay = zeros(nr_ts_gm, 1);
bpm_index_okay = zeros(nr_ts_bpm, 1);
bpm_gm_index_correspondance = zeros(nr_ts_bpm, 3);
take_bpm = 0;
nr_bpm_okay = 0;
index_gm = 0;
ts_now = 0;
ts_last = 0;

for index_bpm = 1:nr_ts_bpm

  % 1. Is this BPM reading okay
  take_bpm = 0;
  if(index_bpm == 1)
    take_bpm = 1;
    ts_last = bpm_ts(index_bpm) - delta_bpm;
  elseif(abs(bpm_ts(index_bpm)-bpm_ts(index_bpm-1)-delta_bpm)<epsilon_bpm)
    take_bpm = 1;
  end

  if(take_bpm == 1)
    bpm_index_okay(index_bpm) = 1;
    nr_bpm_okay = nr_bpm_okay + 1;
    ts_now = bpm_ts(index_bpm);
  end

  % 2. If yes, determine the corresponding GM readings
  if(take_bpm == 1)
    % Determine how much the gm index has to be incremented
    gm_increment = round((ts_now-ts_last)./delta_bpm);
    index_gm = index_gm+gm_increment;
    
    ts_last = ts_now;
    gm_index_okay(index_gm) = 1;
    bpm_gm_index_correspondance(nr_bpm_okay,:) = [index_bpm, index_gm, gm_ts(index_gm)-bpm_ts(index_bpm)];

  end

end
bpm_gm_index_correspondance((nr_bpm_okay)+1:end,:) = [];

%figure(1000);
%plot(bpm_gm_x_index_correspondance(:,1));

%figure(1001);
%plot(bpm_gm_x_index_correspondance(:,2));

figure(1003);
plot(bpm_gm_index_correspondance(:,3));
title('Difference between GM and BPM data of same index');

%figure(1004);
%plot(bpm_gm_x_index_correspondance(:,1), bpm_gm_x_index_correspondance(:,2));

%figure(1005);
%plot(bpm_gm_x_index_correspondance(2:end,2) - bpm_gm_x_index_correspondance(1:end-1,2));



