function []=prepare_lattice_tracking()
% Remove corrector , skew quad and sextupole strengths, remove multipoles
% components, and reset offsets. Also create a bunch at the right energy.
%
% If S,x,y,sigmax or sigmay are retrived, tracking is performed and beam position
% and size are computed at each element.

% The essance here is that the highest influence comes from the steering
% magnets, which cause no-linearities via large offsets. 
%
% The second thing that has a small influence is the coupling. But it only
% contributes to 6% change in the response matrices. Also in reality there will
% we such a coupling so we do not set the elemets to zero. 
%
% The rest (sextupoles and multipoles) can be neglected for the R calculation.
% Hence, it is commented out.

global BEAMLINE;
global PS;
global GIRDER;

%load('/Users/jpfingst/Work/ATF2/FlightSim/latticeFiles/src/v4.5/ATF2lat_BX10BY1.mat','Beam1_IEX');
IEX=findcells(BEAMLINE,'Name','IEX');
IP=findcells(BEAMLINE,'Name','IP');

%get bend angle
%bend=findcells(BEAMLINE,'Class','SBEN',IEX,IP);
%for i=1:length(bend)
%    angle(i)=BEAMLINE{bend(i)}.Angle;
%    Bbend(i)=BEAMLINE{bend(i)}.B(1);
%    Sbend(i)=BEAMLINE{bend(i)}.S;
%    %set offset to 0
%    BEAMLINE{bend(i)}.Offset=zeros(1,6);
%end

%get quad info and set skew quads to 0
%quad=findcells(BEAMLINE,'Class','QUAD',IEX,IP);
%nskew=0;
%for i=1:length(quad)
%    PSquad(i)=BEAMLINE{quad(i)}.PS;
%    tilt=BEAMLINE{quad(i)}.Tilt;
%    if abs(tilt-pi/4)<0.1
%        nskew=nskew+1;
%        PS(PSquad(i)).Ampl=0;
%        BEAMLINE{quad(i)}.B=0;
%    end
%    Squad(i)=BEAMLINE{quad(i)}.S;
%    quadname{i}=BEAMLINE{quad(i)}.Name;
%    quadstrength(i)=PS(PSquad(i)).Ampl*BEAMLINE{quad(i)}.B;
%    %set offset to 0
%    BEAMLINE{quad(i)}.Offset=zeros(1,6);
%end
%fprintf('%i skew quads set to 0\n',nskew/2);

%set correctors to 0
xcorrector=findcells(BEAMLINE,'Class','XCOR',IEX,IP);
ycorrector=findcells(BEAMLINE,'Class','YCOR',IEX,IP);
ncor=0;
for i=[xcorrector ycorrector]
    ncor=ncor+1;
    BEAMLINE{i}.B=0;
    PS(BEAMLINE{i}.PS).Ampl=0;
end
fprintf('%i correctors set to 0\n',ncor);

%%set sextupoles to 0
%sext=findcells(BEAMLINE,'Class','SEXT',IEX,IP);
%nsext=0;
%for i=sext
%    nsext=nsext+1;
%    BEAMLINE{i}.B=0;
%    PS(BEAMLINE{i}.PS).Ampl=0;
%end
%fprintf('%i sextupoles set to 0\n',nsext/2);

%%set mulipoles to 0
%mult=findcells(BEAMLINE,'Class','MULT',IEX,IP);
%nmult=0;
%mult_name={};
%for i=mult
%    mult_name{end+1}=BEAMLINE{i}.Name;
%    nmult=nmult+1;
%    BEAMLINE{i}.B=0;
%    BEAMLINE{i}.Tilt=0;
%    BEAMLINE{i}.PoleIndex=0;
%end
%fprintf('%i multipoles set to 0\n',nmult/2);

%set energy of the beam to the design value of the lattice
%beam_energy=BEAMLINE{1}.P;
%npart=length(Beam1_IEX.Bunch.x(6,:));
%dE_E=std(Beam1_IEX.Bunch.x(6,:))/mean(Beam1_IEX.Bunch.x(6,:));
%Beam1_IEX.Bunch.x(6,:)=beam_energy*(1+randn(1,npart)*dE_E);
%fprintf('set beam energy to %f\n',beam_energy);

%elem=IEX:IP;
%nelem=length(elem);
%for i=1:length(elem)
%    if isfield(BEAMLINE{elem(i)},'Girder')
%        BEAMLINE{elem(i)}.Girder=0;
%    end
%end
    
%tracking test and compute S, x and y position, sigmax and sigmay along the
%lattice

%if nargout>1 %Usefull only if user ask for more than 1 output
%    beam=Beam1_IEX;
%    S=NaN(1,nelem);
%    x=NaN(1,nelem);
%    y=NaN(1,nelem);
%    sigmax=NaN(1,nelem);
%    sigmay=NaN(1,nelem);
%    disp('tracking')
%    for i=1:length(elem)
%        [stat,beam]=TrackThru(elem(i),elem(i),beam,1,1);
%        if stat{1}~=1
%            fprintf('error during the tracking: %s\n',stat{2})
%            break
%        end
%        if isfield(BEAMLINE{elem(i)},'L')
%            L=BEAMLINE{elem(i)}.L;
%        else
%            L=0;
%        end
%        S(i)=BEAMLINE{elem(i)}.S+L;
%        x(i)=mean(beam.Bunch.x(1,:));
%        y(i)=mean(beam.Bunch.x(3,:));
%        sigmax(i)=std(beam.Bunch.x(1,:));
%        sigmay(i)=std(beam.Bunch.x(3,:));
%        if (abs(x(i))>1 || abs(y(i))>1)
%            fprintf('beam diverged at %s: x=%.2fm y=%.2fm\n',BEAMLINE{elem(i)}.Name,x(i),elem(i));
%            break
%        end
%    end
%end
