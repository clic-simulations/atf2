% Function to interpolate missing time steps in taken data sets 
%
% Juergen Pfingstner
% 31th of March 2014

function [data_inter, ts_inter, data_marker] = interpolate_missing_time_stamps(data, ts, col_index)

delta_ts_nominal=1/3.1243;
epsilon_ts = delta_ts_nominal/2;
ts_test = ts(:, col_index);

% Debug
ts_should_be = (0:(length(ts)-1)).*delta_ts_nominal+ts(1);
figure(20);
plot(ts_test' - ts_should_be, '-b');
grid on;
hold on;
% dEBUG

[nr_time_steps, nr_data_sets] = size(data);
[~, nr_ts_sets] = size(ts);
data_inter = zeros(round(nr_time_steps*1.1), nr_data_sets);
ts_inter = zeros(round(nr_time_steps*1.1), nr_ts_sets);
data_marker = zeros(round(nr_time_steps*1.1), 1);

% The first data point is always assumed to be okay
data_inter(1,:) = data(1,:);
ts_inter(1,:) = ts(1,:);
data_marker(1) = 1; 

index_inter = 2;
for i = 2:nr_time_steps

    delta_ts = ts_test(i)-ts_test(i-1);
    if(abs(delta_ts-delta_ts_nominal) > epsilon_ts)
        % One or more time steps are missing here. We have to add them via interpolation
        nr_points_missing = round((delta_ts-delta_ts_nominal)/delta_ts_nominal);
        for j = 1:nr_data_sets
            interpolation = linspace(data(i-1,j), data(i,j), nr_points_missing+2);
            data_inter(index_inter:(index_inter+nr_points_missing-1),j) = interpolation(2:end-1)';
        end
        for j = 1:nr_ts_sets
            interpolation = linspace(ts(i-1,j), ts(i,j), nr_points_missing+2);
            ts_inter(index_inter:(index_inter+nr_points_missing-1),j) = interpolation(2:end-1)';
        end
        index_inter = index_inter + nr_points_missing;
        fprintf(1, 'In special treatment at %i (%i points inserted)\n', i, nr_points_missing);
    end

    % Add the next point
    data_inter(index_inter,:) = data(i,:);
    ts_inter(index_inter,:) = ts(i,:);
    data_marker(index_inter) = 1;
    index_inter = index_inter + 1;
end
data_inter(index_inter:end,:) = [];
ts_inter(index_inter:end, :) = [];
data_marker(index_inter:end, :) = [];

% Debug
ts_should_be = (0:(length(ts_inter(:,col_index))-1)).*delta_ts_nominal+ts(1);
figure(20);
plot(ts_inter(:,col_index)' - ts_should_be, '-r');
grid on;
hold on;
title('Data timing with respect to perfect reference');
legend('before', 'after');
% dEBUG

