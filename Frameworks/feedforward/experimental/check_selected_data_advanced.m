function check_okay = check_selected_data_advanced(gm_dm_x, bpm_dm_low_charge)

% The missing points of the gm (zeros in gm_dm_x, gm_dm_y) should be at the 
% same time as the too low charge values of the BPM data (bpm_dm_low_charge).
%
% Note that gm_dm_x was already checked to be gm_dm_y.

compare_array = xor(gm_dm_x, bpm_dm_low_charge);
if(sum(compare_array)>0.5)
    fprintf(1, 'The missing GM data points are not at the same locations at the low BPM low charge values!\n');
    check_okay = 0;
else
    check_okay = 1;
end

