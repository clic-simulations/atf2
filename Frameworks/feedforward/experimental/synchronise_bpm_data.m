function [bpm_data_x, bpm_data_y, bpm_data_charge, bpm_ts_x, bpm_ts_y, bpm_ts_charge, sync_flag] = synchronise_bpm_data(bpm_data_x, bpm_data_y, bpm_data_charge, bpm_ts_x, bpm_ts_y, bpm_ts_charge)

%% Parameter 

treshold = 1/3.12/2;
ref_bpm = 20;

%%%%%%%%%%%%%%%

bpm_ts_okay = check_bpm_x_y_charge_synchronisation(bpm_ts_x, bpm_ts_y, bpm_ts_charge,treshold, ref_bpm);
if(bpm_ts_okay == 0)
    fprintf(1, 'There are inconsistencies in the time stamps between x, y and charge data!\n');
    % Try to fix it!

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Ensure the same number of time stamps %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    [nr_ts_x, nr_bpm_x] = size(bpm_ts_x);
    [nr_ts_y, nr_bpm_y] = size(bpm_ts_y);
    [nr_ts_charge, nr_bpm_charge] = size(bpm_ts_charge);
    nr_ts = min(nr_ts_x, nr_ts_y, nr_ts_charge);
    bpm_ts_x = bpm_ts_x(1:nr_ts, :);
    bpm_ts_y = bpm_ts_y(1:nr_ts, :);
    bpm_ts_charge = bpm_ts_charge(1:nr_ts, :);
    bpm_data_x = bpm_data_x(1:nr_ts, :);
    bpm_data_y = bpm_data_y(1:nr_ts, :);
    bpm_data_charge = bpm_data_charge(1:nr_ts, :);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Try to shift the data on each other %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Take the bpm_ts_x data as a reference and shift the y data on them

    offset_y = mean(bpm_ts_x(:,ref_bpm)-bpm_ts_y(:,ref_bpm));
    offset_y = round(offset_y./treshold);
    if(offset_y ~= 0)
        if(offset_y > 0)
	    bpm_ts_y(offset_y+1:end,:);
	    bpm_data_y(offset_y+1:end,:)	  
        else
            offset_y = offset_y*(-1);
	    bpm_ts_x(offset_y+1:end,:);
	    bpm_data_x(offset_y+1:end,:)
        end 
        
        [nr_ts_x, nr_bpm_x] = size(bpm_ts_x);
        [nr_ts_y, nr_bpm_y] = size(bpm_ts_y);
        [nr_ts_charge, nr_bpm_charge] = size(bpm_ts_charge);
        nr_ts = min(nr_ts_x, nr_ts_y, nr_ts_charge);
        bpm_ts_x = bpm_ts_x(1:nr_ts, :);
        bpm_ts_y = bpm_ts_y(1:nr_ts, :);
        bpm_ts_charge = bpm_ts_charge(1:nr_ts, :);
        bpm_data_x = bpm_data_x(1:nr_ts, :);
        bpm_data_y = bpm_data_y(1:nr_ts, :);
        bpm_data_charge = bpm_data_charge(1:nr_ts, :);
    end

    % Take the bpm_ts_x data as a reference and shift the charge data on them

    offset_charge = mean(bpm_ts_x(:,ref_bpm)-bpm_ts_charge(:,ref_bpm));
    offset_charge = round(offset_charge./treshold);
    if(offset_charge ~= 0)
        if(offset_charge > 0)
	    bpm_ts_charge(offset_charge+1:end,:);
	    bpm_data_charge(offset_charge+1:end,:)	  
        else
            offset_charge = offset_charge*(-1);
	    bpm_ts_x(offset_charge+1:end,:);
	    bpm_data_x(offset_charge+1:end,:);
	    bpm_ts_y(offset_charge+1:end,:);
	    bpm_data_y(offset_charge+1:end,:);
        end 
        
        [nr_ts_x, nr_bpm_x] = size(bpm_ts_x);
        [nr_ts_y, nr_bpm_y] = size(bpm_ts_y);
        [nr_ts_charge, nr_bpm_charge] = size(bpm_ts_charge);
        nr_ts = min(nr_ts_x, nr_ts_y, nr_ts_charge);
        bpm_ts_x = bpm_ts_x(1:nr_ts, :);
        bpm_ts_y = bpm_ts_y(1:nr_ts, :);
        bpm_ts_charge = bpm_ts_charge(1:nr_ts, :);
        bpm_data_x = bpm_data_x(1:nr_ts, :);
        bpm_data_y = bpm_data_y(1:nr_ts, :);
        bpm_data_charge = bpm_data_charge(1:nr_ts, :);
    end

    % Check again if the data are correct now
    [nr_trains, nr_bpms] = size(bpm_data_x);
    bpm_ts_okay = check_bpm_x_y_charge_synchronisation(bpm_ts_x, bpm_ts_y, bpm_ts_charge);
    if(bpm_ts_okay == 0)
        fprintf(1, 'The inconsistencies could be corrrected.\n');
        sync_flag = 1;
    else
        fprintf(1, 'The inconsistencies could not be corrrected. Sorry!\n');
	synch_flag = 0;
    end
else
    sync_flag = 1;
end