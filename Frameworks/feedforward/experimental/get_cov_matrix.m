function gm_cov=get_cov_matrix(BPMfilename)
%Compute the covariance matrix of the ground motion effect.
%It includes the BPM resolution as in BEAMLINE{BPM}.Res
%If BPMfilename is supplied, it looks for an already computed gm_cov in
%that file, and return it if one is found. If no gm_cov is found, it is
%computed, then added to that file.

%check is gm cov has already been computed for this file.
if exist('BPMfilename') && ~isempty(BPMfilename)
    BPM=load(BPMfilename,'gm_cov');
    if (isfield(BPM,'gm_cov'))
        disp('GM covarience matrice found in %s',BPMfilename)
        gm_cov=BPM.gm_cov;
        return
    end
end

global BEAMLINE
global INSTR
%global PS
%global GIRDER

%Add lucretia to Matlab path
addpath(genpath('/User/jpfingst/Work/Lucretia/src/'))

IEX=findcells(BEAMLINE,'Name','IEX');
IP=findcells(BEAMLINE,'Name','IP');

%remove corrector , skew quad and sextupole strengths, remove multipoles
%components, and reset offsets.
prepare_lattice_tracking();

%create a fake placet latice with just the elements length, to use placet's
%GM generator (As I did not succed to compile the Lucretia GM generator).
make_gm_lattice('gm_lattice.tcl',IEX,IP)
%run the GM generator using this fake lattice.
system('placet-octave run_gm_lattice.tcl');
%load the position of each elements
load('gm_lattice_data.mat')

%get lattice info
npulse=size(posx_quad_no_jitter,2);
quad=findcells(BEAMLINE,'Class','QUAD',IEX,IP);
nquad=length(quad);
bend=findcells(BEAMLINE,'Class','SBEN',IEX,IP);
nbend=length(bend);
bpm=findcells(BEAMLINE,'Class','MONI',IEX,IP);
nbpm=length(bpm);
bpm_instr=zeros(1,nbpm);
for i=1:nbpm
    bpm_instr(i)=findcells(INSTR,'Index',bpm(i));
end

%create a simple centered beam
Beam0_IEX.BunchInterval=3.37e-07;
Beam0_IEX.Bunch.x=[0 0 0 0 0 BEAMLINE{1}.P]';
Beam0_IEX.Bunch.Q=1.602e-13;
Beam0_IEX.Bunch.stop=0;

%tracking with no offsets
[s,~,inst_data]=TrackThru(IEX,IP,Beam0_IEX,1,1);
if (s{1}~=1)
    fprintf('error during initial tracking: %s\n',s{2});
    return
end
xread_alligned=[inst_data{1}.x]';
yread_alligned=[inst_data{1}.y]';

%tracking with GM
xread_no_jitter=zeros(nbpm,npulse);%effects of the GM on horizontal BPM readings
yread_no_jitter=zeros(nbpm,npulse);%effects of the GM on vertical BPM readings
for pulse=1:npulse
    for i=1:nbend
        BEAMLINE{bend(i)}.Offset(1)=posx_bend_no_jitter(i,pulse);
        BEAMLINE{bend(i)}.Offset(3)=posy_bend_no_jitter(i,pulse);
    end
    for i=1:nquad
        BEAMLINE{quad(i)}.Offset(1)=posx_quad_no_jitter(i,pulse);
        BEAMLINE{quad(i)}.Offset(3)=posy_quad_no_jitter(i,pulse);
    end
    for i=1:nbpm
        BEAMLINE{bpm(i)}.Offset(1)=posx_bpm_no_jitter(i,pulse);
        BEAMLINE{bpm(i)}.Offset(3)=posy_bpm_no_jitter(i,pulse);
    end
    [s,~,inst_data]=TrackThru(IEX,IP,Beam0_IEX,1,1);
    if (s{1}~=1)
        fprintf('error during tracking: %s\n',s{2});
        return
    end
    xread_no_jitter(:,pulse)=[inst_data{1}.x]';
    yread_no_jitter(:,pulse)=[inst_data{1}.y]';
end

%compute gm_cov
delta_bpm_read_no_jitter=[xread_no_jitter(:,2:end)-xread_no_jitter(:,1:end-1) ; yread_no_jitter(:,2:end)-yread_no_jitter(:,1:end-1)];
gm_cov=cov(delta_bpm_read_no_jitter');

%Add gm_cov to the BPM file.
if exist('BPMfilename') && ~isempty(BPMfilename)
    save(BPMfilename,'gm_cov','-append')
end