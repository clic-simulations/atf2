% Function that corrects errors in the sensor data, if certain sensors
% did not work at a certain time in x or in y. The file is very specific 
% for certain data sets. 
%
% Juergen Pfingstner
% 3rd of April 2014

function [data_x, data_y, sens_s_x, sens_s_y] = correct_gm_sensor_data(data_x, data_y, sens_s, file_name)

sens_s_x = sens_s;
sens_s_y = sens_s;

if(strcmp(file_name, 'data/ATF2_2013-11-20_09h43m40s.322.mat')==1)

    % There was no sensor installed at QS1X (only 14 not 15 sensors)
    sens_s_x(1) = [];
    sens_s_y(1) = [];

    % Sensor 6 was not working in November 2013 
    sens_s_x(6) = [];
    sens_s_y(6) = [];
    data_x(:,6) = [];
    data_y(:,6) = [];


elseif(strcmp(file_name, 'data/data_20140307_v2/ATF2_2014-03-07_08h12m04s.182.mat')==1)

    % There was no sensor installed at QS1X (only 14 not 15 sensors)
    sens_s_x(1) = [];
    sens_s_y(1) = [];

    % Sensor 5 was not working horizontally in March 2014 
    sens_s_x(5) = [];
    data_x(:,5) = [];

elseif(strcmp(file_name, 'data/data_2014May/ATF2_2014-05-22_01h28m53s.820.mat') ||          strcmp(file_name, 'data/data_2014May/ATF2_2014-05-22_01h46m02s.482.mat')==1)

    % From this time on we use the new synchronisation.
    % Also we load 16 sensor locations instead.

    % The first two sensor loactions are not used (QM6RX and QM7RX) 
    sens_s_x(1) = [];
    sens_s_y(1) = [];
    sens_s_x(2) = [];
    sens_s_y(2) = [];

    % Sensor 5 was not working horizontally in June 2014 
    sens_s_x(5) = [];
    data_x(:,5) = [];

elseif( strcmp(file_name, 'data/data_2014May/ATF2_2014-05-28_10h36m21s.857.mat') || strcmp(file_name, 'data/data_2014May/ATF2_2014-05-28_10h59m01s.052.mat') || strcmp(file_name, 'data/data_2014May/ATF2_2014-05-28_11h35m29s.699.mat') || strcmp(file_name, 'data/data_2014May/ATF2_2014-05-28_11h59m37s.311.mat') || strcmp(file_name, 'data/data_2014May/ATF2_2014-05-28_12h15m38s.278.mat') || strcmp(file_name, 'data/data_2014May/ATF2_2014-05-28_13h01m23s.890.mat') || strcmp(file_name, 'data/data_2014May/ATF2_2014-05-28_13h17m24s.900.mat') || strcmp(file_name, 'data/data_2014May/ATF2_2014-05-28_13h33m25s.961.mat') || strcmp(file_name, 'data/data_2014May/ATF2_2014-05-28_15h48m39s.645.mat') || strcmp(file_name, 'data/data_2014May/ATF2_2014-05-28_16h04m40s.730.mat'))

    % For this measurement series: 
    % the girder has been equiped with additional sensors 

    % The first two sensor loactions are not used (QM6RX and QM7RX) 
    sens_s_x(1) = [];
    sens_s_y(1) = [];
    sens_s_x(2) = [];
    sens_s_y(2) = [];

    % Sensor 4 was not working horizontally
    % Before in June the problem was at sensor 5, but 
    % the sensors have been exchanged by coincident.
    sens_s_x(4) = [];
    data_x(:,4) = [];

    % Sensor 1 and sensor 3 are at the same location, 
    % since sensor 3 has been moved.
    sens_s_x(3) = sens_s_x(1);
    sens_s_y(3) = sens_s_y(1);

    % Here we have to deside, if we use sensor 1 mounted on Q1X or 
    % sensor 3 beside Q1X on the girder. 
    
    % Comment next two lines if QP and not girder motion 
    % should be used 
    %data_x(:,1) = data_x(:,3);
    %data_y(:,1) = data_y(:,3);
    % End comment

    sens_s_x(3) = [];
    data_x(:,3) = [];
    sens_s_y(3) = [];
    data_y(:,3) = [];
end
