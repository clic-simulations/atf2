% Function to find the data between the moments when the beam was switched 
% on and off (new experimental concept). 
%
% Juergen Pfingstner
% 8th of May 2014

function [index_start, index_end] = find_experiment_start_end(charge, treshold, ref_index)

index_start = find(charge(:,20) > treshold, 1, 'first');
index_end = find(charge(:,20) > treshold, 1, 'last');

if(isempty(index_start) || isempty(index_end))
  fprintf(1, 'There was not valid beam detected in the BPM data (charge to small)1\n')
  index_start = -1;
  index_end = -1;
end
