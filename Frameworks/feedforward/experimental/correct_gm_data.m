% The purpose of the function is to detect missing time steps and substitude them 
% with averaged data
%
% Juergen Pfingstner
% 26th of March 2014

function [data_corr, ts_corr] = correct_gm_data(data, ts)

delta_ts=1/3.1243;
epsilon_ts = delta_ts/2;

% Debug
ts_should_be = (0:(length(ts)-1)).*delta_ts+ts(1);
figure(20);
plot(ts' - ts_should_be, '-b');
grid on;
hold on;
% dEBUG

data = data';
[nr_time_steps, nr_sensors] = size(data);
data_corr = zeros(round(nr_time_steps*1.1), nr_sensors);
ts_corr = zeros(round(nr_time_steps*1.1), 1);

% The first data point is always assumed to be okay
data_corr(1,:) = data(1,:);
ts_corr(1,:) = ts(1,:);

index_corr = 2;
for i = 2:nr_time_steps

  if((ts(i)-ts(i-1)-delta_ts) > epsilon_ts)
    % At the moment we assume that only one pulse was missing (from observations)
    fprintf(1, 'In special treatment %i\n', i)
    data_corr(index_corr,:) = (data(i-1,:)+data(i,:))./2;
    ts_corr(index_corr) = (ts(i-1,:)+ts(i,:))./2;
    index_corr = index_corr + 1;
  end

  % Add the next point
  data_corr(index_corr,:) = data(i,:);
  ts_corr(index_corr) = ts(i,:);
  index_corr = index_corr + 1;
end
data_corr(index_corr:end,:) = [];
data_corr = data_corr';
ts_corr(index_corr:end) = [];

% Debug
ts_should_be = (0:(length(ts_corr)-1)).*delta_ts+ts(1);
figure(20);
plot(ts_corr' - ts_should_be, '-r');
grid on;
hold on;
title('GM data timing with respect to perfect reference');
legend('before', 'after');
% dEBUG