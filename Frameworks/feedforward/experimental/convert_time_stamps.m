function [gm_ts, bpm_x_ts, bpm_y_ts, bpm_charge_ts] = convert_time_stamp(gm_structure, bpm_x_ts, bpm_y_ts, bpm_charge_ts)

% Conversion of the different time method to matlab timestamps (float with number of days since 00/00/00 00:00). The number that is added is apparently the different time shifts as a fraction of a day. 
gm_ts=gm_structure.t0+gm_structure.time_transitions/24/60/60+9/24;
size(gm_ts)
bpm_x_ts=bpm_x_ts+16/24;
bpm_y_ts=bpm_y_ts+16/24;
bpm_charge_ts=bpm_charge_ts+16/24;

% remove absolute time and convert to seconds
t0 = bpm_x_ts(1);
bpm_x_ts = (bpm_x_ts - t0)*24*3600;
bpm_y_ts = (bpm_y_ts - t0)*24*3600;
bpm_charge_ts = (bpm_charge_ts - t0)*24*3600;
gm_ts = (gm_ts - t0)*24*3600;

%figure(1);
%plot(bpm_x_ts(2:end,:)-bpm_x_ts(1:end-1,:));
%
%figure(2);
%plot(bpm_y_ts(2:end,:)-bpm_y_ts(1:end-1,:));
%
%figure(3);
%plot(bpm_y_ts(2:end,:)-bpm_x_ts(1:end-1,:));
%
%figure(4);
%plot(gm_ts(2:end,:)-gm_ts(1:end-1,:));
