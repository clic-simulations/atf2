function make_gm_lattice(filename,firstelem,lastelem)

global BEAMLINE;
fid=fopen(filename,'w');
prevelem=firstelem;
if(isfield(BEAMLINE{prevelem},'L'))
    prevelem_l=BEAMLINE{prevelem}.L;
else
    prevelem_l=0;
end
fprintf(fid,'Girder\n');
for elem=firstelem:lastelem
    prevelem_s=BEAMLINE{prevelem}.S;
    elem_s=BEAMLINE{elem}.S;
    if(isfield(BEAMLINE{elem},'L'))
        elem_l=BEAMLINE{elem}.L;
    else
        elem_l=0;
    end
    elem_name=BEAMLINE{elem}.Name;
    if strcmp(BEAMLINE{elem}.Class,'MARK')
        if(elem_s-prevelem_s-prevelem_l)>1e-6
            fprintf(fid,'Drift -length %f\n',elem_s-prevelem_s-prevelem_l);
        end
        fprintf(fid,'Drift -name %s\n',elem_name);
    elseif strcmp(BEAMLINE{elem}.Class,'DRIF')
        if(elem_s-prevelem_s-prevelem_l)>1e-6
            fprintf(fid,'Drift -length %f\n',elem_s-prevelem_s-prevelem_l);
        end
        fprintf(fid,'Drift -name %s -length %f\n',elem_name,elem_l);        
        prevelem=elem;prevelem_l=elem_l; 
    elseif strcmp(BEAMLINE{elem}.Class,'QUAD')
        if(elem_s-prevelem_s-prevelem_l)>1e-6
            fprintf(fid,'Drift -length %f\n',elem_s-prevelem_s-prevelem_l);
        end
        fprintf(fid,'Quadrupole -name %s -length %f\n',elem_name,elem_l);
        fprintf(fid,'Girder\n');
        prevelem=elem;prevelem_l=elem_l; 
    elseif strcmp(BEAMLINE{elem}.Class,'SEXT')
        if(elem_s-prevelem_s-prevelem_l)>1e-6
            fprintf(fid,'Drift -length %f\n',elem_s-prevelem_s-prevelem_l);
        end
        fprintf(fid,'Multipole -type 3 -name %s -length %f\n',elem_name,elem_l);        
        fprintf(fid,'Girder\n');
        prevelem=elem;prevelem_l=elem_l;         
    elseif strcmp(BEAMLINE{elem}.Class,'OCTU')
        if(elem_s-prevelem_s-prevelem_l)>1e-6
            fprintf(fid,'Drift -length %f\n',elem_s-prevelem_s-prevelem_l);
        end
        fprintf(fid,'Mutlipole -type 4 -name %s -length %f\n',elem_name,elem_l);        
        fprintf(fid,'Girder\n');
        prevelem=elem;prevelem_l=elem_l; 
    elseif strcmp(BEAMLINE{elem}.Class,'MULT')
%         if(elem_s-prevelem_s-prevelem_l)>1e-6
%             fprintf(fid,'Drift -length %f\n',elem_s-prevelem_s-prevelem_l);
%         end
%         fprintf(fid,'Multipole -name %s -length %f\n',elem_name,elem_l);
%         prevelem=elem;prevelem_l=elem_l; 
    elseif strcmp(BEAMLINE{elem}.Class,'SBEN')
        if(elem_s-prevelem_s-prevelem_l)>1e-6
            fprintf(fid,'Drift -length %f\n',elem_s-prevelem_s-prevelem_l);
        end
        fprintf(fid,'Sbend -name %s -length %f\n',elem_name,elem_l);        
        fprintf(fid,'Girder\n');
        prevelem=elem;prevelem_l=elem_l; 
    elseif strcmp(BEAMLINE{elem}.Class,'SOLENOID')
    elseif strcmp(BEAMLINE{elem}.Class,'LCAV')
    elseif strcmp(BEAMLINE{elem}.Class,'TCAV')
    elseif strcmp(BEAMLINE{elem}.Class,'MARK')
    elseif strcmp(BEAMLINE{elem}.Class,'XCOR')
    elseif strcmp(BEAMLINE{elem}.Class,'YCOR')
    elseif strcmp(BEAMLINE{elem}.Class,'XYCOR')
    elseif strcmp(BEAMLINE{elem}.Class,'MONI')
        if(elem_s-prevelem_s-prevelem_l)>1e-6
            fprintf(fid,'Drift -length %f\n',elem_s-prevelem_s-prevelem_l);
        end
        fprintf(fid,'Bpm -name %s -length %f\n',elem_name,elem_l);
        prevelem=elem;prevelem_l=elem_l; 
    elseif strcmp(BEAMLINE{elem}.Class,'HMON')
    elseif strcmp(BEAMLINE{elem}.Class,'VMON')
    elseif strcmp(BEAMLINE{elem}.Class,'INST')
    elseif strcmp(BEAMLINE{elem}.Class,'PROF')
    elseif strcmp(BEAMLINE{elem}.Class,'WIRE')
    elseif strcmp(BEAMLINE{elem}.Class,'BLMO')
    elseif strcmp(BEAMLINE{elem}.Class,'SLMO')
    elseif strcmp(BEAMLINE{elem}.Class,'IMON')
    elseif strcmp(BEAMLINE{elem}.Class,'COLL')
    elseif strcmp(BEAMLINE{elem}.Class,'COORD')
    else
        fprintf('%s has unknown class: %s\n', BEAMLINE{elem}.Name, BEAMLINE{elem}.Class)
    end
end