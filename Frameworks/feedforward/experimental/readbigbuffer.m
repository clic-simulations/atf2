%BE CAREFUL ! The "BPM buffer size" of the flight simulator must be >  4725

%npulse=floor(25*60*3.12); %(25 min, 3.12Hz + margin = 4725 pulses)
npulse = 10*60*3
% npulse=100;
FlHwUpdate('flush');
FlHwUpdate('all');
pause(.5)
FlHwUpdate('wait',npulse);
[s,o]=FlHwUpdate('readbuffer');
if(s{1}~=1)
  disp(s{2})
  return
end
[s,o2]=FlHwUpdate('readtsbuffer');
if(s{1}~=1)
  disp(s{2})
  return
end
pulsenum=o(:,1);
pulsenum_ts=o2(:,1);
xread=[];
yread=[];
tmit=[];
ts=[];
pulse=[];
for i=1:length(pulsenum)
  j=find(pulsenum(i)==pulsenum_ts);
  if isempty(j)
    fprintf('pulse %i has no timestamp\n',pulsenum(i));
    continue
  else
    xread=[xread, o(i,2:3:end)'];
    yread=[yread, o(i,3:3:end)'];
    tmit=[tmit, o(i,4:3:end)'];
    ts=[ts, o2(j,2:end)'];
    pulse=[pulse, pulsenum(i)];
  end
end
nbpm=size(xread,1);
if(nbpm~=size(yread,1))
  disp('ERROR: different number of bpm in x and y')
  return
end
if(nbpm~=size(tmit,1))
  disp('ERROR: different number of bpm and tmit values')
  return
end
FL_HwInfo=FL.HwInfo;
FL_SimModel=FL.SimModel;
filename=['./userData/bigbuffer' datestr(now(),30) '.mat'];
save(filename,...
  'pulse','xread','yread','tmit','ts',...
  'INSTR','BEAMLINE','PS','FL_HwInfo','FL_SimModel','GIRDER');

analyse_bigbuffer(filename)
