function [quad_combined] = determine_real_quad_pos(quads,quads_name,nr_char_comp)

% Determine quadrupole parameters
%quads_name=placet_element_get_attribute("ATF2",quads,"name");
nr_quads = length(quads);
index_current_quad = [];
index_current_quad_array = [];
quad_combined = struct('quad_indices', [1;2;3], 'quad_indices_array',[1,2,3]);
quad_count = 0;

for i=1:nr_quads
    if(i==nr_quads)
        % Last element
        if(isempty(index_current_quad)==1)
            index_current_quad = quads(i);
            index_current_quad_array = i;
        else
            index_current_quad = [index_current_quad; quads(i)];
            index_current_quad_array = [index_current_quad_array; i];
        end
        quad_count = quad_count + 1;
        quad_combined(quad_count).quad_indices = index_current_quad; 
        quad_combined(quad_count).quad_indices_array = index_current_quad_array; 
        index_current_quad = [];
        index_current_quad_array = [];
    elseif(strcmp(quads_name(i,1:nr_char_comp),quads_name(i+1,1:nr_char_comp))==1)
        % Still the same QP
        if(isempty(index_current_quad)==1)
            index_current_quad = quads(i);
            index_current_quad_array = i;
        else
            index_current_quad = [index_current_quad; quads(i)];
            index_current_quad_array = [index_current_quad_array; i];
        end
    else
        % New quadrupole found
        quad_count = quad_count + 1;
        if(isempty(index_current_quad)==1)
            index_current_quad = quads(i);
            index_current_quad_array = i;
        else
            index_current_quad = [index_current_quad; quads(i)];
            index_current_quad_array = [index_current_quad_array; i];
        end
        quad_combined(quad_count).quad_indices = index_current_quad; 
        quad_combined(quad_count).quad_indices_array = index_current_quad_array; 
        index_current_quad = [];
        index_current_quad_array = [];
    end
end
