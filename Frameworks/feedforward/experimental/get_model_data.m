% Script to get the model data of the ATF2 lattice for further use
%
% Juergen Pfingstner and Yves Renier
% 1st of April 2014

addpath(genpath('/Users/jpfingst/Work/Lucretia/src/')) % include Lucretia (Juergen)
addpath(genpath('/home/patefon/progs/Lucretia/src/')) % include Lucretia (Marcin)

Beam0_IEX.BunchInterval=3.37e-07;   
Beam0_IEX.Bunch.x=[0 0 0 0 0 BEAMLINE{1}.P]';
Beam0_IEX.Bunch.Q=1.602e-13;
Beam0_IEX.Bunch.stop=0;

IEX=findcells(BEAMLINE,'Name','IEX');
IP=findcells(BEAMLINE,'Name','IP');
bpm_name={};
bpm_s=[];
bpm=findcells(BEAMLINE,'Class','MONI',IEX,IP);
% Select the BPMs that have been recorded 
bpm_select_index = [3:11,13,14,16:18,20:26,28:30,32,33,35:51,57:59];
bpm=bpm(bpm_select_index);
nbpm=length(bpm);
for i=1:nbpm
    bpm_name{i}=BEAMLINE{bpm(i)}.Name;
    bpm_s(end+1)=BEAMLINE{bpm(end)}.S;
end

quad=findcells(BEAMLINE,'Class','QUAD',IEX,IP)';
buffer = [BEAMLINE{quad}];
quad_s = [buffer(:).S]';
quad_L = [buffer(:).L]';
quad_s = quad_s+quad_L/2;
nquad = length(quad);
    
sbend=findcells(BEAMLINE,'Class','SBEN',IEX,IP)';
buffer = [BEAMLINE{sbend}];
sbend_s = [buffer(:).S]';
sbend_L = [buffer(:).L]';
sbend_s = sbend_s+sbend_L/2;
    
[quad_sbend,quad_sbend_order] = sort([quad; sbend]);
quad_sbend_s = [quad_s; sbend_s];
quad_sbend_s = quad_sbend_s(quad_sbend_order);
nquad_sbend=length(quad_sbend);

if(use_sbend_in_R == 1)
    quad = quad_sbend;
    nquad = nquad_sbend;
    quad_s = quad_sbend_s;
end
