%#ok<*FNDSB>
%#ok<*SAGROW>

%BPMfilename='bigbuffer20130611T065531.mat';%file with the BPM reading and the lattice used
%GMfilename='ATF2_2013-06-11_06h45m30s.237.mat';%file with the GM measurement (obtained from .tmds file using analyse.py)
%BPMfilename='bigbuffer20131120T094851.mat';%file with the BPM reading and the lattice used
%GMfilename='ATF2_2013-11-20_09h43m40s.322.mat';%file with the GM measurement (obtained from .tmds file using analyse.py)
BPMfilename='./data/sync_test2_20140228_0045.dat';%file with the BPM reading and the lattice used
BPM_ts_filename='./data/time_stamp_sync_test2_20140228_0045.mat';
GMfilename='./data/ATF2_2014-02-28_00h44m03s.428.mat';%file with the GM measurement (obtained from .tmds file using analyse.py)
BPM_synchro_to=40;%BPM with correct timestamp (used for sync within BPMs measurements)
delta_ts=1/3.12/2; % allowed delta in second between GM timestamp and BPM timestamp (default half the bunch separation)

use_R_calc = 0;
use_sbend_in_R = 1;
interp_method = 'linear';
remove_MFB2FF = 1;
use_jitter_subtraction = 0;
use_jitter_sub_on_gm = 0;
modulo_factor_data = 4;
nr_sensor_avg = 13;
time_shift = 0;
load_model_FL = 0;
use_epics_time_stamp = 1;

%addpath('/Users/jpfingst/Work/ATF2/Yves_heritage/feedforward/');
addpath(genpath('/Users/jpfingst/Work/Lucretia/src/'))% include Lucretia
addpath(genpath('/home/patefon/progs/Lucretia/src/'))% include Lucretia
read = load(BPMfilename);
xread = read(1:(end/2),:);
yread = read((end/2+1):end,:);
ts=load(BPM_ts_filename);
ts=ts.save_data;
ts=ts((end/2+1):end,:);
if(use_epics_time_stamp == 1)
    ts = epicsts2mat(ts);
end
GM=load(GMfilename);
IEX=findcells(BEAMLINE,'Name','IEX');
IP=findcells(BEAMLINE,'Name','IP');

if(load_model_FL == 1)
    FL.HwInfo=FL_HwInfo;
    FL.SimModel=FL_SimModel;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%get BPM information
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

instr_index=[];
beamline_index=[];
bpm_name={};
bpm_s=[];
bpm=findcells(BEAMLINE,'Class','MONI',IEX,IP);
nbpm=length(bpm);
for instr=1:length(INSTR)
    BEAMLINE{INSTR{instr}.Index}.TrackFlag.GetBPMData=0;
    BEAMLINE{INSTR{instr}.Index}.TrackFlag.GetBPMBeamPars=0;  
end 
for i=1:nbpm
    instr_index(i)=findcells(INSTR,'Index',bpm(i));
    beamline_index(i)=bpm(i);
    bpm_name{i}=BEAMLINE{beamline_index(i)}.Name;
    bpm_s(end+1)=BEAMLINE{beamline_index(end)}.S;
    BEAMLINE{bpm(i)}.TrackFlag.GetBPMData=1;
    BEAMLINE{bpm(i)}.TrackFlag.GetBPMBeamPars=1;
end

Beam0_IEX.BunchInterval=3.37e-07;   
Beam0_IEX.Bunch.x=[0 0 0 0 0 BEAMLINE{1}.P]';
Beam0_IEX.Bunch.Q=1.602e-13;
Beam0_IEX.Bunch.stop=0;

[Beam1_IEX,S,x,y,sigmax,sigmay]=prepare_lattice_tracking();
quad=findcells(BEAMLINE,'Class','QUAD',IEX,IP)';
buffer = [BEAMLINE{quad}];
quad_s = [buffer(:).S]';
quad_L = [buffer(:).L]';
quad_s = quad_s+quad_L/2;
nquad = length(quad);
    
sbend=findcells(BEAMLINE,'Class','SBEN',IEX,IP)';
buffer = [BEAMLINE{sbend}];
sbend_s = [buffer(:).S]';
sbend_L = [buffer(:).L]';
sbend_s = sbend_s+sbend_L/2;
    
[quad_sbend,quad_sbend_order] = sort([quad; sbend]);
quad_sbend_s = [quad_s; sbend_s];
quad_sbend_s = quad_sbend_s(quad_sbend_order);
nquad_sbend=length(quad_sbend);

if(use_sbend_in_R == 1)
    quad = quad_sbend;
    nquad = nquad_sbend;
    quad_s = quad_sbend_s;
end

% Combine the separate elements to on bigger one to compare the
% matrices in PLACET and LUCRETIA
quad_name = ['         ';'         '];
for loop_name_collect=1:nquad
    name_temp = BEAMLINE{quad(loop_name_collect)}.Name;
    %name_temp
    quad_name(loop_name_collect,1:length(name_temp))=name_temp;
end
quad_combined = determine_real_quad_pos(quad,quad_name,4);
nquad_combined = length(quad_combined);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%response matrix computation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_R_calc == 1)
    
    RM=zeros(2*nbpm,2*nquad); %response matrix of the quadrupoles on the beam position
    %set bpm resolution to 0 for the tracking
    bpm_resol_old=zeros(length(instr_index));
    for i=1:length(instr_index)
        bpm_resol_old(i)=BEAMLINE{bpm(i)}.Resolution;
        BEAMLINE{bpm(i)}.Resolution = 0.0;
    end
    ampl=1e-6;
    for i=1:nquad
        BEAMLINE{quad(i)}.Offset=[ampl 0 0 0 0 0];
        [~,~,inst_data]=TrackThru(IEX,length(BEAMLINE),Beam0_IEX,1,1);
        read_1=[inst_data{1}.x inst_data{1}.y]';
        % Debug
        %if(i==3)
        %    buffer1=[inst_data{1}.x];
        %    buffer1(1:20)'
        %    std(buffer1)
        %    buffer1=[inst_data{1}.y];
        %    buffer1(1:20)'
        %    std(buffer1)
        %end
        % dEBUG
        BEAMLINE{quad(i)}.Offset=[-ampl 0 0 0 0 0];
        [~,~,inst_data]=TrackThru(IEX,length(BEAMLINE),Beam0_IEX,1,1);
        read_2=[inst_data{1}.x inst_data{1}.y]';
        BEAMLINE{quad(i)}.Offset=[0 0 0 0 0 0];
        RM(:,i)=(read_1-read_2)/(2*ampl);
        % Debug
        %if(i==3)
        %    buffer1=[inst_data{1}.x];
        %    buffer1(1:20)'
        %    std(buffer1)
        %    buffer1=[inst_data{1}.y];
        %    buffer1(1:20)'
        %    std(buffer1)
        %    return
        %end
        % dEBUG
        BEAMLINE{quad(i)}.Offset=[0 0 ampl 0 0 0];
        [~,~,inst_data]=TrackThru(IEX,length(BEAMLINE),Beam0_IEX,1,1);
        read_1=[inst_data{1}.x inst_data{1}.y]';
        BEAMLINE{quad(i)}.Offset=[0 0 -ampl 0 0 0];
        [~,~,inst_data]=TrackThru(IEX,length(BEAMLINE),Beam0_IEX,1,1);
        read_2=[inst_data{1}.x inst_data{1}.y]';
        BEAMLINE{quad(i)}.Offset=[0 0 0 0 0 0];
        RM(:,nquad+i)=(read_1-read_2)/(2*ampl);        
    end
    %restore bpm resolution
    for i=1:nbpm
        BEAMLINE{bpm(i)}.Resolution = bpm_resol_old(i);
    end
end

RM_combined = zeros(nbpm*2, nquad_combined*2);
quad_combined_s = zeros(nquad_combined,1);
for loop_index_combined=1:nquad_combined
    indices_local=quad_combined(loop_index_combined).quad_indices_array;
    if(length(indices_local)==1)
        RM_combined(:,loop_index_combined) = RM(:,indices_local);
        RM_combined(:,loop_index_combined+nquad_combined) = RM(:,indices_local+nquad);
    else  
        RM_combined(:,loop_index_combined) = sum(RM(:,indices_local),2);
        RM_combined(:,loop_index_combined+nquad_combined) = sum(RM(:,indices_local+nquad),2);
    end
    quad_combined_s(loop_index_combined) = mean(quad_s(indices_local));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Remove temperature drifts via high pass filtering 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[b,a]=butter(2, 0.08/1.5,'high');
[nr_sensors,~]=size(GM.interpulse_posx);
for i=1:nr_sensors
    GM.interpulse_posx(i,:) = filtfilt(b,a,GM.interpulse_posx(i,:));
    GM.interpulse_posy(i,:) = filtfilt(b,a,GM.interpulse_posy(i,:));
end

%for i=1:nbpm
%    GM.interpulse_posx(i,:) = filtfilt(b,a,GM.interpulse_posx(i,:));
%    GM.interpulse_posy(i,:) = filtfilt(b,a,GM.interpulse_posy(i,:));
%end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%synchronization of GM and BPM measurement
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Conversion of the different time method to matlab timestamps (float with number of days since 00/00/00 00:00)
% The number that is added is apparently the different time shifts as a fraction of a day. 
GM_ts=GM.t0+GM.time_transitions/24/60/60+9/24;
BPM_ts=ts(instr_index,:)+16/24;

%find a common interval of measurement
min_ts_common=max([GM_ts(1) BPM_ts(1)]);
max_ts_common=min([GM_ts(end) BPM_ts(end)]);
tsBPM_ok=find(sum(logical(BPM_ts>=min_ts_common)&logical(BPM_ts<=max_ts_common))>30);
tsGM_ok=find(logical(GM_ts>=min_ts_common)&logical(GM_ts<=max_ts_common));
tsBPM=BPM_ts(:,tsBPM_ok);
tsGM=GM_ts(tsGM_ok);
if ( isempty(tsBPM_ok) || isempty(tsGM_ok))
    disp('No common interval has been found')
    return
else
    fprintf('found %i GM and %i BPM measurements between %s and %s\n',length(tsGM_ok),length(tsBPM_ok),datestr(min_ts_common),datestr(max_ts_common))
end

% Debug: plot PSD
%GM_temp1 = GM.interpulse_posx(:,tsGM_ok);
%GM_temp = GM_temp1(:,2:end)-GM_temp1(:,1:end-1);
GM_temp = GM.interpulse_posx(:,2:end)-GM.interpulse_posx(:,1:end-1);
%GM_temp = GM.interpulse_posx;
time_temp = GM_ts*24*60*60;
[nr_row, nr_col] = size(GM_temp);
psd_temp = zeros(nr_row, nr_col);
nr_col_irms = floor(nr_col/2);
irms_temp = zeros(nr_row, nr_col_irms);
acf_gm = zeros(nr_row, 101);
delta_f_temp=1/(time_temp(end)-time_temp(1));
freq_temp=[1:nr_col]*delta_f_temp;
for i=1:nr_row
    psd_temp(i,:)=abs(fft(GM_temp(i,:))).^2./delta_f_temp./(nr_col^2);
    buffer = 2*psd_temp(i,1:nr_col_irms);
    buffer = cumsum(buffer(end:-1:1))*delta_f_temp;
    irms_temp(i,:) = sqrt(buffer(end:-1:1));
    acf_gm(i,:) = autocorr(GM_temp(i,:),100);
end

figure(103)
loglog(freq_temp(1:nr_col_irms), psd_temp(:,1:nr_col_irms)');
grid on;
figure(203)
loglog(freq_temp(1:nr_col_irms), irms_temp');
grid on;
figure(303)
plot(acf_gm');
grid on;
% dEBUG: plot PSD

%datestr(tsGM(1:20),'SS:FFF')
%datestr(tsBPM(2,1:20),'SS:FFF')
%datestr(tsBPM(3,1:20),'SS:FFF')
%datestr(tsBPM(4,1:20),'SS:FFF')
%datestr(tsGM([1:20]),'SS:FFF')
%size(tsBPM)
%input('Press a button to continue:');   

%If frequency analysis is needed for GM meas., it goes here. It is because
%some pulses will then be removed, so fft can't be used after that.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Delete the GM pulses where not at least 10 BPMs are synchronised.   %
% The BPM pulse that corresponds to the GM pulse is the one where     %
% the reference BPM (here BPM 40) is within the correct time intercal %
% Only BPMs at the same measurement are considered.                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

npulseGM=length(tsGM);% number of GM pulses in the delta interval
bpm_synchro=zeros(nbpm,npulseGM); %table containing BPM number if synchronized or 0 for each GM pulse
nbpm_synchro=zeros(1,npulseGM); %number of bpm synchronized found each GM pulse
pulses_sync={};%for each GM pulse, list the BPM sync
bpm_BPM_ok={};%for each GM pulse, list the BPM in the delta timestamp, corresponding BPM pulses are in pulse_BPMok
pulse_BPMok={};%for each GM pulse, list the BPM pulses in the delta timestamp, corresponding BPM indexes are in bpm_BPMP_ok
for i=1:npulseGM
    [bpm_BPM_ok{i},pulse_BPMok{i}]=find(tsGM(i)+delta_ts/60/60/24>tsBPM & tsGM(i)-delta_ts/60/60/24<tsBPM);
    npulse_BPMok(i)=length(unique(bpm_BPM_ok{i}));
    if(length(pulse_BPMok{i})>1)
        pulses_sync{i}=pulse_BPMok{i}(bpm_BPM_ok{i}==BPM_synchro_to);
        if length(pulses_sync{i})==1
            bpm_synchro_tmp=find(pulse_BPMok{i}==pulses_sync{i});
            nbpm_synchro(i)=length(bpm_synchro_tmp);
            bpm_synchro(bpm_BPM_ok{i}(bpm_synchro_tmp),i)=bpm_BPM_ok{i}(bpm_synchro_tmp);
        end
    end
end

%keep the GM pulses with at least 10 BPM within the delta interval
pulseGM_synchro=tsGM_ok(nbpm_synchro>10);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Delete the BPM pulses that are not synchronise to the above detected %
% synchronised GM pulses. Then check, if the number of GM and the BPM  %
% pulses is the same.                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

npulseBPM=size(tsBPM,2);%number of BPM pulses in the delta interval
npulseBPM_GMok=size(tsBPM,2);%number of GM pulses corresponding to each BPM pulse
pulseBPM_GMok={};%contains the GM pulses corresponding to each BPM pulse
for i=1:npulseBPM
    pulseBPM_GMok{i}=find(tsGM(pulseGM_synchro)>tsBPM(BPM_synchro_to,i)-delta_ts/60/60/24 & tsGM(pulseGM_synchro)<tsBPM(BPM_synchro_to,i)+delta_ts/60/60/24);
    npulseBPM_GMok(i)=length(pulseBPM_GMok{i});
end 
%keep the BPM pulses with a GM pulse within the delta interval
pulseBPM_synchro=tsBPM_ok(npulseBPM_GMok==1);

%create variables synchronized
GM_posx_synchro=GM.interpulse_posx(:,pulseGM_synchro); %synchronized posx measured by GM sensors (ground position)
GM_posy_synchro=GM.interpulse_posy(:,pulseGM_synchro); %synchronized posy measured by GM sensors
tsGM_synchro=GM_ts(pulseGM_synchro); %timestamp of synchronized GM sensors measurements 
BPM_posx_synchro=xread(instr_index,pulseBPM_synchro); %synchronized posx measured by BPM sensors (beam position)
BPM_posy_synchro=yread(instr_index,pulseBPM_synchro); %synchronized posy measured by BPM sensors 
BPM_tmit_synchro=tmit(instr_index,pulseBPM_synchro); %synchronized TMIT measured by BPM sensors 
tsBPM_synchro=BPM_ts(:,pulseBPM_synchro); %timestamp of synchronized BPM measurements 
npulse=length(tsGM_synchro); %number of synchronized pulses

if(npulse~=length(tsBPM_synchro))
    disp('error different number of synchronized pulses for GM and BPM')
    return
else
    fprintf('found %i pulses synchronized.\n',npulse)
end

% Debug: check synchronisation 
%figure(111);
%size(GM_ts)
%size(GM.interpulse_posx)
%plot((GM_ts-GM_ts(1))*24*60*60,GM.interpulse_posx(:,1:end-1),'-b');
%hold on;
%grid on;
%plot((GM_ts-GM_ts(1))*24*60*60,GM.interpulse_posx(2,1:end-1),'-c');
%plot((GM_ts-GM_ts(1))*24*60*60,GM.interpulse_posx(3,1:end-1),'-g');
%plot((GM_ts-GM_ts(1))*24*60*60,GM.interpulse_posx(7,1:end-1),'-y');
%plot((tsGM_synchro-GM_ts(1))*24*60*60,GM_posx_synchro(1,:),'-rx');
%plot((tsGM_synchro-GM_ts(1))*24*60*60,GM_posx_synchro(2,:),'-mx');
%plot((tsGM_synchro-GM_ts(1))*24*60*60,GM_posx_synchro(3,:),'-kx');
%plot((tsBPM_synchro(5,:)-GM_ts(1))*24*60*60,GM_posx_synchro(7,:),'-kx');
%return
% dEBUG


% Debug: plot PSD
%GM_temp1 = GM.interpulse_posx(:,tsGM_ok);
%GM_temp = GM_temp1(:,2:end)-GM_temp1(:,1:end-1);
%GM_temp = GM.interpulse_posx(:,2:end)-GM.interpulse_posx(:,1:end-1);
GM_temp = GM_posx_synchro(:,2:end)-GM_posx_synchro(:,1:end-1);
time_temp = tsGM_synchro*24*60*60;
[nr_row, nr_col] = size(GM_temp);
psd_temp = zeros(nr_row, nr_col);
nr_col_irms = floor(nr_col/2);
irms_temp = zeros(nr_row,nr_col_irms);
delta_f_temp=1/(time_temp(end)-time_temp(1));
freq_temp=[1:nr_col]*delta_f_temp;
for i=1:nr_row
    psd_temp(i,:)=abs(fft(GM_temp(i,:))).^2./delta_f_temp./(nr_col^2);
    buffer = 2*psd_temp(i,1:nr_col_irms);
    buffer = cumsum(buffer(end:-1:1))*delta_f_temp;
    irms_temp(i,:)=sqrt(buffer(end:-1:1));
end
figure(301)
loglog(freq_temp(1:nr_col_irms), psd_temp(:,1:nr_col_irms)');
grid on;
figure(401)
loglog(freq_temp(1:nr_col_irms), irms_temp');
grid on;
% dEBUG: plot PSD

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Here times that are obviously not correct or noisy can be removed. 
% This is specific to a certain data set and has to be done via an
% inspection by eye.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Data set BPMfilename='bigbuffer20130611T065531.mat'; and GMfilename='ATF2_2013-06-11_06h45m30s.237.mat;
index_erase_begin = 1;
index_erase_end = length(tsGM_synchro);
%index_erase_end = 900;

GM_posx_synchro = GM_posx_synchro(:,index_erase_begin:index_erase_end);
GM_posy_synchro = GM_posy_synchro(:,index_erase_begin:index_erase_end);
tsGM_synchro = tsGM_synchro(index_erase_begin:index_erase_end);
BPM_posx_synchro = BPM_posx_synchro(:,index_erase_begin:index_erase_end);
BPM_posy_synchro = BPM_posy_synchro(:,index_erase_begin:index_erase_end);
BPM_tmit_synchro = BPM_tmit_synchro(:,index_erase_begin:index_erase_end);
tsBPM_synchro = tsBPM_synchro(:,index_erase_begin:index_erase_end);
npulse=length(tsGM_synchro);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Use not every time step but use larger time intervals 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%if(modulo_factor_data ~= 1)
    GM_posx_synchro = GM_posx_synchro(:,1:modulo_factor_data:npulse);
    GM_posy_synchro = GM_posy_synchro(:,1:modulo_factor_data:npulse);
    tsGM_synchro = tsGM_synchro(1:modulo_factor_data:npulse);
    BPM_posx_synchro = BPM_posx_synchro(:,1:modulo_factor_data:npulse);
    BPM_posy_synchro = BPM_posy_synchro(:,1:modulo_factor_data:npulse);
    BPM_tmit_synchro = BPM_tmit_synchro(:,1:modulo_factor_data:npulse);
    tsBPM_synchro = tsBPM_synchro(:,1:modulo_factor_data:npulse);
    npulse=length(tsGM_synchro);
%end

if(time_shift ~= 0)
    if(time_shift>0)
        fprintf(1,'In new shift function!\n');
        GM_posx_synchro(:,1:end-time_shift) = GM_posx_synchro(:,(1+time_shift):end); 
        GM_posy_synchro(:,1:end-time_shift) = GM_posy_synchro(:,(1+time_shift):end); 
        GM_posx_synchro(:,(end-time_shift+1):end)=[];
        GM_posy_synchro(:,(end-time_shift+1):end)=[];
        tsGM_synchro((end-time_shift+1):end)=[];
        BPM_posx_synchro(:,(end-time_shift+1):end)=[];
        BPM_posy_synchro(:,(end-time_shift+1):end)=[];
        BPM_tmit_synchro(:,(end-time_shift+1):end)=[];
        tsBPM_synchro(:,(end-time_shift+1):end)=[];
        npulse=length(tsGM_synchro);
    else
        time_shift=-time_shift;
        GM_posx_synchro(:,(1+time_shift):end) = GM_posx_synchro(:,1:end-time_shift); 
        GM_posy_synchro(:,(1+time_shift):end) = GM_posy_synchro(:,1:end-time_shift); 
        GM_posx_synchro(:,1:time_shift)=[];
        GM_posy_synchro(:,1:time_shift)=[];
        tsGM_synchro(1:time_shift)=[];
        BPM_posx_synchro(:,1:time_shift)=[];
        BPM_posy_synchro(:,1:time_shift)=[];
        BPM_tmit_synchro(:,1:time_shift)=[];
        tsBPM_synchro(:,1:time_shift)=[];
        npulse=length(tsGM_synchro);
    end
end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%noise subtraction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Here as an example,not used as it showed no real improvement (may worth trying again)

sig_val_max=10;%first singular value considered as noise

[u,s,v]=svd(BPM_posx_synchro);
s(sig_val_max:end,sig_val_max:end)=0;
noisex=u*s*v';
[u,s,v]=svd(BPM_posy_synchro);
s(sig_val_max:end,sig_val_max:end)=0;
noisey=u*s*v';
BPM_posx_synchro_noisesub=BPM_posx_synchro-noisex;
BPM_posy_synchro_noisesub=BPM_posy_synchro-noisey;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%INTENSITY CUTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cut for tmit < 1/3 * mean tmit
tmit_ok2=all(BPM_tmit_synchro(mean(BPM_tmit_synchro,2)~=0,:)>(mean(BPM_tmit_synchro(mean(BPM_tmit_synchro,2)~=0,:),2)/3*ones(1,npulse)));
tmit_ok=find(tmit_ok2);
npulse=length(tmit_ok);
fprintf('removed %i bad pulses (TMIT lower than 1/3 of mean)\n',size(BPM_tmit_synchro,2)-length(tmit_ok));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%READING CUTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%cut BPMs with 0 readings
non_zero_read=std(BPM_posx_synchro,'',2)>1e-8 & std(BPM_posy_synchro,'',2)>1e-8;
if(remove_MFB2FF==1)
    index_MFB2FF = find(strcmp(bpm_name,'MFB2FF'));
    non_zero_read(index_MFB2FF)=0;
end
nbpm_cut=length(find(non_zero_read));
fprintf('found %i bad BPMs (0 reading)\n',length(find(non_zero_read==0)));

% cut for dx>6*std(dx)
dx=BPM_posx_synchro(non_zero_read,tmit_ok(2:end))-BPM_posx_synchro(non_zero_read,tmit_ok(1:end-1)); %posx variation between 2 pulses
dy=BPM_posy_synchro(non_zero_read,tmit_ok(2:end))-BPM_posy_synchro(non_zero_read,tmit_ok(1:end-1));
readx_ok=all(abs(dx)./(std(dx,'',2)*ones(1,npulse-1))<6);
ready_ok=all(abs(dy)./(std(dy,'',2)*ones(1,npulse-1))<6);
fprintf('removed %i bad pulses (reading out of 6 sigmas)\n',length(find(readx_ok==0)));

%CREATE VARIABLES WITH CUT APPLIED
cut_ok=tmit_ok(readx_ok & ready_ok);
xread_cut=BPM_posx_synchro(non_zero_read,cut_ok);
yread_cut=BPM_posy_synchro(non_zero_read,cut_ok);
GM_posx_synchro_cut = GM_posx_synchro(:,cut_ok);
GM_posy_synchro_cut = GM_posy_synchro(:,cut_ok);
ts_cut=tsBPM_synchro(non_zero_read,cut_ok);
tmit_cut=BPM_tmit_synchro(non_zero_read,cut_ok);
npulse=length(cut_ok);
dxread_cut=xread_cut(:,2:end)-xread_cut(:,1:end-1);
dyread_cut=yread_cut(:,2:end)-yread_cut(:,1:end-1);
dts_cut=ts(instr_index,cut_ok(2:end));
bpm_name_cut=bpm_name(non_zero_read);
bpm_s_cut=bpm_s(non_zero_read);
instr_index_cut=instr_index(non_zero_read);
beamline_index_cut=beamline_index(non_zero_read);
bpm_s_cut=bpm_s(non_zero_read);
RM_cut = RM([non_zero_read;non_zero_read],:);
RM_combined_cut = RM_combined([non_zero_read;non_zero_read],:);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%SVD CUT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% for the 5 first modes, if a BPMs is alone in the vector, we remove that
% BPM
%max_value=0.8;
%old_nokx=-1;
%bpm_svd_okx=ones(1,nbpm_cut);
%nokx=length(find(bpm_svd_okx));
%while (old_nokx~=nokx)
%  old_nokx=nokx;
%  [ux,sx,vx]=svd(dxread_cut(find(bpm_svd_okx),:));
%  bpm_svd_okx(find(bpm_svd_okx))=all(abs(ux(:,1:5))<max_value,2);
%  fprintf('%i X bad bpms: ',length(find(~bpm_svd_okx)));
%  fprintf('%s ',bpm_name_cut{~bpm_svd_okx});fprintf('\n');
%  nokx=length(find(bpm_svd_okx));
%end
%old_noky=-1;
%bpm_svd_oky=ones(1,nbpm_cut);
%noky=length(find(bpm_svd_oky));
%while (old_noky~=noky)
%  old_noky=noky;
%  [uy,sy,vy]=svd(dyread_cut(find(bpm_svd_oky),:)); 
%  bpm_svd_oky(find(bpm_svd_oky))=all(abs(uy(:,1:5))<max_value,2);
%  fprintf('%i Y bad bpms: ',length(find(~bpm_svd_oky)));
%  fprintf('%s ',bpm_name_cut{~bpm_svd_oky});fprintf('\n');
%  noky=length(find(bpm_svd_oky));
%end
%CREATE VARIABLES WITH CUT APPLIED
%bpm_ok=find(bpm_svd_okx & bpm_svd_oky);
%xread_cut=xread_cut(bpm_ok,:);
%yread_cut=yread_cut(bpm_ok,:);
%tmit_cut=tmit_cut(bpm_ok,:);
%ts_cut=ts_cut(bpm_ok,:);
%nbpm=length(bpm_ok);
%dxread_cut=xread_cut(:,2:end)-xread_cut(:,1:end-1);
%dyread_cut=yread_cut(:,2:end)-yread_cut(:,1:end-1);
%instr_index=instr_index_cut(bpm_ok);
%beamline_index=beamline_index_cut(bpm_ok);
%bpm_name=bpm_name_cut(bpm_ok);
%bpm_s=bpm_s_cut(bpm_ok);


% Some data inspection
%figure(1);
%plot(dxread_cut')
%grid on

%figure(2);
%plot(dyread_cut')
%grid on

%figure(3);
%plot(xread_cut')
%grid on

%figure(4);
%plot(yread_cut')
%grid on

%figure(5);
%plot(dts_cut')
%grid on

%figure(6);
%plot(ts_cut')
%grid on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the transfer matrix from IEX to all BPMs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%from the lattice (not used but can be usefull)
%Tlat=zeros(2*nbpm,5);
%for i=1:nbpm
%    [~,o]=RmatAtoB(IEX,beamline_index(i));
%    Tlat(i,:)=o(1,[1:4 6]);
%    Tlat(nbpm+i,:)=o(3,[1:4 6]);
%end

%transfer matrix from SVD
%[u,s,v]=svd([dxread_cut;dyread_cut]);
%nmode=5;
%Tsvd=u(:,1:nmode)*s(1:nmode,1:nmode);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The analysis is now performed in the other script %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%covariance matrix from GM simulation on current lattice
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%gm_cov=get_cov_matrix(BPMfilename);
%nbpm_cov=size(gm_cov,1)/2;
%gm_cov=gm_cov([bpm_ok nbpm_cov+bpm_ok],[bpm_ok nbpm_cov+bpm_ok]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% jitter subtraction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Old method

%% with T from SVD
%T=Tsvd; 
%% % with T from lattice
%% T=Tlat([bpm_ok nbpm+bpm_ok],:);
%dparam_reco=inv(T'*inv(gm_cov)*T)*T'*inv(gm_cov)*[dxread_cut;dyread_cut];
%% sould be equivalent and optimized (maybe with inv(gm_cov) ?): to be tested
%% dparam_reco=lscov(T,[dxread_cut;dyread_cut],gm_cov);
%residual_reco=[dxread_cut; dyread_cut]-T*dparam_reco;
%residualx_reco=residual_reco(1:nbpm,:);
%residualy_reco=residual_reco(nbpm+1:2*nbpm,:);

% New method
%bpm_name_cut
index_upstream_bpms = find(strcmp(bpm_name_cut,'MQF1X'));
index_upstream_bpms = [index_upstream_bpms; find(strcmp(bpm_name_cut,'MQF3X'))];
index_upstream_bpms = [index_upstream_bpms; find(strcmp(bpm_name_cut,'MQD5X'))];
index_upstream_bpms = [index_upstream_bpms; find(strcmp(bpm_name_cut,'MQD10X'))];
index_upstream_bpms = [index_upstream_bpms; find(strcmp(bpm_name_cut,'MQF11X'))];
index_upstream_bpms = [index_upstream_bpms; find(strcmp(bpm_name_cut,'MQD12X'))];
index_upstream_bpms = [index_upstream_bpms; find(strcmp(bpm_name_cut,'MQD16X'))];
index_upstream_bpms = [index_upstream_bpms; find(strcmp(bpm_name_cut,'MQF17X'))];
%index_upstream_bpms = [index_upstream_bpms; find(strcmp(bpm_name_cut,'MQF18X'))];
%index_upstream_bpms = [index_upstream_bpms; find(strcmp(bpm_name_cut,'MQF19X'))];
%index_upstream_bpms = [index_upstream_bpms; find(strcmp(bpm_name_cut,'MQF20X'))];
%index_upstream_bpms = [index_upstream_bpms; find(strcmp(bpm_name_cut,'MQF21X'))];
%index_upstream_bpms = [index_upstream_bpms; find(strcmp(bpm_name_cut,'MQM16FF'))];
%index_upstream_bpms = [index_upstream_bpms; find(strcmp(bpm_name_cut,'MQM15FF'))];
index_upstream_bpms = [index_upstream_bpms; index_upstream_bpms+nbpm_cut];
index_downstream_bpms = 1:2*nbpm_cut;
index_downstream_bpms(index_upstream_bpms) = [];
%BPM_upstream = delta_bpm_read(index_upstream_bpms,:);
%BPM_downstream = delta_bpm_read(index_downstream_bpms,:);
dxy_read_cut = [dxread_cut;dyread_cut];
BPM_upstream = dxy_read_cut(index_upstream_bpms,:);
BPM_downstream = dxy_read_cut(index_downstream_bpms,:);
%BPM_corr = BPM_downstream*(inv(BPM_upstream'*BPM_upstream)*BPM_upstream');
%sv_tolerance = 1e-6;
%BPM_corr = BPM_downstream*pinv(BPM_upstream,sv_tolerance);
BPM_corr = BPM_downstream*pinv(BPM_upstream);
%[BPM_corr, sigma, residual_temp] = ols(BPM_downstream', BPM_upstream');
%BPM_corr = BPM_corr';
residual_reco = dxy_read_cut;
if(use_jitter_subtraction == 1)
    residual_reco(index_downstream_bpms,:) = BPM_downstream - BPM_corr*BPM_upstream;
end
residualx_reco = residual_reco(1:nbpm_cut,:);
residualy_reco = residual_reco(1+nbpm_cut:2*nbpm_cut,:);

% Store data for the data analysis
save_time_sec = (tsGM_synchro - tsGM_synchro(1))*24*60*60;
save('bpm_motion_sub_x_20131120T094851.dat', 'residualx_reco','-ascii')
save('bpm_motion_sub_y_20131120T094851.dat', 'residualy_reco','-ascii')
save('bpm_motion_sub_t_20131120T094851.dat', 'save_time_sec','-ascii')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ground motion data to beam jitter projection 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% First with the PLACET model

placet_sensA_s = load('placet_sensor15_s.dat');
placet_quads_s = load('placet_quads_s.dat');
placet_R_model = load('placet_R_with_sbend.dat');
placet_R_model_combined = load('placet_R_with_sbend_combined.dat');
placet_quads_combined_s = load('placet_quads_combined_s.dat');
placet_bpm_s = load('placet_bpm_s.dat');

% There was not sensor installed at QS1X (only 14 not 15 sensors)
missing_sensor_index = 1;
placet_sensA_s(missing_sensor_index) = [];

% Debug
% Here for testing with the data from November 2013 we remove
% sensor 6 which did not work properly 
placet_sensA_s(6) = [];
GM_posx_synchro_cut(6,:) = [];
GM_posy_synchro_cut(6,:) = [];
% dEBUG

% To interpolate it to zero at the beginning of the beam line is
% questionable, since then long waves are not averaged out
placet_pos_quad = [interp1([-1e6; placet_sensA_s], [zeros(1,npulse); GM_posx_synchro_cut], placet_quads_s, interp_method,'extrap');interp1([-1e6; placet_sensA_s], [zeros(1,npulse); GM_posy_synchro_cut], placet_quads_s, interp_method,'extrap')];
placet_pos_quad_combined = [interp1([-1e6; placet_sensA_s], [zeros(1,npulse); GM_posx_synchro_cut], placet_quads_combined_s, interp_method,'extrap');interp1([-1e6; placet_sensA_s], [zeros(1,npulse); GM_posy_synchro_cut], placet_quads_combined_s, interp_method,'extrap')];
placet_pos_bpm = [interp1([-1e6; placet_sensA_s], [zeros(1,npulse); GM_posx_synchro_cut], placet_bpm_s, interp_method,'extrap');interp1([-1e6; placet_sensA_s], [zeros(1,npulse); GM_posy_synchro_cut], placet_bpm_s, interp_method,'extrap')];

placet_delta_pos_quad=placet_pos_quad(:,2:end)-placet_pos_quad(:,1:end-1);   
placet_delta_pos_quad_combined=placet_pos_quad_combined(:,2:end)-placet_pos_quad_combined(:,1:end-1);   
placet_delta_pos_bpm=placet_pos_bpm(:,2:end)-placet_pos_bpm(:,1:end-1);   

ref_offset_first_quad_x = mean(GM_posx_synchro_cut(1:nr_sensor_avg,2:end) - GM_posx_synchro_cut(1:nr_sensor_avg,1:end-1));
ref_offset_first_quad_y = mean(GM_posy_synchro_cut(1:nr_sensor_avg,2:end) - GM_posy_synchro_cut(1:nr_sensor_avg,1:end-1));
for loop_time_index=1:(npulse-1)
    placet_delta_pos_quad(1:end/2,loop_time_index) = placet_delta_pos_quad(1:end/2,loop_time_index) - ref_offset_first_quad_x(loop_time_index);
    placet_delta_pos_quad((end/2+1):end,loop_time_index) = placet_delta_pos_quad((end/2+1):end,loop_time_index) - ref_offset_first_quad_y(loop_time_index);
    placet_delta_pos_quad_combined(1:end/2,loop_time_index) = placet_delta_pos_quad_combined(1:end/2,loop_time_index) - ref_offset_first_quad_x(loop_time_index);
    placet_delta_pos_quad_combined((end/2+1):end,loop_time_index) = placet_delta_pos_quad_combined((end/2+1):end,loop_time_index) - ref_offset_first_quad_y(loop_time_index);
    placet_delta_pos_bpm(1:end/2,loop_time_index) = placet_delta_pos_bpm(1:end/2,loop_time_index) - ref_offset_first_quad_x(loop_time_index);
    placet_delta_pos_bpm((end/2+1):end,loop_time_index) = placet_delta_pos_bpm((end/2+1):end,loop_time_index) - ref_offset_first_quad_y(loop_time_index);
end

placet_effect_delta_GM=placet_R_model*placet_delta_pos_quad-placet_delta_pos_bpm;
placet_effect_delta_GM_combined=placet_R_model_combined*placet_delta_pos_quad_combined-placet_delta_pos_bpm;

if(use_jitter_sub_on_gm == 1)
    jitter_modification = pinv(BPM_upstream)*BPM_upstream; 
    jitter_modification = eye(size(jitter_modification)) - jitter_modification;
    placet_index_downstream_bpms = index_downstream_bpms;
    placet_index_downstream_bpms((end/2-5):(end/2)) = [];
    placet_index_downstream_bpms((end-5):(end)) = [];
    placet_index_downstream_bpms((end/2+1):end) = placet_index_downstream_bpms((end/2+1):end)-6;
    placet_effect_delta_GM(placet_index_downstream_bpms,:) = placet_effect_delta_GM(placet_index_downstream_bpms,:)*jitter_modification;
    placet_effect_delta_GM_combined(placet_index_downstream_bpms,:) = placet_effect_delta_GM_combined(placet_index_downstream_bpms,:)*jitter_modification;
end

% Then with the LUCRETIA model

% There are no information on the position of the sensors in
% LUCRETIA. So I project the s scale of placet to lucretia to be
% able to use the same sensor position. The PLACET and LUCRETIA
% model fit longitudional very well (so no problems).
offset_placet_lucretia=placet_quads_s(5)-quad_s(1);
lucretia_quad_s = quad_s+offset_placet_lucretia;
lucretia_bpm_s = bpm_s_cut+offset_placet_lucretia;
lucretia_quad_combined_s = quad_combined_s+offset_placet_lucretia;

lucretia_pos_quad = [interp1([-1e6; placet_sensA_s], [zeros(1,npulse); GM_posx_synchro_cut], lucretia_quad_s, interp_method,'extrap');interp1([-1e6; placet_sensA_s], [zeros(1,npulse); GM_posy_synchro_cut], lucretia_quad_s, interp_method,'extrap')];
lucretia_pos_quad_combined = [interp1([-1e6; placet_sensA_s], [zeros(1,npulse); GM_posx_synchro_cut], lucretia_quad_combined_s, interp_method,'extrap');interp1([-1e6; placet_sensA_s], [zeros(1,npulse); GM_posy_synchro_cut], lucretia_quad_combined_s, interp_method,'extrap')];
lucretia_pos_bpm = [interp1([-1e6; placet_sensA_s], [zeros(1,npulse); GM_posx_synchro_cut], lucretia_bpm_s, interp_method,'extrap');interp1([-1e6; placet_sensA_s], [zeros(1,npulse); GM_posy_synchro_cut], lucretia_bpm_s, interp_method,'extrap')];

lucretia_delta_pos_quad=lucretia_pos_quad(:,2:end)-lucretia_pos_quad(:,1:end-1);   
lucretia_delta_pos_quad_combined=lucretia_pos_quad_combined(:,2:end)-lucretia_pos_quad_combined(:,1:end-1);   
lucretia_delta_pos_bpm=lucretia_pos_bpm(:,2:end)-lucretia_pos_bpm(:,1:end-1);   

ref_offset_first_quad_x = mean(GM_posx_synchro_cut(1:nr_sensor_avg,2:end) - GM_posx_synchro_cut(1:nr_sensor_avg,1:end-1));
ref_offset_first_quad_y = mean(GM_posy_synchro_cut(1:nr_sensor_avg,2:end) - GM_posy_synchro_cut(1:nr_sensor_avg,1:end-1));
for loop_time_index=1:(npulse-1)
    lucretia_delta_pos_quad(1:end/2,loop_time_index) = lucretia_delta_pos_quad(1:end/2,loop_time_index) - ref_offset_first_quad_x(loop_time_index);
    lucretia_delta_pos_quad((end/2+1):end,loop_time_index) = lucretia_delta_pos_quad((end/2+1):end,loop_time_index) - ref_offset_first_quad_y(loop_time_index);
    lucretia_delta_pos_quad_combined(1:end/2,loop_time_index) = lucretia_delta_pos_quad_combined(1:end/2,loop_time_index) - ref_offset_first_quad_x(loop_time_index);
    lucretia_delta_pos_quad_combined((end/2+1):end,loop_time_index) = lucretia_delta_pos_quad_combined((end/2+1):end,loop_time_index) - ref_offset_first_quad_y(loop_time_index);
    lucretia_delta_pos_bpm(1:end/2,loop_time_index) = lucretia_delta_pos_bpm(1:end/2,loop_time_index) - ref_offset_first_quad_x(loop_time_index);
    lucretia_delta_pos_bpm((end/2+1):end,loop_time_index) = lucretia_delta_pos_bpm((end/2+1):end,loop_time_index) - ref_offset_first_quad_y(loop_time_index);
end

lucretia_effect_delta_GM=RM_cut*lucretia_delta_pos_quad-lucretia_delta_pos_bpm;
lucretia_effect_delta_GM_combined=RM_combined_cut*lucretia_delta_pos_quad_combined-lucretia_delta_pos_bpm;

if(use_jitter_sub_on_gm == 1)
    jitter_modification = pinv(BPM_upstream)*BPM_upstream; 
    jitter_modification = eye(size(jitter_modification)) - jitter_modification;
    size(jitter_modification)
    lucretia_effect_delta_GM(index_downstream_bpms,:) = lucretia_effect_delta_GM(index_downstream_bpms,:)*jitter_modification;
    lucretia_effect_delta_GM_combined(index_downstream_bpms,:) = lucretia_effect_delta_GM_combined(index_downstream_bpms,:)*jitter_modification;
end

save_data = lucretia_delta_pos_quad(1:end/2,:);
save('ground_motion_x_20131120T094851.dat', 'save_data','-ascii')
save_data = lucretia_delta_pos_quad((end/2+1):end,:);
save('ground_motion_y_20131120T094851.dat', 'save_data','-ascii')
save('ground_motion_t_20131120T094851.dat', 'save_time_sec','-ascii')

save_data = lucretia_effect_delta_GM(1:end/2,:);
save('ground_motion_effect_x_20131120T094851.dat', 'save_data','-ascii')
save_data = lucretia_effect_delta_GM((end/2+1):end,:);
save('ground_motion_effect_y_20131120T094851.dat', 'save_data','-ascii')
save('ground_motion_effect_t_20131120T094851.dat', 'save_time_sec','-ascii')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Add some analysis of the created data 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Calculate the beam motion per quadrupole ground motion
%effect_gm_per_qp = zeros(nquad_combined,1);
%for loop_analysis_gm=1:nquad_combined
%    buffer = RM_combined_cut(:,loop_analysis_gm)*lucretia_delta_pos_quad(loop_analysis_gm,:)-lucretia_delta_pos_bpm;
%    effect_gm_per_qp(loop_analysis_gm) = std(std(buffer));
%end
%
%figure(12);
%plot(effect_gm_per_qp)
%grid on;
%
%% Calculate the ground motion per quadrupole
%gm_per_qp = zeros(nquad_combined,1);
%for loop_analysis_gm=1:nquad_combined
%    gm_per_qp(loop_analysis_gm) = std(lucretia_delta_pos_quad(loop_analysis_gm,:));
%end
%
%figure(13);
%plot(gm_per_qp)
%grid on;
%%

%figure(3);
%plot(placet_quads_s,'-xb')
%grid on;
%hold on;
%plot(lucretia_quad_s,'-xm')
%plot(lucretia_quad_combined_s,'-xr')
%plot(lucretia_bpm_s,'-xg')

%figure(1);
%plot(placet_quads_s,placet_pos_quad(1:end/2,5),'-xr')
%grid on;
%hold on;
%plot(placet_quads_combined_s,placet_pos_quad_combined(1:end/2,5),'-xm')
%plot(placet_sensA_s,GM_posx_synchro_cut(:,5),'-xg')
%plot(lucretia_quad_s,lucretia_pos_quad(1:end/2,5),'-ob')
%plot(lucretia_quad_combined_s,lucretia_pos_quad_combined(1:end/2,5),'-oc')

% Compare the results 

%figure(4);
%plot(std(placet_effect_delta_GM(1:end/2,:),0,2),'-xr');
%grid on;
%hold on;
%plot(std(placet_effect_delta_GM_combined(1:end/2,:),0,2),'-xm');
%plot(std(lucretia_effect_delta_GM(1:end/2,:),0,2),'-xb');
%plot(std(lucretia_effect_delta_GM_combined(1:end/2,:),0,2),'-xc');

%figure(5);
%plot(std(placet_effect_delta_GM((end/2+1):end,:),0,2),'-xr');
%grid on;
%hold on;
%plot(std(placet_effect_delta_GM_combined((end/2+1):end,:),0,2),'-xm');
%plot(std(lucretia_effect_delta_GM((end/2+1):end,:),0,2),'-xb');
%plot(std(lucretia_effect_delta_GM_combined((end/2+1):end,:),0,2),'-xc');

%norm(placet_effect_delta_GM((end/2+1):end,:)-placet_effect_delta_GM_combined((end/2+1):end,:),'fro')./norm(placet_effect_delta_GM((end/2+1):end,:),'fro')
%norm(lucretia_effect_delta_GM((end/2+1):end,:)-lucretia_effect_delta_GM_combined((end/2+1):end,:),'fro')./norm(lucretia_effect_delta_GM((end/2+1):end,:),'fro')
%norm(lucretia_effect_delta_GM((end/2+1):(end-6),:)-placet_effect_delta_GM((end/2+1):end,:),'fro')./norm(placet_effect_delta_GM((end/2+1):end,:),'fro')
%norm(lucretia_effect_delta_GM_combined((end/2+1):(end-6),:)-placet_effect_delta_GM_combined((end/2+1):end,:),'fro')./norm(placet_effect_delta_GM_combined((end/2+1):end,:),'fro')

figure(8);
plot(placet_quads_s,std(placet_delta_pos_quad(1:end/2,:),0,2),'-xr');
grid on;
hold on;
plot(placet_quads_combined_s,std(placet_delta_pos_quad_combined(1:end/2,:),0,2),'-xm');
plot(placet_quads_s,std(placet_delta_pos_quad((end/2+1):end,:),0,2),'-xb');
plot(placet_quads_combined_s,std(placet_delta_pos_quad_combined((end/2+1):end,:),0,2),'-xc');

figure(9);
plot(std(GM_posx_synchro_cut(:,2:end)-GM_posx_synchro_cut(:,1:end-1),0,2),'-xb');
grid on;
hold on;
plot(std(GM_posy_synchro_cut(:,2:end)-GM_posy_synchro_cut(:,1:end-1),0,2),'-xr');

figure(10);
plot(std(GM.interpulse_dposx([1:5,7:14],1:end),0,2),'-xb');
grid on;
hold on;
plot(std(GM.interpulse_dposy([1:5,7:14],1:end),0,2),'-xr');


%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluate the error 
%%%%%%%%%%%%%%%%%%%%%%%%%

px_lucretia = std(lucretia_effect_delta_GM(1:end/2,:)-residualx_reco,0,2)./std(lucretia_effect_delta_GM(1:end/2,:)+residualx_reco,0,2);
%px_lucretia = std(lucretia_effect_delta_GM(1:(end/2-6),:)-placet_effect_delta_GM(1:end/2,:),0,2)./std(lucretia_effect_delta_GM(1:(end/2-6),:)+placet_effect_delta_GM(1:end/2,:),0,2);
%px_placet = std(placet_effect_delta_GM(1:end/2,:)-residualx_reco(1:(end-6),:),0,2)./std(placet_effect_delta_GM(1:end/2,:)+residualx_reco(1:(end-6),:),0,2);
py_lucretia = std(lucretia_effect_delta_GM((end/2+1):end,:)-residualy_reco,0,2)./std(lucretia_effect_delta_GM((end/2+1):end,:)+residualy_reco,0,2);
%py_placet = std(placet_effect_delta_GM((end/2+1):end,:)-residualy_reco(1:(end-6),:),0,2)./std(placet_effect_delta_GM((end/2+1):end,:)+residualy_reco(1:(end-6),:),0,2);

rx_lucretia=diag(corr(residualx_reco',lucretia_effect_delta_GM(1:end/2,:)'));
ry_lucretia=diag(corr(residualy_reco',lucretia_effect_delta_GM((end/2+1):end,:)'));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%PLOT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%BPM reading plot
%figure(1)
%subplot(3,1,1);
%title('horizontal beam positions measured (mean subtracted)')
%plot(bpm_s_cut,xread_cut-mean(xread_cut,2)*ones(1,npulse));
%ylim([-1 1]*4e-4)
%xlim([BEAMLINE{IEX}.S BEAMLINE{IP}.S])
%subplot(3,1,2);
%title('vertical beam positions measured (mean subtracted)')
%plot(bpm_s_cut,yread_cut-mean(yread_cut,2)*ones(1,npulse));
%ylim([-1 1]*4e-4)
%xlim([BEAMLINE{IEX}.S BEAMLINE{IP}.S]) 
%subplot(3,1,3);
%title('intensity measured')
%plot(bpm_s_cut,tmit_cut);
%ylim([-0.1 1]*5e9)
%xlim([BEAMLINE{IEX}.S BEAMLINE{IP}.S]) 

%%SVD plot
%figure(2)
%subplot(3,1,1)
%semilogy(1:size(s,1),diag(s),'+b')
%ylim([1e-6 1])
%ylabel('singular values')
%subplot(3,1,2)
%plot(bpm_s,u(1:nbpm,1:5),'-+')
%ylim([-1 1]*.8)
%xlim([BEAMLINE{IEX}.S BEAMLINE{IP}.S]) 
%ylabel('X singular vectors')
%subplot(3,1,3)
%plot(bpm_s,u(nbpm+1:2*nbpm,1:5),'-+')
%ylim([-1 1]*.8)
%xlim([BEAMLINE{IEX}.S BEAMLINE{IP}.S]) 
%ylabel('Y singular vectors')

% std residual plot
figure(2);
subplot(2,1,1);
semilogy(std(dxread_cut'),'-b');
grid on;
hold on;
semilogy(std(residualx_reco'),'-r');
semilogy(std(lucretia_effect_delta_GM(1:end/2,:),0,2),'-xg');
legend('BPM read','BPM jitter sub','GM to BPM');
subplot(2,1,2);
semilogy(std(dyread_cut'),'-b');
grid on;
hold on;
semilogy(std(residualy_reco'),'-r');
semilogy(std(lucretia_effect_delta_GM((end/2+1):end,:),0,2),'-xg');
grid on;
hold on;

figure(3);
plot(px_lucretia,'-xb');
grid on;
hold on;
%plot(px_placet,'-xc');
plot(py_lucretia,'-xr');
%plot(py_placet,'-xm');
%legend('px lucretia','px placet','py lucretia','py placet');
legend('px lucretia','py lucretia');
axis([0 length(px_lucretia) 0 1.2]);

figure(4);
plot(rx_lucretia,'-xb');
grid on;
hold on;
plot(ry_lucretia,'-xr');
legend('rx lucretia','ry lucretia');
axis([0 length(px_lucretia) -1.2 1.2]);

% std residual plot
%figure(20);
%plot(std(dxread_cut'),'-b');
%grid on;
%hold on;
%plot(std(residualx_reco'),'-r');
%xlabel('BPM number');
%ylabel('horizontal motion [m]');
%axis([0 44 0 5e-5])
%legend('$$\Delta$$ BPM readings','after jitter subtraction');

%%residual plot
%figure(3)
%subplot(2,1,1)
%%plot(bpm_s,residualx_reco);
%%xlim([BEAMLINE{IEX}.S BEAMLINE{IP}.S]) 
%plot(residualx_reco);
%subplot(2,1,2)
%%plot(bpm_s,residualy_reco);
%%xlim([BEAMLINE{IEX}.S BEAMLINE{IP}.S]) 
%plot(residualy_reco);

%%lattice check plot (beam size from tracking compared to RMS bpm readings)
%figure(4);
%subplot(2,1,1)
%plot(S,.1*sigmax,'b--',bpm_s_cut,std(BPM_posx_synchro(non_zero_read,cut_ok),'',2),'r+-');
%legend('0.1 of sigma_x from on-line model','std(xread) of BPMs')
%xlabel('S [m]')
%subplot(2,1,2)
%plot(S,.2*sigmay,'b--',bpm_s_cut,std(BPM_posy_synchro(non_zero_read,cut_ok),'',2),'r+-');
%legend('0.2 of sigma_y from on-line model','std(yread) of BPMs')
%xlabel('S [m]')
%%enhance_plot()

%synchronization plot
%figure(6); 
%synchro_ok=find(non_zero_read);
%plot(bpm_synchro(synchro_ok(1:end-1),:)','.')
%xlabel('GM sensors pulse')
%ylabel('BPM synchronized')
%enhance_plot()

figure(123);
plot(lucretia_effect_delta_GM(end/2+30,:)); 
grid on;
hold on; 
plot(residualy_reco(30,:),'r');
