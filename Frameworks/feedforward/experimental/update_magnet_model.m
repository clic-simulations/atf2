% File to update initial model with the stored magnet information
%
% Juergen Pfingstner
% 1st of April 2014

function [beam_updated] = update_magnet_model(BEAMLINE_save, PS_save, beam)

% Here there is a small problem. There are a few different elements 
% in the stored data structures. So we copy only a subset, which 
% represents the whole accelerator though. 

global BEAMLINE; 
global PS;

% Change the energy of the beam
beam_updated = beam;
beam_updated.Bunch.x(6,:)=ones(size(beam_updated.Bunch.x(6,:)))*BEAMLINE_save{1}.P;

% Copy power supplies
for copy_index=1:length(PS_save)
  if(copy_index == length(PS_save))
    PS(copy_index) = PS(copy_index-1);
    PS(copy_index).Element = PS_save(copy_index).Element;
  end
  PS(copy_index).Ampl = PS_save(copy_index).Ampl;
end

% Copy beamline elements
for copy_index=1:1933 
  BEAMLINE{copy_index} = BEAMLINE_save{copy_index};
end
