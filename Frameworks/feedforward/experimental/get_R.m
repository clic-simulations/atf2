% Function to calculate the response matrix
%
% Juergen Pfingstner and Yves Renier
% 1st of April 2014

function [R] = get_R(bpm, quad, beam, bpm_select_index)

%%%%%%%%%%%%%%
% Parameters %
%%%%%%%%%%%%%%

ampl=1e-6;

%%%%%%%%
% Init %
%%%%%%%%

global BEAMLINE
IEX=findcells(BEAMLINE,'Name','IEX');
IP=findcells(BEAMLINE,'Name','IP');

nbpm = length(bpm);
nquad = length(quad);
R=zeros(2*nbpm,2*nquad); % Response matrix
bpm_resol_old=zeros(length(bpm));

%set bpm resolution to 0 for the tracking
for i=1:length(bpm)
  bpm_resol_old(i)=BEAMLINE{bpm(i)}.Resolution;
  BEAMLINE{bpm(i)}.Resolution = 0.0;
end

%%%%%%%%%%%%%%%%%
% Calc response %
%%%%%%%%%%%%%%%%%

for i=1:nquad

  %fprintf(1,'Loop %i\n', i);
  % horizontal
  BEAMLINE{quad(i)}.Offset=[ampl 0 0 0 0 0];
  [~,~,inst_data]=TrackThru(IEX,IP,beam,1,1);
  data_x_temp = [inst_data{1}.x];
  data_y_temp = [inst_data{1}.y];
  read_1=[data_x_temp(bpm_select_index) data_y_temp(bpm_select_index)]';
  
  BEAMLINE{quad(i)}.Offset=[-ampl 0 0 0 0 0];
  [~,~,inst_data]=TrackThru(IEX,IP,beam,1,1);
  data_x_temp = [inst_data{1}.x];
  data_y_temp = [inst_data{1}.y];
  read_2=[data_x_temp(bpm_select_index) data_y_temp(bpm_select_index)]';
  BEAMLINE{quad(i)}.Offset=[0 0 0 0 0 0];
  R(:,i)=(read_1-read_2)/(2*ampl);

  % vertical
  BEAMLINE{quad(i)}.Offset=[0 0 ampl 0 0 0];
  [~,~,inst_data]=TrackThru(IEX,IP,beam,1,1);
  data_x_temp = [inst_data{1}.x];
  data_y_temp = [inst_data{1}.y];
  read_1=[data_x_temp(bpm_select_index) data_y_temp(bpm_select_index)]';

  BEAMLINE{quad(i)}.Offset=[0 0 -ampl 0 0 0];
  [~,~,inst_data]=TrackThru(IEX,IP,beam,1,1);
  data_x_temp = [inst_data{1}.x];
  data_y_temp = [inst_data{1}.y];
  read_2=[data_x_temp(bpm_select_index) data_y_temp(bpm_select_index)]';
  BEAMLINE{quad(i)}.Offset=[0 0 0 0 0 0];
  R(:,nquad+i)=(read_1-read_2)/(2*ampl);        
end

%restore bpm resolution
for i=1:nbpm
  BEAMLINE{bpm(i)}.Resolution = bpm_resol_old(i);
end
