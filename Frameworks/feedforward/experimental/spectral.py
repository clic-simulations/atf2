#import libraries needed
import sys
#numpy for arrays
from numpy.fft.fftpack import rfft
from numpy.fft import fftfreq
from numpy import *
# plt for matlab type plot function
import matplotlib.pyplot as plt
# PSD function to compute PSD with welch's method (averaging)
from matplotlib.mlab import psd,csd

def rfft_yves(x,NFFT,Fs,noverlap):
    numFreqs = NFFT/2 + 1
    step = NFFT - noverlap
    ind = arange(0, len(x) - NFFT + 1, step)
    n = len(ind)
    fftx = zeros((numFreqs,n),complex_)
    for i in range(n):
        thisX = x[ind[i]:ind[i]+NFFT]
        thisX = hanning(NFFT) * thisX
        fftx[:,i] = rfft(thisX)
    fftx*=2/(abs(hanning(NFFT))).sum()
    t = 1./Fs * (ind + NFFT / 2.)
    freqs = float(Fs) / NFFT * arange(numFreqs)
    fftx=mean(fftx,axis=1)
    return fftx, freqs
    
#def smooth(x,window_len=11,window='hanning'):
#    if x.ndim != 1:
#        raise ValueError, "smooth only accepts 1 dimension arrays."
#    if x.size < window_len:
#        raise ValueError, "Input vector needs to be bigger than window size."
#    if window_len<3:
#        return x
#    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
#        raise ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"
#    s=r_[2*x[0]-x[window_len-1::-1],x,2*x[-1]-x[-1:-window_len:-1]]
#    if window == 'flat': #moving average
#        w=ones(window_len,'d')
#    else:  
#        w=eval(window+'(window_len)')
#    y=convolve(w/w.sum(),s,mode='same')
#    return y[window_len:-window_len+1]    


 
#-----------------------------------
# Compute PSD
#-----------------------------------
class spectral:
    'class computing PSD and coherence'
    def __init__(self,GM_sensor=object,nfft=32768):
        print "spectral constructor"
        self.nsensorx=GM_sensor.nsensor
        self.nsensory=GM_sensor.nsensor
        self.posx=GM_sensor.posx
        self.posy=GM_sensor.posy
        self.dposx = GM_sensor.dposx
        self.dposy = GM_sensor.dposy
        self.posx_interpulse=GM_sensor.interpulse_posx
        self.posy_interpulse=GM_sensor.interpulse_posy
        self.dposx_interpulse = GM_sensor.interpulse_dposx
        self.dposy_interpulse = GM_sensor.interpulse_dposy
        self.nfft=nfft
        self.nfft_interpulse = int(len(self.posx_interpulse[1,:])/10)
        self.npoint=GM_sensor.npoint
        self.T=GM_sensor.T
        self.T_interpulse=GM_sensor.T_interpulse
        self.suffix=GM_sensor.date
        self.magnets=GM_sensor.magnets
    
    def choose_data(self, data_option):
        if data_option == 'pos':
            # the full positions
            if (hasattr(self, 'posx') and hasattr(self, 'posy')):
                data_x = self.posx
                data_y = self.posy
                delta_T = self.T
                nfft = self.nfft
            else:
               print 'Data for option ', data_option, 'have not been assigned!'
               return
        elif data_option == 'delta_pos':
            # the delta positions
            if ((hasattr(self, 'dposx')) and (hasattr(self, 'dposy'))):
                data_x = self.dposx
                data_y = self.dposy
                delta_T = self.T
                nfft = self.nfft
            else:
               print 'Data for option', data_option, 'have not been assigned!'
               return
        elif data_option == 'pos_interpulse':
            # the interpulse positions
            if ((hasattr(self, 'posx_interpulse')) and (hasattr(self, 'posy_interpulse'))):
                data_x = self.posx_interpulse
                data_y = self.posy_interpulse
                delta_T = self.T_interpulse
                nfft = self.nfft_interpulse
            else:
               print 'Data for option ', data_option, 'have not been assigned!'
               return
        elif data_option == 'delta_pos_interpulse':
            # the interpulse delta positions
            if ((hasattr(self, 'dposx_interpulse')) and (hasattr(self, 'dposy_interpulse'))):
                data_x = self.dposx_interpulse
                data_y = self.dposy_interpulse
                delta_T = self.T_interpulse
                nfft = self.nfft_interpulse
            else:
               print 'Data for option ', data_option, 'have not been assigned!'
               return
        elif data_option == 'external':
            # the external positions
            if ((hasattr(self, 'posx_external')) and (hasattr(self, 'posy_external'))):
                data_x = self.posx_external
                data_y = self.posy_external
                delta_T = self.T_external
                nfft = self.nfft_external
            else:
               print 'Data for option ', data_option, 'have not been assigned!'
               return
        else:
            print "Unknown data option ", data_option
            return
        return (data_x, data_y, delta_T, nfft)

    def write_psd_data(self, data_option, psd_x, psd_y, freq):
        if data_option == 'pos':
            # the full positions
            self.psd_posx = psd_x
            self.psd_posy = psd_y
            self.freq = freq
        elif data_option == 'delta_pos':
            # the delta positions
            self.psd_dposx = psd_x
            self.psd_dposy = psd_y
            self.freq = freq
        elif data_option == 'pos_interpulse':
            # the interpulse positions
            self.psd_posx_interpulse = psd_x
            self.psd_posy_interpulse = psd_y
            self.freq_interpulse = freq
        elif data_option == 'delta_pos_interpulse':
            # the interpulse delta positions
            self.psd_dposx_interpulse = psd_x
            self.psd_dposy_interpulse = psd_y
            self.freq_interpulse = freq
        elif data_option == 'external':
            # the interpulse delta positions
            self.psd_posx_external = psd_x
            self.psd_posy_external = psd_y
            self.freq_external = freq
        else:
            print "Unknown data option ", data_option
            return
        return

    def compute_psd(self, data_option):
        print "spectral.compute_psd()"
        #if(self.nsensorx==0 or self.nsensory==0):
        #    print "no sensor, quit."
        #    return
        #if(self.nsensorx!=0 and self.nsensory!=0 and self.nsensorx!=self.nsensory):
        #    print "nsensorx must be egual to nsensory"
        #    return
        return_ref = self.choose_data(data_option)
        if (return_ref == []):
            return
        data_x = return_ref[0]
        data_y = return_ref[1]
        delta_T = return_ref[2]
        nfft = return_ref[3]
        #allocate memory for horizontal PSD
        p_x=zeros((len(data_x),nfft/2+1))
        p_y=zeros((len(data_x),nfft/2+1))
        for i in range(0,len(data_x)):
            #psd() compute PSD making FFTs with hanning windows of size nfft, overlaping on hatlf this size
            #compute absolute horizontal PSD
            (psd_temp,freq)=psd(data_x[i,:],NFFT=nfft,Fs=1/delta_T,window=hanning(nfft),noverlap=nfft/3)
            p_x[i,:]=psd_temp
            (psd_temp,freq)=psd(data_y[i,:],NFFT=nfft,Fs=1/delta_T,window=hanning(nfft),noverlap=nfft/3)
            p_y[i,:]=psd_temp
        self.write_psd_data(data_option, p_x, p_y, freq)          
        return

    def load_external_data(self, file_name_x, file_name_y, file_name_t):
        data_x = loadtxt(file_name_x)
        if (data_x == []):
            print "File containing the external horizontal data could not be opened or is empty!"
            return
        data_y = loadtxt(file_name_y)
        if (data_y == []):
            print "File containing the external vertical data could not be opened or is empty!"
            return
        data_t = loadtxt(file_name_t)
        if (data_t == []):
            print "File containing the external time data could not be opened or is empty!"
            return
        self.posx_external = data_x
        self.posy_external = data_y
        self.T_external = mean(data_t[1:]-data_t[0:-1])
        self.nfft_external = int(len(data_x[1,:])/3)
        return
        
    def choose_psd_data(self, data_option):
        if data_option == 'pos':
            # the full positions
            if ((hasattr(self, 'psd_posx')) and (hasattr(self, 'psd_posy'))):
                psd_x = self.psd_posx
                psd_y = self.psd_posy
                #delta_f = mean(self.freq[1:]-self.freq[0:-1])
                freq = self.freq
            else:
               print 'PSD for ', data_option, 'has not been calculated!'
               return
        elif data_option == 'delta_pos':
            # the delta positions
            if ((hasattr(self, 'psd_dposx')) and (hasattr(self, 'psd_dposy'))):
                psd_x = self.psd_dposx
                psd_y = self.psd_dposy
                #delta_f = mean(self.freq[1:]-self.freq[0:-1])
                freq = self.freq
            else:
               print 'PSD for ', data_option, 'has not been calculated!'
               return
        elif data_option == 'pos_interpulse':
            # the interpulse positions
            if ((hasattr(self, 'psd_posx_interpulse')) and (hasattr(self, 'psd_posy_interpulse'))):
                psd_x = self.psd_posx_interpulse
                psd_y = self.psd_posy_interpulse
                #delta_f = mean(self.freq_interpulse[1:]-self.freq_interpulse[0:-1]);
                freq = self.freq_interpulse
            else:
               print 'PSD for ', data_option, 'has not been calculated!'
               return
        elif data_option == 'delta_pos_interpulse':
            # the interpulse delta positions
            if ((hasattr(self, 'psd_dposx_interpulse')) and (hasattr(self, 'psd_dposy_interpulse'))):
                psd_x = self.psd_dposx_interpulse
                psd_y = self.psd_dposy_interpulse
                #delta_f = mean(self.freq_interpulse[1:]-self.freq_interpulse[0:-1]);
                freq = self.freq_interpulse
            else:
               print 'PSD for ', data_option, 'has not been calculated!'
               return
        elif data_option == 'external':
            # the interpulse delta positions
            if ((hasattr(self, 'psd_posx_external')) and (hasattr(self, 'psd_posy_external'))):
                psd_x = self.psd_posx_external
                psd_y = self.psd_posy_external
                freq = self.freq_external
            else:
               print 'PSD for ', data_option, 'has not been calculated!'
               return
        else:
            print "Unknown data option ", data_option
            return
        return (psd_x, psd_y, freq)

    def write_irms_data(self, data_option, irms_x, irms_y):
        if data_option == 'pos':
            # the full positions
            self.irms_posx = irms_x
            self.irms_posy = irms_y
        elif data_option == 'delta_pos':
            # the delta positions
            self.irms_dposx = irms_x
            self.irms_dposy = irms_y
        elif data_option == 'pos_interpulse':
            # the interpulse positions
            self.irms_posx_interpulse = irms_x
            self.irms_posy_interpulse = irms_y
        elif data_option == 'delta_pos_interpulse':
            # the interpulse delta positions
            self.irms_dposx_interpulse = irms_x
            self.irms_dposy_interpulse = irms_y
        elif data_option == 'external':
            # the external positions
            self.irms_posx_external = irms_x
            self.irms_posy_external = irms_y
        else:
            print "Unknown data option ", data_option
            return
        return

    def choose_irms_data(self, data_option):
        if data_option == 'pos':
            # the full positions
            if ((hasattr(self, 'irms_posx')) and (hasattr(self, 'irms_posy'))):
                irms_x = self.irms_posx
                irms_y = self.irms_posy
                #delta_f = mean(self.freq[1:]-self.freq[0:-1])
                freq = self.freq
            else:
               print 'IRMS for ', data_option, 'has not been calculated!'
               return
        elif data_option == 'delta_pos':
            # the delta positions
            if ((hasattr(self, 'irms_dposx')) and (hasattr(self, 'irms_dposy'))):
                irms_x = self.irms_dposx
                irms_y = self.irms_dposy
                #delta_f = mean(self.freq[1:]-self.freq[0:-1])
                freq = self.freq
            else:
               print 'IRMS for ', data_option, 'has not been calculated!'
               return
        elif data_option == 'pos_interpulse':
            # the interpulse positions
            if ((hasattr(self, 'irms_posx_interpulse')) and (hasattr(self, 'irms_posy_interpulse'))):
                irms_x = self.irms_posx_interpulse
                irms_y = self.irms_posy_interpulse
                #delta_f = mean(self.freq_interpulse[1:]-self.freq_interpulse[0:-1]);
                freq = self.freq_interpulse
            else:
               print 'IRMS for ', data_option, 'has not been calculated!'
               return
        elif data_option == 'delta_pos_interpulse':
            # the interpulse delta positions
            if ((hasattr(self, 'irms_dposx_interpulse')) and (hasattr(self, 'irms_dposy_interpulse'))):
                irms_x = self.irms_dposx_interpulse
                irms_y = self.irms_dposy_interpulse
                #delta_f = mean(self.freq_interpulse[1:]-self.freq_interpulse[0:-1]);
                freq = self.freq_interpulse
            else:
               print 'IRMS for ', data_option, 'has not been calculated!'
               return
        elif data_option == 'external':
            # the external positions
            if ((hasattr(self, 'irms_posx_external')) and (hasattr(self, 'irms_posy_external'))):
                irms_x = self.irms_posx_external
                irms_y = self.irms_posy_external
                freq = self.freq_external
            else:
               print 'IRMS for ', data_option, 'has not been calculated!'
               return
        else:
            print "Unknown data option ", data_option
            return
        return (irms_x, irms_y, freq)

    def compute_irms(self, data_option):
        # Choose the wanted data
        return_ref = self.choose_psd_data(data_option)     
        if (return_ref == []):
            return
        p_x = return_ref[0]
        p_y = return_ref[1]
        freq = return_ref[2]
        delta_f = mean(freq[1:]-freq[0:-1])
        irms_x = zeros((len(p_x), len(freq)))
        irms_y = zeros((len(p_x), len(freq)))
        for i in range(0,len(p_x)):
            data_temp = p_x[i,::-1]
            data_temp = data_temp.cumsum()*delta_f
            irms_x[i,:] = sqrt(data_temp[::-1])
            data_temp = p_y[i,::-1]
            data_temp = data_temp.cumsum()*delta_f
            irms_y[i,:] = sqrt(data_temp[::-1])
        self.write_irms_data(data_option, irms_x, irms_y)
        return

#    def compute_fft(self):
#        t=arange(0,self.npoint*self.T,self.T)
#        for i in range(0,self.npoint):
#            self.posx[i,:]=1e-6*sin(2*pi*0.1*t)+1e-5*sin(2*pi*10*t)
#        print "spectral.compute_fft()"
#        if(self.nsensorx==0 and self.nsensory==0):
#            print "no sensor, quit."
#            return
#        if(self.nsensorx!=0 and self.nsensory!=0 and self.nsensorx!=self.nsensory):
#            print "nsensorx must be egual to nsensory"
#            return
#        if(self.nsensorx!=0):
#            #allocate memory for horizontal fft
#            self.fft_ampx=zeros((self.nsensorx,self.nfft/2+1))
#            self.fft_phasex=zeros((self.nsensorx,self.nfft/2+1))
#            for i in range(0,self.nsensorx):
#                #compute absolute fft
#                fftx,freq=rfft_yves(self.posx[i,:],self.nfft,1/self.T,self.nfft/2)
#                self.fft_ampx[i,:]=abs(fftx)
#                self.fft_phasex[i,:]=angle(fftx)
#        if(self.nsensory!=0):
#            #allocate memory for vertical fft
#            self.fft_ampy=zeros((self.nsensory,self.nfft/2+1))
#            self.fft_phasey=zeros((self.nsensory,self.nfft/2+1))
#            for i in range(0,self.nsensorx):
#                #compute absolute fft
#                ffty,freq=rfft_yves(self.posy[i,:],self.nfft,1/self.T,self.nfft/2)
#                self.fft_ampy[i,:]=abs(ffty)
#                self.fft_phasey[i,:]=angle(ffty)
#        self.freq=freq
#        if(self.nsensory==0 and self.nsensorx!=0):
#            return (self.freq,self.fft_ampx,self.fft_phasex)
#        if(self.nsensorx==0 and self.nsensory!=0):
#            return (self.freq,self.fft_ampy,self.fft_phasey)
#        if(self.nsensorx!=0 and self.nsensorx!=0):
#            return (self.freq,self.fft_ampx,self.fft_ampy,self.fft_phasex,self.fft_phasey)

    def choose_corr_data(self, data_option):
        if data_option == 'pos':
            # the full positions
            if ((hasattr(self, 'corr_posx')) and (hasattr(self, 'corr_posy'))):
                corr_x = self.corr_posx
                corr_y = self.corr_posy
                corr_xy = self.corr_posxy                
                freq = self.freq
            else:
               print 'Correlation for ', data_option, 'has not been calculated!'
               return
        elif data_option == 'delta_pos':
            # the delta positions
            if ((hasattr(self, 'corr_dposx')) and (hasattr(self, 'corr_dposy'))):
                corr_x = self.corr_dposx
                corr_y = self.corr_dposy
                corr_xy = self.corr_dposxy                
                freq = self.freq
            else:
               print 'Correlation for ', data_option, 'has not been calculated!'
               return
        elif data_option == 'pos_interpulse':
            # the interpulse positions
            if ((hasattr(self, 'corr_posx_interpulse')) and (hasattr(self, 'corr_posy_interpulse'))):
                corr_x = self.corr_posx_interpulse
                corr_y = self.corr_posy_interpulse
                corr_xy = self.corr_posxy_interpulse
                freq = self.freq_interpulse
            else:
               print 'Correlation for ', data_option, 'has not been calculated!'
               return
        elif data_option == 'delta_pos_interpulse':
            # the interpulse delta positions
            if ((hasattr(self, 'corr_dposx_interpulse')) and (hasattr(self, 'corr_dposy_interpulse'))):
                corr_x = self.corr_dposx_interpulse
                corr_y = self.corr_dposy_interpulse
                corr_xy = self.corr_dposxy_interpulse
                freq = self.freq_interpulse
            else:
               print 'Correlation for ', data_option, 'has not been calculated!'
               return
        elif data_option == 'external':
            # the external positions
            if ((hasattr(self, 'corr_posx_external')) and (hasattr(self, 'corr_posy_external'))):
                corr_x = self.corr_posx_external
                corr_y = self.corr_posy_external
                corr_xy = self.corr_posxy_external
                freq = self.freq_external
            else:
               print 'Correlation for ', data_option, 'has not been calculated!'
               return
        else:
            print "Unknown data option ", data_option
            return
        return (corr_x, corr_y, corr_xy, freq)
        
    def write_corr_data(self, data_option, c_x, c_y, c_xy):
        if data_option == 'pos':
            # the full positions
            self.corr_posx = c_x
            self.corr_posy = c_y
            self.corr_posxy = c_xy
        elif data_option == 'delta_pos':
            # the delta positions
            self.corr_dposx = c_x
            self.corr_dposy = c_y
            self.corr_dposxy = c_xy
        elif data_option == 'pos_interpulse':
            # the interpulse positions
            self.corr_posx_interpulse = c_x
            self.corr_posy_interpulse = c_y
            self.corr_posxy_interpulse = c_xy
        elif data_option == 'delta_pos_interpulse':
            # the interpulse delta positions
            self.corr_dposx_interpulse = c_x
            self.corr_dposy_interpulse = c_y
            self.corr_dposxy_interpulse = c_xy            
        elif data_option == 'external':
            # the external positions
            self.corr_posx_external = c_x
            self.corr_posy_external = c_y
            self.corr_posxy_external = c_xy
        else:
            print "Unknown data option ", data_option
            return
        return
        
    def compute_corr(self, data_option):
        print "spectral.compute_corr()"
        #if(self.nsensorx==0 and self.nsensory==0):
        #    print "no sensor, quit."
        #    return
        #if(self.nsensorx!=0 and self.nsensory!=0 and self.nsensorx!=self.nsensory):
        #    print "nsensorx must be egual to nsensory"
        #    return
        #if(self.nsensorx!=0 and hasattr(self,"Px")==False):
        #    self.compute_psd()
        #if(self.nsensory!=0 and hasattr(self,"Py")==False):
        #    self.compute_psd()
        #number of freq in spectral coherence

        # Choose the correct data
        return_ref = self.choose_data(data_option)
        if (return_ref == []):
            return
        data_x = return_ref[0]
        data_y = return_ref[1]
        delta_T = return_ref[2]
        nfft = return_ref[3]
        return_ref = self.choose_psd_data(data_option)     
        if (return_ref == []):
            return
        p_x = return_ref[0]
        p_y = return_ref[1]
        freq = return_ref[2]
        delta_f = mean(freq[1:]-freq[0:-1])

        nr_data = len(p_x)
        c_x = zeros((nr_data*nr_data,len(freq)))
        c_y = zeros((nr_data*nr_data,len(freq)))
        c_xy = zeros((nr_data*nr_data,len(freq)))
        
        # Calculate correlation in a loop
        for i in range(0,nr_data):
        #for i in range(0,4):
            p_x_i = p_x[i,:]
            p_y_i = p_y[i,:]
            data_x_i = data_x[i,:]
            data_y_i = data_y[i,:]
            for j in range(0,nr_data):
            #for j in range(0,4):
                print 'loop:', i, j
                corr_index = nr_data*i+j
                p_x_j = p_x[j,:]
                p_y_j = p_y[j,:]
                data_x_j = data_x[j,:]
                data_y_j = data_y[j,:]
                
                c_x_ij,freq_temp = csd(data_x_i,data_x_j,NFFT=nfft,Fs=1/delta_T,window=hanning(nfft),noverlap=nfft/3)
                buffer_ij = c_x_ij/sqrt(p_x_i*p_x_j)
                c_x[corr_index,:] = buffer_ij.real

                c_y_ij,freq_temp = csd(data_y_i,data_y_j,NFFT=nfft,Fs=1/delta_T,window=hanning(nfft),noverlap=nfft/3)
                buffer_ij = c_y_ij/sqrt(p_y_i*p_y_j)
                c_y[corr_index,:] = buffer_ij.real

                c_xy_ij,frequ_temp = csd(data_x_i,data_y_j,NFFT=nfft,Fs=1/delta_T,window=hanning(nfft),noverlap=nfft/3)
                buffer_ij = c_xy_ij/sqrt(p_x_i*p_y_j)
                c_xy[corr_index,:] = buffer_ij.real

        # Save the data
        self.write_corr_data(data_option,c_x,c_y,c_xy)
        return (freq,c_x,c_y,c_xy)
#            ncoher=self.nsensorx*self.nsensorx
#            #allocate memory for relative horizontal PSD
#            Prelx=zeros((ncoher,npoint))
#            #allocate memory for relative horizontal coherence
#            self.Cx=zeros((ncoher,npoint))
#            for i in range(0,self.nsensorx):
#                for j in range(0,self.nsensorx):
#                    #index in array
#                    index=i*self.nsensorx+j
#                    #compute relative horizontal PSD
#                    (psd_temp,freq)=psd(self.posx[i,:]-self.posx[j,:],NFFT=self.nfft,Fs=1/self.T,window=hanning(self.nfft),noverlap=self.nfft/2)
#                    Prelx[index,:]=psd_temp
#                    #use relative and absolute PSD to get the horizontal spectral coherence
#                    self.Cx[index,:]=1-Prelx[index,:]/sqrt(self.Px[i,:]*self.Px[j,:])
#        if(self.nsensory!=0 ):
#            #number of coherence in a given plane
#            ncoher=self.nsensory*self.nsensory
#            #allocate memory for relative vertical PSD
#            Prely=zeros((ncoher,npoint))
#            #allocate memory for relative vertical coherence
#            self.Cy=zeros((ncoher,npoint))
#            for i in range(0,self.nsensory):
#                for j in range(0,self.nsensory):
#                    #index in array
#                    index=i*self.nsensory+j
#                    #compute relative vertical PSD
#                    (psd_temp,freq)=psd(self.posy[i,:]-self.posy[j,:],NFFT=self.nfft,Fs=1/self.T,window=hanning(self.nfft),noverlap=self.nfft/2)
#                    Prely[index,:]=psd_temp
#                    #use relative and absolute PSD to get the vertical spectral coherence
#                    self.Cy[index,:]=1-Prely[index,:]/sqrt(self.Py[i,:]*self.Py[j,:])
#        if(self.nsensorx!=0 and self.nsensory!=0):
#            #number of coherence in a given plane
#            ncoher=self.nsensorx*self.nsensorx
#            #allocate memory for relative cross PSD
#            Prelxy=zeros((ncoher,npoint))
#            #allocate memory for relative cross coherence
#            self.Cxy=zeros((ncoher,npoint))
#            for i in range(0,self.nsensorx):
#                for j in range(0,self.nsensory):
#                    #index in array
#                    index=i*self.nsensorx+j
#                    #compute relative cross-plane PSD
#                    (psd_temp,freq)=psd(self.posx[i,:]-self.posy[j,:],NFFT=self.nfft,Fs=1/self.T,window=hanning(self.nfft),noverlap=self.nfft/2)
#                    Prelxy[index,:]=psd_temp
#                    #use relative and absolute PSD to get the cross-plane spectral coherence
#                    self.Cxy[index,:]=1-Prelxy[index,:]/sqrt(self.Px[i,:]*self.Py[j,:])
        #if(self.nsensorx!=0 and self.nsensory==0):
        #    return (self.freq,self.Cx)
        #if(self.nsensorx==0 and self.nsensory!=0):
        #    return (self.freq,self.Cy)
        #if(self.nsensorx!=0 and self.nsensory!=0):

        
    def compute_factor(self):
        print "spectral.compute_factor()"
        if(self.nsensorx==0 and self.nsensory==0):
            print "no sensor, quit."
            return
        if(self.nsensorx!=0 and self.nsensory!=0 and self.nsensorx!=self.nsensory):
            print "nsensorx must be egual to nsensory"
            return
        if(self.nsensorx!=0 and hasattr(self,"Px")==False):
            self.compute_psd()
        if(self.nsensory!=0 and hasattr(self,"Py")==False):
            self.compute_psd()
        if(self.nsensorx!=0):
            #compute mean horizontal asolute PSD
            #channel 15 not connected to a sensor => not in mean
            Px_mean=log(self.Px)
            self.Px_mean=exp(Px_mean.mean(axis=0))
            #compute spectral factor between mean PSD and individual measurements
            self.factorx=zeros((self.nsensorx,self.nfft/2+1))
            for i in range(0,self.nsensorx):
                self.factorx[i,:]=self.Px[i,:]/self.Px_mean
        if(self.nsensory!=0):
            #compute mean vertical asolute PSD
            #channel 15 not connected to a sensor => not in mean
            Py_mean=log(self.Py)
            self.Py_mean=exp(Py_mean.mean(axis=0))
            #compute spectral factor between mean PSD and individual measurements
            self.factory=zeros((self.nsensory,self.nfft/2+1))
            for i in range(0,self.nsensory):
                self.factory[i,:]=self.Py[i,:]/self.Py_mean
        if(self.nsensorx!=0 and self.nsensory==0):
            return (self.freq,self.factorx)
        if(self.nsensorx==0 and self.nsensory!=0):
            return (self.freq,self.factory)
        if(self.nsensorx!=0 and self.nsensory!=0):
            return (self.freq,self.factorx,self.factory)
        

    def compute_all(self):
        print "spectral.compute_all()"
        self.compute_psd()
        self.compute_correlation()
        self.compute_factor()   
#-------------------------------------
# Plot
#-------------------------------------

    def plot_psd(self, data_option):
        #plot all horizontal absolute PSD on same plot, mean PSD is in black
        return_ref = self.choose_psd_data(data_option) 
        if (return_ref == []):
            return
        p_x = return_ref[0]
        p_y = return_ref[1]
        freq = return_ref[2]
        plt.figure(1,figsize=(19.5,9.5))
        plt.loglog(freq,transpose(p_x),'-',label='PSD')
        plt.xlim((0.03,500))
        plt.legend(loc='upper right')
        plt.grid(True)
        plt.ylim((1e-25,1e-5))
        plt.xlabel("freq [Hz]")
        plt.ylabel("PSD_x [m^2/Hz]")
        #plt.savefig("PSDx_"+self.suffix+".png")
        #plt.close(0)
        #plot all vertical absolute PSD on same plot, mean PSD is in black
        plt.figure(2,figsize=(19.5,9.5))
        plt.loglog(freq,transpose(p_y),'-',label='PSD')
        plt.xlim((0.03,500))
        plt.grid(True)
        plt.legend(loc='upper right')
        plt.ylim((1e-25,1e-5))
        plt.xlabel("freq [Hz]")
        plt.ylabel("PSD_y [m^2/Hz]")
        #plt.savefig("PSDy_"+self.suffix+".png")
        plt.show()
        plt.close(1)
        plt.close(2)

    def plot_irms(self, data_option):
        #plot all horizontal absolute IRMS on same plot
        return_ref = self.choose_irms_data(data_option) 
        if (return_ref == []):
            return
        irms_x = return_ref[0]
        irms_y = return_ref[1]
        freq = return_ref[2]
        plt.figure(10,figsize=(19.5,9.5))
        plt.loglog(freq,transpose(irms_x),'-',label='IRMS')
        plt.xlim((0.03,500))
        plt.legend(loc='upper right')
        plt.grid(True)
        #plt.ylim((1e-25,1e-5))
        plt.xlabel("freq [Hz]")
        plt.ylabel("IRMS_x [m]")
        #plt.savefig("IRMSx_"+self.suffix+".png")
        #plt.close(0)
        #plot all vertical absolute PSD on same plot, mean PSD is in black
        plt.figure(11,figsize=(19.5,9.5))
        plt.loglog(freq,transpose(irms_y),'-',label='IRMS')
        plt.xlim((0.03,500))
        plt.grid(True)
        plt.legend(loc='upper right')
        #plt.ylim((1e-25,1e-5))
        plt.xlabel("freq [Hz]")
        plt.ylabel("IRMS_y [m]")
        #plt.savefig("IRMSy_"+self.suffix+".png")
        plt.show()
        plt.close(10)
        plt.close(11)

#    def plot_fft(self):
#        plt.figure(30,figsize=(19.5,9.5))
#        plt.loglog(self.freq,transpose(self.fft_ampx),'-')
#        plt.xlim((0.03,500))
#        plt.ylim((1e-14,1e-4))
#        plt.grid(True)
#        plt.xlabel("freq [Hz]")
#        plt.ylabel("Amp. FFT_x [m/Hz]")
#        plt.savefig("FFT_ampx_"+self.suffix+".png")
#        #plt.close(0)
#        plt.figure(31,figsize=(19.5,9.5))
#        plt.semilogx(self.freq,transpose(self.fft_phasex),'-')
#        plt.xlim((0.03,500))
#        plt.ylim((-pi-.1,pi+.1))
#        plt.grid(True)
#        plt.xlabel("freq [Hz]")
#        plt.ylabel("Phase FFT_x")
#        plt.savefig("FFT_phasex_"+self.suffix+".png")
#        #plt.close(0)
#        plt.figure(32,figsize=(19.5,9.5))
#        plt.loglog(self.freq,transpose(self.fft_ampy),'-')
#        plt.xlim((0.03,500))
#        plt.ylim((1e-14,1e-4))
#        plt.grid(True)
#        plt.xlabel("freq [Hz]")
#        plt.ylabel("Amp. FFT_y [m/Hz]")
#        plt.savefig("FFT_ampy_"+self.suffix+".png")
#        #plt.close(0)
#        plt.figure(33,figsize=(19.5,9.5))
#        plt.semilogx(self.freq,transpose(self.fft_phasey),'-')
#        plt.xlim((0.03,500))
#        plt.ylim((-pi-.1,pi+.1))
#        plt.grid(True)
#        plt.xlabel("freq [Hz]")
#        plt.ylabel("Phase FFT_y")
#        plt.savefig("FFT_phasey_"+self.suffix+".png")
#        plt.show()
#        plt.close(30)
#        plt.close(31)
#        plt.close(32)
#        plt.close(33)

    def plot_psd_subplot(self, data_option):
        #plot all absolute PSD on different subplot, spectral factor on the right
        #vertical are in red, horizontal in blue
        return_ref = self.choose_psd_data(data_option)   
        if (return_ref == []):
            return
        p_x = return_ref[0]
        p_y = return_ref[1]
        freq = return_ref[2]
        plt.figure(5,figsize=(19.5,9.5))
        plt.subplot(len(p_x),2,1)
        plt.title("PSD")
        plt.subplot(self.nsensorx,2,2)
        plt.title("PSD/PSD_mean")
        for i in range(0,len(p_x)):
            plt.subplot(len(p_x),2,2*i+1)
            plt.grid(True)
            plt.loglog(freq,transpose(p_x[i,:]),'b-')
            plt.loglog(freq,transpose(p_y[i,:]),'r-')
            plt.xlim((0.03,500))
            plt.ylim((1e-25,1e-5))
            plt.ylabel(self.magnets[i])
            plt.subplot(len(p_x),2,2*i+2)
            plt.grid(True)
            plt.loglog(freq,transpose(self.factorx[i,:]),'b-')
            plt.loglog(freq,transpose(self.factory[i,:]),'r-')
            plt.loglog(plt.xlim(),(1, 1),'k-')
            plt.xlim((0.03,500))
            plt.ylim((.1,10))
        plt.savefig("PSD_subplot_"+self.suffix+".png")
        plt.show()
        plt.close(5)

#    def plot_fft_subplot(self):
#        plt.figure(6,figsize=(19.5,9.5))
#        plt.subplot(self.nsensorx,2,1)
#        plt.title("Ampl. FFT")
#        plt.subplot(self.nsensorx,2,2)
#        plt.title("Phase FFT")
#        for i in range(0,self.nsensorx):
#            plt.subplot(self.nsensorx,2,2*i+1)
#            plt.grid(True)
#            plt.loglog(self.freq,transpose(self.fft_ampx[i,:]),'b-')
#            plt.loglog(self.freq,transpose(self.fft_ampy[i,:]),'r-')
#            plt.xlim((0.03,500))
#            plt.ylim((1e-14,1e-4))
#            plt.ylabel(self.magnets[i])
#            plt.subplot(self.nsensorx,2,2*i+2)
#            plt.grid(True)
#            plt.semilogx(self.freq,transpose(self.fft_phasex[i,:]),'b-')
#            plt.semilogx(self.freq,transpose(self.fft_phasey[i,:]),'r-')
#            plt.xlim((0.03,500))
#            plt.ylim((-pi-.1,pi+.1))
#        plt.savefig("FFT_subplot_"+self.suffix+".png")
#        plt.show
#        plt.close(6)
        
    def plot_corr(self,data_option):
        # Plot all correlations on the same plot
        return_ref = self.choose_corr_data(data_option)   
        if (return_ref == []):
            return
        c_x = return_ref[0]
        c_y = return_ref[1]
        c_xy = return_ref[2]
        freq = return_ref[3]

        plt.figure(7,figsize=(19.5,9.5))
        plt.semilogx(freq,transpose(c_x),'',label='Corr_x')
        plt.xlim((0.03,500))
        plt.ylim((-1.1,1.1))
        plt.grid(True)
        plt.xlabel("freq [Hz]")
        plt.ylabel("Correlation_x")
        plt.savefig("correlation_"+self.suffix+".png")
        plt.legend(loc='upper right')

        plt.figure(8,figsize=(19.5,9.5))
        plt.semilogx(freq,transpose(c_y),'',label='Corr_y')
        plt.xlim((0.03,500))
        plt.ylim((-1.1,1.1))
        plt.grid(True)
        plt.xlabel("freq [Hz]")
        plt.ylabel("Correlation_y")
        plt.savefig("correlationy_"+self.suffix+".png")
        plt.legend(loc='upper right')

        plt.figure(9,figsize=(19.5,9.5))
        plt.semilogx(freq,transpose(c_xy),'',label='Corr_xy')
        plt.xlim((0.03,500))
        plt.ylim((-1.1,1.1))
        plt.grid(True)
        plt.xlabel("freq [Hz]")
        plt.ylabel("Correlation_xy")
        plt.savefig("correlationxy_"+self.suffix+".png")
        plt.legend(loc='upper right')

        plt.show()
        plt.close(7)
        plt.close(8)
        plt.close(9)

    def plot_corr_subplot(self, data_option):
        # plot all correlations in subplots
        # horizontal in blue, vertical in red, cross in green
        return_ref = self.choose_corr_data(data_option)   
        if (return_ref == []):
            return
        c_x = return_ref[0]
        c_y = return_ref[1]
        c_xy = return_ref[2]
        freq = return_ref[3]
        nr_data = int(sqrt(len(c_x)))
        
        plt.figure(10,figsize=(19.5,9.5))
        for i in range(0,nr_data):
            for j in range(0,nr_data):
                index=i*nr_data+j
                plt.subplot(nr_data, nr_data, index+1)
                plt.grid(True)
                plt.semilogx(freq,transpose(c_x[index,:]),'b-')
                plt.semilogx(freq,transpose(c_y[index,:]),'r-')
                plt.semilogx(freq,transpose(c_xy[index,:]),'g-')
                plt.xlim((0.03,500))
                plt.ylim((-1.1,1.1))
                #if (i==self.nsensorx-1):
                #    plt.xlabel(self.magnets[j])
                #if (j==0):
                #    plt.ylabel(self.magnets[i])

        plt.savefig("correlation_subplot_"+self.suffix+".png")
        plt.show()
        plt.close(10)

    def plot_corr_specific(self,data_option, row='all',column='all'):
        # Plot all correlations on the same plot
        if ((row=='all') and column=='all'):
            print "To print all spectra, please use plot_corr!\n"
            return
        return_ref = self.choose_corr_data(data_option)   
        if (return_ref == []):
            return
        c_x = return_ref[0]
        c_y = return_ref[1]
        c_xy = return_ref[2]
        freq = return_ref[3]
        nr_data = int(sqrt(len(c_x)))

        if ((row=='all') or (column=='all')):
            if(row=='all'):
                # Spectra are symmetric anyway. Let's take the column
                row=column
            plot_indices = range(row*nr_data,(row+1)*nr_data)
        else:
            plot_indices = nr_data*row+column
            
        plt.figure(11,figsize=(19.5,9.5))
        plt.semilogx(freq,transpose(c_x[plot_indices,:]),'',label='Corr_x')
        plt.xlim((0.03,500))
        plt.ylim((-1.1,1.1))
        plt.grid(True)
        plt.xlabel("freq [Hz]")
        plt.ylabel("Correlation_x")
        #plt.savefig("correlation_"+self.suffix+".png")
        plt.legend(loc='upper right')

        plt.figure(12,figsize=(19.5,9.5))
        plt.semilogx(freq,transpose(c_y[plot_indices,:]),'',label='Corr_y')
        plt.xlim((0.03,500))
        plt.ylim((-1.1,1.1))
        plt.grid(True)
        plt.xlabel("freq [Hz]")
        plt.ylabel("Correlation_y")
        #plt.savefig("correlationy_"+self.suffix+".png")
        plt.legend(loc='upper right')

        plt.figure(13,figsize=(19.5,9.5))
        plt.semilogx(freq,transpose(c_xy[plot_indices,:]),'',label='Corr_xy')
        plt.xlim((0.03,500))
        plt.ylim((-1.1,1.1))
        plt.grid(True)
        plt.xlabel("freq [Hz]")
        plt.ylabel("Correlation_xy")
        #plt.savefig("correlationxy_"+self.suffix+".png")
        plt.legend(loc='upper right')

        plt.show()
        plt.close(11)
        plt.close(12)
        plt.close(13)
        
    def plot_all(self):
        self.plot_psd()
        self.plot_psd_subplot()
        self.plot_corr()
        self.plot_corr_subplot()
