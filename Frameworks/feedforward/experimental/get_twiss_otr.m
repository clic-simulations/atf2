% Function to propagate the twiss parameters from the OTR position. 
% Either stored value are used or the nominal twiss parameter are propagated. 
%
% Juergen Pfingstner
% 1st of April 2014

function [twiss_prop] = get_twiss_otr(otr_file_name)

index_otr0 = 1535;
index_end_tracking = 1933;

% Load/Create the twiss parameter at the OTR0 position 
if(strcmp(otr_file_name, 'init'))
    global FL;
    otr0_twiss_x = struct('alpha', FL.SimModel.Twiss.alphax(index_otr0));
    otr0_twiss_x.beta = FL.SimModel.Twiss.betax(index_otr0);
    otr0_twiss_x.eta = FL.SimModel.Twiss.etax(index_otr0);
    otr0_twiss_x.etap = FL.SimModel.Twiss.etapx(index_otr0);
    otr0_twiss_x.nu = FL.SimModel.Twiss.nux(index_otr0);
    otr0_twiss_y = struct('alpha', FL.SimModel.Twiss.alphay(index_otr0));
    otr0_twiss_y.beta = FL.SimModel.Twiss.betay(index_otr0);
    otr0_twiss_y.eta = FL.SimModel.Twiss.etay(index_otr0);
    otr0_twiss_y.etap = FL.SimModel.Twiss.etapy(index_otr0);
    otr0_twiss_y.nu = FL.SimModel.Twiss.nuy(index_otr0);
else
    otr_data = load(otr_file_name);
    otr0_twiss_x = struct('alpha', otr_data.data.alphx);
    otr0_twiss_x.beta = otr_data.data.betax;
    otr0_twiss_x.eta = 0;
    otr0_twiss_x.etap = 0;
    otr0_twiss_x.nu = 0;
    otr0_twiss_y = struct('alpha', otr_data.data.alphy);
    otr0_twiss_y.beta = otr_data.data.betay;
    otr0_twiss_y.eta = 0;
    otr0_twiss_y.etap = 0;
    otr0_twiss_y.nu = 0;
end

% Propagate the data to the beginning of the beam line and then to the end
[stat, iex_twiss] = GetTwiss(index_otr0, 1, otr0_twiss_x, otr0_twiss_y);

iex_twiss_x = struct('alpha', iex_twiss.alphax(1));
iex_twiss_x.beta = iex_twiss.betax(1);
iex_twiss_x.eta = iex_twiss.etax(1);
iex_twiss_x.etap = iex_twiss.etapx(1);
iex_twiss_x.nu = 0;
iex_twiss_y = struct('alpha', iex_twiss.alphay(1));
iex_twiss_y.beta = iex_twiss.betay(1);
iex_twiss_y.eta = iex_twiss.etax(1);
iex_twiss_y.etap = iex_twiss.etapx(1);
iex_twiss_y.nu = 0;

[stat, twiss_prop] = GetTwiss(1, index_end_tracking, iex_twiss_x, iex_twiss_y);
