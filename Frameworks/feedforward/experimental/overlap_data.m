% Function to find the overlap of ground motion and bpm data
%
% Juergen Pfingstner
% 31st of March 2014

function [gm_index_overlap, bpm_index_overlap] = overlap_data(gm_ts, bpm_ts)

%%%%%%%%%%%%%%
% Parameters %
%%%%%%%%%%%%%%

delta_bpm = 1/3.1243;
epsilon_bpm = delta_bpm/5; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Overlap the data approximately %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

min_ts_common = max([gm_ts(1), bpm_ts(1)]);
max_ts_common = min([gm_ts(end), bpm_ts(end)]);

if(min_ts_common > max_ts_common)
  fprintf(1, 'No common data interval found\n');
  gm_index_overlap = [];
  bpm_index_overlap = []; 
  return;
end

bpm_index_overlap = (bpm_ts <= max_ts_common) & (bpm_ts >= min_ts_common);
nr_ts_bpm = sum(bpm_index_overlap);

gm_index_overlap = (gm_ts <= max_ts_common) & (gm_ts >= min_ts_common);
nr_ts_gm = sum(gm_index_overlap);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ensure the same number of time stamps %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(nr_ts_gm ~= nr_ts_bpm)
  if(nr_ts_gm > nr_ts_bpm)
    index_last = find(gm_index_overlap>0.5, 1, 'last');
    gm_index_overlap(index_last) = 0;
    nr_ts_gm = sum(gm_index_overlap);
  else
    index_last = find(bpm_index_overlap>0.5, 1, 'last');
    bpm_index_overlap(index_last) = 0;
    nr_ts_bpm = sum(bpm_index_overlap);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find the GM index that fits best to the first BPM indices. %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

gm_index_temp = zeros(size(gm_index_overlap));
bpm_index_temp = zeros(size(bpm_index_overlap));
index_gm_first = find(gm_index_overlap>0.5, 1, 'first');
index_bpm_first = find(bpm_index_overlap>0.5, 1, 'first');
index_gm_last = find(gm_index_overlap>0.5, 1, 'last');
index_bpm_last = find(gm_index_overlap>0.5, 1, 'last');

avg_time_diff = mean(gm_ts(index_gm_first:index_gm_first+20)-bpm_ts(index_bpm_first:index_bpm_first+20));
time_steps_offset = round(avg_time_diff./delta_bpm);
gm_index_temp((index_gm_first-time_steps_offset):(index_gm_last-time_steps_offset)) = ones(size(gm_index_temp(index_gm_first:index_gm_last)));
gm_index_overlap = gm_index_temp;

% Debug
index_gm_first = find(gm_index_overlap>0.5,1);
index_bpm_first = find(bpm_index_overlap>0.5,1);
avg_time_diff = mean(gm_ts(index_gm_first:index_gm_first+20)-bpm_ts(index_bpm_first:index_bpm_first+20));
time_steps_offset = round(avg_time_diff./delta_bpm);
% dEBUG

fprintf(1, 'Data points overlapping: GM = %i, BPM = %i\n', sum(gm_index_overlap), sum(bpm_index_overlap));
gm_index_overlap = logical(gm_index_overlap);
bpm_index_overlap = logical(bpm_index_overlap);

% Debug
%gm_ts_should_be = (0:(length(gm_ts)-1)).*delta_bpm+gm_ts(1)
%figure(40);
%plot(gm_ts' - gm_ts_should_be, '-b');
%grid on;
%hold on;

%figure(41);
%plot(gm_ts(2:end)-gm_ts(1:end-1), '-b');
%grid on;
%hold on;
% dEBUG