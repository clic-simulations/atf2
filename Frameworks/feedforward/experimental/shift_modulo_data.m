% Function to shift ground motion and BPM data against each other and select
% a subset of the data for the correlation.
% 
% Juergen Pfingstner
% 31th of March 2014

function [gm_data_mod, bpm_data_mod] = shift_modulo_data(gm_data, bpm_data, shift_steps, modulo_steps);

% Shift the data
if(shift_steps > 0)
    gm_data_mod = gm_data(1+shift_steps:end,:);  
    bpm_data_mod = bpm_data(1:end-shift_steps,:);
else
    shift_steps = shift_steps*(-1);
    bpm_data_mod = bpm_data(1+shift_steps:end,:);  
    gm_data_mod = gm_data(1:end-shift_steps,:);
end

% Select a subset of the data

gm_data_mod = gm_data_mod(1:modulo_steps:end,:);
bpm_data_mod = bpm_data_mod(1:modulo_steps:end,:);