% Function to determine were a step in the time stamps occurs first. If there
% is no step the index of the last element is returned.
%
% Juergen Pfingstner
% 25th of March 2014

function [step_index] = get_first_step_index(ts)

time_rep = 1/3.12;
epsilon = time_rep/2;
delta_ts = ts(2:end) - ts(1:end-1);

step_index = find(abs(delta_ts-time_rep)>epsilon,1);
if(isempty(step_index)==1)
  step_index = length(ts)
end
