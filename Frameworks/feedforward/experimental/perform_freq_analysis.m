% Script to analysed the data of the BPM and ground motion correlation 
% study in the frequency space. 
%
% Juergen Pfingstner
% 23th of April 2014

index_ref_sensor = 2;
index_ref_quad = 10;

% Calculate the psd of the ground motion sensors to compare it with the 
% results of the python scripts

%P_x_fft = abs(fft(gm_data_x(:,1))).^2;
%P_x_fft = P_x_fft(1:end/2);
%freq_fft = 1:length(P_x_fft);
%freq_fft = freq_fft./length(freq_fft)*3.12/2;

[np_gm_x, nr_sens_x] = size(gm_data_x);
[np_gm_y, nr_sens_y] = size(gm_data_y);
[P_temp, freq_gm_x] = pwelch(gm_data_x(:,1), [], [], [], 3.12);
P_gm_x = zeros(length(P_temp), nr_sens_x);
[P_temp, freq_gm_y] = pwelch(gm_data_y(:,1), [], [], [], 3.12);
P_gm_y = zeros(length(P_temp), nr_sens_y);
IRMS_gm_y = P_gm_y;
P_diff_gm_y = zeros(length(P_temp), nr_sens_y-1);
IRMS_diff_gm_y = P_diff_gm_y;
[P_temp, freq_bpm_x] = pwelch(bpm_data_x(:,1), [], [], [], 3.12);
P_bpm_x = zeros(length(P_temp), nbpm);
[P_temp, freq_bpm_y] = pwelch(bpm_data_y(:,1), [], [], [], 3.12);
P_bpm_y = zeros(length(P_temp), nbpm);
P_gm_effect_x = P_bpm_x;
P_gm_effect_y = P_bpm_y;

[np_quad_y, nr_quad_y] = size(qp_pos_y);
[P_temp, freq_quad_y] = pwelch(qp_pos_y(:,1), [], [], [], 3.12);
P_diff_quad_y = zeros(length(P_temp), nr_quad_y-1);

for i = 1:nr_sens_x
    [P_temp, ~] = pwelch(gm_data_x(:,i), [], [], [], 3.12);
    P_gm_x(:,i) = P_temp;
end

for i = 1:nr_sens_y
    [P_temp, ~] = pwelch(gm_data_y(:,i), [], [], [], 3.12);
    P_gm_y(:,i) = P_temp;
    IRMS_gm_y(:,i) = irms(P_temp, freq_gm_y);
end

index_diff = 1;
for i = 1:nr_sens_y
    if(i ~= index_ref_sensor)
      [P_temp, ~] = pwelch(gm_data_y(:,i)-gm_data_y(:,index_ref_sensor), [], [], [], 3.12);
        P_diff_gm_y(:,index_diff) = P_temp;
        IRMS_diff_gm_y(:,index_diff) = irms(P_temp, freq_gm_y);
        index_diff = index_diff+1;
    end
end

index_diff = 1;
for i = 1:nr_quad_y
    %if(i ~= index_ref_quad)
        %[P_temp, ~] = pwelch(qp_pos_y(:,i)-qp_pos_y(:,index_ref_quad), [], [], [], 3.12);
        [P_temp, ~] = pwelch(qp_pos_y(:,i), [], [], [], 3.12);
        P_diff_quad_y(:,index_diff) = P_temp;
        %IRMS_diff_quad_y(:,index_diff) = irms(P_temp, freq_quad_y);
        index_diff = index_diff+1;
    %end
end

for i = 1:nbpm
    [P_temp, ~] = pwelch(bpm_data_x(:,i), [], [], [], 3.12);
    P_bpm_x(:,i) = P_temp;
    [P_temp, ~] = pwelch(bpm_data_y(:,i), [], [], [], 3.12);
    P_bpm_y(:,i) = P_temp;
    IRMS_bpm_y(:,i) = irms(P_temp, freq_bpm_y);

    [P_temp, ~] = pwelch(gm_effect_init_combined_x(:,i), [], [], [], 3.12);
    P_gm_effect_x(:,i) = P_temp;
    [P_temp, ~] = pwelch(gm_effect_init_combined_y(:,i), [], [], [], 3.12);
    P_gm_effect_y(:,i) = P_temp;
    IRMS_gm_effect_y(:,i) = irms(P_temp, freq_bpm_y);
end

noise_norm = 0.00001*randn(length(gm_data_x(:,1)+1), 1);
noise_norm = noise_norm(2:end)-noise_norm(1:end-1);
noise_uniform = 0.00001*rand(length(gm_data_x(:,1)+1), 1);
noise_uniform = noise_uniform(2:end)-noise_uniform(1:end-1);
[P_noise_normal, freq] = pwelch(noise_norm, [], [], [], 3.12);
[P_noise_uniform, freq] = pwelch(noise_uniform, [], [], [], 3.12);

%figure(11);
%loglog(freq_gm_x, P_gm_x);
%grid on;
%hold on;
%legend('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13');
%xlabel('frequency [Hz]');
%title('GM x');
%ylabel('PSD [m2/Hz]');

figure(12);
loglog(freq_gm_y, P_gm_y);
grid on;
hold on;
legend('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14');
xlabel('frequency [Hz]');
ylabel('PSD [m2/Hz]');
title('GM y');

figure(22);
loglog(freq_gm_y, IRMS_gm_y);
grid on;
hold on;
legend('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14');
xlabel('frequency [Hz]');
ylabel('IRMS [m]');
title('GM y');

figure(13);
loglog(freq_gm_y, P_diff_gm_y);
grid on;
hold on;
legend('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13');
xlabel('frequency [Hz]');
ylabel('PSD [m2/Hz]');
title('Diff GM y');

figure(23);
loglog(freq_gm_y, IRMS_diff_gm_y);
grid on;
hold on;
legend('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13');
xlabel('frequency [Hz]');
ylabel('IRMS [m]');
title('Diff GM y');

%figure(14);
%%figure(16);
%loglog(freq_bpm_x, P_bpm_x);
%grid on;
%hold on;
%%loglog(freq, P_noise_normal, '-ok');
%title('BPM meas x');

%figure(15);
figure(16);
%figure(17);
loglog(freq_bpm_y, P_bpm_y);
grid on;
hold on;
%loglog(freq, P_noise_normal, '-ok');
xlabel('frequency [Hz]');
ylabel('PSD [m2/Hz]');
title('BPM meas y');

figure(26);
loglog(freq_bpm_y, IRMS_bpm_y);
grid on;
hold on;
legend('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13');
xlabel('frequency [Hz]');
ylabel('IRMS [m]');
title('BPM meas y');

%%figure(18);
%figure(20);
%loglog(freq_bpm_x, P_gm_effect_x);
%grid on;
%hold on;
%%loglog(freq, P_noise_normal, '-ok');
%title('BPM GM effect x');

%figure(191);
figure(214);
loglog(freq_bpm_y, P_gm_effect_y);
grid on;
hold on;
%loglog(freq, P_noise_normal, '-ok');
title('BPM GM effect y');

figure(29);
loglog(freq_bpm_y, IRMS_gm_effect_y);
grid on;
hold on;
legend('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13');
xlabel('frequency [Hz]');
ylabel('IRMS [m]');
title('BPM GM effect y');

%figure(15);
%plot(freq, P_noise_normal, '-b');
%grid on;
%hold on;
%%plot(freq, P_noise_uniform, '-g');
%plot(freq_bpm_y, P_bpm_y(:,30)./10, '-r');
%legend('white noise', 'BPM data');
%xlabel('frequency [Hz]');
%ylabel('PSD [m2/Hz]');
%title('Noise comparison');

figure(333);
loglog(freq_quad_y, P_diff_quad_y);
grid on;
hold on;
%legend('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13');
xlabel('frequency [Hz]');
ylabel('PSD [m2/Hz]');
title('Diff quad y');
