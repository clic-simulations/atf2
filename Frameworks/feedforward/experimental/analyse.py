
#!/usr/bin/python

#import libraries needed
import sys
import gm_sensor
import spectral



#files=(\
#"ATF2_2013-05-19_18h22m46s.179.tdms",\
#"ATF2_2013-05-19_18h46m47s.252.tdms",\
#"ATF2_2013-05-19_19h34m49s.187.tdms",\
#"ATF2_2013-05-19_19h10m48s.273.tdms",\
#"ATF2_2013-05-19_19h58m50s.040.tdms",\
#"ATF2_2013-05-19_20h22m51s.048.tdms",\
#"ATF2_2013-05-19_20h46m51s.947.tdms")
#for filename in files:
#    meas=GM_sensor(filename)
#    meas.plot_interpulse_dpos()

#meas=GM_sensor("./data/ATF2_2013-06-11_06h45m30s.237.tdms")
#meas.save_mat("./data/ATF2_2013-06-11_06h45m30s.237.mat")
#meas=gm_sensor.GM_sensor("./data/ATF2_2013-11-20_09h43m40s.322.tdms")
#meas=gm_sensor.GM_sensor("./data/ATF2_2014-02-27_23h34m50s.454.tdms")
#meas.save_mat("./data/ATF2_2014-02-27_23h34m50s.454.mat")
#meas=gm_sensor.GM_sensor("./data/ATF2_2014-02-28_00h44m03s.428.tdms")
#meas.save_mat("./data/ATF2_2014-02-28_00h44m03s.428.mat")
#meas = gm_sensor.GM_sensor("data/data_20140307_v2/ATF2_2014-03-07_08h12m04s.182.tdms");
#meas = gm_sensor.GM_sensor("data/data_2014June/ATF2_2014-05-21_13h23m23s.868.tdms");
#meas = gm_sensor.GM_sensor("data/data_2014May/ATF2_2014-05-22_01h28m53s.820.tdms");
#meas = gm_sensor.GM_sensor("data/data_2014May/ATF2_2014-05-22_01h46m02s.482.tdms");
#meas = gm_sensor.GM_sensor("data/data_2014May/ATF2_2014-05-22_03h30m23s.631.tdms");
#meas = gm_sensor.GM_sensor("data/data_2014May/ATF2_2014-05-22_03h50m24s.802.tdms");
#meas = gm_sensor.GM_sensor("data/data_2014May/ATF2_2014-05-22_06h00m05s.501.tdms");
#meas = gm_sensor.GM_sensor("data/data_2014May/ATF2_2014-05-22_07h26m31s.262.tdms");
#meas = gm_sensor.GM_sensor("data/data_2014May/ATF2_2014-05-22_07h46m32s.298.tdms");
#meas = gm_sensor.GM_sensor("data/data_2014May/ATF2_2014-05-26_15h22m28s.386.tdms");
#meas = gm_sensor.GM_sensor("data/data_2014May/ATF2_2014-05-26_16h01m04s.107.tdms");
#meas = gm_sensor.GM_sensor("data/data_2014May/ATF2_2014-05-28_12h15m38s.278.tdms");
#meas = gm_sensor.GM_sensor("data/data_2014May/ATF2_2014-05-28_13h01m23s.890.tdms");
#meas = gm_sensor.GM_sensor("data/data_2014May/ATF2_2014-05-28_13h33m25s.961.tdms");
#meas = gm_sensor.GM_sensor("data/data_2014May/ATF2_2014-05-28_15h48m39s.645.tdms");
name="data/data_2014May/ATF2_2014-05-28_12h15m38s.278"
meas = gm_sensor.GM_sensor(name + '.tdms');

#sys.exit()
#meas.plot_data()
#meas.plot_pos()
#meas.plot_dpos()
#meas.plot_synchro()
#meas.plot_interpulse_pos()
#meas.plot_interpulse_dpos()
#sys.exit()

spec=spectral.spectral(GM_sensor=meas)
spec.compute_psd('pos')
#meas.save_mat("data/data_2014May/ATF2_2014-05-28_12h15m38s.278.mat")
meas.save_mat(name + '.mat')
#spec.plot_psd('pos')
sys.exit()
#spec.compute_all()
#spec.load_external_data('ground_motion_x_20131120T094851.dat', 'ground_motion_y_20131120T094851.dat', 'ground_motion_t_20131120T094851.dat')
#spec.load_external_data('ground_motion_effect_x_20131120T094851.dat', 'ground_motion_effect_y_20131120T094851.dat', 'ground_motion_effect_t_20131120T094851.dat')
#spec.load_external_data('bpm_motion_x_20131120T094851.dat', 'bpm_motion_y_20131120T094851.dat', 'bpm_motion_t_20131120T094851.dat')
#spec.load_external_data('bpm_motion_sub_x_20131120T094851.dat', 'bpm_motion_sub_y_20131120T094851.dat', 'bpm_motion_sub_t_20131120T094851.dat')
#spec.load_external_data('ground_motion_x_sim.dat', 'ground_motion_y_sim.dat', 'ground_motion_t_sim.dat')
#spec.load_external_data('../simulation/data/ground_motion_effect_x_sim.dat', '../simulation/data/ground_motion_effect_y_sim.dat', '../simulation/data/ground_motion_effect_t_sim.dat')
spec.load_external_data('../simulation/data/sensor_gm_x_sim.dat', '../simulation/data/sensor_gm_y_sim.dat', '../simulation/data/sensor_gm_t_sim.dat')
#spec.load_external_data('bpm_motion_x_sim.dat', 'bpm_motion_y_sim.dat', 'bpm_motion_t_sim.dat')
#spec.load_external_data('bpm_motion_sub_x_sim.dat', 'bpm_motion_sub_y_sim.dat', 'bpm_motion_sub_t_sim.dat')
#spec.compute_psd('delta_pos')
#spec.compute_irms('delta_pos')
#spec.plot_psd('delta_pos')
#spec.plot_irms('delta_pos')
#spec.compute_psd('external')
#spec.compute_irms('external')
#spec.plot_psd('external')
#spec.plot_irms('external')
#sys.exit()
# options are: 'pos', 'delta_pos', 'pos_interpulse', 'delta_pos_interpulse', 'external'
spec.compute_psd('pos')
spec.compute_psd('delta_pos')
spec.compute_psd('pos_interpulse')
spec.compute_psd('delta_pos_interpulse')
spec.compute_psd('external')
spec.compute_irms('pos')
spec.compute_irms('delta_pos')
spec.compute_irms('pos_interpulse')
spec.compute_irms('delta_pos_interpulse')
spec.compute_irms('external')
spec.compute_corr('pos')
spec.compute_corr('delta_pos')
spec.compute_corr('pos_interpulse')
spec.compute_corr('delta_pos_interpulse')
spec.compute_corr('external')
#spec.plot_corr_specific('pos',row=0,column='all')
#spec.plot_corr_specific('pos',row='all',column=0)
#spec.plot_corr_specific('pos',row=1,column=0)
sys.exit()
#spec.compute_factor()
#spec.plot_psd('pos')
#spec.plot_psd('delta_pos')
#spec.plot_psd('pos_interpulse')
spec.plot_psd('delta_pos_interpulse')
#spec.plot_irms('pos')
#spec.plot_irms('delta_pos')
#spec.plot_irms('pos_interpulse')
spec.plot_irms('delta_pos_interpulse')
#spec.plot_psd_subplot()

#meas.save_mat("ATF2_2013-05-19_20h46m51s.947.mat")
#meas1.plot_pos()
#meas1.plot_dpos()
#meas1.plot_synchro()
#meas1.plot_hist_synchro()
#meas1.plot_interpulse_dpos()
#spec1=spectral(GM_sensor=meas1)
#spec1.compute_fft()
#spec1.plot_fft()
#spec1.plot_fft_subplot()
#spec1.compute_all()
#spec1.plot_all()
