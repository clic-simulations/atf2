# Module to load and plot and store the ground motion sensor data

#import libraries needed
import sys
#library nptdms to open tdms files
from nptdms import TdmsFile
# plt for matlab type plot function
import matplotlib.pyplot as plt
#numpy for arrays
from numpy import *
from scipy.io import savemat
from datetime import *

def average_yves(x,window_len=10):
    if x.ndim != 1:
        raise ValueError, "smooth only accepts 1 dimension arrays."
    if x.size < window_len:
        raise ValueError, "Input vector needs to be bigger than window size."
    if window_len<2:
        return x
    s=x[0:len(x)/window_len*window_len].reshape(len(x)/window_len,window_len).mean(axis=1)
    return s

#def date_matlab_to_python(matlab_datenum):
#    return datetime.fromordinal(int(matlab_datenum)) + timedelta(days=matlab_datenum%1) - timedelta(days = 366)

def date_python_to_matlab(python_datetime):
    python_datetime=datetime.combine(python_datetime.date(),python_datetime.time())
    matlab_day_number = (python_datetime + timedelta(days = 366)).toordinal()
    matlab_frac_second = (python_datetime-datetime(python_datetime.year,python_datetime.month,python_datetime.day,0,0,0)).seconds / (24.0 * 60.0 * 60.0)
    matlab_frac_milisecond = (python_datetime-datetime(python_datetime.year,python_datetime.month,python_datetime.day,0,0,0)).microseconds / (24.0 * 60.0 * 60.0 * 1e6)
    return matlab_day_number + matlab_frac_second + matlab_frac_milisecond

class GM_sensor:
    'class computing position from raw data files (*.tdms)'
#------------------------------------
# Load file and compute measured position
#------------------------------------
    
    def __init__(self,filename=''):
        print "GM_sensor constructor"
        if(len(filename)==0):
            print "filename required"
            return
        #load the TMDS File
        tdms_file = TdmsFile(filename)
        date_start=filename.find('_')+1
        date_stop=filename.find('.')-1
        self.date=filename[date_start:date_stop]
        #get the channel list for group 'Continuous Data' (the only group in the measurement file)
        channels=tdms_file.group_channels('Continuous Data')
        #get t0 (measurement starting time)
        self.T0=channels[0].property('wf_start_time')
        #get T (interval between 2 measurements)
        self.T=channels[0].property('wf_increment')
        #get data sizes
        self.nchannel=len(channels)
	#In case of Beam On/Off signal the nchannel value has to be changed (MP)
        #print 'MP test'
	if(self.nchannel == 30):
		print 'Beam On/Off signal discovered'
		self.nchannel= 28
	else:
		print 'Beam On/Off signal NOT discovered'
		self.nchannel=(self.nchannel/2)*2
	#print self.nchannel
	#last 2 channel notconnected		(MP) recently they were connected. 29th channel is for synchronization, 30th for beam ON/OFF
        #self.nchannel=(self.nchannel/2)*2	(MP)
        self.nsensor=self.nchannel/2
        self.npoint=len(channels[0].data)
        #allocate memory for time of aquisition [s]
        time=zeros((self.nchannel,self.npoint))
        #allocate memory for position [m]
        pos=zeros((self.nchannel,self.npoint))
        data_save=zeros((self.nchannel,self.npoint))
        for channel in range(self.nchannel):
            #get data in m/s
            data=channels[channel].data/(2*1200)
            data_save[channel,:]=channels[channel].data/(2*1200)
            #get time of measurements
            time[channel,:]=channels[channel].time_track()
            #integrate data to get position 
            pos[channel,:]=data.cumsum()*self.T
        #horizontal channels
        xchannel=range(1,self.nchannel,2)
        #vertical channels
        ychannel=range(0,self.nchannel,2)
        #horizontal position      
        self.posx=pos[xchannel,:]
        self.datax=data_save[xchannel,:]
        self.timex=time[xchannel,:]
        self.dposx=self.posx[:,1:]-self.posx[:,0:-1]
        #vertical position
        self.posy=pos[ychannel,:]
        self.datay=data_save[ychannel,:]
        self.timey=time[ychannel,:]
        self.dposy=self.posy[:,1:]-self.posy[:,0:-1]



	if(len(channels)==self.nchannel+2):		#MP change
            #synchronization signal
            print "synchronization signal found."
            self.signal_synchro=channels[self.nchannel].data
            self.time_synchro=channels[self.nchannel].time_track()
	    #self.time_transitions =  
	    #self.time_transitions.append(0)
	    
	    transition_counter = 0
	    signal_synchro_threshold=-0.9
	    for index in range(len(self.signal_synchro)):
		    if((self.signal_synchro[index-1]>signal_synchro_threshold)and(self.signal_synchro[index]<signal_synchro_threshold)):
			    transition_counter = transition_counter + 1
			    #self.time_transitions.append(self.time_synchro[index])


	    self.time_transitions = zeros((transition_counter,1)) 
	   
	    transition_counter = 0
	    for index in range(len(self.signal_synchro)):
		    if((self.signal_synchro[index-1]>signal_synchro_threshold)and(self.signal_synchro[index]<signal_synchro_threshold)):
			    self.time_transitions[transition_counter] = self.time_synchro[index]
			    transition_counter = transition_counter + 1

	    self.index_transitions=searchsorted(self.time_synchro,self.time_transitions[:,0])




#            self.signal_synchro=channels[self.nchannel].data
#            self.time_synchro=channels[self.nchannel].time_track()
#            signal_averaged=average_yves(self.signal_synchro)
#            time_averaged=average_yves(self.time_synchro)
#            time_transitions=time_averaged[abs(gradient(signal_averaged))>0.00125]
#            duplicate_index=where(diff(time_transitions)<0.03)[0]
#            time_transitions[duplicate_index]=vstack((time_transitions[duplicate_index],time_transitions[duplicate_index+1])).mean(axis=0)
#            time_transitions=delete(time_transitions,duplicate_index+1)
#            bad_index=[]
#            index_transitions_m00001=searchsorted(time_averaged,time_transitions-0.0001)
#            index_transitions_p00001=searchsorted(time_averaged,time_transitions+0.0001)
#            index_transitions_m005=searchsorted(time_averaged,time_transitions-0.05)
#            index_transitions_p005=searchsorted(time_averaged,time_transitions+0.05)
#            for i in range(len(time_transitions)):
#                ave_before=average(signal_averaged[index_transitions_m005[i]:index_transitions_m00001[i]])
#                ave_after=average(signal_averaged[index_transitions_p00001[i]+1:index_transitions_p005[i]])
#                if(abs(ave_after-ave_before)<0.002):
#                    bad_index.append(i)
#            time_transitions=delete(time_transitions,bad_index)
            ntransitions=len(self.time_transitions)
#            self.index_transitions=searchsorted(self.time_synchro,time_transitions)
#            self.time_transitions=self.time_synchro[self.index_transitions]
            
	    
            self.T_interpulse = mean(self.time_transitions[1:]-self.time_transitions[0:-1])

	    #self.T_interpulse = mean(self.time_transitions[1:]-self.time_transitions[0:-1])
            #plt.figure(1)
            #plt.plot(self.time_synchro,self.signal_synchro,'k', label='recorded signal');
            #plt.plot(time_averaged, signal_averaged, 'b', label='filtered signal');
            #plt.stem(self.time_transitions,[1]*len(self.time_transitions),'r', label='detected transitions')
            #plt.legend(loc='lower right');
            #plt.xlabel('time [s]');
            #plt.ylabel('beam arrival signal [V]');
            #plt.xlim((49,56));
            #plt.ylim((0.22,0.29));
            #plt.show()
            #plt.close(1);

            #plt.figure(2)
            #plt.plot((self.time_transitions[1:]-self.time_transitions[0:-1]),'-xb', label='measured');
            #plt.plot([0, len(self.time_transitions)], [1/3.12, 1/3.12], '-r', label='expected');
            #plt.legend(loc='lower right');
            #plt.xlabel('pulse number');
            #plt.ylabel('delta time [s]');
            ##plt.xlim((49,56));
            ##plt.ylim((0.22,0.29));
            #plt.show()
            #plt.close(2);

            #plt.figure(3)  
            #plt.plot((self.timey[1,1:]-self.timey[1,0:-1]),'-xb');
            #plt.xlabel('pulse number');
            #plt.ylabel('delta time [s]');
            #plt.ylim((0, 0.005));
            #plt.show()
            #plt.close(3);

            print "compute position change between all pulses."

            # Debug
            #self.index_transitions=range(int(round(self.npoint/328))-5);
            #self.index_transitions=multiply(self.index_transitions,328); 
            #ntransitions = len(self.index_transitions)
            # dEBUG
            self.interpulse_posx=zeros((self.nsensor,ntransitions))
            self.interpulse_posy=zeros((self.nsensor,ntransitions))

            for sensor in range(self.nsensor):
                ntransitions=len(self.index_transitions)
                # Yves averages here over the last time step. I am not a fan if this, since it will reduce the correlation. If the time steps are
                # really independent, than this results in at least a factor two reduction of correlation.
                #interpulse_posx_tmp=split(self.posx[sensor],self.index_transitions)
                #interpulse_posy_tmp=split(self.posy[sensor],self.index_transitions)
                #for i in range(ntransitions):
                #    self.interpulse_posx[sensor,i]=sum(interpulse_posx_tmp[i])/len(interpulse_posx_tmp[i])
                #    self.interpulse_posy[sensor,i]=sum(interpulse_posy_tmp[i])/len(interpulse_posy_tmp[i])
                self.interpulse_posx[sensor,:]=self.posx[sensor,self.index_transitions]
                self.interpulse_posy[sensor,:]=self.posy[sensor,self.index_transitions]
            self.interpulse_dposx=self.interpulse_posx[:,1:]-self.interpulse_posx[:,0:-1]
            self.interpulse_dposy=self.interpulse_posy[:,1:]-self.interpulse_posy[:,0:-1]
#        self.magnets=("MQF1X","MQF2X","MQF3X","QF4X","QD5X","QF11X","QD12X","QF13X","QD14X","QF15X","QD16X","QD18X","QF1FF","QD0FF")
        self.magnets=("MQF1X","MQF2X","MQF3X","QF4X","QD5X","QF11X","QD12X","QF13X","QD14X","QF15X","QD16X","QD18X","QF19X","QD0FF")

        self.transition_array = []
	if(len(channels)==self.nchannel+2):		#MP change
		self.signal_beam_on_off=channels[self.nchannel+1].data
		self.time_beam_on_off  =channels[self.nchannel+1].time_track()
		
		#Test for signal transition recognizing - two transitions are artificially created
		#======================================================================================#
		#for index in range(len(self.signal_beam_on_off)/4):
		#	self.signal_beam_on_off[index]=0
		#for index in range(len(self.signal_beam_on_off)/2,len(self.signal_beam_on_off)):
		#	self.signal_beam_on_off[index]=0
		#======================================================================================#	
	
		#Transition recognizing. First column is for time of the transition, second for type of the transition(1 =up, -1 =down)
		max_beam_on_off = max(self.signal_beam_on_off)
		for index in range(len(self.signal_beam_on_off)):
			if((self.signal_beam_on_off[index-1]<max_beam_on_off/2) and (self.signal_beam_on_off[index]>max_beam_on_off/2)):
				self.transition_array.append([self.time_beam_on_off[index],1]) # rising up edge
			if((self.signal_beam_on_off[index-1]> max_beam_on_off/2) and (self.signal_beam_on_off[index]<max_beam_on_off/2)):
				self.transition_array.append([self.time_beam_on_off[index],-1]) # falling down edge

    def plot_data(self):
        print "GM_sensor.plot_data()"
        #plot all horizontal position on same plot
        plt.figure(10,figsize=(19.5,9.5))
        plt.plot(transpose(self.timex),transpose(self.datax),'-')
        plt.xlabel("time [s]")
        plt.ylabel("data x [m]")
        plt.savefig("datax_"+self.date+".png")
        #plt.close(0)
        #plot all vertical position on same plot
        plt.figure(11,figsize=(19.5,9.5))
        plt.plot(transpose(self.timey),transpose(self.datay),'-')
        plt.xlabel("time [s]")
        plt.ylabel("datay [m]")
        plt.savefig("datay_"+self.date+".png")
        #plt.close(0)
        plt.show()
        plt.close(10)
        plt.close(11)

    def plot_pos(self):
        print "GM_sensor.plot_pos()"
        #plot all horizontal position on same plot
        plt.figure(1,figsize=(19.5,9.5))
        plt.plot(transpose(self.timex),transpose(self.posx),'-')
        plt.plot(self.time_transitions,transpose(self.interpulse_posx))
        plt.xlabel("time [s]")
        plt.ylabel("posx [m]")
        plt.savefig("posx_"+self.date+".png")
        #plt.close(0)
        #plot all vertical position on same plot
        plt.figure(2,figsize=(19.5,9.5))
        plt.plot(transpose(self.timey),transpose(self.posy),'-')
        plt.plot(self.time_transitions,transpose(self.interpulse_posy)) 
        plt.xlabel("time [s]")
        plt.ylabel("posy [m]")
        plt.savefig("posy_"+self.date+".png")
        plt.show()
        plt.close(1)
        plt.close(2)
    
    def plot_dpos(self):
        print "GM_sensor.plot_dpos()"
        #plot all horizontal position variation on same plot
        #dposx=self.posx[:,1:]-self.posx[:,0:-1]
        plt.figure(3,figsize=(19.5,9.5))
        plt.plot(transpose(self.timex[:,1:]),transpose(self.dposx),'-')
        plt.xlabel("time [s]")
        plt.ylabel("posx [m]")
        plt.savefig("dposx_"+self.date+".png")
        #plt.close(0)
        #plot all vertical position variation on same plot
        #dposy=self.posy[:,1:]-self.posy[:,0:-1]
        plt.figure(4,figsize=(19.5,9.5))
        plt.plot(transpose(self.timey[:,1:]),transpose(self.dposy),'-')
        plt.xlabel("time [s]")
        plt.ylabel("posy [m]")
        plt.savefig("dposy_"+self.date+".png")
        #plt.close(0)

        plt.figure(7,figsize=(19.5,9.5))
        plt.plot(self.time_transitions[1:],transpose(self.interpulse_dposx),'b-')
        plt.xlabel("time [s]")
        plt.ylabel("dposx interpulse [m]")
        plt.savefig("interpulse_dposx_"+self.date+".png")

        plt.figure(8,figsize=(19.5,9.5))
        plt.plot(self.time_transitions[1:],transpose(self.interpulse_dposy),'b-')
        plt.xlabel("time [s]")
        plt.ylabel("dposy interpulse [m]")
        plt.savefig("interpulse_dposy_"+self.date+".png")
    
        plt.show()
        plt.close(3)
        plt.close(4)
        plt.close(7)
        plt.close(8)
        
    def plot_synchro(self):
        print "GM_sensor.plot_synchro()"
        if(not hasattr(self, 'signal_synchro')):
            raise ValueError, "no synchronisation signal found."
        plt.figure(5,figsize=(19.5,9.5))
        plt.plot(self.time_synchro,self.signal_synchro,'b-')
        plt.plot(average_yves(self.time_synchro),average_yves(self.signal_synchro),'r-+')    
        plt.vlines(self.time_transitions,-0.2,0.3,'k')
        plt.plot(average_yves(self.time_synchro),abs(gradient(average_yves(self.signal_synchro))),'g-')
        plt.xlabel("time [s]")
        plt.ylabel("signal [V]")
        plt.savefig("synchro_"+self.date+".png")
        #plt.close(0)

    def plot_hist_synchro(self):
        print "GM_sensor.plot_hist_synchro()"
        if( not hasattr(self, 'time_transitions')):
            raise ValueError, "no synchronisation signal found."
        plt.figure(6,figsize=(19.5,9.5))
        plt.hist(diff(self.time_transitions),100)
        plt.xlabel("delta T between pulse [s]")
        plt.savefig("hist_synchro_"+self.date+".png")
        #plt.close(0)

    def plot_interpulse_pos(self):
        print "GM_sensor.plot_interpulse_pos()"
        plt.figure(7,figsize=(19.5,9.5))
        plt.plot(self.time_transitions,transpose(self.interpulse_posx),'b-')
        plt.plot(self.time_transitions,transpose(self.interpulse_posy),'r-')
        plt.ylim((-1e-3,1e-3))
        plt.xlabel("time [s]")
        plt.ylabel("pos [m]")
        plt.savefig("pos_interpulse_"+self.date+".png")
        #plt.close(0)
        #plt.show()
        
    def plot_interpulse_dpos(self):
        print "GM_sensor.plot_interpulse_pos()"
        plt.figure(7,figsize=(19.5,9.5))
        plt.plot(self.time_transitions[1:],transpose(self.interpulse_dposx),'b-')
        plt.plot(self.time_transitions[1:],transpose(self.interpulse_dposy),'r-')
        plt.ylim((-1e-3,1e-3))
        plt.xlabel("time [s]")
        plt.ylabel("dpos interpulse [m]")
        plt.savefig("pos_interpulse_"+self.date+".png")
        #plt.close(0)
        #plt.show()

    def plot_interpulse_dpos_individual(self):
        print "GM_sensor.plot_interpulse_dpos()"
        plt.figure(9,figsize=(19.5,9.5))
        for i in range(self.nsensor):
            plt.subplot(self.nsensor,2,2*i+1)
            plt.plot(self.time_transitions[1:],transpose(self.interpulse_dposx[i,:]),'b-')
            plt.ylabel(self.magnets[i])
            plt.ylim((-5e-6,5e-6))            
            plt.subplot(self.nsensor,2,2*i+2)
            plt.plot(self.time_transitions[1:],transpose(self.interpulse_dposy[i,:]),'r-')
            plt.ylim((-5e-6,5e-6))            
        plt.subplot(self.nsensor,2,2*i+1)
        plt.xlabel("time [s]")
        plt.subplot(self.nsensor,2,2*i+2)
        plt.xlabel("time [s]")
        plt.savefig("dpos_interpulse_"+self.date+".png")
        #plt.close(0)
        #plt.show()
 
    def save_mat(self,filename):
        print "GM_sensor.save_mat()"
        data = {}
        data['posx'] = self.posx
        data['posy'] = self.posy
        data['timex'] = self.timex
        data['timey'] = self.timey
        data['t0']=date_python_to_matlab(self.T0)
        data['T']=self.T
        data['nchannel']=self.nchannel
        data['nsensor']=self.nsensor
        data['npoint']=self.npoint
        data['interpulse_dposx']=self.interpulse_dposx
        data['interpulse_dposy']=self.interpulse_dposy
        data['interpulse_posx']=self.interpulse_posx
        data['interpulse_posy']=self.interpulse_posy
	self.time_transitions = transpose(self.time_transitions)
        data['time_transitions']=self.time_transitions
        data['signal_synchro']=self.signal_synchro
        data['time_synchro']=self.time_synchro
	data['signal_beam_on_off']=self.signal_beam_on_off
	data['time_beam_on_off']=self.time_beam_on_off
        data['transition_array']=self.transition_array
	
	data['index_transitions']=self.index_transitions
	savemat(filename,data)
