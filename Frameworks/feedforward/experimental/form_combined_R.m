% Function the to get from an response matrix with sliced elements, the response
% matrix where each column corresponds to one quadrupole (R_compact).
%
% Juergen Pfingstner
% 2nd of April 2014

function [R_combined, quad_combined_s] = form_combined_R(R, bpm_index, quad_index, quad_s)

global BEAMLINE;

% Combine the separate elements to physical ones
nbpm = length(bpm_index);
nquad = length(quad_index);
quad_name = ['         ';'         '];
for loop_name_collect=1:nquad
    name_temp = BEAMLINE{quad_index(loop_name_collect)}.Name;
    quad_name(loop_name_collect,1:length(name_temp))=name_temp;
end
quad_combined = determine_real_quad_pos(quad_index,quad_name,4);
nquad_combined = length(quad_combined);

% Combined the response matrix in the same way as the element indices above
R_combined = zeros(nbpm*2, nquad_combined*2);
quad_combined_s = zeros(nquad_combined,1);
for loop_index_combined=1:nquad_combined
    indices_local=quad_combined(loop_index_combined).quad_indices_array;
    %loop_index_combined
    %quad_combined(loop_index_combined).quad_indices
    if(length(indices_local)==1)
        R_combined(:,loop_index_combined) = R(:,indices_local);
        R_combined(:,loop_index_combined+nquad_combined) = R(:,indices_local+nquad);
    else  
        R_combined(:,loop_index_combined) = sum(R(:,indices_local),2);
        R_combined(:,loop_index_combined+nquad_combined) = sum(R(:,indices_local+nquad),2);
    end
    quad_combined_s(loop_index_combined) = mean(quad_s(indices_local));
end
