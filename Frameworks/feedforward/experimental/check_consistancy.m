% This function checks, if the ground motion and BPM data are consistant.
% This is a pre-requirement for further data analysis. 
%
% Juergen Pfingstner
% 31st of March 2014

function data_okay = check_consistancy(gm_ts_x, gm_ts_y, bpm_ts_x, bpm_ts_y)

delta_bpm = 1/3.1243;
epsilon_bpm = delta_bpm/5; 
data_okay = 1;

% Check for the number of data points

[row_gm_ts_x, col_gm_ts_x] = size(gm_ts_x);
[row_gm_ts_y, col_gm_ts_y] = size(gm_ts_y);
[row_bpm_ts_x, col_bpm_ts_x] = size(bpm_ts_x);
[row_bpm_ts_y, col_bpm_ts_y] = size(bpm_ts_y);

if(row_gm_ts_x ~= row_bpm_ts_x)
  fprintf(1, 'Different number of points in GM_x and BPM_x\n');
  data_okay = 0;
end

if(row_gm_ts_y ~= row_bpm_ts_y)
  fprintf(1, 'Different number of points in GM_y and BPM_y\n');
  data_okay = 0;
end

if(row_bpm_ts_x ~= row_bpm_ts_y)
  fprintf(1, 'Different number of points in BPM_x and BPM_y\n');
  data_okay = 0;
end

% Check if time stamps are consistant with number of points

if((gm_ts_x(1) - gm_ts_y(1))>epsilon_bpm)
  fprintf(1, 'The first ground motion point in x is not the same as in y\n');
  data_okay = 0;
end

if((bpm_ts_x(1,20) - bpm_ts_y(1,20))>epsilon_bpm)
  fprintf(1, 'The first BPM point in x is not the same as in y\n');
  data_okay = 0;
end

if((gm_ts_x(1) - bpm_ts_x(1,20))>epsilon_bpm)
  fprintf(1, 'The first ground motion and BPM points are not the same as in y\n');
  data_okay = 0;
end
