function [check_okay] = check_selected_data_simple(gm_dm_x, gm_dm_y, bpm_dm_x, bpm_dm_y)

check_okay = 1;

% There should be no BPM data missing
if(length(bpm_dm_x) ~= sum(bpm_dm_x))

   % Ask user what he thinks about it. 
   fprintf(1, 'There were missing data points in the x BPM data!\n');
   answer=input('Should the analysis be performed with corrected data? [y/N]','s');
   if(answer == 'N')
      check_okay = 0;
      return;
   end
end

if(length(bpm_dm_y) ~= sum(bpm_dm_y))

   % Ask user what he thinks about it. 
   fprintf(1, 'There were missing data points in the y BPM data!\n');
   answer=input('Should the analysis be performed with corrected data? [y/N]','s');
   if(answer == 'N')
      check_okay = 0;
      return;
   end
end

% Missing data should be at the same time steps. 
compare_array = xor(bpm_dm_x, bpm_dm_y);
if(sum(compare_array)>0.5)
    fprintf(1, 'The missing BPM data points have not been at the same location in x and in y!\n');
    check_okay = 0;
    return;
end

% The missing points in the GM should be at the same location in x and y
compare_array = xor(gm_dm_x, gm_dm_y);
if(sum(compare_array)>0.5)
    fprintf(1, 'The missing GM data points have not been at the same location in x and in y!\n');
    check_okay = 0;
    return;
end

% There should be as many gm as bpm points finally
if(length(gm_dm_x) ~= length(bpm_dm_x))
  fprintf(1, 'The number of detected points is different for GM and BPM data!\n');
  check_okay = 0;
  return; 
end

