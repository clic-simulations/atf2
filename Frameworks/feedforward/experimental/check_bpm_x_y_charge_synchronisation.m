% Function to check if the BPM data are inconsistant with respect to the time 
% stamps in x, y, and the charge readings. This could be to problems in the data
% acquisition. 
%
% Juergen Pfingstner
% 8th of May 2014

function ts_okay = check_bpm_x_y_charge_synchronisation(ts_x, ts_y, ts_charge, treshold, ref_bpm)

ts_okay = 1;

[nr_ts_x, nr_bpm_x] = size(ts_x);
[nr_ts_y, nr_bpm_y] = size(ts_y);
[nr_ts_charge, nr_bpm_charge] = size(ts_charge);

% Check if there are the same number of BPMs (if no that would be really surprising)
if(nr_bpm_x ~= nr_bpm_y)
  ts_okay = 0;
end

if(nr_bpm_x ~= nr_bpm_charge)
  ts_okay = 0;
end

if(ts_okay == 0)
  fprintf(1, 'The number of BPMs is not the same in the data sets!\n');
  return;
end

% Check if the number of points is the same for all measurements 
if(nr_ts_x ~= nr_ts_y)
  ts_okay = 0;
end

if(nr_ts_x ~= nr_ts_charge)
  ts_okay = 0;
end

if(ts_okay == 0)
  fprintf(1, 'The number of time steps is not the same for all data sets!\n');
  return;
end

% Check if the data are overlapping well. Take ts_x as a reference

offset_y = mean(ts_x(:,ref_bpm)-ts_y(:,ref_bpm));
offset_charge = mean(ts_x(:,ref_bpm)-ts_charge(:,ref_bpm));

if(abs(offset_y) > treshold )
  ts_okay = 0;
end

if(abs(offset_charge) > treshold )
  ts_okay = 0;
end

if(ts_okay == 0)
  fprintf(1, 'The time stamps have an offset!\n');
  return;
end