% File to de-correlate the downstream BPMs from the upstream ones
%
% Juergen Pfingstner
% 2nd of April 2014

function [bpm_data_x, bpm_data_y, gm_mod, index_down_bpms] = subtract_jitter(bpm_data_x, bpm_data_y, bpm_name)

   index_up_bpms = [];
    %index_up_bpms = [index_up_bpms: find(strcmp(bpm_name,'MQF1X'))];
    %index_up_bpms = [index_up_bpms; find(strcmp(bpm_name,'MQF3X'))];
    %index_up_bpms = [index_up_bpms; find(strcmp(bpm_name,'MQD5X'))];
    index_up_bpms = [index_up_bpms; find(strcmp(bpm_name,'MQD10X'))];
    index_up_bpms = [index_up_bpms; find(strcmp(bpm_name,'MQF11X'))];
    index_up_bpms = [index_up_bpms; find(strcmp(bpm_name,'MQD12X'))];
    %index_up_bpms = [index_up_bpms; find(strcmp(bpm_name,'MQD16X'))];
    %index_up_bpms = [index_up_bpms; find(strcmp(bpm_name,'MQF17X'))];
    %index_up_bpms = [index_up_bpms; find(strcmp(bpm_name,'MQF18X'))];
    %index_up_bpms = [index_up_bpms; find(strcmp(bpm_name,'MQF19X'))];
    %index_up_bpms = [index_up_bpms; find(strcmp(bpm_name,'MQF20X'))];
    %index_up_bpms = [index_up_bpms; find(strcmp(bpm_name,'MQF21X'))];
    %index_up_bpms = [index_up_bpms; find(strcmp(bpm_name,'MQM16FF'))];
    %index_up_bpms = [index_up_bpms; find(strcmp(bpm_name,'MQM15FF'))];
    index_up_bpms = [index_up_bpms; index_up_bpms+length(bpm_name)];

    nbpm = length(bpm_name);
    bpm_data = [bpm_data_x, bpm_data_y]';
    index_down_bpms = 1:2*nbpm;
    index_down_bpms(index_up_bpms) = [];
    bpm_data_up = bpm_data(index_up_bpms,:);
    bpm_data_down = bpm_data(index_down_bpms,:);
    %bpm_data_corr = bpm_data_down*(inv(bpm_data_up'*bpm_data_up)*bpm_data_up');
    %sv_tolerance = 1e-6;
    %bpm_corr = bpm_data_down*pinv(bpm_data_up, sv_tolerance);
    bpm_corr = bpm_data_down*pinv(bpm_data_up);
    %[bpm_corr, sigma, residual_temp] = ols(bpm_data_down', bpm_data_up');
    %bpm_corr = bpm_dat_corr';
    bpm_data(index_down_bpms,:) = bpm_data_down - bpm_corr*bpm_data_up;
    bpm_data_x = bpm_data(1:end/2,:)';
    bpm_data_y = bpm_data(end/2+1:end,:)';

    % Prepare a matrix for the ground motion effect modification
    gm_mod = pinv(bpm_data_up)*bpm_data_up;
    gm_mod = eye(size(gm_mod)) - gm_mod;
