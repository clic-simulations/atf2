% Script to preprocess and correlate the ground motion data
% with the BPM measurements. Here the new synchronisation concept is used. 
% It uses charge information for the synchronisation. 
%
% Juergen Pfingstner
% 8th of May 2014

%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%
%% A. Parameter %%
%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%

% The data set with the first good results 
%file_name_bpm = 'data/data_2014May/data_20140522_0150.mat';
%file_name_gm = 'data/data_2014May/ATF2_2014-05-22_01h46m02s.482.mat';
%file_name_otr_data = './data/data_20140307_v2/emit2dOTRp_20140307T025450.mat';

% Data set with sensors on the QPs Q1X and Q2X WITH tube touching
file_name_bpm = 'data/data_2014May/data_20140528_1214.mat';
file_name_gm = 'data/data_2014May/ATF2_2014-05-28_12h15m38s.278.mat';

% Data set with sensors on the QPs Q1X and Q2X WITHOUT tube touching
%file_name_bpm = 'data/data_2014May/data_20140528_1333.mat';
%file_name_gm = 'data/data_2014May/ATF2_2014-05-28_13h33m25s.961.mat';

% With also water in pipe turned off
%file_name_bpm = 'data/data_2014May/data_20140528_1549.mat';
%file_name_gm = 'data/data_2014May/ATF2_2014-05-28_15h48m39s.645.mat';
%file_name_bpm = 'data/data_2014May/data_20140528_1605.mat';
%file_name_gm = 'data/data_2014May/ATF2_2014-05-28_16h04m40s.730.mat';

file_name_otr_data = './data/data_2014May/emit2dOTRp_20140527T185711.mat';


use_compare_models = 0;
use_frequency_analysis = 1;
correct_2014March_shift1_bug = 1;

use_R_calc = 1;
use_sbend_in_R = 1;

load_data = 1;
use_font_bpm = 0;
shift_steps = 0;
modulo_steps = 1;
charge_treshold = 20;

use_jitter_subtraction = 0;
use_jitter_sub_on_gm = 0;
use_filter_gm_bpm_data = 0;

use_spacial_lowpass = 1;
nr_sensor_avg = 12;
%nr_sensor_avg = 1;

interp_method = 'linear';
interpolate_to_zero = 0;

% Initialisation

addpath('/Users/jpfingst/Work/clicsim/trunk/ATF2/Frameworks/jitter_localisation/experimental/');
addpath('/Users/jpfingst/Work/clicsim/trunk/ATF2/Frameworks/feedforward/simulation/');

% Set some plot parameters for publishable plots 

%set(0,'defaultaxesfontsize',20);
%set(0,'defaultaxesfontname','Computer Modern Concrete');
%set(0,'DefaultTextInterpreter','latex')
%set(0,'defaultlinelinewidth',2);

if(load_data == 1)
    gm_data=load(file_name_gm);
    bpm_data = load(file_name_bpm);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% B. Setup the model and calculate the response matrix %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Restore initial model

if(exist('BEAMLINE_init','var')==0)
    fprintf(1, 'In definition of BEAMLINE_init');
    BEAMLINE_init = BEAMLINE;
    PS_init = PS;
    FL_init = FL;
end
BEAMLINE = BEAMLINE_init;
PS = PS_init;
FL = FL_init;
get_model_data;

% Calculate initial response matrix with the default and the saved settings

if(use_R_calc)
    beta_x_FL = FL.SimModel.Twiss.betax(bpm);
    beta_y_FL = FL.SimModel.Twiss.betay(bpm);
    %sigma_x_FL = FL.SimModel.Twiss.sigmax(bpm);
    %sigma_y_FL = FL.SimModel.Twiss.sigmay(bpm);
    gamma = 1300/0.511;
    epsilon_y = 0.3e-7./gamma;
    epsilon_x = 50e-7./gamma;
    sigma_x_FL = sqrt(epsilon_x.*beta_x_FL);
    sigma_y_FL = sqrt(epsilon_y.*beta_y_FL);

    twiss_init = get_twiss_otr('init');
    beta_x_init = twiss_init.betax(bpm);
    beta_y_init = twiss_init.betay(bpm);

    fprintf(1, 'Calculate the initial R\n');
    R_init = get_R(bpm, quad, Beam0_IEX, bpm_select_index);
    [R_init_combined, quad_combined_s] = form_combined_R(R_init, bpm, quad, quad_s);
    nr_quad_combined = length(quad_combined_s);

    [Beam1_IEX] = update_magnet_model(bpm_data.BEAMLINE, bpm_data.PS, Beam0_IEX);
    prepare_lattice_tracking();

    twiss_save = get_twiss_otr(file_name_otr_data);
    beta_x_save = twiss_save.betax(bpm);  
    beta_y_save = twiss_save.betay(bpm);
    sigma_x_save = sqrt(epsilon_x.*beta_x_save);
    sigma_y_save = sqrt(epsilon_y.*beta_y_save);

    fprintf(1, 'Calculate R from the saved model\n');
    R_save = get_R(bpm, quad, Beam1_IEX, bpm_select_index);
    [R_save_combined,~] = form_combined_R(R_save, bpm, quad, quad_s);
end

if(use_compare_models == 1)
    delta_model=norm(R_init-R_save, 'fro')/norm(R_init, 'fro');
    fprintf(1, 'Model change: %d\n', delta_model);
  
    figure(100);
    plot(beta_x_FL, '-ob');
    grid on;
    hold on;
    plot(beta_x_init, '-xb');
    plot(beta_x_save, '-xr');
    legend('FL', 'init prop', 'save prop');
    title('beta x');

    figure(101);
    plot(beta_y_FL, '-ob');
    grid on;
    hold on;
    plot(beta_y_init, '-xb');
    plot(beta_y_save, '-xr');
    legend('FL', 'init prop', 'save prop');
    title('beta y');
end

%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%
%% C. Data processing %%
%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%

% Collect the GM and BPM data and bring them to the same format

gm_data_x = gm_data.interpulse_posx';
gm_data_y = gm_data.interpulse_posy';

bpm_data_x = bpm_data.x_data;
bpm_data_y = bpm_data.y_data;
if(correct_2014March_shift1_bug == 1)
    bpm_data_y = bpm_data_y./1e6.*1.6;
end
bpm_data_charge = bpm_data.charge_data;
[nr_trains, nr_bpms] = size(bpm_data_x);
bpm_ts_x = bpm_data.time_stamp_x;
bpm_ts_y = bpm_data.time_stamp_y;
bpm_ts_charge = bpm_data.time_stamp_charge;

if(use_font_bpm == 1)
    font_data = bpm_data.font_data;
    font_ts = bpm_data.time_stamp_font;
    [font_data, font_ts] = straighten_font_data(font_data, font_ts, file_name_bpm, bpm_ts_y);
    y_data(:,bpm_data.font_bpm_index) = font_data;
end

[gm_ts, bpm_ts_x, bpm_ts_y, bpm_ts_charge] = convert_time_stamps(gm_data, bpm_ts_x, bpm_ts_y, bpm_ts_charge);
size(gm_ts)
gm_ts = gm_ts';

% Check if the time stamps of the BPM data x, y and charge are consistant. 
% Otherwise try a simple correction by shifting them on each other.

[bpm_data_x, bpm_data_y, bpm_data_charge, bpm_ts_x, bpm_ts_y, bpm_ts_charge, sync_flag] = synchronise_bpm_data(bpm_data_x, bpm_data_y, bpm_data_charge, bpm_ts_x, bpm_ts_y, bpm_ts_charge);
if(sync_flag == 0)
    return;
end

% Find the first and last BPM measurement where there was beam
% Comment: for the GM measurements, this is most likely already done (to be seen)

[index_start, index_end] = find_experiment_start_end(bpm_data_charge, charge_treshold, 20);

bpm_data_x = bpm_data_x(index_start:index_end,:);
bpm_ts_x = bpm_ts_x(index_start:index_end,:);
bpm_data_y = bpm_data_y(index_start:index_end,:);
bpm_ts_y = bpm_ts_y(index_start:index_end,:);
bpm_data_charge = bpm_data_charge(index_start:index_end,:);
bpm_ts_charge = bpm_ts_charge(index_start:index_end,:);

% Correct the data for missing time stamps
[gm_data_x, gm_ts_x, gm_dm_x] = interpolate_missing_time_stamps_charge(gm_data_x, gm_ts, 1, 'normal');
[gm_data_y, gm_ts_y, gm_dm_y] = interpolate_missing_time_stamps_charge(gm_data_y, gm_ts, 1, 'normal');
[bpm_data_x, bpm_ts_x, bpm_dm_x] = interpolate_missing_time_stamps_charge(bpm_data_x, bpm_ts_x, 20, 'normal');
[bpm_data_y, bpm_ts_y, bpm_dm_y] = interpolate_missing_time_stamps_charge(bpm_data_y, bpm_ts_y, 20, 'normal');
[bpm_data_charge, bpm_ts_charge, bpm_dm_charge] = interpolate_missing_time_stamps_charge(bpm_data_charge, bpm_ts_charge, 20, 'charge');

% Perform the second round of checks
check_ok = check_selected_data_simple(gm_dm_x, gm_dm_y, bpm_dm_x, bpm_dm_y);
if(check_ok == 0)
    fprintf(1,'There have been inconsistencies in the gm and bpm data. Sorry!\n');
    return;
end

% Erase BPM data with too low charge
bpm_dm_low_charge = bpm_data_charge(:,20) > charge_treshold; 
bpm_dm_x = bpm_dm_x & bpm_dm_low_charge;
bpm_dm_y = bpm_dm_y & bpm_dm_low_charge;
bpm_dm_charge = bpm_dm_charge & bpm_dm_low_charge;

% Perform the third round of checks
check_ok = check_selected_data_advanced(gm_dm_x, bpm_dm_low_charge);
if(check_ok == 0)
    fprintf(1,'There have been inconsistencies in the gm and bpm data. Sorry!\n');
    return;
else
    fprintf(1, 'The data are consistent. Lucky you!\n');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% At this point the data are okay %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% We only need one data marker anymore since data overlap perfectly
data_marker = bpm_dm_x & bpm_dm_y & gm_dm_x & gm_dm_y;

% Remove temperature drifts via high pass filtering 
[b,a]=butter(2, 0.2/1.5,'high');
if(use_filter_gm_bpm_data==1)
    [b,a]=butter(2, 1.0/1.5,'high');
end
[nr_sensors,time_steps_temp]=size(gm_data.interpulse_posx);
for i=1:nr_sensors
    gm_data_x(:,i) = filtfilt(b,a,gm_data_x(:,i));
    gm_data_y(:,i) = filtfilt(b,a,gm_data_y(:,i));
end

% As a test filter also the BPM data
if(use_filter_gm_bpm_data == 1)
    [b,a]=butter(2, 1.0/1.5,'high');
    for i=1:nbpm
        bpm_data_x(:,i) = filtfilt(b,a,bpm_data_x(:,i));
        bpm_data_y(:,i) = filtfilt(b,a,bpm_data_y(:,i));
    end
end

% From this time on, time stamps are not important anymore since data are in order
[gm_data_x, bpm_data_x] = shift_modulo_data(gm_data_x, bpm_data_x, shift_steps, modulo_steps);
[gm_data_y, bpm_data_y] = shift_modulo_data(gm_data_y, bpm_data_y, shift_steps, modulo_steps);
[data_marker_gm, data_marker_bpm] = shift_modulo_data(data_marker,data_marker, shift_steps, modulo_steps);
% The data have to correct on either side.  
data_marker = data_marker_gm & data_marker_bpm;

% Form the differential data (also for the low intensity marker)

gm_data_x = gm_data_x(2:end,:) - gm_data_x(1:end-1,:);
gm_data_y = gm_data_y(2:end,:) - gm_data_y(1:end-1,:);
bpm_data_x = bpm_data_x(2:end,:) - bpm_data_x(1:end-1,:);
bpm_data_y = bpm_data_y(2:end,:) - bpm_data_y(1:end-1,:);
data_marker = data_marker(2:end) & data_marker(1:end-1);

% Remove incorrect data

gm_data_x = gm_data_x(data_marker, :);
bpm_data_x = bpm_data_x(data_marker, :);
gm_data_y = gm_data_y(data_marker, :);
bpm_data_y = bpm_data_y(data_marker, :);
nr_pulses = sum(data_marker);

% At this point we have not only a consistant but also clean data set.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% D. Data interpolation, extrapolation and jitter subtraction %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Interpolate the ground motion data to the element positions

placet_sensA_s = load('placet_sensor16_s.dat');
placet_quads_s = load('placet_quads_s.dat');

[gm_data_x, gm_data_y, placet_sensA_s_x, placet_sensA_s_y] = correct_gm_sensor_data(gm_data_x, gm_data_y, placet_sensA_s, file_name_gm);

% There are no information on the position of the sensors in
% LUCRETIA. So I project the s scale of placet to lucretia to be
% able to use the same sensor position. The PLACET and LUCRETIA
% model fit longitudional very well (so no problems).
offset_placet_lucretia=placet_quads_s(5)-quad_s(1);
quad_s = quad_s+offset_placet_lucretia;
bpm_s = bpm_s+offset_placet_lucretia;
quad_combined_s = quad_combined_s+offset_placet_lucretia;

if(interpolate_to_zero == 1)
    dist_inter = 0;
else
    dist_inter = -1e12;
end

qp_pos_x = interp1([dist_inter; placet_sensA_s_x], [zeros(nr_pulses,1), gm_data_x]', quad_s, interp_method,'extrap')';
qp_pos_y =  interp1([dist_inter; placet_sensA_s_y], [zeros(nr_pulses,1), gm_data_y]', quad_s, interp_method,'extrap')';

qp_pos_combined_x = interp1([dist_inter; placet_sensA_s_x], [zeros(nr_pulses,1), gm_data_x]', quad_combined_s, interp_method,'extrap')';
qp_pos_combined_y = interp1([dist_inter; placet_sensA_s_y], [zeros(nr_pulses,1), gm_data_y]', quad_combined_s, interp_method,'extrap')';

bpm_pos_x = interp1([dist_inter; placet_sensA_s_x], [zeros(nr_pulses,1), gm_data_x]', bpm_s, interp_method,'extrap')';
bpm_pos_y = interp1([dist_inter; placet_sensA_s_y], [zeros(nr_pulses,1), gm_data_y]', bpm_s, interp_method,'extrap')';

if(use_spacial_lowpass == 1)
    if(nr_sensor_avg==1)
        qp_ref_offset_x = gm_data_x(:,nr_sensor_avg)';
        qp_ref_offset_y = gm_data_y(:,nr_sensor_avg)';
    else
        qp_ref_offset_x = mean(gm_data_x(:,1:nr_sensor_avg)');
        qp_ref_offset_y = mean(gm_data_y(:,1:nr_sensor_avg)');
    end

    for loop_index=1:nr_pulses
        qp_pos_x(loop_index,:) = qp_pos_x(loop_index,:) - qp_ref_offset_x(loop_index);
        qp_pos_y(loop_index,:) = qp_pos_y(loop_index,:) - qp_ref_offset_y(loop_index);

        qp_pos_combined_x(loop_index,:) = qp_pos_combined_x(loop_index,:) - qp_ref_offset_x(loop_index);
        qp_pos_combined_y(loop_index,:) = qp_pos_combined_y(loop_index,:) - qp_ref_offset_y(loop_index);

        bpm_pos_x(loop_index,:) = bpm_pos_x(loop_index,:) - qp_ref_offset_x(loop_index);
        bpm_pos_y(loop_index,:) = bpm_pos_y(loop_index,:) - qp_ref_offset_y(loop_index);
    end
end

% Use the model fit for testing
%bpm_fit_index = 30;
%r_init = R_init_combined(bpm_fit_index, 1:nr_quad_combined);
%r_save =R_save_combined(nbpm+bpm_fit_index,nr_quad_combined+1:nr_quad_combined);
%bpm_data_fit = bpm_data_y(:,bpm_fit_index) + bpm_pos_y(:,bpm_fit_index);
%r_fit = pinv(qp_pos_combined_y(:,1:40))*bpm_data_fit;
%R_save_combined(nbpm+bpm_fit_index,nr_quad_combined+1:nr_quad_combined+40) = r_fit';

R_ideal_x = pinv(gm_data_x) * (bpm_data_x + bpm_pos_x);
gm_effect_ideal_x = gm_data_x * R_ideal_x - bpm_pos_x;
R_ideal_y = pinv(gm_data_y) * (bpm_data_y + bpm_pos_y);
gm_effect_ideal_y = gm_data_y * R_ideal_y - bpm_pos_y;

% Calculate expected beam motion from quadrupole motion

gm_effect_init = R_init*[qp_pos_x'; qp_pos_y'] - [bpm_pos_x'; bpm_pos_y'];
gm_effect_init_combined = R_init_combined*[qp_pos_combined_x'; qp_pos_combined_y'] - [bpm_pos_x'; bpm_pos_y'];
gm_effect_save = R_save*[qp_pos_x'; qp_pos_y'] - [bpm_pos_x'; bpm_pos_y'];
gm_effect_save_combined = R_save_combined*[qp_pos_combined_x'; qp_pos_combined_y'] - [bpm_pos_x'; bpm_pos_y'];

% Debug
save('../simulation/qp_pos_x.data', 'qp_pos_combined_x', '-ascii');
save('../simulation/qp_pos_y.data', 'qp_pos_combined_y', '-ascii');
save('../simulation/gm_data_x.data', 'gm_data_x', '-ascii');
save('../simulation/gm_data_y.data', 'gm_data_y', '-ascii');
% dEBUG

gm_effect_save_combined_before_jitter_sub_x = gm_effect_save(1:end/2,:)';
gm_effect_save_combined_before_jitter_sub_y = gm_effect_save(end/2+1:end,:)';

% Apply effect of jitter subtraction also to gm effect data

if((use_jitter_sub_on_gm == 1) && (use_jitter_subtraction == 1))
    gm_effect_init(index_down_bpms,:) = gm_effect_init(index_down_bpms,:)*jitter_mod;
    gm_effect_init_combined(index_down_bpms,:) = gm_effect_init_combined(index_down_bpms,:)*jitter_mod;
    gm_effect_save(index_down_bpms,:) = gm_effect_save(index_down_bpms,:)*jitter_mod;
    gm_effect_save_combined(index_down_bpms,:) = gm_effect_save_combined(index_down_bpms,:)*jitter_mod;
end

% Apply jitter subtraction (if requested) to BPM data

bpm_data_jitter_x = bpm_data_x;
bpm_data_jitter_y = bpm_data_y;

if(use_jitter_subtraction == 1)
    [bpm_data_x, bpm_data_y, jitter_mod, index_down_bpms] = subtract_jitter(bpm_data_x, bpm_data_y, bpm_name);
end

%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%
%% E. Data evaluation %%
%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%

% Split ground motion data in x and y
gm_effect_init_x = gm_effect_init(1:end/2,:)';
gm_effect_init_y = gm_effect_init(end/2+1:end,:)';
gm_effect_init_combined_x = gm_effect_init_combined(1:end/2,:)';
gm_effect_init_combined_y = gm_effect_init_combined(end/2+1:end,:)';
gm_effect_save_x = gm_effect_save(1:end/2,:)';
gm_effect_save_y = gm_effect_save(end/2+1:end,:)';
gm_effect_save_combined_x = gm_effect_save_combined(1:end/2,:)';
gm_effect_save_combined_y = gm_effect_save_combined(end/2+1:end,:)';

% Calculate the ground motion effect per quadrupole
gm_effect_init_per_quad_x = zeros(nr_quad_combined, 1);
gm_effect_init_per_quad_y = zeros(nr_quad_combined, 1);
gm_effect_save_per_quad_x = zeros(nr_quad_combined, 1);
gm_effect_save_per_quad_y = zeros(nr_quad_combined, 1);

for i = 1:nr_quad_combined
    gm_effect_init_per_quad_x(i) = std(std(R_init_combined(1:end/2,i)*qp_pos_combined_x(:,i)' - bpm_pos_x'));
    gm_effect_init_per_quad_y(i) = std(std(R_init_combined(end/2+1:end,i+nr_quad_combined)*qp_pos_combined_y(:,i)' - bpm_pos_y'));
    gm_effect_save_per_quad_x(i) = std(std(R_save_combined(1:end/2,i)*qp_pos_combined_x(:,i)' - bpm_pos_x'));
    gm_effect_save_per_quad_y(i) = std(std(R_save_combined(end/2+1:end,i+nr_quad_combined)*qp_pos_combined_y(:,i)' - bpm_pos_y'));
end

% f analysis 

if(use_frequency_analysis == 1)
    perform_freq_analysis;
end

% Correlate the data

px_init = std(gm_effect_init_combined_x-bpm_data_x)./std(gm_effect_init_combined_x+bpm_data_x);
py_init = std(gm_effect_init_combined_y-bpm_data_y)./std(gm_effect_init_combined_y+bpm_data_y);
px_save = std(gm_effect_save_combined_x-bpm_data_x)./std(gm_effect_save_combined_x+bpm_data_x);
py_save = std(gm_effect_save_combined_y-bpm_data_y)./std(gm_effect_save_combined_y+bpm_data_y);
px_ideal = std(gm_effect_ideal_x-bpm_data_x)./std(gm_effect_ideal_x+bpm_data_x);
py_ideal = std(gm_effect_ideal_y-bpm_data_y)./std(gm_effect_ideal_y+bpm_data_y);

rx_init = diag(corr(bpm_data_x, gm_effect_init_combined_x));
ry_init = diag(corr(bpm_data_y, gm_effect_init_combined_y));
rx_save = diag(corr(bpm_data_x, gm_effect_save_combined_x));
ry_save = diag(corr(bpm_data_y, gm_effect_save_combined_y));
rx_ideal = diag(corr(bpm_data_x, gm_effect_ideal_x));
ry_ideal = diag(corr(bpm_data_y, gm_effect_ideal_y));

% Plot data

figure(8);
plot(quad_s, std(qp_pos_x),'-xr');
grid on;
hold on;
plot(quad_combined_s, std(qp_pos_combined_x),'-xm');
plot(quad_s, std(qp_pos_y),'-xb');
plot(quad_combined_s, std(qp_pos_combined_y),'-xc');
legend('x', 'x comb.', 'y', 'y comb.');
title('Diff. quadrupole motion');
xlabel('s [m]');

figure(9);
plot(std(gm_data_x),'-xb');
grid on;
hold on;
plot(std(gm_data_y),'-xr');
legend('x', 'y');
title('Diff. sensor motion');
xlabel('sensor nr. [1]');

% std residual plot
figure(2);
subplot(2,1,1);
semilogy(std(bpm_data_jitter_x),'-xk');
grid on;
hold on;
semilogy(std(bpm_data_x),'-b');
semilogy(std(gm_effect_init_x),'-xr');
semilogy(std(gm_effect_save_x),'-xm');
semilogy(std(gm_effect_save_combined_before_jitter_sub_x),'-g');
semilogy(std(gm_effect_ideal_x),'-k');
legend('BPM read','BPM jitter sub','GM to BPM init','GM to BPM save','GM to BPM save removed', 'GM to BPM ideal');
ylabel('horizontal');
subplot(2,1,2);
semilogy(std(bpm_data_jitter_y),'-xk');
grid on;
hold on;
semilogy(std(bpm_data_y),'-b');
semilogy(std(gm_effect_init_y),'-xr');
semilogy(std(gm_effect_save_y),'-xm');
semilogy(std(gm_effect_save_combined_before_jitter_sub_y),'-g');
semilogy(std(gm_effect_ideal_y),'-k');
ylabel('vertical');
title('signal level');

% Relative jitter
figure(22);
plot(std(bpm_data_jitter_x)./sigma_x_FL./sqrt(2).*100, '--xb');
grid on;
hold on;
plot(std(bpm_data_jitter_x)./sigma_x_save./sqrt(2).*100, '--xc');
plot(std(bpm_data_jitter_y)./sigma_y_FL./sqrt(2).*100, '--xr');
plot(std(bpm_data_jitter_y)./sigma_y_save./sqrt(2).*100, '--xm');
legend('x, nom', 'x, save', 'y, nom', 'y, save');
axis([0 47 0 60]);
xlabel('BPM nr');
ylabel('jitter level [\%]');

figure(3);
plot(px_init,'-xb');
grid on;
hold on;
plot(px_save,'-xc');
plot(px_ideal,'-xk');
plot(py_init,'-xr');
plot(py_save,'-xm');
plot(py_ideal,'-xg');
axis([0 length(px_init) 0.5 1.3]);
%title('P');
legend('px init', 'px save', 'px ideal', 'py init', 'py save', 'py ideal');
%legend('px init', 'px save', 'py init', 'py save');
xlabel('BPM nr');
ylabel('p');

%% Plot for Daniel
%figure(31);
%plot(px_save,'-xb');
%grid on;
%hold on;
%plot(py_save,'-xr');
%axis([0 length(px_init) 0.8 1.2]);
%%title('P');
%%legend('px init', 'px save', 'px ideal', 'py init', 'py save', 'py ideal');
%legend('px', 'py');
%xlabel('BPM nr');
%ylabel('p');

% Plot for paper
figure(44);
plot(bpm_data_y(:,19).*1e6, gm_effect_save_y(:,19).*1e6, 'x');
grid on;
xlabel('BPM meas. [um]');
ylabel('BPM pred. [um]');

figure(45);
plot(bpm_data_x(:,19).*1e6, gm_effect_save_x(:,19).*1e6, 'x');
grid on;
xlabel('BPM meas. [um]');
ylabel('BPM pred. [um]');
axis([-25 25 -15 15]);
%

figure(4);
plot(rx_init,'-xb');
grid on;
hold on;
plot(rx_save,'-xc');
%plot(rx_ideal, '-xk');
plot(ry_init,'-xr');
plot(ry_save,'-xm');
%plot(ry_ideal, '-xg');
axis([0 length(rx_init) -1.0 1.0]);
%title('R');
%legend('rx init', 'rx save', 'rx ideal', 'ry init', 'ry save', 'ry ideal');
legend('rx init', 'rx save', 'ry init', 'ry save');
xlabel('BPM nr');
ylabel('r');

figure(5);
plot(gm_effect_init_per_quad_x, '-ob');
grid on;
hold on;
plot(gm_effect_save_per_quad_x, '-xc');
plot(gm_effect_init_per_quad_y, '-xr');
plot(gm_effect_save_per_quad_y, '-xm');
legend('rx init', 'rx save', 'ry init', 'ry save');
%axis([0 length(rx_init) -0.5 0.5]);
title('Ground motion effect per quadrupole');

[cross30, lags_long, ~]=crosscorr(gm_effect_save_combined_y(:,30), bpm_data_y(:,30),100);
[auto_gm_effect30, lags_short, ~]=autocorr(gm_effect_save_combined_y(:,30),100);
[auto_bpm30, lags_short, ~]=autocorr(bpm_data_y(:,30),100);
figure(6);
plot(lags_long, cross30, '-ob');
grid on;
hold on;
plot(lags_short(2:end), auto_gm_effect30(2:end), '-xr');
plot(lags_short(2:end), auto_bpm30(2:end), '-xm');
legend('cross', 'auto effect', 'auto BPM');
%axis([0 length(rx_init) -0.5 0.5]);
title('Correlation of BPM 30 data');

%figure(7);
%plot(r_init, '-xk');
%grid on;
%hold on;
%plot(r_save, '-xb');
%plot(r_fit, '-xr');

