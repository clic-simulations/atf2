function [C_x_strength,C_y_strength]=compute_steering_EXT(bpm,bpm_read,bpm_ref,xcor,ycor,gain_orbit_x,gain_orbit_y)
%compute the correctors' strength to apply steering
 
	nxcor=length(xcor);
	nycor=length(ycor);
	nbpm=length(bpm);
%get transfert matrix betweens cors and bpms
	xmatrix=zeros(nbpm,nxcor);
	ymatrix=zeros(nbpm,nycor);
    for i=1:nbpm
    	for j=1:nxcor
    		if(xcor(j)<bpm(i))
    			R=placet_get_transfer_matrix("ATF2",xcor(j),bpm(i));
    			xmatrix(i,j)=R(1,2);
    		end
    	end
    	for j=1:nycor
    		if(ycor(j)<bpm(i))
    			R=placet_get_transfer_matrix("ATF2",ycor(j),bpm(i));
    			ymatrix(i,j)=R(3,4);
    		end
    	end
    end
    corx_used=1:nxcor;
    cory_used=1:nycor;
%strength_x & y [urad*GeV]
    cx_strength_old=placet_element_get_attribute("ATF2",xcor,"strength_x")/1.3*1e-6;
    cy_strength_old=placet_element_get_attribute("ATF2",ycor,"strength_y")/1.3*1e-6;
    cor_strength_max_x=1.6128e-3*ones(1,nxcor); %[rad]
    cor_strength_max_y=[0.0033 1.4855e-3*ones(1,nycor-1)]; %[rad]
    ncorx_used_old=0;
    ncory_used_old=0;
%as long as there is a saturation
    while (ncorx_used_old~=length(corx_used) || ncory_used_old~=length(cory_used))
%compute correctors strengths
        if (~isempty(corx_used))
%            C_x_strength(corx_used)=(-lscov(xmatrix(:,corx_used),bpm_read(:,1)-bpm_ref(:,1),1./(bpm_resol(:,1)).^2)).'+cx_strength_old(corx_used);
            C_x_strength(corx_used)=-pinv([xmatrix(:,corx_used);eye(length(corx_used))])*gain_orbit_x*[(bpm_read(:,1)-bpm_ref(:,1));zeros(length(corx_used),1)]+cx_strength_old(corx_used);
        end;
        if(~isempty(cory_used))
%            C_y_strength(cory_used)=(-lscov(ymatrix(:,cory_used),bpm_read(:,2)-bpm_ref(:,2),1./(bpm_resol(:,2)).^2)).'+cy_strength_old(cory_used);
            C_y_strength(cory_used)=-pinv([ymatrix(:,cory_used);eye(length(cory_used))])*gain_orbit_y*[(bpm_read(:,2)-bpm_ref(:,2));zeros(length(cory_used),1)]+cy_strength_old(cory_used);
        end;
        
%11 bit (2048 values) allimentation for cor_strength_max max=> round
		C_x_strength=round(C_x_strength./(cor_strength_max_x/1024)).*(cor_strength_max_x/1024);
		C_y_strength=round(C_y_strength./(cor_strength_max_y/1024)).*(cor_strength_max_y/1024);
%look if there is saturation
		ncorx_used_old=length(corx_used);
		ncory_used_old=length(cory_used);
%set first corrector saturated to saturation, unselect it, compute effect on orbit
		[C_x_strength,corx_used,bpm_read(:,1)]=set_first_saturation(C_x_strength,corx_used,cor_strength_max_x,bpm_read(:,1),xmatrix,cx_strength_old);
		[C_y_strength,cory_used,bpm_read(:,2)]=set_first_saturation(C_y_strength,cory_used,cor_strength_max_y,bpm_read(:,2),ymatrix,cy_strength_old);
%if there is saturation, reset corr strength
		if(ncorx_used_old~=length(corx_used) || ncory_used_old~=length(cory_used))
			C_x_strength(corx_used)=cx_strength_old(corx_used);
			C_y_strength(cory_used)=cy_strength_old(cory_used);
        end
    end
end

function [C_x_strength,corx_used,bpm_read]=set_first_saturation(C_x_strength,corx_used,cor_strength_max_x,bpm_read,matrix_x,cx_strength_old)
%look for saturation
	saturated_x_pos=corx_used(C_x_strength(corx_used)>cor_strength_max_x(corx_used));
	saturated_x_neg=corx_used(C_x_strength(corx_used)<-cor_strength_max_x(corx_used));
%	saturated_x_nul=corx_used(C_x_strength(corx_used)==0);
	saturated_x_nul=[];
	
    if( ~isempty(saturated_x_pos) && ~isempty(saturated_x_neg) && ~isempty(saturated_x_nul))		
		if( (saturated_x_pos(1) <= saturated_x_neg(1)) && (saturated_x_pos(1) <= saturated_x_nul(1)) )
			C_x_strength(saturated_x_pos(1))=cor_strength_max_x(saturated_x_pos(1));
			corx_used(corx_used==saturated_x_pos(1))=[];
			bpm_read=bpm_read+(cor_strength_max_x(saturated_x_pos(1))-cx_strength_old(saturated_x_pos(1)))*matrix_x(:,saturated_x_pos(1));
		elseif ( (saturated_x_neg(1) <= saturated_x_pos(1)) && (saturated_x_neg(1) <= saturated_x_nul(1)) )
			C_x_strength(saturated_x_neg(1))=-cor_strength_max_x(saturated_x_neg(1));
			corx_used(saturated_x_neg(1))=[];
			bpm_read=bpm_read+(-cor_strength_max_x(saturated_x_neg(1))-cx_strength_old(saturated_x_neg(1)))*matrix_x(:,saturated_x_neg(1));
		elseif ( (saturated_x_nul(1) <= saturated_x_pos(1)) && (saturated_x_nul(1) <= saturated_x_neg(1)) )
			C_x_strength(saturated_x_nul(1))=0;
			corx_used(corx_used==saturated_x_nul(1))=[];
		else disp "ERROR : x case with 3 saturations";
		end;
	elseif ( ~isempty(saturated_x_pos) &&~isempty(saturated_x_neg) )
		if(saturated_x_pos(1) <= saturated_x_neg(1))
			C_x_strength(saturated_x_pos(1))=cor_strength_max_x(saturated_x_pos(1));
			corx_used(corx_used==saturated_x_pos(1))=[];
			bpm_read=bpm_read+(cor_strength_max_x(saturated_x_pos(1))-cx_strength_old(saturated_x_pos(1)))*matrix_x(:,saturated_x_pos(1));
		elseif(saturated_x_neg(1) <= saturated_x_pos(1))
			C_x_strength(saturated_x_neg(1))=-cor_strength_max_x(saturated_x_neg(1));
			corx_used(corx_used==saturated_x_neg(1))=[];
			bpm_read=bpm_read+(-cor_strength_max_x(saturated_x_neg(1))-cx_strength_old(saturated_x_neg(1)))*matrix_x(:,saturated_x_neg(1));
	else disp "ERROR : x case with 2 saturations pos & neg";
		end;
	elseif ( ~isempty(saturated_x_pos) &&~isempty(saturated_x_nul) )
	if(saturated_x_pos(1) <= saturated_x_nul(1))
			C_x_strength(saturated_x_pos(1))=cor_strength_max_x(saturated_x_pos(1));
			corx_used(corx_used==saturated_x_pos(1))=[];
			bpm_read=bpm_read+(cor_strength_max_x(saturated_x_pos(1))-cx_strength_old(saturated_x_pos(1)))*matrix_x(:,saturated_x_pos(1));
		elseif(saturated_x_nul(1) <= saturated_x_pos(1))
			C_x_strength(saturated_x_nul(1))=0;
			corx_used(corx_used==saturated_x_nul(1))=[];
		else disp "ERROR : x case with 2 saturations pos & nul";
		end;
	elseif ( ~isempty(saturated_x_neg) &&~isempty(saturated_x_nul) )
		if(saturated_x_neg(1) <= saturated_x_nul(1))
			C_x_strength(saturated_x_neg(1))=-cor_strength_max_x(saturated_x_neg(1));
			corx_used(corx_used==saturated_x_neg(1))=[];
			bpm_read=bpm_read+(-cor_strength_max_x(saturated_x_neg(1))-cx_strength_old(saturated_x_neg(1)))*matrix_x(:,saturated_x_neg(1));
		elseif(saturated_x_nul(1) <= saturated_x_neg(1))
			C_x_strength(saturated_x_nul(1))=0;
			corx_used(corx_used==saturated_x_nul(1))=[];
		else disp "ERROR : x case with 2 saturations neg & nul";
		end;
	elseif (~isempty(saturated_x_pos))
		C_x_strength(saturated_x_pos(1))=cor_strength_max_x(saturated_x_pos(1));
		corx_used(corx_used==saturated_x_pos(1))=[];
		bpm_read=bpm_read+(cor_strength_max_x(saturated_x_pos(1))-cx_strength_old(saturated_x_pos(1)))*matrix_x(:,saturated_x_pos(1));
	elseif (~isempty(saturated_x_neg))
		C_x_strength(saturated_x_neg(1))=-cor_strength_max_x(saturated_x_neg(1));
		corx_used(corx_used==saturated_x_neg(1))=[];
		bpm_read=bpm_read+(-cor_strength_max_x(saturated_x_neg(1))-cx_strength_old(saturated_x_neg(1)))*matrix_x(:,saturated_x_neg(1));
	elseif (~isempty(saturated_x_nul))
		C_x_strength(saturated_x_nul(1))=0;
		corx_used(corx_used==saturated_x_nul(1))=[];
    end;
end
