#--------------------------------------------------#
#               parameters section                 #
#--------------------------------------------------#

# Marker to store different options on different directories
set dir_title "nothing"
# time0 est le temp inital de la simulation en secondes
set time0 0
# Tstep est la duree d'un step -> Interesting to see that Yves takes only every second step
set Tstep [expr 1/3.12]
#set Tstep [expr 1/100.0]
# nstep est nombre de pulse utilise pour l analyse
#set nstep 10
set nstep 100
#set nstep 1856
#seed1 est le premier seed (seed1<=seed<=seed1+nseed), seed1 must be >0
set seed1 1
# nseed est le nombre de fois ou la simulation est repetee avec une graine differente [default 10]
#set nseed 100
set nseed 5
#set nseed 1
# Multiplicative factor to scatter the numbers further seeds.
set seed_factor 324
# offset_ini is the rms in um of initial offset of quadrupoles, BPMs, bends and sexts [default 100]
set offset_ini 100
# scale_error is the scale error on BPMs reading [default 0.01]
set scale_error 0.01
#dk_over_k is the relative error on quadrupole strengths [default 1e-4]
set dk_over_k 1e-4
# use of the ground motion filter (geophone)
set gm_filter 1
set save_additional_data 1
# jitter amplitude: 0.5 corresponds to 25% of beam size in y and 10% in x
set jitter_amplitude_factor 0.0
# interpolation method between sensor measurements 
#   'nearest'
#          Return the nearest neighbour.
#    'linear'
#          Linear interpolation from nearest neighbours
#    'pchip'
#          Piece-wise cubic hermite interpolating polynomial
#    'cubic'
#          Cubic interpolation from four nearest neighbours (same as pchip)
#    'spline'
#          Cubic spline interpolation-smooth first and second derivatives
#          throughout the curve
#set interp_method linear
set interp_method linear
#bpm resolution [um]
set stripline 20.0
#set stripline 0.2
set stripline2 20.0
#set stripline2 0.2
set font_stripline_resolution 1.0
#set cband 0.1
set cband 0.3
set cband_noatt 0.1
set sband 1.0
# nominal 0.002 for ipbpm, observed <0.01
set ipbpm 0.01
# Jitter reduction options
# Option 1: Response matrix of injection parameters
# Option 2: SVD of measured data
# Option 3: Correlation matrix
# Else: no jitter removal
set jitter_removal_option 3
# Option 1: everything as usual 
# Option 2: change of the first three BPMs with the FONT electronics 
# Option 3: change of the first, third and fifth BPMs apart from M2X with the FONT electronics
# Option 4: change of all BPM resolutions that are able to be operated with the FONT electronics
#           the BPMs used have to be chosen by hand in jitter_subtraction.m
set bpm_change_option 3
set bpm_change_resolution 20.0
set use_sbend_in_R 1
set use_freq_analysis 0
#Lattice ("ATF2" for nominal, "UL" for ultra low optics, "ATF2_?strip2" for ATF2 with ? improved BPM only ATF2_10bx for the 10betax optics).
set beamline ATF2_10bx
#sextupole on (1) or off (0)
set sext 0
#switch off synchrotron radiation
set quad_synrad 0
set sbend_synrad 0
set mult_synrad 0
#no kick from MREF3FF dipole cavity
set mref3_dipole_strength 0
#skew quad tilt definition
set skew_tilt [expr 3.1415926535897931/180*45]
# Cband short range wake field
set cband_wake ""
set sband_wake ""
set use_dispersion_ff 0
set save_beam_ff_start 0
set wakeFieldSetup 0
set save_beam_ip 0
set twiss_output 1
#set harm_file_name "/Users/jpfingst/Work/clicsim/trunk/CLIC/Frameworks/clic-integrated-studies/data/GM/harm.K.300"
#set harm_file_name "./harm.K.dat"
#set harm_file_name "/Users/jpfingst/Work/clicsim/trunk/CLIC/Frameworks/clic-integrated-studies/data/GM/harm.ATF2.300"
# After a review a new model was created that fits the measured ones.
set harm_file_name "/Users/jpfingst/Work/clicsim/trunk/CLIC/Frameworks/clic-integrated-studies/data/GM/harm.ATF2_2014_seed1.300"

#--------------------------------------------------#
#            end of parameters section             #
#---------------------------------------------------

source ./setup_lattice_beam.tcl

Octave {

    # Import variables

    time0 = str2double(Tcl_GetVar("time0"));
    Tstep = str2double(Tcl_GetVar("Tstep"));
    nstep = str2double(Tcl_GetVar("nstep"));
    seed1 = str2double(Tcl_GetVar("seed1"));
    nseed = str2double(Tcl_GetVar("nseed"));
    offset_ini = str2double(Tcl_GetVar("offset_ini"));
    scale_error = str2double(Tcl_GetVar("scale_error")); 
    dk_over_k = str2double(Tcl_GetVar("dk_over_k"));
    gm_filter = str2double(Tcl_GetVar("gm_filter"));
    interp_method = Tcl_GetVar("interp_method");
    stripline = str2double(Tcl_GetVar("stripline"));
    stripline2 = str2double(Tcl_GetVar("stripline2"));
    font_stripline_resolution = str2double(Tcl_GetVar("font_stripline_resolution"));
    cband = str2double(Tcl_GetVar("cband"));
    cband_noatt = str2double(Tcl_GetVar("cband_noatt")); 
    sband = str2double(Tcl_GetVar("sband"));
    ipbpm = str2double(Tcl_GetVar("ipbpm"));
    beamline = Tcl_GetVar("beamline");
    sext = str2double(Tcl_GetVar("sext"));
    quad_synrad = str2double(Tcl_GetVar("quad_synrad"));
    sbend_synrad = str2double(Tcl_GetVar("sbend_synrad"));
    mult_synrad = str2double(Tcl_GetVar("mult_synrad"));
    mref3_dipole_strength = str2double(Tcl_GetVar("mref3_dipole_strength"));
    skew_tilt = str2double(Tcl_GetVar("skew_tilt"));
    cband_wake = str2double(Tcl_GetVar("cband_wake"));
    sband_wake = str2double(Tcl_GetVar("sband_wake"));
    use_dispersion_ff = str2double(Tcl_GetVar("use_dispersion_ff"));
    save_beam_ff_start = str2double(Tcl_GetVar("save_beam_ff_start"));
    wakeFieldSetup = str2double(Tcl_GetVar("wakeFieldSetup"));
    save_beam_ip = str2double(Tcl_GetVar("save_beam_ip"));
    plots = Tcl_GetVar("plots");
    data = Tcl_GetVar("data");
    bpm_change_option = str2double(Tcl_GetVar("bpm_change_option"));
    bpm_change_resolution = str2double(Tcl_GetVar("bpm_change_resolution"));
    seed_factor = str2double(Tcl_GetVar("seed_factor"));
    jitter_removal_option = str2double(Tcl_GetVar("jitter_removal_option"));
    use_sbend_in_R = str2double(Tcl_GetVar("use_sbend_in_R"));
    jitter_amplitude_factor = str2double(Tcl_GetVar("jitter_amplitude_factor"));
    save_additional_data = str2double(Tcl_GetVar("save_additional_data"));
    use_freq_analysis = str2double(Tcl_GetVar("use_freq_analysis"));

    # Set random generators

    Tcl_Eval("RandomReset -stream Misalignments -seed [expr $seed1*$seed_factor + 1]");
    Tcl_Eval("RandomReset -stream Instrumentation -seed [expr $seed1*$seed_factor + 2]");
    Tcl_Eval("RandomReset -stream Groundmotion -seed [expr $seed1*$seed_factor + 3]");
    Tcl_Eval("RandomReset -stream Cavity -seed [expr $seed1*$seed_factor + 4]");
    Tcl_Eval("RandomReset -stream User -seed [expr $seed1*$seed_factor + 5]");
    Tcl_Eval("RandomReset -stream Default -seed [expr $seed1*$seed_factor + 6]");
    Tcl_Eval("RandomReset -stream Survey -seed [expr $seed1*$seed_factor + 7]");
    Tcl_Eval("RandomReset -stream Select -seed [expr $seed1*$seed_factor + 8]");
    Tcl_Eval("RandomReset -stream Radiation -seed [expr $seed1*$seed_factor + 9]");
    # Both rand and randn have to be initialised, since they use a different value inside
    rand("state", (seed1+11)*seed_factor + [1:625]);
    randn("state", (seed1+12)*seed_factor + [1:625]);

    fprintf(1, "collecting lattice information\n");
    collect_lattice_info
  
    if(use_sbend_in_R == 1)
        quads = quads_sbends;
        nquad = length(quads_sbends);
        quads_s = quads_sbends_s;

        quads_combined = quads_sbends_combined; 
        nr_quads_combined = nr_quads_sbends_combined;
        quads_combined_s = quads_sbends_combined_s;
    end

    disp("calculate displacement's effects matrix")
    R_disp_quad=displ_effect_mat(bpms,quads,[e0 0 0 0 0 0]);
    #R_disp_quad=displ_effect_mat(bpms,quads,[e0 0 0 0 0 0]);

    R_model_combined = zeros(length(bpm_s)*2, nr_quads_combined);
    for loop_index_combined=1:nr_quads_combined
        indices_local=quads_combined(loop_index_combined).quad_indices_array;
        #placet_element_get_attribute("ATF2",quads_combined(loop_index_combined).quad_indices,"name")
        if(length(indices_local)==1)
            R_model_combined(:,loop_index_combined) = R_disp_quad(:,indices_local);
            R_model_combined(:,loop_index_combined+nr_quads_combined) = R_disp_quad(:,indices_local+nquad);
        else  
            R_model_combined(:,loop_index_combined) = sum(R_disp_quad(:,indices_local),2);
            R_model_combined(:,loop_index_combined+nr_quads_combined) = sum(R_disp_quad(:,indices_local+nquad),2);
        end
    end    

    #size(R_disp_quad)
    #size(R_model_combined)
    #(length(bpm_s)+1):(length(bpm_s)*2)
    #R_model_combined(1:length(bpm_s),1:nr_quads_combined/2)
    #R_model_combined((length(bpm_s)+1):(length(bpm_s)*2),(nr_quads_combined+1):2*nr_quads_combined)
    #R_model_combined(1:7,1:7)
    #pause(3600)

    disp("calculate 1st and 2nd order transfer matrix")
    TM=zeros(2*nbpm,6);
    for i=1:nbpm
        #disp(["from injection to bpm " bpm_name(i,:)]);
        R=placet_get_transfer_matrix("ATF2", 0, bpms(i));
        TM(i,:)=R(1,:);
        TM(nbpm+i,:)=R(3,:);        
    end

    placet_set_beam("part", [e0 0 0 0 0 0]);	
    randn('state',0);
    # To my opinion the jitter has been over estimated. Therefore I reduce it by a factor of two which 
    # corresponds to about 10% in x and about 25% in y. We should be able to achieve that 
    offset=randn(nstep+1,6) .* [ones(nstep+1,1)*1e-5/sqrt(2)*jitter_amplitude_factor ...
                                ones(nstep+1,1)*4e-6/sqrt(2)*jitter_amplitude_factor ...
                                ones(nstep+1,1)*2e-6/sqrt(2)*jitter_amplitude_factor ...
                                ones(nstep+1,1)*3e-6/sqrt(2)*jitter_amplitude_factor ...
                                ones(nstep+1,1)*0/sqrt(2)*jitter_amplitude_factor ...
                                ones(nstep+1,1)*5e-5/sqrt(2)*jitter_amplitude_factor];
    B_ini=placet_get_beam("part");
    B_usi_ini=[[B_ini(:,2) B_ini(:,5) B_ini(:,3) B_ini(:,6) B_ini(:,4)]*1e-6 (B_ini(:,1)-$e0)/$e0];

#########################################################################
# Debut du tracking avec correction du steering et de l'optique		#
#########################################################################

    pos_quad_meas_A=zeros(nseed,2*nquad,nstep+1);    
    pos_bpm_meas_A=zeros(nseed,2*nbpm,nstep+1);
    pos_quad_meas_B=zeros(nseed,2*nquad,nstep+1);    
    pos_bpm_meas_B=zeros(nseed,2*nbpm,nstep+1);
    pos_quad=zeros(nseed,2*nquad,nstep+1);    
    pos_bpm=zeros(nseed,2*nbpm,nstep+1);
    pos_quad_combined=zeros(nseed,2*nr_quads_combined,nstep+1);    
    pos_quad_combined_meas_A=zeros(nseed,2*nr_quads_combined,nstep+1);    
    pos_quad_combined_meas_B=zeros(nseed,2*nr_quads_combined,nstep+1);    
    pos_sensor_read=zeros(nseed,2*length(sensorA_list),nstep+1);

    delta_pos_quad_no_jitter=zeros(nseed,nquad*2,nstep);
    delta_bpm_read_no_jitter=zeros(nseed,nbpm*2,nstep);
    delta_pos_quad_meas_no_jitter_A=zeros(nseed,nquad*2,nstep);
    delta_pos_quad_meas_no_jitter_B=zeros(nseed,nquad*2,nstep);
    delta_bpm_read_meas_no_jitter=zeros(nseed,nbpm*2,nstep);
    delta_pos_quad_combined=zeros(nseed,2*nr_quads_combined,nstep);    
    delta_pos_quad_combined_meas_A=zeros(nseed,2*nr_quads_combined,nstep);    
    delta_pos_quad_combined_meas_B=zeros(nseed,2*nr_quads_combined,nstep);    
    delta_pos_sensor_read=zeros(nseed,2*length(sensorA_list),nstep);

    p=zeros(nseed,nbpm*2);
    p_A=zeros(nseed,nbpm*2);
    p_B=zeros(nseed,nbpm*2);
    p_noR=zeros(nseed,nbpm*2);
    p_A_noR=zeros(nseed,nbpm*2);
    p_B_noR=zeros(nseed,nbpm*2);
    r=zeros(nseed,nbpm*2);
    r_A=zeros(nseed,nbpm*2);
    r_B=zeros(nseed,nbpm*2);
    r_noR=zeros(nseed,nbpm*2);
    r_A_noR=zeros(nseed,nbpm*2);
    r_B_noR=zeros(nseed,nbpm*2);
    quad_ampl_A=zeros(nseed,nquad*2);
    quad_ampl_B=zeros(nseed,nquad*2);
    quad_ampl=zeros(nseed,nquad*2);
    effect_beamjitter=zeros(nseed,nbpm*2);
    effect_beamjittersub=zeros(nseed,nbpm*2);
    effect_GM=zeros(nseed,nbpm*2);
    effect_GM_noR=zeros(nseed,nbpm*2);
    effect_GM_jittersub=zeros(nseed,nbpm*2);
    
    #if (exist("GMcov_$beamline.mat"))
    #    load("GMcov_$beamline.mat");
    #else
    #    cov_delta_bpm_read_no_jitter=get_GM_cov(R_disp_quad,bpms,Tstep,gm_filter,interp_method);
    #    save("GMcov_$beamline.mat","cov_delta_bpm_read_no_jitter");
    #end

    # Store some model, data to check the correctness of the model with LUCRETIA
    #save('placet_sensor15_s.dat', 'sensA_s', '-ascii');
    #save('placet_quads_s.dat', 'quads_s', '-ascii');
    #save('placet_R_with_sbend.dat', 'R_disp_quad', '-ascii');
    #save('placet_R_with_sbend_combined.dat', 'R_model_combined', '-ascii');
    #save('placet_quads_combined_s.dat', 'quads_combined_s', '-ascii');
    #save('placet_bpm_s.dat', 'bpm_s', '-ascii');
    #return

    for seed=seed1 : (nseed+seed1-1)
        fprintf(1, "\n   \# seed %i \#\n\n", seed);
        Tcl_SetVar("seed",seed);
        set_imperfections
	pos_quads_ini(:,1)=placet_element_get_attribute("ATF2",quads,"x");
	pos_quads_ini(:,2)=placet_element_get_attribute("ATF2",quads,"y");
       eval_string = sprintf("/Users/jpfingst/Work/clicsim/trunk/CLIC/Frameworks/clic-integrated-studies/data/GM/harm.ATF2_2014_long_seed%i.300",seed);
       eval_string1 = sprintf("GroundMotionInit -file %s -x 1 -y 1 -t_step %g -s_sign 1 -s_abs 1 -last_start 0 -s_start 0 -t_start 0", eval_string, Tstep); 
        Tcl_Eval(eval_string1);
	if (gm_filter==1)
	disp("add filter for sensors");
        for sensor = sensor_list
                eval_string2 = sprintf("AddGMFilter -file %s -filter_x ./guralp_seismometer_vertical.dat -filter_y ./guralp_seismometer_vertical.dat -start_element %i -end_element %i", eval_string, sensor, sensor);
	        Tcl_Eval(eval_string2);
	    end
	end
    
    fprintf("Tracking for %i pulses\n", nstep);
    for i=0:nstep
        t = time0 + Tstep * (i-1)
	Tcl_Eval("GroundMotion");

        # Real and interpolated element position
        pos_quad(seed,:,i+1)=[ placet_element_get_attribute("ATF2",quads, "x")*1e-6; placet_element_get_attribute("ATF2",quads, 'y')*1e-6];
        pos_bpm(seed,:,i+1)=[ placet_element_get_attribute("ATF2",bpms, "x")*1e-6; placet_element_get_attribute("ATF2",bpms, 'y')*1e-6];

        for loop_index_combined=1:nr_quads_combined
            indices_local=quads_combined(loop_index_combined).quad_indices;
            pos_quad_combined(seed,loop_index_combined,i+1) = mean(placet_element_get_attribute("ATF2",indices_local, "x")*1e-6);
            pos_quad_combined(seed,loop_index_combined+nr_quads_combined,i+1) = mean(placet_element_get_attribute("ATF2",indices_local, "y")*1e-6);
        end    

        pos_sensor_read(seed,:,i+1) = [placet_element_get_attribute("ATF2",sensorA_list, 'x') ; placet_element_get_attribute("ATF2",sensorA_list, 'y')]*1e-6;
        pos_quad_meas_A(seed,:,i+1) = [interp1([0; sensA_s], [0; placet_element_get_attribute("ATF2",sensorA_list, 'x')*1e-6], quads_s, "$interp_method",'extrap');...
interp1([0; sensA_s], [0; placet_element_get_attribute("ATF2",sensorA_list, 'y')*1e-6], quads_s, "$interp_method",'extrap')];
        pos_bpm_meas_A(seed,:,i+1) = [interp1([0; sensA_s], [0; placet_element_get_attribute("ATF2",sensorA_list, 'x')*1e-6], bpm_s, "$interp_method",'extrap');...
interp1([0; sensA_s], [0; placet_element_get_attribute("ATF2",sensorA_list, 'y')*1e-6], bpm_s, "$interp_method",'extrap')];
        pos_quad_meas_B(seed,:,i+1) = [interp1([0; sensB_s], [0; placet_element_get_attribute("ATF2",sensorB_list, 'x')*1e-6], quads_s, "$interp_method",'extrap');...
interp1([0; sensB_s], [0; placet_element_get_attribute("ATF2",sensorB_list, 'y')*1e-6], quads_s, "$interp_method",'extrap')];
        pos_bpm_meas_B(seed,:,i+1) = [interp1([0; sensB_s], [0; placet_element_get_attribute("ATF2",sensorB_list, 'x')*1e-6], bpm_s, "$interp_method",'extrap');...
interp1([0; sensB_s], [0;  placet_element_get_attribute("ATF2",sensorB_list, 'y')*1e-6], bpm_s, "$interp_method",'extrap')];

    #size([interp1([0; sensA_s], [0; placet_element_get_attribute("ATF2",sensorA_list, 'x')*1e-6], quads_combined_s, "$interp_method",'extrap'); interp1([0; sensA_s], [0; placet_element_get_attribute("ATF2",sensorA_list, 'y')*1e-6], quads_combined_s, "$interp_method",'extrap')])
        pos_quad_combined_meas_A(seed,:,i+1) = [interp1([0; sensA_s], [0; placet_element_get_attribute("ATF2",sensorA_list, 'x')*1e-6], quads_combined_s, "$interp_method",'extrap'); interp1([0; sensA_s], [0; placet_element_get_attribute("ATF2",sensorA_list, 'y')*1e-6], quads_combined_s, "$interp_method",'extrap')];
        pos_quad_combined_meas_B(seed,:,i+1) = [interp1([0; sensB_s], [0; placet_element_get_attribute("ATF2",sensorB_list, 'x')*1e-6], quads_combined_s, "$interp_method",'extrap'); interp1([0; sensB_s], [0; placet_element_get_attribute("ATF2",sensorB_list, 'y')*1e-6], quads_combined_s, "$interp_method",'extrap')];

        #adding beam offset, here only one fixed jitter source is assumed!!!
        npart=size(B_usi_ini,1);
        beam_usi=B_usi_ini+ones(npart,1)*offset(i+1,:);
        beam=[ e0 * (1+beam_usi(:,6)) [beam_usi(:,1) beam_usi(:,3) beam_usi(:,5) beam_usi(:,2) beam_usi(:,4)]*1e6];
        placet_set_beam("part", beam);
			
	placet_test_no_correction("ATF2", "part","None");
        bpm_read=placet_get_bpm_readings("ATF2", bpms,true)*1e-6;
        readings_nobpmerrors(seed,1:nbpm,i+1)=bpm_read(:,1);
        readings_nobpmerrors(seed,nbpm+1:2*nbpm,i+1)=bpm_read(:,2);
        bpm_read=placet_get_bpm_readings("ATF2", bpms,false)*1e-6;
        readings(seed,1:nbpm,i+1)=bpm_read(:,1);
        readings(seed,nbpm+1:2*nbpm,i+1)=bpm_read(:,2);

        # Determine the effect of only the ground motion        
        beam_usi=B_usi_ini;
        beam=[ e0 * (1+beam_usi(:,6)) [beam_usi(:,1) beam_usi(:,3) beam_usi(:,5) beam_usi(:,2) beam_usi(:,4)]*1e6];
        placet_set_beam("part", beam);
        placet_test_no_correction("ATF2", "part","None");
        bpm_read=placet_get_bpm_readings("ATF2", bpms,true)*1e-6;
        readings_gm_nobpmerrors(seed,1:nbpm,i+1)=bpm_read(:,1);
        readings_gm_nobpmerrors(seed,nbpm+1:2*nbpm,i+1)=bpm_read(:,2);

        # Get the effect of only the jitter
        readings_jitter_nobpmerrors(seed,:,i+1) = readings_nobpmerrors(seed,:,i+1) - readings_gm_nobpmerrors(seed,:,i+1);    

    end
    disp("jitter subtraction")
    jitter_subtractions
          
    disp("plotting")
    plotting1
end
disp "\nFinal evaluation\n"
plotting2
save -ascii "$data/p_A.dat" "p_A";
        
}
file delete particles.in particles.tmp beam.dat
set now [clock seconds ]
file mtime $plots $now
