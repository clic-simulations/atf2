function [xmover_pos,ymover_pos]=compute_steering_FF(bpm,bpm_read,bpm_ref,xmover,ymover,gain_orbit_x,gain_orbit_y)
%compute the correctors' strength to apply steering

%get transfert matrix betweens movers displacement and bpms
 
    nxmover=length(xmover);
    nymover=length(ymover);
    nbpm=length(bpm);
    xmover_KL=placet_element_get_attribute("ATF2",xmover,"strength");
    ymover_KL=placet_element_get_attribute("ATF2",ymover,"strength");

	xmatrix=zeros(nbpm,nxmover);
	ymatrix=zeros(nbpm,nymover);
    for i=1:nbpm
    	for j=1:nxmover
    		if(xmover(j)<bpm(i))
    			R=placet_get_transfer_matrix("ATF2",xmover(j),bpm(i));
    			xmatrix(i,j)=R(1,2)*xmover_KL(j);
    		end
    	end
    	for j=1:nymover
    		if(ymover(j)<bpm(i))
    			R=placet_get_transfer_matrix("ATF2",ymover(j),bpm(i));
    			ymatrix(i,j)=R(3,4)*-ymover_KL(j);
    		end
    	end
    end    
    xmover_read=placet_element_get_attribute("ATF2",xmover,"x")*1e-6;
    ymover_read=placet_element_get_attribute("ATF2",ymover,"y")*1e-6;
    xmover_used=1:nxmover;
    ymover_used=1:nymover;    
    xmover_pos=xmover_read;
    ymover_pos=ymover_read;
    xmover_max=1e-3; %[rad]
    ymover_max=1e-3; %[rad]
    nxmover_used_old=0;
    nymover_used_old=0;
    
%as long as there is a saturation
    while (nxmover_used_old~=length(xmover_used) || nymover_used_old~=length(ymover_used))
%compute mover positions
        if (~isempty(xmover_used))
            xmover_pos(xmover_used)=-pinv([xmatrix(:,xmover_used);eye(length(xmover_used))])*gain_orbit_x*([bpm_read(:,1)-bpm_ref(:,1) ; 1*zeros(length(xmover_used),1)])+xmover_read(xmover_used);
        end;
        if(~isempty(ymover_used))
            ymover_pos(ymover_used)=-pinv([ymatrix(:,ymover_used);eye(length(ymover_used))])*gain_orbit_y*([bpm_read(:,2)-bpm_ref(:,2); 1*zeros(length(ymover_used),1)])+ymover_read(ymover_used);
        end;
%300nm step=> round
	xmover_pos=round(xmover_pos./0.3e-6).*0.3e-6;
	ymover_pos=round(ymover_pos./0.3e-6).*0.3e-6;
%look if there is saturation
	nxmover_used_old=length(xmover_used);
	nymover_used_old=length(ymover_used);
%set first corrector saturated to saturation, unselect it, compute effect on orbit
	[xmover_pos,xmover_used,bpm_read(:,1)]=set_first_saturation(xmover_pos,xmover_used,xmover_max,bpm_read(:,1),xmatrix,xmover_read);
	[ymover_pos,ymover_used,bpm_read(:,2)]=set_first_saturation(ymover_pos,ymover_used,ymover_max,bpm_read(:,2),ymatrix,ymover_read);
%if there is saturation, reset corr strength
	if(nxmover_used_old~=length(xmover_used) || nymover_used_old~=length(ymover_used))
		xmover_pos(xmover_used)=xmover_read(xmover_used);
		ymover_pos(ymover_used)=ymover_read(ymover_used);
        end
    end
end

function [C_x_strength,corx_used,bpm_read]=set_first_saturation(C_x_strength,corx_used,cor_strength_max_x,bpm_read,matrix_x,cx_strength_old)
%look for saturation
	saturated_x_pos=corx_used(C_x_strength(corx_used)>cor_strength_max_x);
	saturated_x_neg=corx_used(C_x_strength(corx_used)<-cor_strength_max_x);
%	saturated_x_nul=corx_used(C_x_strength(corx_used)==0);
	saturated_x_nul=[];
	
    if( ~isempty(saturated_x_pos) && ~isempty(saturated_x_neg) && ~isempty(saturated_x_nul))		
		if( (saturated_x_pos(1) <= saturated_x_neg(1)) && (saturated_x_pos(1) <= saturated_x_nul(1)) )
			C_x_strength(saturated_x_pos(1))=cor_strength_max_x;
			corx_used(corx_used==saturated_x_pos(1))=[];
			bpm_read=bpm_read+(cor_strength_max_x-cx_strength_old(saturated_x_pos(1)))*matrix_x(:,saturated_x_pos(1));
		elseif ( (saturated_x_neg(1) <= saturated_x_pos(1)) && (saturated_x_neg(1) <= saturated_x_nul(1)) )
			C_x_strength(saturated_x_neg(1))=-cor_strength_max_x;
			corx_used(saturated_x_neg(1))=[];
			bpm_read=bpm_read+(-cor_strength_max_x-cx_strength_old(saturated_x_neg(1)))*matrix_x(:,saturated_x_neg(1));
		elseif ( (saturated_x_nul(1) <= saturated_x_pos(1)) && (saturated_x_nul(1) <= saturated_x_neg(1)) )
			C_x_strength(saturated_x_nul(1))=0;
			corx_used(corx_used==saturated_x_nul(1))=[];
		else disp "ERROR : x case with 3 saturations";
		end;
	elseif ( ~isempty(saturated_x_pos) &&~isempty(saturated_x_neg) )
		if(saturated_x_pos(1) <= saturated_x_neg(1))
			C_x_strength(saturated_x_pos(1))=cor_strength_max_x;
			corx_used(corx_used==saturated_x_pos(1))=[];
			bpm_read=bpm_read+(cor_strength_max_x-cx_strength_old(saturated_x_pos(1)))*matrix_x(:,saturated_x_pos(1));
		elseif(saturated_x_neg(1) <= saturated_x_pos(1))
			C_x_strength(saturated_x_neg(1))=-cor_strength_max_x;
			corx_used(corx_used==saturated_x_neg(1))=[];
			bpm_read=bpm_read+(-cor_strength_max_x-cx_strength_old(saturated_x_neg(1)))*matrix_x(:,saturated_x_neg(1));
	else disp "ERROR : x case with 2 saturations pos & neg";
		end;
	elseif ( ~isempty(saturated_x_pos) &&~isempty(saturated_x_nul) )
	if(saturated_x_pos(1) <= saturated_x_nul(1))
			C_x_strength(saturated_x_pos(1))=cor_strength_max_x;
			corx_used(corx_used==saturated_x_pos(1))=[];
			bpm_read=bpm_read+(cor_strength_max_x-cx_strength_old(saturated_x_pos(1)))*matrix_x(:,saturated_x_pos(1));
		elseif(saturated_x_nul(1) <= saturated_x_pos(1))
			C_x_strength(saturated_x_nul(1))=0;
			corx_used(corx_used==saturated_x_nul(1))=[];
		else disp "ERROR : x case with 2 saturations pos & nul";
		end;
	elseif ( ~isempty(saturated_x_neg) &&~isempty(saturated_x_nul) )
		if(saturated_x_neg(1) <= saturated_x_nul(1))
			C_x_strength(saturated_x_neg(1))=-cor_strength_max_x;
			corx_used(corx_used==saturated_x_neg(1))=[];
			bpm_read=bpm_read+(-cor_strength_max_x-cx_strength_old(saturated_x_neg(1)))*matrix_x(:,saturated_x_neg(1));
		elseif(saturated_x_nul(1) <= saturated_x_neg(1))
			C_x_strength(saturated_x_nul(1))=0;
			corx_used(corx_used==saturated_x_nul(1))=[];
		else disp "ERROR : x case with 2 saturations neg & nul";
		end;
	elseif (~isempty(saturated_x_pos))
		C_x_strength(saturated_x_pos(1))=cor_strength_max_x;
		corx_used(corx_used==saturated_x_pos(1))=[];
		bpm_read=bpm_read+(cor_strength_max_x-cx_strength_old(saturated_x_pos(1)))*matrix_x(:,saturated_x_pos(1));
	elseif (~isempty(saturated_x_neg))
		C_x_strength(saturated_x_neg(1))=-cor_strength_max_x;
		corx_used(corx_used==saturated_x_neg(1))=[];
		bpm_read=bpm_read+(-cor_strength_max_x-cx_strength_old(saturated_x_neg(1)))*matrix_x(:,saturated_x_neg(1));
	elseif (~isempty(saturated_x_nul))
		C_x_strength(saturated_x_nul(1))=0;
		corx_used(corx_used==saturated_x_nul(1))=[];
    end;
end
