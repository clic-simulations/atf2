*******************************************************************************
*                               feedforward                                   *
*******************************************************************************
1) Description

Simulation detecting effect of ground motion on BPMs measurement.

2) Installation and Usage

feedforward require placet-octave to be installed. To install placet-octave, 
see http://project-placet.web.cern.ch/project-placet/
Once placet-octave is installed,to lauch the feedforward simulation, do :
placet-octave feedforward.tcl

3) Parameters

parameters of the simulation are listed at the begining of the feedforward.tcl file.
* time0 : initial time
* Tstep : time between 2 pulses
* nstep : number of pulses to simulate
* seed1 : initial seed (used for the ground motion generator, initial magnets offsets and beam jitter)
* nseed : number of seeds used in the simulation (the simulation is done for nseed starting at seed1)
* offset_ini : initial magnet offsets amplitude in microns
* scale_error : BPM scale errors amplitude
* dk_over_k : fractional strength errors of the quadrupoles
* gm_filter : set to 1 to include the ground motion sensors transfer functions
* interp_method : interpolation method between sensor measurements 
    'nearest'
          Return the nearest neighbour.
    'linear'
          Linear interpolation from nearest neighbours
    'pchip'
          Piece-wise cubic hermite interpolating polynomial
    'cubic'
          Cubic interpolation from four nearest neighbours
    'spline'
          Cubic spline interpolation-smooth first and second derivatives
          throughout the curve
* stripline : striplines BPM resolution in microns (except the 5 firsts)
* stripline2 : 5 first striplines BPM resolution in microns
* cband : Cavity BPM resolution in microns 
* ipbpm : IPBPM resolution in microns
* beamline : lattice to use (either "ATF2"for the nominal lattice or "UL" for the ultralow beta lattice)
* sext : set to 0 to switch off the sextupoles or to 1 to sitch them on

4) Plots

Plots generated are located in the "plots" directory (created at first execution), 
in a subdirrectory named according to the parameters (see section 3).
the subdirrectory name template is :
* offset_ini_$offset_ini-scale_error_$scale_error-dk_$dk_over_k-resolstrip_$stripline-resolstrip2_$stripline2-resolcband_$cband
    if the $stripline and stripline2 parameters are different
* offset_ini_$offset_ini-scale_error_$scale_error-dk_$dk_over_k-resolstrip_$stripline-resolcband_$cband
    if the $stripline and stripline2 parameters are the same

the figures are :

* R_disp_quad.eps : matrix of the effect of the quadrupole displacements on the BPM readings.
and for each seed :
* GM_amplitudex_seed_$seed : horizontal amplitude of the grounf motion effect compared with the error due to the interpolation of the quadrupoles position between the sensors
* GM_amplitudey_seed_$seed : vertical amplitude of the grounf motion effect compared with the error due to the interpolation of the quadrupoles position between the sensors
* effectx-seed_$seed.eps : horizontal amplitudes of the followings effect on the BPMs readings : 
    - the beam jitter effects
    - the ground motion effects
    - the ground motion effects minus the part reconstructed as jitter
    - the BPM resolution
* effecty-seed_$seed.eps : vertical amplitudes of the same effects.
* residualx-seed_$seed.eps : 4 curves related to horizontal measurements called :
    - "residuals" : amplitude of jitter subtracted BPM measurements
    - "residual - GM (true quad. pos.)" : amplitude of jitter subtracted BPM measurements 
        minus the ground motion effect obtain with the true quadrupole displacements.
    - "residual - GM (15 sensors)" : amplitude of jitter subtracted BPM measurements 
        minus the ground motion effect obtain with the interpolation using 15 sensors.
    - "residual - GM (30 sensors)" : amplitude of jitter subtracted BPM measurements 
        minus the ground motion effect obtain with the interpolation using 15 sensors.
* residualy-seed_$seed.eps : the same 4 curves related to vertical measurements
* fraction_residualx-seed_$seed.eps : the resolution of the BPM divided by the sum of the resolution of the BPM and the ground motion effect compared 
    with the "p" value using the true quadrupoles positions and 
    with the "p" value using the interpolation with 15 or 30 sensors .
"p" is the ratio : (amplitude of the jitter substracted BPM readings - ground motion effect)/(amplitude of the jitter substracted BPM readings + ground motion effect) 
if "p"=1 it means there is no correlation between the jitter substracted BPM readings and the estimation of the ground motion effects from the sensors (BAD !)
if "p"=0 it means the jitter substracted BPM readings are equal to the estimation of the ground motion effects from the sensors (GOOOD !!!)
generaly the lower is "p", the better is the estimation of the ground motion effects from the sensors

5) contact
For any question problem, contact me at yrenier@cern.ch
