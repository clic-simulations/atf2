f = figure(1);
set(f,"visible","off");

%%%%%%%%%%%%%%%%%%%
% Response matrix %
%%%%%%%%%%%%%%%%%%%

disp("R_disp_quad.eps")
clf
subplot(1,2,1)
surf(quads_s,bpm_s,R_disp_quad(1:nbpm,1:nquad))
xlabel("quad s")
ylabel("bpm s")
title("horizontal response matrix")
view(0,90)
colorbar
subplot(1,2,2)
surf(quads_s,bpm_s,R_disp_quad(nbpm+1:2*nbpm,nquad+1:2*nquad))
xlabel("quad s")
ylabel("bpm s")
title("vertical response matrix")
view(0,90)
colorbar
text_buffer = sprintf("%s/R_disp_quad.eps",plots);
print(text_buffer,'-color');
subplot(1,1,1)
hold off;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Estimated quadrupole misalignments %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp("GM_amplitudex-allseed.eps");
clf
hold on
%plot(quads_s,mean(quad_ampl_A(:,1:nquad)),'b-')
plot(mean(quad_ampl_A(:,1:nquad)),'b-')
h=errorbar(mean(quad_ampl_A(:,1:nquad)),std(quad_ampl_A(:,1:nquad)),"~");
set(h(1),"color",[0 0 1])
plot(mean(quad_ampl_B(:,1:nquad)),'g-')
h=errorbar(mean(quad_ampl_B(:,1:nquad)),std(quad_ampl_B(:,1:nquad)),"~");
set(h(1),"color",[0 1 0])
plot(mean(quad_ampl(:,1:nquad)),'r-')
h=errorbar(mean(quad_ampl(:,1:nquad)),std(quad_ampl(:,1:nquad)),"~");
set(h(1),"color",[1 0 0])
legend("","measurement error (15 sensors)","","measurement error (30 sensors)","","quadrupole displacement amplitude");
ylabel("horizontal amplitude [um]")
ylim([0 0.5])
grid on;
xlabel("quadrupole number [1]");
text_buffer = sprintf("%s/GM_amplitudex-allseed.eps",plots);
print(text_buffer,'-color');
hold off        

disp("GM_amplitudey-allseed.eps");
clf
hold on
plot(mean(quad_ampl_A(:,nquad+1:2*nquad)),'b-')
h=errorbar(mean(quad_ampl_A(:,nquad+1:2*nquad)),std(quad_ampl_A(:,nquad+1:2*nquad)),"~");
set(h(1),"color",[0 0 1])
plot(mean(quad_ampl_B(:,nquad+1:2*nquad)),'g-')
h=errorbar(mean(quad_ampl_B(:,nquad+1:2*nquad)),std(quad_ampl_B(:,nquad+1:2*nquad)),"~");
set(h(1),"color",[0 1 0])
plot(mean(quad_ampl(:,nquad+1:2*nquad)),'r-')
h=errorbar(mean(quad_ampl(:,nquad+1:2*nquad)),std(quad_ampl(:,nquad+1:2*nquad)),"~");
set(h(1),"color",[1 0 0])
legend("","measurement error (15 sensors)","","measurement error (30 sensors)","","quadrupole displacement amplitude");
ylabel("vertical amplitude [um]")
ylim([0 0.5])
grid on;
xlabel("quadrupole number [1]");
text_buffer = sprintf("%s/GM_amplitudey-allseed.eps",plots);
print(text_buffer,'-color');
hold off

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Different contributions to the beam motion %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp("effectx-allseed.eps");
clf
hold on
%plot(bpm_s,mean(effect_beamjitter(:,1:nbpm)),'b-');
plot(mean(effect_beamjitter(:,1:nbpm)),'b-');
h=errorbar(mean(effect_beamjitter(:,1:nbpm)),std(effect_beamjitter(:,1:nbpm)));
set(h(1),"color",[0 0 1])
plot(mean(effect_beamjittersub(:,1:nbpm)),'-y');
h=errorbar(mean(effect_beamjittersub(:,1:nbpm)),std(effect_beamjittersub(:,1:nbpm)));
set(h(1),"color",'y');
plot(mean(effect_GM(:,1:nbpm)),'g-');
h=errorbar(mean(effect_GM(:,1:nbpm)),std(effect_GM(:,1:nbpm)));
set(h(1),"color",[0 1 0])        
#plot(mean(effect_GM_noR(:,1:nbpm)),'-k');
#h=errorbar(mean(effect_GM_noR(:,1:nbpm)),std(effect_GM_noR(:,1:nbpm)));
#set(h(1),"color",[0 0 0])        
plot(mean(effect_GM_jittersub(:,1:nbpm)),'k-');
h=errorbar(mean(effect_GM_jittersub(:,1:nbpm)),std(effect_GM_jittersub(:,1:nbpm)));
set(h(1),"color",[0 0 0])        
semilogy(sqrt(2).*bpm_resol,'c-');
grid on
legend("","full beam motion","","jitter subtracted beam motion","","real GM effect","","real GM effect (jitter substracted)","BPM resolution")
xlabel("BPM number [1]");
%ylim([1e-3 1e-8]);
ylabel("horizontal effects [m]")
text_buffer = sprintf("%s/effectx-allseed.eps",plots);
print(text_buffer, '-color');
hold off       

disp("effecty-allseed.eps");
clf
hold on
grid on
plot(mean(effect_beamjitter(:,nbpm+1:2*nbpm)),'b-');
h=errorbar(mean(effect_beamjitter(:,nbpm+1:2*nbpm)),std(effect_beamjitter(:,nbpm+1:2*nbpm)));
set(h(1),"color",[0 0 1])
plot(mean(effect_beamjittersub(:,nbpm+1:2*nbpm)),'-y');
h=errorbar(mean(effect_beamjittersub(:,nbpm+1:2*nbpm)),std(effect_beamjittersub(:,nbpm+1:2*nbpm)));
set(h(1),"color",'y')
plot(mean(effect_GM(:,nbpm+1:2*nbpm)),'r-');
h=errorbar(mean(effect_GM(:,nbpm+1:2*nbpm,:)),std(effect_GM(:,nbpm+1:2*nbpm)));
set(h(1),"color",[1 0 0])        
#plot(mean(effect_GM_noR(:,nbpm+1:2*nbpm)),'-k');
#h=errorbar(mean(effect_GM_noR(:,nbpm+1:2*nbpm,:)),std(effect_GM_noR(:,nbpm+1:2*nbpm)));
#set(h(1),"color",[0 0 0])        
plot(mean(effect_GM_jittersub(:,nbpm+1:2*nbpm)),'k-');
h=errorbar(mean(effect_GM_jittersub(:,nbpm+1:2*nbpm)),std(effect_GM_jittersub(:,nbpm+1:2*nbpm)));        
set(h(1),"color",[0 0 0])        
semilogy(sqrt(2).*bpm_resol,'k-');
legend("","beam jitter","","jitter subtracted beam motion","","GM effect","","GM effect (jitter substracted)","BPM resolution")
%legend("","beam jitter","","GM effect","BPM resolution")
xlabel("BPM number [1]");
ylim([1e-8 1e-3]);
ylabel("vertical effects [m]")
text_buffer = sprintf("%s/effecty-allseed.eps",plots);
print(text_buffer,'-color');
hold off

% Debug
%save_data = [bpm_s, mean(effect_beamjitter(:,1:nbpm))',mean(effect_GM(:,1:nbpm))',mean(effect_GM_jittersub(:,1:nbpm))'];
%save('beam_jitter_sim_x.dat', 'save_data', '-ascii');
%save_data = [bpm_s, mean(effect_beamjitter(:,nbpm+1:2*nbpm))',mean(effect_GM(:,nbpm+1:2*nbpm))',mean(effect_GM_jittersub(:,nbpm+1:2*nbpm))'];
%save('beam_jitter_sim_y.dat', 'save_data', '-ascii');
% dEBUG

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Relative error of reconstruction %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp("fraction_residualx-allseed.eps")
clf
hold on
%plot(bpm_s,mean(p(:,1:nbpm)),"b-")
plot(mean(p(:,1:nbpm)),"b-")
h=errorbar(mean(p(:,1:nbpm)),std(p(:,1:nbpm)),"~");
set(h(1),"color",[0 0 1])
plot(mean(p_B(:,1:nbpm)),"g-")
h=errorbar(mean(p_B(:,1:nbpm)),std(p_B(:,1:nbpm)),"~");
set(h(1),"color",[0 1 0])
plot(mean(p_A(:,1:nbpm)),"r-")
h=errorbar(mean(p_A(:,1:nbpm)),std(p_A(:,1:nbpm)),"~");
set(h(1),"color",[1 0 0])

plot(mean(p_noR(:,1:nbpm)),"c-")
h=errorbar(mean(p_noR(:,1:nbpm)),std(p_noR(:,1:nbpm)),"~");
set(h(1),"color",[0 1 1])
plot(mean(p_B_noR(:,1:nbpm)),"k-")
h=errorbar(mean(p_B_noR(:,1:nbpm)),std(p_B_noR(:,1:nbpm)),"~");
set(h(1),"color",[0 0 0])
plot(mean(p_A_noR(:,1:nbpm)),"m-")
h=errorbar(mean(p_A_noR(:,1:nbpm)),std(p_A_noR(:,1:nbpm)),"~");
set(h(1),"color",[1 1 0])

plot(sqrt(2).*bpm_resol./(2.*std(effect_delta_GM_jitter_sub(1:nbpm,:),0,2)+sqrt(2).*bpm_resol),"c")
grid on
legend("","true quadrupole positions","","30 sensors","","15 sensors","","true quadrupole positions noR","","30 sensors no R","","15 sensors noR","best achievable (resol BPM / (effect GM + resol BPM))");
%legend("","true quadrupole positions","","30 sensors","","15 sensors","best achievable (resol BPM / (effect GM + resol BPM))");
%legend("","true quadrupole positions","","30 sensors","","15 sensors","no jitter and true postions");
ylim([0 1.4])
xlabel("BPM number [1]");
ylabel("p")
text_buffer = sprintf("%s/fraction_residualx-allseed.eps",plots);
print(text_buffer,'-color');
hold off

disp("fraction_residualy-allseed.eps")
clf
hold on
plot(mean(p(:,nbpm+1:2*nbpm)),"b-")
h=errorbar(mean(p(:,nbpm+1:2*nbpm)),std(p(:,nbpm+1:2*nbpm)),"~");
set(h(1),"color",[0 0 1])
plot(mean(p_B(:,nbpm+1:2*nbpm)),"g-")
h=errorbar(mean(p_B(:,nbpm+1:2*nbpm)),std(p_B(:,nbpm+1:2*nbpm)),"~");
set(h(1),"color",[0 1 0])
plot(mean(p_A(:,nbpm+1:2*nbpm)),"r-")
h=errorbar(mean(p_A(:,nbpm+1:2*nbpm)),std(p_A(:,nbpm+1:2*nbpm)),"~");
set(h(1),"color",[1 0 0])

plot(mean(p_noR(:,nbpm+1:2*nbpm)),"c-")
h=errorbar(mean(p_noR(:,nbpm+1:2*nbpm)),std(p_noR(:,nbpm+1:2*nbpm)),"~");
set(h(1),"color",[0 1 1])
plot(mean(p_B_noR(:,nbpm+1:2*nbpm)),"k-")
h=errorbar(mean(p_B_noR(:,nbpm+1:2*nbpm)),std(p_B_noR(:,nbpm+1:2*nbpm)),"~");
set(h(1),"color",[0 0 0])
plot(mean(p_A_noR(:,nbpm+1:2*nbpm)),"m-")
h=errorbar(mean(p_A_noR(:,nbpm+1:2*nbpm)),std(p_A_noR(:,nbpm+1:2*nbpm)),"~");
set(h(1),"color",[1 1 0])

plot(sqrt(2).*bpm_resol./(2.*std(effect_delta_GM_jitter_sub(nbpm+1:2*nbpm,:),0,2)+sqrt(2).*bpm_resol),"c")
grid on
legend("","true quadrupole positions","","30 sensors","","15 sensors","","true quadrupole positions noR","","30 sensors noR","","15 sensors noR","best achievable (resol BPM / (effect GM + resol BPM))");
%legend("","true quadrupole positions","","30 sensors","","15 sensors","best achievable (resol BPM / (effect GM + resol BPM))");
%legend("","true quadrupole positions","","30 sensors","","15 sensors","no jitter and true postions");
ylim([0 1.4])
xlabel("BPM number [1]");
ylabel("p")
text_buffer = sprintf("%s/fraction_residualy-allseed.eps",plots);
print(text_buffer,'-color');
hold off

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Correlation of reconstruction %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp("corr_residualx-allseed.eps")
clf
hold on
%plot(bpm_s,mean(p(:,1:nbpm)),"b-")
plot(mean(r(:,1:nbpm)),"b-")
h=errorbar(mean(r(:,1:nbpm)),std(r(:,1:nbpm)),"~");
set(h(1),"color",[0 0 1])
plot(mean(r_B(:,1:nbpm)),"g-")
h=errorbar(mean(r_B(:,1:nbpm)),std(r_B(:,1:nbpm)),"~");
set(h(1),"color",[0 1 0])
plot(mean(r_A(:,1:nbpm)),"r-")
h=errorbar(mean(r_A(:,1:nbpm)),std(r_A(:,1:nbpm)),"~");
set(h(1),"color",[1 0 0])

plot(mean(r_noR(:,1:nbpm)),"c-")
h=errorbar(mean(r_noR(:,1:nbpm)),std(r_noR(:,1:nbpm)),"~");
set(h(1),"color",[0 1 1])
plot(mean(r_B_noR(:,1:nbpm)),"k-")
h=errorbar(mean(r_B_noR(:,1:nbpm)),std(r_B_noR(:,1:nbpm)),"~");
set(h(1),"color",[0 0 0])
plot(mean(r_A_noR(:,1:nbpm)),"m-")
h=errorbar(mean(r_A_noR(:,1:nbpm)),std(r_A_noR(:,1:nbpm)),"~");
set(h(1),"color",[1 1 0])

%plot(sqrt(2).*bpm_resol./(2.*std(effect_delta_GM_jitter_sub(1:nbpm,:),0,2)+sqrt(2).*bpm_resol),"c")
grid on
legend("","true quadrupole positions","","30 sensors","","15 sensors","","true quadrupole positions noR","","30 sensors no R","","15 sensors noR");
%legend("","true quadrupole positions","","30 sensors","","15 sensors","best achievable (resol BPM / (effect GM + resol BPM))");
%legend("","true quadrupole positions","","30 sensors","","15 sensors","no jitter and true postions");
ylim([-1.4 1.4])
xlabel("BPM number [1]");
ylabel("r")
text_buffer = sprintf("%s/corr_residualx-allseed.eps",plots);
print(text_buffer,'-color');
hold off

disp("corr_residualy-allseed.eps")
clf
hold on
plot(mean(r(:,nbpm+1:2*nbpm)),"b-")
h=errorbar(mean(r(:,nbpm+1:2*nbpm)),std(r(:,nbpm+1:2*nbpm)),"~");
set(h(1),"color",[0 0 1])
plot(mean(r_B(:,nbpm+1:2*nbpm)),"g-")
h=errorbar(mean(r_B(:,nbpm+1:2*nbpm)),std(r_B(:,nbpm+1:2*nbpm)),"~");
set(h(1),"color",[0 1 0])
plot(mean(r_A(:,nbpm+1:2*nbpm)),"r-")
h=errorbar(mean(r_A(:,nbpm+1:2*nbpm)),std(r_A(:,nbpm+1:2*nbpm)),"~");
set(h(1),"color",[1 0 0])

plot(mean(r_noR(:,nbpm+1:2*nbpm)),"c-")
h=errorbar(mean(r_noR(:,nbpm+1:2*nbpm)),std(r_noR(:,nbpm+1:2*nbpm)),"~");
set(h(1),"color",[0 1 1])
plot(mean(r_B_noR(:,nbpm+1:2*nbpm)),"k-")
h=errorbar(mean(r_B_noR(:,nbpm+1:2*nbpm)),std(r_B_noR(:,nbpm+1:2*nbpm)),"~");
set(h(1),"color",[0 0 0])
plot(mean(r_A_noR(:,nbpm+1:2*nbpm)),"m-")
h=errorbar(mean(r_A_noR(:,nbpm+1:2*nbpm)),std(r_A_noR(:,nbpm+1:2*nbpm)),"~");
set(h(1),"color",[1 1 0])

%plot(sqrt(2).*bpm_resol./(2.*std(effect_delta_GM_jitter_sub(nbpm+1:2*nbpm,:),0,2)+sqrt(2).*bpm_resol),"c")
grid on
legend("","true quadrupole positions","","30 sensors","","15 sensors","","true quadrupole positions noR","","30 sensors noR","","15 sensors noR");
%legend("","true quadrupole positions","","30 sensors","","15 sensors","best achievable (resol BPM / (effect GM + resol BPM))");
%legend("","true quadrupole positions","","30 sensors","","15 sensors","no jitter and true postions");
ylim([-1.4 1.4])
xlabel("BPM number [1]");
ylabel("r")
text_buffer = sprintf("%s/cov_residualy-allseed.eps",plots);
print(text_buffer,'-color');
hold off
