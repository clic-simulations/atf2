import numpy as np
from array import *

out_file = 'modify.madx'
fw= open(out_file,'w')

filename = 'integrated_magnet_strengths.dat'
f= open(filename)
lines = f.readlines()
for i in range(2,len(lines)):
	line = lines[i].split()
	string = line[0]
	if string[0] != 'Z':
		if float(line[1]) < 0:
			fw.write('exec,modify(' +str(line[0])+ ',' + str(abs(float(line[1]))) + ', -);\n')	
		if float(line[1]) >= 0:
			fw.write('exec,modify(' +str(line[0])+ ',' + str(float(line[1])) + ', +);\n')	
f.close()
fw.close()
print 'Output file ___' + out_file + '___ created.' 


