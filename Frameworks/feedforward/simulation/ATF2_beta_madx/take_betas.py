from numpy import *
from scipy.io import savemat


filename = 'BPMs_beta.dat'
f= open(filename)
lines = f.readlines()
beta_x = zeros(len(lines))
beta_y = zeros(len(lines))
for i in range(0,len(lines)):
	line = lines[i].split()
	beta_x[i] = line[1]
	beta_y[i] = line[2]

#print beta_x
madx_beta = {}
madx_beta['madx_beta_x'] = beta_x
madx_beta['madx_beta_y'] = beta_y
savemat('madx_beta.mat',madx_beta)

