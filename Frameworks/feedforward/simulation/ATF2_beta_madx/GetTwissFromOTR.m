#! /usr/bin/octave -qf

arg_list = argv ();
filename = arg_list{1};
load(filename)


%load emit2dOTRp_20140304T144459.mat

mux=2.964400631;
muy=2.289486318;

disp(['betx_otr= ' num2str(data.betax) ';'])
disp(['bety_otr= ' num2str(data.betay) ';'])
disp(['alfx_otr= ' num2str(data.alphx) ';'])
disp(['alfy_otr= ' num2str(data.alphy) ';'])
disp(['dx_otr= ' num2str(data.DX(1)) ';'])
disp(['dy_otr= ' num2str(data.DY(1)) ';'])
disp(['dpx_otr= ' num2str(data.DPX(1)) ';'])
disp(['dpy_otr= ' num2str(data.DPY(1)) ';'])
disp(['mux_otr= ' num2str(mux) ';'])
disp(['muy_otr= ' num2str(muy) ';'])
