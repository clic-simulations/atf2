#!/bin/bash

INT_MAG_STR="data/ATF2_GM_shift_28May"	# Name of the file containing strengths of ATF2 magnets. File from
echo $INT_MAG_STR "file loaded"		# atfopr@atf-user:/atf/op/sad/current/data/

OTR_DATA="data/emit2dOTRp_20140527T185711.mat"	# Name of OTR data file
echo $OTR_DATA "file loaded"


./make_two_columns.sh $INT_MAG_STR # Removes all the brackets etc, leaves only columns 
python make_modify.py # Modifies the file, so it can be used in madx
octave -qf GetTwissFromOTR.m $OTR_DATA > subEXT_inverted_twiss_parameters.madx
echo "OTR Twiss parameters written to ___ subEXT_inverted_twiss_parameters.madx ___ file."
madx < job3.madx	> madx.log
echo "Madx logs stored in madx.log"	
#echo "BPM_Name	BETX	BETY" > BPMs_beta.dat
grep '\"MQ\|\"MS\|\"MPREIP\|\"IPBPMA\|\"IPBPMB' savelineATF2_onlybetas.twiss > temp1
grep -v 'MS1X\|MSKE\|MSF6FF\|MQD0FF\|MQD2AFF\|MSF1FF\|MQF1FF\|MSD0FF\|MS1IP' temp1 > BPMs_beta.dat
rm temp1
echo "++++++++++++++++++++++++++++"
echo "BPMs_beta generated into BPMs_beta.dat !"
python take_betas.py
echo "Matlab madx_beta.mat containing bpms beta values from MADX model created!"

