#!/bin/bash

OUT='integrated_magnet_strengths.dat'

# replace {" -> blank
if [ $# -eq 0 ]
then
	sed 's/ \({"\)//' $1 > out.txt
else
	sed 's/ \({"\)//' $1 > out.txt
	#Parameter $1 is the name of the input file!!!
fi

#remove };
sed -i 's/\};//' out.txt
#remove },
sed -i 's/\},//' out.txt
#remove }
sed -i 's/\}//' out.txt
# remove the lines with pattern vat
sed -i '/\(vat\)/d' out.txt 

# 2nd column
cut -d "," -f2 out.txt > column2.txt
# 1st column 
cut -d "\"" -f1 out.txt > column1.txt

# Reintegrate the file

paste column1.txt column2.txt > temp.dat
grep Q temp.dat > integrated_magnet_strengths.dat
grep S temp.dat >> integrated_magnet_strengths.dat
grep Z temp.dat >> integrated_magnet_strengths.dat
rm out.txt column1.txt column2.txt temp.dat
echo 'Output file ___' $OUT '___ created.'
