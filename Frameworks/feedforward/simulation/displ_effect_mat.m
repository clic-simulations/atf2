function R_disp_quad=displ_effect_mat(bpms,quads,beam_part)
        IEX=placet_get_name_number_list('ATF2', 'IEX');
        IEX=[ IEX, placet_get_name_number_list('ATF2', 'iex')];
        IP=placet_get_name_number_list('ATF2', 'IP');
        IP=[ IP, placet_get_name_number_list('ATF2', 'ip')];
        ampl=0.1;
	nbpm=length(bpms);
	nquad=length(quads);
        Rx_tot=zeros(nquad,2*nbpm);
        Ry_tot=zeros(nquad,2*nbpm);
        placet_set_beam("part", beam_part);	
        for i=1:nquad
            placet_element_set_attribute("ATF2",quads(i),"x",ampl);
            placet_test_no_correction("ATF2", "part","None");
            readings1=placet_get_bpm_readings("ATF2", bpms,true);
            placet_element_set_attribute("ATF2",quads(i),"x",-ampl);
            placet_test_no_correction("ATF2", "part","None");
            readings2=placet_get_bpm_readings("ATF2", bpms,true);
            placet_element_set_attribute("ATF2",quads(i),"x",0);
            #Rx_tot(i,:)=[(readings1(:,1)-readings2(:,1))/(2*ampl) ...
            #             (readings1(:,2)-readings2(:,2))/(2*ampl)];
            Rx_tot(i,:)=[((readings1(:,1)-readings2(:,1))./(2*ampl))' ...
                         ((readings1(:,2)-readings2(:,2))./(2*ampl))'];
            placet_element_set_attribute("ATF2",quads(i),"y",ampl);
            placet_test_no_correction("ATF2", "part","None");
            readings1=placet_get_bpm_readings("ATF2", bpms,true);
            placet_element_set_attribute("ATF2",quads(i),"y",-ampl);
            placet_test_no_correction("ATF2", "part","None");
            readings2=placet_get_bpm_readings("ATF2", bpms,true);
            placet_element_set_attribute("ATF2",quads(i),"y",0);
            #Ry_tot(i,:)=[(readings1(:,1)-readings2(:,1))/(2*ampl)...
            #             (readings1(:,2)-readings2(:,2))/(2*ampl)];
            Ry_tot(i,:)=[((readings1(:,1)-readings2(:,1))/(2*ampl))'...
                         ((readings1(:,2)-readings2(:,2))/(2*ampl))'];        
        end
        R_disp_quad=[Rx_tot;Ry_tot]';
 end