    IEX=placet_get_name_number_list('ATF2', 'IEX');
    IEX=[ IEX, placet_get_name_number_list('ATF2', 'iex')];
    IP=placet_get_name_number_list('ATF2', 'IP');
    IP=[ IP, placet_get_name_number_list('ATF2', 'ip')];
    bpms=placet_get_number_list("ATF2","bpm");
    % Take only some of the BPMs
    bpms_qx = placet_get_name_number_list('ATF2', 'MQ*X');
    bpms_qff = placet_get_name_number_list('ATF2', 'MQ*FF');
    bpms_sff = placet_get_name_number_list('ATF2', 'MS*FF');
    %placet_element_get_attribute('ATF2',bpms_temp1,'name');
    %placet_element_get_attribute('ATF2',bpms_temp2,'name');
    %placet_element_get_attribute('ATF2',bpms_temp3,'name');
    bpms = sort([bpms_qx'; bpms_qff'; bpms_sff']);
    bpms = bpms(1:end-2)'; % Remove MSF1FF and MSD0FF
    %placet_element_get_attribute('ATF2',bpms,'name')
    nbpm=length(bpms);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Change the BPM resolution according to different options %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if(bpm_change_option == 1)
        % Standard resolution (nothing to change)
    elseif(bpm_change_option == 2)
        % Change of the first three BPMs with the FONT electronics
        placet_element_set_attribute('ATF2', bpms(1),'resolution',bpm_change_resolution);
        placet_element_set_attribute('ATF2', bpms(2),'resolution',bpm_change_resolution);
        placet_element_set_attribute('ATF2', bpms(3),'resolution',bpm_change_resolution);
    elseif(bpm_change_option == 3)
        % Change of the first four BPMs apart from M2X with the FONT electronics
        placet_element_set_attribute('ATF2', bpms(1),'resolution',bpm_change_resolution);
        placet_element_set_attribute('ATF2', bpms(3),'resolution',bpm_change_resolution);
        placet_element_set_attribute('ATF2', bpms(5),'resolution',bpm_change_resolution);
    elseif(bpm_change_option == 4)
        % Change of the first four BPMs apart from M2X with the FONT electronics
        placet_element_set_attribute('ATF2', bpms(1),'resolution',1.0);
        placet_element_set_attribute('ATF2', bpms(2),'resolution',1.0);
        placet_element_set_attribute('ATF2', bpms(3),'resolution',1.0);
        placet_element_set_attribute('ATF2', bpms(4),'resolution',1.0);
        placet_element_set_attribute('ATF2', bpms(5),'resolution',1.0);
        placet_element_set_attribute('ATF2', bpms(6),'resolution',0.7);
        placet_element_set_attribute('ATF2', bpms(7),'resolution',0.7);
        placet_element_set_attribute('ATF2', bpms(8),'resolution',0.7);
        placet_element_set_attribute('ATF2', bpms(9),'resolution',0.4);
        placet_element_set_attribute('ATF2', bpms(13),'resolution',0.4);
        placet_element_set_attribute('ATF2', bpms(14),'resolution',0.4);
        placet_element_set_attribute('ATF2', bpms(15),'resolution',0.4);
    else
        fprintf(1,'Unknown BPM resolution option!');
        exit;
    end
    
    bpm_resol=placet_element_get_attribute("ATF2",bpms,"resolution")*1e-6;
    #bpm_name=placet_element_get_attribute("ATF2",bpms,"name");
    bpm_s=placet_element_get_attribute("ATF2",bpms,"s");
    bpm_length=placet_element_get_attribute("ATF2",bpms,"length");
    bpm_s=bpm_s-bpm_length./2;
    quads=placet_get_number_list("ATF2","quadrupole");
    quads_s=placet_element_get_attribute("ATF2",quads,"s");
    quads_length=placet_element_get_attribute("ATF2",quads,"length");
    quads_s = quads_s - quads_length./2;
    sexts=placet_get_number_list("ATF2","multipole");
    sbends=placet_get_number_list("ATF2","sbend");
    sbends_s=placet_element_get_attribute("ATF2",sbends,"s");
    sbends_length=placet_element_get_attribute("ATF2",sbends,"length");
    sbends_s=sbends_s-sbends_length./2;
    [quads_sbends,index_quads_sbends] = sort([quads,sbends]);
    buffer_data = [quads_s; sbends_s];
    quads_sbends_s = buffer_data(index_quads_sbends);
    
    %placet_element_get_attribute('ATF2',sbends,'name')
    %placet_element_get_attribute('ATF2',quads,'name')
    %pause(3600)
    
    sensor_list = placet_get_name_number_list('ATF2', 'sensor*');
    sensorA_list = placet_get_name_number_list('ATF2', 'sensorA*');
    sensorB_list = placet_get_name_number_list('ATF2', 'sensorB*');
    sensorC_list = placet_get_name_number_list('ATF2', 'sensorC*');
    sens_s=placet_element_get_attribute("ATF2",sensor_list,"s");
    sensA_s=placet_element_get_attribute("ATF2",sensorA_list,"s");
    sensB_s=placet_element_get_attribute("ATF2",sensorB_list,"s");
    #quads_name=[];
    #for i=1:length(quads)
        #        fprintf(1,'Debug name\n');	
        #	quads_name=strvcat(quads_name,placet_element_get_attribute("ATF2",quads(i),"name"));
	#end
	#sexts_name=[];
	#for i=1:length(sexts)
	#	sexts_name=strvcat(sexts_name,placet_element_get_attribute("ATF2",sexts(i),"name"));
	#end
	#sbends_name=[];
	#for i=1:length(sbends)
	#	sbends_name=strvcat(sbends_name,placet_element_get_attribute("ATF2",sbends(i),"name"));#
    #end
    xcors=placet_get_name_number_list('ATF2','zh*x');
    xcors=[xcors, placet_get_name_number_list('ATF2', 'zx*x')];
    xcors=[xcors, placet_get_name_number_list('ATF2', 'zs*x')];
    xcors=[xcors, placet_get_name_number_list('ATF2', 'ZH*X')];
    xcors=[xcors, placet_get_name_number_list('ATF2', 'ZX*X')];
    xcors=[xcors, placet_get_name_number_list('ATF2', 'ZS*X')];
    ycors=placet_get_name_number_list('ATF2','zv*x');
    ycors=[ycors, placet_get_name_number_list('ATF2', 'ZV*X')];
    QM16FF=placet_get_name_number_list('ATF2','QM16FF');
    QM16FF=[QM16FF, placet_get_name_number_list('ATF2','qm16ff')];
    xmovers=quads(quads>=QM16FF(1));
    ymovers=quads(quads>=QM16FF(1));

    quads_name=placet_element_get_attribute("ATF2",quads,"name");
    quads_combined = determine_real_quad_pos(quads,quads_name,5);
    nr_quads_combined = length(quads_combined);
    quads_combined_s = zeros(size(quads_combined));
    quads_combined_s = quads_combined_s';
    for loop_index_combined=1:nr_quads_combined
        quad_combined_s_temp = placet_element_get_attribute('ATF2',quads_combined(loop_index_combined).quad_indices,'s');
        quad_combined_length_temp = placet_element_get_attribute('ATF2',quads_combined(loop_index_combined).quad_indices,'length');
        quad_combined_s_temp = quad_combined_s_temp - quad_combined_length_temp./2;
        quads_combined_s(loop_index_combined) = mean([quad_combined_s_temp(1);quad_combined_s_temp(end)]);
    end
    quads_sbends_name=placet_element_get_attribute("ATF2",quads_sbends,"name");
    quads_sbends_combined = determine_real_quad_pos(quads_sbends,quads_sbends_name,4);
    nr_quads_sbends_combined = length(quads_sbends_combined);
    quads_sbends_combined_s = zeros(size(quads_sbends_combined));
    quads_sbends_combined_s = quads_sbends_combined_s';
    for loop_index_combined=1:nr_quads_sbends_combined
        quad_sbends_combined_s_temp = placet_element_get_attribute('ATF2',quads_sbends_combined(loop_index_combined).quad_indices,'s');
        quad_sbends_combined_length_temp = placet_element_get_attribute('ATF2',quads_sbends_combined(loop_index_combined).quad_indices,'length');
        quad_sbends_combined_s_temp = quad_sbends_combined_s_temp - quad_sbends_combined_length_temp./2;
        quads_sbends_combined_s(loop_index_combined) = mean([quad_sbends_combined_s_temp(1);quad_sbends_combined_s_temp(end)]);
    end
    
    sexts_s=placet_element_get_attribute("ATF2",sexts,"s")';	
    #sbends_s=placet_element_get_attribute("ATF2",sbends,"s")';	
    #quads_name=[];
    #for i=1:length(quads)
	#	quads_name=strvcat(quads_name,placet_element_get_attribute("ATF2",quads(i),"name"));
	#end
	#sexts_name=[];
	#for i=1:length(sexts)
	#	sexts_name=strvcat(sexts_name,placet_element_get_attribute("ATF2",sexts(i),"name"));
	#end
	#sbends_name=[];
	#for i=1:length(sbends)
	#	sbends_name=strvcat(sbends_name,placet_element_get_attribute("ATF2",sbends(i),"name"));
	#end
	nquad=length(quads);
	nsext=length(sexts);
	nsbend=length(sbends);
	bpm_read_x=zeros(nstep,nbpm);
	bpm_read_y=zeros(nstep,nbpm);
	e0=eval(Tcl_GetVar('e0'));
