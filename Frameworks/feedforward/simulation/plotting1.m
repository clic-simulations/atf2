%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluation of results %
%%%%%%%%%%%%%%%%%%%%%%%%%

% Relative error of reconstruction
p(seed,:)=std(residual_reco-effect_delta_GM_jitter_sub,0,2)./std(residual_reco+effect_delta_GM_jitter_sub,0,2);
p_B(seed,:)=std(residual_reco-effect_delta_GM_jitter_sub_B,0,2)./std(residual_reco+effect_delta_GM_jitter_sub_B,0,2);
p_A(seed,:)=std(residual_reco-effect_delta_GM_jitter_sub_A,0,2)./std(residual_reco+effect_delta_GM_jitter_sub_A,0,2);

p_noR(seed,:)=std(residual_reco-effect_delta_GM_jitter_sub_noR,0,2)./std(residual_reco+effect_delta_GM_jitter_sub_noR,0,2);
p_B_noR(seed,:)=std(residual_reco-effect_delta_GM_jitter_sub_B_noR,0,2)./std(residual_reco+effect_delta_GM_jitter_sub_B_noR,0,2);
p_A_noR(seed,:)=std(residual_reco-effect_delta_GM_jitter_sub_A_noR,0,2)./std(residual_reco+effect_delta_GM_jitter_sub_A_noR,0,2);

% Correlation of the two signals
r(seed,:)=diag(corr(residual_reco',effect_delta_GM_jitter_sub'));
r_B(seed,:)=diag(corr(residual_reco',effect_delta_GM_jitter_sub_B'));
r_A(seed,:)=diag(corr(residual_reco',effect_delta_GM_jitter_sub_A'));

r_noR(seed,:)=diag(corr(residual_reco',effect_delta_GM_jitter_sub_noR'));
r_B_noR(seed,:)=diag(corr(residual_reco',effect_delta_GM_jitter_sub_B_noR'));
r_A_noR(seed,:)=diag(corr(residual_reco',effect_delta_GM_jitter_sub_A_noR'));

% Quadrupole position estimate error
quad_ampl_A(seed,:)=std(delta_pos_quad-delta_pos_quad_meas_A,0,2)*1e6;
quad_ampl_B(seed,:)=std(delta_pos_quad-delta_pos_quad_meas_B,0,2)*1e6;
quad_ampl(seed,:)=std(delta_pos_quad,0,2)*1e6;
     

% The averaged amplitude of differential BPM readings due to 
% 1.) full reading without noise
effect_beamjitter(seed,:)=std(delta_bpm_read,0,2);
% 2.) Reading due to real ground motion
effect_GM(seed,:)=std(effect_delta_GM,0,2);
effect_GM_noR(seed,:)=std(effect_delta_GM_noR,0,2);
% 3.) Readings due to real ground motion after jitter reconstruction
effect_GM_jittersub(seed,:)=std(effect_delta_GM_jitter_sub,0,2);  
effect_GM_jittersub_noR(seed,:)=std(effect_delta_GM_jitter_sub_noR,0,2);  
% 4.) Reading after jitter removal 
effect_beamjittersub(seed,:)=std(residual_reco,0,2);  

% Determine the errors of jitter and gm estimation
error_jitter_est(seed,:)=std(jitter_estimate-delta_bpm_read_jitter_nobpmerrors,0,2)./std(delta_bpm_read_jitter_nobpmerrors,0,2);
error_jitter_est2(seed,:)=std(delta_bpm_read_nobpmerrors-delta_bpm_read_jitter_nobpmerrors,0,2)./std(delta_bpm_read_jitter_nobpmerrors,0,2);
error_gm_est(seed,:)=std(effect_delta_GM-delta_bpm_read_gm_nobpmerrors,0,2)./std(delta_bpm_read_gm_nobpmerrors,0,2);
error_gm_jitter_sub_est(seed,:)=std(effect_delta_GM_jitter_sub-delta_bpm_read_gm_nobpmerrors,0,2)./std(delta_bpm_read_gm_nobpmerrors,0,2);
error_gm_residual_est(seed,:)=std(residual_reco-delta_bpm_read_gm_nobpmerrors,0,2)./std(delta_bpm_read_gm_nobpmerrors,0,2);

%%%%%%%%%%%%%%%%%%%%
% Plot the results %
%%%%%%%%%%%%%%%%%%%%


mkdir(plots);
mkdir(data);
text_buffer = sprintf("%s/seeds",plots);
mkdir(text_buffer);
f = figure(1);
set(f,"visible","off");
set (0, "defaultlinelinewidth", 5);
set (0, "defaultlinelinestyle", "-");
set(0, 'defaulttextfontsize', 15);
set(0, 'defaultaxesfontsize', 15);
%set (0, 'linewidth', 10);
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot the residual beam motion, after removing the jitter and %
% the ground motion                                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

plot(std(residual_reco(1:nbpm,:)-effect_delta_GM_jitter_sub(1:nbpm,:),0,2),";residuals - GM (true quad. pos.);",...
   std(residual_reco(1:nbpm,:)-effect_delta_GM_jitter_sub_A(1:nbpm,:),0,2),";residuals - GM (15 sensors);",...
   std(residual_reco(1:nbpm,:)-effect_delta_GM_jitter_sub_B(1:nbpm,:),0,2),";residuals - GM (30 sensors);",...
   std(residual_reco(1:nbpm,:),0,2),";residuals;")
xlabel("BPM number [1]");
ylim([0 5e-6]);
text_buffer = sprintf('horizontal residuals seed %i',seed);
title(text_buffer);
grid on;
text_buffer = sprintf("%s/seeds/residualx-seed_%i.eps", plots, seed);
print(text_buffer,'-color');
hold off;       

plot(std(residual_reco(nbpm+1:2*nbpm,:)-effect_delta_GM_jitter_sub(nbpm+1:2*nbpm,:),0,2),";residuals - GM (true quad. pos.);",...
   std(residual_reco(nbpm+1:2*nbpm,:)-effect_delta_GM_jitter_sub_A(nbpm+1:2*nbpm,:),0,2),";residuals - GM (15 sensors);",...
   std(residual_reco(nbpm+1:2*nbpm,:)-effect_delta_GM_jitter_sub_B(nbpm+1:2*nbpm,:),0,2),";residuals - GM (30 sensors);",...
   std(residual_reco(nbpm+1:2*nbpm,:),0,2),";residuals;")
ylim([0 10e-6]);
xlabel("BPM number [1]");
text_buffer = sprintf("vertical residuals GM subtracted seed %i",seed);
title(text_buffer);
grid on;
text_buffer = sprintf("%s/seeds/residualy-seed_%i.eps", plots,seed);
print(text_buffer,'-color');
hold off;        

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot the relative error of reconstruction and the BPM resolution %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

plot(p(seed,1:nbpm),";true quadrupole positions;",...
   p_B(seed,1:nbpm),";30 sensors;",...
   p_A(seed,1:nbpm),";15 sensors;",...
#   std(residual_reco(1:nbpm,:)-effect_delta_GM_jitter_sub(1:nbpm,:),0,2)./std(residual_reco(1:nbpm,:),0,2)*100,";normalized by residuals (true quad. pos.);",...
#   std(residual_reco(1:nbpm,:)-effect_delta_GM_jitter_sub_B(1:nbpm,:),0,2)./std(residual_reco(1:nbpm,:),0,2)*100,";normalized by residuals (30 sensors);",...
#   std(residual_reco(1:nbpm,:)-effect_delta_GM_jitter_sub_A(1:nbpm,:),0,2)./std(residual_reco(1:nbpm,:),0,2)*100,";normalized by residuals (15 sensors);",...
   sqrt(2).*bpm_resol./(2.*std(effect_delta_GM_jitter_sub(1:nbpm,:),0,2)+sqrt(2).*bpm_resol),";best achievable (resol BPM / (effect GM + resol BPM));")
ylim([0 2])
xlabel("BPM number [1]");
ylabel("p")
grid on;
text_buffer = sprintf("%s/seeds/fraction_residualx-seed_%i.eps",plots,seed);
print(text_buffer,'-color');
hold off;

plot(p(seed,nbpm+1:2*nbpm),";true quadrupole positions;",...
   p_B(seed,nbpm+1:2*nbpm),";30 sensors;",...
   p_A(seed,nbpm+1:2*nbpm),";15 sensors;",...
#   std(residual_reco(nbpm+1:2*nbpm,:)-effect_delta_GM_jitter_sub(nbpm+1:2*nbpm,:),0,2)./
#	 std(residual_reco(nbpm+1:2*nbpm,:),0,2)*100,";normalized by residuals (true quad. pos.);",...
#   std(residual_reco(nbpm+1:2*nbpm,:)-effect_delta_GM_jitter_sub_B(nbpm+1:2*nbpm,:),0,2)./
#	 std(residual_reco(nbpm+1:2*nbpm,:),0,2)*100,";normalized by residuals (30 sensors);",...
#   std(residual_reco(nbpm+1:2*nbpm,:)-effect_delta_GM_jitter_sub_A(nbpm+1:2*nbpm,:),0,2)./
#	 std(residual_reco(nbpm+1:2*nbpm,:),0,2)*100,";normalized by residuals (15 sensors);",...
   sqrt(2).*bpm_resol./(2.*std(effect_delta_GM_jitter_sub(nbpm+1:2*nbpm,:),0,2)+sqrt(2).*bpm_resol),";best achievable (resol BPM / (effect GM + resol BPM));")
ylim([0 2]);
xlabel("BPM number [1]");
ylabel("p")
grid on;
text_buffer = sprintf("%s/seeds/fraction_residualy-seed_%i.eps",plots,seed);
print(text_buffer,'-color');
hold off;        
        

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plotting the difference between real and estimated jitter and gm %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

plot(error_jitter_est(seed,1:nbpm),'-xb');
hold on;
plot(error_jitter_est(seed,nbpm+1:2*nbpm),'-xr');
plot(error_jitter_est2(seed,1:nbpm),'-xc');
plot(error_jitter_est2(seed,nbpm+1:2*nbpm),'-xm');
xlabel("BPM number [1]");
ylabel("relative error [1]")
grid on;
legend('jitter est x', 'jitter est y', 'full data x', 'full data y');
text_buffer = sprintf("%s/seeds/jitter_est_error-seed_%i.eps",plots,seed);
print(text_buffer,'-color');
hold off;        

plot_bpm_index = nbpm+30;
plot(jitter_estimate(plot_bpm_index,:),'-xr');
hold on;
plot(delta_bpm_read_jitter_nobpmerrors(plot_bpm_index,:),'-xb');
plot(delta_bpm_read(plot_bpm_index,:),'-xg');
xlabel("time (1)");
ylabel("jitter [1]")
grid on;
legend('jitter est, BPM 30 y', 'jitter, BPM 30 y', 'full data, BPM 30 y');
text_buffer = sprintf("%s/seeds/jitter_BPM30-seed_%i.eps",plots,seed);
print(text_buffer,'-color');
hold off; 

plot(delta_bpm_read_gm_nobpmerrors(plot_bpm_index,:),'-xk');
hold on;
plot(effect_delta_GM(plot_bpm_index,:),'-xc');
plot(effect_delta_GM_jitter_sub(plot_bpm_index,:),'-xb');
plot(residual_reco(plot_bpm_index,:),'-xm');
xlabel("time (1)");
ylabel("jitter [1]")
grid on;
legend('real gm effect', 'estimated gm effect', 'estimated_gm_effect_jitter_sub','residual_beam_motion');
text_buffer = sprintf("%s/seeds/gm_BPM30-seed_%i.eps",plots,seed);
print(text_buffer,'-color');
hold off; 

#disp(residual_reco(nbpm+50,:)/effect_delta_GM_jitter_sub_A(nbpm+50,:))
##surf(bpm_s,bpm_s,cor(residual_reco(nbpm+1:2*nbpm,:)',effect_delta_GM_jitter_sub_A(nbpm+1:2*nbpm,:)'))
##view(0,90)
##colorbar
#plot(bpm_s,1-diag(cor(residual_reco(nbpm+1:2*nbpm,:)',effect_delta_GM_jitter_sub_A(nbpm+1:2*nbpm,:)')),";1-correlation BPM/GM;",...
#   bpm_s,residual_reco(nbpm+1:2*nbpm,:)/effect_delta_GM_jitter_sub_A(nbpm+1:2*nbpm,:),";ratio;")
#ylim([0 2]);
#text_buffer = sprintf(%s/seeds/testy-seed_%i.eps",plots,seed);
#print(text_buffer,'-color');
#text_buffer = sprintf("correlation seed %i:",seed);
#disp(text_buffer);

#surf(quads_s,bpm_s,cor(delta_bpm_read(1:nbpm,:)',delta_pos_quad(1:nquad,:)'));
#view(0,90)
#colorbar
#xlabel("BPM");
#ylabel("quadrupole")
#text_buffer = sprintf("%s/seeds/correlation_GM_BPMx-seed_%i.eps",plots,seed);
#print(text_buffer,'-color');

#surf(quads_s,bpm_s,cor(delta_bpm_read(nbpm+1:2*nbpm,:)',delta_pos_quad(nquad+1:2*nquad,:)'));
#view(0,90)
#colorbar
#xlabel("BPM")
#ylabel("quadrupole")
#text_buffer = sprintf("%s/seeds/correlation_GM_BPMy-seed_%i.eps",plots,seed);
#print(text_buffer,'-color');

#surf(quads_s,bpm_s,cov(delta_bpm_read(1:nbpm,:)',delta_pos_quad(1:nquad,:)'));
#view(0,90)
#colorbar
#xlabel("BPM");
#ylabel("quadrupole")
#text_buffer = sprintf("%s/seeds/covariance_GM_BPMx-seed_%i.eps",plots,seed);
#print(text_buffer,'-color');

#surf(quads_s,bpm_s,cov(delta_bpm_read(nbpm+1:2*nbpm,:)',delta_pos_quad(nquad+1:2*nquad,:)'));
#view(0,90)
#colorbar
#xlabel("BPM")
#ylabel("quadrupole")
#text_buffer = sprintf("%s/seeds/covariance_GM_BPMy-seed_%i.eps",plots,seed);
#print(text_buffer,'-color');
        
% Calculate the PSD of certain signals 
if(use_freq_analysis == 1)
  perform_freq_analysis_sim;
end

disp("plotting done");
