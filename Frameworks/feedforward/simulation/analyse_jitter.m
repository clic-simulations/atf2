# Octave script to plot the beam size and the jitter from different sources
# The intention is to analyse the ground motion reconstruction scripts of Yves. 
# 
# Juergen Pfingstner
# 27th of January 2014

#################
# Load the data #
#################

# PLACET model
base_address = '/Users/jpfingst/Work/ATF2/Yves_heritage/feedforward/';
twiss_v4 = load([base_address 'twiss_v4.dat']);
twiss_v5 = load([base_address 'twiss_v5_no_sext.dat']);

# LUCRETIA model
model_data_lucr = load([base_address 'lucretia_model.dat']);
beta_lucr_x = model_data_lucr(91:end-2,1);
beta_lucr_y = model_data_lucr(91:end-2,2);
#eta_lucr_x = model_data_lucr(91:end-2,3);
#eta_lucr_y = model_data_lucr(91:end-2,4);
nu_lucr_x = model_data_lucr(91:end-2,5);
nu_lucr_y = model_data_lucr(91:end-2,6);
sigma_lucr_x = model_data_lucr(91:end-2,7);
sigma_lucr_y = model_data_lucr(91:end-2,8);
s_lucr_bpm = model_data_lucr(91:end-2,9)-137.5;
ref_lucr_noise = model_data_lucr(91:end-2,10);

# Created jitter
beam_jitter_sim_x = load([base_address 'beam_jitter_sim_x.dat']);
beam_jitter_sim_y = load([base_address 'beam_jitter_sim_y.dat']);

# Measured jitter 
beam_jitter_meas = load([base_address 'jitter_save_2013June.dat']);

####################
# Process the data #
####################

gamma = 1300/0.51;
emitt_geo_x = 5e-6;
emitt_geo_y = 3e-8;

sigma_v4_x = sqrt(emitt_geo_x./gamma.*twiss_v4(:,6));
sigma_v4_y = sqrt(emitt_geo_y./gamma.*twiss_v4(:,10));
sigma_v5_x = sqrt(emitt_geo_x./gamma.*twiss_v5(:,6));
sigma_v5_y = sqrt(emitt_geo_y./gamma.*twiss_v5(:,10));

#################
# Plot the data #
#################

figure(3);
plot(twiss_v5(:,2), sqrt(twiss_v5(:,6)), 'x-b');
grid on;
hold on;
plot(twiss_v5(:,2), sqrt(twiss_v5(:,10)),'x-r');
%plot(twiss_v4(:,2), sqrt(twiss_v4(:,6)),'x-c');
%plot(twiss_v4(:,2), sqrt(twiss_v4(:,10)),'x-m')
plot(s_lucr_bpm, sqrt(beta_lucr_x),'x-k');
plot(s_lucr_bpm, sqrt(beta_lucr_y),'x-g');
legend('x v5','y v5','x Lucr','y Lucr');
xlabel('s [m]');
ylabel('beta [m]');
hold off;

figure(4);
plot(twiss_v5(:,2), sigma_v5_x.*1e6, 'x-b');
grid on;
hold on;
%plot(twiss_v4(:,2), sigma_v4_x.*1e6,'x-c');
plot(s_lucr_bpm, sigma_lucr_x.*1e6,'x-g');
plot(beam_jitter_sim_x(:,1), beam_jitter_sim_x(:,2).*1e6,'x-r');
plot(beam_jitter_sim_x(:,1), beam_jitter_sim_x(:,3).*1e6,'x-m');
plot(beam_jitter_sim_x(:,1), beam_jitter_sim_x(:,4).*1e6,'x-c');
plot(beam_jitter_sim_x(:,1), beam_jitter_meas(:,1).*1e6,'x-k');
xlabel('s [m]');
ylabel('sigma x [um]');
legend('x v5','x Lucr','x jitter sim');
%axis([60 70]);
hold off;

figure(5);
plot(twiss_v5(:,2), sigma_v5_y.*1e6, 'x-b');
grid on;
hold on;
%plot(twiss_v4(:,2), sigma_v4_y.*1e6,'x-m');
plot(s_lucr_bpm, sigma_lucr_y.*1e6,'x-g');
plot(beam_jitter_sim_y(:,1), beam_jitter_sim_y(:,2).*1e6,'x-r');
plot(beam_jitter_sim_y(:,1), beam_jitter_sim_y(:,3).*1e6,'x-m');
plot(beam_jitter_sim_y(:,1), beam_jitter_sim_y(:,4).*1e6,'x-c');
plot(beam_jitter_sim_x(:,1), beam_jitter_meas(:,2).*1e6,'x-k');
xlabel('s [m]');
ylabel('sigma y [um]');
legend('y v5','y Lucr','y jitter sim');
%axis([60 70]);
hold off;

