function cov_delta_bpm_read_no_jitter=get_GM_cov(R_disp_quad,bpms,Tstep,gm_filter,interp_method,nseed,npulse)
        IEX=placet_get_name_number_list('ATF2', 'IEX');
        IEX=[ IEX, placet_get_name_number_list('ATF2', 'iex')];
        IP=placet_get_name_number_list('ATF2', 'IP');
        IP=[ IP, placet_get_name_number_list('ATF2', 'ip')];
        if (~exist("nseed"))
            nseed=20;
        end
        if (~exist("npulse"))
            npulse=1000;
        end
        %bpms=placet_get_number_list("ATF2","bpm");
        nbpm=length(bpms);
        bpm_s=placet_element_get_attribute("ATF2",bpms,"s");
        quads=placet_get_number_list("ATF2","quadrupole");
        nquad=length(quads);
        quads_s=placet_element_get_attribute("ATF2",quads,"s");
        sensor_list = placet_get_name_number_list('ATF2', 'sensor*');
        sensorA_list = placet_get_name_number_list('ATF2', 'sensorA*');
        sensorB_list = placet_get_name_number_list('ATF2', 'sensorB*');
        sensorC_list = placet_get_name_number_list('ATF2', 'sensorC*');
        sens_s=placet_element_get_attribute("ATF2",sensor_list,"s");
        sensA_s=placet_element_get_attribute("ATF2",sensorA_list,"s");
        sensB_s=placet_element_get_attribute("ATF2",sensorB_list,"s");
        e0=eval(Tcl_GetVar('e0'));
        
        disp("ground_motion cov matrix measurement");
        Tcl_Eval("Zero");
        delta_bpm_read_no_jitter_all=zeros(nbpm*2,0);
        for seed=1:nseed
            fprintf(1,"seed: %i\n",seed);
            seed_factor = 324;
            Tcl_SetVar("seed_factor", seed_factor);
            Tcl_SetVar("seed", seed);
            Tcl_Eval("RandomReset -stream Misalignments -seed [expr $seed*$seed_factor + 1]");
            Tcl_Eval("RandomReset -stream Instrumentation -seed [expr $seed*$seed_factor + 2]");
            Tcl_Eval("RandomReset -stream Groundmotion -seed [expr $seed*$seed_factor + 3]");
            Tcl_Eval("RandomReset -stream Cavity -seed [expr $seed*$seed_factor + 4]");
            Tcl_Eval("RandomReset -stream User -seed [expr $seed*$seed_factor + 5]");
            Tcl_Eval("RandomReset -stream Default -seed [expr $seed*$seed_factor + 6]");
            Tcl_Eval("RandomReset -stream Survey -seed [expr $seed*$seed_factor + 7]");
            Tcl_Eval("RandomReset -stream Select -seed [expr $seed*$seed_factor + 8]");
            Tcl_Eval("RandomReset -stream Radiation -seed [expr $seed*$seed_factor + 9]");
            # Both rand and randn have to be initialised, since they use a different value inside
            rand("state", (seed+11)*seed_factor + [1:625]);
            randn("state", (seed+12)*seed_factor + [1:625]);
            #GM init
            eval_string = sprintf("GroundMotionInit -file ./harm.K.dat -x 1 -y 1 -t_step %g -s_sign 1 -s_abs 1 -last_start 0 -s_start 0 -t_start 0",Tstep);
            Tcl_Eval(eval_string);
            if (gm_filter==1)
                for sensor = sensor_list
                    eval_string = sprintf("AddGMFilter -file ./harm.K.dat -filter_x ./guralp_seismometer_vertical.dat -filter_y ./guralp_seismometer_vertical.dat -start_element %i -end_element %i", sensor, sensor);
               	    Tcl_Eval(eval_string);
                end
            end            

            #determination of the covariance matrix of the GM effect
            pos_quad_no_jitter=zeros(nquad*2,npulse+1);
            pos_bpm_no_jitter=zeros(nbpm*2,npulse+1);
            pos_quad_meas_no_jitter_A=zeros(nquad*2,npulse+1);
            pos_quad_meas_no_jitter_B=zeros(nquad*2,npulse+1);
            pos_bpm_meas_no_jitter_A=zeros(nbpm*2,npulse+1);
            pos_bpm_meas_no_jitter_B=zeros(nbpm*2,npulse+1);
            bpm_read_no_jitter=zeros(nbpm*2,npulse+1);
            delta_pos_quad_no_jitter=zeros(nquad*2,npulse);
            delta_pos_bpm_no_jitter=zeros(nbpm*2,npulse);
            delta_pos_quad_meas_no_jitter_A=zeros(nquad*2,npulse);
            delta_pos_bpm_meas_no_jitter_A=zeros(nbpm*2,npulse);
            delta_pos_quad_meas_no_jitter_B=zeros(nquad*2,npulse);
            delta_pos_bpm_meas_no_jitter_B=zeros(nbpm*2,npulse);
            delta_bpm_read_no_jitter=zeros(nbpm*2,npulse);
            
            for i=0:npulse
                Tcl_Eval("GroundMotion");
                placet_test_no_correction("ATF2", "part","None");
                pos_quad_no_jitter(:,i+1)=[placet_element_get_attribute("ATF2",quads,"x"); placet_element_get_attribute("ATF2",quads,"y")]*1e-6;
                pos_bpm_no_jitter(:,i+1)=[placet_element_get_attribute("ATF2",bpms,"x"); placet_element_get_attribute("ATF2",bpms,"y")]*1e-6;
                pos_quad_meas_no_jitter_A(:,i+1) = [interp1([0; sensA_s], [0; placet_element_get_attribute("ATF2",sensorA_list, 'x')*1e-6], quads_s, interp_method,'extrap');interp1([0; sensA_s], [0; placet_element_get_attribute("ATF2",sensorA_list, 'y')*1e-6], quads_s, interp_method,'extrap')];
                pos_quad_meas_no_jitter_B(:,i+1) = [interp1([0; sensB_s], [0; placet_element_get_attribute("ATF2",sensorB_list, 'x')*1e-6], quads_s, interp_method,'extrap');interp1([0; sensB_s], [0; placet_element_get_attribute("ATF2",sensorB_list, 'y')*1e-6], quads_s, interp_method,'extrap')];
                pos_bpm_meas_no_jitter_A(:,i+1) = [interp1([0; sensA_s], [0; placet_element_get_attribute("ATF2",sensorA_list, 'x')*1e-6], bpm_s, interp_method,'extrap');interp1([0; sensA_s], [0; placet_element_get_attribute("ATF2",sensorA_list, 'y')*1e-6], bpm_s, interp_method,'extrap')];
                pos_bpm_meas_no_jitter_B(:,i+1) = [interp1([0; sensB_s], [0; placet_element_get_attribute("ATF2",sensorB_list, 'x')*1e-6], bpm_s, interp_method,'extrap');interp1([0; sensB_s], [0; placet_element_get_attribute("ATF2",sensorB_list, 'y')*1e-6], bpm_s, interp_method,'extrap')];
                data_buffer = placet_get_bpm_readings("ATF2", bpms,true)*1e-6;
                bpm_read_no_jitter(:,i+1) = [data_buffer(:,1); data_buffer(:,2)];
            end

            delta_pos_quad_no_jitter=pos_quad_no_jitter(:,1:end-1)-pos_quad_no_jitter(:,2:end);
            delta_pos_bpm_no_jitter=pos_bpm_no_jitter(:,1:end-1)-pos_bpm_no_jitter(:,2:end);
            delta_pos_quad_meas_no_jitter_A=pos_quad_meas_no_jitter_A(:,1:end-1)-pos_quad_meas_no_jitter_A(:,2:end);
            delta_pos_bpm_meas_no_jitter_A=pos_bpm_meas_no_jitter_A(:,1:end-1)-pos_bpm_meas_no_jitter_A(:,2:end);
            delta_pos_quad_meas_no_jitter_B=pos_quad_meas_no_jitter_B(:,1:end-1)-pos_quad_meas_no_jitter_B(:,2:end);
            delta_pos_bpm_meas_no_jitter_B=pos_bpm_meas_no_jitter_B(:,1:end-1)-pos_bpm_meas_no_jitter_B(:,2:end);
            delta_bpm_read_no_jitter=bpm_read_no_jitter(:,1:end-1)-bpm_read_no_jitter(:,2:end);
            delta_bpm_read_no_jitter_all=[delta_bpm_read_no_jitter_all delta_bpm_read_no_jitter];
            Tcl_Eval("Zero");
        end
        cov_delta_bpm_read_no_jitter=cov(delta_bpm_read_no_jitter_all');

        effect_delta_GM_no_jitter=(R_disp_quad*delta_pos_quad_no_jitter)-delta_pos_bpm_no_jitter;
        effect_delta_GM_meas_no_jitter_A=(R_disp_quad*delta_pos_quad_meas_no_jitter_A)-delta_pos_bpm_meas_no_jitter_A;
        effect_delta_GM_meas_no_jitter_B=(R_disp_quad*delta_pos_quad_meas_no_jitter_B)-delta_pos_bpm_meas_no_jitter_B;
        
        f = figure(1);
        set(f,"visible","off");
        set (0, "defaultlinelinewidth", 5);
        set (0, "defaultlinelinestyle", "-");
        set(0, 'defaulttextfontsize', 15);
        set(0, 'defaultaxesfontsize', 15);
        
      plot(bpm_s,std(delta_bpm_read_no_jitter(1:nbpm,:),0,2)*1e6,';p.t.p BPM readings variation (no jitter);',...
#             bpm_s,std(effect_delta_GM_no_jitter(1:nbpm,:),0,2)*1e6,';predicted amplitude from quad. p.t.p. positions;',...
#             bpm_s,std(effect_delta_GM_meas_no_jitter_A(1:nbpm,:),0,2)*1e6,';predicted amplitude from GM measurements (15 sensors);',...
#             bpm_s,std(effect_delta_GM_meas_no_jitter_B(1:nbpm,:),0,2)*1e6,';predicted amplitude from GM measurements (30 sensors);',...
             bpm_s,std(effect_delta_GM_no_jitter(1:nbpm,:)- delta_bpm_read_no_jitter(1:nbpm,:),0,2)*1e6,';GM effect determination error (true quad. pos);',...
             bpm_s,std(effect_delta_GM_meas_no_jitter_A(1:nbpm,:)- delta_bpm_read_no_jitter(1:nbpm,:),0,2)*1e6,';GM effect determination error (15 sensors);',...
             bpm_s,std(effect_delta_GM_meas_no_jitter_B(1:nbpm,:)- delta_bpm_read_no_jitter(1:nbpm,:),0,2)*1e6,';GM effect determination error (30 sensors);')
#        title("Ground Motion horizontal effect amplitude and error on evaluation (no beam jitter)")
        ylim([0 5])
        xlabel("longitudinal BPM position (m)");
        ylabel("horizontal amplitude [um]")
        print -deps -solid "plots/GM_effectx.eps"        

        plot(bpm_s,std(delta_bpm_read_no_jitter(nbpm+1:2*nbpm,:),0,2)*1e6,';p.t.p BPM readings variation (no jitter);',...
#             bpm_s,std(effect_delta_GM_no_jitter(nbpm+1:2*nbpm,:),0,2)*1e6,';predicted amplitude from quad. p.t.p. positions;',...
#             bpm_s,std(effect_delta_GM_meas_no_jitter_A(nbpm+1:2*nbpm,:),0,2)*1e6,';predicted amplitude from GM measurements (15 sensors);',...
#             bpm_s,std(effect_delta_GM_meas_no_jitter_B(nbpm+1:2*nbpm,:),0,2)*1e6,';predicted amplitude from GM measurements (30 sensors);',...
             bpm_s,std(effect_delta_GM_no_jitter(nbpm+1:2*nbpm,:)- delta_bpm_read_no_jitter(nbpm+1:2*nbpm,:),0,2)*1e6,';GM determination error (true quad. pos);',...
             bpm_s,std(effect_delta_GM_meas_no_jitter_A(nbpm+1:2*nbpm,:)- delta_bpm_read_no_jitter(nbpm+1:2*nbpm,:),0,2)*1e6,';GM effect determination error (15 sensors);',...
             bpm_s,std(effect_delta_GM_meas_no_jitter_B(nbpm+1:2*nbpm,:)- delta_bpm_read_no_jitter(nbpm+1:2*nbpm,:),0,2)*1e6,';GM effect determination error (30 sensors);')
#        title("Ground Motion vertical effect amplitude and error on evaluation (no beam jitter)")
        xlabel("longitudinal BPM position (m)");
        ylabel("vertical amplitude [um]")
        ylim([0 10])
        print -deps -solid "plots/GM_effecty.eps"        

        surf(cov_delta_bpm_read_no_jitter);
        title("Ground motion effects covariance matrix")
        print -deps -solid "plots/GMcov_matrix.eps"
end
