% Script to analysed the data of the BPM and ground motion correlation 
% study in the frequency space. 
%
% Juergen Pfingstner
% 8th of April 2014

% Calculate the psd of the ground motion sensors to compare it with the 
% results of the python scripts

pkg load signal

[nr_bpm, np] = size(delta_bpm_read_nobpmerrors);
nr_bpm=nr_bpm/2;
[nr_sensor, np] = size(delta_pos_sensor_read);
nr_sensor = nr_sensor/2;
[P_temp, freq_bpm] = pwelch(delta_bpm_read_nobpmerrors(1,:), round(np/4), [], [], 3.12);
P_bpm = zeros(length(P_temp), nr_bpm);
P_bpm_gm = P_bpm;
P_bpm_effect_gm = P_bpm;
P_bpm_sub = P_bpm;
IRMS_bpm = P_bpm;
IRMS_bpm_gm = P_bpm;
IRMS_bpm_effect_gm = P_bpm;
IRMS_bpm_sub = P_bpm;
P_gm = zeros(length(P_temp), nr_sensor);
IRMS_gm = P_gm;

for i = 1:nr_sensor
  [P_temp, ~] = pwelch(delta_pos_sensor_read(end/2+i,:), round(np/4), [], [], 3.12);
  P_gm(:,i) = P_temp;
  IRMS_gm(:,i) = irms(P_temp,freq_bpm);
end

for i = 1:nbpm
    [P_temp, ~] = pwelch(delta_bpm_read_nobpmerrors(end/2+i,:), round(np/4), [], [], 3.12);
    P_bpm(:,i) = P_temp;
    IRMS_bpm(:,i) = irms(P_temp,freq_bpm);

    [P_temp, ~] = pwelch(delta_bpm_read_gm_nobpmerrors(end/2+i,:), round(np/4), [], [], 3.12);
    P_bpm_gm(:,i) = P_temp;
    IRMS_bpm_gm(:,i) = irms(P_temp,freq_bpm);

    [P_temp, ~] = pwelch(effect_delta_GM_jitter_sub(end/2+i,:), round(np/4), [], [], 3.12);
    P_bpm_effect_gm(:,i) = P_temp;
    IRMS_bpm_effect_gm(:,i) = irms(P_temp,freq_bpm);

    [P_temp, ~] = pwelch(residual_reco(end/2+i,:), round(np/4), [], [], 3.12);
    P_bpm_sub(:,i) = P_temp;
    IRMS_bpm_sub(:,i) = irms(P_temp,freq_bpm);
end

figure(111);
loglog(freq_bpm(2:end), P_gm(2:end,1:5),'-b');
grid on;
hold on;
loglog(freq_bpm(2:end), P_gm(2:end,6:10),'-r');
loglog(freq_bpm(2:end), P_gm(2:end,11:15),'-g');
legend('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15');
xlabel('frequency [Hz]');
ylabel('PSD [m^2/Hz]');

figure(211);
loglog(freq_bpm(2:end), IRMS_gm(2:end,1:5),'-b');
grid on;
hold on;
loglog(freq_bpm(2:end), IRMS_gm(2:end,6:10),'-r');
loglog(freq_bpm(2:end), IRMS_gm(2:end,11:15),'-g');
xlabel('frequency [Hz]');
ylabel('IRMS [m]');

figure(112);
loglog(freq_bpm(2:end), P_bpm(2:end,:));
grid on;
hold on;
xlabel('frequency [Hz]');
ylabel('PSD [m^2/Hz]');

figure(212);
loglog(freq_bpm(2:end), IRMS_bpm(2:end,:));
grid on;
hold on;
xlabel('frequency [Hz]');
ylabel('IRMS [m]');

figure(113);
loglog(freq_bpm(2:end), P_bpm_sub(2:end,:));
grid on;
hold on;
xlabel('frequency [Hz]');
ylabel('PSD [m^2/Hz]');

figure(213);
loglog(freq_bpm(2:end), IRMS_bpm_sub(2:end,:));
grid on;
hold on;
xlabel('frequency [Hz]');
ylabel('IRMS [m]');

figure(114);
loglog(freq_bpm(2:end), P_bpm_gm(2:end,:));
grid on;
hold on;
xlabel('frequency [Hz]');
ylabel('PSD [m^2/Hz]');

figure(214);
loglog(freq_bpm(2:end), IRMS_bpm_gm(2:end,:));
grid on;
hold on;
xlabel('frequency [Hz]');
ylabel('IRMS [m]');

figure(115);
loglog(freq_bpm(2:end), P_bpm_effect_gm(2:end,:));
grid on;
hold on;
xlabel('frequency [Hz]');
ylabel('PSD [m^2/Hz]');

figure(215);
loglog(freq_bpm(2:end), IRMS_bpm_effect_gm(2:end,:));
grid on;
hold on;
xlabel('frequency [Hz]');
ylabel('IRMS [m]');
