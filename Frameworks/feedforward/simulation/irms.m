% Function to calculate the integrated rms from a given PSD
%
% Juergen Pfingstner
% 28th April 2014

function [IRMS_w] = irms(PSD_w, f);

delta_f = mean(f(2:end)-f(1:end-1));
IRMS_w = cumsum(PSD_w(end:-1:1))*delta_f;
IRMS_w = sqrt(IRMS_w(end:-1:1));

