# Set random seeds 
Tcl_Eval("RandomReset -stream Misalignments -seed [expr $seed*$seed_factor + 1]");
Tcl_Eval("RandomReset -stream Instrumentation -seed [expr $seed*$seed_factor + 2]");
Tcl_Eval("RandomReset -stream Groundmotion -seed [expr $seed*$seed_factor + 3]");
Tcl_Eval("RandomReset -stream Cavity -seed [expr $seed*$seed_factor + 4]");
Tcl_Eval("RandomReset -stream User -seed [expr $seed*$seed_factor + 5]");
Tcl_Eval("RandomReset -stream Default -seed [expr $seed*$seed_factor + 6]");
Tcl_Eval("RandomReset -stream Survey -seed [expr $seed*$seed_factor + 7]");
Tcl_Eval("RandomReset -stream Select -seed [expr $seed*$seed_factor + 8]");
Tcl_Eval("RandomReset -stream Radiation -seed [expr $seed*$seed_factor + 9]");
# Both rand and randn have to be initialised, since they use a different value inside
rand("state", (seed+11)*seed_factor + [1:625]);
randn("state", (seed+12)*seed_factor + [1:625]);


#initial offset for quads and bpms
		pos_bpms_ini=randn(nbpm,2)*offset_ini;
		placet_element_set_attribute("ATF2",bpms,"x",pos_bpms_ini(:,1));
		placet_element_set_attribute("ATF2",bpms,"y",pos_bpms_ini(:,2));		
		pos_quads_ini=randn(nquad,2)*offset_ini;
		placet_element_set_attribute("ATF2",quads,"x",pos_quads_ini(:,1));
		placet_element_set_attribute("ATF2",quads,"y",pos_quads_ini(:,2));
		pos_sexts_ini=randn(nsext,2)*offset_ini;
		placet_element_set_attribute("ATF2",sexts,"x",pos_sexts_ini(:,1));
		placet_element_set_attribute("ATF2",sexts,"y",pos_sexts_ini(:,2));
		pos_sbends_ini=randn(nsbend,2)*offset_ini;
		placet_element_set_attribute("ATF2",sbends,"x",pos_sbends_ini(:,1));
		placet_element_set_attribute("ATF2",sbends,"y",pos_sbends_ini(:,2));
   
#0% scale errors on BPMS
		scale_bpms=randn(nbpm,2)*scale_error+1;
		placet_element_set_attribute("ATF2",bpms,"scale_x",scale_bpms(:,1));
		placet_element_set_attribute("ATF2",bpms,"scale_y",scale_bpms(:,2));
                
#dK/K=1e-4 for quads   	
                quads_only = placet_get_number_list("ATF2","quadrupole");
		K=placet_element_get_attribute("ATF2",quads_only,"strength")';
		dK_quads_ini=randn(length(quads_only),1).*dk_over_k.*K';
		placet_element_set_attribute("ATF2",quads_only,"strength",dK_quads_ini+K');
	    
#trajectory correction
	disp(" EXT trajectory correction in EXT:")
        bpm_ref=zeros(nbpm,2);
    	placet_test_no_correction("ATF2", "part","None");
        bpm_read=placet_get_bpm_readings("ATF2", bpms,true)*1e-6;
		disp(std(bpm_read(bpms<QM16FF(1),:)));
		for iterEXT=1:5
		    bpm_ok= find(all(abs(bpm_read)>1e-9,2)' & (bpms<QM16FF(1)));
                    [C_x_strength,C_y_strength]=compute_steering_EXT(bpms(bpm_ok),bpm_read(bpm_ok,:),bpm_ref(bpm_ok,:),xcors,ycors,1,1);
                    placet_element_set_attribute("ATF2",xcors,"strength_x",C_x_strength*1.3*1e6);
                    placet_element_set_attribute("ATF2",ycors,"strength_y",C_y_strength*1.3*1e6);
                    placet_test_no_correction("ATF2", "part","None");
                    bpm_read=placet_get_bpm_readings("ATF2", bpms,true)*1e-6;
                    disp(std(bpm_read(bpms<QM16FF(1),:)));
		end
		disp(" EXT+FF trajectory correction in EXT:")
		disp(std(bpm_read));
		for iterEXT=1:5
		    bpm_ok= find(all(abs(bpm_read)>1e-9,2));
                    [C_x_strength,C_y_strength]=compute_steering_EXT(bpms(bpm_ok),bpm_read(bpm_ok,:),bpm_ref(bpm_ok,:),xcors,ycors,1,1);
                    placet_element_set_attribute("ATF2",xcors,"strength_x",C_x_strength*1.3*1e6);
                    placet_element_set_attribute("ATF2",ycors,"strength_y",C_y_strength*1.3*1e6);
                    placet_test_no_correction("ATF2", "part","None");
                    bpm_read=placet_get_bpm_readings("ATF2", bpms,true)*1e-6;
                    disp(std(bpm_read));
		end
		disp(" FF trajectory correction in FF:")
		disp(std(bpm_read(bpms>QM16FF(1),:)));
		for iterFF=1:10
                    bpm_ok= find(all(abs(bpm_read)>1e-9,2)' & (bpms>=QM16FF(1)));
                    [xmover_pos,ymover_pos]=compute_steering_FF(bpms(bpm_ok),bpm_read(bpm_ok,:),bpm_ref(bpm_ok,:),xmovers,ymovers,1,1);			
                    placet_element_set_attribute("ATF2",xmovers,"x",xmover_pos*1e6);
                    placet_element_set_attribute("ATF2",ymovers,"y",ymover_pos*1e6);
                    placet_test_no_correction("ATF2", "part","None");
                    bpm_read=placet_get_bpm_readings("ATF2", bpms,true)*1e-6;
                    disp(std(bpm_read(bpms>=QM16FF(1),:)));
  end
