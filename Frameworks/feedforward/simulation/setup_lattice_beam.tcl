#directories needed
set lattice ../../../Lattices/ATF2/
set scripts ../../../Common/scripts/
set plots offset_ini_$offset_ini-scale_error_$scale_error-dk_$dk_over_k-resolstrip_$stripline-resolcband_$cband-title_$dir_title
if {$stripline != $stripline2} {
    set plots offset_ini_$offset_ini-scale_error_$scale_error-dk_$dk_over_k-resolstrip_$stripline-resolstrip2_$stripline2-resolcband_$cband
}
if {$beamline != "ATF2" } {
    set plots $beamline-$plots
}

file mkdir plots
set data data/$plots
set plots plots/$plots
puts "plot dir :$plots"
puts "params : time0:$time0 Tstep:$Tstep nstep:$nstep seed0:$seed1 nseed:$nseed"

puts "Set beamline"
source $scripts/init.tcl;
set e0 1.3
set e_initial $e0
Girder
ReferencePoint -sense 1
#Drift -length 1000
if { $beamline == "ATF2" } {
    source $lattice/ATF2_v4.0.tcl;
} elseif { $beamline == "ATF2_swapBPMs" } {
    source $lattice/ATF2_v4.0_swapBPMs.tcl;
} elseif { $beamline == "UL" } {
    source $lattice/ATF2_UltraLow_v4.2_noMULTS.tcl;
} elseif { $beamline == "ATF2_10bx" } {
    source $lattice/ATF2_v5.1.tcl;
} elseif {$beamline == "ATF2_4strip2"} {
    source $lattice/ATF2_v4.0_4_stripline2.tcl;
} elseif {$beamline == "ATF2_3strip2"} {
    source $lattice/ATF2_v4.0_3_stripline2.tcl;
} elseif {$beamline == "ATF2_2strip2"} {
    source $lattice/ATF2_v4.0_2_stripline2.tcl;
} elseif {$beamline == "ATF2_1strip2"} {
    source $lattice/ATF2_v4.0_1_stripline2.tcl;
} else {
    puts "lattice unknown, check beamline option"
    exit
}
Drift -length 0
ReferencePoint -sense -1
BeamlineSet -name ATF2

puts "Set beam"
#emitance normalized in e-7 m
set match(emitt_x) [expr 50]
set match(emitt_y) [expr .3]
#beam long in e-6 m
set match(sigma_z) 8000
#energy spread normalized
set match(e_spread) 0.08
set match(phase) 0.0
set charge 2e10
# ATF2_v4 beta in m
#array set match {beta_x 6.54290 alpha_x 0.99638 beta_y  3.23785 alpha_y -1.9183 }
# ATF2_v5 beta in m
#dispersion is missing => pb
array set match {beta_x 8.158262183 alpha_x 0 beta_y 0.7917879269 alpha_y 0 }
# Debug
#array set match {beta_x 6.848560987444 alpha_x 1.108024744392 beta_y 2.935758991906 alpha_y -1.907222941526}
# dEBUG

# Get some beam parameters
set gamma [expr $e0*1000/0.5] 
set emitt_geo_x [expr ${match(emitt_x)}*100/$gamma]
set emitt_geo_y [expr ${match(emitt_y)}*100/$gamma]
set sigma_x [expr sqrt($emitt_geo_x*${match(beta_x)})]
set sigma_y [expr sqrt($emitt_geo_y*${match(beta_y)})]
puts "gamma $gamma, emitt_geo_x $emitt_geo_x, emitt_geo_y $emitt_geo_y, sigma_x $sigma_x, sigma_y $sigma_y"

source $scripts/ilc_beam.tcl;

set n_slice 1
set n 1
set n_total [expr $n * $n_slice]

make_beam_many part $n_slice $n

FirstOrder 1

#delete all misalignements
Zero

set i 0
set seed 0

# Debug
if {$sext==0} {
    Octave {
	#sexts=placet_get_number_list("ATF2","multipole"); # In this case also all other non-linearities are switched off, which is not the case in reality 
	sexts = placet_get_name_number_list("ATF2", "S*");
	#size(sexts)
	sexts_name = placet_element_get_attribute("ATF2",sexts,"name");
	placet_element_set_attribute("ATF2", sexts, "strength", zeros(size(sexts)));
    }
}
# dEBUG

if {$twiss_output==1} {
    make_beam_many twiss_beam 30 30
    puts "calculate twiss functions"
    TwissPlot -beam twiss_beam -file twiss.dat
    PhaseAdvance -beam twiss_beam -file_result phase.dat -file_detail phase_detail.dat 
}
