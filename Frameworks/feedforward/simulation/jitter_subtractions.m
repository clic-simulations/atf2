##########################
# Form differential data #
##########################

delta_param_inj=offset(2:end,[1:4 6])'-offset(1:end-1,[1:4 6])';
delta_bpm_read_nobpmerrors=reshape(readings_nobpmerrors(seed,:,2:end),2*nbpm,nstep) -reshape(readings_nobpmerrors(seed,:,1:end-1),2*nbpm,nstep);
delta_bpm_read=reshape(readings(seed,:,2:end),2*nbpm,nstep) -reshape(readings(seed,:,1:end-1),2*nbpm,nstep);
delta_bpm_read_gm_nobpmerrors=reshape(readings_gm_nobpmerrors(seed,:,2:end),2*nbpm,nstep) -reshape(readings_gm_nobpmerrors(seed,:,1:end-1),2*nbpm,nstep);
delta_bpm_read_jitter_nobpmerrors=reshape(readings_jitter_nobpmerrors(seed,:,2:end),2*nbpm,nstep) -reshape(readings_jitter_nobpmerrors(seed,:,1:end-1),2*nbpm,nstep);

delta_pos_quad=reshape(pos_quad(seed,:,2:end),2*nquad,nstep)-reshape(pos_quad(seed,:,1:end-1),2*nquad,nstep);
delta_pos_quad_meas_A=reshape(pos_quad_meas_A(seed,:,2:end),2*nquad,nstep)-reshape(pos_quad_meas_A(seed,:,1:end-1),2*nquad,nstep);   
delta_pos_quad_meas_B=reshape(pos_quad_meas_B(seed,:,2:end),2*nquad,nstep)-reshape(pos_quad_meas_B(seed,:,1:end-1),2*nquad,nstep);   

delta_pos_quad_combined=reshape(pos_quad_combined(seed,:,2:end),2*nr_quads_combined,nstep)-reshape(pos_quad_combined(seed,:,1:end-1),2*nr_quads_combined,nstep);
delta_pos_quad_combined_meas_A=reshape(pos_quad_combined_meas_A(seed,:,2:end),2*nr_quads_combined,nstep)-reshape(pos_quad_combined_meas_A(seed,:,1:end-1),2*nr_quads_combined,nstep);
delta_pos_quad_combined_meas_B=reshape(pos_quad_combined_meas_B(seed,:,2:end),2*nr_quads_combined,nstep)-reshape(pos_quad_combined_meas_B(seed,:,1:end-1),2*nr_quads_combined,nstep);   

delta_pos_bpm=reshape(pos_bpm(seed,:,2:end),2*nbpm,nstep)-reshape(pos_bpm(seed,:,1:end-1),2*nbpm,nstep);
delta_pos_bpm_meas_A=reshape(pos_bpm_meas_A(seed,:,2:end),2*nbpm,nstep)-reshape(pos_bpm_meas_A(seed,:,1:end-1),2*nbpm,nstep);   
delta_pos_bpm_meas_B=reshape(pos_bpm_meas_B(seed,:,2:end),2*nbpm,nstep)-reshape(pos_bpm_meas_B(seed,:,1:end-1),2*nbpm,nstep);   

delta_pos_sensor_read = reshape(pos_sensor_read(seed,:,2:end),2*length(sensorA_list),nstep)-reshape(pos_sensor_read(seed,:,1:end-1),2*length(sensorA_list),nstep);

########################################################
# Calculat the covariance matrix of the reading:       #
#    cov = diag(bpm_resol^2) + covariance matrix of GM #
########################################################

#cov_tot=2*diag([(bpm_resol).^2; (bpm_resol).^2])+cov_delta_bpm_read_no_jitter;
#cov_noGMcov=2*diag([(bpm_resol).^2; (bpm_resol).^2]);
#cov_nobpmerrors=eye(2*nbpm).*1e-20+cov_delta_bpm_read_no_jitter;   #no bpm err#or <=> resol 0.1nm (should not affect the results)
#cov_nothing=eye(2*nbpm).*1e-20;

######################################################
# Get the initial jitter directions, either from the #
# model or via an SVD of the jitter data             #
######################################################

disp("correct GM motion by jitter sub");
if(jitter_removal_option == 1)
    # Model
    T=TM(:,[1:4 6]);
    delta_param_inj_reco=inv(T'*inv(cov_tot)*T)*T'*inv(cov_tot)*delta_bpm_read;
    delta_param_inj_reco_noGMcov=inv(T'*inv(cov_noGMcov)*T)*T'*inv(cov_noGMcov)*delta_bpm_read;
    delta_param_inj_reco_nobpmerrors=inv(T'*inv(cov_nobpmerrors)*T)*T'*inv(cov_nobpmerrors)*delta_bpm_read_nobpmerrors;
    delta_param_inj_reco_nothing=inv(T'*inv(cov_nothing)*T)*T'*inv(cov_nothing)*delta_bpm_read_nobpmerrors;
    residual_reco=delta_bpm_read-T*delta_param_inj_reco;
    residual_reco_noGMcov=delta_bpm_read-T*delta_param_inj_reco_noGMcov;
    residual_reco_nobpmerrors=delta_bpm_read_nobpmerrors-T*delta_param_inj_reco_nobpmerrors;
    residual_reco_nothing=delta_bpm_read_nobpmerrors-T*delta_param_inj_reco_nothing;
elseif(jitter_removal_option == 2)
    # SVD of jitter data
    [U,S,V]=svd(delta_bpm_read); #U(:,1:5) contains the 5 principal modes (2 betatron oscilation in each plane and dispersive mode).
    nmode=5 #number of modes to remove from measurements
    T=U(:,1:nmode)*S(1:nmode,1:nmode);
    delta_param_inj_reco=inv(T'*inv(cov_tot)*T)*T'*inv(cov_tot)*delta_bpm_read;
    delta_param_inj_reco_noGMcov=inv(T'*inv(cov_noGMcov)*T)*T'*inv(cov_noGMcov)*delta_bpm_read;
    delta_param_inj_reco_nobpmerrors=inv(T'*inv(cov_nobpmerrors)*T)*T'*inv(cov_nobpmerrors)*delta_bpm_read_nobpmerrors;
    delta_param_inj_reco_nothing=inv(T'*inv(cov_nothing)*T)*T'*inv(cov_nothing)*delta_bpm_read_nobpmerrors;
    residual_reco=delta_bpm_read-T*delta_param_inj_reco;
    residual_reco_noGMcov=delta_bpm_read-T*delta_param_inj_reco_noGMcov;
    residual_reco_nobpmerrors=delta_bpm_read_nobpmerrors-T*delta_param_inj_reco_nobpmerrors;
    residual_reco_nothing=delta_bpm_read_nobpmerrors-T*delta_param_inj_reco_nothing;
elseif(jitter_removal_option == 3)
    # Removal with correlation matrix
    #index_upstream_bpms = [10:12, nbpm+(10:12)];
    #index_upstream_bpms = [10:12, 16:17, nbpm+(10:12), nbpm+(16:17)];
    #index_upstream_bpms = [16:17, nbpm+(16:17)];
    #index_upstream_bpms = [nbpm+(10:18)];
    #index_upstream_bpms = [1:15, nbpm+(1:15)];
    #index_upstream_bpms = [1,2,3,4,5, nbpm+[1,2,3,4,5]];
    index_upstream_bpms = [1,3,5,nbpm+[1,3,5]];
    #index_upstream_bpms = [1,3,5,10,11,12,nbpm+[1,3,5,10,11,12]];
    #index_upstream_bpms = [1,3,5,10,11,12,16,17,nbpm+[1,3,5,10,11,12,16,17]];
    #index_upstream_bpms = [6,7,8,10,11,12,16,17, nbpm+[6,7,8,10,11,12,16,17]];
    #index_upstream_bpms = [10,11,12,16,17, nbpm+[10,11,12,16,17]];
    #index_upstream_bpms = [1:17,nbpm+[1:17]];
    index_downstream_bpms = 1:2*nbpm;
    index_downstream_bpms(index_upstream_bpms) = [];
    BPM_upstream = delta_bpm_read(index_upstream_bpms,:);
    BPM_downstream = delta_bpm_read(index_downstream_bpms,:);
    nr_bpms_up = length(index_upstream_bpms);
    nr_bpms_down = length(index_downstream_bpms);
    #BPM_corr = BPM_downstream*(inv(BPM_upstream'*BPM_upstream)*BPM_upstream');
    sv_tolerance = 1e-6;
    BPM_corr = BPM_downstream*pinv(BPM_upstream,sv_tolerance);
    BPM_corr_x = BPM_downstream(1:nr_bpms_down/2,:)*pinv(BPM_upstream(1:(nr_bpms_up/2),:));
    BPM_corr_y = BPM_downstream((nr_bpms_down/2+1):nr_bpms_down,:)*pinv(BPM_upstream((nr_bpms_up/2+1):nr_bpms_up,:));
    BPM_corr_uncoupled = [BPM_corr_x,zeros(size(BPM_corr_x));zeros(size(BPM_corr_y)),BPM_corr_y];
    #[BPM_corr, sigma, residual_temp] = ols(BPM_downstream', BPM_upstream');
    #BPM_corr = BPM_corr';
    #BPM_corr1 = zeros(length(index_downstream_bpms),length(index_upstream_bpms));
    #BPM_corr1_uncoupled = zeros(length(index_downstream_bpms),length(index_upstream_bpms));
    #for loop_corr_calc = 1:length(index_downstream_bpms)
    #    # This is somehow useless since the diffent BPM resolutions 
    #    # cannot be included in the calculation, because the measurement
    #    # noise always referes to the same BPM. The different noise
    #    # levels actually are related to the input values. This can 
    #    # be considered with the plane fitting algorithm!
    #    #[BPM_corr, sigma, residual_temp] = ols(BPM_downstream', BPM_upstream');
    #    Cov_plain_fit = cov([BPM_downstream(loop_corr_calc,:)',BPM_upstream']);
    #    [V,lambda] = eig(Cov_plain_fit);
    #    [lambda,id] = sort(-abs(diag(lambda)));
    #    V = V(:,id);
    #    BPM_corr1(loop_corr_calc,:) = -V(2:end,end)'/V(1,end);   
    #    if(loop_corr_calc<=(nr_bpms_down/2))
    #        % Estimation in x
    #        Cov_plain_fit = cov([BPM_downstream(loop_corr_calc,:)',BPM_upstream(1:nr_bpms_up/2,:)']);
    #        [V,lambda] = eig(Cov_plain_fit);
    #        [lambda,id] = sort(-abs(diag(lambda)));
    #        V = V(:,id);
    #        BPM_corr1_uncoupled(loop_corr_calc,1:nr_bpms_up/2) = -V(2:end,end)'/V(1,end);
    #    else
    #        % Estimation in y 
    #        Cov_plain_fit = cov([BPM_downstream(loop_corr_calc,:)',BPM_upstream((nr_bpms_up/2+1):nr_bpms_up,:)']);
    #        [V,lambda] = eig(Cov_plain_fit);
    #        [lambda,id] = sort(-abs(diag(lambda)));
    #        V = V(:,id);
    #        BPM_corr1_uncoupled(loop_corr_calc,(nr_bpms_up/2+1):nr_bpms_up) = -V(2:end,end)'/V(1,end);
    #    end
    #end
    #cov_tot_downstream = cov_tot(index_downstream_bpms,index_downstream_bpms);
    residual_reco = delta_bpm_read;
    residual_reco(index_downstream_bpms,:) = BPM_downstream - BPM_corr*BPM_upstream;
    jitter_estimate = delta_bpm_read;
    jitter_estimate(index_downstream_bpms,:) = BPM_downstream-residual_reco(index_downstream_bpms,:);
    residual_reco_noGMcov = residual_reco;

    BPM_upstream_nobpmerrors = delta_bpm_read_nobpmerrors(index_upstream_bpms,:);
    BPM_downstream_nobpmerrors = delta_bpm_read_nobpmerrors(index_downstream_bpms,:);
    #BPM_corr_nobpmerrors = BPM_downstream_nobpmerrors*(inv(BPM_upstream_nobpmerrors'*BPM_upstream_nobpmerrors)*BPM_upstream_nobpmerrors');
    BPM_corr_nobpmerrors = BPM_downstream_nobpmerrors*pinv(BPM_upstream_nobpmerrors);
    #BPM_corr_nobpmerrors1 = zeros(length(index_downstream_bpms),length(index_upstream_bpms));
    #for loop_corr_calc = 1:length(index_downstream_bpms)
    #    Cov_plain_fit = cov([BPM_downstream_nobpmerrors(loop_corr_calc,:)',BPM_upstream_nobpmerrors']);
    #    # Only y 
    #    [V,lambda] = eig(Cov_plain_fit);
    #    [lambda,id] = sort(-abs(diag(lambda)));
    #    V = V(:,id);
    #    BPM_corr_nobpmerrors1(loop_corr_calc,:) = -V(2:end,end)'/V(1,end);
    #end
    residual_reco_nobpmerrors = delta_bpm_read_nobpmerrors;
    residual_reco_nobpmerrors(index_downstream_bpms,:) = BPM_downstream_nobpmerrors - BPM_corr_nobpmerrors*BPM_upstream_nobpmerrors;
    residual_reco_nothing = residual_reco_nobpmerrors;
else
    # No jitter removal
    residual_reco=delta_bpm_read;
    residual_reco_noGMcov=delta_bpm_read;
    residual_reco_nobpmerrors=delta_bpm_read_nobpmerrors;
    residual_reco_nothing=delta_bpm_read_nobpmerrors;
end

############################################################
# Removing of beam jitter using the propagation directions #
############################################################

##########################################################################
# Compute effect of GM minus residual beam motion: beam motion due to GM #
##########################################################################
        
# Here perfect system knowledge is assumed in R_disp_quad!!!
effect_delta_GM=R_disp_quad*delta_pos_quad-delta_pos_bpm;
effect_delta_GM_A=R_disp_quad*delta_pos_quad_meas_A-delta_pos_bpm_meas_A;
effect_delta_GM_B=R_disp_quad*delta_pos_quad_meas_B-delta_pos_bpm_meas_B;

effect_delta_GM_combined = R_model_combined*delta_pos_quad_combined-delta_pos_bpm;
effect_delta_GM_A_combined = R_model_combined*delta_pos_quad_combined_meas_A-delta_pos_bpm_meas_A;
effect_delta_GM_B_combined = R_model_combined*delta_pos_quad_combined_meas_B-delta_pos_bpm_meas_B;

%index_reduced_quads = 1:2*nr_quads_combined;
%index_reduced_quads([1,2,3,37,nr_quads_combined+[1,2,3,37]]) = [];
%effect_delta_GM_combined_reduced = R_model_combined(:,index_reduced_quads)*delta_pos_quad_combined(index_reduced_quads,:)-delta_pos_bpm;
%effect_delta_GM_A_combined_reduced = R_model_combined(:,index_reduced_quads)*delta_pos_quad_combined_meas_A(index_reduced_quads,:)-delta_pos_bpm_meas_A;
%effect_delta_GM_B_combined_reduced = R_model_combined(:,index_reduced_quads)*delta_pos_quad_combined_meas_B(index_reduced_quads,:)-delta_pos_bpm_meas_B;

% Debug
%norm(effect_delta_GM-effect_delta_GM,'fro')/norm(effect_delta_GM,'fro')
%norm(effect_delta_GM-effect_delta_GM_A,'fro')/norm(effect_delta_GM,'fro')
%norm(effect_delta_GM-effect_delta_GM_B,'fro')/norm(effect_delta_GM,'fro')

%norm(effect_delta_GM-effect_delta_GM_A_combined,'fro')/norm(effect_delta_GM,'fro')
%norm(effect_delta_GM-effect_delta_GM_B_combined,'fro')/norm(effect_delta_GM,'fro')

%norm(effect_delta_GM-effect_delta_GM_combined,'fro')/norm(effect_delta_GM,'fro')
%norm(effect_delta_GM_A-effect_delta_GM_A_combined,'fro')/norm(effect_delta_GM,'fro')
%norm(effect_delta_GM_B-effect_delta_GM_B_combined,'fro')/norm(effect_delta_GM,'fro')
%norm(effect_delta_GM-effect_delta_GM_combined_reduced,'fro')/norm(effect_delta_GM,'fro')
%norm(effect_delta_GM_A-effect_delta_GM_A_combined_reduced,'fro')/norm(effect_delta_GM,'fro')
%norm(effect_delta_GM_B-effect_delta_GM_B_combined_reduced,'fro')/norm(effect_delta_GM,'fro')
%%pause(3600)
% dEBUG

# Here R_disp_quad is estimated from the data
#R_disp_quad_est = *pinv(delta_pos_quad);
#R_disp_quad_A_est = *pinv(delta_pos_quad_meas_A);
#R_disp_quad_B_est = *pinv(delta_pos_quad_meas_B);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Here we try to estimate R from the data instead of using the     %
% modeled one. The result was not satisfying. Even though the      %
% reconstruction of R works fine if the perfect data are used, the %
% jitter as well as the ground motion interpolation cause the      %
% estimated matrix to be very different from the real matrix. Even %
% though the P-value is much smaller, this is not satisfying. We   %
% will use instead the model resonse matrix and hope that it is    %
% correct.                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Mask matrix
mean_R_model_combined = mean(abs(R_model_combined));
treshold = 1e-2;
R_mask = (abs(R_model_combined))>(ones(size(R_model_combined)).*(mean_R_model_combined*treshold));

R_disp_quad_combined_est = zeros(nbpm*2,nr_quads_combined*2);
R_disp_quad_combined_A_est = zeros(nbpm*2,nr_quads_combined*2);
R_disp_quad_combined_B_est = zeros(nbpm*2,nr_quads_combined*2);
R_disp_quad_gm_combined_est = zeros(nbpm*2,nr_quads_combined*2);
R_disp_quad_gm_combined_A_est = zeros(nbpm*2,nr_quads_combined*2);
R_disp_quad_gm_combined_B_est = zeros(nbpm*2,nr_quads_combined*2);

% Method 1: does not really work
%R_disp_quad_combined_est = residual_reco*pinv(delta_pos_quad_combined);
%R_disp_quad_combined_A_est = residual_reco*pinv(delta_pos_quad_combined_meas_A);
%R_disp_quad_combined_B_est = residual_reco*pinv(delta_pos_quad_combined_meas_B);
%R_disp_quad_gm_combined_est = delta_bpm_read_gm_nobpmerrors*pinv(delta_pos_quad_combined);
%R_disp_quad_gm_combined_A_est = delta_bpm_read_gm_nobpmerrors*pinv(delta_pos_quad_combined_meas_A);
%R_disp_quad_gm_combined_B_est = delta_bpm_read_gm_nobpmerrors*pinv(delta_pos_quad_combined_meas_B);

% Method 2: let's see
pinv_tol = 1e-12;
% For the first three quadrupoles the estimation of the ground
% motion with the sensors is very bad. I leave them away since they
% do not contribute to the overall ground motion much. 
R_mask(:,[1,2,3,nr_quads_combined+[1,2,3]]) = zeros(size(R_mask(:,[1,2,3,nr_quads_combined+[1,2,3]])));
for loop_R_index=1:(nbpm*2)
    indices_of_importance = find(R_mask(loop_R_index,:));
    if(isempty(indices_of_importance)==1)
        % The elements all stay zero. So there is nothing to do
    else     
        R_disp_quad_combined_est(loop_R_index,indices_of_importance) = (residual_reco(loop_R_index,:)-delta_pos_bpm(loop_R_index,:))*pinv(delta_pos_quad_combined(indices_of_importance,:),pinv_tol);
        R_disp_quad_combined_A_est(loop_R_index,indices_of_importance) = (residual_reco(loop_R_index,:)-delta_pos_bpm_meas_A(loop_R_index,:))*pinv(delta_pos_quad_combined_meas_A(indices_of_importance,:),pinv_tol);
        R_disp_quad_combined_B_est(loop_R_index,indices_of_importance) = (residual_reco(loop_R_index,:)-delta_pos_bpm_meas_B(loop_R_index,:))*pinv(delta_pos_quad_combined_meas_B(indices_of_importance,:),pinv_tol);

        R_disp_quad_gm_combined_est(loop_R_index,indices_of_importance) = (delta_bpm_read_gm_nobpmerrors(loop_R_index,:)+delta_pos_bpm(loop_R_index,:))*pinv(delta_pos_quad_combined(indices_of_importance,:),pinv_tol);
        %R_disp_quad_gm_combined_est(loop_R_index,indices_of_importance) = (effect_delta_GM_combined(loop_R_index,:)+delta_pos_bpm(loop_R_index,:))*pinv(delta_pos_quad_combined(indices_of_importance,:),pinv_tol);
        R_disp_quad_gm_combined_A_est(loop_R_index,indices_of_importance) =(delta_bpm_read_gm_nobpmerrors(loop_R_index,:)+delta_pos_bpm_meas_A(loop_R_index,:))*pinv(delta_pos_quad_combined_meas_A(indices_of_importance,:),pinv_tol);
        R_disp_quad_gm_combined_B_est(loop_R_index,indices_of_importance) = (delta_bpm_read_gm_nobpmerrors(loop_R_index,:)+delta_pos_bpm_meas_B(loop_R_index,:))*pinv(delta_pos_quad_combined_meas_B(indices_of_importance,:),pinv_tol);
    end
end

%% Debug
%index_upstream_v1_bpms = [1:17, nbpm+(1:17)];
%index_downstream_v1_bpms = 1:2*nbpm;
%index_downstream_v1_bpms(index_upstream_v1_bpms) = [];
%
%norm(delta_bpm_read(index_downstream_v1_bpms,:)- delta_bpm_read_gm_nobpmerrors(index_downstream_v1_bpms,:))./norm(delta_bpm_read_gm_nobpmerrors(index_downstream_v1_bpms,:))
%norm(effect_delta_GM_combined(index_downstream_v1_bpms,:) - delta_bpm_read_gm_nobpmerrors(index_downstream_v1_bpms,:))./norm(delta_bpm_read_gm_nobpmerrors(index_downstream_v1_bpms,:))
%norm(residual_reco_nobpmerrors(index_downstream_v1_bpms,:) - delta_bpm_read_gm_nobpmerrors(index_downstream_v1_bpms,:))./norm(delta_bpm_read_gm_nobpmerrors(index_downstream_v1_bpms,:))
%disp('----');
%
%error_R_bpm = zeros(nbpm*2,1);
%for loop_R_index=1:(nbpm*2)
%    error_R_bpm(loop_R_index)=norm(R_disp_quad_gm_combined_A_est(loop_R_index,:)-R_model_combined(loop_R_index,:),'fro')./norm(R_model_combined(loop_R_index,:),'fro');
%end
%
%error_R_quad = zeros(nr_quads_combined*2,1);
%for loop_R_index=1:(nr_quads_combined*2)
%    error_R_quad(loop_R_index) = norm(R_disp_quad_gm_combined_A_est(:,loop_R_index)-R_model_combined(:,loop_R_index),'fro')./norm(R_model_combined,'fro');
%end
%
%error_bpm_gm = zeros(nbpm*2,1);
%for loop_R_index=1:(nbpm*2)
%    error_bpm_gm(loop_R_index)=norm(delta_bpm_read_gm_nobpmerrors(loop_R_index,:)-effect_delta_GM_combined(loop_R_index,:),'fro')./norm(effect_delta_GM_combined(loop_R_index,:),'fro');
%end
%
%figure(1);
%plot(error_R_bpm(1:nbpm),'-xb');
%grid on;
%hold on
%plot(error_R_bpm(nbpm+1:2*nbpm),'-xr');
%title('Error BPMs per row');
%legend('x','y');
%axis([0 45, 0 2]);
%hold off;
%
%figure(2);
%plot(error_R_quad(1:nr_quads_combined),'-xb');
%grid on;
%hold on
%plot(error_R_quad(nr_quads_combined+1:2*nr_quads_combined),'-xr');
%title('Error Quads per row');
%legend('x','y');
%axis([0 60, 0 1]);
%hold off;
%
%figure(3);
%plot(error_bpm_gm(1:nbpm),'-xb');
%grid on;
%hold on
%plot(error_bpm_gm(nbpm+1:2*nbpm),'-xr');
%title('Error BPMs GM');
%legend('x','y');
%%axis([0 45, 0 2]);
%hold off;
%
%temp = length(index_downstream_v1_bpms);
%mean(error_R_bpm(index_downstream_v1_bpms(1:(temp/2))))
%mean(error_R_bpm(index_downstream_v1_bpms((1+(temp/2)):temp)))
%sum(error_R_quad(1:nr_quads_combined))
%sum(error_R_quad(1+nr_quads_combined:2*nr_quads_combined))
%
%disp('Estimation of R from data:');
%#R_disp_quad_downstream = R_disp_quad(index_downstream_bpms,:);
%#R_disp_quad_downstream = R_disp_quad;
%norm(R_disp_quad_combined_est-R_model_combined,'fro')/norm(R_model_combined,'fro')
%norm(R_disp_quad_combined_A_est-R_model_combined,'fro')/norm(R_model_combined,'fro')
%norm(R_disp_quad_combined_B_est-R_model_combined,'fro')/norm(R_model_combined,'fro')%
%
%norm(R_disp_quad_gm_combined_est(index_downstream_v1_bpms,:)-R_model_combined(index_downstream_v1_bpms,:),'fro')/norm(R_model_combined(index_downstream_v1_bpms,:),'fro')
%norm(R_disp_quad_gm_combined_A_est(index_downstream_v1_bpms,:)-R_model_combined(index_downstream_v1_bpms,:),'fro')/norm(R_model_combined(index_downstream_v1_bpms,:),'fro')
%norm(R_disp_quad_gm_combined_B_est(index_downstream_v1_bpms,:)-R_model_combined(index_downstream_v1_bpms,:),'fro')/norm(R_model_combined(index_downstream_v1_bpms,:),'fro')
%norm(delta_bpm_read_gm_nobpmerrors-effect_delta_GM,'fro')/norm(delta_bpm_read_gm_nobpmerrors,'fro')
%norm(delta_bpm_read_gm_nobpmerrors-effect_delta_GM_A,'fro')/norm(delta_bpm_read_gm_nobpmerrors,'fro')
%norm(delta_bpm_read_gm_nobpmerrors-effect_delta_GM_B,'fro')/norm(delta_bpm_read_gm_nobpmerrors,'fro')
%norm(delta_bpm_read_gm_nobpmerrors-effect_delta_GM_combined,'fro')/norm(delta_bpm_read_gm_nobpmerrors,'fro')
%norm(delta_bpm_read_gm_nobpmerrors-effect_delta_GM_A_combined,'fro')/norm(delta_bpm_read_gm_nobpmerrors,'fro')
%norm(delta_bpm_read_gm_nobpmerrors-effect_delta_GM_B_combined,'fro')/norm(delta_bpm_read_gm_nobpmerrors,'fro')
%% dEBUG

effect_delta_GM_noR=R_disp_quad_combined_est*delta_pos_quad_combined-delta_pos_bpm;
effect_delta_GM_A_noR=R_disp_quad_combined_A_est*delta_pos_quad_combined_meas_A-delta_pos_bpm_meas_A;
effect_delta_GM_B_noR=R_disp_quad_combined_B_est*delta_pos_quad_combined_meas_B-delta_pos_bpm_meas_B;
%effect_delta_GM_noR=R_disp_quad_gm_combined_est*delta_pos_quad_combined-delta_pos_bpm;
%effect_delta_GM_A_noR=R_disp_quad_gm_combined_A_est*delta_pos_quad_combined_meas_A-delta_pos_bpm_meas_A;
%effect_delta_GM_B_noR=R_disp_quad_gm_combined_B_est*delta_pos_quad_combined_meas_B-delta_pos_bpm_meas_B;

if((jitter_removal_option == 1) || (jitter_removal_option == 2))
    # Via model response matrix or SVD of data
    effect_delta_GM_jitter_sub=effect_delta_GM-T*inv(T'*inv(cov_tot)*T)*T'*inv(cov_tot)*effect_delta_GM;
    effect_delta_GM_jitter_sub_A=effect_delta_GM_A-T*inv(T'*inv(cov_tot)*T)*T'*inv(cov_tot)*effect_delta_GM_A;
    effect_delta_GM_jitter_sub_B=effect_delta_GM_B-T*inv(T'*inv(cov_tot)*T)*T'*inv(cov_tot)*effect_delta_GM_B;
    #effect_delta_GM_jitter_sub_noGMcov=effect_delta_GM-T*inv(T'*inv(cov_noGMcov)*T)*T'*inv(cov_noGMcov)*effect_delta_GM;
    #effect_delta_GM_jitter_sub_nobpmerrors=effect_delta_GM-T*inv(T'*inv(cov_nobpmerrors)*T)*T'*inv(cov_nobpmerrors)*effect_delta_GM;
    #effect_delta_GM_jitter_sub_nothing=effect_delta_GM-T*inv(T'*inv(cov_nothing)*T)*T'*inv(cov_nothing)*effect_delta_GM;
elseif((jitter_removal_option == 3))
    # Via correlation matrix
    jitter_modification = pinv(BPM_upstream,sv_tolerance)*BPM_upstream; 
    jitter_modification = eye(size(jitter_modification)) - jitter_modification;
    jitter_modification = 1;
    
    effect_delta_GM_jitter_sub = effect_delta_GM;
    effect_delta_GM_jitter_sub(index_downstream_bpms,:)=effect_delta_GM(index_downstream_bpms,:)*jitter_modification;
    
    effect_delta_GM_jitter_sub_A = effect_delta_GM_A;
    effect_delta_GM_jitter_sub_A(index_downstream_bpms,:)=effect_delta_GM_A(index_downstream_bpms,:)*jitter_modification;

    effect_delta_GM_jitter_sub_B = effect_delta_GM_B;
    effect_delta_GM_jitter_sub_B(index_downstream_bpms,:)=effect_delta_GM_B(index_downstream_bpms,:)*jitter_modification;
    
    effect_delta_GM_jitter_sub_noR = effect_delta_GM_noR;
    effect_delta_GM_jitter_sub_noR(index_downstream_bpms,:)=effect_delta_GM_noR(index_downstream_bpms,:)*jitter_modification;
    
    effect_delta_GM_jitter_sub_A_noR = effect_delta_GM_A_noR;
    effect_delta_GM_jitter_sub_A_noR(index_downstream_bpms,:)=effect_delta_GM_A_noR(index_downstream_bpms,:)*jitter_modification;

    effect_delta_GM_jitter_sub_B_noR = effect_delta_GM_B_noR;
    effect_delta_GM_jitter_sub_B_noR(index_downstream_bpms,:)=effect_delta_GM_B_noR(index_downstream_bpms,:)*jitter_modification;
    
else
    # No jitter removal
    effect_delta_GM_jitter_sub=effect_delta_GM;
    effect_delta_GM_jitter_sub_A=effect_delta_GM_A;
    effect_delta_GM_jitter_sub_B=effect_delta_GM_B;
    effect_delta_GM_jitter_sub_noR=effect_delta_GM;
    effect_delta_GM_jitter_sub_A_noR=effect_delta_GM_A;
    effect_delta_GM_jitter_sub_B_noR=effect_delta_GM_B;
end

% Save some data for the PSD comparision

time_vector=([0:nstep]*Tstep)';

if(save_additional_data == 1)
    temp_data = reshape(pos_quad_combined(seed,:,1:end-1),2*nr_quads_combined,nstep);
    save_data = temp_data(1:end/2,:);
    save('./data/ground_motion_x_sim.dat', 'save_data','-ascii');
    save_data = temp_data((end/2+1):end,:);
    save('./data/ground_motion_y_sim.dat', 'save_data','-ascii');
    save('./data/ground_motion_t_sim.dat', 'time_vector','-ascii');

    save_data = delta_pos_quad_combined(1:end/2,:);
    save('./data/delta_ground_motion_x_sim.dat', 'save_data','-ascii');
    save_data = delta_pos_quad_combined((end/2+1):end,:);
    save('./data/delta_ground_motion_y_sim.dat', 'save_data','-ascii');
    save('./data/delta_ground_motion_t_sim.dat', 'time_vector','-ascii');

    save_data = effect_delta_GM_jitter_sub(1:end/2,:);
    save('./data/ground_motion_effect_x_sim.dat', 'save_data','-ascii');
    save_data = effect_delta_GM_jitter_sub((end/2+1):end,:);
    save('./data/ground_motion_effect_y_sim.dat', 'save_data','-ascii');
    save('./data/ground_motion_effect_t_sim.dat', 'time_vector','-ascii');

    save_data = residual_reco(1:end/2,:);
    save('./data/bpm_motion_sub_x_sim.dat', 'save_data','-ascii');
    save_data = residual_reco((end/2+1):end,:);
    save('./data/bpm_motion_sub_y_sim.dat', 'save_data','-ascii');
    save('./data/bpm_motion_sub_t_sim.dat', 'time_vector','-ascii');
    
    save_data = delta_bpm_read_nobpmerrors(1:end/2,:);
    save('./data/bpm_motion_x_sim.dat', 'save_data','-ascii');
    save_data = delta_bpm_read_nobpmerrors((end/2+1):end,:);
    save('./data/bpm_motion_y_sim.dat', 'save_data','-ascii');
    save('./data/bpm_motion_t_sim.dat', 'time_vector','-ascii');
    
    save_data = delta_pos_sensor_read(1:end/2,:);
    save('./data/sensor_gm_x_sim.dat', 'save_data','-ascii');
    save_data = delta_pos_sensor_read((end/2+1):end,:);
    save('./data/sensor_gm_y_sim.dat', 'save_data','-ascii');
    save('./data/sensor_gm_t_sim.dat', 'time_vector','-ascii');
end
