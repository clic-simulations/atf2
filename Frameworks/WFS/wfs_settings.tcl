set nr_time_steps 12

set dir_name "simplewfs"

set use_misalignment 1
set alignment_qp 1
set alignment_bpm 1
set alignment_drift 1
set alignment_sigma 30 ;#[um]

set cband 0.2 ;#[um]

# store true readings or use resolution
set bpm_noise 0

set use_wfs 1
set use_dfs 1
# nr of steering iterations
set steering_iter 3
# beta parameter, cuts on singular values (the higher, the harder the cut)
set steering_beta 6

set charge 0.8e10
set wfs_dcharge [expr 0.5/0.8]
set wfs_weight [expr round(sqrt(($cband * $cband + $alignment_sigma * $alignment_sigma) / (2 * $cband * $cband)))]

set dfs_dE 0.995
set dfs_weight [expr round(sqrt(($cband * $cband + $alignment_sigma * $alignment_sigma) / (2 * $cband * $cband)))]

set steering_select_bpms 0

#set response_matrix_file "../ResponseMatrices/Response.dat"
set response_matrix_file "../ATF2_studies/ResponseMatrices/Response.dat"

