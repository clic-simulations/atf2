source simple_settings.tcl

source simple_setup.tcl

############
# Tracking #
############

DispersionFreeEmittance -on

Octave {
    [Emitt,Beam]=placet_test_no_correction("${beamline_name}", "${beam_name}", ...
                                           "None", "${tracking_output_string}" );
    file_temp = "tracking.dat";
    save("-ascii",file_temp,"Emitt");
}
