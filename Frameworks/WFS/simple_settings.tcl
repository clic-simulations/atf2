set script_dir [pwd]
set lattice_dir $script_dir/../../Lattices/ATF2
set script_common_dir $script_dir/../../Common
source $script_dir/../ATF2_studies/scripts/tracking_procs.tcl

# Options

set seed 2
# total number of simulated particles and slices
set n_slice 10
set n 3000
set beamline_name "ATF2_v5.2"
set tracking_output_string "%s %E %dE %ex %ey %sz %sx %sy" ;# position, energy, dispersion, emittx, emitty, bunch_length, beam size x, beam size y
set dir_name "simpletracking"

###################
# Beam Parameters #
###################

# copied mostly from Mad8 file

#emitance normalized in e-7 m
set match(emitt_x) 52
set match(emitt_y) 0.3
#energy spread normalized in %, negative means flat distribution, positive gaussian distribution, maxed at 3 sigma
set match(e_spread) 0.08
#beam long in e-6 m
set match(sigma_z) 8000
set match(phase) 0.0
set e_initial 1.3 ;# design beam energy [GeV]
# Reference beamline energy (can be different from beam energy)
set e0_start 1.3 ;# [GeV]
#set e_initial 1.269 ;# operating beam energy? [GeV], from Mad8 file
set charge 1e10
if { $beamline_name == "ATF2" || $beamline_name == "ATF2_v5.1" || $beamline_name == "ATF2_v5.2"} {
    # ATF2 v5 beta in m 
    # (by Hector Garcia Morales)
    #array set match {beta_x 8.158262183 alpha_x 0.0 beta_y 0.7917879269 alpha_y 0.0}
    # Mad8 (for lattices without matching part)
    array set match {beta_x 6.848560987444 alpha_x 1.108024744392 beta_y 2.935758991906 alpha_y -1.907222941526}

} elseif { $beamline_name == "ATF2_v4.0" || $beamline_name == "UL"} {
    # ATF2 v4.0 beta in m
    array set match {beta_x 6.54290 alpha_x 0.99638 beta_y 3.23785 alpha_y -1.9183 }
}

# synchrotron on/off in certain elements
set quad_synrad 0
set mult_synrad 0
set sbend_synrad 0

# CBPM Wakefield
set use_wakefield 1
# use short bunch approximation - 
set use_wakefield_shortbunch 1
set wakefield_shortbunch 300 ;# [um]
