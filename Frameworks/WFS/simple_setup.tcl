# make output directory:
exec mkdir -p $dir_name
cd $dir_name

RandomReset -seed $seed

set e0 $e0_start
# add beamline
Girder
ReferencePoint -name "START" -angle 1 -sens 1
proc HCORRECTOR { a b c d } { Dipole $a $b $c $d }
proc VCORRECTOR { a b c d } { Dipole $a $b $c $d }
if {$use_wakefield} {
    source $script_dir/../ATF2_studies/scripts/wakefield.tcl
}
 
source $lattice_dir/ATF2_v5.2-MadX.tcl
ReferencePoint -sense -1
puts "Final beam energy at IP: $e0 GeV"

BeamlineSet -name $beamline_name

#######################
# Setting up the beam #
#######################

set beam_name electron
set scripts $script_common_dir/scripts  ; # This is necessary for compatibility
# load RF and injection scripts:
source $script_common_dir/scripts/init.tcl;

# loading of the beam making scripts
source $script_common_dir/scripts/ilc_beam.tcl;
# Total number of particles
set n_total [expr $n * $n_slice]

# make initial perfect beam
make_beam_many_energy $beam_name $n_slice $n 1
