# make output directory:
exec mkdir -p $dir_name
cd $dir_name

RandomReset -seed $seed

set e0 $e0_start
# add beamline
Girder
ReferencePoint -name "START" -angle 1 -sens 1
proc HCORRECTOR { a b c d } { Dipole $a $b $c $d }
proc VCORRECTOR { a b c d } { Dipole $a $b $c $d }
source $lattice_dir/ATF2_v5.2-MadX.tcl
ReferencePoint -sense -1
puts "Final beam energy at IP: $e0 GeV"

BeamlineSet -name $beamline_name

#######################
# Setting up the beam #
#######################

set beam_name electron
set scripts $script_common_dir/scripts  ; # This is necessary for compatibility
# load RF and injection scripts:
source $script_common_dir/scripts/init.tcl;

# loading of the beam making scripts
source $script_common_dir/scripts/ilc_beam.tcl;
# Total number of particles
set n_total [expr $n * $n_slice]

# make initial perfect beam
make_beam_many_energy $beam_name $n_slice $n 1

# make additional beams
# off energy
set e_initial_orig $e_initial
set e_initial [expr $e_initial*$dfs_dE]
make_beam_many_energy ${beam_name}_dfs $n_slice $n 1

# off charge
set e_initial $e_initial_orig
set charge_orig $charge
set charge [expr $charge*$wfs_dcharge]

make_beam_many_energy ${beam_name}_wfs $n_slice $n 1

#put charge back
set charge $charge_orig
