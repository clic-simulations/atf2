source simple_settings.tcl

source wfs_settings.tcl

source wfs_setup.tcl

############
# Tracking #
############

# Load response matrices
Octave {
    bpm_index = placet_get_number_list("$beamline_name", "bpm");
    qp_index = placet_get_number_list("$beamline_name", "quadrupole");
    drift_index = placet_get_number_list("$beamline_name", "drift");

    response_matrix_name = strcat("${script_dir}/","${response_matrix_file}")

    if exist(response_matrix_name, "file")
        load(response_matrix_name);
    else
        printf("WARNING Response matrix %s not found!\n", "$response_matrix_file");
        exit(1);
    end

# BPM nominal values

if($use_dfs || $use_wfs)
    % Binning
    steering_nbins = 1; % adapt if chosen for more than one bin
    noverlap = 0;
    nBpms = length(Response.Bpms);
    Blen = nBpms / (steering_nbins * (1 - noverlap) + noverlap);
    for i=1:steering_nbins
        Bmin = floor((i - 1) * Blen - (i - 1) * Blen * noverlap) + 1;
        Bmax = floor((i)     * Blen - (i - 1) * Blen * noverlap);
        Cxmin = find(Response.Cx < Response.Bpms(Bmin))(end);
        Cxmax = find(Response.Cx < Response.Bpms(Bmax))(end);
        Cymin = find(Response.Cy < Response.Bpms(Bmin))(end);
        Cymax = find(Response.Cy < Response.Bpms(Bmax))(end);
        if ($steering_select_bpms)
            % MQF1X, MQF3X, MQF4X, MQD5X, MQF7X, MQF9X, MQF11X,
            % MQD14X, MQD16X, MQD18X,
            % MQD20X, FB2FF, QD10BFF,
            % QD2BFF
            Bins(i).Bpms = [2,4,5,6,8,10,13,17,20,22,24,29,33,48];
        else
            Bins(i).Bpms = Bmin:Bmax;
        end
        Bins(i).Cx = Cxmin:Cxmax;
        Bins(i).Cy = Cymin:Cymax;
    end
    printf("Each steering bin contains approx %g bpms.\n", round(Blen));
    
    % make indices for more than one bin!
    for bin = 1:steering_nbins
        Bin = Bins(bin);
        nCx = length(Bin.Cx);
        nCy = length(Bin.Cy);
        DFSWFS_Bpms = Response.Bpms(Bin.Bpms);
        DFSWFS_B = Response.B(Bin.Bpms,:);
        Rx = Response.Rx(Bin.Bpms,Bin.Cx);
        Ry = Response.Ry(Bin.Bpms,Bin.Cy);
        if ($use_dfs)
            DFSWFS_BDFS = Response.BDFS(Bin.Bpms,:);
            RxDFS = Response.RxDFS(Bin.Bpms,Bin.Cx);
            RyDFS = Response.RyDFS(Bin.Bpms,Bin.Cy);
        end
        if ($use_wfs)
            DFSWFS_BWFS = Response.BWFS(Bin.Bpms,:);
            RxWFS = Response.RxWFS(Bin.Bpms,Bin.Cx);
            RyWFS = Response.RyWFS(Bin.Bpms,Bin.Cy);
        end
    end
end

if($use_misalignment)
    if ($alignment_bpm)
    placet_element_set_attribute("$beamline_name", bpm_index, "x", randn(size(bpm_index)) * $alignment_sigma);
    placet_element_set_attribute("$beamline_name", bpm_index, "y", randn(size(bpm_index)) * $alignment_sigma);
    end
    if ($alignment_qp)
    placet_element_set_attribute("$beamline_name", qp_index, "x", randn(size(qp_index)) * $alignment_sigma);
    placet_element_set_attribute("$beamline_name", qp_index, "y", randn(size(qp_index)) * $alignment_sigma);
    end
    if ($alignment_drift)
    placet_element_set_attribute("$beamline_name", drift_index, "x", randn(size(drift_index)) * $alignment_sigma);
    placet_element_set_attribute("$beamline_name", drift_index, "y", randn(size(drift_index)) * $alignment_sigma);
    end
end

disp("end loading")
}

Octave {

disp("loop")
% initial beam
beam_name = "${beam_name}"

for time_step_index=1:12

time_step_index

%%%%%%%%%%%%%%%
% DFS and WFS %
%%%%%%%%%%%%%%%

% start at step 2
if (time_step_index == 2)
iter = 0;
dfs_onoff=0; %DFS steering state: 0 neutral, 1 reduced energy,
                 %2 reset energy and store bpm reading, 3 done, 4 apply correction
wfs_onoff=0; %DFS steering state: 0 neutral, 1 reduced charge,
                 %2 reset charge and store bpm reading, 3 done, 4 apply correction
end

if (time_step_index >= 2 && iter < $steering_iter)
    printf('Steering - iteration %d\n',iter);

% DFS, alternating nominal and reduced energy
if ($use_dfs || $use_wfs)
    % save BPM readings nominal beam
    if (dfs_onoff == 0 && wfs_onoff == 0)
        B = placet_get_bpm_readings("$beamline_name", DFSWFS_Bpms);
    end
    
    if ($use_dfs && dfs_onoff == 0)
        dfs_onoff = 1; % iteration with reduced energy
    elseif ($use_wfs && wfs_onoff == 0)
        wfs_onoff = 1; % iteration with reduced charge
    end
    
    if (dfs_onoff==1) 
        dfs_onoff = 2;
        disp('reduced energy beam')
        beam_name = "${beam_name}_dfs";
    elseif (dfs_onoff==2)
        dfs_onoff = 3;
        % save BPM readings
        B_DFS = placet_get_bpm_readings("$beamline_name", DFSWFS_Bpms);
        beam_name = "$beam_name";
    end
    
    if(wfs_onoff==1)
        wfs_onoff = 2;
        disp('reduced charge beam')
        % beam with reduced charge
        beam_name = "${beam_name}_wfs";
    elseif (wfs_onoff==2)
        wfs_onoff = 3;
        % save BPM readings
        B_WFS = placet_get_bpm_readings("$beamline_name", DFSWFS_Bpms);
        % reset charge
        beam_name = "$beam_name";
    end

    if (dfs_onoff == 3 && $use_wfs==0)
        disp('DFS steering')
        dfs_onoff = 4;
        % get readings from reduced energy beam
        % differential readings compared to nominal reduced energy beam
        BDFS = B_DFS - DFSWFS_B - DFSWFS_BDFS;
        % differential readings from 'nominal orbit'
        B -= DFSWFS_B;
        
        % cut for nbins:
        %B = B(Bin.Bpms,:);
        %BDFS = BDFS(Bin.Bpms,:);
        Cx = -[ Rx ; $dfs_weight * RxDFS ; $steering_beta * eye(nCx) ] \ [ B(:,1) ; $dfs_weight * BDFS(:,1) ; zeros(nCx,1) ];
        Cy = -[ Ry ; $dfs_weight * RyDFS ; $steering_beta * eye(nCy) ] \ [ B(:,2) ; $dfs_weight * BDFS(:,2) ; zeros(nCy,1) ];
    elseif (wfs_onoff == 3 && $use_dfs == 0)
        disp('WFS steering')
        wfs_onoff = 4;
        % get readings from reduced charge beam
        % differential readings compared to nominal reduced charge beam
        BWFS = B_WFS - DFSWFS_B - DFSWFS_BWFS;
        % differential readings from 'nominal orbit'
        B -= DFSWFS_B;
        
        Cx = -[ Rx ; $wfs_weight * RxWFS ; $steering_beta * eye(nCx) ] \ [ B(:,1) ; $wfs_weight * BWFS(:,1) ; zeros(nCx,1) ];
        Cy = -[ Ry ; $wfs_weight * RyWFS ; $steering_beta * eye(nCy) ] \ [ B(:,2) ; $wfs_weight * BWFS(:,2) ; zeros(nCy,1) ];
    elseif (dfs_onoff == 3 && wfs_onoff == 3)
        disp('DFS and WFS steering')
        dfs_onoff = 4;
        wfs_onoff = 4;
        % differential readings compared to nominal reduced charge beam
        BDFS = B_DFS - DFSWFS_B - DFSWFS_BDFS;
        BWFS = B_WFS - DFSWFS_B - DFSWFS_BWFS;
        % differential readings from 'nominal orbit'
        B -= DFSWFS_B;
        
        Cx = -[ Rx ; $dfs_weight * RxDFS ; $wfs_weight * RxWFS ; $steering_beta * eye(nCx) ] \ [ B(:,1) ; $dfs_weight * BDFS(:,1) ; $wfs_weight * BWFS(:,1) ; zeros(nCx,1) ];
        Cy = -[ Ry ; $dfs_weight * RyDFS ; $wfs_weight * RyWFS ; $steering_beta * eye(nCy) ] \ [ B(:,2) ; $dfs_weight * BDFS(:,2) ; $wfs_weight * BWFS(:,2) ; zeros(nCy,1) ];
    end
end

% apply correction
if (dfs_onoff==4 || wfs_onoff==4)
    disp('Apply correction')
    iter+=1;
    dfs_onoff = 0;
    wfs_onoff = 0;

    placet_element_vary_attribute("$beamline_name", Response.Cx(Bin.Cx), "strength_x", Cx);
    placet_element_vary_attribute("$beamline_name", Response.Cy(Bin.Cy), "strength_y", Cy);

end

end % iteration

% Tracking 
    disp ("tracking")
    [Emitt,Beam]=placet_test_no_correction("${beamline_name}", beam_name, ...
                                           "None", "${tracking_output_string}" );
    file_temp = strcat("tracking",num2str(time_step_index),".dat");
    save("-ascii",file_temp,"Emitt");

%% save bpm readings
    if (${bpm_noise} == 1)
       bpm_readings = placet_get_bpm_readings("${beamline_name}", bpm_index, false);
    else
       bpm_readings = placet_get_bpm_readings("${beamline_name}", bpm_index, true);
    end

    f_meas_x = fopen("bpmreadingsx.dat", 'at');
    f_meas_y = fopen("bpmreadingsy.dat", 'at');
    for i = 1:length(bpm_readings(:,1))
	fprintf(f_meas_x, '%g   ', bpm_readings(i,1));
    end
    fprintf(f_meas_x, '\n');
    fclose(f_meas_x);

    for i = 1:length(bpm_readings(:,2))
	fprintf(f_meas_y, '%g   ', bpm_readings(i,2));
    end
    fprintf(f_meas_y, '\n');
    fclose(f_meas_y);

end % for loop

}

