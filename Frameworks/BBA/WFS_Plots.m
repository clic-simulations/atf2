figure(1)
clf
plot(Corrx);
hold on
plot(Corry, 'r-');

figure(2);
clf
plot(DispE.X);
hold on
plot(zeros(size(DispE.X)), 'r-');

figure(3);
clf
plot(DispE.Y);
hold on
plot(zeros(size(DispE.Y)), 'r-');

figure(4)
plotyy(1:iteration, norm_plot(:,1), 1:iteration, norm_plot(:,2));

figure(5)
plotyy(1:iteration, normb_plot(:,1), 1:iteration, normb_plot(:,2));
