function JitterRemovalWake(Orbitx0, Orbity0, StabilityX, StabilityY, BPMIndexBefore)

% reference orbit data
%orbitx_file = 'Data/20141211_StabilityWake15/OrbitX0.mat';
%orbity_file = 'Data/20141211_StabilityWake15/OrbitY0.mat';

%orbitx_file5 = 'Data/20141211_StabilityWake5/OrbitX0.mat';
%orbity_file5 = 'Data/20141211_StabilityWake5/OrbitY0.mat';

%orbitx_file = 'Data/20141211_Stability0/OrbitX0.mat';
%orbity_file = 'Data/20141211_Stability0/OrbitY0.mat';

%Bx = load(orbitx_file);
%load(orbitx_file);
%load(orbity_file);

%load(orbitx_file5);
%load(orbity_file5);

StabilityX = StabilityX';
StabilityY = StabilityY';

Orbitx0 = Orbitx0';
Orbity0 = Orbity0';

%BPMIndexBefore = 6;

% find name and index of corrector
StabilityX1 = StabilityX(1:BPMIndexBefore,:);
StabilityX2 = StabilityX(BPMIndexBefore+1:end,:);
StabilityY1 = StabilityY(1:BPMIndexBefore,:);
StabilityY2 = StabilityY(BPMIndexBefore+1:end,:);

%correlation matrix
Xx = pinv(StabilityX1)' * StabilityX2';
Xy = pinv(StabilityY1)' * StabilityY2';

%Bx = this.state.bpms.X;
Orbitx1 = Orbitx0(1:BPMIndexBefore,:);
Orbitx2 = Orbitx0(BPMIndexBefore+1:end,:);

%By = this.state.bpms.Y;
Orbity1 = Orbity0(1:BPMIndexBefore,:);
Orbity2 = Orbity0(BPMIndexBefore+1:end,:);

Rx = Orbitx1' * Xx - Orbitx2';
Ry = Orbity1' * Xy - Orbity2';

%keyboard;
Bx_JR = zeros (size(Orbitx2));
Bx_JR = Rx';
%this.state.bpms.X = Bx_JR;

By_JR = zeros (size(Orbity2));
By_JR = Ry';
%this.state.bpms.Y = By_JR;

figure(1)
plot(mean(By_JR'),'r-');
figure(2)
plot(mean(Bx_JR'),'r-');
%hold on;
%plot(mean(Orbity2'),'b-');

%S = State(this.state);

end
