Add_path

Interface = InterfaceATF2(30);

bpm_mask = [ 1:21, 26:49 57 ];
bpm_mask = bpm_mask(1:24);

load 'Response_pinv_47.mat';

Rx  = Response.Rx(bpm_mask,:);
Ry  = Response.Ry(bpm_mask,:);
%Rxx = Response.Rx(bpm_mask,Response.MASK_CORSX);

%mask = [ 9 10 11 23 24 ];

%C =  pinv(Rx(mask,:),47)  * [ 10 200 100 0 0]';
%Cx = pinv(Rxx(mask,:),20) * [ 100 400 300 0 0]';

C = randn(size(Response.CORS')) * 0.1;


expected = Rx * C;
%expected = Rxx * Cx;

state = struct;
S0 = GetMachine(Interface);
O0 = GetOrbit(S0, Response.BPMS(bpm_mask));

VaryCorrs(Interface, Response.CORS, C);

state = struct;
S1 = State(atf2_get_bpms(state, 20));
O1 = GetOrbit(S1, Response.BPMS(bpm_mask));

Diff_X = O1.X - O0.X;
Diff_Y = O1.Y - O0.Y;

figure(1)
clf
plot(expected, 'b-');
hold on
plot(Diff_X, 'r-');

expected = Ry * C;

figure(2)
clf
plot(expected, 'b-');
hold on
plot(Diff_Y, 'r-');

Restore(Interface, S0, Response.CORS);

save(sprintf('response_test_%s.mat',strrep(datestr(clock, 30), 'T', '_')),'S0', 'S1', 'O0', 'O1', 'C', 'expected', 'bpm_mask');
