% Function to read the corrector settings from EPICS
%
% Juergen Pfingstner
% 27th of Februar 2014

function [value_x, value_y] = get_pv_xy(pv_x, pv_y)

   nr_x = length(pv_x);
   nr_y = length(pv_y);
   pv = [pv_x;pv_y];

   % Get the values of the PVs
   value=lcaGet(pv);

   value_x = value(1:nr_x);
   value_y = value(nr_x+1:nr_x+nr_y);

end
