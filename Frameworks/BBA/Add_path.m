%% on-line setup
main_dir = '/Users/alatina/SVN/clicsim/ATF2/Frameworks/';


%% For Placet Interface			   
%main_dir = (['/Users/Nuria/Desktop/PlacetBDSIM_WFS/ATF2/Frameworks']);
%flight_simulator_dir = (['/Users/Nuria/Desktop/PlacetBDSIM_WFS/ATF2/Frameworks/BBA_flight_simulator']);
%addpath([ main_dir,'/BBA_flight_simulator/' ]);

%% 			   
addpath([ main_dir ]);
addpath([ main_dir, '/BBA/' ]);
addpath([ main_dir, '/BBA/ATF2_tools/' ]);
addpath([ main_dir, '/BBA/Classes/' ]);
addpath([ main_dir, '/BBA/CheckTool/' ]);
addpath([ main_dir, '/BBA/tools/' ]);
addpath([ main_dir, '/BBA/SysID_GUI/' ]);
addpath([ main_dir, '/BBA/BBA_GUI/' ]);

if ~is_octave()
    addpath([ main_dir, '/BBA/tools/matlab' ]);
end