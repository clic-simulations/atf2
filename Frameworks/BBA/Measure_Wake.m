bpm_mask = [ 1:21 26 27 28 35:49 ];

Interface = InterfaceATF2(20);

S0 = GetMachine(Interface);
O0 = GetOrbit(S0);

ChangeBunchCharge(Interface, 8*0.01)

S1 = GetMachine(Interface);
O1 = GetOrbit(S1);

ChangeBunchCharge(Interface, 5*0.01)

DY= (O1.Y - O0.Y);
DX= (O1.X - O0.X);

figure(1)
clf
plot(DX(bpm_mask),'b-')
hold on
plot(zeros(size(bpm_mask)),'r-')
ylabel('X orbit difference');
xlabel('bpm nb');

figure(2)
clf
plot(DY(bpm_mask),'b-')
hold on
plot(zeros(size(bpm_mask)),'r-')
ylabel('Y orbit difference');
xlabel('bpm nb');

normxy = [ norm(DX) norm(DY) ]

