function [corr,sgn,iter] = filename2corrector(filename)
    % DATA_XCOR_LI02_830_p0001.mat
    [tok, filename] = strtok(filename,'_'); %dump 'DATA_'
    [tok, filename] = strtok(filename,'_'); % XCOR
    corr = [ tok ':' ];
    [tok, filename] = strtok(filename,'_'); % LI02
    corr = [ corr tok ':' ];
    [tok, filename] = strtok(filename,'_'); % 830
    corr = [ corr tok ];
    iter = filename(3:6);  % 0001
    sgn = filename(2);     % p
