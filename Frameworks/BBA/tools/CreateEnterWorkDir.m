function dirname = BBA_CreateEnterWorkDir(basename)
%% create a work directory and enter it
  dirname = sprintf('%s_%s', basename, strrep(datestr(clock, 30), 'T', '_'));
  system(sprintf('mkdir -p %s', dirname));
  chdir(dirname);
end
