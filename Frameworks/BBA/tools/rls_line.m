% Implements the function rls_line.m
% 
% Function: It implements one step of the recursive least square algorithm (RLS),
% for one line of the orbit response matrix.
%
% Juergen Pfingstner
% 23. Januar 2012

function [r_new, P_new] = rls_line(bpm, phi, r_old, P_old, lambda)
% Usage of [r_new, P_new] = rls_line(bpm, phi, r_old, P_old, lambda)
% 
% Variables:
%
%      Input:
%            bpm:     Measurement values of the BPMs
%            phi:     Excitation values
%            r_old:   Last estimated vector r
%            P_old:   Last estimated covarinace matrix of the parameters
%            lambda:  Forgetting factor
%
%      Output:
%            r_new:   Actual estimated vector r
%            P_new:   Actual estimated covariance matrix of the parameters
%

% Calculate the error of the last measurement
error = bpm - r_old'*phi;

% Calculate K
b = lambda + phi' * P_old * phi;
b = 1 / b;
K = P_old * phi;
K = K .* b;

% Calculate the updated matrix P_new
P_new = eye(length(r_old)) - K * phi';
P_new = P_new * P_old;
P_new = P_new ./ lambda;

% Calculate the new estimated values
r_new = r_old + K.*error;

end
