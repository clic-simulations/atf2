function M = svd_mean(T,n_sv)

if nargin ~= 2
n_sv = 30;
end

n_sv = min(n_sv,min(size(T)));

[U,S,V] = svd(T);

S = diag(S);
S((n_sv+1):end) = 0.0;
T = zeros(size(T));
T(1:length(S), 1:length(S))=diag(S);


M = mean(U*T*V');
