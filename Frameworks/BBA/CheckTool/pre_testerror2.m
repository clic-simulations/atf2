

bucket = 85.5;
offset_list = -1.2:0.6:1.2;
phase_list = [15:5:155 165 175];
load Response_Electron_Real.mat
BPM_MASK = [ find(strcmp(state.bpms.name, Response_Real.BPMS{1})): find(strcmp(state.bpms.name, Response_Real.BPMS{end})) ];
NBpm = numel(BPM_MASK);

Fake_Respond = [1:NBpm]';
Wake = 30;
Fake_Data = Wake*Fake_Respond*ones(1, 100);

WT = zeros(1, numel(phase_list));
ET = zeros(1, numel(phase_list));
for iph = 1:numel(phase_list)
  Kick = zeros(NBpm, numel(offset_list));
  filename=sprintf('DATA_ELE_%g_%d_%g_%g.mat', bucket, phase_list(iph), 0, 0);
  load(filename);
  ref_orbit = svd_mean(state.bpms.Y(BPM_MASK, :)',10)';
  for ioff = 1:numel(offset_list)
    filename=sprintf('DATA_ELE_%g_%d_%g_%g.mat', bucket, phase_list(iph), 0, offset_list(ioff));
    load(filename);
    Kick(:, ioff) = svd_mean(state.bpms.Y(BPM_MASK, :)',10)' - ref_orbit;
    % Kick(:, ioff) = mean((offset_list(ioff)+0.6*randn([100, 1])*ones(1, NBpm) ) .* Fake_Data' .* (1+0.4*randn(size(Fake_Data'))))';
  end
  Kick_Offset_Less = Response_Real.Ry \ Kick;
  
  % [A, B] = polyfit(Fake_Respond, DY', 1);
  [A, B] = polyfit(offset_list, Kick_Offset_Less, 1);
  WT(iph) = A(1);
  ET(iph) = B.normr/sqrt(sum(offset_list.^2));
  % ET(iph) = B.normr/sqrt(sum(Fake_Respond.^2));
end
plot(phase_list, WT, 'r-', phase_list, ET, 'b--');
