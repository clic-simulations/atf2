function pre_vieworbit(file, show_option, ref_orbit)
% 'Y' (default) shows Y orbit, 'X' shows X orbit, TMIT shows TMIT in the overlap area

if nargin < 1
  error('You should define note!!!');
elseif nargin < 2
  show_option = 'Y';
end

if show_option ~= 'TMIT'
  BPM_MASK = [1:26, 36:61];
else
 BPM_MASK = [27 28 29 30 32];
end


file_matching = ['DATA_ELE_' file '.mat'];
file_list = dir(file_matching);
v_left = strfind(file_matching, '*');
all_orbit = [];
all_legend = {};


Nf = numel(file_list);
if Nf == 0
  error('Nothing is found!');
elseif mod(Nf, 2) == 0
  error('Data set is incomplete!');
end

for ii = 1 : Nf
  file_name = file_list(ii).name;
  load(file_name);
  v_name = file_name(v_left:end);
  v_right = [strfind(v_name, '_') strfind(v_name, '.mat')];
  v_right = min(v_right);
  all_legend = [all_legend v_name(1:v_right-1)];
  Z = state.bpms.Z(BPM_MASK);
  if show_option == 'TMIT'
     orbit = mean(state.bpms.TMIT(BPM_MASK, :)');
  elseif show_option == 'X'
     orbit = mean(state.bpms.X(BPM_MASK, :)');
  else
     orbit = mean(state.bpms.Y(BPM_MASK, :)');
  end
  all_orbit = vertcat(all_orbit, orbit);
end

if show_option ~= 'TMIT'
  if nargin < 3
    ref_orbit = mean(all_orbit);
  end
  for ii = 1:Nf
    all_orbit(ii,  :) = all_orbit(ii,  :) - ref_orbit;
  end
end



plot(Z, all_orbit');
grid on
legend(all_legend)
