function [Vx,Vy,X,Y] = pca(filename, bpm_mask)

if nargin == 1
    bpm_mask = [];
end

% filename = e.g. 'DATA_ELE_45.75_20_0_*mat'

% get the list of all data files in the current directory
data_files = dir(filename);
data_files = { data_files.name }';

nFiles = numel(data_files);

X = [];
Y = [];
for j=1:nFiles
    load(data_files{j});
    X = [ X state.bpms.X ];
    Y = [ Y state.bpms.Y ];
end

if numel(bpm_mask) ~= 0
  X = X(bpm_mask,:);
  Y = Y(bpm_mask,:);
end

X = X';
Y = Y';

[Vx,Dx] = eig(cov(X));
[Vy,Dy] = eig(cov(Y));

% the uncorrelated data are obtained as
% X_unc = X * Vx
% Y_unc = Y * Vy


