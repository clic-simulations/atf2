function [FrequencyFFT,FrequencyFit] = SlowDriftFrequency(X)

%% fits BPM reading array with a sine wave, and does an FFT.
Fs = 3.12;
T = 1/Fs;
L =  length(X)
Time = 1:L;
Time = Time/Fs;
figure(1)
clf
plot(Time,X)
xlabel('time (seconds)')
ylabel('X position');

% FFT

XAv = mean(X)
XAvS = X - XAv;

figure(2)
clf
plot(Time,XAvS)
xlabel('time (seconds)')
ylabel('X position average subtracted');

NFFT = 2^nextpow2(L)
Y = fft(XAvS,NFFT)/L;
f = Fs/2*linspace(0,1,NFFT/2+1);

% Plot single-sided amplitude spectrum.
figure(3)
clf
Xf = 2*abs(Y(1:NFFT/2+1));
plot(f,Xf) 
title('Single-Sided Amplitude Spectrum of y(t)')
xlabel('Frequency (Hz)')
ylabel('|X(f)|')

% find maximum, excluding peak at 1 (offset)!
[maximum index] = max(Xf(1:end))
fprintf('the frequency at the maximum is %f Hz with a peak at %f\n', f(index), Xf(index))
nrPulses = round(1./f(index)*Fs)
nrPulsesDown = round(1./f(index-1)*Fs)
nrPulsesUp = round(1./f(index+1)*Fs)

fprintf('the number of pulses in the slow drift is %d \n', nrPulses)

% Try to fit the data

SineFit(Time,XAvS,4)

% smooth data
windowSize = 30;
b = (1/windowSize)*ones(1,windowSize)

Xfilter = filter(b,1,XAvS);

SineFit(Time,Xfilter,5)

%fit = lsqcurvefit(@SinWave,[Xf(index),f(index),0],Time,XAvS,[0,f(index- ...
%                                                  1),0],[2*Xf(index),f(index+1),1/f(index)])

%figure(2)
%hold on
%plot(Time,fit(1)*sin(fit(2) * XAvS + fit(3)));
