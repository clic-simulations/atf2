function Test_Electron_SlowJitter(string)

if nargin ~= 1
  error('You should define note!!!');
end

addpath([ '~/alatina/E-208/tools']);
addpath([ '~/alatina/E-208/']);

global Asset_Particle

%BPM_MASK = [1:26, 36:62];
BPM_MASK = [1:61];

%aidainit;
%da = DaObject();
%da.setParam('MKB', 'POSRAMP.MKB')
%d = da.setDaValue('MKB//VAL', DaValue(java.lang.Float(0)));
%phase = d.get(1).getAsDoubles;
%d = da.setDaValue('MKB//VAL', DaValue(java.lang.Float(-45 - phase)));
%pause(5);
%pause(5);

Asset_Particle = 'electron';
Interface = InterfaceAsset(100);
S = GetMachine(Interface);
state = GetStruct(S);
Ref_orbit = mean(state.bpms.Y(BPM_MASK, :)');

asset_elec(1);

test_num = {'1', '2', '3', '4', '5', '6', '7', '8', '9', 10' };
orbit = [];
for ii = 1:numel(test_num)
    filename=sprintf('DATA_TEST_ELE_%s_%s.mat', string, test_num{ii});
    if ~exist(filename, 'file')
      Asset_Particle = 'electron';
      S = GetMachine(Interface);
      state = GetStruct(S);
      save(filename, 'state');
    else
      disp([ 'Skipping file ''' filename ''' ...']);
      load(filename);
    end
    orbit = vertcat(orbit, mean(state.bpms.Y(BPM_MASK, :)'));
end

for ii = 1:numel(test_num)
  orbit(ii,  :) = orbit(ii,  :) - Ref_orbit;
end

plot(orbit');
legend(test_num)
