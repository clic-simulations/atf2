function Test_Scan()

addpath([ '~/alatina/E-208/tools']);
addpath([ '~/alatina/E-208/']);

global Asset_Particle

Asset_Particle = 'positron';
Interface = InterfaceAsset(1);

load 'Response_positron.mat';

BPMS = {
 'LI02:BPMS:134'
 'LI02:BPMS:146'
};

MASK_BPMS = [ 
  find(strcmp(Response.BPMS, BPMS{1}))  
  find(strcmp(Response.BPMS, BPMS{2}))
];

Rx = Response.Rx(MASK_BPMS, :);
Ry = Response.Ry(MASK_BPMS, :);

Rx(:, Response.MASK_CORSY) = 0;
Ry(:, Response.MASK_CORSX) = 0;

%%%%%%%%%%%%%%%

R = [ Rx ; Ry ];

[U,M,V] = svd(R);

sv_M = diag(M)
inv_M = zeros(size(M'));

n_sv = 4;
n_sv = min(n_sv, min(size(M)));

for i=1:n_sv
  inv_M(i,i) = 1/M(i,i);
end

inv_R = V * inv_M * U';

%%%%%%%%%%%%%%%

asset_elec(1)
asset_posi(1)

dX = [ -0.6 0.6];
offset = [
  0 zeros(size(dX))
  0 dX
];

S0 = GetMachine(Interface);
state = GetStruct(S0);
CORR0 = GetCorrectorsData(S0, Response.CORS);

Last_corr = zeros(numel(Response.CORS),1);
count = 1;

for i = 1:size(offset,2)

    offsetx = offset(1,i);
    offsety = offset(2,i);

    Corr = inv_R * [
      offsetx
      offsetx
      offsety
      offsety
    ];


      Asset_Particle = 'positron';
      %VaryCorrs(Interface, Response.CORS, Corr-Last_corr);
      SetCorrs(Interface, Response.CORS, [ CORR0.BDES ]' + Corr);
      Last_corr = Corr

      % read the electron orbit
      Asset_Particle = 'electron';
      S_kick = GetMachine(Interface);

      %% save data on disk
      state = GetStruct(S_kick);

      %% reset correctors
      %Asset_Particle = 'positron';
      %VaryCorrs(Interface, Response.CORS, -Corr);

  end

disp('original BDES');
CORR0.BDES


%% restore original state
Asset_Particle = 'positron';
Restore(Interface, S0, Response.CORS);
S0 = GetMachine(Interface);
state = GetStruct(S0);
CORR1 = GetCorrectorsData(S0, Response.CORS);
disp('new BDES');
lsCORR1.BDES
