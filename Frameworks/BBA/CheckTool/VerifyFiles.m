addpath([ '~/alatina/E-208/tools']);
addpath([ '~/alatina/E-208/']);

% get the list of all data files in the current directory
data_files = dir('DATA_ELE_*');
data_files = { data_files.name }';

nFiles = length(data_files);

% read all files
for i=1:nFiles
  load(data_files{i});
  index1 = find(strcmp(state.bpms.name, 'LI02:BPMS:146'));
  index2 = find(strcmp(state.bpms.name, 'LI02:BPMS:231'));
  TMIT_percent = mean(state.bpms.TMIT([index1 index2],:),2)/2e10 * 100;
  bad = false;

  if TMIT_percent(1) > 150
    warning('TMIT(1) > 30% !!!!!');
    bad = true;
  end

  if TMIT_percent(2) < 90
	warning('TMIT(2) < 80% !!!!!');
    bad = true;
  end
  
  if bad
    fprintf('%s:\tLI02:BPMS:146 = %.2f%%; LI02:BPMS:231 = %.2f%%\n', data_files{i}, TMIT_percent(1), TMIT_percent(2));
    %delete(data_files{i});
    %system(['mv ' data_files{i} ' Trash/' data_files{i} ]);
  end
end
