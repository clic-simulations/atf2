function [D,chi2] = fit_dispersion(X, bpm_mask)
   
    addpath('~/Projects/ATF2/BBA/Checktool')
    addpath('~/Projects/ATF2/BBA/tools')
    addpath('~/Projects/ATF2/BBA')

    R16 = load('~/Projects/ATF2/BBA/ATF2_tools/disp.dat');
    R16 = R16 * 1e6;
    
    if nargin > 1
        if numel(bpm_mask) ~= 0
            X = X(:,bpm_mask);
            R16 = R16(bpm_mask);
        end
    end
    
    X = X - repmat(mean(X), size(X,1), 1);
    %%%%%%%polyfit((1:size(X,1))', X(:,1),1)(1)
    
    nOrbits = size(X,1)
    nBpms = size(X,2)
     
    D = zeros(nOrbits,2);
    
    for i=1:nOrbits
        d_k = X(i,:) / [ R16' ; ones(1,nBpms) ];
        D(i,1) = d_k(1);
        D(i,2) = d_k(2);
    end
    
     Xf = D *  [ R16' ; ones(1,nBpms) ];
     
     D = D(:,1);
     chi2 = norm(X - Xf, 'fro');
     
     plot(Xf(10,:), 'b-', X(10,:), 'r-');
end



    D = zeros(nOrbits,2);
    
    for i=1:nOrbits
        d_1 = X(i,:) / [ R16' ; ones(1,nBpms) ];
        D(i,1) = d_1(1);
        D(i,2) = d_1(2);
    end
