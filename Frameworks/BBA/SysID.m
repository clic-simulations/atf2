function SysID()
  global SYSID;
  feature('UseOldFileDialogs',0);
  
  Add_path % Added paths to needed directories
  
  loaded_old_state = 0;
  ans = questdlg('Do you want to start a new SysID?', 'SysID', 'Yes', 'No, I want to open an old one', 'Cancel', 'Yes');
  switch ans
  case 'Yes'
    disp('Starting a new project...');
    % ask what Interface should be used
    ans2 = questdlg('Select the Interface you want to use', 'SysID', 'ATF2 On-line', 'PLACET Simulation', 'PLACET Simulation');
    if strcmp(ans2, 'ATF2 On-line')
      disp('Going on-line with ATF2!');
      Interface = InterfaceATF2();
    else
      disp('Using Flight Simulator');
      dir=flight_simulator_dir; % Added paths in the Add_path function
      Interface = InterfacePlacet(dir);
    end
    basename = 'new_SYSID';
    dirname = sprintf('Data/%s_%s', basename, strrep(datestr(clock, 30), 'T', '_'));
    system(sprintf('mkdir -p %s', dirname));
    chdir('Data/');
    %
    disp(['Created a new data set in ' dirname ]);
    dirname = uigetdir('.', 'Select a data set...');
    chdir(dirname);
    disp('Saving the status of the machine...');
    S = GetMachine(Interface);
    Save(S, sprintf('machine_status_%s.mat',strrep(datestr(clock, 30), 'T', '_')));

    SYSID.Interface = Interface;
    SYSID.Directory = dirname;
    SYSID.State = S;
    SYSID.Correctors = {'<empty>'};
    SYSID.nsamples = 100;
    SYSID.CycleMode = 1;
    SYSID.Excitation.X = 0.4;
    SYSID.Excitation.Y = 0.3;
    SYSID.MaxStrength = 0.060;
    SYSID.CorrectorStrengths = CorrectorStrengths(GetCorrectors(S),SYSID.Directory);
    SYSID.do_orbit_plots = true;

  case 'No, I want to open an old one'
    dirname = uigetdir('/userhome/atfopr/cernSteering/BBA/Data', 'Select a data set...');
    if dirname == 0
        disp('Ok...');
        return
    end
    disp(['Entering data set in ' dirname ]);
    chdir(dirname);
    if exist('sysid.mat', 'file') ~= 2
      error([ 'File sysid.mat not found in folder ' dirname '. Please select another folder or start a new project.']);
      return;
    end
    load('sysid.mat', '-mat');
    SYSID.Directory = dirname; %overload the saved directory to allow for renamed folders
    if exist('corrector_strenghts.txt', 'file') ~= 2
      SYSID.CorrectorStrengths = CorrectorStrengths(GetCorrectors(SYSID.State),SYSID.Directory);
    end
    ans = questdlg('Do you want to acquire a new machine state or load an old one?', 'SysID', 'New', 'Old','Old');
    switch ans
    case 'New'
      disp('Acquiring the status of the machine...');
      SYSID.State = GetMachine(SYSID.Interface);
      Save(SYSID.State, sprintf('machine_status_%s.mat',strrep(datestr(clock, 30), 'T', '_')));
    case 'Old'
      [FileName,PathName] = uigetfile('*.mat','Select a machine status file');
      if FileName == 0
        disp('Ok...');
        return;
      end
      if PathName(end) == '/' ; PathName = PathName(1:end-1); end
      if dirname(end) == '/' ; dirname = dirname(1:end-1); end
      if strcmp(PathName, dirname) == 0
        copyfile([PathName '/' FileName],[dirname '/' FileName]);
      end
      SYSID.State = State([dirname '/' FileName]);
      disp('Restoring the status of the machine...');
      Restore(SYSID.Interface, SYSID.State, SYSID.Correctors);
    end
  case 'Cancel'
    disp('Ok...');
    return
  end

  SysID_GUI();

  %save('sysid.mat','SYSID','-mat'); % done on gui closing event
  %chdir('../..');
  %ret_value = SYSID;
end
