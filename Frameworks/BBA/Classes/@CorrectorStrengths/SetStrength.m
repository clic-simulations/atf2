function SetStrength(this,corr,strength)
  fid = fopen(this.filename);
  C = textscan(fid,'%s%n');
  fclose(fid);
  indx = find(strcmp(C{1}, corr));
  if length(indx) == 0
    error(['./CorrectorStrengths::SetStrength corrector ' corr ' not found!' ]);  
    return;
  else
    C{2}(indx) = strength;
    fid = fopen(this.filename,'w');
    for i=1:length(C{1})
      fprintf(fid, '%s %g\n', C{1}{i}, C{2}(i));
    end
    fclose(fid);
  end
end
