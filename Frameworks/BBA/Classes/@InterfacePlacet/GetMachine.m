function S = GetMachine(this)
  disp('Interface-Placet::GetMachine()');
  S_ = GetBpms(this);
  state_ = GetStruct(S_);
  state = this.state;
  state.bpms = state_.bpms;
  S = State(state);
end
