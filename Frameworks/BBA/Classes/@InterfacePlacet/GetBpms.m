
function S = GetBpms(this)
setenv('DYLD_LIBRARY_PATH','/usr/local/bin/lib')

  disp('InterfacePlacet::GetBpms()');
  state = struct;
  % set up the directory
  if ~exist(this.dir,'dir')
    mkdir(this.dir);
  end
  if ~exist(strcat(this.dir,'/tmp_placet'),'dir')
    mkdir(strcat(this.dir,'/tmp_placet'));
  end

  % run placet (scrive il file coi bpms)
  run_placet = [ 'cd ' this.dir '; placet -s ' this.script ];
  system(run_placet);


  % read BPMs file values
  %copyfile('/Users/Nuria/Desktop/PlacetBDSIM_WFS/ATF2/Frameworks/BBA_flight_simulator/placet_BBA_bpms.txt','/Users/Nuria/Desktop/PlacetBDSIM_WFS/ATF2/Frameworks/BBA/Classes/@InterfacePlacet/')
  copyfile(strcat(this.dir,'/placet_BBA_bpms.txt'),strcat(this.dir,'/../BBA/Classes/@InterfacePlacet/'))
  copyfile(strcat(this.dir,'/placet_BBA_bpms.txt'),strcat(this.dir,'/tmp_placet'))
  FileID = fopen('placet_BBA_bpms.txt','r');
  C = textscan(FileID, '%s %f %f %f');
  fclose(FileID);

% write the data read from placet file in state.bpms.X / Y / TMIT
this.state.bpms.X = zeros(length(this.state.bpms.name),1);
this.state.bpms.Y = zeros(length(this.state.bpms.name),1);
this.state.bpms.TMIT = zeros(length(this.state.bpms.name),1);

for i=1:length(C{1})
j = find(strcmp(this.state.bpms.name, C{1}(i)));
this.state.bpms.X(j) = C{3}(i);
this.state.bpms.Y(j) = C{4}(i);
this.state.bpms.TMIT(i) = 1e9; %%%%%%%%%%%%%%%C{4}(i);
end

now_ = now;
this.state.timestamp.start = now_;
this.state.timestamp.done  = now_;
S = State(this.state);
end
