function I = ChangeBunchCharge(this, S)

if nargin < 2 | S==0
  error('You need to specify the Sign (>0 decreases the charge, <0 increases it)');
end

fid = fopen([this.dir '/tmp_placet/placet_BBA_charge.txt'],'w');
if S>0
  fprintf(fid, 'CHARGE 0.5');
else
  fprintf(fid, 'CHARGE 0.25');
end
fclose(fid);

I = this;
end
