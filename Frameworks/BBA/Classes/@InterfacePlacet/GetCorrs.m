function S = GetCorrs(this)
  disp('InterfacePlacet::GetCorrs()');
  state = struct;
  state.mags = this.state.mags;
  S = State(state);
end
