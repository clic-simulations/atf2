function I = ChangeEnergy(this, deltafreq)

% Calculation of energy change from the frequency change in the RF-ramp
% from DR

DR_freq = 714e3; % 714 MHz in kHz
DR_momentum_compaction = 2.1e-3;
E_nominal=1.3;

dE_E = -deltafreq / DR_freq / DR_momentum_compaction;

E_changed=E_nominal+E_nominal*dE_E

if nargin < 2 
  error('You need to specify the Sign (>0 decreases the charge, <0 increases it)');
end

fid = fopen([this.dir '/tmp_placet/placet_BBA_energy.txt'],'w');
if deltafreq > 0
  disp('Low Test energy ');
  fprintf(fid, 'ENERGY %f', E_changed);
    else if deltafreq==0
        disp('Nominal energy ');   
        fprintf(fid, 'ENERGY 1.3');
        else if deltafreq < 0
          disp('High test energy ');
          fprintf(fid, 'ENERGY %f', E_changed);  
        end
end

fclose(fid);

I = this;
end
