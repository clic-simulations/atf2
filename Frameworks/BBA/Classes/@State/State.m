function S = State(state_)
  S = struct;
  if nargin == 0 % default constructor
    S.state = struct;
  elseif nargin == 1
    if isstruct(state_)
      S.state = state_;
    elseif ischar(state_) % load from file
      load(state_, '-mat');
      S.state = state;
  end
  S = class(S, 'State');
end
