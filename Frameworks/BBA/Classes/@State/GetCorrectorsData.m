function C = GetCorrectorsData(this, NAMES)
  if nargin == 1
    NAMES = GetCorrectors(this);
  end
  if ~iscell(NAMES)
    NAMES_ = cell(1);
    NAMES_{1} = NAMES;
    NAMES = NAMES_;
  end
  nNAMES = length(NAMES);
  for i=1:nNAMES
    idx = find(strcmp(this.state.mags.name, NAMES{i}));
    C(i).name = NAMES{i};
    C(i).BACT = this.state.mags.BACT(idx);
    C(i).BDES = this.state.mags.BDES(idx);
    C(i).Z = this.state.mags.Z(idx);
  end
  Z = [ C(:).Z ];
  [Z,I] = sort(Z);
  C = C(I);
end
