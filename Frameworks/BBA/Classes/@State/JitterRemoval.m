function S = JitterRemoval(this, BPMIndexBefore)

%TODO

% reference orbit data
orbitx_file = '20141211_Stability0/OrbitX0.mat';
orbity_file = '20141211_Stability0/OrbitY0.mat';
%Bx = load(orbitx_file);
load(orbitx_file);
load(orbity_file);

Orbitx0 = X';
Orbity0 = Y';

% find name and index of corrector
Orbitx1 = Orbitx0(1:BPMIndexBefore,:);
Orbitx2 = Orbitx0(BPMIndexBefore+1:end,:);
Orbity1 = Orbity0(1:BPMIndexBefore,:);
Orbity2 = Orbity0(BPMIndexBefore+1:end,:);

%correlation matrix
Xx = pinv(Orbitx1)' * Orbitx2';
Xy = pinv(Orbity1)' * Orbity2';

Bx = this.state.bpms.X;
Bx1 = Bx(1:BPMIndexBefore,:);
Bx2 = Bx(BPMIndexBefore+1:end,:);

By = this.state.bpms.Y;
By1 = By(1:BPMIndexBefore,:);
By2 = By(BPMIndexBefore+1:end,:);

Rx = Bx1' * Xx - Bx2';
Ry = By1' * Xy - By2';

Bx_JR = zeros (size(this.state.bpms.X));
Bx_JR(BPMIndexBefore+1:end,:) = Rx';
this.state.bpms.X = Bx_JR;

By_JR = zeros (size(this.state.bpms.Y));
By_JR(BPMIndexBefore+1:end,:) = Ry';
this.state.bpms.Y = By_JR;

S = State(this.state);

