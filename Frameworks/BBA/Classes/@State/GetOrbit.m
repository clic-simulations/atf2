function ORBIT = GetOrbit(this, BPMS, mode)
  if nargin==1
    NAMES = GetBpms(this);
  else
    if iscell(BPMS)
      NAMES = BPMS;
    else
      NAMES = { BPMS(:).name };
    end
  end
  if nargin < 3
    mode = 1;
  end
  X = this.state.bpms.X';
  Y = this.state.bpms.Y';
  Z = this.state.bpms.Z;
  TMIT = this.state.bpms.TMIT';
  ISOK = ones(size(TMIT));
  stdX = zeros(size(X,2),1);
  stdY = zeros(size(Y,2),1);
  N = zeros(size(Y,2),1);
  if size(X,1) > 1 % filtering needed !!!
    switch mode
      % simple average
      case 0
        X = median(X);
        Y = median(Y);
        stdX = std(X);
        stdY = std(Y);
        TMIT = median(TMIT);
        ISOK = ones(size(TMIT));
        N = ones(size(N));
      % see description
      case 1
        nsamp = size(X,1); % nsamples
        bpmres = 50; % 50 um
        cut = 10; % cuts at cut*bpmres
        isok_threshold = 0.5; % the BPM is OK if at least 0.5*nsamp (50%) measurements are acceptable
        avgX = median(X);
        avgY = median(Y);
        % S = 1 means BPM reading < cut*bpmres far from the average
        % basically, S=1 are the good ones
        Sx = abs(X-repmat(avgX, nsamp, 1)) < (cut*bpmres);
        Sy = abs(Y-repmat(avgY, nsamp, 1)) < (cut*bpmres);
        ISOKx = sum(Sx) >= round(isok_threshold*nsamp);
        ISOKy = sum(Sy) >= round(isok_threshold*nsamp);
        % a bpm is failed also if std(over nsamp) is 0.0
        ISOKx = ISOKx .* (std(X)~=0.0);
        ISOKy = ISOKy .* (std(Y)~=0.0);
        % a bpm is failed if its measurement is Nan
        ISOKx = ISOKx .* (sum(isnan(X))==0);
        ISOKy = ISOKy .* (sum(isnan(Y))==0);
        % now it calculates the new mask of good measurements
        newSx = Sx .* repmat(ISOKx, nsamp, 1);
        newSy = Sy .* repmat(ISOKy, nsamp, 1);
        % it eliminates the NaN
        X(isnan(X))=0;
        Y(isnan(Y))=0;
        %%% now it calculates the new averages
        X2 = sum(X.*X.*newSx) ./ sum(newSx);
        Y2 = sum(Y.*Y.*newSy) ./ sum(newSy);
        X = sum(X.*newSx) ./ sum(newSx);
        Y = sum(Y.*newSy) ./ sum(newSy);
        %stdX = sqrt((X2 - X.*X) .* sum(newSx) ./ (sum(newSx)-1));
        %stdY = sqrt((Y2 - Y.*Y) .* sum(newSy) ./ (sum(newSy)-1));
        stdX = sqrt((X2 - X.*X) ./ (sum(newSx)-1));
        stdY = sqrt((Y2 - Y.*Y) ./ (sum(newSy)-1));
        % it eliminates again the NaN
        X(isnan(X))=0; % there might be NaN is sum(newS) is zero - no problem
        Y(isnan(Y))=0; % there might be NaN is sum(newS) is zero - no problem
        stdX(isnan(stdX))=0; % there might be NaN is sum(newS) is zero - no problem
        stdY(isnan(stdY))=0; % there might be NaN is sum(newS) is zero - no problem
        ISOK = ISOKx .* ISOKy;
        TMIT = median(TMIT);
        ISOK = ISOK .* (TMIT ~= 0.0);
        N = sum(newSx) .* ISOK;
      otherwise
        error(sprintf('my_filter_bpms() : mode %d not recognized!', mode));
    end
  end
  % packs the results
  nNAMES = length(NAMES);
  ORBIT.X = zeros(nNAMES,1);
  ORBIT.Y = zeros(nNAMES,1);
  ORBIT.Z = zeros(nNAMES,1);
  ORBIT.N = N;
  ORBIT.stdX = zeros(nNAMES,1);
  ORBIT.stdY = zeros(nNAMES,1);
  ORBIT.TMIT = zeros(nNAMES,1);
  ORBIT.ISOK = zeros(nNAMES,1);
  for bpm = 1:nNAMES
      % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
      % find the index of the current bpm and its reading
      bpm_idx = find(strcmp(this.state.bpms.name, NAMES{bpm}));
      ORBIT.X(bpm) = X(bpm_idx);
      ORBIT.Y(bpm) = Y(bpm_idx);
      ORBIT.Z(bpm) = Z(bpm_idx);
      ORBIT.stdX(bpm) = stdX(bpm_idx);
      ORBIT.stdY(bpm) = stdY(bpm_idx);
      ORBIT.TMIT(bpm) = TMIT(bpm_idx);
      ORBIT.ISOK(bpm) = ISOK(bpm_idx);
   end
end
