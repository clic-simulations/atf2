function Load(this, filename)
  if ischar(filename) && exists(filename, 'file')
    load(filename);
    this.state = state;
  else
    error(['State::Load(\'' filename '\') failed' ]);
  end
end
