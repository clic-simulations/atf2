function obj = loadobj(a)
  if ~isfield(a, 'state')
    a.state = struct;
  end
  if ~isfield(a, 'nsamples')
    a.nsamples = 10;
  end
  obj = class(a, 'InterfaceATF2');
