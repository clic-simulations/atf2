function Restore(this, State, CORRS)
  state = GetStruct(State);
  if nargin < 3
    error('at least two arguments are required: state, CORRS (names) [optional KLYS]');
  end
  debug = 0;
  if ~debug
    if length(CORRS) > 0
      tic;
      current_state = struct;
      current_state = atf2_get_correctors(current_state);
      corr_names = [];
      corr_values = [];
      for i = 1:length(CORRS)
        idx = find(strcmp(CORRS{i}, state.mags.name));
        current_idx = find(strcmp(CORRS{i}, current_state.mags.name));
        current_value = current_state.mags.BDES(current_idx);
        old_value = state.mags.BDES(idx);
        if abs(current_value - old_value) > 5e-6
          fprintf('Restoring corrector : %s\n', char(state.mags.name(idx)))
          corr_names = [ corr_names ; state.mags.name(idx) ];
          corr_values = [ corr_values ; old_value ];
        end
      end
      if length(corr_names)>0
        SetCorrs(this, corr_names, corr_values);
        WriteLog('InterfaceATF2::Restore()_Correctors', toc);
      end
    end
  end
end
