function M = InterfaceATF2(nsamples)
  M = struct;
  if nargin == 1
    M.nsamples = nsamples;
  else
    warning('InterfaceATF2::InterfaceATF2: `nsamples` not specified, using 20');
    M.nsamples = 20;
  end
  M.state = struct;
  M = class(M, 'InterfaceATF2');
end
