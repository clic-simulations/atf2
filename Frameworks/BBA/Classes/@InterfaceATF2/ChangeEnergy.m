function I = ChangeEnergy(this, delta_freq)

% delta_freq in kHz, -5 to 5 is a good range.
% always absolute from nominal.

tic


% test for valid range and integer value
if (delta_freq ~= int32(delta_freq))
    error ('DR frequency change is not an integer: %s', num2str(delta_freq))
end

if (-5 > delta_freq || delta_freq > 5)
    error ('DR frequency change is out of a safe range: %d',delta_freq)
end

if (delta_freq ~= 0)
    
  % switch ramp on
  %pv_name_switch = 'atf:rfRamp:sw';
  system('caput atf:rfRamp:sw 1');
  pause(2.0)

  % change frequency
  %sv_name_freq = 'atf:rfRamp:freq:set';
  system(strjoin(['caput atf:rfRamp:freq:set ' num2str(delta_freq)]));
  pause(2.0)

else 

  system('caput atf:rfRamp:sw 0');
  pause(2.0)

  %switch ramp off, t
  system('caput atf:rfRamp:freq:set 0');
  pause(2.0)

end

WriteLog('InterfaceATF2::ChangeEnergy()', toc);
I = this;

end
