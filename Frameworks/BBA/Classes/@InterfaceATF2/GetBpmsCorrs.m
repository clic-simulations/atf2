function S = GetBpmsCorrs(this)
  disp('InterfaceATF2::GetBpmsCorrs()');
  tic;
  this.state = atf2_get_bpms_correctors(this.state, this.nsamples);
%  while 1
%    TEMP = this.state.bpms.STAT;
%    TEMP(isnan(TEMP))=1;
%    if sum(sum(TEMP))/this.nsamples/size(TEMP,1) > 0.8
%      break
%    end
%    disp('Reading the orbit again...');
%  end
  WriteLog('InterfaceATF2::GetBpmsCorrs()', toc);
  S = State(this.state);
end
