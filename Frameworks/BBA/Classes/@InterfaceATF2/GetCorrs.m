function S = GetCorrs(this)
  disp('InterfaceATF2::GetCorrs()');
  tic;
  this.state = atf2_get_correctors(this.state);
  WriteLog('InterfaceATF2::GetCorrs()', toc);
  S = State(this.state);
end
