bpm_mask = [ 1:21 26 27 28 39:49 ];

Interface = InterfaceATF2(20);

S0 = GetMachine(Interface);
O0 = GetOrbit(S0);

ChangeEnergy(Interface, -2);

S1 = GetMachine(Interface);
O1 = GetOrbit(S1);

ChangeEnergy(Interface, 0);

state_disp = atf2_get_target_dispersion(-2);
SD = State(state_disp);
OD = GetOrbit(SD);

DY= (O1.Y - O0.Y);
DX= (O1.X - O0.X);

figure(1)
clf
plot(DX(bpm_mask),'b-')
hold on
plot(OD.X(bpm_mask),'r-')

figure(2)
clf
plot(DY(bpm_mask),'b-')
hold on
plot(DY(bpm_mask)*0,'r-')

normxy = [ norm(DX - OD.X) norm(DY) ]

