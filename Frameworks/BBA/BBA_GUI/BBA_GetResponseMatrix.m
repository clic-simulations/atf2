function [Rx, Ry, XCORR, YCORR, R0, R1, R2] = BBA_GetResponseMatrix()
global BBA_DATA

if BBA_MatrixSanityCheck()
    %
    wgt_orb_x = BBA_DATA.wgt_orb_x * BBA_DATA.do_apply_orbit_x;
    wgt_orb_y = BBA_DATA.wgt_orb_y * BBA_DATA.do_apply_orbit_y;
    wgt_dfs_x = BBA_DATA.wgt_dfs_x * BBA_DATA.do_apply_dfs_x;
    wgt_dfs_y = BBA_DATA.wgt_dfs_y * BBA_DATA.do_apply_dfs_y;
    wgt_wfs_x = BBA_DATA.wgt_wfs_x * BBA_DATA.do_apply_wfs_x;
    wgt_wfs_y = BBA_DATA.wgt_wfs_y * BBA_DATA.do_apply_wfs_y;
    wgt_ff_x  = BBA_DATA.wgt_ff_x * BBA_DATA.do_apply_ff_x;
    wgt_ff_y  = BBA_DATA.wgt_ff_y * BBA_DATA.do_apply_ff_y;
    %
    svd_Ox = BBA_DATA.svd_nsingularvalues_x;
    svd_Oy = BBA_DATA.svd_nsingularvalues_y;
    svd_Dx = BBA_DATA.svd_nsingularvalues_x;
    svd_Dy = BBA_DATA.svd_nsingularvalues_y;
    svd_Wx = BBA_DATA.svd_nsingularvalues_x;
    svd_Wy = BBA_DATA.svd_nsingularvalues_y;
    %
    
    %%%% load the response matrices
    
    load(BBA_DATA.Response0_file, '-mat')
    Response0 = Response;
    
    % dispersion
    if wgt_dfs_x > 0 | wgt_dfs_y > 0
        load(BBA_DATA.Response1_file, '-mat')
        Response1 = Response;
    else
        Response1 = Response0;
    end
    
    % wake
    if wgt_wfs_x > 0 | wgt_wfs_y > 0
        load(BBA_DATA.Response2_file, '-mat')
        Response2 = Response;
    else
        Response2 = Response0;
    end
    
    %%%% figure out which correctors and bpms should be used
    
    CORS_Orbit = FigureOutCors(Response0, 128, 64);
    CORS_Dfs   = FigureOutCors(Response1,  32, 16);
    CORS_Wfs   = FigureOutCors(Response2,   8,  4);
    CORS_FF    = FigureOutCors(Response0,   2,  1);
    
    BPMS_Orbit = FigureOutBpms(Response0, 128, 64);
    BPMS_Dfs   = FigureOutBpms(Response1,  32, 16);
    BPMS_Wfs   = FigureOutBpms(Response2,   8,  4);
    BPMS_FF    = FigureOutBpms(Response0,   2,  1);
    
    %%%% get a list of all correctors in use, and their indexes
    
    XCORR = unique(horzcat({ CORS_Orbit.X.name }, { CORS_Dfs.X.name }, { CORS_Wfs.X.name } ));
    YCORR = unique(horzcat({ CORS_Orbit.Y.name }, { CORS_Dfs.Y.name }, { CORS_Wfs.Y.name } ));
    
    if BBA_DATA.do_include_coupled_terms
        XCORR = unique(horzcat(XCORR, YCORR));
        YCORR = XCORR;
    end
    
    % sort XCORR by Z not by name
    tempx = zeros(length(XCORR),1);
    tempy = zeros(length(YCORR),1);
    for i=1:length(XCORR); C = GetCorrectorsData(BBA_DATA.State, XCORR(i)); tempx(i) = C.Z; end
    for i=1:length(YCORR); C = GetCorrectorsData(BBA_DATA.State, YCORR(i)); tempy(i) = C.Z; end
    [S,Ix] = sort(tempx);
    [S,Iy] = sort(tempy);
    XCORR = XCORR(Ix);
    YCORR = YCORR(Iy);
    
    %%%% build the complete, big, response matrices
    
    R0.Rx = zeros(length([ BPMS_Orbit.X.R0_index ]), length(XCORR));
    R1.Rx = zeros(length([ BPMS_Dfs.X.R0_index ]),   length(XCORR));
    R2.Rx = zeros(length([ BPMS_Wfs.X.R0_index ]),   length(XCORR));
    
    R0.Ry = zeros(length([ BPMS_Orbit.Y.R0_index ]), length(YCORR));
    R1.Ry = zeros(length([ BPMS_Dfs.Y.R0_index ]),   length(YCORR));
    R2.Ry = zeros(length([ BPMS_Wfs.Y.R0_index ]),   length(YCORR));
    
    for i=1:length(XCORR)
        Oi = find(strcmp(XCORR{i}, { CORS_Orbit.X.name }));
        Di = find(strcmp(XCORR{i}, { CORS_Dfs.X.name }));
        Wi = find(strcmp(XCORR{i}, { CORS_Wfs.X.name }));
        if Oi; R0.Rx(:,i) = Response0.Rx([ BPMS_Orbit.X.R0_index ], CORS_Orbit.X(Oi).R0_index); end
        if Di; R1.Rx(:,i) = Response1.Rx([ BPMS_Dfs.X.R0_index ],   CORS_Dfs.X(Di).R0_index) - Response0.Rx([ BPMS_Dfs.X.R0_index ], CORS_Dfs.X(Di).R0_index); end
        if Wi; R2.Rx(:,i) = Response2.Rx([ BPMS_Wfs.X.R0_index ],   CORS_Wfs.X(Wi).R0_index) - Response0.Rx([ BPMS_Wfs.X.R0_index ], CORS_Wfs.X(Wi).R0_index); end
    end
    
    for i=1:length(YCORR)
        Oi = find(strcmp(YCORR{i}, { CORS_Orbit.Y.name }));
        Di = find(strcmp(YCORR{i}, { CORS_Dfs.Y.name }));
        Wi = find(strcmp(YCORR{i}, { CORS_Wfs.Y.name }));
        if Oi; R0.Ry(:,i) = Response0.Ry([ BPMS_Orbit.Y.R0_index ], CORS_Orbit.Y(Oi).R0_index); end
        if Di; R1.Ry(:,i) = Response1.Ry([ BPMS_Dfs.Y.R0_index ],   CORS_Dfs.Y(Di).R0_index) - Response0.Ry([ BPMS_Dfs.Y.R0_index ], CORS_Dfs.Y(Di).R0_index); end
        if Wi; R2.Ry(:,i) = Response2.Ry([ BPMS_Wfs.Y.R0_index ],   CORS_Wfs.Y(Wi).R0_index) - Response0.Ry([ BPMS_Wfs.Y.R0_index ], CORS_Wfs.Y(Wi).R0_index); end
    end
    
    %%%% get the golden orbit out of State
    
    [U,S,V] = svd(R0.Rx); for i=(svd_Ox+1):min(size(S)); S(i,i) = 0; end; R0.Rx = U*S*V';
    [U,S,V] = svd(R0.Ry); for i=(svd_Oy+1):min(size(S)); S(i,i) = 0; end; R0.Ry = U*S*V';
    Rx = [ wgt_orb_x * R0.Rx ];
    Ry = [ wgt_orb_y * R0.Ry ];
    
    if wgt_dfs_x > 0 | wgt_dfs_y > 0
        [U,S,V] = svd(R1.Rx); for i=(svd_Dx+1):min(size(S)); S(i,i) = 0; end; R1.Rx = U*S*V';
        [U,S,V] = svd(R1.Ry); for i=(svd_Dy+1):min(size(S)); S(i,i) = 0; end; R1.Ry = U*S*V';
    end
    if wgt_dfs_x > 0; Rx = [ Rx ; wgt_dfs_x * R1.Rx ]; end
    if wgt_dfs_y > 0; Ry = [ Ry ; wgt_dfs_y * R1.Ry ]; end
    
    if wgt_wfs_x > 0 | wgt_wfs_y > 0
        [U,S,V] = svd(R2.Rx); for i=(svd_Wx+1):min(size(S)); S(i,i) = 0; end; R2.Rx = U*S*V';
        [U,S,V] = svd(R2.Ry); for i=(svd_Wy+1):min(size(S)); S(i,i) = 0; end; R2.Ry = U*S*V';
    end
    if wgt_wfs_x > 0; Rx = [ Rx ; wgt_wfs_x * R2.Rx ]; end
    if wgt_wfs_y > 0; Ry = [ Ry ; wgt_wfs_y * R2.Ry ]; end
    
end
