function BBA_DisableGUI(handles)
    set(handles.text_Mode, 'Enable', 'off');
    set(handles.edit_dir, 'Enable', 'off');
    set(handles.text_machine_status, 'Enable', 'off');
    set(handles.list_corr, 'Enable', 'off');
    set(handles.list_bpm, 'Enable', 'off');
    set(handles.cb_bba_orbit_x, 'Enable', 'off');
    set(handles.cb_bba_orbit_y, 'Enable', 'off');
    set(handles.cb_bba_dfs_x, 'Enable', 'off');
    set(handles.cb_bba_dfs_y, 'Enable', 'off');
    set(handles.cb_bba_wfs_x, 'Enable', 'off');
    set(handles.cb_bba_wfs_y, 'Enable', 'off');
    set(handles.cb_bba_ff_x, 'Enable', 'off');
    set(handles.cb_bba_ff_y, 'Enable', 'off');
    set(handles.cb_plot_orbit, 'Enable', 'off');
    set(handles.cb_plot_dispersion, 'Enable', 'off');
    set(handles.cb_plot_wakes, 'Enable', 'off');
    set(handles.cb_plot_orbitC, 'Enable', 'off');
    set(handles.cb_plot_dispersionC, 'Enable', 'off');
    set(handles.cb_plot_wakesC, 'Enable', 'off');
    set(handles.cb_plot_corr, 'Enable', 'off');
    set(handles.edit_response0, 'Enable', 'off');
    set(handles.edit_response1, 'Enable', 'off');
    set(handles.edit_response2, 'Enable', 'off');
    set(handles.edit_change_energy, 'Enable', 'off');
    set(handles.edit_reset_energy, 'Enable', 'off');
    set(handles.edit_change_charge, 'Enable', 'off');
    set(handles.edit_reset_charge, 'Enable', 'off');
    set(handles.edit_gainx, 'Enable', 'off');
    set(handles.edit_gainy, 'Enable', 'off');
    set(handles.edit_orbitx, 'Enable', 'off');
    set(handles.edit_orbity, 'Enable', 'off');
    set(handles.edit_dfsx, 'Enable', 'off');
    set(handles.edit_dfsy, 'Enable', 'off');
    set(handles.edit_wfsx, 'Enable', 'off');
    set(handles.edit_wfsy, 'Enable', 'off');
    set(handles.edit_ffx, 'Enable', 'off');
    set(handles.edit_ffy, 'Enable', 'off');
    set(handles.edit_svdx, 'Enable', 'off');
    set(handles.edit_svdy, 'Enable', 'off');
    set(handles.cb_bba_coupling, 'Enable', 'off');
