function R0 = BBA_GetResponse_Orbit()
  global BBA_DATA

  load(BBA_DATA.Response0_file, '-mat')
  R0 = Response;

  CORS_Orbit = FigureOutCors(R0, 128, 64);
  BPMS_Orbit = FigureOutBpms(R0, 128, 64);
  
  %%%% apply masks to the response matrices

  R0.Rx = R0.Rx( [ BPMS_Orbit.X.R0_index ], [ CORS_Orbit.X.R0_index ]);
  R0.Ry = R0.Ry( [ BPMS_Orbit.Y.R0_index ], [ CORS_Orbit.Y.R0_index ]);

end
