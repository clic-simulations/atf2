function varargout = BBA_Choose_function_of_corrs(varargin)
% BBA_CHOOSE_FUNCTION_OF_CORRS M-file for BBA_Choose_function_of_corrs.fig
%      BBA_CHOOSE_FUNCTION_OF_CORRS by itself, creates a new BBA_CHOOSE_FUNCTION_OF_CORRS or raises the
%      existing singleton*.
%
%      H = BBA_CHOOSE_FUNCTION_OF_CORRS returns the handle to a new BBA_CHOOSE_FUNCTION_OF_CORRS or the handle to
%      the existing singleton*.
%
%      BBA_CHOOSE_FUNCTION_OF_CORRS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BBA_CHOOSE_FUNCTION_OF_CORRS.M with the given input arguments.
%
%      BBA_CHOOSE_FUNCTION_OF_CORRS('Property','Value',...) creates a new BBA_CHOOSE_FUNCTION_OF_CORRS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before BBA_Choose_function_of_corrs_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to BBA_Choose_function_of_corrs_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help BBA_Choose_function_of_corrs

% Last Modified by GUIDE v2.5 19-Mar-2014 11:02:38

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @BBA_Choose_function_of_corrs_OpeningFcn, ...
                   'gui_OutputFcn',  @BBA_Choose_function_of_corrs_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before BBA_Choose_function_of_corrs is made visible.
function BBA_Choose_function_of_corrs_OpeningFcn(hObject, eventdata, handles, varargin)
  handles.output = -1;
  set(handles.cb_Ox,'Value', 1);
  set(handles.cb_Dx,'Value', 1);
  set(handles.cb_Wx,'Value', 1);
  set(handles.cb_Fx,'Value', 0);
  guidata(hObject, handles);
  set(handles.figure1,'WindowStyle','modal')
  uiwait(handles.figure1);
  
% Insert custom Title and Text if specified by the user
% Hint: when choosing keywords, be sure they are not easily confused 
% with existing figure properties.  See the output of set(figure) for
% a list of figure properties.

% --- Outputs from this function are returned to the command line.
function varargout = BBA_Choose_function_of_corrs_OutputFcn(hObject, eventdata, handles)
  if handles.output ~= -1
    ret = 0;
    ret = ret + (128 + 64) * get(handles.cb_Ox,'Value');
    ret = ret +  (32 + 16) * get(handles.cb_Dx,'Value');
    ret = ret +    (8 + 4) * get(handles.cb_Wx,'Value');
    ret = ret +    (2 + 1) * get(handles.cb_Fx,'Value');
    varargout{1} = ret;
  else 
    varargout{1} = -1;
  end
  delete(handles.figure1);

% --- Executes on button press in pb_Apply.
function pb_Apply_Callback(hObject, eventdata, handles)
% hObject    handle to pb_Apply (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.output = 1;
guidata(hObject, handles);
uiresume(handles.figure1);

% --- Executes on button press in pb_Cancel.
function pb_Cancel_Callback(hObject, eventdata, handles)
handles.output = -1;
guidata(hObject, handles);
uiresume(handles.figure1);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isequal(get(handles.figure1, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
    uiresume(handles.figure1);
else
    % The GUI is no longer waiting, just close it
    delete(handles.figure1);
end


% --- Executes on key press over figure1 with no controls selected.
function figure1_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Check for "enter" or "escape"
if isequal(get(hObject,'CurrentKey'),'escape')
    % User said no by hitting escape
    handles.output = -1;
    
    % Update handles structure
    guidata(hObject, handles);
    
    uiresume(handles.figure1);
end    
    
if isequal(get(hObject,'CurrentKey'),'return')
    uiresume(handles.figure1);
end    


% --- Executes on button press in cb_Ox.
function cb_Ox_Callback(hObject, eventdata, handles)
% hObject    handle to cb_Ox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_Ox

% --- Executes on button press in cb_Dx.
function cb_Dx_Callback(hObject, eventdata, handles)
% hObject    handle to cb_Dx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_Dx


% --- Executes on button press in cb_Wx.
function cb_Wx_Callback(hObject, eventdata, handles)
% hObject    handle to cb_Wx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_Wx


% --- Executes on button press in cb_Fx.
function cb_Fx_Callback(hObject, eventdata, handles)
% hObject    handle to cb_Fx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_Fx