function [Wake,State2] = BBA_MeasureWakefield(State0)
  global BBA_DATA

  Interface = BBA_DATA.Interface;

  load(BBA_DATA.Response2_file, '-mat')
  R2 = Response;

  CORS_Wfs = FigureOutCors(R2, 8, 4);
  BPMS_Wfs = FigureOutBpms(R2, 8, 4);

  Orbit0X = GetOrbit(State0, { BPMS_Wfs.X.name } );
  Orbit0Y = GetOrbit(State0, { BPMS_Wfs.Y.name } );

  %%%% read the disp to be corrected
  disp('Changing the bunch charge...');
  eval(BBA_DATA.ChangeCharge_cmd);  
  BBA_DATA.Interface = Interface;
  disp('Reading wakefield orbit...');
  State2 = GetBpms(BBA_DATA.Interface);
  disp('Resetting the bunch charge...');
  eval(BBA_DATA.ResetCharge_cmd);  
  BBA_DATA.Interface = Interface;
  Orbit2X = GetOrbit(State2, { BPMS_Wfs.X.name } );
  Orbit2Y = GetOrbit(State2, { BPMS_Wfs.Y.name } );
  Wake.X = Orbit2X.X - Orbit0X.X;
  Wake.Y = Orbit2Y.Y - Orbit0Y.Y;
  Wake.XZ = Orbit2X.Z;
  Wake.YZ = Orbit2Y.Z;
