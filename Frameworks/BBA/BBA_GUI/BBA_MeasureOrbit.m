function [Orbit, State0] = BBA_MeasureOrbit()
  global BBA_DATA

  load(BBA_DATA.Response0_file, '-mat')
  R0 = Response;

  CORS_Orbit = FigureOutCors(R0, 128, 64);
  BPMS_Orbit = FigureOutBpms(R0, 128, 64);

  GoldenX = GetOrbit(BBA_DATA.State, { BPMS_Orbit.X.name } );
  GoldenY = GetOrbit(BBA_DATA.State, { BPMS_Orbit.Y.name } );

  %%%% read the orbit to be corrected
  
  disp('Reading orbit...');
  State0 = GetBpms(BBA_DATA.Interface);
  Orbit0x = GetOrbit(State0, { BPMS_Orbit.X.name } );
  Orbit0y = GetOrbit(State0, { BPMS_Orbit.Y.name } );
  Orbit.X = Orbit0x.X - GoldenX.X;
  Orbit.Y = Orbit0y.Y - GoldenY.Y;
  Orbit.XZ = Orbit0x.Z;
  Orbit.YZ = Orbit0y.Z;
