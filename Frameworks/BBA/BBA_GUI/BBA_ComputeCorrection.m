function BBA_ComputeCorrection()
global BBA_DATA BBA_PLOTS

if BBA_MatrixSanityCheck()
    
    [Rx, Ry, XCORR, YCORR, R0, R1, R2] = BBA_GetResponseMatrix();
    
    wgt_orb_x = BBA_DATA.wgt_orb_x * BBA_DATA.do_apply_orbit_x;
    wgt_orb_y = BBA_DATA.wgt_orb_y * BBA_DATA.do_apply_orbit_y;
    wgt_dfs_x = BBA_DATA.wgt_dfs_x * BBA_DATA.do_apply_dfs_x;
    wgt_dfs_y = BBA_DATA.wgt_dfs_y * BBA_DATA.do_apply_dfs_y;
    wgt_wfs_x = BBA_DATA.wgt_wfs_x * BBA_DATA.do_apply_wfs_x;
    wgt_wfs_y = BBA_DATA.wgt_wfs_y * BBA_DATA.do_apply_wfs_y;
    wgt_ff_x  = BBA_DATA.wgt_ff_x * BBA_DATA.do_apply_ff_x;
    wgt_ff_y  = BBA_DATA.wgt_ff_y * BBA_DATA.do_apply_ff_y;
    
    [Orbit,State0] = BBA_MeasureOrbit();
    bx = [ wgt_orb_x * Orbit.X ];
    by = [ wgt_orb_y * Orbit.Y ];
    
    Disp = []; State1 = [];
    if wgt_dfs_x > 0 | wgt_dfs_y > 0
        [Disp, State1] = BBA_MeasureDispersion(State0);
    end
    if wgt_dfs_x > 0; bx = [ bx ; wgt_dfs_x * Disp.X ]; end
    if wgt_dfs_y > 0; by = [ by ; wgt_dfs_y * Disp.Y ]; end
    
    Wake = []; State2 = [];
    if wgt_wfs_x > 0 | wgt_wfs_y > 0
        [Wake, State2] = BBA_MeasureWakefield(State0);
    end
    if wgt_wfs_x > 0; bx = [ bx ; wgt_wfs_x * Wake.X ]; end
    if wgt_wfs_y > 0; by = [ by ; wgt_wfs_y * Wake.Y ]; end
    
    %% compute the correction
    
    Interface = BBA_DATA.Interface;
    
    gain_x = BBA_DATA.gain_x;
    gain_y = BBA_DATA.gain_y;
    
    if BBA_DATA.do_include_coupled_terms
        
        Corrx = -gain_x * [ Rx ; Ry ] \ [ bx ; by ];
        
        %  if BBA_ApproveCorrection()
        
        VaryCorrs(Interface, XCORR, Corrx);
        
    else
        
        Corrx = -gain_x * Rx \ bx;
        Corry = -gain_y * Ry \ by;
        
        %  if BBA_ApproveCorrection()
        
        VaryCorrs(Interface, horzcat(XCORR, YCORR), [ Corrx ; Corry ]);
        
    end

  BBA_PLOTS.nsteps = BBA_PLOTS.nsteps+1;
  BBA_PLOTS.data(BBA_PLOTS.nsteps).Orbit = Orbit;
  BBA_PLOTS.data(BBA_PLOTS.nsteps).Disp = Disp;
  BBA_PLOTS.data(BBA_PLOTS.nsteps).Wake = Wake;
  BBA_PLOTS.data(BBA_PLOTS.nsteps).R0 = R0;
  BBA_PLOTS.data(BBA_PLOTS.nsteps).R1 = R1;
  BBA_PLOTS.data(BBA_PLOTS.nsteps).R2 = R2;
  if BBA_DATA.do_include_coupled_terms
    BBA_PLOTS.data(BBA_PLOTS.nsteps).coupled = true;
    BBA_PLOTS.data(BBA_PLOTS.nsteps).Corrs.X = Corrx;
    BBA_PLOTS.data(BBA_PLOTS.nsteps).Corrs.Y = Corrx;
    BBA_PLOTS.data(BBA_PLOTS.nsteps).Corr_names = XCORR;    
  else
    BBA_PLOTS.data(BBA_PLOTS.nsteps).coupled = false;
    BBA_PLOTS.data(BBA_PLOTS.nsteps).Corrs.X = Corrx;
    BBA_PLOTS.data(BBA_PLOTS.nsteps).Corrs.Y = Corry;
    BBA_PLOTS.data(BBA_PLOTS.nsteps).Corr_names = horzcat(XCORR, YCORR);
  end

  save(sprintf('BBA_DATA_%s.mat',strrep(datestr(clock, 30), 'T', '_')), 'BBA_DATA', 'BBA_PLOTS', 'State0', 'State1', 'State2', 'Rx', 'Ry', '-mat');
  
  BBA_Plot_Orbits
  BBA_Plot_Convergence
  BBA_Plot_Correctors

  end
