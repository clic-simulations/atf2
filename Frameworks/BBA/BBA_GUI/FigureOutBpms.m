function BPMS = FigureOutBpms(R0, X_mask, Y_mask)
  global BBA_DATA
  BPMS.X = struct('name', [], 'R0_index', []);
  BPMS.Y = struct('name', [], 'R0_index', []);
  R0x_i = 1;
  R0y_i = 1;
  for i=1:length(BBA_DATA.Bpms)
    bpm_name = BBA_DATA.Bpms(i).name;
    bpm_mask = BBA_DATA.Bpms(i).mask;
    bpm_index = find(fnmatch(bpm_name, R0.BPMS));
    if ~isempty(bpm_index)
      if bitand(bpm_mask, X_mask) % employed for Dx
        bpm_index = unique(bpm_index);
        BPMS.X(R0x_i).name = bpm_name;
        BPMS.X(R0x_i).R0_index = bpm_index;
        R0x_i = R0x_i + 1;
      end
      if bitand(bpm_mask, Y_mask) % employed for Dy
        bpm_index = unique(bpm_index);
        BPMS.Y(R0y_i).name = bpm_name;
        BPMS.Y(R0y_i).R0_index = bpm_index;
        R0y_i = R0y_i + 1;
      end
    end
  end
