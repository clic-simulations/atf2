function BBA_Plot_Correctors()
global BBA_PLOTS

figure(4);

CX =  BBA_PLOTS.data(BBA_PLOTS.nsteps).Corrs.X;
% should be CORS Z-pos, currently not available
ZCX = 1:length( CX );

CY =  BBA_PLOTS.data(BBA_PLOTS.nsteps).Corrs.Y;
% should be CORS Z-pos, currently not available
  ZCY = length(CX) + (1:length( CY));

% temp: info not available
C_MAX_X = ones(size(CX))*max(abs(CX));
C_MAX_Y = ones(size(CY))*max(abs(CY));

bar_width = 0.8;
bar_X = ZCX+0.001*randn(size(ZCX)); % to avoid problems with same z
bar_Y = ZCY+0.001*randn(size(ZCY)); % to avoid problems with same z

% X
hb = bar(bar_X, C_MAX_X, bar_width,'g', 'EdgeColor', [0,1,0]);
hold on;
hb = bar(bar_X, -C_MAX_X, bar_width,'g', 'EdgeColor', [0,1,0]);
hb = bar(bar_X, CX, bar_width, 'r', 'EdgeColor', [1,0,0]);
hold off;

hold on;

% Y
hb = bar(bar_Y, C_MAX_Y, bar_width,'g', 'EdgeColor', [0,1,0]);
hold on;
hb = bar(bar_Y, -C_MAX_Y, bar_width,'g', 'EdgeColor', [0,1,0]);
hb = bar(bar_Y, CY, bar_width, 'b', 'EdgeColor', [0,0,1]);
hold off;

names = BBA_PLOTS.data(BBA_PLOTS.nsteps).Corr_names;

for i = 1:length(names)
    text(i,0,names(i),'Rotation',90,'Interpreter','none')
end
%set(gca,'Xtick',ZCX)
%set(gca,'XtickLabel',names)
xlabel('Corrector name')
ylabel('CORR BDES_{NEW} [kGm]')
  hl = legend('NEW', '', 'X MAX', '', '', 'Y MAX', 'Location', 'SouthEast');

  
end
 
