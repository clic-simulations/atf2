function varargout = Edit_corr_POPUP(varargin)
% EDIT_CORR_POPUP M-file for Edit_corr_POPUP.fig
%      EDIT_CORR_POPUP, by itself, creates a new EDIT_CORR_POPUP or raises the existing
%      singleton*.
%
%      H = EDIT_CORR_POPUP returns the handle to a new EDIT_CORR_POPUP or the handle to
%      the existing singleton*.
%
%      EDIT_CORR_POPUP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EDIT_CORR_POPUP.M with the given input arguments.
%
%      EDIT_CORR_POPUP('Property','Value',...) creates a new EDIT_CORR_POPUP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Edit_corr_POPUP_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Edit_corr_POPUP_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Edit_corr_POPUP

% Last Modified by GUIDE v2.5 03-Mar-2014 23:16:41

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Edit_corr_POPUP_OpeningFcn, ...
                   'gui_OutputFcn',  @Edit_corr_POPUP_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Edit_corr_POPUP is made visible.
function Edit_corr_POPUP_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Edit_corr_POPUP (see VARARGIN)

% Choose default command line output for Edit_corr_POPUP
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

set(handles.edit_current, 'String', varargin{1});
set(handles.edit_desired, 'String', varargin{1});
set(handles.textTitle, 'String', varargin{2});

% UIWAIT makes Edit_corr_POPUP wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Edit_corr_POPUP_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = str2num(get(handles.edit_desired,'String'));
delete(handles.figure1);

% --- Executes on button press in pb_apply.
function pb_apply_Callback(hObject, eventdata, handles)
% hObject    handle to pb_apply (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.figure1);

% --- Executes on button press in pb_cancel.
function pb_cancel_Callback(hObject, eventdata, handles)
% hObject    handle to pb_cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.edit_desired,'String',get(handles.edit_current,'String'));
close(handles.figure1);


% --- Executes on button press in pb_up.
function pb_up_Callback(hObject, eventdata, handles)
% hObject    handle to pb_up (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
s = str2num(get(handles.edit_desired,'String')) + str2num(get(handles.edit_current,'String')) * 0.1;
set(handles.edit_desired,'String',s);

% --- Executes on button press in pb_down.
function pb_down_Callback(hObject, eventdata, handles)
% hObject    handle to pb_down (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
s = str2num(get(handles.edit_desired,'String')) - str2num(get(handles.edit_current,'String')) * 0.1;
set(handles.edit_desired,'String',s);

function edit_desired_Callback(hObject, eventdata, handles)
% hObject    handle to edit_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_desired as text
%        str2double(get(hObject,'String')) returns contents of edit_desired as a double


% --- Executes during object creation, after setting all properties.
function edit_desired_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_current_Callback(hObject, eventdata, handles)
% hObject    handle to edit_current (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_current as text
%        str2double(get(hObject,'String')) returns contents of edit_current as a double


% --- Executes during object creation, after setting all properties.
function edit_current_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_current (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
    uiresume(hObject);
else
    % The GUI is no longer waiting, just close it
    delete(hObject);
end

