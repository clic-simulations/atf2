# Since there is not even a main linac for ATF2 (test of BDS of ILC), 
# the wake field is set to zero. 

# transverse wakefield
# s is given in micro metres
# return value is in V/pCm^2

# longitudinal wakefield
# s is given in micro metres
# return value is in V/pCm

proc w_long {s} {
    return 0
}

proc w_transv {s} {
    return 0
}
