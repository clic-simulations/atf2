# Since there is not even a main linac for ATF2 (test of BDS of ILC), 
# the long range wakes are not set. 

#
# Use this list to create fields
#

#WakeSet wakelong $cav_modes

#
# Define accelerating structure
#

#InjectorCavityDefine -lambda $lambda \
#	-wakelong wakelong
