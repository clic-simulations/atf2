# time0 est la duree de la simulation en secondes
set time0 0
# Tstep est la duree d'un step
set Tstep [expr 1/1.5]
# nstep est nombre de pulse utilise pour l analyse
set nstep 100
#seed1 est le premier seed (seed1<=seed<=seed1+nseed) 
set seed1 1
# nseed est le nombre de fois ou la simulation est repetee avec une graine differente [default 10]
set nseed 10
# offset_ini is the rms in um of initial offset of quadrupoles, BPMs, bends and sexts [default 100]
set offset_ini 100
# scale_error is the scale error on BPMs reading [default 0.01]
set scale_error 0.01
#dk_over_k is the relative error on quadrupole strengths [default 1e-4]
set dk_over_k 1e-4
# use of the ground motion filter (geophone)
set gm_filter 1
# interpolation method between sensor measurements 
#   'nearest'
#          Return the nearest neighbour.
#    'linear'
#          Linear interpolation from nearest neighbours
#    'pchip'
#          Piece-wise cubic hermite interpolating polynomial
#    'cubic'
#          Cubic interpolation from four nearest neighbours
#    'spline'
#          Cubic spline interpolation-smooth first and second derivatives
#          throughout the curve
set interp_method linear
#bpm resolution [um]
set stripline [expr .5]
set stripline2 [expr .5]
set cband [expr 0.1]
# nominal 0.002 for ipbpm
set ipbpm [expr 0.1] 
#sextupole on (1) or off (0)
set sext 1

#directories needed
set lattice ../Lattices/ATF2
set scripts ./scripts
set plots ./
if {$stripline != $stripline2} {
    set plots plots/offset_ini_$offset_ini-scale_error_$scale_error-dk_$dk_over_k-resolstrip_$stripline-resolstrip2_$stripline2-resolcband_$cband
}
puts "plot dir :$plots"
puts "params : time0:$time0 Tstep:$Tstep nstep:$nstep seed0:$seed1 nseed:$nseed"

source $scripts/init.tcl;
set e0 1.3
set e_initial $e0
proc HCORRECTOR {a b c d } {
}
proc VCORRECTOR {a b c d } {
}
proc Marker {a b } {
}
Girder
#source $lattice/ATF2_UltraLow_v4.2_noMULTS.tcl;
source $lattice/ATF2_v4.0.tcl;
ReferencePoint -sense -1
BeamlineSet -name ATF2

#emitance normalized in e-7 m
set match(emitt_x) [expr 50]
set match(emitt_y) [expr .3]
#beam long in e-6 m
set match(sigma_z) 8000
#energy spread normalized
set match(e_spread) 0.08
set match(phase) 0.0
set charge 2e10
# ATF2 v4.0 beta in m
array set match {beta_x 6.54290 alpha_x 0.99638 beta_y  3.23785 alpha_y -1.9183 }

source $scripts/ilc_beam.tcl;

set n_slice 10
set n 100
set n_total [expr $n * $n_slice]

make_beam_many beam0 $n_slice $n

set n_slice 1
set n 1
set n_total [expr $n * $n_slice]

make_beam_many part $n_slice $n

FirstOrder 1

#delete all misalignements
Zero

set i 0
set seed 0
Octave {
            [E,B_ini]=placet_test_no_correction("ATF2", "beam0","None");
            B_usi_ini=[[B_ini(:,2) B_ini(:,5) B_ini(:,3) B_ini(:,6) B_ini(:,4)]*1e-6 (B_ini(:,1)-$e0)/$e0];
            disp("beam size before waist adjustement")
            std(B_usi_ini)
            qd0ff=placet_get_name_number_list("ATF2","qd0ff");
            klqd0=placet_element_get_attribute("ATF2",qd0ff,"strength");
            klqd0=klqd0(1);
            dklqd0=(-5:5)*.001*klqd0;
            for i=1:length(dklqd0)
                klqd0_i=klqd0+dklqd0(i);
                placet_element_set_attribute("ATF2",qd0ff,"strength",klqd0_i);
                [E,B_ini]=placet_test_no_correction("ATF2", "beam0","None");
                B_usi_ini=[[B_ini(:,2) B_ini(:,5) B_ini(:,3) B_ini(:,6) B_ini(:,4)]*1e-6 (B_ini(:,1)-$e0)/$e0];
                x_qd0(i)=std(B_usi_ini(:,1));
                y_qd0(i)=std(B_usi_ini(:,3));
            end
            qf1ff=placet_get_name_number_list("ATF2","qf1ff");
#            klqf1=placet_element_get_attribute("ATF2",qf1ff,"strength");
#            klqf1=klqf1(1);
#            dklqf1=(-5:5)*.001*klqf1;
#            for i=1:length(dklqf1)
#                klqf1_i=klqf1+dklqf1(i);
#                placet_element_set_attribute("ATF2",qf1ff,"strength",klqf1_i);
#                [E,B_ini]=placet_test_no_correction("ATF2", "beam0","None");
#                B_usi_ini=[[B_ini(:,2) B_ini(:,5) B_ini(:,3) B_ini(:,6) B_ini(:,4)]*1e-6 (B_ini(:,1)-$e0)/$e0];
#                x_qf1(i)=std(B_usi_ini(:,1));
#                y_qf1(i)=std(B_usi_ini(:,3));
#            end
            p=polyfit(dklqd0,y_qd0,2);
            dkqd0_opt=-0.5*p(2)/p(1)
#            p=polyfit(dklqf1,x_qf1,2);
#            dkqf1_opt=-0.5*p(2)/p(1)
            figure(1)
            plot(dklqd0,x_qd0,'b+',dklqd0,y_qd0,'r+')
#            figure(2)
#            plot(dklqf1,x_qf1,'b+',dklqf1,y_qf1,'r+')
            placet_element_set_attribute("ATF2",qd0ff,"strength",klqd0+dkqd0_opt);
#            placet_element_set_attribute("ATF2",qf1ff,"strength",klqf1+dkqf1_opt);
            [E,B_ini]=placet_test_no_correction("ATF2", "beam0","None");
            B_usi_ini=[[B_ini(:,2) B_ini(:,5) B_ini(:,3) B_ini(:,6) B_ini(:,4)]*1e-6 (B_ini(:,1)-$e0)/$e0];
            disp("after waist adjustement :")
            disp(std(B_usi_ini))
            figure(3)
            plot(B_usi_ini(:,4),B_usi_ini(:,3),"+")
            qd0ff=placet_get_name_number_list("ATF2","qd0ff");
            klqd0=placet_element_get_attribute("ATF2",qd0ff,"strength");
            klqd0=klqd0(1);
            dklqd0=(-5:5)*.0001*klqd0;
            for i=1:length(dklqd0)
                klqd0_i=klqd0+dklqd0(i);
                placet_element_set_attribute("ATF2",qd0ff,"strength",klqd0_i);
                [E,B_ini]=placet_test_no_correction("ATF2", "beam0","None");
                B_usi_ini=[[B_ini(:,2) B_ini(:,5) B_ini(:,3) B_ini(:,6) B_ini(:,4)]*1e-6 (B_ini(:,1)-$e0)/$e0];
                x_qd0(i)=std(B_usi_ini(:,1));
                y_qd0(i)=std(B_usi_ini(:,3));
            end
#            qf1ff=placet_get_name_number_list("ATF2","qf1ff");
#            klqf1=placet_element_get_attribute("ATF2",qf1ff,"strength");
#            klqf1=klqf1(1);
#            dklqf1=(-5:5)*.001*klqf1;
#            for i=1:length(dklqf1)
#                klqf1_i=klqf1+dklqf1(i);
#                placet_element_set_attribute("ATF2",qf1ff,"strength",klqf1_i);
#                [E,B_ini]=placet_test_no_correction("ATF2", "beam0","None");
#                B_usi_ini=[[B_ini(:,2) B_ini(:,5) B_ini(:,3) B_ini(:,6) B_ini(:,4)]*1e-6 (B_ini(:,1)-$e0)/$e0];
#                x_qf1(i)=std(B_usi_ini(:,1));
#                y_qf1(i)=std(B_usi_ini(:,3));
#            end
            p=polyfit(dklqd0,y_qd0,2);
            dkqd0_opt=-0.5*p(2)/p(1)
#            p=polyfit(dklqf1,x_qf1,2);
#            dkqf1_opt=-0.5*p(2)/p(1)
            figure(1)
            plot(dklqd0,x_qd0,'b+',dklqd0,y_qd0,'r+')
#            figure(2)
#            plot(dklqf1,x_qf1,'b+',dklqf1,y_qf1,'r+')
            placet_element_set_attribute("ATF2",qd0ff,"strength",klqd0+dkqd0_opt);
#            placet_element_set_attribute("ATF2",qf1ff,"strength",klqf1+dkqf1_opt);
            [E,B_ini]=placet_test_no_correction("ATF2", "beam0","None");
            B_usi_ini=[[B_ini(:,2) B_ini(:,5) B_ini(:,3) B_ini(:,6) B_ini(:,4)]*1e-6 (B_ini(:,1)-$e0)/$e0];
            disp("after 2nd waist adjustement :")
            disp(std(B_usi_ini))
            figure(3)
            plot(B_usi_ini(:,4),B_usi_ini(:,3),"+")
            qd0ff=placet_get_name_number_list("ATF2","qd0ff");
            klqd0=placet_element_get_attribute("ATF2",qd0ff,"strength");
            klqd0=klqd0(1);
            dklqd0=(-5:5)*.00001*klqd0;
            for i=1:length(dklqd0)
                klqd0_i=klqd0+dklqd0(i);
                placet_element_set_attribute("ATF2",qd0ff,"strength",klqd0_i);
                [E,B_ini]=placet_test_no_correction("ATF2", "beam0","None");
                B_usi_ini=[[B_ini(:,2) B_ini(:,5) B_ini(:,3) B_ini(:,6) B_ini(:,4)]*1e-6 (B_ini(:,1)-$e0)/$e0];
                x_qd0(i)=std(B_usi_ini(:,1));
                y_qd0(i)=std(B_usi_ini(:,3));
            end
#            qf1ff=placet_get_name_number_list("ATF2","qf1ff");
#            klqf1=placet_element_get_attribute("ATF2",qf1ff,"strength");
#            klqf1=klqf1(1);
#            dklqf1=(-5:5)*.001*klqf1;
#            for i=1:length(dklqf1)
#                klqf1_i=klqf1+dklqf1(i);
#                placet_element_set_attribute("ATF2",qf1ff,"strength",klqf1_i);
#                [E,B_ini]=placet_test_no_correction("ATF2", "beam0","None");
#                B_usi_ini=[[B_ini(:,2) B_ini(:,5) B_ini(:,3) B_ini(:,6) B_ini(:,4)]*1e-6 (B_ini(:,1)-$e0)/$e0];
#                x_qf1(i)=std(B_usi_ini(:,1));
#                y_qf1(i)=std(B_usi_ini(:,3));
#            end
            p=polyfit(dklqd0,y_qd0,2);
            dkqd0_opt=-0.5*p(2)/p(1)
#            p=polyfit(dklqf1,x_qf1,2);
#            dkqf1_opt=-0.5*p(2)/p(1)
            figure(1)
            plot(dklqd0,x_qd0,'b+',dklqd0,y_qd0,'r+')
#            figure(2)
#            plot(dklqf1,x_qf1,'b+',dklqf1,y_qf1,'r+')
            placet_element_set_attribute("ATF2",qd0ff,"strength",klqd0+dkqd0_opt);
#            placet_element_set_attribute("ATF2",qf1ff,"strength",klqf1+dkqf1_opt);
            [E,B_ini]=placet_test_no_correction("ATF2", "beam0","None");
            B_usi_ini=[[B_ini(:,2) B_ini(:,5) B_ini(:,3) B_ini(:,6) B_ini(:,4)]*1e-6 (B_ini(:,1)-$e0)/$e0];
            disp("after 3rd waist adjustement :")
            disp(std(B_usi_ini))
            figure(3)
            plot(B_usi_ini(:,4),B_usi_ini(:,3),"+")
            disp("klqf0 final :")
            fprintf("%.16g(*\$e0)\n",placet_element_get_attribute("ATF2",qd0ff,"strength")/$e0)
            disp("klqf1 final :")
            fprintf("%.16g(*\$e0)\n",placet_element_get_attribute("ATF2",qf1ff,"strength")/$e0)
}
