set sext 1
set scripts ../Common/scripts

source $scripts/init.tcl

set e0 1.3
set e_initial $e0

proc HCORRECTOR {a b c d } {
}

proc VCORRECTOR {a b c d } {
}

proc Marker {a b } {
}

set quad_synrad 1
set mult_synrad 1
set sbend_synrad 1

Girder
#SlicesToParticles
source ATF2/ATF2_v5.2-MadX-Coll_10x0.5.tcl
BeamlineSet

#emitance normalized in e-7 m
set match(emitt_x) 0.02
set match(emitt_y) .3

#beam long in e-6 m
set match(sigma_z) 8000

#energy spread normalized
set match(e_spread) 0.0008
set match(phase) 0.0
set charge 2e10

# ATF2 v4.0 beta in m
array set match {beta_x 6.54290 alpha_x 0.99638 beta_y  3.23785 alpha_y -1.9183 }

set n_slice 1
set n 40000
set n_total [expr $n * $n_slice]

source $scripts/ilc_beam.tcl
make_beam_many beam0 $n_slice $n
FirstOrder 1

set nm 100

set ql [QuadrupoleNumberList]
set ml [MultipoleNumberList]
set sl [NameNumberList "BS?" "B?" "BEC?A" "BEC?B"]
set el [concat $ql $ml $sl]
set el [lsort -integer $el]
set nel [expr [llength $el]/2]

#delete all misalignements

Zero

#tracking 

puts "Tracking ..."
Octave {
	nbelement=size(placet_get_number_list("atf2","*"))(2);
	sig_0=placet_get_sigma_matrix(placet_get_beam("beam0"));
	emit_0=placet_get_emittance(placet_get_beam("beam0"));
	[E,B] = placet_test_no_correction("atf2", "beam0","None",1);
	size.x = std(B(:,2));
	size.y = std(B(:,3));
	pos.x = mean(B(:,2));
	pos.y = mean(B(:,3));
	delta = mean(B(:,1).*B(:,5));
	matrix=[size.x*1e-6 size.y*1e-6 pos.x*1e-6 pos.y*1e-6];
	sig_ip=placet_get_sigma_matrix(B);
	twiss_ip=placet_get_twiss_matrix(B);
	emit_ip=placet_get_emittance(B);
	disp("E_spread (%)=");disp(std(B(:,1))/mean(B(:,1))*100);
	disp("sigma_0=");disp(sig_0);
	disp("sigma_ip=");disp(sig_ip);
	disp("emittance_ip=");disp(emit_ip);
	disp("disp_ip=");disp(delta);
	disp("emittance_0=");disp(emit_0);
	disp("twiss_ip=");disp(twiss_ip);
	disp("size.x(m) size.y(m) pos.x(m) pos.y(m)");
	disp(matrix);
}

TwissPlot -beam beam0 -file twiss_v5.2-MadX.dat



puts "done !"
