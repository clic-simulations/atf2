# options for lattice, should be loaded via external settings file

#bpm resolution [um]
#set stripline [expr 3]
#set stripline2 [expr 3]
#set cband [expr 0.03]
## nominal 0.002 for ipbpm, observed <0.01
#set ipbpm [expr 0.01]
#set font_stripline_resolution [expr 0.5]
#set skew_tilt [expr 3.1415926535897931/180*45]
#set mref3_dipole_strength 0


SetReferenceEnergy $e0

#after rematching of waist position
set klqd0 [ expr -0.6835219447145264*2 ]
set klqf1 [ expr  .37196034712500000000*2 ]


Drift -name IEX
Sbend -name kex1a -angle 0.0025 -e0 $e0 -E1 0 -length 0.25000104
Drift -name ip01
Girder
Sbend -name kex1b -angle 0.0025 -e0 $e0 -E2 0.005 -length 0.25000104
Drift -name l001 -length 0.92313904
Girder
Sbend -name qm6r -angle 0.00231097 -e0 $e0 -K [expr -0.35587*$e0] -length 0.09937459
Sbend -name qm6r -angle 0.00231097 -e0 $e0 -K [expr -0.35587*$e0] -length 0.09937459
Drift -name l002a -length 1.3981922
Drift -name coll
Drift -name l002b -length 0.065003009
Bpm -name mb1x -resolution $stripline
Drift -name l002c -length 0.08555046
Girder
Sbend -name qm7r -angle -0.0045874 -e0 $e0 -K [expr 0.20411*$e0] -length 0.039455639
Sbend -name qm7r -angle -0.0045874 -e0 $e0 -K [expr 0.20411*$e0] -length 0.039455639
Drift -name l003 -length 0.15054652
Girder
Sbend -name bs1xa -angle 0.0140178 -e0 $e0 -E1 0 -length 0.3
Girder
Dipole -name zs1x
Girder
Sbend -name bs1xb -angle 0.0140178 -e0 $e0 -E2 0 -length 0.3
Drift -name l004 -length 0.2
Girder
Sbend -name bs2xa -angle 0.0371717 -e0 $e0 -E1 0 -length 0.4
Girder
Dipole -name zs2x
Girder
Sbend -name bs2xb -angle 0.0371717 -e0 $e0 -E2 0 -length 0.4
Drift -name l005a -length 0.1
Bpm -name mb2x -resolution $stripline
Drift -name l005b -length 0.1
Girder
Sbend -name bs3xa -angle 0.117511 -e0 $e0 -E1 0 -length 0.5
Girder
Dipole -name zs3x
Girder
Sbend -name bs3xb -angle 0.117511 -e0 $e0 -E2 0 -length 0.5
Drift -name l006a -length 0.32
Drift -name otr1x
Drift -name l006b -length 0.186905
Girder
Dipole -name zv1x -length 0.1248
Drift -name l006c -length 0.008265
Girder
Quadrupole -name qs1x -strength [expr 0*$e0] -tilt $skew_tilt -length 0.039335
Drift -name sensorA_1
Drift -name sensorB_1
Quadrupole -name qs1x -strength [expr 0*$e0] -tilt $skew_tilt -length 0.039335
Drift -name l006d -length 0.08136
Girder
Quadrupole -name qf1x -strength [expr 0.536737*$e0] -length 0.099305
Drift -name sensorA_2
Drift -name sensorB_2
Quadrupole -name qf1x -strength [expr 0.536737*$e0] -length 0.099305
Drift -name l007a -length 0.020695
Bpm -name mqf1x -resolution $stripline2
Drift -name l007b -length 0.215
Drift -name ms1x
Drift -name l007c -length 0.264305
Girder
Sbend -name bh1xa -angle 0.0941997 -e0 $e0 -E1 0 -K2 [expr -0.367967*$e0*0] -length 0.4
Girder
Dipole -name zx1x
Girder
Sbend -name bh1xb -angle 0.0941997 -e0 $e0 -E2 0 -K2 [expr -0.367967*$e0*0] -length 0.4
Drift -name l008a -length 0.216905
Girder
Dipole -name zv2x -length 0.1248
Drift -name l008b -length 0.158295
Girder
Quadrupole -name qd2x -strength [expr -0.470609*$e0] -length 0.099305
Drift -name sensorA_3
Drift -name sensorB_3
Quadrupole -name qd2x -strength [expr -0.470609*$e0] -length 0.099305
Drift -name l009a -length 0.020695
Bpm -name mqd2x -resolution $stripline2
Drift -name l009b -length 0.75180508
Girder
Quadrupole -name qf3x -strength [expr 0.33456*$e0] -length 0.099305
Drift -name sensorA_4
Drift -name sensorB_4
Quadrupole -name qf3x -strength [expr 0.33456*$e0] -length 0.099305
Drift -name l010a -length 0.020695
Bpm -name mqf3x -resolution $stripline2
Drift -name l010b -length 0.1440395
Girder
Dipole -name zh1x -length 0.111921
Drift -name l010c -length 0.1416395
Girder
Dipole -name zv3x -length 0.1248
Drift -name l010d -length 1.5103344
Drift -name mid
Drift -name l010e -length 2.0534294
Girder
Quadrupole -name qf4x -strength [expr 0.340093*$e0] -length 0.099305
Drift -name sensorA_5
Drift -name sensorB_5
Quadrupole -name qf4x -strength [expr 0.340093*$e0] -length 0.099305
Drift -name l011a -length 0.020695
Bpm -name mqf4x -resolution $stripline2
Drift -name l011b -length 0.1440395
Girder
Dipole -name zh2x -length 0.111921
Drift -name l011c -length 0.47514958
Bpm -name mqd5x -resolution $stripline2
Drift -name l011d -length 0.020695
Girder
Quadrupole -name qd5x -strength [expr -0.463497*$e0] -length 0.099305
Drift -name sensorA_6
Drift -name sensorB_6
Quadrupole -name qd5x -strength [expr -0.463497*$e0] -length 0.099305
Drift -name l012a -length 0.158295
Girder
Dipole -name zv4x -length 0.1248
Drift -name l012b -length 0.216905
Girder
Sbend -name bh2xa -angle -0.0941997 -e0 $e0 -E1 0 -K2 [expr 0.367967*$e0*0] -length 0.4
Girder
Dipole -name zx2x
Girder
Sbend -name bh2xb -angle -0.0941997 -e0 $e0 -E2 0 -K2 [expr 0.367967*$e0*0] -length 0.4
Drift -name l013a -length 0.206905
Girder
Dipole -name zv5x -length 0.1248
Drift -name l013b -length 0.1476
Bpm -name mqf6x -resolution $stripline
Drift -name l013c -length 0.020695
Girder
Quadrupole -name qf6x -strength [expr 0.564176*$e0] -length 0.099305
Drift -name sensorB_7
Quadrupole -name qf6x -strength [expr 0.564176*$e0] -length 0.099305
Drift -name l014a -length 0.08136
Girder
Quadrupole -name qs2x -strength [expr 0*$e0] -tilt $skew_tilt -length 0.039335
Quadrupole -name qs2x -strength [expr 0*$e0] -tilt $skew_tilt -length 0.039335
Drift -name l014b -length 0.8461729
Girder
Sbend -name bh3xa -angle -0.171424 -e0 $e0 -E1 0 -length 0.675
Girder
Dipole -name zx3x
Girder
Sbend -name bh3xb -angle -0.171424 -e0 $e0 -E2 0 -length 0.675
Drift -name l015 -length 0.76503824
Girder
Quadrupole -name qf7x -strength [expr 0.19489*$e0] -length 0.039335
Drift -name sensorB_8
Quadrupole -name qf7x -strength [expr 0.19489*$e0] -length 0.039335
Drift -name l016a -length 0.020665
Bpm -name mqf7x -resolution $stripline
Drift -name l016b -length 1.7745607
Girder
Dipole -name zh3x -length 0.111921
Drift -name l016c -length 0.1647345
Girder
Quadrupole -name qd8x -strength [expr -0.294833*$e0] -length 0.099305
Drift -name sensorB_9
Quadrupole -name qd8x -strength [expr -0.294833*$e0] -length 0.099305
Drift -name l017a -length 0.020695
Bpm -name mqd8x -resolution $stripline
Drift -name l017b -length 0.1376
Girder
Dipole -name zv6x -length 0.1248
Drift -name l017c -length 0.68423124
Girder
Sbend -name kex2a -angle 0.0025 -e0 $e0 -E1 0.005 -length 0.25000104
Drift -name ip02
Girder
Sbend -name kex2b -angle 0.0025 -e0 $e0 -E2 0 -length 0.25000104
Drift -name mdisp
Drift -name l101a -length 0.4602755
Bpm -name mqf9x -resolution $stripline
Drift -name l101b -length 0.020695
Girder
Quadrupole -name qf9x -strength [expr 0.367681*$e0] -length 0.099305
Drift -name sensorB_10
Quadrupole -name qf9x -strength [expr 0.367681*$e0] -length 0.099305
Drift -name l102a -length 0.1647345
Girder
Dipole -name zh4x -length 0.111921
Drift -name l102b -length 1.602221
Drift -name fontk1
Drift -name l102c -length 0.1231
Girder
Dipole -name zv7x -length 0.1248
Drift -name l102d -length 0.0471
Bpm -name fontp1 -resolution $font_stripline_resolution
Drift -name l102e -length 0.1129
Girder
Quadrupole -name qk1x -strength [expr 0*$e0] -tilt $skew_tilt -length 0.039335
Quadrupole -name qk1x -strength [expr 0*$e0] -tilt $skew_tilt -length 0.039335
Drift -name l102f -length 0.100755
Girder
Quadrupole -name qd10x -strength [expr -0.511666*$e0] -length 0.099245
Drift -name mskew
Drift -name sensorB_11
Quadrupole -name qd10x -strength [expr -0.511666*$e0] -length 0.099245
Drift -name l103a -length 0.017755
Bpm -name mqd10x  -resolution $cband
Drift -name l103b -length 0.961079
Girder
Dipole -name zh5x -length 0.111921
Drift -name l103c -length 0.160755
Girder
Quadrupole -name qf11x -strength [expr 0.511666*$e0] -length 0.099245
Drift -name sensorA_7
Drift -name sensorB_12
Quadrupole -name qf11x -strength [expr 0.511666*$e0] -length 0.099245
Drift -name l104a -length 0.017755
Bpm -name mqf11x  -resolution $cband
Drift -name l104b -length 0.64643
Drift -name fontk2
Drift -name l104c -length 0.1231
Girder
Dipole -name zv8x -length 0.1248
Drift -name l104d -length 0.0471
Bpm -name fontp2  -resolution $font_stripline_resolution
Drift -name l104e -length 0.1129
Girder
#Quadrupole -name qk2x -strength [expr 0*$e0] -tilt $skew_tilt -length 0.039335
#Quadrupole -name qk2x -strength [expr 0*$e0] -tilt $skew_tilt -length 0.039335
Drift -name qk2x -length 0.039335
Drift -name qk2x -length 0.039335
Drift -name l104f -length 0.100755
Girder
Quadrupole -name qd12x -strength [expr -0.511666*$e0] -length 0.099245
Drift -name sensorA_8
Drift -name sensorB_13
Quadrupole -name qd12x -strength [expr -0.511666*$e0] -length 0.099245
Drift -name l105a -length 0.017755
Bpm -name mqd12x  -resolution $cband
Drift -name l105b -length 0.54043765
Girder
Dipole -name zh6x -length 0.111921
Drift -name l105c -length 0.164667
Girder
Quadrupole -name qf13x -strength [expr 0.684119*$e0] -length 0.0993725
Drift -name sensorA_9
Drift -name sensorB_14
Quadrupole -name qf13x -strength [expr 0.684119*$e0] -length 0.0993725
Drift -name l106a -length 0.0206275
Bpm -name mqf13x -resolution $stripline
Drift -name l106b -length 0.81616708
Girder
Quadrupole -name qd14x -strength [expr -0.507718*$e0] -length 0.0993725
Drift -name fit180
Drift -name sensorA_10
Drift -name sensorB_15
Quadrupole -name qd14x -strength [expr -0.507718*$e0] -length 0.0993725
Drift -name l107a -length 0.0206275
Bpm -name mqd14x -resolution $stripline
Drift -name l107b -length 0.2853275
Bpm -name fontp3 -resolution $font_stripline_resolution
Drift -name l107c -length 0.25425158
Girder
Dipole -name zh7x -length 0.111921
Drift -name l107d -length 0.164667
Girder
Quadrupole -name qf15x -strength [expr 0.684119*$e0] -length 0.0993725
Drift -name sensorA_11
Drift -name sensorB_16
Quadrupole -name qf15x -strength [expr 0.684119*$e0] -length 0.0993725
Drift -name l108a -length 0.0206275
Bpm -name mqf15x -resolution $stripline
Drift -name l108b -length 0.34992815
Girder
Dipole -name zv9x -length 0.1248
Drift -name l108c -length 0.16
Girder
#Quadrupole -name qk3x -strength [expr 0*$e0] -tilt $skew_tilt -length 0.039335
#Quadrupole -name qk3x -strength [expr 0*$e0] -tilt $skew_tilt -length 0.039335
Drift -name qk3x -length 0.039335
Drift -name qk3x -length 0.039335
Drift -name l108d -length 0.100755
Girder
Quadrupole -name qd16x -strength [expr -0.511666*$e0] -length 0.099245
Drift -name sensorA_12
Drift -name sensorB_17
Quadrupole -name qd16x -strength [expr -0.511666*$e0] -length 0.099245
Drift -name l109a -length 0.017755
Bpm -name mqd16x  -resolution $cband
Drift -name l109b -length 0.961079
Girder
Dipole -name zh8x -length 0.111921
Drift -name l109c -length 0.160755
Girder
Quadrupole -name qf17x -strength [expr 0.511666*$e0] -length 0.099245
Drift -name sensorB_18
Quadrupole -name qf17x -strength [expr 0.511666*$e0] -length 0.099245
Drift -name l110a -length 0.017755
 Bpm -name mqf17x  -resolution $cband
Drift -name l110b -length 0.243
Drift -name mref1x
Drift -name l110c -length 0.81133
Girder
Quadrupole -name qk4x -strength [expr 0*$e0] -tilt $skew_tilt -length 0.039335
Quadrupole -name qk4x -strength [expr 0*$e0] -tilt $skew_tilt -length 0.039335
Drift -name l110d -length 0.7477
Girder
Dipole -name zv10x -length 0.1248
Drift -name l110e -length 0.160755
Girder
Quadrupole -name qd18x -strength [expr -0.343036*$e0] -length 0.099245
Drift -name sensorA_13
Drift -name sensorB_19
Quadrupole -name qd18x -strength [expr -0.343036*$e0] -length 0.099245
Drift -name l111a -length 0.017755
Bpm -name mqd18x  -resolution $cband
Drift -name l111b -length 0.283
Drift -name mw0x
Drift -name l111c -length 0.6727289
Girder
Dipole -name zh9x -length 0.111921
Drift -name l111d -length 0.160755
Girder
Quadrupole -name qf19x -strength [expr 0.327583*$e0] -length 0.099245
Drift -name sensorB_20
Quadrupole -name qf19x -strength [expr 0.327583*$e0] -length 0.099245
Drift -name l112a -length 0.017755
Bpm -name mqf19x  -resolution $cband
Drift -name l112b -length 0.283
Drift -name mw1x
Drift -name l112c -length 0.56
Drift -name lw1x
Drift -name l112d -length 0.613
Girder
Dipole -name zv11x -length 0.1248
Drift -name l112e -length 1.901535
Drift -name mw2x
Drift -name l112f -length 0.283
Bpm -name mqd20x  -resolution $cband
Drift -name l112g -length 0.017665
Girder
Quadrupole -name qd20x -strength [expr -0.151135*$e0] -length 0.039335
Drift -name memit
Drift -name sensorA_14
Drift -name sensorB_21
Quadrupole -name qd20x -strength [expr -0.151135*$e0] -length 0.039335
Drift -name l113a -length 0.480665
Drift -name lw2x
Drift -name l113b -length 1.8527095
Girder
Dipole -name zh10x -length 0.111921
Drift -name l113c -length 0.9740395
Drift -name ict1x
Drift -name l113d -length 0.380665
Girder
Quadrupole -name qf21x -strength [expr 0.150713*$e0] -length 0.039335
Drift -name sensorB_22
Quadrupole -name qf21x -strength [expr 0.150713*$e0] -length 0.039335
Drift -name l114a -length 0.017665
Bpm -name mqf21x  -resolution $cband
Drift -name l114b -length 0.283
Drift -name mw3x
Drift -name l114c -length 0.165
Drift -name rcip
Drift -name l114d -length 0.295
Bpm -name ipbpm1  -resolution $cband
Drift -name l114e -length 0.24
Bpm -name ipbpm2  -resolution $cband
Drift -name l114f -length 1.049335
Drift -name rcn
Drift -name l114g -length 0.4
Drift -name nbpm1 
Drift -name l114h -length 0.3
Drift -name nbpm2  
Drift -name l114i -length 0.3
Drift -name nbpm3 
Drift -name l114j -length 0.45
Drift -name mw4x
Drift -name l114k -length 0.283
Drift -name begff
Bpm -name mqm16ff  -resolution $cband
Drift -name l200 -length 0.017755
Girder
Quadrupole -name qm16ff -strength [expr .33121360316427500000*$e0] -length 0.099245
Drift -name aqm16ff
Drift -name sensorB_23
Quadrupole -name qm16ff -strength [expr .33121360316427500000*$e0] -length 0.099245
Drift -name l201a -length 0.7647945
Girder
Dipole -name zh1ff -length 0.111921
Drift -name l201b -length 0.1416395
Girder
Dipole -name zv1ff -length 0.1248
Drift -name l201c -length 0.158355
Girder
Quadrupole -name qm15ff -strength [expr -.26493163301957500000*$e0] -length 0.099245
Drift -name aqm15ff
Drift -name sensorB_24
Quadrupole -name qm15ff -strength [expr -.26493163301957500000*$e0] -length 0.099245
Drift -name l202a -length 0.017755
Bpm -name mqm15ff  -resolution $cband
Drift -name l202b -length 1.283755
Girder
Quadrupole -name qm14ff -strength [expr -.74929095699224500000*$e0] -length 0.099245
Drift -name aqm14ff
Drift -name sensorB_25
Quadrupole -name qm14ff -strength [expr -.74929095699224500000*$e0] -length 0.099245
Drift -name l203a -length 0.017755
Bpm -name mqm14ff  -resolution $cband
Drift -name l203b -length 0.8385
Drift -name mfb2ff
Drift -name afb2ff
Drift -name l203c -length 0.445255
Girder
Quadrupole -name qm13ff -strength [expr .40301048229106000000*$e0] -length 0.099245
Drift -name aqm13ff
Drift -name sensorB_26
Quadrupole -name qm13ff -strength [expr .40301048229106000000*$e0] -length 0.099245
Drift -name l204a -length 0.017755
Bpm -name mqm13ff  -resolution $cband
Drift -name l204b -length 1.283755
Girder
Quadrupole -name qm12ff -strength [expr .17105548184497500000*$e0] -length 0.099245
Drift -name aqm12ff
Drift -name sensorB_27
Quadrupole -name qm12ff -strength [expr .17105548184497500000*$e0] -length 0.099245
Drift -name l205a -length 0.017755
Bpm -name mqm12ff  -resolution $cband
Drift -name l205b -length 0.6113
Drift -name mfb1ff
Drift -name l205c -length 0.772455
Girder
Quadrupole -name qm11ff -strength [expr .05364449209199300000*$e0] -length 0.099245
Drift -name aqm11ff
Quadrupole -name qm11ff -strength [expr .05364449209199300000*$e0] -length 0.099245
Drift -name l206a -length 0.017755
Bpm -name mqm11ff  -resolution $cband
Drift -name l206b -length 1.283755
Girder
Quadrupole -name qd10bff -strength [expr -.14738993508077000000*$e0] -length 0.099245
Drift -name aqd10bff
Quadrupole -name qd10bff -strength [expr -.14738993508077000000*$e0] -length 0.099245
Drift -name l207a -length 0.017755
Bpm -name mqd10bff  -resolution $cband
Drift -name l207b -length 0.243
Drift -name mref3ff
Drift -name l207c -length 0.440755
Girder
Quadrupole -name qd10aff -strength [expr -0.14501*$e0] -length 0.099245
Drift -name aqd10aff
Quadrupole -name qd10aff -strength [expr -0.14501*$e0] -length 0.099245
Drift -name l208a -length 0.017755
Bpm -name mqd10aff  -resolution $cband
Drift -name l208b -length 0.283
Drift -name ms1ff
Drift -name l208c -length 0.800755
Girder
Quadrupole -name qf9bff -strength [expr .18644139102340000000*$e0] -length 0.099245
Drift -name aqf9bff
Quadrupole -name qf9bff -strength [expr .18644139102340000000*$e0] -length 0.099245
Drift -name l209a -length 0.017755
Bpm -name mqf9bff  -resolution $cband
Drift -name l209b -length 0.39296098
Girder
Multipole -name sf6ff -type 3  -strength [expr 3.17925409471899192*$e0*$sext] -length 0.050039015
Drift -name asf6ff
Multipole -name sf6ff -type 3  -strength [expr 3.17925409471899192*$e0*$sext] -length 0.050039015
Drift -name l210a -length 0.066960985
Bpm -name msf6ff  -resolution $cband
Drift -name l210b -length 0.34375499
Girder
Quadrupole -name qf9aff -strength [expr .18644139102340000000*$e0] -length 0.099245
Drift -name aqf9aff
Quadrupole -name qf9aff -strength [expr .18644139102340000000*$e0] -length 0.099245
Drift -name l211a -length 0.017755
Bpm -name mqf9aff  -resolution $cband
Drift -name l211b -length 1.583755
Girder
Quadrupole -name qd8ff -strength [expr -.30706856192368000000*$e0] -length 0.099245
Drift -name aqd8ff
Quadrupole -name qd8ff -strength [expr -.30706856192368000000*$e0] -length 0.099245
Drift -name l212a -length 0.017755
Bpm -name mqd8ff  -resolution $cband
Drift -name l212b -length 1.683755
Girder
Quadrupole -name qf7ff -strength [expr .27329798780976000000*$e0] -length 0.099245
Drift -name aqf7ff
Drift -name sensorB_28
Quadrupole -name qf7ff -strength [expr .27329798780976000000*$e0] -length 0.099245
Drift -name l213a -length 0.017755
Bpm -name mqf7ff  -resolution $cband
Drift -name l213b -length 0.42660971
Girder
Sbend -name b5ffa -angle -0.024948 -e0 $e0 -E1 -0.024947967 -length 0.30643179
Girder
Sbend -name b5ffb -angle -0.024948 -e0 $e0 -E2 -0.024947967 -length 0.30643179
Drift -name l214 -length 0.64436471
Girder
Quadrupole -name qd6ff -strength [expr -.29669610949319000000*$e0] -length 0.099245
Drift -name aqd6ff
Quadrupole -name qd6ff -strength [expr -.29669610949319000000*$e0] -length 0.099245
Drift -name l215a -length 0.017755
Bpm -name mqd6ff  -resolution $cband
Drift -name l215b -length 1.583755
Girder
Quadrupole -name qf5bff -strength [expr .19359465631448500000*$e0] -length 0.099245
Drift -name aqf5bff
Quadrupole -name qf5bff -strength [expr .19359465631448500000*$e0] -length 0.099245
Drift -name l216a -length 0.017755
Bpm -name mqf5bff  -resolution $cband
Drift -name l216b -length 0.39296098
Girder
Multipole -name sf5ff -type 3  -strength [expr -.57122859204545352*$e0*$sext] -length 0.050039015
Drift -name asf5ff
Multipole -name sf5ff -type 3  -strength [expr -.57122859204545352*$e0*$sext] -length 0.050039015
Drift -name l217a -length 0.066960985
Bpm -name msf5ff  -resolution $cband
Drift -name l217b -length 0.34375499
Girder
Quadrupole -name qf5aff -strength [expr .19359465631448500000*$e0] -length 0.099245
Drift -name aqf5aff
Quadrupole -name qf5aff -strength [expr .19359465631448500000*$e0] -length 0.099245
Drift -name l218a -length 0.017755
Bpm -name mqf5aff  -resolution $cband
Drift -name l218b -length 0.863
Drift -name mref2ff
Drift -name l218c -length 0.220755
Girder
Quadrupole -name qd4bff -strength [expr -.15420148078181500000*$e0] -length 0.099245
Drift -name aqd4bff
Quadrupole -name qd4bff -strength [expr -.15420148078181500000*$e0] -length 0.099245
Drift -name l219a -length 0.017755
Bpm -name mqd4bff  -resolution $cband
Drift -name l219b -length 0.39296098
Girder
Multipole -name sd4ff -type 3  -strength [expr 7.2332075256354672*$e0*$sext] -length 0.050039015
Drift -name asd4ff
Multipole -name sd4ff -type 3  -strength [expr 7.2332075256354672*$e0*$sext] -length 0.050039015
Drift -name l220a -length 0.066960985
Bpm -name msd4ff  -resolution $cband
Drift -name l220b -length 0.34375499
Girder
Quadrupole -name qd4aff -strength [expr -.15420148078181500000*$e0] -length 0.099245
Drift -name aqd4aff
Quadrupole -name qd4aff -strength [expr -.15420148078181500000*$e0] -length 0.099245
Drift -name l221a -length 0.017755
Bpm -name mqd4aff  -resolution $cband
Drift -name l221b -length 1.5266299
Girder
Sbend -name b2ffa -angle -0.0150879 -e0 $e0 -E1 -0.015087937 -length 0.30641163
Girder
Sbend -name b2ffb -angle -0.0150879 -e0 $e0 -E2 -0.015087937 -length 0.30641163
Drift -name l222 -length 0.44438487
Girder
Quadrupole -name qf3ff -strength [expr .28544963274687000000*$e0] -length 0.099245
Drift -name aqf3ff
Drift -name sensorB_29
Quadrupole -name qf3ff -strength [expr .28544963274687000000*$e0] -length 0.099245
Drift -name l223a -length 0.017755
Bpm -name mqf3ff  -resolution $cband
Drift -name l223b -length 0.42660454
Girder
Sbend -name b1ffa -angle -0.0269021 -e0 $e0 -E1 -0.02690209 -length 0.30643696
Girder
Sbend -name b1ffb -angle -0.0269021 -e0 $e0 -E2 -0.02690209 -length 0.30643696
Drift -name l224a -length 0.22360454
Drift -name mref1ff
Drift -name l224b -length 0.220755
Girder
Quadrupole -name qd2bff -strength [expr -.12548617920488500000*$e0] -length 0.099245
Drift -name aqd2bff
Quadrupole -name qd2bff -strength [expr -.12548617920488500000*$e0] -length 0.099245
Drift -name l225a -length 0.017755
Bpm -name mqd2bff  -resolution $cband
Drift -name l225b -length 1.483755
Girder
Quadrupole -name qd2aff -strength [expr -.13238662202676000000*$e0] -length 0.099245
Drift -name aqd2aff
Quadrupole -name qd2aff -strength [expr -.13238662202676000000*$e0] -length 0.099245
Drift -name l226a -length 0.017755
Bpm -name mqd2aff  -resolution $cband
Drift -name l226b -length 4.3499
Drift -name ms2ff
Drift -name l226c -length 0.325
Bpm -name msf1ff  -resolution $cband
Drift -name l226d -length 0.0331
Girder
Multipole -name sf1ff -type 3  -strength [expr -1.371294364589438160*$e0*$sext] -length 0.05
Drift -name asf1ff
Multipole -name sf1ff -type 3  -strength [expr -1.371294364589438160*$e0*$sext] -length 0.05
Drift -name l227 -length 0.2875
Girder
Quadrupole -name qf1ff -strength [expr $klqf1/2*$e0] -length 0.2375
Drift -name aqf1ff
Quadrupole -name qf1ff -strength [expr $klqf1/2*$e0] -length 0.2375
Drift -name l228a -length 0.052544
Bpm -name mqf1ff  -resolution $cband
Drift -name l228b -length 0.416856
Bpm -name msd0ff  -resolution $cband
Drift -name l228c -length 0.0331
Girder
Multipole -name sd0ff -type 3  -strength [expr 2.18930694947074488*$e0*$sext] -length 0.05
Drift -name asd0ff
Multipole -name sd0ff -type 3  -strength [expr 2.18930694947074488*$e0*$sext] -length 0.05
Drift -name l229 -length 0.2875
Girder
Quadrupole -name qd0ff -strength [expr $klqd0/2*$e0] -length 0.2375
Drift -name aqd0ff
Drift -name sensorA_15
Drift -name sensorB_30
Quadrupole -name qd0ff -strength [expr $klqd0/2*$e0] -length 0.2375
Drift -name l230a -length 0.052544
Bpm -name mqd0ff  -resolution $cband
Girder
Drift -name l230b -length 0.3275
Drift -name sweep
Drift -name l230c -length 0.1825
Bpm -name mbs2ip -resolution $ipbpm
Drift -name l230d -length 0.275
Drift -name m1_2ip
Drift -name l230e -length 0.149956
Bpm -name mbs1ip -resolution $ipbpm
Drift -name ip
Drift -name endff
