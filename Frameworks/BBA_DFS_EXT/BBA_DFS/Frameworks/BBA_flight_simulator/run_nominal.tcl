# Input: charge file, energy file, correctors strength
# Output: Tracking results and BPMs readings
 
# Possible beam dynamics set up: Misalignment and beam incoming jitter

####################
# Directory set up #
####################

set script_dir ../../
set lattice_dir $script_dir/Lattices/ATF2
set script_common_dir $script_dir/Common
source $script_common_dir/scripts/tcl_procs.tcl


#########################################################
# Charge change from file generated on Placet Interface #
#########################################################

set dcharge 1.0
set charge [expr 1e10 * $dcharge ]


#########################################################
# Charge energy from file generated on Placet Interface #
#########################################################

set energy 1.3
puts "USING ENERGY = $energy"
set e0 $energy 



#######################
# Tracking Output dir #
#######################

set beamline_name "ATF2_v5.2"
set tracking_output_string "%s %E %dE %ex %ey %sz %sx %sy" ;# position, energy, dispersion, emittx, emitty, bunch_length, beam size x, beam size y
set dir_name "nominaltracking"

###################
# Beam Parameters #
###################
# total number of simulated particles and slices
set n_slice 10
set n 3000

# copied mostly from Mad8 file
# emitance normalized in e-7 m
set match(emitt_x) 52
set match(emitt_y) 0.3
# energy spread normalized in %, negative means flat distribution, positive gaussian distribution, maxed at 3 sigma
set match(e_spread) 0.08
# beam long in e-6 m
set match(sigma_z) 8000
set match(phase) 0.0
set e_initial 1.3 ;# design beam energy [GeV]
set e0_start $e0 ;# [GeV]
set charge $charge

#######################
#Add beam line set up #
#######################
set beamline_name "ATF2_v5.2"
array set match {beta_x 6.848560987444 alpha_x 1.108024744392 beta_y 2.935758991906 alpha_y -1.907222941526}

# Use beam incoming jitter
set use_beam_jitter 0
set beam_jitter_x 10; #[um]
set beam_jitter_y 10; #[um]
set beam_jitter_xp 1; #[urad]
set beam_jitter_yp 1; #[urad]

# synchrotron on/off in certain elements
set quad_synrad 0
set mult_synrad 0
set sbend_synrad 0

# CBPM Wakefield
set use_wakefield 0
# use short bunch approximation - 
set use_wakefield_shortbunch 0
set wakefield_shortbunch 300 ;# [um]

# Options for bpms and quadrupoles misalignments
set seed 0
set sigma 0


#######################
# Add beam line       #
#######################
Girder
ReferencePoint -name "START" -angle 1 -sens 1
proc HCORRECTOR { a b c d } { Dipole $a $b $c $d }
proc VCORRECTOR { a b c d } { Dipole $a $b $c $d }
source $lattice_dir/ATF2_v5.2-MadX.tcl
ReferencePoint -sense -1
BeamlineSet -name $beamline_name

###########################
# Add beam line wakefields#
###########################

if {$use_wakefield} {
    source $script_dir/Frameworks/ATF2_studies/scripts/wakefield.tcl
}
#######################
# Setting up the beam #
#######################

set beam_name electron
set scripts $script_common_dir/scripts  ; # This is necessary for compatibility

# load RF and injection scripts:
source $script_common_dir/scripts/init.tcl;

# loading of the beam making scripts
source $script_common_dir/scripts/ilc_beam.tcl;

# Total number of particles
set n_total [expr $n * $n_slice]

# make initial perfect beam
make_beam_many_energy $beam_name $n_slice $n 1


##########################################################################
# Change of the strength of specified horizontal and vertical correctors #  
##########################################################################

####################################
# Bpms and quadrupole misalignment #
####################################

proc my_survey {} {
    global seed sigma
    Octave {
        randn("seed", $seed );
	BI = placet_get_number_list("ATF2_v5.2", "bpm");
	QI = placet_get_number_list("ATF2_v5.2", "quadrupole");	
	placet_element_set_attribute("ATF2_v5.2", BI, "x", randn(size(BI)) * $sigma);
	placet_element_set_attribute("ATF2_v5.2", BI, "y", randn(size(BI)) * $sigma);
	placet_element_set_attribute("ATF2_v5.2", QI, "x", randn(size(QI)) * $sigma);
	placet_element_set_attribute("ATF2_v5.2", QI, "y", randn(size(QI)) * $sigma);
        if $sigma > 0
           printf("BPMs and quadrupole misalignment is implemented");
        end 
   }
}

 
##############################
# Apply incoming beam jitter #  (Not working)
##############################

Octave {
%    if ($use_beam_jitter==1)
%    Tcl_Eval("BeamAddOffset -beam electron -x $beam_jitter_x -y $beam_jitter_y -angle_x $beam_jitter_xp -angle_y $beam_jitter_yp")
%    else 
%    end
%    printf("Incoming beam jitter is considered");
}

      	     
############
# Tracking #
############

DispersionFreeEmittance -on

Octave {
    disp("Performing particle tracking ...\n") 
    [E,B] = placet_test_no_correction("ATF2_v5.2", "electron","my_survey"); # $sigma is in um 
    printf("FINAL HORIZONTAL EMITTANCE = %g um*rad\n",   E(end,2) / 10);
    printf("FINAL VERTICAL   EMITTANCE = %g um*rad\n\n", E(end-1,6) / 10);
    Ex = E(end,2) / 10;
    Ey = E(end-1,6) / 10; # The IP index correspond to end-1 
    save -mat placet_BBA_emitt.mat Ex Ey E B ;
    save -text emitt_nominal.dat E;
}
