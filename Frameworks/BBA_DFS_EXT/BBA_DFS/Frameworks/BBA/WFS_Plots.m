figure(1)
clf
plot(Corrx);
hold on
plot(Corry, 'r-');
title('Corrx and Corry');
xlabel('Corrector number');
ylabel('Correctors strength');

figure(2);
clf
plot(DispE.X);
hold on
plot(zeros(size(DispE.X)), 'r-');
title('Wake X and Target X');
xlabel('BPM number');
ylabel('Wakefield');

figure(3);
clf
plot(DispE.Y);
hold on
plot(zeros(size(DispE.Y)), 'r-');
xlabel('BPM number');
ylabel('Wakefield');
title('Wake Y and Target Y');

figure(4)
plotyy(1:iteration, norm_plot(:,1), 1:iteration, norm_plot(:,2));
xlabel('Iteration');
ylabel('Wakefield norm X and Y');

figure(5)
plotyy(1:iteration, normb_plot(:,1), 1:iteration, normb_plot(:,2));
xlabel('Iteration');
ylabel('Orbit norm X and Y');