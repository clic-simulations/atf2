function BBA()
  global BBA_DATA BBA_PLOTS
  feature('UseOldFileDialogs',0);
 
  Add_path % Added paths to needed directories
  
  loaded_old_state = 0;
  ans = questdlg('Do you want to start a new session of BBA?', 'BBA', 'Yes', 'No, I want to continue with an old one', 'Cancel', 'Yes');
  switch ans
  case 'Yes'
    disp('Starting a new BBA Session...');
    % ask what Interface should be used
    ans2 = questdlg('Select the Interface you want to use', 'BBA', 'ATF2 On-line', 'PLACET Simulation', 'PLACET Simulation');
    if strcmp(ans2, 'ATF2 On-line')
      disp('Going on-line with ATF2!');
      Interface = InterfaceATF2();
    else
      disp('Using Flight Simulator'); 
      dir=[ pwd '/../BBA_flight_simulator' ]; % Added paths in the Add_path function
      Interface = InterfacePlacet(dir);
    end
    basename = 'new_BBA';
    dirname = sprintf('Data/%s_%s', basename, strrep(datestr(clock, 30), 'T', '_'));
    system(sprintf('mkdir -p %s', dirname));
    chdir('Data/');
    %
    disp(['Created a new data set in ' dirname ]);
    dirname = uigetdir('.', 'Select a data set...');
    chdir(dirname);
    disp('Saving the current status of the machine...');
    S = GetMachine(Interface);
    machine_state_file = sprintf('machine_status_%s.mat',strrep(datestr(clock, 30), 'T', '_'));
    Save(S, machine_state_file);

    % set the defaults
    BBA_DATA.Interface = Interface;
    BBA_DATA.Directory = dirname;
    BBA_DATA.State = S;
    BBA_DATA.StateFile = machine_state_file;
    BBA_DATA.nsamples = 100;
    BBA_DATA.Correctors = struct('name', [], 'mask', []);
    BBA_DATA.Correctors(1).name = '<empty>';
    BBA_DATA.Correctors(1).mask = 0; % Oxy Dxy Wxy Fxy (bitmask: 1<<7 Ox 1<<6 Oy ; 1<<5 Dx 1<<4 Dy ; 1<<3 Wx 1<<2 Wy ; 1<<1 Fx 1<<0 Fy )
    BBA_DATA.Bpms = struct('name', [], 'mask', []);
    BBA_DATA.Bpms(1).name = '<empty>';
    BBA_DATA.Bpms(1).mask = 0; % Oxy Dxy Wxy Fxy (bitmask: 1<<7 Ox 1<<6 Oy ; 1<<5 Dx 1<<4 Dy ; 1<<3 Wx 1<<2 Wy ; 1<<1 Fx 1<<0 Fy )
    BBA_DATA.Response0_file = 'Response0.mat'; % should be in the same directory
    BBA_DATA.Response1_file = 'Response1.mat'; % should be in the same directory
    BBA_DATA.Response2_file = 'Response2.mat'; % should be in the same directory
    BBA_DATA.ChangeEnergy_cmd = 'Interface = ChangeEnergy(Interface, +2);';
    BBA_DATA.ResetEnergy_cmd  = 'Interface = ChangeEnergy(Interface, 0);';
    BBA_DATA.ChangeCharge_cmd = 'Interface = ChangeBunchCharge(Interface, +1);';
    BBA_DATA.ResetCharge_cmd  = 'Interface = ChangeBunchCharge(Interface, -1);';
    BBA_DATA.gain_x = 0.8;
    BBA_DATA.gain_y = 0.8;
    BBA_DATA.wgt_orb_x = 1;
    BBA_DATA.wgt_orb_y = 1;
    BBA_DATA.wgt_dfs_x = 40;
    BBA_DATA.wgt_dfs_y = 40;
    BBA_DATA.wgt_wfs_x = 40;
    BBA_DATA.wgt_wfs_y = 40;
    BBA_DATA.wgt_ff_x = 0;
    BBA_DATA.wgt_ff_y = 0;
    BBA_DATA.svd_nsingularvalues_x = 6;
    BBA_DATA.svd_nsingularvalues_y = 8;
    BBA_DATA.do_include_coupled_terms = false;
    BBA_DATA.do_apply_orbit_x = 1;
    BBA_DATA.do_apply_orbit_y = 1;
    BBA_DATA.do_apply_dfs_x = 1;
    BBA_DATA.do_apply_dfs_y = 1;
    BBA_DATA.do_apply_wfs_x = 1;
    BBA_DATA.do_apply_wfs_y = 1;
    BBA_DATA.do_apply_ff_x = 0;
    BBA_DATA.do_apply_ff_y = 0;
    BBA_DATA.do_plot_orbit = 0;
    BBA_DATA.do_plot_disp = 1;
    BBA_DATA.do_plot_wake = 1;
    BBA_DATA.do_plot_orbit_convergence = 1;
    BBA_DATA.do_plot_disp_convergence = 1;
    BBA_DATA.do_plot_wake_convergence = 1;
    BBA_DATA.do_plot_corrector_strengths = 1;

    BBA_PLOTS.nsteps = 0;
  
  case 'No, I want to continue with an old one'
    dirname = uigetdir('/userhome/atfopr/cernSteering/BBA/Data', 'Select a data set...');
    if dirname == 0
        disp('Ok...');
        return
    end
    disp(['Entering data set in ' dirname ]);
    chdir(dirname);
    if exist('bba.mat', 'file') ~= 2
      error([ 'File bba.mat not found in folder ' dirname '. Please select another folder or start a new project.']);
      return;
    end
    load('bba.mat', '-mat');
    BBA_DATA.Directory = dirname; %overload the saved directory to allow for renamed folders
    %if exist('corrector_strenghts.txt', 'file') ~= 2
    %  BBA_DATA.CorrectorStrengths = CorrectorStrengths(GetCorrectors(BBA_DATA.State),BBA_DATA.Directory);
    %end
    BBA_AcquireMachineState();
  case 'Cancel'
    disp('Ok...');
    return
  end

  BBA_GUI();

end
