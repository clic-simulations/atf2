
cur = -75;
bucket = 84;
offset_list = -0.6:0.2:0.6;
load Response_Electron_Real.mat

BPM_MASK = [ find(strcmp(state.bpms.name, Response_Real.BPMS{1})): find(strcmp(state.bpms.name, Response_Real.BPMS{end})) ];

NBpm = numel(BPM_MASK);


X = zeros(numel(offset_list), NBpm); 
Err = []; 
KArray = [];
TMIT = [];

for phase = -175:20:cur
  TMITV = 0;
  for ioff = 1:numel(offset_list)
    filename=sprintf('DATA_ELE_%g_%d_%g_%g.mat', bucket, phase, 0, offset_list(ioff));
    if ~exist(filename, 'file')
      error('The data on this phase is not ready yet!');
    end
    load(filename);
    X(ioff, :) = mean(state.bpms.Y(BPM_MASK, :)');
    TMITV = TMITV + mean(state.etoro.TMIT(3, :));
  end
  
  
  ErrV = zeros(NBpm, 1);
  KV = zeros(NBpm, 1);
  for ifit = 1:NBpm
     [K,R] = polyfit(offset_list.', X(:, ifit), 1);
     KV(ifit) = K(1);
     ErrV(ifit) = R.normr;
  end
  KArray = horzcat(KArray, KV);
  Err = horzcat(Err, ErrV);
  TMIT = horzcat(TMIT, TMITV);
end


 
