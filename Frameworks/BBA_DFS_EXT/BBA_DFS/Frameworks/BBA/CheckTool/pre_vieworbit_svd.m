function pre_vieworbit_svd(string)

if nargin ~= 1
  error('You should define note!!!');
end

BPM_MASK = [1:26, 36:62];
% BPM_MASK = [27 28 29 30 32];

% test_num = {'-1.2', '-0.8', '-0.4',  '0.4', '0.8', '1.2'};
test_num = {'-1.2', '-0.6',  '0.6', '1.2'};
% test_num = {'-0.6', '-0.4', '-0.2', '0.2', '0.4', '0.6'};
% test_num = {'-0.8', '-0.6', '-0.4', '-0.2', '0.2', '0.4', '0.6', '0.8'};

filename=sprintf('DATA_ELE_%s_%d.mat', string, 0);
load(filename);

Y=state.bpms.Y(BPM_MASK, :);
Nsamp = size(Y,2);

orbit = [];
for ii = 1:numel(test_num)
    filename=sprintf('DATA_ELE_%s_%s.mat', string, test_num{ii});
    load(filename);
    Y = horzcat(Y, state.bpms.Y(BPM_MASK, :));
end


[U,S,V] = svd(Y);
S0 = diag(S);
S0(1) = 0;
S0(3:end) = 0.0;
for i=1:numel(S0); S(i,i) = S0(i); end
Y = U*S*V';

Ref_orbit = mean(Y(:,1:Nsamp)');

for ii = 1:numel(test_num)
  orbit_ = mean(Y(:,ii*100 + (1:Nsamp))');
  orbit(ii,  :) = orbit_ - Ref_orbit;
end

for i =1:numel(test_num)
  test_num{i} = ['positron offset =', test_num{i}, 'mm'];
end

plot(state.bpms.Z(BPM_MASK), orbit');
legend(test_num)
xlabel('longitude positon in Linac (m)')
ylabel('vertical orbit offset (orbit-reference)\n from BPM reading (mm)');

