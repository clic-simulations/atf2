

bucket = 30.75;
offset_list = -1.2:0.6:1.2;
% phase_list = [15:5:40, 41:1:49, 50:5:155 165 175];
% phase_list = [-175 -168:1:-165 -155:10:175 ];
phase_list = [11];

load Response_Electron_Real.mat
BPM_MASK = [ find(strcmp(state.bpms.name, Response_Real.BPMS{1})): find(strcmp(state.bpms.name, Response_Real.BPMS{end})) ];


% Fake_Respond = [1:NBpm]';
% Wake = 30;
% Fake_Data = Wake*Fake_Respond*(0.95+0.1*randn([1, 100]));


WT = zeros(1, numel(phase_list));
ET = zeros(1, numel(phase_list));
KickPhase = zeros(numel(offset_list), numel(phase_list));
for iph = 1:numel(phase_list)
  filename=sprintf('DATA_ELE_%g_%d_%g_%g.mat', bucket, phase_list(iph), 0, 0);
  load(filename);
  Kick = zeros(numel(offset_list), size(state.bpms.Y, 2));
  ref_orbit = mean(state.bpms.Y(BPM_MASK, :)')';
  for ioff = 1:numel(offset_list)
    filename=sprintf('DATA_ELE_%g_%d_%g_%g.mat', bucket, phase_list(iph), 0, offset_list(ioff));
    load(filename);
    for isamp = 1:size(state.bpms.Y, 2)
      Kick(ioff, isamp) = Response_Real.Ry \ (state.bpms.Y(BPM_MASK, isamp) - ref_orbit);
      % Kick(ioff, isamp) = Fake_Respond \ ( (offset_list(ioff) + 0.6*randn(1)) .* Fake_Data(:, isamp) .* (1+0.4*randn([NBpm, 1])));
    end
  end
  Wake = zeros(1, size(state.bpms.Y, 2));
  % KickPhase(:, iph) = svd_mean(Kick', 2)';
  KickPhase(:, iph) = mean(Kick')';
  for isamp = 1:numel(Wake)
     Wake(isamp) = offset_list' \ Kick(:, isamp);
  end
  Wake = Wake/2.8/1.3/0.9;
  WT(iph) = mean(Wake);
  ET(iph) = std(Wake)/sqrt(numel(Wake)-1);
end
figure
% plot(phase_list, WT, 'r-', phase_list, ET, 'b--');
errorbar(phase_list, WT, ET);
% norm(ET)/2/sqrt(length(ET))

