function [T,Y] = test_svd(filename, bpm_mask)

if nargin == 1
    bpm_mask = [];
end

% filename = e.g. 'DATA_ELE_45.75_20_0_*mat'

% get the list of all data files in the current directory
data_files = dir(filename);
data_files = { data_files.name }';

nFiles = numel(data_files);

Y = [];
for j=1:nFiles
    load(data_files{j});
    Y = [ Y state.bpms.X ];
end

if numel(bpm_mask) ~= 0
  Y = Y(bpm_mask,:);
end


first_bpm=1;
first_sv=1;

bpms=first_bpm:size(Y,1); 
T = zeros(numel(bpms)); 
for i=bpms;
  [U,S,V] = svd(Y(first_bpm:i,:));
  for j=first_sv:min(size(S))
     T(i,j) = S(j,j);
  end
end

plot(T, '-')
