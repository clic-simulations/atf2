function [X,Y] = cat_data(filename, bpm_mask)

if nargin == 1
    bpm_mask = [];
end

% filename = e.g. 'DATA_ELE_45.75_20_0_*mat'

% get the list of all data files in the current directory
data_files = dir(filename);
data_files = { data_files.name }';

nFiles = numel(data_files);

X = [];
Y = [];
T = zeros(nFiles,1);
for j=1:nFiles
    load(data_files{j});
    X = [ X state.bpms.X ];
    Y = [ Y state.bpms.Y ];
    % T(j) = state.timestamp;
end

if numel(bpm_mask) ~= 0
  X = X(bpm_mask,:);
  Y = Y(bpm_mask,:);
end

X = X';
Y = Y';
