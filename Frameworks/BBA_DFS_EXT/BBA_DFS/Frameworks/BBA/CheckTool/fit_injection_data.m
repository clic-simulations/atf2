function [Dx,Dy,chi2x,chi2y] = fit_injection_data(X, Y, bpm_mask)
    
    %addpath('~/Projects/ATF2/BBA/Checktool')
    %addpath('~/Projects/ATF2/BBA/tools')
    %addpath('~/Projects/ATF2/BBA')

    load('/Users/alatina/SVN/clicsim/ATF2/Frameworks/BBA/ATF2_tools/r11_r12_r16_r33_r34.mat');
    
    if nargin > 2
        if numel(bpm_mask) ~= 0
            X = X(:,bpm_mask);
            Y = Y(:,bpm_mask);
            Rxx = Rxx(bpm_mask,:);
        end
    end

    R11 = Rxx(:,1);
    R12 = Rxx(:,2);
    R16 = Rxx(:,3) * 1e6;
    R33 = Rxx(:,4);
    R34 = Rxx(:,5);
    
    %X = X - repmat(mean(X), size(X,1), 1);
    %Y = Y - repmat(mean(Y), size(Y,1), 1);
    %%%%%%%polyfit((1:size(X,1))', X(:,1),1)(1)
    
    nPulses = size(X,1)
    nBpms = size(X,2)
    
    Yx = []; % zeros(nBpms * nPulses, 1);
    Yy = []; % zeros(nBpms * nPulses, 1);
    Mx = zeros(nBpms * nPulses, 3);
    My = zeros(nBpms * nPulses, 2);
    
    Mx = [ Mx repmat(eye(nBpms), nPulses, 1) ];
    My = [ My repmat(eye(nBpms), nPulses, 1) ];
    
    for p=1:nPulses
        Yx = [ Yx ; X(p,:)' ];
        Yy = [ Yy ; Y(p,:)' ];
        Mx(1:nBpms,1:3) = [ R11 R12 R16 ];
        My(1:nBpms,1:2) = [ R33 R34 ];
    end
    
    %%%%%%%%%
    Dx = Mx \ Yx;
    Dy = My \ Yy;
    
    Xfit = Mx * Dx;
    Yfit = My * Dy;
    
    chi2x = norm(Yx - Xfit, 'fro');
    chi2y = norm(Yy - Yfit, 'fro');
    
    B0x = Dx(4:end);
    B0y = Dy(3:end);
    
    % Fit individual pulse offsets with these BPMs
    
end