addpath([ '~/alatina/E-208/tools']);
addpath([ '~/alatina/E-208/']);

global Asset_Particle
Asset_Particle = 'electron';
load 'machine_status_20141127_163623.mat';
S = State(state);

load 'Response_Electron_Real.mat';
Kicks = [ -400, -200, 0, 200, 400];

Kicks = [
  Kicks zeros(size(Kicks))
  zeros(size(Kicks)) Kicks
];


for i = 1:size(offset,2)

  kickx = Kicks(1,i);
  kicky = Kicks(2,i);

  Corr = Response_Real.Kx * kickx + Response_Real.Ky * kicky;

  %%%%%%

  % get reference orbit
  S = GetMachine(Interface);
  Orbit = GetOrbit(S, Response_Real.AllBPMS);
  fprintf('We would like to have a reference orbit:\n');
  fprintf('X = %g(%g) -- %g(%g);\n', Orbit.X(1), Orbit.stdX(1), Orbit.X(2), Orbit.stdX(2));
  fprintf('Y = %g(%g) -- %g(%g);\n', Orbit.Y(1), Orbit.stdY(1), Orbit.Y(2), Orbit.stdY(2));

  % get bumping orbit
  VaryCorrs(Interface, Response_Real.CORS, Corr);
  S_kick = GetMachine(Interface);
  Orbit_kick = GetOrbit(S_kick, Response_Real.AllBPMS);
  DX = Orbit_kick.X - Orbit.X;
  DY = Orbit_kick.Y - Orbit.Y;
  fprintf('We would like to kick the electron orbit with dX= %g and dY= %g:\n', offsetx, offsety);
  fprintf('X = %g(%g) -- %g(%g), dX = %g -- %g;\n', Orbit.X(1), Orbit.stdX(1), Orbit.X(2), Orbit.stdX(2), DX(1), DX(2));
  fprintf('Y = %g(%g) -- %g(%g), dY = %g -- %g;\n', Orbit.Y(1), Orbit.stdY(1), Orbit.Y(2), Orbit.stdY(2), DY(1), DY(2));
  
  figure
  plot(DX(3:end), 'b--');
  plot(DY(3:end), 'r--');
  legend('dX', 'dY');
  fprintf('Reconstruct kick X= %g KeV and Y= %g KeV:\n', Response_Real.Rx\DX(3:end), Response_Real.Ry\DY(3:end));

  VaryCorrs(InterfaceResponse_Real.CORS, -Corr);
end




