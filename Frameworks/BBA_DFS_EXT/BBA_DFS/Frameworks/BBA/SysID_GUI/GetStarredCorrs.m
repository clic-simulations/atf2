function corrs = GetStarredCorrs(CORRS,dir)
for i=1:length(CORRS) 
        corrs{i} = [ CORRS{i} ' ' ];
    end
    files = getAllFiles(dir);
    %DATA_XCOR_LI02_830_p0001.mat
    file_indexes = find(fnmatch('*DATA_?COR_*.mat', files)); % match only corrector files
    for i = file_indexes'
        [d,name] = fileparts(files{i});
        [cor,sgn] = filename2corrector(name);
        x = find(strcmp(CORRS, cor));
        if isempty(x)
          continue;
        end
        corrs{x} = [corrs{x} sgn];
    end
