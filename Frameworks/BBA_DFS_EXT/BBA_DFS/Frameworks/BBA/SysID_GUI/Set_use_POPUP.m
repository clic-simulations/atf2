function varargout = Set_use_POPUP(varargin)
% SET_USE_POPUP M-file for Set_use_POPUP.fig
%      SET_USE_POPUP, by itself, creates a new SET_USE_POPUP or raises the existing
%      singleton*.
%
%      H = SET_USE_POPUP returns the handle to a new SET_USE_POPUP or the handle to
%      the existing singleton*.
%
%      SET_USE_POPUP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SET_USE_POPUP.M with the given input arguments.
%
%      SET_USE_POPUP('Property','Value',...) creates a new SET_USE_POPUP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Set_use_POPUP_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Set_use_POPUP_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Set_use_POPUP

% Last Modified by GUIDE v2.5 09-Mar-2014 03:17:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Set_use_POPUP_OpeningFcn, ...
                   'gui_OutputFcn',  @Set_use_POPUP_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Set_use_POPUP is made visible.
function Set_use_POPUP_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Set_use_POPUP (see VARARGIN)

% Choose default command line output for Set_use_POPUP
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Set_use_POPUP wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Set_use_POPUP_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in cb_orbit.
function cb_orbit_Callback(hObject, eventdata, handles)
% hObject    handle to cb_orbit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_orbit


% --- Executes on button press in cb_wakefields.
function cb_wakefields_Callback(hObject, eventdata, handles)
% hObject    handle to cb_wakefields (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_wakefields


% --- Executes on button press in cb_dispersion.
function cb_dispersion_Callback(hObject, eventdata, handles)
% hObject    handle to cb_dispersion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_dispersion


% --- Executes on button press in cb_ff.
function cb_ff_Callback(hObject, eventdata, handles)
% hObject    handle to cb_ff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_ff


