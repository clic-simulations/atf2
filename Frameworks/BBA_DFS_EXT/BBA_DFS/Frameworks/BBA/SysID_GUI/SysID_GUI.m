function varargout = SysID_GUI(varargin)
% SYSID_GUI M-file for SysID_GUI.fig
%      SYSID_GUI, by itself, creates a new SYSID_GUI or raises the existing
%      singleton*.
%
%      H = SYSID_GUI returns the handle to a new SYSID_GUI or the handle to
%      the existing singleton*.
%
%      SYSID_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SYSID_GUI.M with the given input arguments.
%
%      SYSID_GUI('Property','Value',...) creates a new SYSID_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SysID_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SysID_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SysID_GUI

% Last Modified by GUIDE v2.5 07-Dec-2017 12:42:17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SysID_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @SysID_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SysID_GUI is made visible.
function SysID_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SysID_GUI (see VARARGIN)

% Choose default command line output for SysID_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
    global SYSID;
    set(handles.text_Mode, 'String', GetDescription(SYSID.Interface));
    set(handles.list_Correctors, 'String', GetStarredCorrs(SYSID.Correctors,SYSID.Directory));
    set(handles.text_CurrCorr, 'String', SYSID.Correctors{1});
    set(handles.do_orbit_plots, 'Value', SYSID.do_orbit_plots);
    [path,basedir] = fileparts(SYSID.Directory);
    set(handles.edit_dir,'String', basedir);
    set(handles.options_nsamp, 'String', SYSID.nsamples);
    set(handles.options_CycleMode, 'Value', SYSID.CycleMode);
    set(handles.options_maxStrength, 'String', SYSID.MaxStrength);
    set(handles.options_ExcitationX, 'String', SYSID.Excitation.X);
    set(handles.options_ExcitationY, 'String', SYSID.Excitation.Y);

% UIWAIT makes SysID_GUI wait for user response (see UIRESUME)
%uiwait(handles.SysID_GUI);

% --- Outputs from this function are returned to the command line.
function varargout = SysID_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
%    global SYSID;
%    varargout{1} = SYSID;
%    delete(hObject);

% --- Executes on button press in pb_start.
function pb_start_Callback(hObject, eventdata, handles)
% hObject    handle to pb_start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global SYSID stop_run;
    DisableSYSID(handles);
    set(handles.pb_stop,'Enable', 'on');
    set(handles.do_orbit_plots,'Enable', 'on');
    stop_run = 0;
    pause(0.1)
%%%% TEST %%%%
%    while ~stop_run, disp('running'), pause(1), end

%%%% REAL %%%%
if SYSID.CycleMode == 1
  CORRS = SYSID.Correctors;
else
  selection = get(handles.list_Correctors,'Value');
  CORRS = { SYSID.Correctors{selection} };
end

% plot the orbit for all bpms that follow the first corrector
corrs = GetCorrectorsData(SYSID.State, SYSID.Correctors);
corrsZ = min([ corrs(:).Z ]);

bpmSZ = GetBpmsZ(SYSID.State);
BPMS = GetBpms(SYSID.State);
BPMS = BPMS(bpmSZ > corrsZ);
% start
last_CORR.name = '';
last_Orbit = [];
iter = 1;
if SYSID.do_orbit_plots
  hfig = figure(2);
end
while true
  for icorr = 1:length(CORRS)
    % look up this corrector's data
    CORR = GetCorrectorsData(SYSID.State, CORRS{icorr} );
    set(handles.text_CurrCorr, 'String', CORR.name);
    
    if fnmatch('ZH*', { CORR.name } )
      scaleFactor = SYSID.Excitation.X / 1;
    else
      scaleFactor = SYSID.Excitation.Y / 1;
    end
    % loop over signs
    for exct = [ 'p' 'm' ];
      filename = sprintf('DATA_%s_%c%04d.mat', strrep(CORR.name, ':', '_'), exct, iter);
      if ~exist(filename, 'file')
        if exct == 'p'
          sign = +1;
        else
          sign = -1;
        end
        % set the desired corrector amplitude
        % look up this corrector to find its strength to excite 1 mm orbit
        ampl = GetStrength(SYSID.CorrectorStrengths, CORR.name);
        CORR.AMPL = sign * ampl * scaleFactor;
        % prepare to issue the correction
        if strcmp(last_CORR.name, CORR.name) | strcmp(last_CORR.name, '')
          corr_names = { CORR.name };
          corr_values = CORR.BDES + CORR.AMPL;
        else
          corr_names = { last_CORR.name; CORR.name };
          corr_values = [ last_CORR.BDES; CORR.BDES + CORR.AMPL ];
        end

        %%%% set the correctors
        SYSID.Interface = SetCorrs(SYSID.Interface, corr_names, corr_values);
       
        %%%%% read the orbit
        State = GetBpmsCorrs(SYSID.Interface);
        Save(State, filename);
        %%%%%

        bpm_mask = [ 1:21, 26:42 ];
%         bpm_mask = [ 1:57 ];
%        bpm_mask = bpm_mask(1:24);
   
        Orbit = GetOrbit(State, BPMS(bpm_mask));
        if exct == 'p'
          Orbit1 = Orbit;
        elseif strcmp(last_CORR.name, CORR.name)
          Orbit0 = Orbit;
          % orbit difference
          Diff.X = (Orbit1.X - Orbit0.X)/2;
          Diff.Y = (Orbit1.Y - Orbit0.Y)/2;
          clear Excitation
          Excitation.X = max(abs(Diff.X));
          Excitation.Y = max(abs(Diff.Y));
          Excitation = max(Excitation.X, Excitation.Y);
          if SYSID.do_orbit_plots
            if ~ishandle(hfig)
              hfig = figure(2);
            end
            Diff.Z = Orbit0.Z;
            Diff.TMIT = (Orbit0.TMIT+Orbit1.TMIT)/2;
            change_current_figure(hfig);
            PlotOrbit(Orbit0, Orbit1, Diff, CORR.name);
            drawnow;
          end
          if Excitation > 0
            if fnmatch('ZH*', { CORR.name } )
              targetExcitation = SYSID.Excitation.X;
            else
              targetExcitation = SYSID.Excitation.Y;
            end
            ampl_new = ampl * targetExcitation / Excitation * 1e3;
            ampl_new = max(-SYSID.MaxStrength, min(ampl_new, SYSID.MaxStrength));
            ampl_new = 0.8 * ampl_new + 0.2 * ampl;
            SetStrength(SYSID.CorrectorStrengths, CORR.name, ampl_new);
          end
        end
        last_CORR = CORR;
        last_Orbit = Orbit;
        pause(0.1)
        if stop_run
          set(handles.list_Correctors,'String', GetStarredCorrs(SYSID.Correctors,SYSID.Directory));
          return
        end
      end
    end
  end
  set(handles.list_Correctors,'String', GetStarredCorrs(SYSID.Correctors,SYSID.Directory));
  iter = iter+1;
end


% --- Executes on button press in pb_stop.
function pb_stop_Callback(hObject, eventdata, handles)
% hObject    handle to pb_stop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global stop_run SYSID;
    DisableSYSID(handles);
    fprintf('Stopping acquisition. ');
    stop_run=1;
    for i=1:3
        pause(1);
        fprintf('. ');
    end

    fprintf('\nNow restoring the machine status. . . ');
    Restore(SYSID.Interface, SYSID.State, SYSID.Correctors);
    fprintf('OK!\n');
    fidx = fopen('xcorr.txt', 'w');
    fidy = fopen('ycorr.txt', 'w');
    for i=1:length(SYSID.Correctors)
      corr_name = SYSID.Correctors{i};
      if fnmatch('ZH*', { corr_name } )
        fprintf(fidx, '%s\n', corr_name);
      else
        fprintf(fidy, '%s\n', corr_name);
      end
    end
    fclose(fidx);
    fclose(fidy);
    EnableSYSID(handles);

% --- Executes on selection change in list_Correctors.
function list_Correctors_Callback(hObject, eventdata, handles)
if strcmp(get(gcf, 'SelectionType'), 'open')
    global SYSID;
    corr_id = get(handles.list_Correctors,'Value');
    corr_name = SYSID.Correctors{corr_id};
    corr_strength = GetStrength(SYSID.CorrectorStrengths,corr_name);
    new_strength = Edit_corr_POPUP(corr_strength,corr_name);
    if new_strength ~= corr_strength
        SetStrength(SYSID.CorrectorStrengths,corr_name,new_strength);
    end
end
% hObject    handle to list_Correctors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns list_Correctors contents as cell array
%        contents{get(hObject,'Value')} returns selected item from list_Correctors


% --- Executes during object creation, after setting all properties.
function list_Correctors_CreateFcn(hObject, eventdata, handles)
% hObject    handle to list_Correctors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in bt_removeCorrs.
function bt_removeCorrs_Callback(hObject, eventdata, handles)
  global SYSID
  selection = get(handles.list_Correctors,'Value');
  remaining = setxor(1:length(SYSID.Correctors), selection);
  if length(remaining) == 0
      SYSID.Correctors = { '<empty>' };
  else
      SYSID.Correctors = { SYSID.Correctors{remaining} };
  end
  set(handles.text_CurrCorr, 'String', SYSID.Correctors{1});
  set(handles.list_Correctors,'Value', []);
  set(handles.list_Correctors,'String', GetStarredCorrs(SYSID.Correctors,SYSID.Directory));
% hObject    handle to bt_removeCorrs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in bt_addCorrs.
function bt_addCorrs_Callback(hObject, eventdata, handles)
% hObject    handle to bt_addCorrs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
  global SYSID;
  add_corrs_arg = struct;
  add_corrs_arg.Correctors = GetCorrectors(SYSID.State);
  add_corrs_arg.Directory = SYSID.Directory;
  DisableSYSID(handles);
  corrs = Add_corrs_POPUP(add_corrs_arg);
  EnableSYSID(handles);
  if isempty(corrs)
      return;
  end
  if strcmp(SYSID.Correctors{1},'<empty>')    
      SYSID.Correctors = {};
  end
  corrs = unique( [SYSID.Correctors, corrs' ]);
  temp = zeros(length(corrs),1);
  for i=1:length(corrs)
    C = GetCorrectorsData(SYSID.State, corrs{i});
    temp(i) = C.Z;
  end
  [S,I] = sort(temp);
%  corrs = unique( [SYSID.Correctors, corrs' ]);
%  temp = cell(length(corrs),1);
%  for i=1:length(corrs)
%    temp{i} = corrs{i}(2:end);
%  end
%  [S,I] = sort(temp);
  SYSID.Correctors = {corrs{I}};
  set(handles.text_CurrCorr, 'String', SYSID.Correctors{1});
  set(handles.list_Correctors,'Value', []);
  set(handles.list_Correctors,'String', GetStarredCorrs(SYSID.Correctors,SYSID.Directory));


% --- Executes on button press in pb_load_corrs.
function pb_load_corrs_Callback(hObject, eventdata, handles)
  global SYSID;
  DisableSYSID(handles);
  [FileName,PathName] = uigetfile('*.txt','Select a file with a list of correctors');
  if FileName ~= 0
    fid = fopen([PathName FileName]);
    C = textscan(fid,'%s%n');
    fclose(fid);
    if isempty(C)
        disp('The cells seems empty...');
        EnableSYSID(handles);
        return;
    end
    if strcmp(SYSID.Correctors{1},'<empty>')
        SYSID.Correctors = {};
    end
    corrs = unique(vertcat(SYSID.Correctors', C{1}));
    temp = zeros(length(corrs),1);
    for i=1:length(corrs)
      C = GetCorrectorsData(SYSID.State, corrs{i});
      temp(i) = C.Z;
    end
    
    [S,I] = sort(temp);
%    corrs = unique(vertcat(SYSID.Correctors', C{1}));
%    temp = cell(length(corrs),1);
%    for i=1:length(corrs)
%      temp{i} = corrs{i}(2:end);
%    end
%    [S,I] = sort(temp);

    SYSID.Correctors = {corrs{I}};
    set(handles.list_Correctors, 'String', GetStarredCorrs(SYSID.Correctors, SYSID.Directory));
  end
  EnableSYSID(handles);
% hObject    handle to pb_load_corrs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

function list_Pattern_Callback(hObject, eventdata, handles)
  global SYSID
  pattern = get(hObject,'String');
  set(handles.list_Correctors,'Value', find(fnmatch(pattern, SYSID.Correctors)));
% hObject    handle to list_Pattern (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of list_Pattern as text
%        str2double(get(hObject,'String')) returns contents of list_Pattern as a double


% --- Executes during object creation, after setting all properties.
function list_Pattern_CreateFcn(hObject, eventdata, handles)
% hObject    handle to list_Pattern (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in options_CycleMode.
function options_CycleMode_Callback(hObject, eventdata, handles)
  global SYSID
  SYSID.CycleMode = get(hObject, 'Value');
% hObject    handle to options_CycleMode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns options_CycleMode contents as cell array
%        contents{get(hObject,'Value')} returns selected item from options_CycleMode


% --- Executes during object creation, after setting all properties.
function options_CycleMode_CreateFcn(hObject, eventdata, handles)
% hObject    handle to options_CycleMode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function options_nsamp_Callback(hObject, eventdata, handles)
  global SYSID
  SYSID.nsamples = str2num(get(hObject,'String'));
  SYSID.Interface = SetSamples(SYSID.Interface, SYSID.nsamples);
% hObject    handle to options_nsamp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of options_nsamp as text
%        str2double(get(hObject,'String')) returns contents of options_nsamp as a double


% --- Executes during object creation, after setting all properties.
function options_nsamp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to options_nsamp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function options_maxStrength_Callback(hObject, eventdata, handles)
  global SYSID
  SYSID.MaxStrength = str2num(get(hObject, 'String'));
% hObject    handle to options_maxStrength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of options_maxStrength as text
%        str2double(get(hObject,'String')) returns contents of options_maxStrength as a double


% --- Executes during object creation, after setting all properties.
function options_maxStrength_CreateFcn(hObject, eventdata, handles)
% hObject    handle to options_maxStrength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in do_orbit_plots.
function do_orbit_plots_Callback(hObject, eventdata, handles)
  global SYSID
  SYSID.do_orbit_plots = get(hObject,'Value');
% hObject    handle to do_orbit_plots (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of do_orbit_plots



function options_ExcitationX_Callback(hObject, eventdata, handles)
  global SYSID
  SYSID.Excitation.X = str2num(get(hObject,'String'));
% hObject    handle to options_ExcitationX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of options_ExcitationX as text
%        str2double(get(hObject,'String')) returns contents of options_ExcitationX as a double


% --- Executes during object creation, after setting all properties.
function options_ExcitationX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to options_ExcitationX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function options_ExcitationY_Callback(hObject, eventdata, handles)
  global SYSID
  SYSID.Excitation.Y = str2num(get(hObject,'String'));
% hObject    handle to options_ExcitationY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of options_ExcitationY as text
%        str2double(get(hObject,'String')) returns contents of options_ExcitationY as a double


% --- Executes during object creation, after setting all properties.
function options_ExcitationY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to options_ExcitationY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function bt_addCorrs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bt_addCorrs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function SysID_GUI_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SysID_GUI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over text_Mode.
function text_Mode_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to text_Mode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over list_Pattern.
function list_Pattern_ButtonDownFcn(hObject, eventdata, handles)

% hObject    handle to list_Pattern (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function edit_dir_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_dir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes when user attempts to close SysID_GUI.
function SysID_GUI_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to SysID_GUI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global SYSID;
    save([SYSID.Directory '/sysid.mat'],'SYSID','-mat');
    fidx = fopen('xcorr.txt', 'w');
    fidy = fopen('ycorr.txt', 'w');
    for i=1:length(SYSID.Correctors)
      corr_name = SYSID.Correctors{i};
      if fnmatch('ZH*', { corr_name } )
        fprintf(fidx, '%s\n', corr_name);
      else
        fprintf(fidy, '%s\n', corr_name);
      end
    end
    fclose(fidx);
    fclose(fidy);
    chdir('../..');
% Hint: delete(hObject) closes the figure
%if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
%    uiresume(hObject);
%else
    % The GUI is no longer waiting, just close it
    delete(hObject);
%end

function EnableSYSID(handles)
    set(handles.list_Pattern,'Enable', 'on');
    set(handles.list_Correctors,'Enable', 'on');
    set(handles.bt_addCorrs,'Enable', 'on');
    set(handles.bt_removeCorrs,'Enable', 'on');
    set(handles.options_nsamp,'Enable', 'on');
    set(handles.options_maxStrength,'Enable', 'on');
    set(handles.options_CycleMode,'Enable', 'on');
    set(handles.options_ExcitationX,'Enable', 'on');
    set(handles.options_ExcitationY,'Enable', 'on');
    set(handles.do_orbit_plots,'Enable', 'on');
    set(handles.pb_start,'Enable', 'on');
    set(handles.pb_stop,'Enable', 'on');
    set(handles.pb_load_corrs,'Enable', 'on');

function DisableSYSID(handles)
    set(handles.list_Pattern,'Enable', 'off');
    set(handles.list_Correctors,'Enable', 'off');
    set(handles.bt_addCorrs,'Enable', 'off');
    set(handles.bt_removeCorrs,'Enable', 'off');
    set(handles.options_nsamp,'Enable', 'off');
    set(handles.options_maxStrength,'Enable', 'off');
    set(handles.options_CycleMode,'Enable', 'off');
    set(handles.options_ExcitationX,'Enable', 'off');
    set(handles.options_ExcitationY,'Enable', 'off');
    set(handles.do_orbit_plots,'Enable', 'off');
    set(handles.pb_start,'Enable', 'off');
    set(handles.pb_stop,'Enable', 'off');
    set(handles.pb_load_corrs,'Enable', 'off');
