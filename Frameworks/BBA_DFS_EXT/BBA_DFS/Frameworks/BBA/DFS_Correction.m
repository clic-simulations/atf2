% Add_path % Added paths to needed directories

nSamples = 1;
if true
    Interface = InterfaceATF2(nSamples); %%%%%%%%
else
    dir=[ pwd '../../../../BBA_flight_simulator' ]; % Added paths in the Add_path function
    Interface = InterfacePlacet(dir);
end
    
load 'Response0.mat';
R0 = Response;

load 'Response1.mat';
R1 = Response;

%%%%%%%%%%%%% BPMs and correctors selection

CORSx = {    % region 1
    'ZH1X'
    'ZH2X'
    'ZH3X'
    'ZH4X'
    'ZH5X'
    'ZH6X'
    'ZH7X'
    'ZH8X'
    'ZH9X'
  %  'ZH10X'
    'ZH1FF'
    };

CORSy = {    % region 1
    'ZV1X'
    'ZV2X'
    'ZV3X'
    'ZV4X'
    'ZV5X'
    'ZV6X'
    'ZV7X'
    'ZV8X'
    'ZV9X'
    'ZV10X'
    'ZV11X'
    'ZV1FF'
    };

BPMS = {	% region 1
   'MQF1X'
   'MQD2X'
   'MQF3X'
   'MQF4X'
   'MQD5X'
   'MQF6X'
   'MQF7X'
   'MQD8X'
% follow the non-dispersive bpms
    'QF9X'
    'QD10X'
    'QF11X'
    'QD12X'
    'QF13X'
    'QD14X'
    'QF15X'
    'QD16X'
    'QF17X'
    'QD18X'
    'QF19X'
    'QD20X'
    'QF21X'
    'QM16FF'
    'QM15FF'
    'QM14FF'
    'QM13FF'
    'QM12FF'
    'QD10AFF'
    'QF9AFF'
    'QD8FF'    
    'QF7FF'
    'QF5BFF'
    'QD4BFF'
    'QF3FF'
    'QD2BFF'    
    'QD2AFF'
    };

%%%%%%%%%%%%% Excluded BPMs and correctors

CORS_excluded = {
%        'ZH3X'  
%        'ZH4X'
'ZH10X'
           };

BPMS_excluded = {
  
    'QF9AFF'
       };

%%%%%%%%%%%%%% Masks stuff

MASK_BPMS  = []; for i=1:length(BPMS);  MASK_BPMS  = [ MASK_BPMS  ; find(strcmp(R0.BPMS, BPMS{i})) ]; end
MASK_CORSx = []; for i=1:length(CORSx); MASK_CORSx = [ MASK_CORSx ; find(strcmp(R0.CORS, CORSx{i})) ]; end
MASK_CORSy = []; for i=1:length(CORSy); MASK_CORSy = [ MASK_CORSy ; find(strcmp(R0.CORS, CORSy{i})) ]; end

MASK_CORSx = MASK_CORSx(find(fnmatch('ZH*', R0.CORS(MASK_CORSx))));
MASK_CORSy = MASK_CORSy(find(fnmatch('ZV*', R0.CORS(MASK_CORSy))));

for i=1:numel(BPMS_excluded);
    index = find(strcmp(R0.BPMS, BPMS_excluded{i}));
    MASK_BPMS = setdiff(MASK_BPMS, index);
end

for i=1:numel(CORS_excluded);
    index = find(strcmp(R0.CORS, CORS_excluded{i}));
    MASK_CORSx = setdiff(MASK_CORSx, index);
    MASK_CORSy = setdiff(MASK_CORSy, index);
end

%%%%%%%%%%%%% Geeting their names

BPMS  = R0.BPMS(MASK_BPMS);
CORSx = R0.CORS(MASK_CORSx)';
CORSy = R0.CORS(MASK_CORSy)';

%%%%%%%%%%%%% Orbit matrices

R0x = R0.Rx([ MASK_BPMS ], [ MASK_CORSx ]);
R0y = R0.Ry([ MASK_BPMS ], [ MASK_CORSy ]);

R1x = R1.Rx([ MASK_BPMS ], [ MASK_CORSx ]);
R1y = R1.Ry([ MASK_BPMS ], [ MASK_CORSy ]);

%%%%%%%%%%%%% Dispersion matrices

DEx = R1x - R0x;
DEy = R1y - R0y;

%%%%%%%%%%%%% Reference dispersion

state_disp = atf2_get_target_dispersion(-2);
ST = State(state_disp);

%%%%%%%%%%%%% Acquire orbits

if true
    SE = GetMachine(Interface);
    OrbitE  = GetOrbit(SE, BPMS); % nominal
end

if true
%     disp('Changing the klystron phase...');
    ChangeEnergy(Interface, -2);
    SD = GetMachine(Interface);
%     disp('Resetting the klystron phase...');
    ChangeEnergy(Interface, 0);
    OrbitD  = GetOrbit(SD, BPMS); % dispersive
%     DispE.X = OrbitD.X - OrbitE.X;
%    DispE.X = (OrbitD.X - OrbitE.X)*18;   
    DispE.X = (OrbitD.X - OrbitE.X);          
    DispE.Y = (OrbitD.Y - OrbitE.Y);
%     DispE.X = OrbitE.X - OrbitD.X;
%     DispE.Y = OrbitE.Y - OrbitD.Y;
end

%%%%%%%%%%%%% Set referecnce orbits

SR = SE;
SRD = SD;

TargetD  = GetOrbit(ST, BPMS); % target dispersion
OrbitR   = GetOrbit(SR, BPMS); % target nominal orbit

%[NAME,SPOS,X_POS,X_SIGMA,Y_POS,Y_SIGMA] = textread('/home/atf2-fs/Desktop/Pierre/atf2bpm18nov09_125709.asc', '%s %f %f %f %f %f');

%OrbitR.X = X_POS; % use orbit as anchor
%OrbitR.Y = Y_POS;

%OrbitR.X = 0 * OrbitR.X; % use zero orbit as anchor
%OrbitR.Y = 0 * OrbitR.Y;

%%%%%%%%%%%%% Start iterations

norm_plot = [];
normb_plot = [];

figure(4)
clf

figure(5)
clf

%%%%%%%%%%%%% BBA
    
gain = 0.2;
wgt_orb = 1;
wgt_dfsx = 2;
wgt_dfsy = 10;
n_svx = 8;
n_svy = 8;
        
save(sprintf('params_%s.mat',strrep(datestr(clock, 30), 'T', '_')), ...
    'nSamples', ...
    'gain', ...
    'wgt_orb', ...
    'wgt_dfsx', ...
    'wgt_dfsy', ...
    'n_svx', ...
    'n_svy');

Rx = [
    wgt_orb * R0x
    wgt_dfsx * DEx
];
    
Ry = [
    wgt_orb * R0y
    wgt_dfsy * DEy
];

for iteration=1:15

    bx = [
        wgt_orb * (OrbitE.X - OrbitR.X)
        wgt_dfsx * (DispE.X - TargetD.X)
    ];
    
    by = [
        wgt_orb * (OrbitE.Y - OrbitR.Y)
        wgt_dfsy * DispE.Y
    ];
    
    [U,S,V] = svd(Rx);
    inv_S = zeros(size(S'));
    n_svx = min(n_svx, min(size(S)));
    for i=1:n_svx
        if S(i,i)~=0
            inv_S(i,i) = 1/S(i,i);
        end
    end
    inv_Rx = V * inv_S * U';
    
    [U,S,V] = svd(Ry);
    inv_S = zeros(size(S'));
    n_svy = min(n_svy, min(size(S)));
    for i=1:n_svy
        if S(i,i)~=0
            inv_S(i,i) = 1/S(i,i);
        end        
    end
    inv_Ry = V * inv_S * U';
    
    %%%%%%%%%%%%%%%%%%
    
    normb = [ norm(bx) norm(by) ]
    normD = [ norm(DispE.X - TargetD.X) norm(DispE.Y) ]
    
    norm_plot  = [ norm_plot  ; normD ];
    normb_plot = [ normb_plot ; normb ];
    
    Corrx = -gain * inv_Rx * bx
    Corry = -gain * inv_Ry * by
  
    fprintf('maxCorrx_strength = %g\n', max(abs(Corrx)));
    fprintf('maxCorry_strength = %g\n', max(abs(Corry)));

    DFS_Plots

    VaryCorrs(Interface, vertcat(CORSx, CORSy), [ Corrx ; Corry ]);
   
    if true
        pause(5);
        SE = GetMachine(Interface);
        OrbitE  = GetOrbit(SE, BPMS); % nominal
    end
    
    if true
%         disp('Changing the klystron phase...');
        ChangeEnergy(Interface, -2);
        SD = GetMachine(Interface);
%         disp('Resetting the klystron phase...');
        ChangeEnergy(Interface, 0);
        OrbitD  = GetOrbit(SD, BPMS); % dispersive
%         DispE.X = OrbitD.X - OrbitE.X;
%        DispE.X = (OrbitD.X - OrbitE.X)*18;  
        DispE.X = (OrbitD.X - OrbitE.X);          
        DispE.Y = (OrbitD.Y - OrbitE.Y);
%         DispE.X = OrbitE.X - OrbitD.X;
%         DispE.Y = OrbitE.Y - OrbitD.Y;
    end  

    state_dispersive = GetStruct(SD);
    state_reference = GetStruct(SR);
    state_nominal = GetStruct(SE);

    orbits_state_file = sprintf('orbits_%s.mat',strrep(datestr(clock, 30), 'T', '_'));
    save(orbits_state_file, 'state_nominal', 'state_dispersive', 'state_reference');

end

DFS_Plots
