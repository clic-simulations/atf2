function ok = BBA_MatrixSanityCheck()
  global BBA_DATA

  load(BBA_DATA.Response0_file, '-mat');
  R0 = Response;

  load(BBA_DATA.Response1_file, '-mat');
  R1 = Response;

  load(BBA_DATA.Response2_file, '-mat');
  R2 = Response;

  ok = false;
  if length(R0.CORS) == length(R1.CORS) && sum(strcmp(R0.CORS, R1.CORS)) == length(R0.CORS)
    if length(R0.CORS) == length(R2.CORS) && sum(strcmp(R0.CORS, R2.CORS)) == length(R0.CORS)
      if length(R0.BPMS) == length(R1.BPMS) && sum(strcmp(R0.BPMS, R1.BPMS)) == length(R0.BPMS)
        if length(R0.BPMS) == length(R2.BPMS) && sum(strcmp(R0.BPMS, R2.BPMS)) == length(R0.BPMS)
          ok = true;
        else; error('R0 and R2 must contain the same bpms'); end
      else; error('R0 and R1 must contain the same bpms'); end
    else; error('R0 and R2 must contain the same correctors'); end
  else; error('R0 and R1 must contain the same correctors'); end
   
