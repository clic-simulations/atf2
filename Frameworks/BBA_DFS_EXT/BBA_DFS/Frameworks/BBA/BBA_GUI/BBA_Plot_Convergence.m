function BBA_Plot_Convergence()
global BBA_PLOTS BBA_DATA

 BBA.wgt_orb_x = BBA_DATA.wgt_orb_x * BBA_DATA.do_apply_orbit_x;
  BBA.wgt_orb_y = BBA_DATA.wgt_orb_y * BBA_DATA.do_apply_orbit_y;
  %
  BBA.wgt_dfs_x = BBA_DATA.wgt_dfs_x * BBA_DATA.do_apply_dfs_x;
  BBA.wgt_dfs_y = BBA_DATA.wgt_dfs_y * BBA_DATA.do_apply_dfs_y;
  %
  BBA.wgt_wfs_x = BBA_DATA.wgt_wfs_x * BBA_DATA.do_apply_wfs_x;
  BBA.wgt_wfs_y = BBA_DATA.wgt_wfs_y * BBA_DATA.do_apply_wfs_y;
  %
  BBA.wgt_ff_x  = BBA_DATA.wgt_ff_x * BBA_DATA.do_apply_ff_x;
  BBA.wgt_ff_y  = BBA_DATA.wgt_ff_y * BBA_DATA.do_apply_ff_y;

figure(3);

if BBA_DATA.do_plot_orbit
for ix=1: BBA_PLOTS.nsteps,
% should be BPM Z-pos, currently not available
X =  BBA_PLOTS.data( ix ).Orbit.X;
Y =  BBA_PLOTS.data( ix ).Orbit.Y;
normX(ix) =  norm(X) / sqrt(length(X));
normY(ix) =  norm(Y) / sqrt(length(Y));
end% for
end

if BBA_DATA.do_plot_disp & (BBA.wgt_dfs_x > 0 | BBA.wgt_dfs_y > 0)
for ix=1: BBA_PLOTS.nsteps,
% should be BPM Z-pos, currently not available
DX =  BBA_PLOTS.data( ix ).Disp.X;
DY =  BBA_PLOTS.data( ix ).Disp.Y;
normDX(ix) =  norm(DX) / sqrt(length(DX));
normDY(ix) =  norm(DY) / sqrt(length(DY));
end% for
end

if BBA_DATA.do_plot_wake & (BBA.wgt_wfs_x > 0 | BBA.wgt_wfs_y > 0)
for ix=1: BBA_PLOTS.nsteps,
% should be BPM Z-pos, currently not available
WX =  BBA_PLOTS.data( ix ).Wake.X;
WY =  BBA_PLOTS.data( ix ).Wake.Y;
normWX(ix) =  norm(WX) / sqrt(length(WX));
normWY(ix) =  norm(WY) / sqrt(length(WY));
end% for
end

if BBA_DATA.do_plot_orbit
subplot(3,1,1);

abscissa = 1:length(normX);
hh = plot(abscissa, normX, '-+r');
set(hh, 'LineWidth', 4);
hold on;
hh = plot(abscissa, normY, '-+k');
set(hh, 'LineWidth', 4);
xlabel('iteration');
ylabel('Orbit norm [mm]');
legend('X','Y', 'Location', 'NorthWest');
myaxis = axis;
axis([1 myaxis(2) myaxis(3) myaxis(4)]);
xlim('auto');
ylim('auto');
end

if BBA_DATA.do_plot_disp & (BBA.wgt_dfs_x > 0 | BBA.wgt_dfs_y > 0)
subplot(3,1,2);
abscissa = 1:length(normDX);
hh = plot(abscissa, normDX, '-+r');
set(hh, 'LineWidth', 4);
hold on;
hh = plot(abscissa, normDY, '-+k');
set(hh, 'LineWidth', 4);
xlabel('iteration');
ylabel('Disp norm [mm]');
legend('DX','DY', 'Location', 'NorthWest');
myaxis = axis;
axis([1 myaxis(2) myaxis(3) myaxis(4)]);
xlim('auto');
ylim('auto');
end

if BBA_DATA.do_plot_wake & (BBA.wgt_wfs_x > 0 | BBA.wgt_wfs_y > 0)
subplot(3,1,3);
abscissa = 1:length(normWX);
hh = plot(abscissa, normWX, '-+r');
set(hh, 'LineWidth', 4);
hold on;
hh = plot(abscissa, normWY, '-+k');
set(hh, 'LineWidth', 4);
xlabel('iteration');
ylabel('Wake norm [mm]');
legend('WX','WY', 'Location', 'NorthWest');
myaxis = axis;
axis([1 myaxis(2) myaxis(3) myaxis(4)]);
xlim('auto');
ylim('auto');
end
