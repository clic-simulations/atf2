function [Disp, State1] = BBA_MeasureDispersion(State0)
  global BBA_DATA

  Interface = BBA_DATA.Interface;

  load(BBA_DATA.Response1_file, '-mat')
  R1 = Response;

  CORS_Dfs = FigureOutCors(R1, 32, 16);
  BPMS_Dfs = FigureOutBpms(R1, 32, 16);

  Orbit0X = GetOrbit(State0, { BPMS_Dfs.X.name } );
  Orbit0Y = GetOrbit(State0, { BPMS_Dfs.Y.name } );

  %%%% read the disp to be corrected
  
  disp('Changing the klystron phase...');
  eval(BBA_DATA.ChangeEnergy_cmd);  
  BBA_DATA.Interface = Interface;
  disp('Reading dispersive orbit...');
  State1 = GetBpms(BBA_DATA.Interface);
  disp('Resetting the klystron phase...');
  eval(BBA_DATA.ResetEnergy_cmd);  
  BBA_DATA.Interface = Interface;
  Orbit1X = GetOrbit(State1, { BPMS_Dfs.X.name } );
  Orbit1Y = GetOrbit(State1, { BPMS_Dfs.Y.name } );
  Disp.X = Orbit1X.X - Orbit0X.X;
  Disp.Y = Orbit1Y.Y - Orbit0Y.Y;
%%%%%
 warning('using target dispersion for delta_frequency = +2 kHz');
 state_disp = atf2_get_target_dispersion(+2);
 StateDisp = State(state_disp);
 TargetDispX = GetOrbit(StateDisp, { BPMS_Dfs.X.name } );
 TargetDispY = GetOrbit(StateDisp, { BPMS_Dfs.Y.name } );
%%%%%
  Disp.X = Disp.X - TargetDispX.X;
  Disp.Y = Disp.Y - TargetDispY.Y;
  Disp.XZ = Orbit1X.Z;
  Disp.YZ = Orbit1Y.Z;
