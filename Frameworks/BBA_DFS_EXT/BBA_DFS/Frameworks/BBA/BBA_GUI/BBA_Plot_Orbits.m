function BBA_Plot_Orbits()
global BBA_PLOTS BBA_DATA

 BBA.wgt_orb_x = BBA_DATA.wgt_orb_x * BBA_DATA.do_apply_orbit_x;
  BBA.wgt_orb_y = BBA_DATA.wgt_orb_y * BBA_DATA.do_apply_orbit_y;
  %
  BBA.wgt_dfs_x = BBA_DATA.wgt_dfs_x * BBA_DATA.do_apply_dfs_x;
  BBA.wgt_dfs_y = BBA_DATA.wgt_dfs_y * BBA_DATA.do_apply_dfs_y;
  %
  BBA.wgt_wfs_x = BBA_DATA.wgt_wfs_x * BBA_DATA.do_apply_wfs_x;
  BBA.wgt_wfs_y = BBA_DATA.wgt_wfs_y * BBA_DATA.do_apply_wfs_y;
  %
  BBA.wgt_ff_x  = BBA_DATA.wgt_ff_x * BBA_DATA.do_apply_ff_x;
  BBA.wgt_ff_y  = BBA_DATA.wgt_ff_y * BBA_DATA.do_apply_ff_y;

figure(2);

if BBA_DATA.do_plot_orbit
subplot(3,1,1);

% should be BPM Z-pos, currently not available
X =  BBA_PLOTS.data( BBA_PLOTS.nsteps ).Orbit.X;
X_exp = X + BBA_PLOTS.data( BBA_PLOTS.nsteps ).R0.Rx * BBA_PLOTS.data( BBA_PLOTS.nsteps ).Corrs.X;
ZX = 1:length( X );

Y =  BBA_PLOTS.data( BBA_PLOTS.nsteps ).Orbit.Y;
Y_exp = Y + BBA_PLOTS.data( BBA_PLOTS.nsteps ).R0.Ry * BBA_PLOTS.data( BBA_PLOTS.nsteps ).Corrs.Y;
ZY = 1:length( Y );
hh = plot( ZX, X  , '-xr');
set(hh, 'LineWidth', 5);
hold on;
hh = plot( ZX, X_exp  , '-og');
set(hh, 'LineWidth', 1);
hh = plot( ZY, Y  , '-xk');
set(hh, 'LineWidth', 5);
hh = plot( ZY, Y_exp  , '-oc');
set(hh, 'LineWidth', 1);
hold off;
xlabel('s [m]');
ylabel('Orbit [mm]');
legend('X','expd. X', 'Y', 'expd. Y', 'Location', 'SouthEast');
end

if BBA_DATA.do_plot_disp & (BBA.wgt_dfs_x > 0 | BBA.wgt_dfs_y > 0)
subplot(3,1,2);

% should be BPM Z-pos, currently not available
DX =  BBA_PLOTS.data( BBA_PLOTS.nsteps ).Disp.X;
DX_exp = DX + BBA_PLOTS.data( BBA_PLOTS.nsteps ).R1.Rx * BBA_PLOTS.data( BBA_PLOTS.nsteps ).Corrs.X;
ZDX = 1:length( DX );

DY =  BBA_PLOTS.data( BBA_PLOTS.nsteps ).Disp.Y;
DY_exp = DY + BBA_PLOTS.data( BBA_PLOTS.nsteps ).R1.Ry * BBA_PLOTS.data( BBA_PLOTS.nsteps ).Corrs.Y;
ZDY = 1:length( DY );

hh = plot( ZDX, DX  , '-xr');
set(hh, 'LineWidth', 5);
hold on;
hh = plot( ZDX, DX_exp  , '-og');
set(hh, 'Linewidth', 1);
hh = plot( ZDY, DY  , '-xk');
set(hh, 'LineWidth', 5);
hh = plot( ZDY, DY_exp  , '-oc');
set(hh, 'LineWidth', 1);
hold off;
xlabel('s [m]');
ylabel('Disp. diff. orbit [mm]');
legend('Dx','expd. Dx',  'Dy', 'expd. Dy', 'Location', 'SouthEast');
end

if BBA_DATA.do_plot_wake & (BBA.wgt_wfs_x > 0 | BBA.wgt_wfs_y > 0)
subplot(3,1,3);

% should be BPM Z-pos, currently not available
WX =  BBA_PLOTS.data( BBA_PLOTS.nsteps ).Wake.X;
WX_exp = WX + BBA_PLOTS.data( BBA_PLOTS.nsteps ).R2.Rx * BBA_PLOTS.data( BBA_PLOTS.nsteps ).Corrs.X;
ZDX2 = 1:length( WX );

WY =  BBA_PLOTS.data( BBA_PLOTS.nsteps ).Wake.Y;
WY_exp = WY + BBA_PLOTS.data( BBA_PLOTS.nsteps ).R2.Ry * BBA_PLOTS.data( BBA_PLOTS.nsteps ).Corrs.Y;
ZDY2 = 1:length( WY );

hh = plot( ZDX2, WX  , '-xr');
set(hh, 'LineWidth', 5);
hold on;
hh = plot( ZDX2, WX_exp  , '-og');
set(hh, 'Linewidth', 1);
hh = plot( ZDY2, WY  , '-xk');
set(hh, 'LineWidth', 5);
hh = plot( ZDY2, WY_exp  , '-oc');
set(hh, 'LineWidth', 1);
hold off;
xlabel('s [m]');
ylabel('Wake diff. orbit [mm]');
legend('Wx','expd. Wx',  'Wy', 'expd. Wy', 'Location', 'SouthEast');

end
end
