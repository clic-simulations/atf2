function CORS = FigureOutCors(R0, X_mask, Y_mask)
  global BBA_DATA
  do_coupling = BBA_DATA.do_include_coupled_terms;
  CORS.X = struct('name', [], 'R0_index', []);
  CORS.Y = struct('name', [], 'R0_index', []);
  R0x_i = 1;
  R0y_i = 1;
  for i=1:length(BBA_DATA.Correctors)
    cor_name = BBA_DATA.Correctors(i).name;
    cor_mask = BBA_DATA.Correctors(i).mask;
    cor_index = find(fnmatch(cor_name, R0.CORS));
    if ~isempty(cor_index)
      if bitand(cor_mask, X_mask) % employed for X
        if do_coupling == true || fnmatch('ZH*', { cor_name })
          cor_index = unique(cor_index);
          CORS.X(R0x_i).name = cor_name;
          CORS.X(R0x_i).R0_index = cor_index;
          R0x_i = R0x_i + 1;
        end
      end
      if bitand(cor_mask, Y_mask) % employed for X
        if do_coupling == true || fnmatch('ZV*', { cor_name }) 
          cor_index = unique(cor_index);
          CORS.Y(R0y_i).name = cor_name;
          CORS.Y(R0y_i).R0_index = cor_index;
          R0y_i = R0y_i + 1;
        end
      end
    end
  end
