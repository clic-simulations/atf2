function R2 = BBA_GetResponse_Wakefield()
  global BBA_DATA

  load(BBA_DATA.Response0_file, '-mat')
  R0 = Response;

  load(BBA_DATA.Response2_file, '-mat')
  R2 = Response;

  CORS_Wfs = FigureOutCors(R0, 8, 4);
  BPMS_Wfs = FigureOutBpms(R0, 8, 4);
  
  %%%% apply masks to the response matrices

  R0.Rx = R0.Rx( [ BPMS_Wfs.X.R0_index ], [ CORS_Wfs.X.R0_index ]);
  R0.Ry = R0.Ry( [ BPMS_Wfs.Y.R0_index ], [ CORS_Wfs.Y.R0_index ]);

  R2.Rx = R2.Rx( [ BPMS_Wfs.X.R0_index ], [ CORS_Wfs.X.R0_index ]) - R0.Rx;
  R2.Ry = R2.Ry( [ BPMS_Wfs.Y.R0_index ], [ CORS_Wfs.Y.R0_index ]) - R0.Ry;

end
