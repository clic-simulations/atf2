function retBpms = BBA_GetBpmsList()
  global BBA_DATA
  Bpms = BBA_DATA.Bpms;
  if strcmp(Bpms(1).name,'<empty>')
    retBpms{1} = '<empty>';
    return
  end
  retBpms = cell(length(Bpms),1);
  for i=1:length(Bpms)
    name = Bpms(i).name;
    mask = Bpms(i).mask;
    str = [ name '  ' ];
    if bitand(mask, 128) ; str = [ str 'Ox' ] ; else ; str = [ str 'O-' ]; end
    if bitand(mask, 64) ; str = [ str 'y ' ] ; else ; str = [ str '- ' ]; end
    if bitand(mask, 32) ; str = [ str 'Dx' ] ; else ; str = [ str 'D-' ]; end
    if bitand(mask, 16) ; str = [ str 'y ' ] ; else ; str = [ str '- ' ]; end
    if bitand(mask, 8) ; str = [ str 'Wx' ] ; else ; str = [ str 'W-' ]; end
    if bitand(mask, 4) ; str = [ str 'y ' ] ; else ; str = [ str '- ' ]; end
    if bitand(mask, 2) ; str = [ str ' Fx' ] ; else ; str = [ str ' F-' ]; end
    if bitand(mask, 1) ; str = [ str 'y' ] ; else ; str = [ str '-' ]; end
    retBpms{i} = str;
  end
