function BBA_AcquireMachineState()
    global BBA_DATA;
    ans = questdlg('Do you want to acquire a new machine state or load an old one?', 'BBA', 'New', 'Old','Old');
    switch ans
    case 'New'
      disp('Acquiring the status of the machine...');
      machine_state_file = sprintf('machine_status_%s.mat',strrep(datestr(clock, 30), 'T', '_'));
      BBA_DATA.State = GetMachine(BBA_DATA.Interface);
      BBA_DATA.StateFile = machine_state_file;
      Save(BBA_DATA.State, machine_state_file);
    case 'Old'
      [FileName,PathName] = uigetfile('*.mat','Select a machine status file');
      if FileName == 0
        disp('Ok...');
        return;
      end
      dirname = BBA_DATA.Directory;
      if PathName(end) == '/' ; PathName = PathName(1:end-1); end
      if dirname(end) == '/' ; dirname = dirname(1:end-1); end
      if strcmp(PathName, dirname) == 0
        copyfile([PathName '/' FileName],[dirname '/' FileName]);
      end
      BBA_DATA.State = State([dirname '/' FileName]);
      BBA_DATA.StateFile = FileName;
    end
