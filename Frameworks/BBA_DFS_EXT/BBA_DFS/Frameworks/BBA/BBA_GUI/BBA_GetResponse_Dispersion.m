function R1 = BBA_GetResponse_Dispersion()
  global BBA_DATA

  load(BBA_DATA.Response0_file, '-mat')
  R0 = Response;

  load(BBA_DATA.Response1_file, '-mat')
  R1 = Response;

  CORS_Dfs = FigureOutCors(R0, 32, 16);
  BPMS_Dfs = FigureOutBpms(R0, 32, 16);
  
  %%%% apply masks to the response matrices

  R0.Rx = R0.Rx( [ BPMS_Dfs.X.R0_index ], [ CORS_Dfs.X.R0_index ]);
  R0.Ry = R0.Ry( [ BPMS_Dfs.Y.R0_index ], [ CORS_Dfs.Y.R0_index ]);

  R1.Rx = R1.Rx( [ BPMS_Dfs.X.R0_index ], [ CORS_Dfs.X.R0_index ]) - R0.Rx;
  R1.Ry = R1.Ry( [ BPMS_Dfs.Y.R0_index ], [ CORS_Dfs.Y.R0_index ]) - R0.Ry;

end
