function retCorrs = BBA_GetCorrectorsList()
  global BBA_DATA
  Corrs = BBA_DATA.Correctors;
  if strcmp(Corrs(1).name, '<empty>')
    retCorrs{1} = '<empty>';
    return
  end
  retCorrs = cell(length(Corrs),1);
  for i=1:length(Corrs)
    name = Corrs(i).name;
    mask = Corrs(i).mask;
    str = [ name '  ' ];
    if bitand(mask, 128) ; str = [ str 'O ' ] ; else ; str = [ str '- ' ]; end
    if bitand(mask, 32) ; str = [ str 'D ' ] ; else ; str = [ str '- ' ]; end
    if bitand(mask, 8) ; str = [ str 'W ' ] ; else ; str = [ str '- ' ]; end
    if bitand(mask, 2) ; str = [ str ' F' ] ; else ; str = [ str ' -' ]; end
    % should be any correctors used independenly to correct X and Y
    %if bitand(mask, 128) ; str = [ str 'Ox' ] ; else ; str = [ str 'O-' ]; end
    %if bitand(mask, 64) ; str = [ str 'y ' ] ; else ; str = [ str '- ' ]; end
    %if bitand(mask, 32) ; str = [ str 'Dx' ] ; else ; str = [ str 'D-' ]; end
    %if bitand(mask, 16) ; str = [ str 'y ' ] ; else ; str = [ str '- ' ]; end
    %if bitand(mask, 8) ; str = [ str 'Wx' ] ; else ; str = [ str 'W-' ]; end
    %if bitand(mask, 4) ; str = [ str 'y ' ] ; else ; str = [ str '- ' ]; end
    %if bitand(mask, 2) ; str = [ str 'Fx' ] ; else ; str = [ str 'F-' ]; end
    %if bitand(mask, 1) ; str = [ str 'y' ] ; else ; str = [ str '-' ]; end
    retCorrs{i} = str;
  end
