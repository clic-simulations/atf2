function varargout = BBA_GUI(varargin)
% BBA_GUI M-file for BBA_GUI.fig
%      BBA_GUI, by itself, creates a new BBA_GUI or raises the existing
%      singleton*.
%
%      H = BBA_GUI returns the handle to a new BBA_GUI or the handle to
%      the existing singleton*.
%
%      BBA_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BBA_GUI.M with the given input arguments.
%
%      BBA_GUI('Property','Value',...) creates a new BBA_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before BBA_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to BBA_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help BBA_GUI

% Last Modified by GUIDE v2.5 22-Mar-2014 07:28:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @BBA_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @BBA_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before BBA_GUI is made visible.
function BBA_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to BBA_GUI (see VARARGIN)

% Choose default command line output for BBA_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
    global BBA_DATA
    set(handles.text_Mode, 'String', GetDescription(BBA_DATA.Interface));
    [path,basedir] = fileparts(BBA_DATA.Directory);
    set(handles.edit_dir, 'String', basedir);
    set(handles.text_machine_status, 'String', BBA_DATA.StateFile);
    set(handles.list_corr, 'String', BBA_GetCorrectorsList());
    set(handles.list_bpm, 'String', BBA_GetBpmsList());
    set(handles.cb_bba_orbit_x, 'Value', BBA_DATA.do_apply_orbit_x);
    set(handles.cb_bba_orbit_y, 'Value', BBA_DATA.do_apply_orbit_y);
    set(handles.cb_bba_dfs_x, 'Value', BBA_DATA.do_apply_dfs_x);
    set(handles.cb_bba_dfs_y, 'Value', BBA_DATA.do_apply_dfs_y);
    set(handles.cb_bba_wfs_x, 'Value', BBA_DATA.do_apply_wfs_x);
    set(handles.cb_bba_wfs_y, 'Value', BBA_DATA.do_apply_wfs_y);
    set(handles.cb_bba_ff_x, 'Value', BBA_DATA.do_apply_ff_x);
    set(handles.cb_bba_ff_y, 'Value', BBA_DATA.do_apply_ff_y);
    set(handles.cb_plot_orbit, 'Value', BBA_DATA.do_plot_orbit);
    set(handles.cb_plot_dispersion, 'Value', BBA_DATA.do_plot_disp);
    set(handles.cb_plot_wakes, 'Value', BBA_DATA.do_plot_wake);
    set(handles.cb_plot_orbitC, 'Value', BBA_DATA.do_plot_orbit_convergence);
    set(handles.cb_plot_dispersionC, 'Value', BBA_DATA.do_plot_disp_convergence);
    set(handles.cb_plot_wakesC, 'Value', BBA_DATA.do_plot_wake_convergence);
    set(handles.cb_plot_corr, 'Value', BBA_DATA.do_plot_corrector_strengths);
    set(handles.edit_response0, 'String', BBA_DATA.Response0_file);
    set(handles.edit_response1, 'String', BBA_DATA.Response1_file);
    set(handles.edit_response2, 'String', BBA_DATA.Response2_file);
    set(handles.edit_change_energy, 'String', BBA_DATA.ChangeEnergy_cmd);
    set(handles.edit_reset_energy, 'String', BBA_DATA.ResetEnergy_cmd);
    set(handles.edit_change_charge, 'String', BBA_DATA.ChangeCharge_cmd);
    set(handles.edit_reset_charge, 'String', BBA_DATA.ResetCharge_cmd);
    set(handles.edit_gainx, 'String', BBA_DATA.gain_x);
    set(handles.edit_gainy, 'String', BBA_DATA.gain_y);
    set(handles.edit_orbitx, 'String', BBA_DATA.wgt_orb_x);
    set(handles.edit_orbity, 'String', BBA_DATA.wgt_orb_y);
    set(handles.edit_dfsx, 'String', BBA_DATA.wgt_dfs_x);
    set(handles.edit_dfsy, 'String', BBA_DATA.wgt_dfs_y);
    set(handles.edit_wfsx, 'String', BBA_DATA.wgt_wfs_x);
    set(handles.edit_wfsy, 'String', BBA_DATA.wgt_wfs_y);
    set(handles.edit_ffx, 'String', BBA_DATA.wgt_ff_x);
    set(handles.edit_ffy, 'String', BBA_DATA.wgt_ff_y);
    set(handles.edit_svdx, 'String', BBA_DATA.svd_nsingularvalues_x);
    set(handles.edit_svdy, 'String', BBA_DATA.svd_nsingularvalues_y);
    set(handles.cb_bba_coupling, 'Value', BBA_DATA.do_include_coupled_terms);

% UIWAIT makes BBA_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = BBA_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit_gainx_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.gain_x = str2num(get(hObject,'String'));
  set(handles.edit_gainx, 'String', BBA_DATA.gain_x);

% hObject    handle to edit_gainx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_gainx as text
%        str2double(get(hObject,'String')) returns contents of edit_gainx as a double


% --- Executes during object creation, after setting all properties.
function edit_gainx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_gainx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_dfsx_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.wgt_dfs_x = str2num(get(hObject,'String'));
  set(handles.edit_dfsx, 'String', BBA_DATA.wgt_dfs_x);
% hObject    handle to edit_dfsx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_dfsx as text
%        str2double(get(hObject,'String')) returns contents of edit_dfsx as a double


% --- Executes during object creation, after setting all properties.
function edit_dfsx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_dfsx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_wfsx_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.wgt_wfs_x = str2num(get(hObject,'String'));
  set(handles.edit_wfsx, 'String', BBA_DATA.wgt_wfs_x);
% hObject    handle to edit_wfsx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_wfsx as text
%        str2double(get(hObject,'String')) returns contents of edit_wfsx as a double


% --- Executes during object creation, after setting all properties.
function edit_wfsx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_wfsx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_svdx_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.svd_nsingularvalues_x = str2num(get(hObject,'String'));
  set(handles.edit_svdx, 'String', BBA_DATA.svd_nsingularvalues_x);

% hObject    handle to edit_svdx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_svdx as text
%        str2double(get(hObject,'String')) returns contents of edit_svdx as a double


% --- Executes during object creation, after setting all properties.
function edit_svdx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_svdx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_response0_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.Response0_file = get(hObject,'String');
  

% hObject    handle to edit_response0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_response0 as text
%        str2double(get(hObject,'String')) returns contents of edit_response0 as a double


% --- Executes during object creation, after setting all properties.
function edit_response0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_response0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_response1_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.Response1_file = get(hObject,'String');
  
% hObject    handle to edit_response1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_response1 as text
%        str2double(get(hObject,'String')) returns contents of edit_response1 as a double


% --- Executes during object creation, after setting all properties.
function edit_response1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_response1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pb_load_resp0.
function pb_load_resp0_Callback(hObject, eventdata, handles)
% hObject    handle to pb_load_resp0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pb_load_resp1.
function pb_load_resp1_Callback(hObject, eventdata, handles)
% hObject    handle to pb_load_resp1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit_response2_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.Response2_file = get(hObject,'String');

% hObject    handle to edit_response2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_response2 as text
%        str2double(get(hObject,'String')) returns contents of edit_response2 as a double


% --- Executes during object creation, after setting all properties.
function edit_response2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_response2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pb_load_resp2.
function pb_load_resp2_Callback(hObject, eventdata, handles)
% hObject    handle to pb_load_resp2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pb_showR1.
function pb_showR1_Callback(hObject, eventdata, handles)
global BBA_DATA plotR1
  load(BBA_DATA.Response1_file, '-mat');
  if isempty(plotR1)
    plotR1 = figure;
  end
  figure(plotR1);
  subplot(2,2,1);
  surf(Response.Rx(:,Response.MASK_CORSX));
  xlabel('X correctors');
  ylabel('X bpms');
  zlabel('R_{xx}');
  subplot(2,2,2);
  surf(Response.Rx(:,Response.MASK_CORSY));
  xlabel('Y correctors');
  ylabel('X bpms');
  zlabel('R_{xy}');
  subplot(2,2,3);
  surf(Response.Ry(:,Response.MASK_CORSX));
  xlabel('X correctors');
  ylabel('Y bpms');
  zlabel('R_{yx}');
  subplot(2,2,4);
  surf(Response.Ry(:,Response.MASK_CORSY));
  xlabel('Y correctors');
  ylabel('Y bpms');
  zlabel('R_{yy}');
% hObject    handle to pb_showR1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pb_showR2.
function pb_showR2_Callback(hObject, eventdata, handles)
global BBA_DATA plotR2
  load(BBA_DATA.Response2_file, '-mat');
  if isempty(plotR2)
    plotR2 = figure;
  end
  figure(plotR2);
  subplot(2,2,1);
  surf(Response.Rx(:,Response.MASK_CORSX));
  xlabel('X correctors');
  ylabel('X bpms');
  zlabel('R_{xx}');
  subplot(2,2,2);
  surf(Response.Rx(:,Response.MASK_CORSY));
  xlabel('Y correctors');
  ylabel('X bpms');
  zlabel('R_{xy}');
  subplot(2,2,3);
  surf(Response.Ry(:,Response.MASK_CORSX));
  xlabel('X correctors');
  ylabel('Y bpms');
  zlabel('R_{yx}');
  subplot(2,2,4);
  surf(Response.Ry(:,Response.MASK_CORSY));
  xlabel('Y correctors');
  ylabel('Y bpms');
  zlabel('R_{yy}');
% hObject    handle to pb_showR2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in list_bpm.
function list_bpm_Callback(hObject, eventdata, handles)
% hObject    handle to list_bpm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns list_bpm contents as cell array
%        contents{get(hObject,'Value')} returns selected item from list_bpm


% --- Executes during object creation, after setting all properties.
function list_bpm_CreateFcn(hObject, eventdata, handles)
% hObject    handle to list_bpm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pb_add_bpm.
function pb_add_bpm_Callback(hObject, eventdata, handles)
  global BBA_DATA
  load(BBA_DATA.Response0_file, '-mat')
  add_corrs_arg = struct;
  add_corrs_arg.Correctors = Response.BPMS;
  add_corrs_arg.Directory = BBA_DATA.Directory;
  add_corrs_arg.Pattern = 'BPMS:LI??:*';
  BBA_DisableGUI(handles);
  names = BBA_Select_from_list(add_corrs_arg);
  BBA_EnableGUI(handles);
  if isempty(names)
      return;
  end
  if strcmp(BBA_DATA.Bpms(1).name, '<empty>')
    nbpms = 0;
  else
    nbpms = length(BBA_DATA.Bpms);
  end
  if nbpms > 0
      names = setdiff( names , { BBA_DATA.Bpms.name }' );
  end
  for i=1:length(names)
    BBA_DATA.Bpms(nbpms + i).name = names{i};
    BBA_DATA.Bpms(nbpms + i).mask = 0;
  end
  temp = zeros(length(BBA_DATA.Bpms),1);
  for i=1:length(BBA_DATA.Bpms)
    temp(i) = GetBpmsZ(BBA_DATA.State, { BBA_DATA.Bpms(i).name });
  end
  [S,I] = sort(temp);
  BBA_DATA.Bpms = BBA_DATA.Bpms(I);
  set(handles.list_bpm, 'Value', []);
  set(handles.list_bpm, 'String', BBA_GetBpmsList());
% hObject    handle to pb_add_bpm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pb_rem_bpm.
function pb_rem_bpm_Callback(hObject, eventdata, handles)
  global BBA_DATA
  selection = get(handles.list_bpm,'Value');
  remaining = setxor(1:length(BBA_DATA.Bpms), selection);
  if length(remaining) == 0
    BBA_DATA.Bpms = struct('name', [], 'mask', []);
    BBA_DATA.Bpms(1).name = '<empty>';
    BBA_DATA.Bpms(1).mask = 0;
  else
    BBA_DATA.Bpms = BBA_DATA.Bpms(remaining);
  end
  set(handles.list_bpm, 'Value', []);
  set(handles.list_bpm, 'String', BBA_GetBpmsList());
% hObject    handle to pb_rem_bpm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in list_corr.
function list_corr_Callback(hObject, eventdata, handles)
% hObject    handle to list_corr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns list_corr contents as cell array
%        contents{get(hObject,'Value')} returns selected item from list_corr


% --- Executes during object creation, after setting all properties.
function list_corr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to list_corr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pb_add_corr.
function pb_add_corr_Callback(hObject, eventdata, handles)
  global BBA_DATA
  load(BBA_DATA.Response0_file, '-mat')
  add_corrs_arg = struct;
  add_corrs_arg.Correctors = Response.CORS;
  add_corrs_arg.Directory = BBA_DATA.Directory;
  add_corrs_arg.Pattern = '?COR:LI??:*';
  BBA_DisableGUI(handles);
  names = BBA_Select_from_list(add_corrs_arg);
  BBA_EnableGUI(handles);
  if isempty(names)
      return;
  end
  if strcmp(BBA_DATA.Correctors(1).name, '<empty>')
    ncorrs = 0;
  else
    ncorrs = length(BBA_DATA.Correctors);
  end
  if ncorrs > 0
      names = setdiff( names , { BBA_DATA.Correctors.name }' );
  end
  for i=1:length(names)
    BBA_DATA.Correctors(ncorrs + i).name = names{i};
    BBA_DATA.Correctors(ncorrs + i).mask = 0;
  end
  temp = cell(length(BBA_DATA.Correctors),1);
  for i=1:length(BBA_DATA.Correctors)
    temp{i} = BBA_DATA.Correctors(i).name(2:end);
  end
  [S,I] = sort(temp);
  BBA_DATA.Correctors = BBA_DATA.Correctors(I);
  set(handles.list_corr, 'Value', []);
  set(handles.list_corr, 'String', BBA_GetCorrectorsList());
% hObject    handle to pb_add_corr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pb_rem_corr.
function pb_rem_corr_Callback(hObject, eventdata, handles)
  global BBA_DATA
  selection = get(handles.list_corr,'Value');
  remaining = setxor(1:length(BBA_DATA.Correctors), selection);
  if length(remaining) == 0
    BBA_DATA.Correctors = struct('name', [], 'mask', []);
    BBA_DATA.Correctors(1).name = '<empty>';
    BBA_DATA.Correctors(1).mask = 0;
  else
    BBA_DATA.Correctors = BBA_DATA.Correctors(remaining);
  end
  set(handles.list_corr, 'Value', []);
  set(handles.list_corr, 'String', BBA_GetCorrectorsList());

% hObject    handle to pb_rem_corr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit_corr_pattern_Callback(hObject, eventdata, handles)
global BBA_DATA
  pattern = get(hObject,'String');
  set(handles.list_corr,'Value', find(fnmatch(pattern, { BBA_DATA.Correctors.name } )));
% 
% hObject    handle to edit_corr_pattern (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_corr_pattern as text
%        str2double(get(hObject,'String')) returns contents of edit_corr_pattern as a double


% --- Executes during object creation, after setting all properties.
function edit_corr_pattern_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_corr_pattern (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pb_compute.
function pb_compute_Callback(hObject, eventdata, handles)
BBA_DisableGUI(handles);
pause(0.1)
BBA_ComputeCorrection();
BBA_EnableGUI(handles);
% hObject    handle to pb_compute (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


function edit_change_energy_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.ChangeEnergy_cmd = get(hObject,'String');

% hObject    handle to edit_change_energy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_change_energy as text
%        str2double(get(hObject,'String')) returns contents of edit_change_energy as a double


% --- Executes during object creation, after setting all properties.
function edit_change_energy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_change_energy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_reset_energy_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.ResetEnergy_cmd = get(hObject,'String');
% hObject    handle to edit_reset_energy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_reset_energy as text
%        str2double(get(hObject,'String')) returns contents of edit_reset_energy as a double


% --- Executes during object creation, after setting all properties.
function edit_reset_energy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_reset_energy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton17.
function pushbutton17_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton18.
function pushbutton18_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit_change_charge_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.ChangeCharge_cmd = get(hObject,'String');
% hObject    handle to edit_change_charge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_change_charge as text
%        str2double(get(hObject,'String')) returns contents of edit_change_charge as a double


% --- Executes during object creation, after setting all properties.
function edit_change_charge_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_change_charge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton19.
function pushbutton19_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit_reset_charge_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.ResetCharge_cmd = get(hObject,'String');
% hObject    handle to edit_reset_charge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_reset_charge as text
%        str2double(get(hObject,'String')) returns contents of edit_reset_charge as a double


% --- Executes during object creation, after setting all properties.
function edit_reset_charge_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_reset_charge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton20.
function pushbutton20_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in text3.
function cb_do_wakes_Callback(hObject, eventdata, handles)
% hObject    handle to text3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of text3


% --- Executes on button press in text1.
function cb_do_orbit_Callback(hObject, eventdata, handles)
% hObject    handle to text1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of text1


% --- Executes on button press in text2.
function cb_do_dispersion_Callback(hObject, eventdata, handles)
% hObject    handle to text2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of text2


% --- Executes on button press in cb_plot_wakes.
function cb_plot_wakes_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.do_plot_wake = get(hObject,'Value');
% hObject    handle to cb_plot_wakes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_plot_wakes


% --- Executes on button press in cb_plot_orbit.
function cb_plot_orbit_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.do_plot_orbit = get(hObject,'Value');
% 
% hObject    handle to cb_plot_orbit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_plot_orbit


% --- Executes on button press in cb_plot_dispersion.
function cb_plot_dispersion_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.do_plot_disp = get(hObject,'Value');
% hObject    handle to cb_plot_dispersion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_plot_dispersion


% --- Executes on button press in cb_plot_wakesC.
function cb_plot_wakesC_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.do_plot_wake_convergence = get(hObject,'Value');

% hObject    handle to cb_plot_wakesC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_plot_wakesC


% --- Executes on button press in cb_plot_orbitC.
function cb_plot_orbitC_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.do_plot_orbit_convergence = get(hObject,'Value');
% hObject    handle to cb_plot_orbitC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_plot_orbitC


% --- Executes on button press in cb_plot_dispersionC.
function cb_plot_dispersionC_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.do_plot_disp_convergence = get(hObject,'Value');

% hObject    handle to cb_plot_dispersionC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_plot_dispersionC


% --- Executes on button press in pb_clear_plots.
function pb_clear_plots_Callback(hObject, eventdata, handles)
global BBA_PLOTS
  BBA_PLOTS.nsteps = 0;
  BBA_PLOTS.data(:) = [];
  clf(2);
  clf(3);
  clf(4);

  
% hObject    handle to pb_clear_plots (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in cb_plot_corr.
function cb_plot_corr_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.do_plot_corrector_strengths = get(hObject,'Value');
% hObject    handle to cb_plot_corr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_plot_corr


% --- Executes on button press in pb_showR0.
function pb_showR0_Callback(hObject, eventdata, handles)
global BBA_DATA plotR0
  load(BBA_DATA.Response0_file, '-mat');
  if isempty(plotR0)
    plotR0 = figure;
  end
  figure(plotR0);
  subplot(2,2,1);
  surf(Response.Rx(:,Response.MASK_CORSX));
  xlabel('X correctors');
  ylabel('X bpms');
  zlabel('R_{xx}');
  subplot(2,2,2);
  surf(Response.Rx(:,Response.MASK_CORSY));
  xlabel('Y correctors');
  ylabel('X bpms');
  zlabel('R_{xy}');
  subplot(2,2,3);
  surf(Response.Ry(:,Response.MASK_CORSX));
  xlabel('X correctors');
  ylabel('Y bpms');
  zlabel('R_{yx}');
  subplot(2,2,4);
  surf(Response.Ry(:,Response.MASK_CORSY));
  xlabel('Y correctors');
  ylabel('Y bpms');
  zlabel('R_{yy}');
% hObject    handle to pb_showR0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pb_showsvd.
function pb_showsvd_Callback(hObject, eventdata, handles)
global BBA_DATA plotsvdR
  [Rx, Ry] = BBA_GetResponseMatrix;
 
  if isempty(plotsvdR)
    plotsvdR = figure;
  end
  figure(plotsvdR);

  subplot(1,1,1);
    if BBA_DATA.do_include_coupled_terms
    Sx = svd([ Rx ; Ry ]);
    plot(Sx, '-b*');
  else
    Sx = svd(Rx);
    Sy = svd(Ry);
    plot(1:length(Sx), Sx, '-r*', 1:length(Sy), Sy, '-b*');
  end
  xlabel('singular values [nb.]');
  ylabel('value');

% hObject    handle to pb_showsvd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in cb_bba_coupling.
function cb_bba_coupling_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.do_include_coupled_terms = get(hObject,'Value');

% hObject    handle to cb_bba_coupling (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_bba_coupling


% --- Executes on button press in pb_apply.
function pb_apply_Callback(hObject, eventdata, handles)
% hObject    handle to pb_apply (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit_ffx_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.wgt_ff_x = str2num(get(hObject,'String'));
  set(handles.edit_ffx, 'String', BBA_DATA.wgt_ff_x);
% hObject    handle to edit_ffx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_ffx as text
%        str2double(get(hObject,'String')) returns contents of edit_ffx as a double


% --- Executes during object creation, after setting all properties.
function edit_ffx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_ffx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in text4.
function cb_do_ff_Callback(hObject, eventdata, handles)
% hObject    handle to text4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of text4


% --- Executes on button press in pb_load_bpm.
function pb_load_bpm_Callback(hObject, eventdata, handles)
  global BBA_DATA;
  BBA_DisableGUI(handles);
  [FileName,PathName] = uigetfile('*.txt','Select a file with a list of BPMs');
  if FileName ~= 0
    fid = fopen([PathName FileName]);
    C = textscan(fid,'%s %s');
    fclose(fid);
    if isempty(C)
        disp('The file seems empty...');
        BBA_EnableGUI(handles);
        return;
    end
    BBA_DATA.Bpms = struct('name', [], 'mask', []);
    bpms = C{1};
    masks = C{2};
    for i=1:length(bpms)
        BBA_DATA.Bpms(i).name = bpms{i};
        BBA_DATA.Bpms(i).mask = bin2dec(masks{i});
    end
    temp = GetBpmsZ(BBA_DATA.State, { BBA_DATA.Bpms.name });
    [S,I] = sort(temp);
    BBA_DATA.Bpms = BBA_DATA.Bpms(I);
    set(handles.list_bpm, 'String', BBA_GetBpmsList());
  end
  BBA_EnableGUI(handles);

% hObject    handle to pb_load_bpm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pb_load_corr.
function pb_load_corr_Callback(hObject, eventdata, handles)
  global BBA_DATA;
  BBA_DisableGUI(handles);
  [FileName,PathName] = uigetfile('*.txt','Select a file with a list of correctors');
  if FileName ~= 0
    fid = fopen([PathName FileName]);
    C = textscan(fid,'%s %s');
    fclose(fid);
    if isempty(C)
        disp('The file seems empty...');
        BBA_EnableGUI(handles);
        return;
    end
    BBA_DATA.Correctors = struct('name', [], 'mask', []);
    corrs = C{1};
    masks = C{2};
    temp = cell(length(corrs),1);
    for i=1:length(corrs)
      temp{i} = corrs{i}(2:end);
    end
    [S,I] = sort(temp);
    for i=1:length(I)
      BBA_DATA.Correctors(i).name = corrs{I(i)};
      BBA_DATA.Correctors(i).mask = bin2dec(masks{I(i)});
    end
    set(handles.list_corr, 'String', BBA_GetCorrectorsList());
  end
  BBA_EnableGUI(handles);

% hObject    handle to pb_load_corr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit_bpm_pattern_Callback(hObject, eventdata, handles)
global BBA_DATA
  pattern = get(hObject,'String');
  set(handles.list_bpm,'Value', find(fnmatch(pattern, { BBA_DATA.Bpms } )));

% hObject    handle to edit_bpm_pattern (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_bpm_pattern as text
%        str2double(get(hObject,'String')) returns contents of edit_bpm_pattern as a double


% --- Executes during object creation, after setting all properties.
function edit_bpm_pattern_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_bpm_pattern (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pb_acquire_orbit.
function pb_acquire_orbit_Callback(hObject, eventdata, handles)
    global BBA_DATA;
    BBA_AcquireMachineState();
    set(handles.text_machine_status, 'String', BBA_DATA.StateFile);
% hObject    handle to pb_acquire_orbit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit_gainy_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.gain_y = str2num(get(hObject,'String'));
  set(handles.edit_gainy, 'String', BBA_DATA.gain_y);
% hObject    handle to edit_gainy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_gainy as text
%        str2double(get(hObject,'String')) returns contents of edit_gainy as a double


% --- Executes during object creation, after setting all properties.
function edit_gainy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_gainy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_dfsy_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.wgt_dfs_y = str2num(get(hObject,'String'));
  set(handles.edit_dfsy, 'String', BBA_DATA.wgt_dfs_y);
% hObject    handle to edit_dfsy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_dfsy as text
%        str2double(get(hObject,'String')) returns contents of edit_dfsy as a double


% --- Executes during object creation, after setting all properties.
function edit_dfsy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_dfsy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_wfsy_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.wgt_wfs_y = str2num(get(hObject,'String'));
  set(handles.edit_wfsy, 'String', BBA_DATA.wgt_wfs_y);

% hObject    handle to edit_wfsy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_wfsy as text
%        str2double(get(hObject,'String')) returns contents of edit_wfsy as a double


% --- Executes during object creation, after setting all properties.
function edit_wfsy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_wfsy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_svdy_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.svd_nsingularvalues_y = str2num(get(hObject,'String'));
  set(handles.edit_svdy, 'String', BBA_DATA.svd_nsingularvalues_y);
% hObject    handle to edit_svdy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_svdy as text
%        str2double(get(hObject,'String')) returns contents of edit_svdy as a double


% --- Executes during object creation, after setting all properties.
function edit_svdy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_svdy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cb_bba_orbit_x.
function cb_bba_orbit_x_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.do_apply_orbit_x = get(hObject,'Value');
% hObject    handle to cb_bba_orbit_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_bba_orbit_x


% --- Executes on button press in cb_bba_dfs_x.
function cb_bba_dfs_x_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.do_apply_dfs_x = get(hObject,'Value');
% hObject    handle to cb_bba_dfs_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_bba_dfs_x


% --- Executes on button press in cb_bba_wfs_x.
function cb_bba_wfs_x_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.do_apply_wfs_x = get(hObject,'Value');
% hObject    handle to cb_bba_wfs_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_bba_wfs_x


% --- Executes on button press in cb_bba_ff_x.
function cb_bba_ff_x_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.do_apply_ff_x = get(hObject,'Value');
% hObject    handle to cb_bba_ff_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_bba_ff_x


% --- Executes on button press in cb_bba_orbit_y.
function cb_bba_orbit_y_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.do_apply_orbit_y = get(hObject,'Value');

% hObject    handle to cb_bba_orbit_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_bba_orbit_y


% --- Executes on button press in cb_bba_dfs_y.
function cb_bba_dfs_y_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.do_apply_dfs_y = get(hObject,'Value');
% hObject    handle to cb_bba_dfs_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_bba_dfs_y


% --- Executes on button press in cb_bba_wfs_y.
function cb_bba_wfs_y_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.do_apply_wfs_y = get(hObject,'Value');
% hObject    handle to cb_bba_wfs_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_bba_wfs_y


% --- Executes on button press in cb_bba_ff_y.
function cb_bba_ff_y_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.do_apply_ff_y = get(hObject,'Value');
% hObject    handle to cb_bba_ff_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_bba_ff_y



function edit_ffy_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.wgt_ff_y = str2num(get(hObject,'String'));
  set(handles.edit_ffy, 'String', BBA_DATA.wgt_ff_y);
% hObject    handle to edit_ffy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_ffy as text
%        str2double(get(hObject,'String')) returns contents of edit_ffy as a double


% --- Executes during object creation, after setting all properties.
function edit_ffy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_ffy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pb_bpms_select.
function pb_bpms_select_Callback(hObject, eventdata, handles)
global BBA_DATA
selection = get(handles.list_bpm,'Value');
if length(selection) > 0
  BBA_DisableGUI(handles);
  ret_bpms = BBA_Choose_function_of_bpms();
  if ret_bpms ~= -1
    for i=selection
      BBA_DATA.Bpms(i).mask = ret_bpms;
    end
  end
  set(handles.list_bpm, 'Value', []);
  set(handles.list_bpm, 'String', BBA_GetBpmsList());
  BBA_EnableGUI(handles);
end

% hObject    handle to pb_bpms_select (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pb_corrs_select.
function pb_corrs_select_Callback(hObject, eventdata, handles)
global BBA_DATA
selection = get(handles.list_corr,'Value');
if length(selection) > 0
  BBA_DisableGUI(handles);
  ret_corrs = BBA_Choose_function_of_corrs();
  if ret_corrs ~= -1
    for i=selection
      BBA_DATA.Correctors(i).mask = ret_corrs;
    end
  end
  set(handles.list_corr, 'Value', []);
  set(handles.list_corr, 'String', BBA_GetCorrectorsList());
  BBA_EnableGUI(handles);
end
% hObject    handle to pb_corrs_select (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit_dir_Callback(hObject, eventdata, handles)
% hObject    handle to edit_dir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_dir as text
%        str2double(get(hObject,'String')) returns contents of edit_dir as a double


% --- Executes during object creation, after setting all properties.
function edit_dir_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_dir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_orbitx_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.wgt_orb_x = str2num(get(hObject,'String'));
  set(handles.edit_orbitx, 'String', BBA_DATA.wgt_orb_x);

% hObject    handle to edit_orbitx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_orbitx as text
%        str2double(get(hObject,'String')) returns contents of edit_orbitx as a double


% --- Executes during object creation, after setting all properties.
function edit_orbitx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_orbitx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_orbity_Callback(hObject, eventdata, handles)
global BBA_DATA
  BBA_DATA.wgt_orb_y = str2num(get(hObject,'String'));
  set(handles.edit_orbity, 'String', BBA_DATA.wgt_orb_y);
% hObject    handle to edit_orbity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_orbity as text
%        str2double(get(hObject,'String')) returns contents of edit_orbity as a double


% --- Executes during object creation, after setting all properties.
function edit_orbity_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_orbity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes when user attempts to create BBA_GUI.
function BBA_GUI_CreateFcn(hObject, eventdata, handles)

% --- Executes when user attempts to close BBA_GUI.
function BBA_GUI_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to BBA_GUI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global BBA_DATA BBA_PLOTS;
    save([BBA_DATA.Directory '/bba.mat'],'BBA_DATA', 'BBA_PLOTS', '-mat');
    fidx = fopen('xcorr.txt', 'w');
    fidy = fopen('ycorr.txt', 'w');
    corrs = BBA_DATA.Correctors;
    for i=1:length(corrs)
      corr_str = corrs(i).name;
      if fnmatch('ZH*', { corr_str })
        fprintf(fidx, '%s %s\n', corr_str, dec2bin(corrs(i).mask));
      elseif fnmatch('ZV*', { corr_str })
        fprintf(fidy, '%s %s\n', corr_str, dec2bin(corrs(i).mask));
      end
    end
    fclose(fidx);
    fclose(fidy);
    fidb = fopen('bpms.txt', 'w');
    bpms = BBA_DATA.Bpms;
    for i=1:length(bpms)
      bpm_str = bpms(i).name;
      if bpm_str(1) == 'B'
        fprintf(fidx, '%s %s\n', bpm_str, dec2bin(bpms(i).mask));
      end
    end
    fclose(fidb);
    chdir('../..');
% Hint: delete(hObject) closes the figure
%if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
%    uiresume(hObject);
%else
    % The GUI is no longer waiting, just close it
    delete(hObject);
%end


% --- Executes on button press in pb_showR.
function pb_showR_Callback(hObject, eventdata, handles)
global BBA_DATA plotR
  [Rx, Ry] = BBA_GetResponseMatrix;

  if isempty(plotR)
    plotR = figure;
  end
  figure(plotR);

  if BBA_DATA.do_include_coupled_terms

    subplot(1,1,1);
    surf([ Rx ; Ry ]);
    xlabel('[ X correctors ; Y correctors ]');
    ylabel('[ X bpms ; Y bpms ]');
    zlabel('R');
 
  else

    subplot(1,2,1);
    surf(Rx);
    xlabel('X correctors');
    ylabel('X bpms');
    zlabel('R_{xx}');
 
    subplot(1,2,2);
    surf(Ry);
    xlabel('Y correctors');
    ylabel('Y bpms');
    zlabel('R_{yy}');
  end
