addpath([ '/Users/Nuria/Desktop/PlacetBDSIM_WFS/ATF2/Frameworks/BBA/ATF2_tools/' ]);
addpath([ '/Users/Nuria/Desktop/PlacetBDSIM_WFS/ATF2/Frameworks/BBA/Classes/' ]);
addpath([ '/Users/Nuria/Desktop/PlacetBDSIM_WFS/ATF2/Frameworks/BBA/tools/' ]);
addpath([ '/Users/Nuria/Desktop/PlacetBDSIM_WFS/ATF2/Frameworks/BBA/' ]);

Interface = InterfaceATF2(20);

S = GetMachine(Interface);

bpm_mask = [ 1:21, 26:49 57 ]; % BPM selection, extraction line BPMs plus a few final focus
bpm_mask = bpm_mask(1:24); % BPM selection in extraction line

ChangeBunchCharge(Interface,0.1); % typical nominal charge, absolute
ChangeBunchCharge(Interface,0.6*0.1); % typical off charge beam, absolute

ChangeEnergy(Interface,2); % off (lower) energy beam, absolute
ChangeEnergy(Interface,0); % nominal energy beam ; reset energy
ChangeEnergy(Interface,-2); % off (higher) energy beam, absolute

state = struct; state = atf2_get_bpms_correctors(state,1000);

save(sprintf('machine_state_%s.mat',strrep(datestr(clock, 30), 'T', '_')),'state');
