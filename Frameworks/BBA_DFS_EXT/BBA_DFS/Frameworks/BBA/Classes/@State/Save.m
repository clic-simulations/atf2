function Save(this, filename)
  if ischar(filename)
    state = this.state;
    save(filename, 'state', '-mat');
  else
    error('State::Save() requires a filename');
  end
end
