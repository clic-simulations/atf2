function C = GetCorrectors(this,AXIS)
  if nargin == 1 || (nargin == 2 && lower(AXIS) == 'x')
    CORRS = find(fnmatch('ZH*', this.state.mags.name));
  else
    CORRS = [];
  end
  if nargin == 1 || (nargin == 2 && lower(AXIS) == 'y')
    CORRS = [ CORRS ; find(fnmatch('ZV*', this.state.mags.name)) ];
  end
  if nargin == 1
    CORRS = sort(CORRS);
  end
  C = cell(length(CORRS),1);
  for i=1:length(CORRS)
    C{i} = this.state.mags.name{CORRS(i)};
  end
end
