function Z = GetBpmsZ(this, NAMES)
    if nargin == 1
      NAMES = GetBpms(this);
    end
    nNAMES = length(NAMES);
    Z = zeros(nNAMES,1);
    for i=1:nNAMES
      idx = find(strcmp(this.state.bpms.name, NAMES{i}));
      Z(i) = this.state.bpms.Z(idx);
    end
end
