function S = GetStrength(this,corr)
  fid = fopen(this.filename);
  C = textscan(fid,'%s%n');
  fclose(fid);
  indx = find(strcmp(C{1}, corr));
  if length(indx) == 0
    error(['CorrectorStrengths::GetStrength corrector ' corr ' not found!' ]);  
    S = 0.0;
  else
    S = C{2}(indx);
  end
end
