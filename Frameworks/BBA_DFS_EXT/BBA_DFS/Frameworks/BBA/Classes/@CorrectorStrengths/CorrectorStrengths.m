function C = CorrectorStrengths(CORR, directory)
  default_strength = 0.001;
  C = struct;
  C.default_strength = default_strength;
  C.filename = [directory '/corrector_strengths.txt'];
  if ~exist( C.filename, 'file')
    choice = questdlg('File: corrector_strengths.txt has not been found in the current folder','corrector_strengths.txt not found','Create a Default','Copy from elsewhere','Copy from elsewhere');
    import_error = 1;
    if strcmp(choice,'Copy from elsewhere')
      [FileName,PathName] = uigetfile('corrector_strenghts.txt','Select a corrector_strenghts.txt file');
      if FileName == 0
        msgbox('Ok, creating a default corrector_strengths.txt file','Info');
      else
        status = copyfile([PathName FileName], C.filename);
        if status
          disp(['File: corrector_strengths.txt succesfully imported from ' PathName]);
          import_error = 0;
        else
          errordlg('Error while importing a corrector_strengths.txt file, creating a default one','Error');
        end
      end
    end
    if import_error | strcmp(choice,'Create a Default')
      fid = fopen(C.filename,'w');
      for i=1:length(CORR)
        fprintf(fid, '%s %g\n', CORR{i}, default_strength); 
      end
      fclose(fid);
      disp('Created a new corrector_strengths.txt');
    end
  end
  C = class(C, 'CorrectorStrengths');
end

function C = CorrectorStrenghts(other)
  C = other
end
