function I = VaryCorrs(this, corr_names, corr_values)
% given a list of corrector names, and values, it sets the new
% absolute value of each corrector

disp('InterfaceATF2::VaryCorrs()');
tic;
state = struct;
state = atf2_get_correctors(state);

for i=1:numel(corr_names)
  index = find(strcmp(state.mags.name, corr_names(i)));
  if numel(index) == 0
    error(['corrector ''' corr_names{i} ''' not found']);
  end
  corr_values(i) = corr_values(i) + state.mags.BDES(index);
end

atf2_set_correctors(corr_names,corr_values);
WriteLog('InterfaceATF2::VaryCorrs()', toc);
I = this;

end
