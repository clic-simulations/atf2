function S = GetMachine(this)
    disp('InterfaceATF2::GetMachine()');

    this.state = atf2_get_bpms(this.state,this.nsamples);
    this.state = atf2_get_correctors(this.state);
    this.state.timestamp = now;

    S = State(this.state); % construct an object out of it
end
