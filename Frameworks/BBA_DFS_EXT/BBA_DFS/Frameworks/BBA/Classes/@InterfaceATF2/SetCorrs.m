function I = SetCorrs(this, corr_names, corr_values)
% given a list of corrector names, and values, it sets the new
% absolute value of each corrector

disp('InterfaceATF2::SetCorrs()');
tic;
atf2_set_correctors(corr_names,corr_values);
WriteLog('InterfaceATF2::SetCorrs()', toc);
I = this;

end
