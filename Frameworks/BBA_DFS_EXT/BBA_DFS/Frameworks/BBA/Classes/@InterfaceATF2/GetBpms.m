function S = GetBpms(this)
%% read bpms state and returns a state structure with it
    disp('InterfaceATF2::GetBpms()');
    tic;
    this.state = atf2_get_bpms(this.state,this.nsamples);
    this.state.timestamp = now;
    
    WriteLog('InterfaceATF2::GetBpms()', toc);
    S = State(this.state);

end
