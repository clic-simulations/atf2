function I = SetKlystronPhase(this, klys_name, phase)
  if nargin < 3
    error('You need to specify the the klystron name and the phase');
  end
  fid = fopen([this.dir '/placet_BBA_klystrons.txt'],'w');
  fprintf(fid, '%s %f', klys_name, phase);
  fclose(fid);
  I = this;
end
