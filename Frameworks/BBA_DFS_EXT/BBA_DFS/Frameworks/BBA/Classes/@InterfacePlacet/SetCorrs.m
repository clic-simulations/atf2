function I = SetCorrs(this, CORRS, vals)

for i=1:length(CORRS)
    index = find(strcmp(this.state.mags.name, CORRS{i}));
    this.state.mags.BACT(index) = vals(i);
    this.state.mags.BDES(index) = vals(i);
end

% write the correctors file to be used by placet
if ~exist(this.dir,'dir')
    mkdir(this.dir);
end

FileID = fopen(strcat(this.dir,'/placet_BBA_cors.txt'),'w')

CORRS = find(fnmatch('Z*', this.state.mags.name))

for i=1:length(CORRS)
    n = this.state.mags.name(CORRS(i));
    b = this.state.mags.BACT(CORRS(i));
    if b~=0.0
        fprintf(FileID,'%s %f\n',n{1},b);
    end
end
fclose(FileID);

I = this;
