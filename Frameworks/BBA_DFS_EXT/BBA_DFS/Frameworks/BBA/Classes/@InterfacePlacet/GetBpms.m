function S = GetBpms(this)

    disp('InterfacePlacet::GetBpms()');
    state = struct;
    % set up the directory
    if ~exist(this.dir,'dir')
        mkdir(this.dir);
    end

    % run placet (scrive il file coi bpms)
    run_placet = [ 'bash -l -c "cd ' this.dir '; placet -s ' this.script '" '];
    system(run_placet);

    disp(run_placet);
    
    % read BPMs file values
    %%%% copyfile(strcat(this.dir,'/placet_BBA_bpms.txt'), strcat(this.dir,'/../BBA/Classes/@InterfacePlacet/'))
    % copyfile(strcat(this.dir,'/placet_BBA_bpms.txt'), strcat(this.dir,'/tmp_placet'))
    FileID = fopen([ this.dir '/placet_BBA_bpms.txt' ],'r');
    C = textscan(FileID, '%s %f %f %f');
    fclose(FileID);
    disp(FileID)

    % write the data read from placet file in state.bpms.X / Y / TMIT
    this.state.bpms.X = zeros(length(this.state.bpms.name),1);
    this.state.bpms.Y = zeros(length(this.state.bpms.name),1);
    this.state.bpms.TMIT = zeros(length(this.state.bpms.name),1);

    for i=1:length(C{1})
        j = find(strcmp(this.state.bpms.name, C{1}(i)));
        this.state.bpms.X(j) = C{3}(i);
        this.state.bpms.Y(j) = C{4}(i);
        this.state.bpms.TMIT(i) = 1e9; %%%%%%%%%%%%%%%C{4}(i);
    end

    now_ = now;
    this.state.timestamp.start = now_;
    this.state.timestamp.done  = now_;
    S = State(this.state);
end
