function M = InterfacePlacet(dir, run_script)
    M = struct;
    %add m.charge
    if nargin == 0
        M.dir =([ '../BBA_flight_simulator'])
        M.script = 'run_simple.tcl';
    elseif nargin == 1
        M.dir = dir;
        M.script = 'run_simple.tcl';
    else
        M.dir = dir;
        M.script = run_script;
    end
    
    fid=fopen('placet_BBA_corr_nominal.txt','r'); % File needed to set the state structure with the ATF2 correctors names 
    A=textscan(fid,'%s %f %f %f %f');
    fclose(fid);
    fid=fopen('placet_BBA_bpms_nominal.txt','r');
    B=textscan(fid,'%s %f %f');
    fclose(fid);

    state = State();
    state.mags.name = A{1};
    state.bpms.name = B{1};
    state.bpms.Z = B{2};
    for index=1:size(state.mags.name,1)
        a=findstr(state.mags.name{index}, 'Z');
        if a==1
            state.mags.Z(index)=A{4}(index);
            state.mags.BACT(index) = 0.0;
            state.mags.BDES(index) = 0.0;
        end
    end

    % write the correctors file to be used by placet
    if ~exist(M.dir,'dir')
        mkdir(M.dir);
    end
    
    copyfile(strcat(M.dir,'/../BBA/Classes/@InterfacePlacet/placet_BBA_corr_nominal.txt'),M.dir); 
    fid = fopen(strcat(M.dir,'placet_BBA_corr_nominal.txt'),'r');
    if fid ~= -1
        CORRS = textscan(fid, '%s %f %f %f', 'CommentStyle', '%');
        CORRSn = CORRS{1};
        CORRSv = [ CORRS{2} ];
        fclose(fid);
        for i=1:length(CORRSn)
            index = find(strcmp(state.mags.name, CORRSn{i}));
            state.mags.BACT(index) = CORRSv(i);
            state.mags.BDES(index) = CORRSv(i);
        end
    end
    
    M.state = state;
    M = class(M, 'InterfacePlacet');

%    ChangeEnergy(M, 0);
    ChangeEnergy(M, -2);
    ChangeBunchCharge(M, 5*0.01);

end
