function S = GetBpmsCorrs(this)
  disp('InterfacePlacet::GetBpmsCorrs()');
  S1 = GetBpms(this);
  S2 = GetCorrs(this);
  state1 = GetStruct(S1);
  state2 = GetStruct(S2);
  state = struct;
  state.bpms = state1.bpms;
  state.mags = state2.mags;
  S = State(state);
end
