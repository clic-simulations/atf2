function BumpPositronTest(string)

global Asset_Particle
Asset_Particle = 'positron';
Interface = InterfaceAsset(100);
S0 = GetMachine(Interface);
state = GetStruct(S0);
save(sprintf('machine_status_%s.mat',strrep(datestr(clock, 30), 'T', '_')), 'state');



CORR_SWITCH_OFF = {
    'XCOR:LI02:138'
    'YCOR:LI02:139'
};

SetCorrs(Interface, CORR_SWITCH_OFF, zeros(size(CORR_SWITCH_OFF)));

load 'Response_positron.mat';

% wrong ASSET location
%BPMS = {Interface = InterfaceAsset(100);
% 'LI02:BPMS:201'
% 'LI02:BPMS:211'
%};

BPMS = {
 'LI02:BPMS:134'
 'LI02:BPMS:146'
};

MASK_BPMS = [ 
  find(strcmp(Response.BPMS, BPMS{1}))  
  find(strcmp(Response.BPMS, BPMS{2}))
];

Rx = Response.Rx(MASK_BPMS, :);
Ry = Response.Ry(MASK_BPMS, :);

Rx(:, Response.MASK_CORSY) = 0;
Ry(:, Response.MASK_CORSX) = 0;

%%%%

R = [ Rx ; Ry ];

[U,M,V] = svd(R);

sv_M = diag(M)
inv_M = zeros(size(M'));

n_sv = 4;
n_sv = min(n_sv, min(size(M)));

for i=1:n_sv
  inv_M(i,i) = 1/M(i,i);
end

inv_R = V * inv_M * U';

%%%%%%%%%%%%%%%

dX = [ -1.2:0.4:1.2];

offset = [
  dX zeros(size(dX))
  zeros(size(dX)) dX
];

% offset = [
%  zeros(size(dX))
%  dX
%];


Asset_Particle = 'positron';
% get reference orbit
S = GetMachine(Interface);
Orbit = GetOrbit(S, BPMS);
filename=sprintf('DATA_TEST_POS_%s_0_0.mat', string);
state = GetStruct(S);
save(filename, 'state');
fprintf('We would like to have a reference orbit, %g - %g\n', Orbit.TMIT(1), Orbit.TMIT(2));
fprintf('X = %g(%g) -- %g(%g);\n', Orbit.X(1), Orbit.stdX(1), Orbit.X(2), Orbit.stdX(2));
fprintf('Y = %g(%g) -- %g(%g);\n', Orbit.Y(1), Orbit.stdY(1), Orbit.Y(2), Orbit.stdY(2));



for i = 1:size(offset,2)

  offsetx = offset(1,i);
  offsety = offset(2,i);

  Corr = inv_R * [
    offsetx
    offsetx
    offsety
    offsety
  ];

  filename=sprintf('DATA_TEST_POS_%s_%g_%g.mat', string, offsetx, offsety);
  %%%%%%


  % get bumping orbit
  VaryCorrs(Interface, Response.CORS, Corr);
  S_kick = GetBpms(Interface);
  Orbit_kick = GetOrbit(S_kick, BPMS);
  DX = Orbit_kick.X - Orbit.X;
  DY = Orbit_kick.Y - Orbit.Y;
  fprintf('We would like to bump orbit with dX= %g and dY= %g, %g - %g :\n', offsetx, offsety, Orbit.TMIT(1), Orbit.TMIT(2));
  fprintf('X = %g(%g) -- %g(%g), dX = %g -- %g;\n', Orbit_kick.X(1), Orbit_kick.stdX(1), Orbit_kick.X(2), Orbit_kick.stdX(2), DX(1), DX(2));
  fprintf('Y = %g(%g) -- %g(%g), dY = %g -- %g;\n', Orbit_kick.Y(1), Orbit_kick.stdY(1), Orbit_kick.Y(2), Orbit_kick.stdY(2), DY(1), DY(2));
  state = GetStruct(S_kick);
  save(filename, 'state');

  VaryCorrs(Interface, Response.CORS, -Corr);
end


Restore(Interface, S0, CORR_SWITCH_OFF);
