% Add_path % Added paths to needed directories

fid = fopen('xcorr.txt');
CORRSx = textscan(fid, '%s', 'CommentStyle', '%');
CORRSx = CORRSx{1};
fclose(fid);
fid = fopen('ycorr.txt');
CORRSy = textscan(fid, '%s', 'CommentStyle', '%');
CORRSy = CORRSy{1};
fclose(fid);

% get the list of all data files in the current directory
all_files = getAllFiles('.');
data_files = all_files(fnmatch('./DATA*.mat', all_files));

load(data_files{1});
S = State(state);

for i=1:length(data_files)
    file_name = data_files{i};
    file_name(length(file_name)-8) = 'X';
    data_files{i} = file_name;
end

% 
data_files = unique(data_files);
nFiles     = length(data_files);

% setup the list of correctors
CORRSD = GetCorrectorsData(S, vertcat(CORRSx, CORRSy));
CORRS = { CORRSD(:).name };
CORRSZ = [ CORRSD(:).Z ];

% only consider all bpms that follow the first corrector
BPMS = GetBpms(S);
BPMSZ  = GetBpmsZ(S, BPMS);
BPMS   = BPMS(BPMSZ > CORRSZ(1));
BPMSZ  = BPMSZ(BPMSZ > CORRSZ(1));
BPMSX  = BPMS;
BPMSXZ = GetBpmsZ(S, BPMSX);
BPMSX  = BPMSX(BPMSXZ > CORRSZ(1));
BPMSY  = BPMS;
BPMSYZ = GetBpmsZ(S, BPMSY);
BPMSY  = BPMSY(BPMSYZ > CORRSZ(1));

%
nCorrs    = length(CORRS);
nBpms     = length(BPMS);

% here we go

Bx = zeros(nBpms,  nFiles);
By = zeros(nBpms,  nFiles);
C  = zeros(nCorrs, nFiles);

% read all files
for i=1:nFiles
    file_name = data_files{i};
    for c='pm'
        file_name(length(file_name)-8) = c;
        if ~exist(file_name)
            error('file ''%s'' does not exist!', file_name);
        end
        S = State(file_name);
        Orbit = GetOrbit(S, BPMS);
        Corrs = GetCorrectorsData(S, CORRS);
        if c == 'p'
            Bx(:,i) = Orbit.X;
            By(:,i) = Orbit.Y;
            C (:,i) = [ Corrs(:).BACT ]';
        else
            Bx(:,i) = Bx(:,i) - Orbit.X;
            By(:,i) = By(:,i) - Orbit.Y;
            C (:,i) = C (:,i) - [ Corrs(:).BACT ]';
        end
    end
    if isnan(C(:,i))
        error(['datafile `' data_files{i} '` contains nan!'])
    end
end

% compute the response matrices
Rx = Bx / C;
Ry = By / C;

% zero the half lower part of the matrix
for j=1:nCorrs
  Rx(find(BPMSZ < CORRSZ(j)), j) = 0;
  Ry(find(BPMSZ < CORRSZ(j)), j) = 0;
end

% save the result
Response.CORS  = CORRS;
Response.BPMS  = BPMS;
Response.CORSZ = CORRSZ;
Response.BPMSZ = BPMSZ;
Response.Rx    = Rx(1:nBpms,1:nCorrs);
Response.Ry    = Ry(1:nBpms,1:nCorrs);

CX = find(fnmatch('ZH*', Response.CORS));
CY = find(fnmatch('ZV*', Response.CORS));
BX = [];
BY = [];
for i=1:length(BPMSX); BX = [ BX find(fnmatch(BPMSX{i}, Response.BPMS)) ]; end;
for i=1:length(BPMSY); BY = [ BY find(fnmatch(BPMSY{i}, Response.BPMS)) ]; end;
Response.MASK_BPMSX = BX;
Response.MASK_BPMSY = BY;
Response.MASK_CORSX = CX;
Response.MASK_CORSY = CY;
save(sprintf('Response_pinv_%d.mat', nFiles), 'Response', '-mat');
