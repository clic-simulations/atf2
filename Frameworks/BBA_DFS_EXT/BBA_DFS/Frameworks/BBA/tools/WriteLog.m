function WriteLog(entry,t)
  fid = fopen('log.txt','a');
  if nargin == 1
    fprintf(fid,'%s %s\n', strrep(datestr(clock, 30), 'T', '_'), entry);
  else 
    fprintf(fid,'%s %.2f %s\n', strrep(datestr(clock, 30), 'T', '_'), t, entry);
  end
  fclose(fid);
end
