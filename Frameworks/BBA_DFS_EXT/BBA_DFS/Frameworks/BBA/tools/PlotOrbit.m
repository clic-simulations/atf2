function PlotOrbit(Orbit0, Orbit1, Orbit2, Title)
%figure(2);
if nargin < 4
  Title = '';
end

if nargin >= 3

subplot(3,3,1);
% Create plot
plot(Orbit0.Z,Orbit0.X);
xlabel('s [m]');
ylabel('X [mm]');
legend('filtered');
%myaxis = axis;
%axis([0 2500 -1 1]);

subplot(3,3,4);
% Create plot
plot(Orbit0.Z,Orbit0.Y);
xlabel('s [m]');
ylabel('Y [mm]');
legend('filtered');
%myaxis = axis;
%axis([0 2500 -1 1]);

subplot(3,3,7);
% Create plot
plot(Orbit0.Z,Orbit0.TMIT/1e9);
xlabel('s [m]');
ylabel('TMIT [# 1e9]');
legend('Orbit0');
%myaxis = axis;
%axis([0 2500 0 25 ]);

subplot(3,3,2);
% Create plot
plot(Orbit1.Z,Orbit1.X);
title(Title);
xlabel('s [m]');
ylabel('X [mm]');
legend('filtered');
%myaxis = axis;
%axis([0 2500 -1 1]);

subplot(3,3,5);
% Create plot
plot(Orbit1.Z,Orbit1.Y);
xlabel('s [m]');
ylabel('Y [mm]');
legend('filtered');
%myaxis = axis;
%axis([0 2500 -1 1]);

subplot(3,3,8);
% Create plot
plot(Orbit1.Z,Orbit1.TMIT/1e9);
xlabel('s [m]');
ylabel('TMIT [# 1e9]');
legend('Orbit1');
%myaxis = axis;
%axis([0 2500 0 25 ]);

subplot(3,3,3);
% Create plot
plot(Orbit2.Z,Orbit2.X);
xlabel('s [m]');
ylabel('X [mm]');
legend('filtered');
%myaxis = axis;
%axis([0 2500 -1 1]);

subplot(3,3,6);
% Create plot
plot(Orbit2.Z,Orbit2.Y);
xlabel('s [m]');
ylabel('Y [mm]');
legend('filtered');
%myaxis = axis;
%axis([0 2500 -1 1]);

subplot(3,3,9);
% Create plot
plot(Orbit2.Z,Orbit2.TMIT/1e9);
xlabel('s [m]');
ylabel('TMIT [# 1e9]');
legend('Difference');
%myaxis = axis;
%axis([0 2500 0 25 ]);

elseif nargin == 1

subplot(3,1,1);
% Create plot
plot(Orbit0.Z,Orbit0.X);
xlabel('s [m]');
ylabel('X [mm]');
legend('filtered');
%myaxis = axis;
%axis([0 2500 -1 1]);

subplot(3,1,2);
% Create plot
plot(Orbit0.Z,Orbit0.Y);
xlabel('s [m]');
ylabel('Y [mm]');
legend('filtered');
%myaxis = axis;
%axis([0 2500 -1 1]);

subplot(3,1,3);
% Create plot
plot(Orbit0.Z,Orbit0.TMIT);
xlabel('s [m]');
ylabel('TMIT [#]');
legend('filtered');
%myaxis = axis;
%axis([0 2500 0 25 ]);

end
