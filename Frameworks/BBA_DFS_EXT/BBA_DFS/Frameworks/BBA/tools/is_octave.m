function isOctave = is_octave()
    isOctave = exist('OCTAVE_VERSION') ~= 0;
end
