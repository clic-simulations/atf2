figure(1)
clf
plot(Corrx);
hold on
plot(Corry, 'r-');
title('Corrx and Corry');
xlabel('Corrector number');
ylabel('Correctors strength');

figure(2);
clf
plot(DispE.X);
title('Disp X and Target X');
hold on
plot(TargetD.X, 'r-');
xlabel('BPM number');
ylabel('Dispersion');
legend

figure(3);
clf
plot(DispE.Y);
hold on
plot(zeros(size(DispE.Y)), 'r-');
xlabel('BPM number');
ylabel('Dispersion');
title('Disp Y and Target Y');

figure(4)
plotyy(1:iteration, norm_plot(:,1), 1:iteration, norm_plot(:,2));
xlabel('Iteration');
ylabel('Dispersion norm X and Y');

figure(5)
plotyy(1:iteration, normb_plot(:,1), 1:iteration, normb_plot(:,2));
xlabel('Iteration');
ylabel('Orbit norm X and Y');
