function [nan_indicator] = check_for_NaN(Q,beta,phase,factor_beta,goodbpm,dQ,dbeta,dphase,dfactor_beta)
  nan_indicator = sum(isnan([Q, dQ]));
  nan_indicator = nan_indicator + sum(sum(isnan([beta(goodbpm) dbeta(goodbpm)])));
  nan_indicator = nan_indicator + sum(sum(isnan([phase(goodbpm) dphase(goodbpm)])));
end
