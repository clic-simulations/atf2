% --- Get resolution of BPMs
% ===========================
close all
inds=40:160;
bpms={'MQF1X' 'MQD2X' 'MQF3X' 'MQF4X' 'MQD5X' 'MQF6X' 'MQF7X' 'MQD8X' 'MQF9X' 'MQF13X' 'MQD14X' 'MQF15X' 'MFB1FF'};

for ibpm=1:length(bpms)
  bpme(ibpm)=findcells(BEAMLINE,'Name',bpms{ibpm});
  bpmi(ibpm)=findcells(INSTR,'Index',bpme(ibpm));
end

% plot raw data
xdata=bpmdata(:,bpmi.*3-1);
ydata=bpmdata(:,bpmi.*3);
figure
subplot(2,1,1),plot(xdata)
subplot(2,1,2),plot(ydata)

% get svd modes
[Ux,Sx,Vx]=svd(xdata);
[Uy,Sy,Vy]=svd(ydata);

% Plot mode amplitudes
figure
subplot(2,1,1),semilogy(diag(Sx))
subplot(2,1,2),semilogy(diag(Sy))

% subtract correlated jitter modes
for im=1:5
  Sx(1,1)=0; Sy(1,1)=0;
  xdata_sub=Ux*Sx*Vx';
  ydata_sub=Uy*Sy*Vy';
  [Ux,Sx,Vx]=svd(xdata_sub);
  [Uy,Sy,Vy]=svd(ydata_sub);
end
% xdata_sub=Ux*Sx*Vx';
% ydata_sub=Uy*Sy*Vy';

% plot jitter subtracted data
figure
subplot(2,1,1),plot(xdata_sub)
subplot(2,1,2),plot(ydata_sub)

% Get RMS values
disp('X:')
for ibpm=1:length(bpms)
  sx(ibpm)=std(xdata_sub(inds,ibpm));
  fprintf('%s: %g um\n',bpms{ibpm},sx(ibpm)*1e6)
end
disp('Y:')
for ibpm=1:length(bpms)
  sy(ibpm)=std(ydata_sub(inds,ibpm));
  fprintf('%s: %g um\n',bpms{ibpm},sy(ibpm)*1e6)
end

% Plot resolutions
figure
subplot(2,1,1), plot(sx)
subplot(2,1,2), plot(sy)