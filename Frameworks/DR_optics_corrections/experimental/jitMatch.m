function [bsx bsy betax alphax bmagx betay alphay bmagy]=jitMatch(im1,im2,bpmdata,emitx,emity)
global BEAMLINE FL INSTR

% Get list of BPMs
[stat data]=FlBpmToolFn('GetData');
instEle=findcells(BEAMLINE,'Class','MONI',im1,im2);
inst=[];instI=[];
for ii=1:length(instEle)
  iInst=findcells(INSTR,'Index',instEle(ii));
  if ~isempty(iInst) && ismember(iInst,find(data.global.use)) && ismember(iInst,find(data.global.hw))
    inst(end+1)=iInst;
    instI(end+1)=instEle(ii);
  end
end

% Fit jitter data to get flat jitter distribution
Tmodel=FL.SimModel.Twiss;
getDataBeamSize([Tmodel.betax(instI(1)) Tmodel.alphax(instI(1)) Tmodel.betay(instI(1)) Tmodel.alphay(instI(1))],...
  inst,instI,emitx,emity,bpmdata);
bsdata=getDataBeamSize('GetData');
bsx=bsdata{1}; bsy=bsdata{2};
figure;plot(instI,bsx,':',instI,bsy,':'); hold on
xmin=lsqnonlin(@(x) getDataBeamSize(x,inst,instI,emitx,emity,bpmdata),...
  [Tmodel.betax(instI(1)) Tmodel.alphax(instI(1)) Tmodel.betay(instI(1)) Tmodel.alphay(instI(1))],...
  [-200 -50 -200 -50],[200 50 200 50],optimset('Display','iter','TolFun',1e-8));
getDataBeamSize(xmin,inst,instI,emitx,emity,bpmdata);
bsdata=getDataBeamSize('GetData');
bsx=bsdata{1}; bsy=bsdata{2};
plot(instI,bsx,instI,bsy); plot(instI,sqrt(FL.SimModel.Twiss.betay(instI)))
betax=xmin(1); alphax=xmin(2); betay=xmin(3); alphay=xmin(4);
bx=Tmodel.betax(instI(1));
ax=Tmodel.alphax(instI(1));
by=Tmodel.betay(instI(1));
ay=Tmodel.alphay(instI(1));
bmagx=0.5*((betax/bx)+(bx/betax))+0.5*((ax*sqrt(betax/bx)-alphax*sqrt(bx/betax))^2);
bmagy=0.5*((betay/by)+(by/betay))+0.5*((ay*sqrt(betay/by)-alphay*sqrt(by/betay))^2);

% Display results
disp('==================================')
fprintf('Match data at %s :\n',BEAMLINE{instI(1)}.Name)
disp('==================================')
fprintf('alpha_x = %g (Design: %g)\n',alphax,Tmodel.alphax(instI(1)))
fprintf('beta_x = %g (Design: %g)\n',betax,Tmodel.betax(instI(1)))
fprintf('BMAG_x = %g (Design: 1)\n',bmagx)
fprintf('alpha_y = %g (Design: %g)\n',alphay,Tmodel.alphay(instI(1)))
fprintf('beta_y = %g (Design: %g)\n',betay,Tmodel.betay(instI(1)))
fprintf('BMAG_y = %g (Design: 1)\n',bmagy)
disp('==================================')
% Get beam size jitter normalised by model sigmas
function out=getDataBeamSize(x,inst,instEle,ex,ey,bpmdata)
% x: [betax alphax betay alphay]
persistent bsx bsy
global FL
if isequal(x,'GetData')
  out={bsx bsy};
  return
end
jx=bpmdata{1}(4,inst);
jy=bpmdata{1}(5,inst);
Tmodel=FL.SimModel.Twiss;
Tx.S=Tmodel.S(instEle(1));
Tx.E=Tmodel.P(instEle(1));
Tx.beta=x(1);
Tx.alpha=x(2);
Tx.eta=0;
Tx.etap=0;
Tx.nu=0;
Ty.beta=x(3);
Ty.alpha=x(4);
Ty.eta=0;
Ty.etap=0;
Ty.nu=0;
[stat T]=GetTwiss(instEle(1),instEle(end),Tx,Ty);
betax=T.betax(instEle-instEle(1)+1);
betay=T.betay(instEle-instEle(1)+1);
bsx=jx./sqrt(betax.*ex);
bsy=jy./sqrt(betay.*ey);
out=[bsx-mean(bsx) bsy-mean(bsy)].^2;
