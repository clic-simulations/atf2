
% define fully qualified names for external data files that could be loaded
% into the Flight Simulator (via the FlLoadExternal function)

% ATF control system SET file
% i.e. SETfile='/home/mdw/ATF2lattice/set09oct27_0734.dat';
SETfile='~/home/mdw/2012/set12oct24_1139.dat';

% MAD Twiss tape-file [DR+EXT+FF]
% i.e. MADfile='/home/mdw/ATF2lattice/ATF2_twiss_BX1BY1.tape';
MADfile='';
