% Test file to test the functionallity of try and catch statements 
%
% Juergen Pfingstner
% 23th of November 2013

try  
disp('Let us try');
exception_NaN = MException('MyComponent:nan', 'The result of the calculation has been Nan.');
throw(exception_NaN);
%warning('Throwing a warning'); % Is not caught by catch
%error('Throwing an error'); % That works
catch Excep
Excep.message
disp('That did not work');
end

disp('Final execution');
