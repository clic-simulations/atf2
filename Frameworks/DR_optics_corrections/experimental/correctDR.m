function correctDR(filename)

global BEAMLINE PS FL;

%Reading of the changeparameters.tfs file
fid=fopen(filename,'r');
if fid==-1
  fprintf('error openning %s\n',filename);
  return
end
C=textscan(fid,'%s%f','HeaderLines',5);
fclose(fid);
if(length(C)~=2)
  fprintf('error: bad input file, should have 5 header lines, then 2 columns\n');
  return
end
if(length(C{1})~=length(C{2}))
  fprintf('error: different number of names and values\n');
  return
end
name=C{1};
deltaKL=C{2};
nmagnet=length(deltaKL);

%Convert in Floodland units and set PS().SetPt
E0=FL.SimModel.Initial.Momentum;
Cb=1e9/299792458; % rigidity constant (T-m/GeV)
brho=Cb*E0; % rigidity (T-m)
for i=1:nmagnet
  mag_name_mad=name{i}(3:end);
  index__=strfind(mag_name_mad,'_');
  mag_name_fs=mag_name_mad([1:index__-1 index__+1:end]);
  indexes_mag=findcells(BEAMLINE,'Name',mag_name_fs);
  PS_index=BEAMLINE{indexes_mag(1)}.PS;
  pvmain=FL.HwInfo.PS(PS_index).pvname{1}{1};
  pvtrim=FL.HwInfo.PS(PS_index).pvname{1}{2};
  nt_ratio= FL.HwInfo.PS(PS_index).nt_ratio;
  conv= FL.HwInfo.PS(PS_index).conv;
  conv=[-conv(:,end:-1:2) conv];
  Itrim0=lcaGet(pvtrim);
  if(~isfinite(Itrim0))
    fprintf('lcaget failed for %s\n',pvtrim)
    continue
  end
  I0=lcaGet(pvmain)+Itrim0*nt_ratio;
  GL0=interp1(conv(1,:),conv(2,:),I0);
  GL=GL0+deltaKL(i)*brho;
  I=interp1(conv(2,:),conv(1,:),GL);
  if(~isfinite(I))
    fprintf('out of range of convestion for %s\n',mag_name_fs)
    continue
  end
  dItrim=(I0-I)/nt_ratio;
  if (dItrim+Itrim0)>4.5
    lcaPut(pvtrim,4.5);
    fprintf('lcaPut(%s, %f instead of %f\n',pvtrim,0.45,dItrim+Itrim0);
  elseif ((dItrim+Itrim0)<-4.5)
    lcaPut(pvtrim,-4.5);
    fprintf('lcaPut(%s, %f instead of %f\n',pvtrim,-0.45,dItrim+Itrim0);
  else
    lcaPut(pvtrim,dItrim+Itrim0);
  end
end