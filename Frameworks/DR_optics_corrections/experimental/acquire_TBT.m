function [xread,yread,tmit]=acquire_TBT(start_turn,nturn,nshot)

if ~exist('start_turn','var') || isempty(start_turn) || start_turn<1
  % Select start turn
  start_turn = 1;
end
if ~exist('nturn','var') || isempty(nturn) || nturn<1
  % Number of turns to acquire
  nturn = 1024;
end
if ~exist('nshot','var') || isempty(nshot) || nshot<1
  % Number of beam  triggers to acquire
  nshot = 1; 
end

%as we remove 2 readings later, we acquire 2 more now
nturn=nturn+2;
if(nturn>1024)
  nturn=1024;
end
  
% time to wait for trigger
triger_wait = 1.5;
% PV names
pvTurn = 'bpm:crate1.TURN';
pvSnap = 'bpm:crate1.SNAP';

% Init arrays
xread = zeros(nshot,nturn-1,96);
yread = zeros(nshot,nturn-1,96);
tmit = zeros(nshot,nturn-1,96);

% Read start turn so we can reset then set study turn
Oturn = lcaGet(pvTurn);

for is = 1:nshot
  % set turn value & take snapshot
  lcaPut(pvTurn,start_turn);
  lcaPut(pvSnap,1);
%   if (nshot>1)
%     disp(['Shot #' num2str(is)])
%   end
  pause(triger_wait);
  % be nice while we readback our snapshot
  if start_turn>1 
    lcaPut(pvTurn,Oturn);
  end
  % read all bpms
  for ii=1:96
    if ii<10
        bstr = ['0' num2str(ii)];
    else
        bstr = num2str(ii);
    end
    pvBpm = ['bpm' bstr ':snap1.'];
    lcaPut([pvBpm 'PROC'],9);
    % read intensity array
    t = lcaGet([pvBpm 'WINT'],nturn);
    % read horz pos array
    x = lcaGet([pvBpm 'WPSH'],nturn);
    % read vert pos array
    y = lcaGet([pvBpm 'WPSV'],nturn);
    % shit reading by 1 for BPM after injection kicker
    if ii<21
      tmit(is,:,ii) = t(1:end-1);
      xread(is,:,ii) = x(1:end-1);
      yread(is,:,ii) = y(1:end-1);
    else
      tmit(is,:,ii) = t(2:end);
      xread(is,:,ii) = x(2:end);
      yread(is,:,ii) = y(2:end);          
    end
  end % loop over bpms
end % loop over shots
xread(xread==999999)=NaN;
yread(yread==999999)=NaN;
tmit(tmit==999999)=NaN;
xread=xread(:,2:end,:);
yread=yread(:,2:end,:);
tmit=tmit(:,2:end,:);

