mag='QM16FF';
spoints=linspace(-50,50,20);
nbpm=20;

%
iele=findcells(BEAMLINE,'Name',mag);iele=iele(1);
igir=BEAMLINE{iele}.Girder;
orig=GIRDER{igir}.MoverPos;
bpmdataSave=cell(2,length(spoints));
pl='xy';
for ipl=1:2
  for iscan=1:length(spoints)
    fprintf('%s (%c) scan %d of %d\n',mag,pl(ipl),iscan,length(spoints))
    GIRDER{igir}.MoverSetPt(ipl)=orig(ipl)+spoints(iscan);
    FlHwUpdate('wait',nbpm);
    [stat bpmdata]=FlHwUpdate('readbuffer',nbpm);
    bpmdataSave{ipl,iscan}=bpmdata;
  end
end

save(sprintf('userData/bpmResData_%s',mag),'bpmdataSave')