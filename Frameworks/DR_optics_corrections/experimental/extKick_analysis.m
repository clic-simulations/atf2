global BEAMLINE;
global INSTR;

%elem_name={'IEX','BS1XB','BS2XB','BS3XB','QS1X','BH1XB'};
elem_name={'BS1XB','BS2XB','BS3XB'};%,'QS1X','BH1XB'};
nelem=length(elem_name);
elem=zeros(1,nelem);
for i=1:nelem
  elems=findcells(BEAMLINE,'Name',elem_name{i});
  elem(i)=elems(length(elems));
end
bpm=sort([findcells(BEAMLINE,'Name','MQ*X') findcells(BEAMLINE,'Name','MQ*FF')]);
nbpm=length(bpm);
bpm_name='';
for i=1:nbpm
  bpm_s(i)=BEAMLINE{bpm(i)}.S;
  bpm_inst(i)=findcells(INSTR,'Index',bpm(i));
  bpm_name=char(bpm_name,BEAMLINE{bpm(i)}.Name);
end
extKick_0=load('userData/extKick_0.mat');
bpm_read_0=extKick_0.bpmdata{1}(1:2,bpm_inst);
dbpm_read_0=extKick_0.bpmdata{1}(4:5,bpm_inst);
extKick_m100=load('userData/extKick_m100.mat');
bpm_read_m100=extKick_m100.bpmdata{1}(1:2,bpm_inst);
dbpm_read_m100=extKick_m100.bpmdata{1}(4:5,bpm_inst);
extKick_m200=load('userData/extKick_m200.mat');
bpm_read_m200=extKick_m200.bpmdata{1}(1:2,bpm_inst);
dbpm_read_m200=extKick_m200.bpmdata{1}(4:5,bpm_inst);
extKick_m300=load('userData/extKick_m300.mat');
bpm_read_m300=extKick_m300.bpmdata{1}(1:2,bpm_inst);
dbpm_read_m300=extKick_m300.bpmdata{1}(4:5,bpm_inst);
extKick_m400=load('userData/extKick_m400.mat');
bpm_read_m400=extKick_m400.bpmdata{1}(1:2,bpm_inst);
dbpm_read_m400=extKick_m400.bpmdata{1}(4:5,bpm_inst);
extKick_m500=load('userData/extKick_m500.mat');
bpm_read_m500=extKick_m500.bpmdata{1}(1:2,bpm_inst);
dbpm_read_m500=extKick_m500.bpmdata{1}(4:5,bpm_inst);

for i=1:nbpm
  [qx,dqx]=noplot_polyfit([0 -100 -200 -300 -400 -500],...
              [bpm_read_0(1,i) bpm_read_m100(1,i) bpm_read_m200(1,i) bpm_read_m300(1,i) bpm_read_m400(1,i) bpm_read_m500(1,i)],...
              [dbpm_read_0(1,i) dbpm_read_m100(1,i) dbpm_read_m200(1,i) dbpm_read_m300(1,i) dbpm_read_m400(1,i) dbpm_read_m500(1,i)],1);
  Rx(i)=qx(2);
  dRx(i)=dqx(2);
  [qy,dqy]=noplot_polyfit([0 -100 -200 -300 -400 -500],...
              [bpm_read_0(2,i) bpm_read_m100(2,i) bpm_read_m200(2,i) bpm_read_m300(2,i) bpm_read_m400(2,i) bpm_read_m500(2,i)],...
              [dbpm_read_0(2,i) dbpm_read_m100(2,i) dbpm_read_m200(2,i) dbpm_read_m300(2,i) dbpm_read_m400(2,i) dbpm_read_m500(2,i)],1);
  Ry(i)=qy(2);
  dRy(i)=dqy(2);
end

TM=zeros(nelem,nbpm);
for i=1:nbpm
  for j=1:nelem
    if elem(j)<bpm(i)
      [s,r]=RmatAtoB(elem(j),bpm(i));
      TM(j,i)=r(3,4);
    end
  end
end

coeff=lscov(TM(:,~isnan(Ry))',Ry(~isnan(Ry))',dRy(~isnan(Ry))')

figure(1)
clf
subplot(2,1,1)
errorbar(bpm,Rx,dRx)
subplot(2,1,2)
errorbar(bpm,Ry,dRy)
hold on
plot(bpm,TM'*coeff,'r')
hold off
