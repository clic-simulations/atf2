function [betax_XSR,dbetax_XSR,betay_XSR,dbetay_XSR]=fitbeta_XSR(bpms,betax,dbetax,betay,dbetay)

global BEAMLINE
global betax_model
global betay_model

if ~exist('bpms','var') || isempty(bpms)
  bpms=64:68;
end

if (~exist('betax','var') || ~exist('betay','var'))
  fprintf(1,'Aquire data ...\n');
  [x,y,]=acquire_TBT(10,512,10);
  fprintf(1,'done\n');
end
if ~exist('betax','var') 
  [~,betax,~,~,~,~,dbetax,~,~]=analyse_TBT(x,4e-2,betax_model);
end
if ~exist('betay','var') 
  [~,betay,~,~,~,~,dbetay,~,~]=analyse_TBT(y,4e-2,betay_model);
end

betax_bpm=betax(bpms);
dbetax_bpm=dbetax(bpms);
betay_bpm=betay(bpms);
dbetay_bpm=dbetay(bpms);

index_bpm=[];
for i=bpms
  index_bpm(end+1)=findcells(BEAMLINE,'Name',sprintf('MB%iR',i));
end

% betax_quad=[ 0.944 2.713 6.972 4.226 15.196];
% dbetax_quad=betax_quad.*1e-1;
% betay_quad=[ 2.318 0.651 1.970 11.971 1.429];
% dbetay_quad=betay_quad.*1e-1;
% 
% index_quad=[];
% for i=3:7
%   quads=findcells(BEAMLINE,'Name',sprintf('QM%iR2',i));
%   index_quad(end+1)=quads(2);
% end

XSR=findcells(BEAMLINE,'Name','XSR');
%[twissx0,dtwissx0,twissy0,dtwissy0]=fitbeta([betax_bpm betax_quad],[dbetax_bpm dbetax_quad],[betay_bpm betay_quad],[dbetay_bpm dbetay_quad],[index_bpm index_quad],XSR);
[twissx0,dtwissx0,twissy0,dtwissy0]=fitbeta(betax_bpm,dbetax_bpm,betay_bpm,dbetay_bpm,index_bpm,XSR);
betax_XSR=twissx0(1);
betay_XSR=twissy0(1);
dbetax_XSR=dtwissx0(1);
dbetay_XSR=dtwissy0(1);
