function archive_plot

t0=datenum('Dec-18-2012 01:00:00');
t1=datenum('Dec-18-2012 09:00:00');
%t0=datenum('Dec-14-2012 22:00:00');
% t1=now;
I_cut=300;
reconstruct_point_name='MB2X';
refBpm={'MB2X:X';'MB2X:Y'};

addpath('~/ATF2/control-software/epics-3.14.8/extensions/src/ChannelArchiver/Matlab');
addpath('~/ATF2/control-software/epics-3.14.8/extensions/src/ChannelArchiver/Matlab/util');
url='http://localhost/archive/cgi/ArchiveDataServer.cgi';
key=1;
global is_matlab PS BEAMLINE INSTR FL
is_matlab=1;
% ml_arch_info(url);

nread=round(3600*24*(t1-t0));
reconstruct_point=findcells(BEAMLINE,'Name',reconstruct_point_name);
if isempty(reconstruct_point)
  fprintf('%s not found\n',reconstruct_point_name);
  return
end

%get BPMs info
MQF1X=findcells(BEAMLINE,'Name','MQF1X');
MQD2X=findcells(BEAMLINE,'Name','MQD2X');
MQF3X=findcells(BEAMLINE,'Name','MQF3X');
MQF4X=findcells(BEAMLINE,'Name','MQF4X');
bpm=[MQF1X MQD2X MQF3X MQF4X];
nbpm=length(bpm);
bpm_instr=zeros(nbpm,1);
for i=1:nbpm
  bpm_instr(i)=findcells(INSTR,'Index',bpm(i));
  pv=FL.HwInfo.INSTR(bpm_instr(i)).pvname{1};
  bpmx_pv{i}= pv{1}(1:strfind(pv{1},'.VAL')-1);
  bpmy_pv{i}= pv{2}(1:strfind(pv{2},'.VAL')-1);
  bpm_name{i}=BEAMLINE{bpm(i)}.Name;
end

%get correctors info
ZV1X=findcells(BEAMLINE,'Name','ZV1X');
ZV2X=findcells(BEAMLINE,'Name','ZV2X');
ZH1X=findcells(BEAMLINE,'Name','ZH1X');
ZV3X=findcells(BEAMLINE,'Name','ZV3X');
cor=[ZV1X ZV2X ZH1X ZV3X];
cor=cor(cor>=reconstruct_point);
ncor=length(cor);
cor_ps=zeros(ncor,1);
for i=1:ncor
  cor_ps(i)=BEAMLINE{cor(i)}.PS;
  pv=FL.HwInfo.PS(cor_ps(i)).pvname{1};
  cor_pv{i}=pv{1};
  cor_conv(i)=FL.HwInfo.PS(cor_ps(i)).conv;
  cor_class{i}=BEAMLINE{cor(i)}.Class;
  cor_name{i}=BEAMLINE{cor(i)}.Name;
end

%get BPM and intensity readings
[times,read]=ml_arch_get_more(url, key,[{'REFC1:amp'}; bpmx_pv'; bpmy_pv' ], t0, t1, 0, nread);
read_I=read(:,1);
read_x=read(:,1+(1:nbpm));
read_y=read(:,nbpm+1+(1:nbpm));
nread=size(read_I,1);
read_x=read_x*1e-6;
read_y=read_y*1e-6;
[reftimes,refread]=ml_arch_get_more(url, key,[{'REFC1:amp'};refBpm], t0, t1, 0, nread);
refread_I=refread(:,1);
refread_x=refread(:,2);
refread_y=refread(:,3);
refnread=size(refread_I,1);
refread_x=refread_x*1e-6;
refread_y=refread_y*1e-6;

%remove BPM reading with low intensity
if any(read_I<I_cut)
  fprintf('intensity cut: remove %i/%i readings\n',sum(read_I<I_cut),nread);
end
read_x(read_I<I_cut,:)=NaN;
read_y(read_I<I_cut,:)=NaN;
% refread_x(refread_I<I_cut,:)=NaN;
% refread_y(refread_I<I_cut,:)=NaN;

%get quads info and readings
QS1X=findcells(BEAMLINE,'Name','QS1X')';
QF1X=findcells(BEAMLINE,'Name','QF1X')';
QD2X=findcells(BEAMLINE,'Name','QD2X')';
QF3X=findcells(BEAMLINE,'Name','QF3X')';
QF4X=findcells(BEAMLINE,'Name','QF4X')';
quad=[QS1X QF1X QD2X QF3X QF4X];
nquad=length(quad);
for i=1:nquad
  quad_ps(i)=BEAMLINE{quad(1,i)}.PS;
  quad_B(i)=BEAMLINE{quad(1,i)}.B;
  quad_conv{i}=FL.HwInfo.PS(quad_ps(i)).conv;
  pv{i}=FL.HwInfo.PS(quad_ps(i)).pvname{1};
  quad_name{i}=BEAMLINE{quad(1,i)}.Name;
end
for i=1:nquad
  [t_q{i},~,q_A{i}]=ml_arch_get(url, key, pv{i}, t0, t1, 0, nread);
  [q_A{i},index]=unique(q_A{i},'stable');
  t_q{i}=t_q{i}(index);
end
t=sort(cell2mat(t_q));
t_quad(1)=t(1);
for i=1:nquad
  quad_ps_read(1,i)=interp1(quad_conv{i}(1,:),quad_conv{i}(2,:),q_A{i}(1))/abs(quad_B(i));
end
for i=2:length(t)
  t_quad(i)=t(i);
  for j=1:nquad
    k=find(t_q{j}==t(i));
    if k
      quad_ps_read(i,j)=interp1(quad_conv{j}(1,:),quad_conv{j}(2,:),q_A{j}(k))/abs(quad_B(j));
    else
      quad_ps_read(i,j)=quad_ps_read(i-1,j);
    end
  end
end
[t_quad,index]=unique(t_quad,'last');
quad_ps_read=quad_ps_read(index,:);
nquad_change=length(t_quad);


%get corrector settings
angle_cor=zeros(nread,ncor);
for i=1:ncor
  [t,~,r]=ml_arch_get(url, key, cor_pv{i}, t0, t1, 0, nread);
  for j=1:nread
    angle_cor(j,i)=r(find(times(j,1)>=t,1,'last'))*cor_conv(i);
  end
end

%get TM from correctors to BPMs
dread_cor_x=zeros(nread,nbpm);
dread_cor_y=zeros(nread,nbpm);
for t=1:nquad_change
  TM_cor_bpmx=zeros(ncor,nbpm);
  TM_cor_bpmy=zeros(ncor,nbpm);
  for i=1:nquad
    PS(quad_ps(i)).Ampl=quad_ps_read(t,i);
  end
  if t==1
    t_1=min(times(1,:));
  else
    t_1=t_quad(t);
  end
  if t==nquad_change
    t_2=max(times(end,:)+1);
  else
    t_2=t_quad(t+1);
  end  
  for i=1:ncor
    for j=1:nbpm
      if(cor(i)<bpm(j))
        [~,r]=RmatAtoB_cor(cor(i),bpm(j));
      else
        r=zeros(6,6);
      end
      if strcmp(cor_class{i},'XCOR')
        TM_cor_bpmx(i,j)=r(1,2);
        TM_cor_bpmy(i,j)=r(3,2);
      elseif strcmp(cor_class{i},'YCOR')
        TM_cor_bpmx(i,j)=r(1,4);
        TM_cor_bpmy(i,j)=r(3,4);
      end
    end
  end
  times_ok=find((times>=t_1).*(times<t_2));  
  if any(times_ok)
    dread_cor_x(times_ok,:)=angle_cor(times_ok,:)*TM_cor_bpmx;
    dread_cor_y(times_ok,:)=angle_cor(times_ok,:)*TM_cor_bpmy;
  end
end


%get sbend info and settings
BS1XA=findcells(BEAMLINE,'Name','BS1XA');
BS1XB=findcells(BEAMLINE,'Name','BS1XB');
BS2XA=findcells(BEAMLINE,'Name','BS2XA');
BS2XB=findcells(BEAMLINE,'Name','BS2XB');
BS3XA=findcells(BEAMLINE,'Name','BS3XA');
BS3XB=findcells(BEAMLINE,'Name','BS3XB');
BH1XA=findcells(BEAMLINE,'Name','BH1XA');
BH1XB=findcells(BEAMLINE,'Name','BH1XB');
% sbend=[BS1XA BS1XB BS2XA BS2XB BS3XA BS3XB BH1XA BH1XB];
sbend=[BH1XA BH1XB];
sbend=sbend(sbend>=reconstruct_point);
nsbend=length(sbend);
for i=1:nsbend
  if any(sbend(i)==[BS1XA BS1XB])
    sbend_pv{i}='BS1X:currentWrite';
    sbend_conv{i}=[0 2000;0 0.0593];%current value 1450 A =? 0.0593 Tm =? 0.014 rad
  elseif any(sbend(i)==[BS2XA BS2XB])
    sbend_pv{i}='BS3X:currentWrite';
    sbend_conv{i}=[0 3300;0 0.1573];%current value 2575 A =? 0.1573 Tm =? 0.0372 rad
  elseif any(sbend(i)==[BS3XA BS3XB])
    sbend_pv{i}='BS3X:currentWrite';
    sbend_conv{i}=[0 3300;0 0.3881];%current value 2575 A =? 0.4974 Tm =? 0.1175 rad
  else
    sbend_ps(i)=BEAMLINE{sbend(i)}.PS;
    sbend_pv{i}=FL.HwInfo.PS(sbend_ps(i)).pvname{1}{1};
    sbend_conv{i}=FL.HwInfo.PS(sbend_ps(i)).conv;
  end
  sbend_B(i)=BEAMLINE{sbend(i)}.B(1);
  sbend_P(i)=BEAMLINE{sbend(i)}.P;
  sbend_L(i)=BEAMLINE{sbend(i)}.L;
  sbend_name{i}=BEAMLINE{sbend(i)}.Name; 
end
for i=1:nsbend
  [t_b,~,b_A]=ml_arch_get(url, key, sbend_pv{i}, t0, t1, 0, nread);
  for j=1:nread
    dB=interp1(sbend_conv{i}(1,:),sbend_conv{i}(2,:),b_A(find(times(j,1)>=t_b,1,'last')))-sbend_B(i);
    angle_sbend(j,i)=-dB/(sbend_P(i)*1e9/3e8);
  end
end

%get TM from sbends to BPMs
dread_sbend_x=zeros(nread,nbpm);
dread_sbend_y=zeros(nread,nbpm);
for t=1:nquad_change
  TM_sbend_bpmx=zeros(nsbend,nbpm);
  TM_sbend_bpmy=zeros(nsbend,nbpm);
  for i=1:nquad
    PS(quad_ps(i)).Ampl=quad_ps_read(t,i);
  end
  if t==1
    t_1=min(times(1,:));
  else
    t_1=t_quad(t);
  end
  if t==nquad_change
    t_2=max(times(end,:)+1);
  else
    t_2=t_quad(t+1);
  end  
  for i=1:nsbend
    for j=1:nbpm
      if(sbend(i)<bpm(j))
        [~,r]=RmatAtoB_cor(sbend(i),bpm(j));
      else
        r=zeros(6,6);
      end
      TM_sbend_bpmx(i,j)=r(1,2);
      TM_sbend_bpmy(i,j)=r(3,2);
    end
  end
  times_ok=find((times>=t_1).*(times<t_2));  
  if any(times_ok)
    dread_sbend_x(times_ok,:)=angle_sbend(times_ok,:)*TM_sbend_bpmx;
    dread_sbend_y(times_ok,:)=angle_sbend(times_ok,:)*TM_sbend_bpmy;
  end
end

%correct BPM readings by the effect of the correctors and bends
read_x=read_x-dread_sbend_x;%-dread_cor_x ;
read_y=read_y-dread_sbend_y;%-dread_cor_y;

%get TM from reconstruct_point to BPMs
x=zeros(nread,1);
xp=zeros(nread,1);
y=zeros(nread,1);
yp=zeros(nread,1);
dE=zeros(nread,1);
for t=1:nquad_change
  TM_bpmx=zeros(nbpm,6);
  TM_bpmy=zeros(nbpm,6);
  for i=1:nquad
    PS(quad_ps(i)).Ampl=quad_ps_read(t,i);
  end
  if t==1
    t_1=min(times(1,:));
  else
    t_1=t_quad(t);
  end
  if t==nquad_change
    t_2=max(times(end,:)+1);
  else
    t_2=t_quad(t+1);
  end
  for i=1:nbpm
    [~,r]=RmatAtoB(reconstruct_point,bpm(i));
    TM_bpmx(i,:)=r(1,:);
    TM_bpmy(i,:)=r(3,:);
  end
  x_coeff=pinv(TM_bpmx(:,1));
  xp_coeff=pinv(TM_bpmx(:,2));
  y_coeff=pinv(TM_bpmy(:,3));
  yp_coeff=pinv(TM_bpmy(:,4));
  dE_coeff=pinv(TM_bpmx(:,6));
  %back propagate to obtain parameters at injection
  times_ok=find((times>=t_1).*(times<t_2));
  if any(times_ok)
    x(times_ok)=x_coeff*read_x(times_ok,:)';
    xp(times_ok)=xp_coeff*read_x(times_ok,:)';
    y(times_ok)=y_coeff*read_y(times_ok,:)';
    yp(times_ok)=yp_coeff*read_y(times_ok,:)';
    dE(times_ok)=dE_coeff*read_x(times_ok,:)';
  end
end

if(t1-t0>365)
  datefmt='dd-mmm-yyyy HH:MM';
elseif(t1-t0>1)
  datefmt='dd-mmm HH:MM';
else
  datefmt='HH:MM';
end 
%plot corrected BPM readings
figure(1)
subplot(2,1,1)
plot(times,read_x-repmat(mean(read_x(read_I>I_cut,:)),nread,1),'-')
title('corrected BPM readings')
legend(bpm_name)
xlim([t0 t1])
set(gca,'Xtick',linspace(t0, t1,10));
datetick('x',datefmt,'keeplimits','keepticks') 
ylabel('X [m]')
subplot(2,1,2)
plot(times,read_y-repmat(mean(read_y(read_I>I_cut,:)),nread,1),'-')
xlim([t0 t1])
set(gca,'Xtick',linspace(t0, t1,10));
datetick('x',datefmt,'keeplimits','keepticks')   
ylabel('Y [m]')

%plot parameters at injection
figure(2)
subplot(2,1,1)
plot(times,x-mean(x(isfinite(x))),'b-',...
  times,xp-mean(xp(isfinite(xp))),'r-',...
  times,y-mean(y(isfinite(y))),'c-',...
  times,yp-mean(yp(isfinite(yp))),'g-')
legend('X','X''','Y','Y''')
title(sprintf('parameters at %s',reconstruct_point_name))
xlim([t0 t1])
set(gca,'Xtick',linspace(t0, t1,10));
datetick('x',datefmt,'keeplimits','keepticks')   
ylabel('[m] or [rad]')
subplot(2,1,2)
plot(times,dE,'k-')
xlim([t0 t1])
set(gca,'Xtick',linspace(t0, t1,10));
datetick('x',datefmt,'keeplimits','keepticks')   
ylabel('dE/E')

% Plot reference BPM and compare with back propagated values
figure(5)
subplot(2,1,1)
plot(reftimes,refread_x-mean(refread_x(isfinite(refread_x))),'r-')
hold on
plot(times,x-mean(x(isfinite(x))),'b-')
hold off
xlim([t0 t1])
set(gca,'Xtick',linspace(t0, t1,10));
datetick('x',datefmt,'keeplimits','keepticks')
legend('reading','propagation')
ylabel('X [m]')
title(refBpm{1})
subplot(2,1,2)
plot(reftimes,refread_y-mean(refread_y(isfinite(refread_y))),'r-')
hold on
plot(times,y-mean(y(isfinite(y))),'b-')
hold off
xlim([t0 t1])
set(gca,'Xtick',linspace(t0, t1,10));
datetick('x',datefmt,'keeplimits','keepticks')   
ylabel('Y [m]')
title(refBpm{2})

figure(6)
subplot(2,1,1)
x_pred=x-mean(x(isfinite(x)));
xbpm=refread_x-repmat(mean(refread_x(i,:)),refnread,1);
plot(xbpm(ismember(reftimes,times)),x_pred(ismember(times,reftimes)),'.')
title('correlation between reading and prediction')
subplot(2,1,2)
y_pred=y-mean(y(isfinite(y)));
ybpm=refread_y-repmat(mean(refread_y(refread_I>I_cut,:)),refnread,1);
plot(ybpm(ismember(reftimes,times)),y_pred(ismember(times,reftimes)),'.')

% plot corrector effects
figure(3)
subplot(2,1,1)
plot(times,dread_sbend_x-repmat(mean(dread_sbend_x),nread,1),'--',...
     times,dread_cor_x-repmat(mean(dread_cor_x),nread,1),'-')
xlim([t0 t1])
set(gca,'Xtick',linspace(t0, t1,10));
datetick('x',datefmt,'keeplimits','keepticks')   
legend([strcat('bend->',bpm_name) strcat('cor->',bpm_name)])
ylabel('X [m]')
subplot(2,1,2)
plot(times,dread_sbend_y-repmat(mean(dread_sbend_y),nread,1),'--',...
     times,dread_cor_y-repmat(mean(dread_cor_y),nread,1),'-')
xlim([t0 t1])
title('sbend effects')
set(gca,'Xtick',linspace(t0, t1,10));
datetick('x',datefmt,'keeplimits','keepticks')   
ylabel('Y [m]')

%plot quad ps values
figure(4)
subplot(2,1,1)
plot(t_quad,quad_ps_read,'o')
xlim([t0 t1])
set(gca,'Xtick',linspace(t0, t1,10));
datetick('x',datefmt,'keeplimits','keepticks')   
legend(quad_name)
ylabel('PS [A]')
title('power supply values')
subplot(2,1,2)
plot(times,angle_sbend-repmat(mean(angle_sbend),nread,1),'--',...
     times,angle_cor-repmat(mean(angle_cor),nread,1),'-')
xlim([t0 t1])
set(gca,'Xtick',linspace(t0, t1,10));
datetick('x',datefmt,'keeplimits','keepticks')   
ylabel('angle [rad]')
legend([sbend_name cor_name])

end

function [t,r]=ml_arch_get_more(url, key, name, t0, t1, how, nread)
  npv=length(name);
  good_t=[];
  while (isempty(good_t))
    if nread>1000
      [data{1:npv}]=ArchiveData(url, 'values',key, name, t0, t1, 1000,how);
    else
      [data{1:npv}]=ArchiveData(url, 'values',key, name, t0, t1, nread,how);
    end
    for i=1:npv
      if exist('t_all','var') && size(t_all,1)>size(data{i},2)
        n=size(data{i},2);
        t_all=t_all(1:n,:);
        r=r(1:n,:);
      elseif exist('t_all','var') && size(t_all,1)<size(data{i},2)
        n=size(data{i},2)-size(t_all,1);
        t_all=[t_all;zeros(n,size(t_all,2))];
        r=[r;zeros(n,size(r,2))];
      end
      t_all(:,i)=data{i}(1,:)';
      r(:,i)=data{i}(3,:)';
    end
    good_t=find(std(t_all,'',2)*3600*24<0.3);
    if isempty(good_t)
      tmin=zeros(1,npv);
      deltat=t_all(1:end-1,:)-t_all(2:end,:);
      for i=1:npv
        ok=find( (abs(deltat(:,i))<(2/(24*3600))) .* (t_all(1:end-1,i)>=t0) ,1,'first');
        tmin(i)=t(ok,i);
      end
      t0=max(tmin);
      fprintf('mismatch time, set t0=%s\n',datestr(t0))
      continue
    else
      nbad=size(t_all,1)-length(good_t);
      if nbad~=0
        fprintf('synchronzation : remove %i readings\n',nbad)
      end
    end
  end
  t=t_all(good_t,1);
  nr_all=size(r,1);
  r=r(good_t,:);
  if nr_all==1000 && (t1-t(end))*3600*24>1
    if length(good_t)>10
      t0new=t(end);
    else
      t0new=t_all(good_t(end)+10);
    end
    [t2,r2]=ml_arch_get_more(url, key, name, t0new, t1, how, (t1-t0)*24*3600);
    index=find(t2>t0new,1,'first');
    t=[t; t2(index:end,:)];
    r=[r; r2(index:end,:)];
  end
end