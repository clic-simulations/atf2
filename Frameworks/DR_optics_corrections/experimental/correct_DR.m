function correct_DR(meas, modulo, weight_parameter)

%addpath('/Users/jpfingst/Work/ATF2/Yves_heritage/ATFDR_matlab/user_Files_2013Nov20_27');

online_mode = 0;

if ~exist('meas')
  load ../userData/resp_matrix_DR
end

meas_removed = [];
copy_index = 1;
for i=1:length(meas)
	if(i==67 || i==97)
	  % Do not copy
        else
	  meas_removed{copy_index} = meas{i};
          copy_index = copy_index+1;
        end
end
meas = meas_removed';

max_bad_bpms = 10;
meas = remove_failed_corrector(meas, max_bad_bpms);
nPS=length(meas);
modulo_index = modulo:modulo:nPS;
nPS = length(modulo_index);
pv=cell(nPS,1);
for ps=1:nPS
  pv{ps}=meas{modulo_index(ps)}.pv;
end
if(online_mode == 1)
    I0=lcaGet(pv);
else
    I0 = 10.*rand(nPS,1) - 5;
end

nbpm=length(meas{ps}.betax);
[goodbpmx_R, goodbpmy_R]=select_good_bpm(meas, modulo_index);

twiss=importdata('../userData/twiss.dat',' ',47);
S_model=twiss.data(:,1)';
betax_model=twiss.data(:,2)';
phasex_model=twiss.data(:,6)';
betay_model=twiss.data(:,4)';
phasey_model=twiss.data(:,7)';
Dx_model=twiss.data(:,8)';
comments= strncmp(twiss.textdata,'$',1) | strncmp(twiss.textdata,'*',1) | strncmp(twiss.textdata,'@',1);
bpms_model=twiss.textdata(~comments);
%convert cumulative phase advance to relative phase advance
phasex_model=diff([0 phasex_model]);
phasey_model=diff([0 phasey_model]);
Qx_model=0.18;
Qy_model=0.43;

%create a response matrix in a format which can be inverted:
% R*[I1;...;I99]=[dQx;dQy;dBetx(1:nbpm);dBety(1:nbpm);dPhix(1:nbpm);dPhiy(1:nbpm)]
R=zeros(1+1+nbpm+nbpm+nbpm+nbpm,nPS);
dR=zeros(1+1+nbpm+nbpm+nbpm+nbpm,nPS);
for ps=1:nPS
  R(1,ps)=meas{modulo_index(ps)}.resp_Qx;
  dR(1,ps)=meas{modulo_index(ps)}.dresp_Qx;
  R(2,ps)=meas{modulo_index(ps)}.resp_Qx;
  dR(2,ps)=meas{modulo_index(ps)}.dresp_Qx;
  R(2+(1:nbpm),ps)=meas{modulo_index(ps)}.resp_betax;
  dR(2+(1:nbpm),ps)=meas{modulo_index(ps)}.dresp_betax;
  R(2+nbpm+(1:nbpm),ps)=meas{modulo_index(ps)}.resp_betay;
  dR(2+nbpm+(1:nbpm),ps)=meas{modulo_index(ps)}.dresp_betay;
  R(2+2*nbpm+(1:nbpm),ps)=meas{modulo_index(ps)}.resp_phasex;
  dR(2+2*nbpm+(1:nbpm),ps)=meas{modulo_index(ps)}.dresp_phasex;
  R(2+3*nbpm+(1:nbpm),ps)=meas{modulo_index(ps)}.resp_phasey;
  dR(2+3*nbpm+(1:nbpm),ps)=meas{modulo_index(ps)}.dresp_phasey;  
end

fprintf('Measuring the current beta function\n\n');
if(online_mode == 1)
    [x,y,tmit]=acquire_TBT(1,512,20);
else
    x = load('./data_2013Nov27/2013_11_27_15h00_before_corr_x.mat');
    y = load('./data_2013Nov27/2013_11_27_15h00_before_corr_y.mat');
    x = x.xread;
    y = y.yread;
end
[Qx,betax,phasex,factor_betax,goodbpmx,dQx,dbetax,dphasex,dfactor_betax]=analyse_TBT(x,4e-2,betax_model);
[Qy,betay,phasey,factor_betay,goodbpmy,dQy,dbetay,dphasey,dfactor_betay]=analyse_TBT(y,4e-2,betay_model);


% Remove also the BPMs that result in an infinite result
for i=1:nbpm
  if(isinf(1/dbetax(i))==1)
    fprintf('BPM %i had infinite weight in x\n', i);
    goodbpmx(i) = 0;
  end
  if(isinf(1/dbetay(i))==1)
    fprintf('BPM %i had infinite weight in y\n', i);
    goodbpmy(i) = 0;
  end
end

% Take neither the noisy BPMs of in R (not a single one), 
% nor the noisy BPMs in the current measurement.
fprintf('Number of removed BPMs in R: x=%i, y=%i\n', nbpm-sum(goodbpmx_R), nbpm-sum(goodbpmy_R));
fprintf('Number of removed BPMs in current measurement: x=%i, y=%i\n', nbpm-sum(goodbpmx), nbpm-sum(goodbpmy));
goodbpmx = logical(bitand(goodbpmx,goodbpmx_R));
goodbpmy = logical(bitand(goodbpmy,goodbpmy_R));
fprintf('Number of removed BPMs all: x=%i, y=%i\n', nbpm-sum(goodbpmx), nbpm-sum(goodbpmy));
fprintf('Names of removed BPMs:\n');
for i=1:nbpm
  if(goodbpmx(i)==0)
    fprintf('BPM%i x\n',i);
  end
  if(goodbpmy(i)==0)
    fprintf('BPM%i y\n',i);
  end
end

%to limit the values of intensities, we use:
%[R;Id]*[I1;...;I99]=[dQx;dQy;dBetx(1:nbpm);dBety(1:nbpm);0;...;0]

% Temporally remove faulty BPMs from correction
%goodbpmx(13) = 0;
%goodbpmx(22) = 0;
%goodbpmx(41) = 0;
%goodbpmx(42) = 0;
%goodbpmx(77) = 0;
%goodbpmx(87) = 0;
%goodbpmx(88) = 0;
%goodbpmx(70) = 0;

%goodbpmy(13) = 0;
%goodbpmy(22) = 0;
%goodbpmy(41) = 0;
%goodbpmy(42) = 0;
%goodbpmy(77) = 0;
%goodbpmy(87) = 0;
%goodbpmy(88) = 0;
%goodbpmy(70) = 0;

R_beta_tot=[ R([1:2 (2+find(goodbpmx)) (2+nbpm+find(goodbpmy)) ],:) ; eye(nPS) ];
delta_vector_tot=-[Qx-Qx_model; Qy-Qy_model; betax(goodbpmx)'-betax_model(goodbpmx)';betay(goodbpmy)'-betay_model(goodbpmy)'; I0];
if(weight_parameter == 1)
  weight_tot=1./[0.0001;0.0001; dbetax(goodbpmx)' ; dbetay(goodbpmy)' ; ones(nPS,1)];
elseif(weight_parameter == 2)
  weight_tot = ones(2+length(dbetax(goodbpmx))+length(dbetay(goodbpmy))+nPS,1);
  weight_tot(1) = 10000;
  weight_tot(2) = 10000;
else
  weight_tot = [1;1];

  norm_beta_x = (1./dbetax(goodbpmx)) * (1./dbetax(goodbpmx))';
  norm_beta_x = sqrt(norm_beta_x/length(dbetax(goodbpmx)));
  weights_beta_x = (1./dbetax(goodbpmx))./norm_beta_x;
  %norm_weights_beta_x = sqrt((weights_beta_x*weights_beta_x')/length(weights_beta_x))
  weight_tot = [weight_tot; weights_beta_x'];

  norm_beta_y = (1./dbetay(goodbpmy)) * (1./dbetay(goodbpmy))';
  norm_beta_y = sqrt(norm_beta_y/length(dbetay(goodbpmy)));
  weights_beta_y = (1./dbetay(goodbpmy))./norm_beta_y;
  %norm_weights_beta_y = sqrt((weights_beta_y*weights_beta_y')/length(weights_beta_y))
  weight_tot = [weight_tot; weights_beta_y'];

  weight_tot = [weight_tot; ones(nPS,1)];
end

dI=lscov(R_beta_tot,delta_vector_tot,weight_tot);
delta_vector_prediction=(R*dI)';
delta_Qx_pred=delta_vector_prediction(1);
delta_Qy_pred=delta_vector_prediction(2);
delta_betax_pred=delta_vector_prediction(2+(1:nbpm));
delta_betay_pred=delta_vector_prediction(2+nbpm+(1:nbpm));
delta_phasex_pred=delta_vector_prediction(2+2*nbpm+(1:nbpm));
delta_phasey_pred=delta_vector_prediction(2+3*nbpm+(1:nbpm));

fprintf('Qx predicted: %.4f\n',Qx+delta_Qx_pred);
fprintf('Qy predicted: %.4f\n',Qy+delta_Qy_pred);

bpm_index_plot = 1:nbpm;
%plot measurement
figure(1)
clf
subplot(2,2,1)
plot(S_model(goodbpmx),betax(goodbpmx),'+-',S_model,betax_model,'-',S_model(goodbpmx),betax(goodbpmx)+delta_betax_pred(goodbpmx))
%plot(bpm_index_plot(goodbpmx),betax(goodbpmx),'+-',bpm_index_plot(goodbpmx),betax_model(goodbpmx),'-',S_model(goodbpmx),betax(goodbpmx)+delta_betax_pred(goodbpmx))
hold on
errorbar(S_model(goodbpmx),betax(goodbpmx),dbetax(goodbpmx))
hold off
xlim([min(S_model) max(S_model)])
ylim([0 30])
xlabel('S [m]');
ylabel('\beta_x [m]')
legend(sprintf('meas. (amp^2/%.3g)',factor_betax),'model','prediction')
subplot(2,2,2)
plot(S_model(goodbpmx),phasex(goodbpmx),'+-',S_model,phasex_model,'-',S_model(goodbpmx),phasex(goodbpmx)+delta_phasex_pred(goodbpmx))
hold on
errorbar(S_model(goodbpmx),phasex(goodbpmx),dphasex(goodbpmx))
hold off
xlabel('S [m]')
ylabel('\nu_x')
xlim([min(S_model) max(S_model)])
ylim([-.5 .5])
legend('meas.','model','prediction','Location','NorthEast') 
subplot(2,2,3)
plot(S_model(goodbpmy),betay(goodbpmy),'+-',S_model,betay_model,'-',S_model(goodbpmy),betay(goodbpmy)+delta_betay_pred(goodbpmy))
hold on
errorbar(S_model(goodbpmy),betay(goodbpmy),dbetay(goodbpmy))
hold off
xlim([min(S_model) max(S_model)])
ylim([0 30])
xlabel('S [m]')
ylabel('\beta_y [m]')
legend(sprintf('meas. (amp^2/%.3g)',factor_betay),'model','prediction')
subplot(2,2,4)
plot(S_model(goodbpmy),phasey(goodbpmy),'+-',S_model,phasey_model,'-',S_model(goodbpmy),phasey(goodbpmy)+delta_phasey_pred(goodbpmy))
hold on
errorbar(S_model(goodbpmy),phasey(goodbpmy),dphasey(goodbpmy))
hold off
xlabel('S [m]')
ylabel('\nu_y')
xlim([min(S_model) max(S_model)])
ylim([-.5 .5])
legend('meas.','model','prediction','Location','NorthEast') 

figure(2)
plot(1:nPS,I0,'+',1:nPS,I0+dI,'+')
ylabel('Intensity [A]')
legend('present value','after correction');


answer=input('apply correction ? [y/N]','s');

if(~strcmpi(answer,'Y'))
  save(['../userData/correctDR_' datestr(now,30)])
  disp('exiting')
  return;
end

if(online_mode == 1)
    lcaPut(pv,I0+dI);
    pause(5);
else
    fprintf('Here would be the correction\n');
end

[x2,y2,tmit2]=acquire_TBT(1,512,10);
[Qx2,betax2,phasex2,factor_betax2,goodbpmx2,dQx2,dbetax2,dphasex2,dfactor_betax2]=analyse_TBT(x2,4e-2,betax_model);
[Qy2,betay2,phasey2,factor_betay2,goodbpmy2,dQy2,dbetay2,dphasey2,dfactor_betay2]=analyse_TBT(y2,4e-2,betay_model);

%plot measurement
figure(1)
clf
subplot(2,2,1)
plot(S_model(goodbpmx2),betax2(goodbpmx2),'+-',...
  S_model,betax_model,'-',...
  S_model(goodbpmx),...
  betax(goodbpmx)+delta_betax_pred(goodbpmx))
hold on
errorbar(S_model(goodbpmx2),betax2(goodbpmx2),dbetax2(goodbpmx2))
hold off
xlim([min(S_model) max(S_model)])
ylim([0 30])
xlabel('S [m]');
ylabel('\beta_x [m]')
legend(sprintf('meas. after corr. (amp^2/%.3g)',factor_betax),'model','prediction')
subplot(2,2,2)
plot(S_model(goodbpmx2),phasex2(goodbpmx2),'+-',...
  S_model,phasex_model,'-',...
  S_model(goodbpmx),phasex(goodbpmx)+delta_phasex_pred(goodbpmx))
hold on
errorbar(S_model(goodbpmx2),phasex2(goodbpmx2),dphasex2(goodbpmx2))
hold off
xlabel('S [m]')
ylabel('\nu_x')
xlim([min(S_model) max(S_model)])
ylim([-.5 .5])
legend('meas. after corr.','model','prediction','Location','NorthEast') 
subplot(2,2,3)
plot(S_model(goodbpmy2),betay2(goodbpmy2),'+-',...
  S_model,betay_model,'-',...
  S_model(goodbpmy),betay(goodbpmy)+delta_betay_pred(goodbpmy))
hold on
errorbar(S_model(goodbpmy2),betay2(goodbpmy2),dbetay2(goodbpmy2))
hold off
xlim([min(S_model) max(S_model)])
ylim([0 30])
xlabel('S [m]')
ylabel('\beta_y [m]')
legend(sprintf('meas. after corr. (amp^2/%.3g)',factor_betay),'model','prediction')
subplot(2,2,4)
plot(S_model(goodbpmy2),phasey2(goodbpmy2),'+-',...
  S_model,phasey_model,'-',...
  S_model(goodbpmy),phasey(goodbpmy)+delta_phasey_pred(goodbpmy))
hold on
errorbar(S_model(goodbpmy2),phasey2(goodbpmy2),dphasey2(goodbpmy2))
hold off
xlabel('S [m]')
ylabel('\nu_y')
xlim([min(S_model) max(S_model)])
ylim([-.5 .5])
legend('meas. after corr.','model','prediction','Location','NorthEast') 

figure(2)
clf
subplot(2,1,1)
plot(S_model(goodbpmx),100*(betax(goodbpmx)-betax_model(goodbpmx))./betax_model(goodbpmx),'+-',...
  S_model(goodbpmx2),100*(betax2(goodbpmx2)-betax_model(goodbpmx2))./betax_model(goodbpmx2),'+-')
xlim([min(S_model) max(S_model)])
ylim([-100 100])
xlabel('S [m]');
ylabel('\beta_x beat. [%]')
legend('before corr.','after corr.')
subplot(2,1,2)
plot(S_model(goodbpmy),100*(betay(goodbpmy)-betay_model(goodbpmy))./betay_model(goodbpmy),'+-',...
  S_model(goodbpmy2),100*(betay2(goodbpmy2)-betay_model(goodbpmy2))./betay_model(goodbpmy2),'-+')
xlim([min(S_model) max(S_model)])
ylim([-100 100])
xlabel('S [m]');
ylabel('\beta_y beat. [%]')
legend('before corr.','after corr.')


save(['../userData/correctDR_' datestr(now,30)])
