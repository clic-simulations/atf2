function [meas_clean]=remove_failed_corrector(meas, max_bad_bpms)

  [nPS] = size(meas);
  [nI, nbpms] = size(meas{1}.goodbpmx);
  ps_clean = 1;  

  for ps=1:nPS
    goodbpmx = sum(meas{ps}.goodbpmx);
    goodbpmx = floor(goodbpmx./nI);
    nr_noisybpm_x = nbpms-sum(goodbpmx);

    goodbpmy = sum(meas{ps}.goodbpmy);
    goodbpmy = floor(goodbpmy./nI);
    nr_noisybpm_y = nbpms-sum(goodbpmy);

    if((nr_noisybpm_x>max_bad_bpms) || (nr_noisybpm_y>max_bad_bpms))
      % That is too bad -> no copying
      fprintf('Removed corrector %i with x=%i, y=%i noisy BPMs\n', ps, nr_noisybpm_x, nr_noisybpm_y);
    else
      % That is okay -> copy data
      meas_clean{ps_clean} = meas{ps};
      ps_clean = ps_clean + 1;
    end
  end
  meas_clean = meas_clean';
end
