function stat=zeroOrbit(cmd,E1,E2)
global FL BEAMLINE PS GIRDER
persistent id1 id2 psh a0h psv a0v idg mp0
validcmd={'set','setp','reset','resetp'};
if (nargin==0)
  stat{1}=-1;
  stat{2}='zeroOrbit: No cmd';
end
cmd=lower(cmd);
if (~ismember(cmd,validcmd))
  stat{1}=-1;
  stat{2}='zeroOrbit: Invalid cmd';
end
switch cmd
  case {'reset','resetp'}
    if (~exist('psh','var')||isempty(psh))
      stat{1}=-1;
      stat{2}='zeroOrbit: Nothing to reset';
    end
    % restore Ampls and mover positions
    for n=1:length(psh),PS(psh(n)).Ampl=a0h(n);end
    for n=1:length(psv),PS(psv(n)).Ampl=a0v(n);end
    for n=1:length(idg),GIRDER{idg(n)}.MoverPos=mp0(n,:);end
    % reset the reset
    psh=[];a0h=[];psv=[];a0v=[];idg=[];mp0=[];
  case {'set','setp'}
    id1=FL.SimModel.extStart;
    id2=length(BEAMLINE);
    if (nargin>=2),id1=E1;end
    if (nargin>2),id2=E2;end
    % turn off correctors (assumes NOAUTO)
    idh=intersect(findcells(BEAMLINE,'Class','XCOR'),(id1:id2))';
    psh=cellfun(@(x) x.PS,BEAMLINE(idh));
    a0h=arrayfun(@(x) x.Ampl,PS(psh))';
    for n=1:length(psh),PS(psh(n)).Ampl=0;end
    idv=intersect(findcells(BEAMLINE,'Class','YCOR'),(id1:id2))';
    psv=cellfun(@(x) x.PS,BEAMLINE(idv));
    a0v=arrayfun(@(x) x.Ampl,PS(psv))';
    for n=1:length(psv),PS(psv(n)).Ampl=0;end
    % put all movers to zero (assumes NOAUTO)
    idg=findcells(GIRDER,'Mover')';
    mp0=cell2mat(cellfun(@(x) x.MoverPos,GIRDER(idg),'UniformOutput',0)');
    for n=1:length(idg),GIRDER{idg(n)}.MoverPos=[0,0,mp0(n,3)];end
end
if (strcmp(cmd,'set')||strcmp(cmd,'reset'))
  stat{1}=1;
  return
end
% track a single on-energy particle
P0=FL.SimModel.Initial.Momentum;
beamin=FL.SimBeam{2};
beamin.Bunch.x=[0;0;0;0;0;P0];
id=(id1:id2)';
S=zeros(size(id));
x=zeros(size(id));px=zeros(size(id));
y=zeros(size(id));py=zeros(size(id));
z=zeros(size(id));
for n=1:length(id)
  [stat,beamout]=TrackThru(id1,id(n),beamin,1,1,0);
  S(n)=BEAMLINE{id(n)}.S;
  x(n)=beamout.Bunch.x(1);
  px(n)=beamout.Bunch.x(2);
  y(n)=beamout.Bunch.x(3);
  py(n)=beamout.Bunch.x(4);
  z(n)=beamout.Bunch.x(5);
end
% plot
figure(1),clf,subplot
plot(S(2:end),1e3*[x(1:end-1),y(1:end-1)])
set(gca,'XLim',[S(1),S(end)])
if (max([max(abs(x)),max(abs(y))])<1e-6)
  set(gca,'YLim',1e-3*[-1,1])
end
hor_line(0,'k:')
ylabel('Trajectory (mm)')
xlabel('S (m)')
[h0,h1]=plot_magnets_Lucretia(BEAMLINE,1,0);
% return
stat{1}=1;
end
