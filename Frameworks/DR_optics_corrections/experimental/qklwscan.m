iex=findcells(BEAMLINE,'Name','IEX');
lw=findcells(BEAMLINE,'Name','LW1FF');
qk1=findcells(BEAMLINE,'Name','QK1X'); qk1=BEAMLINE{qk1(1)}.PS;
qk2=findcells(BEAMLINE,'Name','QK2X'); qk2=BEAMLINE{qk2(1)}.PS;
qk3=findcells(BEAMLINE,'Name','QK3X'); qk3=BEAMLINE{qk3(1)}.PS;
qk4=findcells(BEAMLINE,'Name','QK4X'); qk4=BEAMLINE{qk4(1)}.PS;

qq=linspace(-0.22,0.22,21);
iqq=linspace(-20,20,21);
sq=qk4;
ys=[];
for iqk=1:length(qq)
  PS(sq).Ampl=qq(iqk);
  [stat bo]=TrackThru(iex,lw,Beam1_IEX,1,1,0);
  ys(iqk)=std(bo.Bunch.x(3,:));
end
PS(sq).Ampl=0;