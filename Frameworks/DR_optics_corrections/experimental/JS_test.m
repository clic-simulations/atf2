JS=FlJitterSubtract;
JS.bpmToolINSTR='svdjit';
JS.getModes;
iele=findcells(BEAMLINE,'Name','MQM14FF');
data=JS.getSubData(20,iele);
figure
subplot(2,1,1), errorbar(1:length(data.x(1,:)),data.x_uncor_mean,data.x_uncor_rms,'b.'),...
  hold on,errorbar(1:length(data.x(1,:)),data.x_mean,data.x_rms,'r.');
subplot(2,1,2), errorbar(1:length(data.y(1,:)),data.y_uncor_mean,data.y_uncor_rms,'b.'),...
  hold on,errorbar(1:length(data.y(1,:)),data.y_mean,data.y_rms,'r.');
fprintf('Raw data: sig_x: %g sig_y: %g\n',sum(data.x_uncor_rms),sum(data.y_uncor_rms))
fprintf('Jitter subtracted data: sig_x: %g sig_y: %g\n',sum(data.x_rms),sum(data.y_rms))