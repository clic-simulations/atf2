function [last_ps]=get_last_measured_ps(meas)
  [nr_cells] = size(meas)
  last_ps = 0;
for i=nr_cells:-1:1
    if(isempty(meas{i}) == 1)
      % Not yet measured
    else
      % Already measured
      last_ps = i;
      return;
    end
  end
end
