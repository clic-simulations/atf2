function plot_resp_matrix_DR(meas,knobs,fig_nr)

  twiss=importdata('../userData/twiss.dat',' ',47);
  S_model=twiss.data(:,1)';
  nknob=length(knobs);
  col=hsv(nknob);
%plot response matrices
  figure(fig_nr);
  clf
  for i=1:nknob
    ps=knobs(i);
    goodbpmx=sum(meas{ps}.goodbpmx)>1;
    goodbpmy=sum(meas{ps}.goodbpmy)>1;
    subplot(2,2,1)

    hold on
    plot(S_model(goodbpmx),meas{ps}.resp_betax(goodbpmx),'-','color',col(i,:));
    errorbar(S_model(goodbpmx),meas{ps}.resp_betax(goodbpmx),meas{ps}.dresp_betax(goodbpmx),'color',col(i,:))
    hold off
    xlabel('S [m]');
    ylabel('\beta_x/I [m/A]')
    xlim([min(S_model) max(S_model)])
    subplot(2,2,2)
    hold on
    plot(S_model(goodbpmx),meas{ps}.resp_phasex(goodbpmx),'-','color',col(i,:));
    errorbar(S_model(goodbpmx),meas{ps}.resp_phasex(goodbpmx),meas{ps}.dresp_phasex(goodbpmx),'color',col(i,:))
    hold off
    xlabel('S [m]')
    ylabel('\nu_x/I [A^{-1}]')
    xlim([min(S_model) max(S_model)])
    subplot(2,2,3)
    hold on
    plot(S_model(goodbpmy),meas{ps}.resp_betay(goodbpmy),'-','color',col(i,:));
    errorbar(S_model(goodbpmy),meas{ps}.resp_betay(goodbpmy),meas{ps}.dresp_betay(goodbpmy),'color',col(i,:))
    hold off
    xlabel('S [m]');
    ylabel('\beta_y/I [m/A]')
    xlim([min(S_model) max(S_model)])
    subplot(2,2,4)
    hold on
    plot(S_model(goodbpmy),meas{ps}.resp_phasey(goodbpmy),'-','color',col(i,:));
    errorbar(S_model(goodbpmy),meas{ps}.resp_phasey(goodbpmy),meas{ps}.dresp_phasey(goodbpmy),'color',col(i,:))
    hold off
    xlabel('S [m]')
    ylabel('\nu_y/I [A^{-1}]')
    xlim([min(S_model) max(S_model)])
  end
