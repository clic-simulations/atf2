bpmname='IPBPMB';
ipbpm=findcells(BEAMLINE,'Name',bpmname);ipbpm=findcells(INSTR,'Index',ipbpm);
wf='lsis12:waveform6';
if strcmp(bpmname,'IPBPMA')
  phwf='lsis11:waveform6';
else
  phwf='lsis12:waveform4';
end
pt=69; ptave=3;
npulse=1000;
ypos=zeros(1,npulse); yposFS=ypos; pha=ypos;
lcaSetMonitor(wf);
for ipulse=1:npulse
  lcaNewMonitorWait(wf);
  wfdat=lcaGet(wf,pt+1);
  phwfdat=lcaGet(phwf,pt+1);
  ypos(ipulse)=mean(wfdat(end-(ptave-1):end));
  pha(ipulse)=mean(phwfdat(end-(ptave-1):end));
  FlHwUpdate;
  yposFS(ipulse)=INSTR{ipbpm}.Data(2);
  if ~mod(ipulse,100)
    disp(ipulse)
  end
end
disp('done.')
ypospl=(ypos-mean(ypos(1:50))).*1e-6;
yposFSpl=yposFS-mean(yposFS);
phapl=pha;
for icut=1:1
  yposFSpl=yposFSpl(abs(ypospl-mean(ypospl))<(2*std(ypospl)));
%   phapl=phapl(abs(ypospl-mean(ypospl))<(2*std(ypospl)));
  ypospl=ypospl(abs(ypospl-mean(ypospl))<(2*std(ypospl)));
  ypospl=ypospl(abs(yposFSpl-mean(yposFSpl))<(2*std(yposFSpl)));
  yposFSpl=yposFSpl(abs(yposFSpl-mean(yposFSpl))<(2*std(yposFSpl)));
  yposFSpl=yposFSpl-mean(yposFSpl);
%   phapl=phapl(abs(yposFSpl-mean(yposFSpl))<(2*std(yposFSpl)));
  ypospl=ypospl-mean(ypospl);
end
figure
plot(1:length(ypospl),ypospl,1:length(ypospl),yposFSpl);
legend('FPGA POS','FS POS')
figure
plot(ypospl,yposFSpl,'.')
xlabel('FPGA POS'); ylabel('FS POS');
% figure
% plotyy(1:length(phapl),phapl,1:length(ypospl),ypospl)