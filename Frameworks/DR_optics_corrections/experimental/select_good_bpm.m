function [goodbpm_x, goodbpm_y]=select_good_bpm(meas, ps_index)

  [nI, nbpms] = size(meas{ps_index(1)}.goodbpmx);
  goodbpm_x = ones(1,nbpms);
  goodbpm_y = ones(1,nbpms);
  
  for ps=1:length(ps_index)
    ps_current = ps_index(ps);
    goodbpm_current = sum(meas{ps_current}.goodbpmx);
    goodbpm_current = floor(goodbpm_current./nI);
    %fprintf('Removed BPMs for power supply %i: %i\n',ps_current, nbpms-sum(goodbpm_current));
    goodbpm_x = bitand(goodbpm_x, goodbpm_current);

    goodbpm_current = sum(meas{ps_current}.goodbpmy);
    goodbpm_current = floor(goodbpm_current./nI);
    goodbpm_y = bitand(goodbpm_y, goodbpm_current);
  end
end
