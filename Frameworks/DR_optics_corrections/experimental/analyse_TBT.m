function [Q,beta,phase,factor_beta,goodbpm,dQ,dbeta,dphase,dfactor_beta]=analyse_TBT(data,freqmin,beta_model)
if(~exist('freqmin','var') || isempty(freqmin))
  freqmin=4e-2;
end
if(~exist('beta_model','var') || isempty(beta_model))
  beta_model=1;
end
nshot=size(data,1);
nturn=size(data,2);
nbpm=size(data,3);
phasex=zeros(nshot,nbpm);
goodbpmx=zeros(nshot,nbpm);
betax=zeros(nshot,nbpm);
for shot=1:nshot
  x=reshape(data(shot,:,:),nturn,nbpm);
  x=x-repmat(mean(x),nturn,1);
  %remove low freq (to avoid synchrotron tune)
  frange=ifftshift(-0.5:1/nturn:0.5-1/nturn);
  badfrange=abs(frange)<freqmin;
  fftx=fft(x);
  fftx_hpf=fftx;
  fftx_hpf(badfrange,:)=0;
  x_hpf=real(ifft(fftx_hpf));
  [Qx(shot),dQx(shot),~,phasex(shot,:),ampx,~,~,goodbpmx(shot,:)]=freqan(x_hpf,1,'all');
  factor_betax(shot)=mean(ampx(~isnan(ampx)).^2)/mean(beta_model);
  betax(shot,:)=(ampx).^2/factor_betax(shot);
end
[Q,dQ]=noplot_polyfit(1:nshot,Qx,dQx,0);
[factor_beta,dfactor_beta]=noplot_polyfit(1:nshot,factor_betax,1,0);
beta=zeros(1,nbpm);
phase=zeros(1,nbpm);
dbeta=zeros(1,nbpm);
dphase=zeros(1,nbpm);
goodbpm=zeros(1,nbpm);
for bpm=1:nbpm
  if(sum(goodbpmx(:,bpm))>=1)
    [beta(bpm),dbeta(bpm)]=noplot_polyfit(1:sum(goodbpmx(:,bpm)),betax(find(goodbpmx(:,bpm)),bpm),1,0);
    [phase(bpm),dphase(bpm)]=noplot_polyfit(1:sum(goodbpmx(:,bpm)),phasex(find(goodbpmx(:,bpm)),bpm),1,0);
    goodbpm(bpm)=1;
  end
end
goodbpm=logical(goodbpm);
