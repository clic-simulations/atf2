files=ls('userData/bpmdata_refcav*.mat');
files=regexp(files,'\n','split');
files=files(1:end-1);
nfile=length(files);
charge=zeros(1,nfile);% charge of the pulse in 10^10 e-
ringRF_V=zeros(1,nfile);%voltage of the ring RF in MV
ref_pos=zeros(1,nfile);
xdata={};
ydata={};
Idata={};
IEX=findcells(BEAMLINE,'Name','IEX');
bpm=[];
bpm_name={};
bpm_instr=[];
bpm_type={};
for i=1:length(INSTR)
  if INSTR{i}.Index>IEX
    if ~isempty(FL.HwInfo.INSTR(i).pvname)
      if(strcmp(INSTR{i}.Type,'button') || strcmp(INSTR{i}.Type,'stripline') || strcmp(INSTR{i}.Type,'ccav') || strcmp(INSTR{i}.Type,'ip') )
        bpm(end+1)=INSTR{i}.Index;
        bpm_name{end+1}=BEAMLINE{bpm(end)}.Name;
        bpm_instr(end+1)=i;
        bpm_type{end+1}=INSTR{i}.Type;
      end
    end
  end
end
for i=1:nfile
  ref_pos(i)=str2double(regexp(files{i},'([\-0-9.]*)mm','tokens','once'));
  V=str2double(regexp(files{i},'E([0-9.]*)kV','tokens','once'));
  if isempty(V)
    ringRF_V(i)=0.2;
  else
    ringRF_V(i)=0.29;
  end
  I=regexp(files{i},'Charge([0-9_]*)[._]','tokens','once');
  if isempty(I)
    charge(i)=0.2;
  else
    charge(i)=str2double(regexprep(I,'_','.'))/10;
  end
  test=load(files{i});
  I=test.bpmdata(:,bpm_instr*3+1);
  Icut=I(:,end)>.05e10;
  Idata{i}=I(Icut,:);
  X=test.bpmdata(Icut,bpm_instr*3-1);
  Y=test.bpmdata(Icut,bpm_instr*3);
  [U,S,V]=svd(X);
  S(1:2,1:2)=0;
  xdata{i}=U*S*V';
  
end
figure(1)
clf
hold on
bpm=45;
legend_txt={};
for i=1:nfile
  plot(Idata{i}(:,bpm),xdata{i}(:,bpm),'.')
  legend_txt{i}=sprintf('I=%.2f RF=%.2fMV REFpos=%.1f',charge(i),ringRF_V(i),ref_pos(i));
end
hold off
legend(char(legend_txt))