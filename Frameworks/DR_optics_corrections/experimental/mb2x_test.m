brwf='slacvme1:sdigi14:Cavity_BPM:CH1';
blwf='slacvme1:sdigi14:Cavity_BPM:CH2';
tlwf='slacvme1:sdigi14:Cavity_BPM:CH3';
trwf='slacvme1:sdigi14:Cavity_BPM:CH4';

np=200;
br=zeros(1,np); bl=br; tr=br; tl=br;
for ipulse=1:np
  pause(1/3)
  br(ipulse)=std(lcaGet(brwf,100));
  bl(ipulse)=std(lcaGet(brwf,100));
  tl(ipulse)=std(lcaGet(tlwf,100));
  tr(ipulse)=std(lcaGet(trwf,100));
end