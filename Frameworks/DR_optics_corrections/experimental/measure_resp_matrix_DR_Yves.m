function meas=measure_resp_matrix_DR(dI_scan)
global BEAMLINE PS FL;

if ~exist('dI_scan','var')
  dI_scan=[-3 0 3]; %PS intensity (A) scan values
end
Imax=5; %Max PS intensity (A) 
Imin=-5; %Min PS intensity (A) 

%determine usefull information
nbpm=96;
nI=length(dI_scan);
QF1R_index=findcells(BEAMLINE,'Name','QF1R*');
QF2R_index=findcells(BEAMLINE,'Name','QF2R*');
QMR1_index=findcells(BEAMLINE,'Name','QM*R1');
QMR2_index=findcells(BEAMLINE,'Name','QM*R2');
mag_index=[QF1R_index QF2R_index QMR1_index QMR2_index];

nmag=length(mag_index);
PS_index=zeros(1,nmag);
mag_name=cell(nmag,1);
for i=1:nmag
  ind=mag_index(i);
  PS_index(i)=BEAMLINE{ind}.PS;
  mag_name{i}=BEAMLINE{ind}.Name;
end
PS_index=unique(PS_index);
nPS=length(PS_index);
pvmain=cell(nPS,1);
pvtrim=cell(nPS,1);
for i=1:nPS
  pvmain{i}=FL.HwInfo.PS(PS_index(i)).pvname{1}{1};
  pvtrim{i}=FL.HwInfo.PS(PS_index(i)).pvname{1}{2};
end
I0=lcaGet(pvtrim);
%remove unavalable PS
goodPS=find(isfinite(I0));
PS_index=PS_index(goodPS);
nPS=length(PS_index);
pvmain=pvmain(goodPS);
pvtrim=pvtrim(goodPS);
I0=I0(goodPS);
%get nominal model
twiss=importdata('../userData/twiss.dat',' ',47);
S_model=twiss.data(:,1)';
betax_model=twiss.data(:,2)';
phasex_model=twiss.data(:,6)';
betay_model=twiss.data(:,4)';
phasey_model=twiss.data(:,7)';
Dx_model=twiss.data(:,8)';
comments= strncmp(twiss.textdata,'$',1) | strncmp(twiss.textdata,'*',1) | strncmp(twiss.textdata,'@',1);
bpms_model=twiss.textdata(~comments);
%convert cumulative phase advance to relative phase advance
phasex_model=diff([0 phasex_model]);
phasey_model=diff([0 phasey_model]);

%scan all quadrupoles

I=zeros(nI,nPS); % intensity during scans
meas=cell(nPS,1); % structure with all measurement for all I and all PS
for ps=1:nPS
  I(:,ps)=dI_scan+I0(ps);
  while any(I(:,ps)>(Imax-.5))
    I(:,ps)=I(:,ps)-.5;
  end
  while any(I(:,ps)<(Imin+.5))
    I(:,ps)=I(:,ps)+.5;
  end
  fprintf('scan %s: ',pvtrim{ps});
  meas{ps}.pv=pvtrim{ps};
  meas{ps}.I=I(:,ps);
  meas{ps}.I0=I0(ps);
  meas{ps}.Qx=zeros(nI,1);
  meas{ps}.dQx=zeros(nI,1);
  meas{ps}.betax=zeros(nI,nbpm);
  meas{ps}.phasex=zeros(nI,nbpm);
  meas{ps}.goodbpmx=zeros(nI,nbpm);
  meas{ps}.Qy=zeros(nI,1);
  meas{ps}.dQy=zeros(nI,1);
  meas{ps}.betay=zeros(nI,nbpm);
  meas{ps}.phasey=zeros(nI,nbpm);
  meas{ps}.goodbpmy=zeros(nI,nbpm);
  for scan=1:nI
    fprintf('I=%.3f A ',I(scan,ps));
    %set the PS
    for i=1:5
      lcaPut(pvtrim{ps},I(scan,ps));
      pause(2);
      Iread=lcaGet(pvtrim{ps});
      if(abs(Iread-I(scan,ps)) <.1)
        break
      else
        fprintf('\nWarning: %s not set to %.3f, try %i/5\n',pvtrim{ps},I(scan,ps),i);
      end
    end
    [x,y,tmit]=acquire_TBT(1,512,10);
    [Qx,betax,phasex,factor_betax,goodbpmx,dQx,dbetax,dphasex,dfactor_betax]=analyse_TBT(x,4e-2,betax_model);
    [Qy,betay,phasey,factor_betay,goodbpmy,dQy,dbetay,dphasey,dfactor_betay]=analyse_TBT(y,4e-2,betay_model);
    meas{ps}.Qx(scan)=Qx;
    meas{ps}.betax(scan,:)=betax;
    meas{ps}.phasex(scan,:)=phasex;
    meas{ps}.goodbpmx(scan,:)=goodbpmx;
    meas{ps}.dQx(scan)=dQx;
    meas{ps}.dbetax(scan,:)=dbetax;
    meas{ps}.dphasex(scan,:)=dphasex;
    meas{ps}.Qy(scan)=Qy;
    meas{ps}.betay(scan,:)=betay;
    meas{ps}.phasey(scan,:)=phasey;
    meas{ps}.goodbpmy(scan,:)=goodbpmy;
    meas{ps}.dQy(scan)=dQy;
    meas{ps}.dbetay(scan,:)=dbetay;
    meas{ps}.dphasey(scan,:)=dphasey;
   
%     %plot measurement
%     figure(1)
%     clf
%     subplot(2,2,1)
%     plot(S_model(goodbpmx),betax(goodbpmx),'+-',S_model,betax_model,'-')
%     hold on
%     errorbar(S_model(goodbpmx),betax(goodbpmx),dbetax(goodbpmx))
%     hold off
%     ylim([0 30])
%     xlabel('S [m]');
%     ylabel('\beta_x [m]')
%     legend(sprintf('meas. (amp^2/%.3g)',factor_betax),'model')
%     subplot(2,2,2)
%     plot(S_model(goodbpmx),phasex(goodbpmx),'+-',S_model,phasex_model,'-')
%     hold on
%     errorbar(S_model(goodbpmx),phasex(goodbpmx),dphasex(goodbpmx))
%     hold off
%     xlabel('S [m]')
%     ylabel('\nu_x')
%     ylim([-.5 .5])
%     legend('meas.','model','Location','NorthEast') 
%     subplot(2,2,3)
%     plot(S_model(goodbpmy),betay(goodbpmy),'+-',S_model,betay_model,'-')
%     hold on
%     errorbar(S_model(goodbpmy),betay(goodbpmy),dbetay(goodbpmy))
%     hold off
%     ylim([0 30])
%     xlabel('S [m]')
%     ylabel('\beta_y [m]')
%     legend(sprintf('meas. (amp^2/%.3g)',factor_betay),'model')
%     subplot(2,2,4)
%     plot(S_model(goodbpmy),phasey(goodbpmy),'+-',S_model,phasey_model,'-')
%     hold on
%     errorbar(S_model(goodbpmy),phasey(goodbpmy),dphasey(goodbpmy))
%     hold off
%     xlabel('S [m]')
%     ylabel('\nu_y')
%     ylim([-.5 .5])
%     legend('meas.','model','Location','NorthEast') 
  end
  fprintf('\n');
  %reset the PS to the initial value
  fprintf('reset to I=%.3f A\n',I0(ps));
  for i=1:5
    lcaPut(pvtrim{ps},I0(ps));
    pause(2);
    Iread=lcaGet(pvtrim{ps});
    if(abs(Iread-I0(ps)) <.1)
      break
    else
      fprintf('Warning: %s not set to %.3f, try %i/5\n',pvtrim{ps},I0(ps),i);
    end
  end
end

for ps=1:nPS
  %compute response matrices
  Qx_ok=isfinite(meas{ps}.Qx);
  [q,dq]=noplot_polyfit(meas{ps}.I(Qx_ok),meas{ps}.Qx(Qx_ok),meas{ps}.dQx(Qx_ok),1);
  meas{ps}.resp_Qx=q(2);
  meas{ps}.dresp_Qx=dq(2);
  meas{ps}.resp_betax=zeros(1,nbpm);
  meas{ps}.dresp_betax=zeros(1,nbpm);
  meas{ps}.resp_phasex=zeros(1,nbpm);
  meas{ps}.dresp_phasex=zeros(1,nbpm);
  Qy_ok=isfinite(meas{ps}.Qy);
  [q,dq]=noplot_polyfit(meas{ps}.I(Qy_ok),meas{ps}.Qy(Qy_ok),meas{ps}.dQy(Qy_ok),1);
  meas{ps}.resp_Qy=q(2);
  meas{ps}.dresp_Qy=dq(2);
  meas{ps}.resp_betay=zeros(1,nbpm);
  meas{ps}.dresp_betay=zeros(1,nbpm);
  meas{ps}.resp_phasey=zeros(1,nbpm);
  meas{ps}.dresp_phasey=zeros(1,nbpm);
  for bpm=1:nbpm
    scan_ok=logical(meas{ps}.goodbpmx(:,bpm) & isfinite(meas{ps}.Qx));
    if(sum(scan_ok)>=2)
      [q,dq]=noplot_polyfit(meas{ps}.I(scan_ok),meas{ps}.betax(scan_ok,bpm),meas{ps}.dbetax(scan_ok,bpm),1);
      meas{ps}.resp_betax(bpm)=q(2);
      meas{ps}.dresp_betax(bpm)=dq(2);
      [q,dq]=noplot_polyfit(meas{ps}.I(scan_ok),meas{ps}.phasex(scan_ok,bpm),meas{ps}.dphasex(scan_ok,bpm),1);
      meas{ps}.resp_phasex(bpm)=q(2);
      meas{ps}.dresp_phasex(bpm)=dq(2);
    end
    scan_ok=logical(meas{ps}.goodbpmy(:,bpm) & isfinite(meas{ps}.Qy));
    if(sum(scan_ok)>=2)
      [q,dq]=noplot_polyfit(meas{ps}.I(scan_ok),meas{ps}.betay(scan_ok,bpm),meas{ps}.dbetay(scan_ok,bpm),1);
      meas{ps}.resp_betay(bpm)=q(2);
      meas{ps}.dresp_betay(bpm)=dq(2);
      [q,dq]=noplot_polyfit(meas{ps}.I(scan_ok),meas{ps}.phasey(scan_ok,bpm),meas{ps}.dphasey(scan_ok,bpm),1);
      meas{ps}.resp_phasey(bpm)=q(2);
      meas{ps}.dresp_phasey(bpm)=dq(2);
    end
  end
end
save(['../userData/resp_matrix_DR_' datestr(now,30)],'meas');
