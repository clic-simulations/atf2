function resonnances = get_resonnances( filename,freqmin,nfreq,sigma,order_limit,amp_min,skip,nread)
%GET_RESONNANCES return resonances found with NAFF
% usage       : resonnances = get_resonnances( filename,freqmin,nfreq,sigma,order_limit,amp_min,skip,nread)
% filename    : NAFF input file
% freqmin     : frequency below freqmin are filtred out (default=0.01)
% nfreq       : number of pics to detect with NAFF (default=10)
% sigma       : frequency resolution to search for resonnances
% order_limit : max resonnance order to look for (default=5)
% amp_min     : min amplitude of the pics to be accounted for resonnances (default=0)
% skip        : number of turns to skip
% nread       : number of turn to concider
% resonnances : 1 cell per resonnance including
%               coeff : [nQx nQy]
%               name : name in the format '(%i %i),nQx,nQy'
%               f : frequency of the resonnance
%               ampx_hpf,phasex_hpf,freqx_hpf : amplitude phase and frequency of the resonance for each horizontal bpm
%               ampy_hpf,phasey_hpf,freqy_hpf : amplitude phase and frequency of the resonance for each vertical bpm

if ~exist('filename','var') || isempty(filename)
    filename='good.dat';
end
if ~exist('freqmin','var')|| isempty(freqmin)
    freqmin=0.04;
end
if ~exist('nfreq','var')|| isempty(nfreq)
    nfreq=20;
end
if ~exist('order_limit','var')|| isempty(order_limit)
    order_limit=3;
end
if ~exist('amp_min','var') || isempty(amp_min)
    amp_min=0;
end
amp_minx=amp_min;
amp_miny=amp_min;
if ~exist('skip','var') || isempty(skip)
    skip=0;
end
if ~exist('nread','var') || isempty(nread)
    nread=200;
end

nbpm_min=10;
sigma_min=3e-4;
sigma_max=0.01;

A=importdata(filename,' ',1);
xlines=find(str2double({A.textdata{2:end,1}}')==0);
ylines=find(str2double({A.textdata{2:end,1}}')==1);

fprintf('remove M.10\n');
xlines=xlines(~strcmp(A.textdata(xlines+1,2),'M.10'));
ylines=ylines(~strcmp(A.textdata(ylines+1,2),'M.10'));

bpmsx={A.textdata{xlines+1,2}};
bpmsy={A.textdata{ylines+1,2}};
xdata=A.data(xlines,3:end)';
ydata=A.data(ylines,3:end)';
nbpmx=size(xdata,2);
nbpmy=size(ydata,2);
nturn=size(xdata,1);
bpmsx_S=A.data(xlines,1)
bpmsy_S=A.data(ylines,1)


twiss=importdata('twiss.dat',' ',47);
betax_model=twiss.data(:,2)' ;
phasex_model=twiss.data(:,6)';
betay_model=twiss.data(:,4)';
phasey_model=twiss.data(:,7)';
Dx_model=twiss.data(:,8)';
comments= strncmp(twiss.textdata,'$',1) | strncmp(twiss.textdata,'*',1) | strncmp(twiss.textdata,'@',1);
bpms_model=twiss.textdata(~comments);
bpmsx_measured=[];
bpmsy_measured=[];
for i=1:length(bpms_model)
    bpmsx_measured(end+1)=any(strcmp(bpms_model{i},bpmsx));
    bpmsy_measured(end+1)=any(strcmp(bpms_model{i},bpmsy));
end
bpmsx_measured=find(bpmsx_measured);
bpmsy_measured=find(bpmsy_measured);
betax_model=betax_model(bpmsx_measured);
betay_model=betay_model(bpmsy_measured);
phasex_model=phasex_model(bpmsx_measured);
phasey_model=phasey_model(bpmsy_measured);
Dx_model=Dx_model(bpmsx_measured);

%remove bad readings
xdata(xdata==999999)=NaN;
ydata(ydata==999999)=NaN;

%to distinguish between simu and data,
%look for bad readings
simu=~any(any(isnan(xdata)));
if (simu)
    %add BPM noise
    noise_level=0;
%    noise_level=30;
    fprintf('DEBUG : add noise (%i um) to BPM readings\n',noise_level);
    xdata=xdata+noise_level*randn(size(xdata));
    ydata=ydata+noise_level*randn(size(ydata));
end

%convert BPM readings in meters
xdata=xdata*1e-6;
ydata=ydata*1e-6;
%remove low freq (<freqmin)
frange=ifftshift(-0.5:1/nturn:0.5-1/nturn);
badfrange=abs(frange)<freqmin;
fftx=fft(xdata);
ffty=fft(ydata);
if freqmin>1/nturn
    fprintf('frequencies cut up to %g\n',max(frange(badfrange)))
    fftx_hpf=fftx;
    fftx_hpf(badfrange,:)=0;
    ffty_hpf=ffty;
    ffty_hpf(badfrange,:)=0;
    xdata_hpf=real(ifft(fftx_hpf));
    ydata_hpf=real(ifft(ffty_hpf));
    fftx_lpf=fftx;
    fftx_lpf(~badfrange,:)=0;
    ffty_lpf=ffty;
    ffty_lpf(~badfrange,:)=0;
    xdata_lpf=real(ifft(fftx_lpf));
    ydata_lpf=real(ifft(ffty_lpf));
else
    fprintf('freqmin %.3g too low for %i turns, remove mean value\n',freqmin,nturn);
    for i=1:nbpmx
        xdata_hpf(:,i)=xdata(:,i)-mean(xdata(~isnan(xdata(i,:)),i));
    end
    for i=1:nbpmy    
        ydata_hpf(:,i)=ydata(:,i)-mean(ydata(~isnan(ydata(i,:)),i));
    end
end
for i=1:nbpmx
    xdata(:,i)=xdata(:,i)-mean(xdata(~isnan(xdata(i,:)),i));
end
for i=1:nbpmy
    ydata(:,i)=ydata(:,i)-mean(ydata(~isnan(ydata(i,:)),i));
end
xdata_all=xdata;
ydata_all=ydata;
xdata_hpf=xdata_hpf(skip+1:nread+skip,:);
ydata_hpf=ydata_hpf(skip+1:nread+skip,:);

%find tunes
[Qx_hpf,dQx_hpf,tunesx_hpf,phasex_hpf,ampx_hpf,numturnsx_hpf,frespecx_hpf]=freqan(xdata_hpf,nfreq,'all');
[Qy_hpf,dQy_hpf,tunesy_hpf,phasey_hpf,ampy_hpf,numturnsy_hpf,frespecy_hpf]=freqan(ydata_hpf,nfreq,'all');
[Qx_all,dQx_all,tunesx_all,phasex_all,ampx_all,numturnsx_all,frespecx_all]=freqan(xdata_all,nfreq,'all');
[Qy_all,dQy_all,tunesy_all,phasey_all,ampy_all,numturnsy_all,frespecy_all]=freqan(ydata_all,nfreq,'all');
% Qx=Qx_hpf;
freq_ok=(abs(frespecx_all(:,3))>abs(Qx_hpf)-0.05) & (abs(frespecx_all(:,3))<abs(Qx_hpf)+0.05);
% if(all(freq_ok))
%     amp_min=0;
% else
%     amp_min=max(frespecx_all(~freq_ok,4));
% end
amp_min=5e-6;
amp_ok=frespecx_all(:,4)>amp_min;
Qx=sum(abs(frespecx_all(freq_ok&amp_ok,3)).*frespecx_all(freq_ok&amp_ok,4))/sum(frespecx_all(freq_ok&amp_ok,4));
[amp,phase,freq]=analyse_frespec(frespecx_all,nfreq,Qx,5e-3,0);
Qx=mean(freq(~isnan(freq)));
dQx=std(freq(~isnan(freq)));
freq_ok=(abs(frespecy_all(:,3))>abs(Qy_hpf)-0.05) & (abs(frespecy_all(:,3))<abs(Qy_hpf)+0.05);
% if(all(freq_ok))
%     amp_min=0;
% else
%     amp_min=max(frespecy_hpf(~freq_ok,4));
% end
amp_ok=frespecy_all(:,4)>amp_min;
Qy=sum(abs(frespecy_all(freq_ok&amp_ok,3)).*frespecy_all(freq_ok&amp_ok,4))/sum(frespecy_all(freq_ok&amp_ok,4));
[amp,phase,freq]=analyse_frespec(frespecy_all,nfreq,Qy,5e-3,0);
Qy=-mean(freq(~isnan(freq)));
dQy=std(freq(~isnan(freq)));
% Qy=-Qy_hpf;
dQy=dQy_hpf;
[Qx_lpf,dQx_lpf,tunesx_lpf,phasex_lpf,ampx_lpf,numturnsx_lpf,frespecx_lpf]=freqan(xdata_lpf,nfreq,'all');
[amp,phase,freq]=analyse_frespec(frespecx_all,nfreq,5e-3,1e-3,0);
Qs=mean(freq(~isnan(freq)));
dQs=std(freq(~isnan(freq)));

fprintf('found Qx=%.4f+-%.4f, Qy=%.4f+-%4f and Qs=%.4f+-%4f\n',Qx,dQx,Qy,dQy,Qs,dQs)

%search resonances
if (~exist('sigma','var') ||  isempty(sigma))
    sigma=3*sqrt(dQx^2+dQy^2);
end
if sigma>sigma_max
    fprintf('df = %.2e too big, take %.2e\n',sigma,sigma_max)
    sigma=sigma_max;
end
if sigma<sigma_min
    fprintf('df = %.2e too small, take %.2e\n',sigma,sigma_min)
    sigma=sigma_min;
end
fprintf('resonnance search with a resolution df = %.2e\n',sigma)
coeffs=[];
nQx=0;
nQy=0;
%coeffs [0 0 i]
for nQs=1:order_limit
    coeffs(end+1,:)=[nQx nQy nQs];
end
%coeffs [i 0 j]
for nQx=1:order_limit
    for nQs=-order_limit+nQx:order_limit-nQx
%     for nQs=0
        coeffs(end+1,:)=[nQx nQy nQs];
    end
end
nQx=0;
%coeffs [0 i j]
for nQy=1:order_limit
    for nQs=-order_limit+nQy:order_limit-nQy
%     for nQs=0
        coeffs(end+1,:)=[nQx nQy nQs];
    end
end
%coeffs [i j 0]
for nQx=1:order_limit
    for nQy=-order_limit+nQx:order_limit-nQx
%         for nQs=-order_limit+nQx+abs(nQy):order_limit-nQx-abs(nQy)
        for nQs=0
            coeffs(end+1,:)=[nQx nQy nQs];
        end
    end
end
coeffs=unique(coeffs,'rows');
ncoeff=size(coeffs,1);
resonnances={};
for i=1:ncoeff
    freq=coeffs(i,1)*Qx+coeffs(i,2)*Qy+coeffs(i,3)*Qs;
%     if(abs(freq)>freqmin)
%         [ampx,phasex,freqx]=analyse_frespec(frespecx_hpf,nfreq,freq,sigma,amp_minx);
%         [ampy,phasey,freqy]=analyse_frespec(frespecy_hpf,nfreq,freq,sigma,amp_miny);
%     else
%         [ampx,phasex,freqx]=analyse_frespec(frespecx_all,nfreq,freq,sigma,amp_minx);
%         [ampy,phasey,freqy]=analyse_frespec(frespecy_all,nfreq,freq,sigma,amp_miny);
%     end    
    [ampx,phasex,freqx]=analyse_frespec(frespecx_all,nfreq,freq,sigma,amp_minx);
    [ampy,phasey,freqy]=analyse_frespec(frespecy_all,nfreq,freq,sigma,amp_miny);
    nfoundx=sum(~isnan(ampx));
    nfoundy=sum(~isnan(ampy));    
    if (nfoundx+nfoundy)>nbpm_min
        resonnances{end+1}.name='Q_{';
        if(coeffs(i,1)~=0)
            if(coeffs(i,1)==1)
                resonnances{end}.name=[resonnances{end}.name 'x'];
            else
                resonnances{end}.name=[resonnances{end}.name sprintf('%ix',coeffs(i,1))];
            end
        end
        if(coeffs(i,2)~=0)
            if(coeffs(i,2)==1)
                resonnances{end}.name=[resonnances{end}.name 'y'];
            elseif(coeffs(i,2)==-1)
                resonnances{end}.name=[resonnances{end}.name '-y'];
            else
                resonnances{end}.name=[resonnances{end}.name sprintf('%iy',coeffs(i,2))];
            end
        end
        if(coeffs(i,3)~=0)
            if(coeffs(i,3)==1)
                resonnances{end}.name=[resonnances{end}.name 's'];
            elseif(coeffs(i,3)==-1)
                resonnances{end}.name=[resonnances{end}.name '-s'];
            else
                resonnances{end}.name=[resonnances{end}.name sprintf('%is',coeffs(i,3))];
            end
        end
        resonnances{end}.name=[resonnances{end}.name '}'];
        resonnances{end}.f=freq;
        resonnances{end}.coeff=coeffs(i,:);
        resonnances{end}.ampx=ampx;
        resonnances{end}.ampy=ampy;
        resonnances{end}.phasex=phasex;
        resonnances{end}.phasey=phasey;
        resonnances{end}.freqx=freqx;
        resonnances{end}.freqy=freqy;
        resonnances{end}.nfoundx=nfoundx;
        resonnances{end}.nfoundy=nfoundy;
        fprintf('%s resonnance found at %.4f in %i X bpms and %i Y bpms\n',resonnances{end}.name,freq,nfoundx,nfoundy)
    end
end

%get usefull resonances
nresonnance=length(resonnances);
for i=1:nresonnance
    if all(resonnances{i}.coeff==[1 0 0])
        amp_10x=resonnances{i}.ampx;
        phase_10x=resonnances{i}.phasex;
    end
    if all(resonnances{i}.coeff==[0 1 0])
        phase_01y=resonnances{i}.phasey;
        amp_01y=resonnances{i}.ampy;
    end
    if all(resonnances{i}.coeff==[1 0 1])
        phase_Qxs=resonnances{i}.phasex;
        amp_Qxs=resonnances{i}.ampx;
    end
    if all(resonnances{i}.coeff==[1 0 -1])
        phase_Qxms=resonnances{i}.phasex;
        amp_Qxms=resonnances{i}.ampx;
    end
    if all(resonnances{i}.coeff==[1 0 2])
        phase_Qx2s=resonnances{i}.phasex;
        amp_Qx2s=resonnances{i}.ampx;
    end
    if all(resonnances{i}.coeff==[1 0 -2])
        phase_Qxm2s=resonnances{i}.phasex;
        amp_Qxm2s=resonnances{i}.ampx;
    end
    if all(resonnances{i}.coeff==[0 0 1])
        phase_Qs=resonnances{i}.phasex;
        amp_Qs=resonnances{i}.ampx;
        changesign=mod(phase_Qs+pi,2*pi)-pi>0;
        phase_Qs(changesign)=mod(phase_Qs(changesign)+pi,2*pi);
        amp_Qs(changesign)=-amp_Qs(changesign);
        phase_Qsy=resonnances{i}.phasey;
        amp_Qsy=resonnances{i}.ampy;
        changesign=mod(phase_Qsy+pi,2*pi)-pi>0;
        phase_Qsy(changesign)=mod(phase_Qsy(changesign)+pi,2*pi);
        amp_Qsy(changesign)=-amp_Qsy(changesign);
    end
    if all(resonnances{i}.coeff==[0 1 1])
        phase_Qys=resonnances{i}.phasey;
        amp_Qys=resonnances{i}.ampy;
    end
    if all(resonnances{i}.coeff==[0 1 -1])
        phase_Qyms=resonnances{i}.phasey;
        amp_Qyms=resonnances{i}.ampy;
    end
    if all(resonnances{i}.coeff==[0 1 2])
        phase_Qy2s=resonnances{i}.phasey;
        amp_Qy2s=resonnances{i}.ampy;
    end
    if all(resonnances{i}.coeff==[0 1 -2])
        phase_Qym2s=resonnances{i}.phasey;
        amp_Qym2s=resonnances{i}.ampy;
    end
end


% figure(1)
% ymax=1;
% ymin=1e-6;
% subplot(2,2,1)
% plot(xdata)
% xlabel('turn')
% ylabel('X [m]')
% title('before filtering')
% subplot(2,2,2)
% semilogy(fftshift(frange),fftshift(abs(fftx)))
% set(line([-freqmin freqmin;-freqmin freqmin],[ymin ymin; ymax ymax]),'color','k')
% ylim([ymin ymax])
% xlabel('frequency')
% ylabel('Amplitude [m/Hz]')
% title('before filtering')
% subplot(2,2,3)
% plot(xdata_hpf)
% xlabel('turn')
% ylabel('X [m]')
% title('after filtering')
% subplot(2,2,4)
% semilogy(fftshift(frange),fftshift(abs(fft(xdata_hpf))))
% set(line([Qx_hpf -Qx_hpf Qy_hpf -Qy_hpf;Qx_hpf -Qx_hpf Qy_hpf -Qy_hpf],[ymin ymin ymin ymin; ymax ymax ymax ymax ]),'color','k')
% ylim([ymin ymax])
% xlabel('frequency')
% ylabel('Amplitude [m/Hz]')
% title('after filtering')
% enhance_plot()

% figure(2)
% subplot(2,2,1)
% plot(ydata)
% subplot(2,2,2)
% semilogy(fftshift(frange),fftshift(abs(ffty)))
% set(line([-freqmin freqmin;-freqmin freqmin],[ymin ymin ; ymax ymax ]),'color','k')
% ylim([ymin ymax])
% subplot(2,2,3)
% plot(ydata_hpf)
% % legend(bpms)
% subplot(2,2,4)
% semilogy(fftshift(frange),fftshift(abs(ffty_hpf)))
% set(line([Qx_hpf -Qx_hpf Qy_hpf -Qy_hpf;Qx_hpf -Qx_hpf Qy_hpf -Qy_hpf],[ymin ymin ymin ymin; ymax ymax ymax ymax ]),'color','k')
% ylim([ymin ymax])
% enhance_plot()

figure(3)
clf
freqx=[];freqy=[];
ampx=[];ampy=[];
for i=1:nresonnance
    freqx(:,i)=resonnances{i}.freqx';
    ampx(:,i)=resonnances{i}.ampx';
    freqy(:,i)=resonnances{i}.freqy';
    ampy(:,i)=resonnances{i}.ampy';    
    legend_text{i}=sprintf('%s : %.3f (%iX %iY BPMs)',resonnances{i}.name, mod(resonnances{i}.f+.5,1)-.5,resonnances{i}.nfoundx,resonnances{i}.nfoundy);
end
% frespecx_all=[frespecx_hpf(abs(frespecx_hpf(:,3))>freqmin,:);frespecx_all(abs(frespecx_all(:,3))<freqmin,:)];
% frespecy_all=[frespecy_hpf(abs(frespecy_hpf(:,3))>freqmin,:);frespecy_all(abs(frespecy_all(:,3))<freqmin,:)];
subplot(2,1,1)
set(gca,'ColorOrder',hsv(nresonnance))
semilogy(abs(frespecx_all(:,3)),frespecx_all(:,4),'.k',abs(freqx),ampx,'+')
xlabel('f')
ylabel('amp [m/Hz]')
title('pics found');
xlim([0 .5])
ylim([1e-7 1e-3])
legend_ok=(any(~isnan(ampx)));
legend(cat(2,'all',legend_text(legend_ok)),'Location','NorthEastOutside')
for i=1:nresonnance
    if(legend_ok(i))
        text(abs(mod(resonnances{i}.f+.5,1)-.5),max(resonnances{i}.ampx),resonnances{i}.name);
    end
end
ylim([1e-6 1e-3])
subplot(2,1,2)
set(gca,'ColorOrder',hsv(nresonnance))
semilogy(abs(frespecy_all(:,3)),frespecy_all(:,4),'.k',abs(freqy),ampy,'+')
xlabel('f')
ylabel('amp [m/Hz]')
title('pics found');
xlim([0 .5])
ylim([1e-7 1e-3])
legend_ok=(any(~isnan(ampy)));
legend(cat(2,'all',legend_text(legend_ok)),'Location','NorthEastOutside')
for i=1:nresonnance
    if(legend_ok(i))
        text(abs(mod(resonnances{i}.f+.5,1)-.5),max(resonnances{i}.ampy),resonnances{i}.name);
    end
end
ylim([1e-6 1e-3])
enhance_plot()


% figure(4)
% frange2=0:sigma:.5;
% subplot(2,1,1)
% counts_noise=histc(freq_noisex(:),frange2);
% counts=[];
% legend_text={};
% for i=1:nresonnance
%     counts(:,i)=histc(resonnances{i}.freqx',frange2);
%     legend_text{i}=sprintf('%s : %.3f %i bpms',...
%                             resonnances{i}.name,...
%                             mod(resonnances{i}.f+.5,1)-.5,...
%                             sum(~isnan([resonnances{i}.freqx resonnances{i}.freqy])));
% end
% set(bar(frange2,counts_noise,'w'),'EdgeColor','k')
% hold on
% set(bar(frange2,counts,'stacked'),'EdgeColor','none');
% hold off
% xlim([0 0.5])
% xlabel('f')
% ylabel(sprintf('# of pics (%i bpms)',ngoodbpmx))
% legend(cat(2,'all',legend_text),'Location','NorthEastOutside')
% title('Resonnances in the horizontal plane');
% subplot(2,1,2)
% counts_noise=histc(freq_noisey(:),frange2);
% counts=[];
% for i=1:nresonnance
%     counts(:,i)=histc(resonnances{i}.freqy',frange2);
%     legend_text{i}=sprintf('%s : %.3f %i bpms',...
%                             resonnances{i}.name,...
%                             mod(resonnances{i}.f+.5,1)-.5,...
%                             sum(~isnan([resonnances{i}.freqx resonnances{i}.freqy])));
% end
% set(bar(frange2,counts_noise,'w'),'EdgeColor','k')
% hold on
% set(bar(frange2,counts,'stacked'),'EdgeColor','none');
% hold off
% xlim([0 0.5])
% xlabel('f')
% ylabel(sprintf('# of pics (%i bpms)',ngoodbpmy))
% legend(cat(2,'all',legend_text),'Location','NorthEastOutside')
% title('Resonnances in the vertical plane');
% enhance_plot()

figure(5)
clf
factorx=mean(amp_10x(~isnan(amp_10x)).^2)/mean(betax_model);
factory=mean(amp_01y(~isnan(amp_01y)).^2)/mean(betay_model);
subplot(2,1,1)
plot(bpmsx_S,(amp_10x).^2/factorx,'+-',bpmsx_S,betax_model,'-')
xlabel('bpm')
ylabel('\beta_x [m]')
legend(sprintf('meas. (amp^2/%.3g)',factorx),'model')
subplot(2,1,2)
bpm0=find(~isnan(phase_10x));
phase0x=phase_10x(bpm0(1))-phasex_model(bpm0(1))*2*pi;
plot(bpmsx_S,mod(phase_10x-phase0x,2*pi)-pi,'+-',...
     bpmsx_S,mod(phasex_model*2*pi,2*pi)-pi,'-',...
     bpmsx_S,mod(phase_10x-phase0x-phasex_model*2*pi+pi,2*pi)-pi,'+-')
xlabel('bpm')
ylabel('\nu_x')
ylim([-pi pi])
legend('meas.','model','diff','Location','NorthEast')
enhance_plot()

figure(6)
clf
subplot(2,1,1)
plot(bpmsy_S,(amp_01y).^2/factory,'+-',bpmsy_S,betay_model,'-')
xlabel('bpm')
ylabel('\beta_y [m]')
legend(sprintf('meas. (amp^2/%.3g)',factory),'model')
subplot(2,1,2)
bpm0=find(~isnan(phase_01y));
phase0y=phase_01y(bpm0(1))-phasey_model(bpm0(1))*2*pi;
plot(bpmsy_S,mod(phase_01y-phase0y,2*pi)-pi,'+-',...
     bpmsy_S,mod(phasey_model*2*pi,2*pi)-pi,'-',...
     bpmsy_S,mod(phase_01y-phase0y-phasey_model*2*pi+pi,2*pi)-pi,'+-')
xlabel('bpm')
ylabel('\nu_y')
ylim([-pi pi])
legend('meas.','model','diff','Location','NorthEast')
enhance_plot()

figure(7)
clf
orderQs_x=[];
amprQs_x=[];
damprQs_x=[];

if(exist('amp_Qxs','var'))
    ok_Qxs=find(~isnan(amp_Qxs) & ~isnan(amp_10x));
    factor_Qxs= amp_10x(ok_Qxs)/amp_Qxs(ok_Qxs);
    dfactor_Qxs=sqrt(sum((amp_10x(ok_Qxs)/factor_Qxs-amp_Qxs(ok_Qxs)).^2))/sqrt(sum((amp_Qxs(ok_Qxs)).^2));
    bpm0=find(~isnan(phase_10x) & ~isnan(phase_Qxs));
    phase0Qxs=phase_Qxs(bpm0(1))-phase_10x(bpm0(1));
    orderQs_x(end+1)=1;
    amprQs_x(end+1)=1/factor_Qxs;
    damprQs_x(end+1)=amprQs_x(end)*dfactor_Qxs;
end
if(exist('amp_Qx2s','var'))
    ok_Qx2s=find(~isnan(amp_Qx2s) & ~isnan(amp_10x));
    factor_Qx2s= amp_10x(ok_Qx2s)/amp_Qx2s(ok_Qx2s);
    dfactor_Qx2s=sqrt(sum((amp_10x(ok_Qx2s)/factor_Qx2s-amp_Qx2s(ok_Qx2s)).^2))/sqrt(sum((amp_Qx2s(ok_Qx2s)).^2));
    bpm0=find(~isnan(phase_10x) & ~isnan(phase_Qx2s));
    phase0Qx2s=phase_Qx2s(bpm0(1))-phase_10x(bpm0(1));
    orderQs_x(end+1)=2;
    amprQs_x(end+1)=1/factor_Qx2s;
    damprQs_x(end+1)=amprQs_x(end)*dfactor_Qx2s;
end
if(exist('amp_Qxms','var'))
    ok_Qxms=find(~isnan(amp_Qxms) & ~isnan(amp_10x));
    factor_Qxms= amp_10x(ok_Qxms)/amp_Qxms(ok_Qxms);
    dfactor_Qxms=sqrt(sum((amp_10x(ok_Qxms)/factor_Qxms-amp_Qxms(ok_Qxms)).^2))/sqrt(sum((amp_Qxms(ok_Qxms)).^2));
    bpm0=find(~isnan(phase_10x) & ~isnan(phase_Qxms));
    phase0Qxms=phase_Qxms(bpm0(1))-phase_10x(bpm0(1));
    orderQs_x(end+1)=-1;
    amprQs_x(end+1)=1/factor_Qxms;
    damprQs_x(end+1)=amprQs_x(end)*dfactor_Qxms;
end
if(exist('amp_Qxm2s','var'))
    ok_Qxm2s=find(~isnan(amp_Qxm2s) & ~isnan(amp_10x));
    factor_Qxm2s= amp_10x(ok_Qxm2s)/amp_Qxm2s(ok_Qxm2s);
    dfactor_Qxm2s=sqrt(sum((amp_10x(ok_Qxm2s)/factor_Qxm2s-amp_Qxm2s(ok_Qxm2s)).^2))/sqrt(sum((amp_Qxm2s(ok_Qxm2s)).^2));
    bpm0=find(~isnan(phase_10x) & ~isnan(phase_Qxm2s));
    phase0Qxm2s=phase_Qxm2s(bpm0(1))-phase_10x(bpm0(1));
    orderQs_x(end+1)=-2;
    amprQs_x(end+1)=1/factor_Qxm2s;
    damprQs_x(end+1)=amprQs_x(end)*dfactor_Qxm2s;
end
subplot(2,1,1)
plot(bpmsx_S,amp_10x,'k+-')
legend_txt{1}='amp(Q_x)';
hold on
if(exist('amp_Qxs','var'))
    plot(bpmsx_S,amp_Qxs*factor_Qxs,'b+-')
    legend_txt{end+1}=sprintf('%.3g*amp(Q_{xs})',factor_Qxs);
end
if(exist('amp_Qx2s','var'))
    plot(bpmsx_S,amp_Qx2s*factor_Qx2s,'g+-')
    legend_txt{end+1}=sprintf('%.3g*amp(Q_{x2s})',factor_Qx2s);
end
if(exist('amp_Qxms','var'))
    plot(bpmsx_S,amp_Qxms*factor_Qxms,'r+-')
    legend_txt{end+1}=sprintf('%.3g*amp(Q_{x-s})',factor_Qxms);
end
if(exist('amp_Qxm2s','var'))
    plot(bpmsx_S,amp_Qxm2s*factor_Qxm2s,'c+-')
    legend_txt{end+1}=sprintf('%.3g*amp(Q_{x-2s})',factor_Qxm2s);
end
xlabel('bpm')
ylabel('amp [m/Hz]')
legend(legend_txt,'Location','NorthEastOutside')
subplot(2,1,2)
legend_txt={};
hold on
if(exist('amp_Qxs','var'))
    plot(bpmsx_S,mod(phase_Qxs-phase_10x+pi,2*pi)-pi,'b+-')
    legend_txt{end+1}='\phi(Q_{xs}-\phi(Q_x))';
end
if(exist('amp_Qx2s','var'))
    plot(bpmsx_S,mod(phase_Qx2s-phase_10x+pi,2*pi)-pi,'g+-')
    legend_txt{end+1}='\phi(Q_{x2s}-\phi(Q_x))';
end
if(exist('amp_Qxms','var'))
    plot(bpmsx_S,mod(phase_Qxms-phase_10x+pi,2*pi)-pi,'r+-')
    legend_txt{end+1}='\phi(Q_{x-s})-\phi(Q_x)';
end
if(exist('amp_Qxm2s','var'))
    plot(bpmsx_S,mod(phase_Qxm2s-phase_10x+pi,2*pi)-pi,'c+-')
    legend_txt{end+1}='\phi(Q_{x-2s})-\phi(Q_x)';
end
hold off
% set(line([0 0;nbpm nbpm],[0 pi;0 pi]),'color','k')
ylim([-pi pi])
xlabel('bpm')
ylabel('phase')
legend(legend_txt,'Location','NorthEastOutside')
enhance_plot()

figure(8)
clf
orderQs_y=[];
amprQs_y=[];
damprQs_y=[];

if(exist('amp_Qys','var'))
    ok_Qys=find(~isnan(amp_Qys) & ~isnan(amp_01y));
    factor_Qys= amp_01y(ok_Qys)/amp_Qys(ok_Qys);
    dfactor_Qys=sqrt(sum((amp_01y(ok_Qys)/factor_Qys-amp_Qys(ok_Qys)).^2))/sqrt(sum((amp_Qys(ok_Qys)).^2));
    bpm0=find(~isnan(phase_01y) & ~isnan(phase_Qys));
    phase0Qys=phase_Qys(bpm0(1))-phase_01y(bpm0(1));
    orderQs_y(end+1)=1;
    amprQs_y(end+1)=1/factor_Qys;
    damprQs_y(end+1)=amprQs_y(end)*dfactor_Qys;
end
if(exist('amp_Qy2s','var'))
    ok_Qy2s=find(~isnan(amp_Qy2s) & ~isnan(amp_01y));
    factor_Qy2s= amp_01y(ok_Qy2s)/amp_Qy2s(ok_Qy2s);
    dfactor_Qy2s=sqrt(sum((amp_01y(ok_Qy2s)/factor_Qy2s-amp_Qy2s(ok_Qy2s)).^2))/sqrt(sum((amp_Qy2s(ok_Qy2s)).^2));
    bpm0=find(~isnan(phase_01y) & ~isnan(phase_Qy2s));
    phase0Qy2s=phase_Qy2s(bpm0(1))-phase_01y(bpm0(1));
    orderQs_y(end+1)=2;
    amprQs_y(end+1)=1/factor_Qy2s;
    damprQs_y(end+1)=amprQs_y(end)*dfactor_Qy2s;
end
if(exist('amp_Qyms','var'))
    ok_Qyms=find(~isnan(amp_Qyms) & ~isnan(amp_01y));
    factor_Qyms= amp_01y(ok_Qyms)/amp_Qyms(ok_Qyms);
    dfactor_Qyms=sqrt(sum((amp_01y(ok_Qyms)/factor_Qyms-amp_Qyms(ok_Qyms)).^2))/sqrt(sum((amp_Qyms(ok_Qyms)).^2));
    bpm0=find(~isnan(phase_01y) & ~isnan(phase_Qyms));
    phase0Qyms=phase_Qyms(bpm0(1))-phase_01y(bpm0(1));
    orderQs_y(end+1)=-1;
    amprQs_y(end+1)=1/factor_Qyms;
    damprQs_y(end+1)=amprQs_y(end)*dfactor_Qyms;
end
if(exist('amp_Qym2s','var'))
    ok_Qym2s=find(~isnan(amp_Qym2s) & ~isnan(amp_01y));
    factor_Qym2s= amp_01y(ok_Qym2s)/amp_Qym2s(ok_Qym2s);
    dfactor_Qym2s=sqrt(sum((amp_01y(ok_Qym2s)/factor_Qym2s-amp_Qym2s(ok_Qym2s)).^2))/sqrt(sum((amp_Qym2s(ok_Qym2s)).^2));
    bpm0=find(~isnan(phase_01y) & ~isnan(phase_Qym2s));
    phase0Qym2s=phase_Qym2s(bpm0(1))-phase_01y(bpm0(1));
    orderQs_y(end+1)=-2;
    amprQs_y(end+1)=1/factor_Qym2s;
    damprQs_y(end+1)=amprQs_y(end)*dfactor_Qym2s;
end
subplot(2,1,1)
plot(bpmsy_S,amp_01y,'k+-')
legend_txt={};
legend_txt{1}='amp(Q_y)';
hold on
if(exist('amp_Qys','var'))
    plot(bpmsy_S,amp_Qys*factor_Qys,'b+-')
    legend_txt{end+1}=sprintf('%.3g*amp(Q_{ys})',factor_Qys);
end
if(exist('amp_Qy2s','var'))
    plot(bpmsy_S,amp_Qy2s*factor_Qy2s,'g+-')
    legend_txt{end+1}=sprintf('%.3g*amp(Q_{y2s})',factor_Qy2s);
end
if(exist('amp_Qyms','var'))
    plot(bpmsy_S,amp_Qyms*factor_Qyms,'r+-')
    legend_txt{end+1}=sprintf('%.3g*amp(Q_{y-s})',factor_Qyms);
end
if(exist('amp_Qym2s','var'))
    plot(bpmsy_S,amp_Qym2s*factor_Qym2s,'c+-')
    legend_txt{end+1}=sprintf('%.3g*amp(Q_{y-2s})',factor_Qym2s);
end
xlabel('bpm')
ylabel('amp [m/Hz]')
legend(legend_txt,'Location','NorthEastOutside')
subplot(2,1,2)
legend_txt={};
hold on
if(exist('amp_Qys','var'))
    plot(bpmsy_S,mod(phase_Qys-phase_01y+pi,2*pi)-pi,'b+-')
    legend_txt{end+1}='\phi(Q_{ys}-\phi(Q_y))';
end
if(exist('amp_Qy2s','var'))
    plot(bpmsy_S,mod(phase_Qy2s-phase_01y+pi,2*pi)-pi,'g+-')
    legend_txt{end+1}='\phi(Q_{y2s}-\phi(Q_y))';
end
if(exist('amp_Qyms','var'))
    plot(bpmsy_S,mod(phase_Qyms-phase_01y+pi,2*pi)-pi,'r+-')
    legend_txt{end+1}='\phi(Q_{y-s})-\phi(Q_y)';
end
if(exist('amp_Qym2s','var'))
    plot(bpmsy_S,mod(phase_Qym2s-phase_01y+pi,2*pi)-pi,'c+-')
    legend_txt{end+1}='\phi(Q_{y-2s})-\phi(Q_y)';
end
hold off
% set(line([0 0;nbpm nbpm],[0 pi;0 pi]),'color','k')
ylim([-pi pi])
xlabel('bpm')
ylabel('phase')
legend(legend_txt,'Location','NorthEastOutside')
enhance_plot()

figure(9)
clf
%fit modulation_index mi, we have :
%besselj(orderQs_x,mi)/besselj(0,mi)=amprQs_x
f = fittype('A*abs(besselj(abs(x),mi))');
f2 = fittype('besselr(x,mi)');
ordersx=[];
ampsx=[];
phisx=[];
ordersy=[];
ampsy=[];
phisy=[];
ix=1;
iy=1;
for i=1:nresonnance
    if (all(resonnances{i}.coeff([1 2]) == [1 0]))
        ordersx(ix,1:nbpmx)=resonnances{i}.coeff(3);
        ampsx(ix,1:nbpmx)=resonnances{i}.ampx;
        ampsrx(ix,1:nbpmx)=resonnances{i}.ampx;
        phisx(ix,1:nbpmx)=mod(resonnances{i}.phasex,2*pi);
        ix=ix+1;
    elseif (all(resonnances{i}.coeff([1 2]) == [0 1]))
        ordersy(iy,1:nbpmy)=resonnances{i}.coeff(3);
        ampsy(iy,1:nbpmy)=resonnances{i}.ampy;
        ampsry(iy,1:nbpmy)=resonnances{i}.ampy;
        phisy(iy,1:nbpmy)=mod(resonnances{i}.phasey,2*pi);
        iy=iy+1;
    end
end
for i=1:nbpmx
    ampsrx(:,i)=ampsrx(:,i)/sqrt(sum(ampsrx(~isnan(ampsrx(:,i)),i).^2));
end
for i=1:nbpmy
    ampsry(:,i)=ampsry(:,i)/sqrt(sum(ampsry(~isnan(ampsry(:,i)),i).^2));
end
mix=[];
mix2=[];
Ax=[];
miy=[];
miy2=[];
Ay=[];
for i=1:nbpmx
    okx=~isnan(ampsrx(:,i));
    if(sum(okx)>=2)
        rx=fit(ordersx(okx,i),ampsrx(okx,i),f2,fitoptions('Method','NonlinearLeastSquares',...
                    'StartPoint',0,'Algorithm','Levenberg-Marquardt','TolFun',1e-20,'TolX',1e-20,...
                    'DiffMinChange',1e0,'DiffMaxChange',1e3));
        mix(i)=rx.mi;
    else
        mix(i)=NaN;
    end
end
for i=1:nbpmy
    oky=~isnan(ampsry(:,i));
    if(sum(oky)>=2)
        ry=fit(ordersy(oky,i),ampsry(oky,i),f2,fitoptions('Method','NonlinearLeastSquares',...
                    'StartPoint',0,'Algorithm','Levenberg-Marquardt','TolFun',1e-20,'TolX',1e-20,...
                    'DiffMinChange',1e0,'DiffMaxChange',1e3));
        miy(i)=ry.mi;
    else
        miy(i)=NaN;
    end
end
subplot(2,1,1)
x=min(ordersx)-.1:.01:max(ordersx)+.1;
hold on
for i=1:nbpmx
    plot(x,besselr(x,mix(i)),'r-');
end
plot(ordersx(:),ampsrx(:),'k+')
hold off
legend(sprintf('dQx=%.2f+-%.2f',mean(mix(~isnan(mix))),std(mix(~isnan(mix)))))
xlim([min(x) max(x)])
subplot(2,1,2)
x=min(ordersy)*1.1:.01:max(ordersy)*1.1;
hold on
for i=1:nbpmy
    plot(x,besselr(x,miy(i)),'r-');
end
plot(ordersy(:),ampsry(:),'k+')
hold off
legend(sprintf('dQy=%.2f+-%.2f',mean(miy(~isnan(miy))),std(miy(~isnan(miy)))))
if all(isfinite([min(x) max(x)])) && min(x)<max(x) 
    xlim([min(x) max(x)])
end

figure('Units','pixels','Position',[0,30,800,400],'PaperUnits','points','PaperSize',[800 400]);
clf
ok_Qs=find(~isnan(amp_Qs));
factor_Qs= Dx_model(ok_Qs)/(amp_Qs(ok_Qs));
% subplot(2,1,1)
legend_txt={};
hold('on')
if(exist('amp_Qs','var'))
    plot(bpmsx_S,amp_Qs*factor_Qs,'+-r')
    legend_txt{end+1}=sprintf('Amp(Q_s)*%3.f',factor_Qs);
end
if(exist('amp_Qsy','var'))
    plot(bpmsy_S,amp_Qsy*factor_Qs,'+-b')
    legend_txt{end+1}=sprintf('Amp(Q_{sy})*%3.f',factor_Qs);
end
plot(bpmsx_S,Dx_model,'k--')
legend_txt{end+1}='D_xmodel';
hold off
ylim('auto')
xlabel('bpm')
ylabel('Dispersion [m]')
legend(legend_txt,'Location','NorthEastOutside')
% subplot(2,1,2)
% legend_txt={};
% hold('on')
% if(exist('amp_Qs','var'))
%     bpm0=find(~isnan(amp_Qs));
% %     phase0_Qs=phase_Qs(bpm0(1));
%     phase0_Qs=0;
% %     plot(1:nbpm,mod(phase_Qs-phase0_Qs+pi,2*pi)-pi,'*-r')
%     plot(1:nbpm,mod(phase_Qs-phase0_Qs,2*pi)-pi,'+-r')
%     legend_txt{end+1}='\phi(Qs)-\pi';
% end
% if(exist('amp_Qsy','var'))
%     bpm0=find(~isnan(amp_Qsy));
% %     phase0_Qsy=phase_Qsy(bpm0(1));
%     phase0_Qsy=0;
% %     plot(1:nbpm,mod(phase_Qsy-phase0_Qsy+pi,2*pi)-pi,'*-r')
%     plot(1:nbpm,mod(phase_Qsy-phase0_Qsy,2*pi)-pi,'+-b')
%     legend_txt{end+1}='\phi(Qs)-\pi in Y';
% end
% legend(legend_txt,'Location','NorthEastOutside')
% set(line([0 nbpm],[0 0]),'color','k')
% ylim([-pi pi])
% hold off
% xlabel('bpm')
% ylabel('Phase')
enhance_plot()

end

