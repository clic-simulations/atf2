global BEAMLINE

[x,y,tmit]=acquire_TBT(1,512,10);
[Qx,betax,phasex,factor_betax,goodbpmx,dQx,dbetax,dphasex,dfactor_betax]=analyse_TBT(x,4e-2,betax_model);
[Qy,betay,phasey,factor_betay,goodbpmy,dQy,dbetay,dphasey,dfactor_betay]=analyse_TBT(y,4e-2,betay_model);

index_bpm=zeros(1,96);
for i=1:96
  index_bpm(i)=findcells(BEAMLINE,'Name',sprintf('MB%iR',i));
end
XSR=findcells(BEAMLINE,'Name','XSR');

goodbpm=goodbpmx&goodbpmy;
fitbeta(betax(goodbpm),dbetax(goodbpm),betay(goodbpm),dbetay(goodbpm),index_bpm(goodbpm),XSR);