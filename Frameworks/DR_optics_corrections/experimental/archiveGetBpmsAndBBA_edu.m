function [read_x, read_y, bpm_name]=archiveGetBpmsAndBBA

t0=datenum('Dec-07-2012 02:40:00');
t1=datenum('Dec-07-2012 03:05:00');

addpath('~/ATF2/control-software/epics-3.14.8/extensions/src/ChannelArchiver/Matlab');
addpath('~/ATF2/control-software/epics-3.14.8/extensions/src/ChannelArchiver/Matlab/util');
url='http://localhost/archive/cgi/ArchiveDataServer.cgi';
key=1;
global is_matlab BEAMLINE INSTR
is_matlab=1;
% ml_arch_info(url);
load ../latticeFiles/ATF2lat
I_cut=200;
nread=round(3600*24*(t1-t0));

%get BPMs info
bpmind=findcells(INSTR,'Type','ccav')
%bpmind=findcells(INSTR,'Type','stripline')
bpmind=arrayfun(@(x) INSTR{x}.Index,bpmind)
%bpmind=bpmind(bpmind>findcells(BEAMLINE,'Name','IEX'))
%bpmind=bpmind(bpmind<findcells(BEAMLINE,'Name','MFB1FF'));
bpmind=bpmind(bpmind<findcells(BEAMLINE,'Name','MPREIP'));
for i=1:length(bpmind)
  bpm_name{i}=BEAMLINE{bpmind(i)}.Name;
  bpmx_pv{i}=[bpm_name{i}(2:end) 'x:pos'];
  bpmy_pv{i}=[bpm_name{i}(2:end) 'y:pos'];
  bbax_pv{i}=[bpm_name{i}(2:end) 'x:bbaOffset'];
  bbay_pv{i}=[bpm_name{i}(2:end) 'y:bbaOffset'];
end
BPMS_edu=bpm_name
%get BPM and intensity readings
nbpm=length(bpm_name);
[~,read]=ml_arch_get_more(url, key,{'REFC1:amp'},t0, t1, 0, nread);
read_I=read(:,1);
for ibpm=1:nbpm
  [times,read]=ml_arch_get_more(url, key,[bpmx_pv(ibpm);...
    bpmy_pv(ibpm); ],t0, t1, 0, nread);
  read_x(:,ibpm)=read(:,1).*1e-6;
  read_y(:,ibpm)=read(:,2).*1e-6;
  nread=size(read_I,1);
  [~,read]=ml_arch_get_more(url, key,[bbax_pv(ibpm);...
    bbay_pv(ibpm) ],t0, t1, 0, nread);
  read_xbba(1,ibpm)=read(end,1)*1e-6;
  read_ybba(1,ibpm)=read(end,2)*1e-6;
end
  
%remove BPM reading with low intensity
if any(read_I<I_cut)
  fprintf('intensity cut: remove %i/%i readings\n',sum(read_I<I_cut),nread);
end
read_x(read_I<I_cut,:)=NaN;
read_y(read_I<I_cut,:)=NaN;

% BBA is subtracted from BPM readings, add back to get BPM reading wrt
% center of BPM
read_x=read_x+repmat(read_xbba,nread,1);
read_y=read_y+repmat(read_ybba,nread,1);

% Set date format
if(t1-t0>365)
  datefmt='dd-mmm-yyyy HH:MM';
elseif(t1-t0>1)
  datefmt='dd-mmm HH:MM';
else
  datefmt='HH:MM';
end 

%plot corrected BPM readings
figure(1)
subplot(2,1,1)
plot(times,read_x-repmat(mean(read_x(read_I>I_cut,:)),nread,1),'-')
title('corrected BPM readings')
legend(bpm_name)
xlim([t0 t1])
set(gca,'Xtick',linspace(t0, t1,10));
datetick('x',datefmt,'keeplimits','keepticks') 
ylabel('X [m]')
subplot(2,1,2)
plot(times,read_y-repmat(mean(read_y(read_I>I_cut,:)),nread,1),'-')
xlim([t0 t1])
set(gca,'Xtick',linspace(t0, t1,10));
datetick('x',datefmt,'keeplimits','keepticks')   
ylabel('Y [m]')
read_x2=read_x-repmat(mean(read_x(read_I>I_cut,:)),nread,1);
read_y2=read_y-repmat(mean(read_y(read_I>I_cut,:)),nread,1);
%figure(2)
%subplot(2,1,1)
%plot(times,read_x,'-')
%title('corrected BPM readings')
% legend(bpm_name)
% xlim([t0 t1])
% set(gca,'Xtick',linspace(t0, t1,10));
% datetick('x',datefmt,'keeplimits','keepticks') 
% ylabel('X [m]')
% subplot(2,1,2)
% plot(times,read_y,'-')
% xlim([t0 t1])
% set(gca,'Xtick',linspace(t0, t1,10));
% datetick('x',datefmt,'keeplimits','keepticks')   
% ylabel('Y [m]')
%save tempx_edu.dat read_x /ascii
save tempy_edu.dat read_y /ascii

end

function [t,r]=ml_arch_get_more(url, key, name, t0, t1, how, nread)
  npv=length(name);
    [data{1:npv}]=ArchiveData(url, 'values',key, name, t0, t1, nread,how);
    for i=1:npv
      if exist('t_all','var') && size(t_all,1)>size(data{i},2)
        n=size(data{i},2);
        t_all=t_all(1:n,:);
        r=r(1:n,:);
      elseif exist('t_all','var') && size(t_all,1)<size(data{i},2)
        n=size(data{i},2)-size(t_all,1);
        t_all=[t_all;zeros(n,size(t_all,2))];
        r=[r;zeros(n,size(r,2))];
      end
      t_all(:,i)=data{i}(1,:)';
      r(:,i)=data{i}(3,:)';
    end
  t=t_all(:,1);
  r=r(:,:);
end