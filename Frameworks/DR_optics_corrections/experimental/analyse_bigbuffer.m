function analyse_bigbuffer(filename)

load(filename);

FL.HwInfo=FL_HwInfo;
FL.SimModel=FL_SimModel;

instr_index=[];
beamline_index=[];
bpm_name={};
bpm_s=[];
for instr=1:length(INSTR)
  if ~isempty(FL.HwInfo.INSTR(instr))
    if ~isempty(FL.HwInfo.INSTR(instr).pvname)
      if strcmp(INSTR{instr}.Class,'MONI')
        if ~strcmp(INSTR{instr}.Type,'button')
          instr_index(end+1)=instr;
          beamline_index(end+1)=INSTR{instr_index(end)}.Index;
          bpm_name{end+1}=BEAMLINE{beamline_index(end)}.Name;
          bpm_s(end+1)=BEAMLINE{beamline_index(end)}.S;
        end
      end
    end
  end
end

nbpm=length(bpm_s);
%INTENSITY CUTS
npulse=size(xread,2);
% cut for I<1e8 and I>1e10
tmit_ok1=all( (tmit(instr_index,:)>1e8) & (tmit(instr_index,:)<1e13) );
% cut for tmit < 1/3 * mean tmit
tmit_ok2=all(tmit(instr_index,:)>(mean(tmit(instr_index,:),2)/3*ones(1,npulse)));
tmit_ok=find(tmit_ok1 & tmit_ok2);
npulse=length(tmit_ok);

%READING CUTS
dx=xread(instr_index,tmit_ok)-mean(xread(instr_index,tmit_ok),2)*ones(1,npulse);
dy=yread(instr_index,tmit_ok)-mean(yread(instr_index,tmit_ok),2)*ones(1,npulse);
% cut for x-xmean>6*std(x-xmean)
readx_ok=all(abs(dx)./(std(dx,'',2)*ones(1,npulse))<6);
ready_ok=all(abs(dy)./(std(dy,'',2)*ones(1,npulse))<6);

%CREATE VARIABLES WITH CUT APPLIED
cut_ok=tmit_ok(readx_ok & ready_ok);
xread_cut=xread(instr_index,cut_ok);
yread_cut=yread(instr_index,cut_ok);
tmit_cut=tmit(instr_index,cut_ok);
npulse=length(cut_ok);
dxread_cut=xread_cut-mean(xread_cut,2)*ones(1,npulse);
dyread_cut=yread_cut-mean(yread_cut,2)*ones(1,npulse);

%SVD CUT (find in 5 first modes, the ones dominated by 1 BPM )
max_value=0.8;
old_nokx=-1;
bpm_svd_okx=ones(1,nbpm);
nokx=length(find(bpm_svd_okx));
while (old_nokx~=nokx)
  old_nokx=nokx;
  [ux,sx,vx]=svd(dxread_cut(find(bpm_svd_okx),:));
  bpm_svd_okx(find(bpm_svd_okx))=all(abs(ux(:,1:5))<max_value,2);
  fprintf('%i X bad bpms: ',length(find(~bpm_svd_okx)));
  fprintf('%s ',bpm_name{~bpm_svd_okx});fprintf('\n');
  nokx=length(find(bpm_svd_okx));
end
old_noky=-1;
bpm_svd_oky=ones(1,nbpm);
noky=length(find(bpm_svd_oky));
while (old_noky~=noky)
  old_noky=noky;
  [uy,sy,vy]=svd(dyread_cut(find(bpm_svd_oky),:));
  bpm_svd_oky(find(bpm_svd_oky))=all(abs(uy(:,1:5))<max_value,2);
  fprintf('%i Y bad bpms: ',length(find(~bpm_svd_oky)));
  fprintf('%s ',bpm_name{~bpm_svd_oky});fprintf('\n');
  noky=length(find(bpm_svd_oky));
end

%CREATE VARIABLES WITH CUT APPLIED
bpm_ok=find(bpm_svd_okx & bpm_svd_oky);
xread_cut=xread_cut(bpm_ok,:);
yread_cut=yread_cut(bpm_ok,:);
tmit_cut=tmit_cut(bpm_ok,:);
nbpm=length(bpm_ok);
dxread_cut=xread_cut-mean(xread_cut,2)*ones(1,npulse);
dyread_cut=yread_cut-mean(yread_cut,2)*ones(1,npulse);
instr_index=instr_index(bpm_ok);
beamline_index=beamline_index(bpm_ok);
bpm_name=bpm_name(bpm_ok);
bpm_s=bpm_s(bpm_ok);


%PLOT
figure(1)
subplot(3,1,1);
plot(bpm_s,xread_cut-mean(xread_cut,2)*ones(1,npulse));
ylim([-1 1]*4e-4) 
subplot(3,1,2);
plot(bpm_s,yread_cut-mean(yread_cut,2)*ones(1,npulse));
ylim([-1 1]*4e-4)
subplot(3,1,3);
plot(bpm_s,tmit_cut);
ylim([-0.1 1]*5e9)

figure(2)
[ux,sx,vx]=svd(dxread_cut);
[uy,sy,vy]=svd(dyread_cut);
subplot(3,1,1)
semilogy(1:size(sx,1),diag(sx),'+b',1:size(sy,1),diag(sy),'r+')
ylim([1e-6 1])
subplot(3,1,2)
plot(bpm_s,ux(:,1:3))
ylim([-1 1]*.8)
subplot(3,1,3)
plot(bpm_s,uy(:,1:3))
ylim([-1 1]*.8)
