function EXTorbit()
  global BEAMLINE;
  global INSTR;
  
  bpm_measured=99:157;
  bpm_fit=99:122;
  
  BH1X=load('bpmdata_bh1xscan');
  q=zeros(157,2);
  dq=zeros(157,2);
  BH1X_qxth=zeros(157,1);
  BH1X_index=findcells(BEAMLINE,'Name','BH1XB');
  for bpm=bpm_measured
    [q(bpm,:),dq(bpm,:)]=noplot_polyfit([366.441 366.541 366.641 366.741],...
      [BH1X.bpmdata_366p441{1}(1,bpm) BH1X.bpmdata_366p541{1}(1,bpm) BH1X.bpmdata_366p641{1}(1,bpm) BH1X.bpmdata_366p741{1}(1,bpm) ],...
      [BH1X.bpmdata_366p441{1}(4,bpm) BH1X.bpmdata_366p541{1}(4,bpm) BH1X.bpmdata_366p641{1}(4,bpm) BH1X.bpmdata_366p741{1}(4,bpm) ],1);
    if(BH1X_index<INSTR{bpm}.Index)
      [~,R]=RmatAtoB(BH1X_index,INSTR{bpm}.Index);
      BH1X_qxth(bpm)=R(1,2);
    end
    if(~isfinite(dq(bpm,2)))
      bpm_fit(bpm_fit==bpm)=[];
    end
  end
  BH1X_qx=q(:,2);
  BH1X_dqx=dq(:,2);
  [BH1X_conv,BH1X_dconv]=lscov(BH1X_qxth(bpm_fit),BH1X_qx(bpm_fit),BH1X_dqx(bpm_fit));
  figure(1)
  errorbar(bpm_measured,BH1X_qx(bpm_measured),BH1X_dqx(bpm_measured),'b')
  hold on
  plot(bpm_measured,BH1X_qxth(bpm_measured)*BH1X_conv,'r--')
  hold off
  xlabel('BPM #')
  title(sprintf('BH1X I to angle conv : %.1e+-%.0e rad.A^{-1}',BH1X_conv,BH1X_dconv))

  BH3X=load('bpmdata_bh3xscan');
  q=zeros(157,2);
  dq=zeros(157,2);
  BH3X_qxth=zeros(157,1);
  BH3X_index=findcells(BEAMLINE,'Name','BH3XB');
  for bpm=bpm_measured
    [q(bpm,:),dq(bpm,:)]=noplot_polyfit([376.07 376.17 376.27 376.37],...
      [BH3X.bpmdata_376p07{1}(1,bpm) BH3X.bpmdata_376p17{1}(1,bpm) BH3X.bpmdata_376p27{1}(1,bpm) BH3X.bpmdata_376p37{1}(1,bpm) ],...
      [BH3X.bpmdata_376p07{1}(4,bpm) BH3X.bpmdata_376p17{1}(4,bpm) BH3X.bpmdata_376p27{1}(4,bpm) BH3X.bpmdata_376p37{1}(4,bpm) ],1);
    if(BH3X_index<INSTR{bpm}.Index)
      [~,R]=RmatAtoB(BH3X_index,INSTR{bpm}.Index);
      BH3X_qxth(bpm)=R(1,2);
    end
    if(~isfinite(dq(bpm,2)))
      bpm_fit(bpm_fit==bpm)=[];
    end
  end
  BH3X_qx=q(:,2);
  BH3X_dqx=dq(:,2);  
  [BH3X_conv,BH3X_dconv]=lscov(BH3X_qxth(bpm_fit),BH3X_qx(bpm_fit),BH3X_dqx(bpm_fit));
  figure(2)
  errorbar(bpm_measured,BH3X_qx(bpm_measured),BH3X_dqx(bpm_measured),'b');
  hold on
  plot(bpm_measured,BH3X_qxth(bpm_measured)*BH3X_conv,'r--')
  hold off
  xlabel('BPM #')
  title(sprintf('BH3X I to angle conv : %.1e+-%.0e rad.A^{-1}',BH3X_conv,BH3X_dconv))
  
  BS3X=load('bpmdata_bs3xscan');
  q=zeros(157,2);
  dq=zeros(157,2);
  BS3X_qxth=zeros(157,1);
  BS3X_index=findcells(BEAMLINE,'Name','BS3XB');
  for bpm=bpm_measured
    [q(bpm,:),dq(bpm,:)]=noplot_polyfit([2599.53 2600.03 2600.53 2601.03],...
      [BS3X.bpmdata_2599p53{1}(1,bpm) BS3X.bpmdata_2600p03{1}(1,bpm) BS3X.bpmdata_2600p53{1}(1,bpm) BS3X.bpmdata_2601p03{1}(1,bpm) ],...
      [BS3X.bpmdata_2599p53{1}(4,bpm) BS3X.bpmdata_2600p03{1}(4,bpm) BS3X.bpmdata_2600p53{1}(4,bpm) BS3X.bpmdata_2601p03{1}(4,bpm) ],1);
    if(BS3X_index<INSTR{bpm}.Index)
      [~,R]=RmatAtoB(BS3X_index,INSTR{bpm}.Index);
      BS3X_qxth(bpm)=R(1,2);
    end
    if(~isfinite(dq(bpm,2)))
      bpm_fit(bpm_fit==bpm)=[];
    end
  end
  BS3X_qx=q(:,2);
  BS3X_dqx=dq(:,2);  
  [BS3X_conv,BS3X_dconv]=lscov(BS3X_qxth(bpm_fit),BS3X_qx(bpm_fit),BS3X_dqx(bpm_fit));
  figure(3)
  errorbar(bpm_measured,BS3X_qx(bpm_measured),BS3X_dqx(bpm_measured),'b');
  hold on
  plot(bpm_measured,BS3X_qxth(bpm_measured)*BS3X_conv,'r--')
  hold off
  xlabel('BPM #')
  title(sprintf('BS3X I to angle conv : %.1e+-%.e rad.A^{-1}',BS3X_conv,BS3X_dconv))
end