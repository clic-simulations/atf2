%%
samp=lcaGet('slacnq1:SAMPSEL')+7;
for ipulse=1:100
  wf5(ipulse,:)=lcaGet('slacnq1:waveform5');
  wf7(ipulse,:)=lcaGet('slacnq1:waveform7');
  pause(1/3)
end
figure
subplot(2,1,1),plot(wf5(:,samp))
subplot(2,1,2),plot(wf7(:,samp))

%%
gains=linspace(0.1,0.9,100);
for igain=1:length(gains)
  lcaPut('slacnq1:Y1GAIN',gains(igain))
  pause(1/3)
  lsis=lcaGet('lsis13:waveform0');
  y1I(igain)=lsis(1500);
end
figure
plot(y1I)