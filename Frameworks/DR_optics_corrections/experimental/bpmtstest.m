[stat td]=FlHwUpdate('readtsbuffer');
cav=findcells(INSTR,'Type','*cav');
strip=findcells(INSTR,'Type','stripline');
but=findcells(INSTR,'Type','button');
refbpm=99; % MQF1X
tsref=td(:,1+refbpm);
cavTime=zeros(1,length(cav));
for ibpm=1:length(cav)
  ts=td(:,1+cav(ibpm));
  cavTime(ibpm)=std(etime(datevec(td(:,1+cav(ibpm))),datevec(tsref)));
  cavTimeMean(ibpm)=mean(etime(datevec(td(:,1+cav(ibpm))),datevec(tsref)));
end
strTime=zeros(1,length(strip)-1);
for ibpm=1:length(strip)
  ts=td(:,1+strip(ibpm));
  strTime(ibpm)=std(etime(datevec(td(:,1+strip(ibpm))),datevec(tsref)));
  strTimeMean(ibpm)=mean(etime(datevec(td(:,1+strip(ibpm))),datevec(tsref)));
end
butTime=zeros(1,length(but));
for ibpm=1:length(but)
  ts=td(:,1+but(ibpm));
  butTime(ibpm)=std(etime(datevec(td(:,1+but(ibpm))),datevec(tsref)));
  butTimeMean(ibpm)=mean(etime(datevec(td(:,1+but(ibpm))),datevec(tsref)));
end
% Negative in mean time means later than ref bpm