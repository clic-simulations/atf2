function [twissx0,dtwissx0,twissy0,dtwissy0]=fitbeta(betax_meas,dbetax_meas,betay_meas,dbetay_meas,index_meas,index_fit)

global BEAMLINE FL

S_fit_point=BEAMLINE{index_fit}.S;
fit_point_name=BEAMLINE{index_fit}.Name;

nmeas=length(betax_meas);
if(length(betay_meas)~=nmeas || length(index_meas)~=nmeas)
  disp('betax_meas, betay_meas and index_meas must have same length');
  return;
end
if(size(betax_meas,1)==1)
  betax_meas=betax_meas';
end
if(size(dbetax_meas,1)==1)
  dbetax_meas=dbetax_meas';
end
if(size(betay_meas,1)==1)
  betay_meas=betay_meas';
end
if(size(dbetay_meas,1)==1)
  dbetay_meas=dbetay_meas';
end

Mx=zeros(nmeas,3);
My=zeros(nmeas,3);
S=zeros(nmeas,1);
for i=1:nmeas
  %get transfer matrix between 1st measurement point and the other
  if index_fit<index_meas(i)
    [~,r]=RmatAtoB(index_fit,index_meas(i));
    if(isfield(BEAMLINE{index_meas(i)},'L'))
      S(i)=BEAMLINE{index_meas(i)}.S+BEAMLINE{index_meas(i)}.L;
    else
      S(i)=BEAMLINE{index_meas(i)}.S;
    end
  else
    [~,r]=RmatAtoB(index_meas(i),index_fit);
    r=pinv(r);
    S(i)=BEAMLINE{index_meas(i)}.S;
  end
  %we know:
  %beta(x)=R11(0->x)^2*beta(0)-2*R11(0->x)*R12(0->x)*alpha(0)+R12(0->x)^2*gamma(0)
  %we just have to determine beta(0), alpha(0) and gamma(0) from
  %measurement points
  Mx(i,1)=r(1,1)^2;
  Mx(i,2)=-2*r(1,1)*r(1,2);
  Mx(i,3)=r(1,2)^ 2;
  My(i,1)=r(3,3)^2;
  My(i,2)=-2*r(3,3)*r(3,4);
  My(i,3)=r(3,4)^ 2;
end

nelem_plot=max(index_meas)-min(index_meas)+1;
Mx_plot=zeros(nelem_plot,3);
My_plot=zeros(nelem_plot,3);
S_plot=zeros(nelem_plot,1);
S_model=zeros(nelem_plot,1);
betax_model=zeros(nelem_plot,1);
betay_model=zeros(nelem_plot,1);
for i=1:nelem_plot
  index=min(index_meas)-1+i;
  %get transfer matrix between 1st measurement point and the other
  if index_fit<index
    [~,r]=RmatAtoB(index_fit,index);
    if(isfield(BEAMLINE{index},'L'))
      S_plot(i)=BEAMLINE{index}.S+BEAMLINE{index}.L;
    else
      S_plot(i)=BEAMLINE{index}.S;
    end
  else
    [~,r]=RmatAtoB(index,index_fit);
    r=pinv(r);
    S_plot(i)=BEAMLINE{index}.S;
  end
  Mx_plot(i,1)=r(1,1)^2;
  Mx_plot(i,2)=-2*r(1,1)*r(1,2);
  Mx_plot(i,3)=r(1,2)^ 2;
  My_plot(i,1)=r(3,3)^2;
  My_plot(i,2)=-2*r(3,3)*r(3,4);
  My_plot(i,3)=r(3,4)^ 2;
  S_model(i)=BEAMLINE{index}.S;
  betax_model(i)=FL.SimModel.Twiss.betax(index);
  betay_model(i)=FL.SimModel.Twiss.betay(index);
end

dbetax_meas
[twissx0,dtwissx0]=lscov(Mx,betax_meas,1./dbetax_meas);
[twissy0,dtwissy0]=lscov(My,betay_meas,1./dbetay_meas);

betax0_model=FL.SimModel.Twiss.betax(index_fit);
alphax0_model=FL.SimModel.Twiss.alphax(index_fit);
betay0_model=FL.SimModel.Twiss.betay(index_fit);
alphay0_model=FL.SimModel.Twiss.alphay(index_fit);

Bmagx=.5* (betax0_model/twissx0(1) + twissx0(1)/betax0_model + betax0_model*twissx0(1)*(twissx0(2)/twissx0(1)-alphax0_model/betax0_model)^2 );
Bmagy=.5* (betay0_model/twissy0(1) + twissy0(1)/betay0_model + betay0_model*twissy0(1)*(twissy0(2)/twissy0(1)-alphay0_model/betay0_model)^2 );

betax_plot=Mx_plot*twissx0;
betay_plot=My_plot*twissy0;

disp('At fit point :')
fprintf('betax=%.3f+-%.3f m betay=%.3f+-%3f m\n',twissx0(1),dtwissx0(1),twissy0(1),dtwissy0(1))

figure(1)
clf
hold on
errorbar(S,betax_meas,dbetax_meas,'b.')
errorbar(S,betay_meas,dbetay_meas,'r.')
plot(S_plot,betax_plot,'b-',S_plot,betay_plot,'r-')
plot(S_model,betax_model,'b--',S_model,betay_model,'r--')
ylimits=ylim();
ver_line(S_fit_point)
hold off
xlabel('S [m]')
ylabel('\beta [m]')
text(S_fit_point,ylimits(2)*.9,sprintf('beta_x(%s)=%.3f m\nbeta_y(%s)=%.3f m\nBmag_x=%.3f, Bmag_y=%.3f',fit_point_name,twissx0(1),fit_point_name,twissy0(1),Bmagx,Bmagy))
