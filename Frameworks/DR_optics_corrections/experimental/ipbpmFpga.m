function varargout=ipbpmFpga(req,varargin)
% IPBPMFPGA - routines for FPGA IPBPM (vertical)
% [I_jitter, Q_jitter] = ipbpmFpga('jitter',Npulse,bpmName)
% [moverPositions, I, I_err, Q, Q_err, Phase, Phase_err] = ipbpmFpga('mcalib',Npulse,bpmName)
% bpmName = 'IPA' or 'IPB'

varargout{1}=[];

wfsamp=900:1000;
mpos=linspace(-50,50,11).*1e-6;
pv='lsis13:waveform%d';
for iw=1:6
  lcaSetMonitor(sprintf(pv,iw-1));
end
nbpm=varargin{1};
bpm=varargin{2};
if strcmp(bpm,'IPA')
  ibpm=[0 1 2];
else
  ibpm=[3 4 5];
end
switch req
  case 'jitter'
    bpmpos=zeros(2,nbpm);
    for ipulse=1:nbpm
      lcaNewMonitorWait(sprintf(pv,ibpm(1)));
      lcaNewMonitorWait(sprintf(pv,ibpm(2)));
      b1=lcaGet(sprintf(pv,ibpm(1)));
      b2=lcaGet(sprintf(pv,ibpm(2)));
      bpmpos(1,ipulse)=mean(b1(wfsamp));
      bpmpos(2,ipulse)=mean(b2(wfsamp));
    end
    varargout{1}=std(bpmpos(1,:));
    varargout{2}=std(bpmpos(2,:));
  case 'mcalib'
    lcaSetMonitor('FSECS:command');
    lcaPut('FSECS:command','bumpipinit');
    waitForECS;
    lcaPut('FSECS:strCmd','y');
    bpmMover=zeros(2,length(mpos));
    bpmMover_err=bpmMover;
    for im=1:length(mpos)
      lcaPut('FSECS:valCmd',mpos(im));
      if strcmp(bpm,'IPA')
        lcaPut('FSECS:command','bumpipbpma');
      else
        lcaPut('FSECS:command','bumpipbpmb');
      end
      waitForECS;
      fprintf('Taking data %d of %d (Mover at %g um)\n',im,length(mpos),mpos(im));
      pause
      bpmpos=zeros(3,nbpm);
      for ipulse=1:nbpm
        lcaNewMonitorWait(sprintf(pv,ibpm(1)));
        lcaNewMonitorWait(sprintf(pv,ibpm(2)));
        lcaNewMonitorWait(sprintf(pv,ibpm(3)));
        b1=lcaGet(sprintf(pv,ibpm(1)));
        b2=lcaGet(sprintf(pv,ibpm(2)));
        b3=lcaGet(sprintf(pv,ibpm(3)));
        bpmpos(1,ipulse)=mean(b1(wfsamp));
        bpmpos(2,ipulse)=mean(b2(wfsamp));
        bpmpos(2,ipulse)=mean(b3(wfsamp));
      end
      bpmMover(1,im)=mean(bpmpos(1,:)); bpmMover_err(1,im)=std(bpmpos(1,:));
      bpmMover(2,im)=mean(bpmpos(2,:)); bpmMover_err(2,im)=std(bpmpos(2,:));
      bpmMover(3,im)=mean(bpmpos(3,:)); bpmMover_err(3,im)=std(bpmpos(3,:));
    end
    varargout{1}=mpos; varargout{2}=bpmMover(1,:); varargout{3}=bpmMover_err(1,:);
    varargout{4}=bpmMover(2,:); varargout{5}=bpmMover_err(2,:);
    varargout{6}=bpmMover(3,:); varargout{7}=bpmMover_err(3,:);
end

function waitForECS()
timeout=20; % s
t0=clock;
while 1
  cmd=lcaGet('FSECS:command');
  if strcmp(cmd,' ')
    break
  elseif etime(clock,t0)>timeout
    error('Timeout waiting for FSECS command to clear')
  end
end
    