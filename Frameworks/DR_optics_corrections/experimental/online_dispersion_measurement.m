function [IEX_D_fit_ext,error_IEX_D_fit_ext,IP_D_fit_ext,error_IP_D_fit_ext]=online_dispersion_measurement(plot_on,quiet)
% [IEX_D_fit_ext,error_IEX_D_fit_ext,IP_D_fit_ext,error_IP_D_fit_ext]=online_dispersion_measurement(plot_on,quiet)
%--------------------------------------
% Fit dispersion using energy jitter in EXT
% parameters :
%   plot_on (default=0) : boolean to enable (plot_on=1) or disable (plot_on=0) plotting.
%   quiet (default=0) : if 0 no warning are displayed
% output :
%   IEX_D_fit_ext : dispersion (Dx,Dx',Dy,Dy') fitted at IEX
%   error_IEX_D_fit_ext : error on dispersion fitted at IEX
%   IP_D_fit_ext : dispersion (Dx,Dx',Dy,Dy') fitted at IP
%   error_IP_D_fit_ext : error on dispersion fitted at IP
%--------------------------------------

  global BEAMLINE INSTR FL
  persistent IEX MQM16FF IP bpm bpm_s bpm_name bpm_index nbpm R_IEX R_IP Dx_nominal Dy_nominal PVnames PVnames_i_Rchange PVnames_i_BPMchange pulsenum_init

%program parameters
  if(~exist('plot_on','var'))
      plot_on=1;
  end
  
% initialization (done once for runtime saving)
  if(isempty(IEX))
    if(~quiet)
      disp('initialization')
    end
    IEX=findcells(BEAMLINE,'Name','IEX');
    IP=findcells(BEAMLINE,'Name','IP');
    MQM16FF=findcells(BEAMLINE,'Name','MQM16FF');
%       findcells(BEAMLINE,'Name','MB*X') ...
%       findcells(BEAMLINE,'Name','MDUMP')
%       findcells(BEAMLINE,'Name','MS*FF') ...
    bpm=[findcells(BEAMLINE,'Name','MB*R') ...
      findcells(BEAMLINE,'Name','MQ*X') ...
      findcells(BEAMLINE,'Name','MQ*FF') ...
      findcells(BEAMLINE,'Name','MPREIP')...
      findcells(BEAMLINE,'Name','IPBPMA')...
      findcells(BEAMLINE,'Name','IPBPMB')...
      findcells(BEAMLINE,'Name','IP')...
      findcells(BEAMLINE,'Name','MPIP')];
    bpm=unique(bpm);
    nbpm=length(bpm);
    bpm_name=[];
    bpm_index=[];
    for i=1:nbpm
        bpm_name=char(bpm_name,BEAMLINE{bpm(i)}.Name);
        bpm_s(i)=BEAMLINE{bpm(i)}.S;
        bpm_index(end+1)=findcells(INSTR,'Index',bpm(i));
        Dx_nominal(i)=FL.SimModel.Twiss.etax(bpm(i));
        Dy_nominal(i)=FL.SimModel.Twiss.etay(bpm(i));
    end
    bpm_name(1,:)=[];
    PVnames = FlUpdate('getUpdateListNames');
% Find index in PVnames corresponding to quad PS (if change = R change)
    quads_extff=findcells(BEAMLINE,'Class','QUAD',1200,length(BEAMLINE));
    PVnames_i_Rchange=[];
    for i=1:length(quads_extff)
      quad_ps=BEAMLINE{quads_extff(i)}.PS;
      quad_ps_pv=regexprep(FL.HwInfo.PS(quad_ps).pvname{1},'.VAL','');
      for pv=quad_ps_pv
        PVnames_i_Rchange=[PVnames_i_Rchange; find(strcmp(PVnames,pv))];
      end
    end
    PVnames_i_Rchange=unique(PVnames_i_Rchange);
% Find index in PVnames corresponding to cor PS or mover pos (if change = BPM reading change)
    % Mover readback change all the time => disabled
    % PVnames_i_BPMchange=find(strncmp(PVnames,'c1:qmov:',8)); 
    PVnames_i_BPMchange=[];
    cor_extff=findcells(BEAMLINE,'Class','*COR',1200,length(BEAMLINE));
    % ZH4X ZH5X ZV1X ZV2X Used in orbit feedback => disabled
    cor_extff(cor_extff==findcells(BEAMLINE,'Name','ZH4X'))=[];
    cor_extff(cor_extff==findcells(BEAMLINE,'Name','ZH5X'))=[];
    cor_extff(cor_extff==findcells(BEAMLINE,'Name','ZV1X'))=[];
    cor_extff(cor_extff==findcells(BEAMLINE,'Name','ZV2X'))=[];
    % ZX1X change too much => disabled
    cor_extff(cor_extff==findcells(BEAMLINE,'Name','ZX1X'))=[];    
    for i=1:length(cor_extff)
      cor_ps=BEAMLINE{cor_extff(i)}.PS;
      cor_ps_pv=regexprep(FL.HwInfo.PS(cor_ps).pvname{1},'.VAL','');
      for pv=cor_ps_pv
        PVnames_i_BPMchange=[PVnames_i_BPMchange; find(strcmp(PVnames,pv))];
      end
    end
    PVnames_i_BPMchange=unique(PVnames_i_BPMchange);
    
    FlUpdate('updateListSetRef');
    [s,pulsenum_init]=FlHwUpdate('getpulsenum');
    while(s{1}~=1)
      if(~quiet)
        disp('FlHwUpdate(''getpulsenum'') failed')
      end
      [s,pulsenum_init]=FlHwUpdate('getpulsenum');
    end  
    return
  end
  
%get BPM data
  bpm_ext_i=find(bpm>=IEX & bpm<MQM16FF);
  bpm_ff_i=find(bpm>=MQM16FF);
  bpm_ring_i=find(bpm<IEX);
  updateVals = FlUpdate('getUpdateList');
  if(any(updateVals(PVnames_i_Rchange)))
    if(~quiet)
      disp('lattice change :')
      PVnames_i_Rchange_logical=zeros(size(updateVals));
      PVnames_i_Rchange_logical(PVnames_i_Rchange)=1;
      disp(PVnames(updateVals & PVnames_i_Rchange_logical))
    end
    clear R_IEX RIP;
    FlUpdate('updateListSetRef');
    [s,pulsenum_init]=FlHwUpdate('getpulsenum');
    while(s{1}~=1)
      if(~quiet)
        disp('FlHwUpdate(''getpulsenum'') failed')
      end
      [s,pulsenum_init]=FlHwUpdate('getpulsenum');
    end  
    return
  end
  if(any(updateVals(PVnames_i_BPMchange)))
    if(~quiet)
      disp('corrector or mover change :')
    end
    PVnames_i_BPMchange_logical=zeros(size(updateVals));
    PVnames_i_BPMchange_logical(PVnames_i_BPMchange)=1;
    disp(PVnames(updateVals & PVnames_i_BPMchange_logical))
    FlUpdate('updateListSetRef');
    [s,pulsenum_init]=FlHwUpdate('getpulsenum');
    while(s{1}~=1)
      if(~quiet)
        disp('FlHwUpdate(''getpulsenum'') failed :retry')
      end
      [s,pulsenum_init]=FlHwUpdate('getpulsenum');
    end  
    return
  end
  [s,pulsenum]=FlHwUpdate('getpulsenum');
  if(s{1}~=1)
    if(~quiet)
      disp('FlHwUpdate(''getpulsenum'') failed')
    end
    return
  end  
  npulse=pulsenum-pulsenum_init;
  if(npulse<10)
    if(~quiet)
      fprintf('only %i pulses available since last change.\n',npulse)
    end
    return
  end
  [s,raw]=FlHwUpdate('readbuffer',npulse);
  if(s{1}~=1)
    if(~quiet)
      disp('FlHwUpdate(''readbuffer'') failed')
    end
    return
  end
  x_read=raw(:,bpm_index*3-1);
  y_read=raw(:,bpm_index*3);
  TMIT=raw(:,154*3+1);
  bad_TMIT=[find(TMIT<0.7*mean(TMIT)); find(TMIT>1.3*mean(TMIT)); find(TMIT<3e9)];
  x_read(bad_TMIT,:)=[];
  y_read(bad_TMIT,:)=[];
  x_read(:,bpm_ring_i)=-x_read(:,bpm_ring_i);%different coordonate system in the ring
  y_read(:,bpm_ring_i)=-y_read(:,bpm_ring_i);%different coordonate system in the ring
  dx_read=x_read(1:end-1,:)-x_read(2:end,:);
  dy_read=y_read(1:end-1,:)-y_read(2:end,:);
  npulse=length(x_read);
  if(~quiet)
    fprintf('Analysing with %i relative pulses.\n',npulse)
  end
  
%estimate BPM resolution
  [U,S,V]=svd([dx_read dy_read]);
  S_noise=S;
  S_noise(1:5,1:5)=0;
  noise=U*S_noise*V';
  resol=std(noise);
  

%remove bad BPMs
  bad_bpm_i=[find(resol<1e-8) find(std(x_read)>1e-3) find(std(y_read)>1e-3)];
  bad_bpm_i(bad_bpm_i>nbpm)=bad_bpm_i(bad_bpm_i>nbpm)-nbpm;
  bad_bpm_i=unique(bad_bpm_i);
  good_bpm_i=1:nbpm;
  if(~isempty(bad_bpm_i))
    if(~quiet)
      disp('bad bpms :')
      disp(bpm_name(bad_bpm_i,:))
    end
    for index_bad=bad_bpm_i(:)'
      bpm_ring_i(bpm_ring_i==index_bad)=[];
      bpm_ext_i(bpm_ext_i==index_bad)=[];
      bpm_ff_i(bpm_ff_i==index_bad)=[];
    end
    good_bpm_i(bad_bpm_i)=[];
  end
  
%get R=[R_16 R_36] from model  
  if(isempty(R_IEX))
    R_IEX=zeros(nbpm*2,5);
    for i=1:nbpm;
      if (bpm(i)<=IEX)
          [~,r]=RmatAtoB(bpm(i),IEX);
          r=inv(r);
      else
          [~,r]=RmatAtoB(IEX,bpm(i));
      end
      R_IEX(i,:)=r(1,[1:4 6]);
      R_IEX(nbpm+i,:)=r(3,[1:4 6]);
    end
  end
  if(isempty(R_IP))
    R_IP=zeros(nbpm*2,5);
    for i=1:nbpm;
      if (bpm(i)<=IP)
          [~,r]=RmatAtoB(bpm(i),IP);
          r=inv(r);
      else
          [~,r]=RmatAtoB(IP,bpm(i));
      end
      R_IP(i,:)=r(1,[1:4 6]);
      R_IP(nbpm+i,:)=r(3,[1:4 6]);
    end
  end
  
  range_param_fit=bpm_ext_i;
  range_param_fitxy=[range_param_fit nbpm+range_param_fit];
%fit injection parameters
  [IEX_param,error_IEX_param]=lscov(R_IEX(range_param_fitxy,:),[dx_read(:,range_param_fit)'; dy_read(:,range_param_fit)'],resol(range_param_fitxy).^-2);
  [IP_param,error_IP_param]=lscov(R_IP(range_param_fitxy,:),[dx_read(:,range_param_fit)'; dy_read(:,range_param_fit)'],resol(range_param_fitxy).^-2);

%remove bad pulse
  bad_pulse=[find(std(noise(:,good_bpm_i),'',2)>2*mean(std(noise(:,good_bpm_i),'',2)));...
             find(error_IEX_param(5,:)>5e-5)'];
  bad_pulse=unique(bad_pulse);
  dx_read(bad_pulse,:)=[];
  dy_read(bad_pulse,:)=[];
  IEX_param(:,bad_pulse)=[];
  IP_param(:,bad_pulse)=[];
  error_IEX_param(:,bad_pulse)=[];
  error_IP_param(:,bad_pulse)=[];
  npulse=size(dx_read,1);
  if(~quiet)
    fprintf('%i bad pulse removed.\n', length(bad_pulse))
  end
  if(npulse<5)
    if(~quiet)
      disp('not enough pulse remains.')
    end
    return
  end
  
%fit dispersion for each BPM in range_fit
  if(plot_on)
    range_individual_fit=[bpm_ring_i bpm_ext_i bpm_ff_i];
  else
    range_individual_fit=[bpm_ext_i bpm_ff];
  end
  for i=range_individual_fit
    [p,dp]=noplot_polyfit(IEX_param(5,:),dx_read(:,i)',1,[0 1]);
    Dx_ext(i)=p(1);
    error_Dx_ext(i)=dp(1);
    [p,dp]=noplot_polyfit(IEX_param(5,:),dy_read(:,i)',1,[0 1]);
    Dy_ext(i)=p(1);
    error_Dy_ext(i)=dp(1);
%     [p,dp]=noplot_polyfit(param_ring(5,:),dx_read(:,i),1,1);
%     Dx_ring(i)=p(2);
%     error_Dx_ring(i)=dp(2);
%     [p,dp]=noplot_polyfit(param_ring(5,:),dy_read(:,i),1,1);
%     Dy_ring(i)=p(2);
%     error_Dy_ring(i)=dp(2);
  end
  
%fit incoming dispersion
  range_fit=bpm_ext_i;
  [IEX_D_fit_ext,error_IEX_D_fit_ext]=lscov(R_IEX([range_fit nbpm+range_fit],1:4),[Dx_ext(range_fit) Dy_ext(range_fit)]'-R_IEX([range_fit nbpm+range_fit],5),[error_Dx_ext(range_fit) error_Dy_ext(range_fit)]'.^-2);
  [IP_D_fit_ext,error_IP_D_fit_ext]=lscov(R_IP([range_fit nbpm+range_fit],1:4),[Dx_ext(range_fit) Dy_ext(range_fit)]'-R_IP([range_fit nbpm+range_fit],5),[error_Dx_ext(range_fit) error_Dy_ext(range_fit)]'.^-2);
  Dx_propagated_ext=R_IEX(1:nbpm,1:4)*IEX_D_fit_ext+R_IEX(1:nbpm,5);
  Dy_propagated_ext=R_IEX(nbpm+1:2*nbpm,1:4)*IEX_D_fit_ext+R_IEX(nbpm+1:2*nbpm,5);
  range_fit=bpm_ff_i;
  [IEX_D_fit_ff,error_IEX_D_fit_ff]=lscov(R_IEX([range_fit nbpm+range_fit],1:4),[Dx_ext(range_fit) Dy_ext(range_fit)]'-R_IEX([range_fit nbpm+range_fit],5),[error_Dx_ext(range_fit) error_Dy_ext(range_fit)]'.^-2);
  [IP_D_fit_ff,error_IP_D_fit_ff]=lscov(R_IP([range_fit nbpm+range_fit],1:4),[Dx_ext(range_fit) Dy_ext(range_fit)]'-R_IP([range_fit nbpm+range_fit],5),[error_Dx_ext(range_fit) error_Dy_ext(range_fit)]'.^-2);
  Dx_propagated_ff=R_IEX(1:nbpm,1:4)*IEX_D_fit_ff+R_IEX(1:nbpm,5);
  Dy_propagated_ff=R_IEX(nbpm+1:2*nbpm,1:4)*IEX_D_fit_ff+R_IEX(nbpm+1:2*nbpm,5);

  if(plot_on)
  %plot energy fitted
    figure(1)
    subplot(3,2,1:2)
    errorbar(1:npulse,IEX_param(5,:),error_IEX_param(5,:),'r+')
    hold on
    errorbar(1:npulse,IP_param(5,:),error_IP_param(5,:),'b+')
    hold off
    ylabel('dE/E')
    legend('@ IEX','@ IP')
    xlim([0 npulse+1])
    subplot(3,2,3)
    errorbar(1:npulse,IEX_param(1,:),error_IEX_param(1,:),'r+')
    hold on
    errorbar(1:npulse,IP_param(1,:),error_IP_param(1,:),'b+')
    hold off
    ylabel('X')
    xlim([0 npulse+1])
    subplot(3,2,4)
    errorbar(1:npulse,IEX_param(2,:),error_IEX_param(2,:),'r+')
    hold on
    errorbar(1:npulse,IP_param(2,:),error_IP_param(2,:),'b+')
    hold off
    ylabel('X''')
    xlim([0 npulse+1])
    subplot(3,2,5)
    errorbar(1:npulse,IEX_param(3,:),error_IEX_param(3,:),'r+')
    hold on
    errorbar(1:npulse,IP_param(3,:),error_IP_param(3,:),'b+')
    hold off
    ylabel('Y')
    xlim([0 npulse+1])
    subplot(3,2,6)
    errorbar(1:npulse,IEX_param(4,:),error_IEX_param(4,:),'r+')
    hold on
    errorbar(1:npulse,IP_param(4,:),error_IP_param(4,:),'b+')
    hold off
    ylabel('Y''')
    xlim([0 npulse+1])

  %plot dispersion fitted
    figure(2)
    subplot(2,1,1)
    errorbar(bpm_s(good_bpm_i),Dx_ext(good_bpm_i),error_Dx_ext(good_bpm_i),'k+')
    hold on
    plot(bpm_s(1:nbpm),Dx_propagated_ext,'b-')
    plot(bpm_s(1:nbpm),Dx_propagated_ff,'r-')
    plot(bpm_s(1:nbpm),Dx_nominal,'c--')
    hold off
    ylim([-.7 .7])
    title([sprintf('EXT BPM fit :Dx(IEX)=%.3f+-%.3f  Dx''(IEX)=%.3f+-%.3f Dy(IEX)=%.3f+-%.3f  Dy''(IEX)=%.3f+-%.3f\n',IEX_D_fit_ext(1),error_IEX_D_fit_ext(1),IEX_D_fit_ext(2),error_IEX_D_fit_ext(2),IEX_D_fit_ext(3),error_IEX_D_fit_ext(3),IEX_D_fit_ext(4),error_IEX_D_fit_ext(4)) ...
          sprintf('FF BPM fit :Dx(IEX)=%.3f+-%.3f  Dx''(IEX)=%.3f+-%.3f Dy(IEX)=%.3f+-%.3f  Dy''(IEX)=%.3f+-%.3f',IEX_D_fit_ff(1),error_IEX_D_fit_ff(1),IEX_D_fit_ff(2),error_IEX_D_fit_ff(2),IEX_D_fit_ff(3),error_IEX_D_fit_ff(3),IEX_D_fit_ff(4),error_IEX_D_fit_ff(4))])
    xlabel('S [m]')
    ylabel('Dx mismatch [m]')
    legend('individual','fit from EXT BPM','fit from FF BPM','nominal','Location','NorthWest')
    subplot(2,1,2)
    errorbar(bpm_s(good_bpm_i),Dy_ext(good_bpm_i),error_Dy_ext(good_bpm_i),'k+')
    hold on
    plot(bpm_s(1:nbpm),Dy_propagated_ext,'b-')
    plot(bpm_s(1:nbpm),Dy_propagated_ff,'r-')
    plot(bpm_s(1:nbpm),Dy_nominal,'c--')
    hold off
    ylim([-0.2 0.2])
    title([sprintf('EXT BPM fit :Dx(IP)=%.3f+-%.3f  Dx''(IP)=%.3f+-%.3f Dy(IP)=%.3f+-%.3f  Dy''(IP)=%.3f+-%.3f\n',IP_D_fit_ext(1),error_IP_D_fit_ext(1),IP_D_fit_ext(2),error_IP_D_fit_ext(2),IP_D_fit_ext(3),error_IP_D_fit_ext(3),IP_D_fit_ext(4),error_IP_D_fit_ext(4)) ...
          sprintf('FF BPM fit :Dx(IP)=%.3f+-%.3f  Dx''(IP)=%.3f+-%.3f Dy(IP)=%.3f+-%.3f  Dy''(IP)=%.3f+-%.3f',IP_D_fit_ff(1),error_IP_D_fit_ff(1),IP_D_fit_ff(2),error_IP_D_fit_ff(2),IP_D_fit_ff(3),error_IP_D_fit_ff(3),IP_D_fit_ff(4),error_IP_D_fit_ff(4))])
    ylabel('Dy mismatch [m]')
    set(gca,'XTick',bpm_s);
    set(gca,'XTickLabel',bpm_name);
    xticklabel_rotate90();

  %plot dispersion missmatch
    figure(3)
    subplot(2,1,1)
    errorbar(bpm_s(good_bpm_i),Dx_ext(good_bpm_i)-Dx_nominal(good_bpm_i),error_Dx_ext(good_bpm_i),'k+')
    hold on
    plot(bpm_s(1:nbpm),Dx_propagated_ext'-Dx_nominal,'b-')
    plot(bpm_s(1:nbpm),Dx_propagated_ff'-Dx_nominal,'r-')
    hold off
    ylim([-0.2 0.2])
    xlabel('S [m]')
    ylabel('Dx mismatch [m]')
    title([sprintf('EXT BPM fit :Dx(IEX)=%.3f+-%.3f  Dx''(IEX)=%.3f+-%.3f Dy(IEX)=%.3f+-%.3f  Dy''(IEX)=%.3f+-%.3f\n',IEX_D_fit_ext(1),error_IEX_D_fit_ext(1),IEX_D_fit_ext(2),error_IEX_D_fit_ext(2),IEX_D_fit_ext(3),error_IEX_D_fit_ext(3),IEX_D_fit_ext(4),error_IEX_D_fit_ext(4)) ...
          sprintf('FF BPM fit :Dx(IEX)=%.3f+-%.3f  Dx''(IEX)=%.3f+-%.3f Dy(IEX)=%.3f+-%.3f  Dy''(IEX)=%.3f+-%.3f',IEX_D_fit_ff(1),error_IEX_D_fit_ff(1),IEX_D_fit_ff(2),error_IEX_D_fit_ff(2),IEX_D_fit_ff(3),error_IEX_D_fit_ff(3),IEX_D_fit_ff(4),error_IEX_D_fit_ff(4))])
    legend('individual','fit from EXT BPM','fit from FF BPM','Location','NorthWest')
    subplot(2,1,2)
    errorbar(bpm_s(good_bpm_i),Dy_ext(good_bpm_i)-Dy_nominal(good_bpm_i),error_Dy_ext(good_bpm_i),'k+')
    hold on
    plot(bpm_s(1:nbpm),Dy_propagated_ext'-Dy_nominal,'b-')
    plot(bpm_s(1:nbpm),Dy_propagated_ff'-Dy_nominal,'r-')
    hold off
    ylim([-0.2 0.2])
    title([sprintf('EXT BPM fit :Dx(IP)=%.3f+-%.3f  Dx''(IP)=%.3f+-%.3f Dy(IP)=%.3f+-%.3f  Dy''(IP)=%.3f+-%.3f\n',IP_D_fit_ext(1),error_IP_D_fit_ext(1),IP_D_fit_ext(2),error_IP_D_fit_ext(2),IP_D_fit_ext(3),error_IP_D_fit_ext(3),IP_D_fit_ext(4),error_IP_D_fit_ext(4)) ...
          sprintf('FF BPM fit :Dx(IP)=%.3f+-%.3f  Dx''(IP)=%.3f+-%.3f Dy(IP)=%.3f+-%.3f  Dy''(IP)=%.3f+-%.3f',IP_D_fit_ff(1),error_IP_D_fit_ff(1),IP_D_fit_ff(2),error_IP_D_fit_ff(2),IP_D_fit_ff(3),error_IP_D_fit_ff(3),IP_D_fit_ff(4),error_IP_D_fit_ff(4))])
    ylabel('Dy mismatch [m]')
    set(gca,'XTick',bpm_s);
    set(gca,'XTickLabel',bpm_name);
    xticklabel_rotate90();
  end
end