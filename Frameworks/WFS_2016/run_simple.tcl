# Input: charge file, energy file, correctors strength
# Output: Tracking results and BPMs readings
 
# Possible beam dynamics set up: Misalignment and beam incoming jitter

####################
# Directory set up #
####################
set base_dir [ pwd ]
set script_dir $base_dir/../../
set lattice_dir $script_dir/Lattices/ATF2
set script_common_dir $script_dir/Common
source $script_common_dir/scripts/tcl_procs.tcl


set e0 1.3 


#######################
# Tracking Output dir #
#######################

set beamline_name "ATF2_v5.2"
set tracking_output_string "%s %ex %ey %sx %sy %sz" ;# position, energy, dispersion, emittx, emitty, bunch_length, beam size x, beam size y
set dir_name "simpletracking"

exec mkdir -p RESULTS_Coll
cd RESULTS_Coll


###################
# Beam Parameters #
###################
# Bunch offset
set use_bunch_offset 0
set bunch_offset_x 0; #[um]
set bunch_offset_y 0; #[um]
set bunch_offset_xp 0; #[urad]
set bunch_offset_yp 0; #[urad]

# total number of simulated particles and slices
set n_slice 25
set n 5000

# copied mostly from Mad8 file
# emitance normalized in e-7 m
set match(emitt_x) 50
set match(emitt_y) 0.3
# energy spread normalized in %, negative means flat distribution, positive gaussian distribution, maxed at 3 sigma
set match(e_spread) 0.0
# beam long in e-6 m
set match(sigma_z) 300
set match(phase) 0.0
set e_initial 1.3 ;# design beam energy [GeV]
set e0_start 1.3 ;# [GeV]
#set match(charge)  6e9
#set charge 6e9
set match(charge)  6e9
set charge 6e9

#######################
#Add beam line set up #
#######################
set beamline_name "ATF2_v5.2"
##########  10x1 optics  ########
#array set match {beta_x 6.848560987444 alpha_x 1.108024744392 beta_y 2.935758991906 alpha_y -1.907222941526}
##########  10x0.5 optics  ########
array set match {beta_x 6.54290 alpha_x 0.99638 beta_y 3.23785 alpha_y -1.9183}

# Use beam incoming jitter
set use_beam_jitter 0
set beam_jitter_x 10; #[um]
set beam_jitter_y 10; #[um]
set beam_jitter_xp 1; #[urad]
set beam_jitter_yp 1; #[urad]

# synchrotron on/off in certain elements
set quad_synrad 1
set mult_synrad 1
set sbend_synrad 1

# CBPM Wakefield
set use_wakefield 1
# use short bunch approximation - 
set use_wakefield_shortbunch 1
set wakefield_shortbunch 300 ;# [um]

# Options for bpms and quadrupoles misalignments
set seed 0
set sigma 0

######## COLLIMATOR PARAMETERS ###########
# metode to calculate collimators: 0 Giovani's method
set wmetode 0
### Example collimator number 3
# Maximum half gap
set paramb 12.0e-3
# Minimum half gap
set parama 12.0e-3
# Collimator half width 
set colwidth 12e-3
# Taper part length
set tlength  100e-3
# Flat part length
set flength 100.0e-3
# Material conductivity (for Stainless Steel, Aluminium, Cooper)
#set conduct 1.45e6
#set conduct 4.8e7
set conduct 5.8e7
set offset 0
#######################
# Add beam line       #
#######################
Girder
ReferencePoint -name "START" -angle 1 -sens 1
proc HCORRECTOR { a b c d } { Dipole $a $b $c $d }
proc VCORRECTOR { a b c d } { Dipole $a $b $c $d }
#source $lattice_dir/ATF2_v5.2-MadX-Coll2.tcl
source $lattice_dir/ATF2_v5.2-MadX-Coll_10x0.5.tcl

ReferencePoint -sense -1
BeamlineSet -name $beamline_name

###########################
# Add beam line wakefields#
###########################
set use_wakefield 1

if {$use_wakefield} {
    source $script_dir/Frameworks/ATF2_studies/scripts/wakefield.tcl
}
# use short bunch approximation - 
set use_wakefield_shortbunch 1
set wakefield_shortbunch 300 ;# [um]
#######################
# Setting up the beam #
#######################

set beam_name electron
set scripts $script_common_dir/scripts  ; # This is necessary for compatibility

# load RF and injection scripts:
source $script_common_dir/scripts/init.tcl;

# loading of the beam making scripts
source $script_common_dir/scripts/ilc_beam.tcl;

# Total number of particles
set n_total [expr $n * $n_slice]

# make initial perfect beam
make_beam_many $beam_name $n_slice $n
BeamAddOffset -beam $beam_name -x $bunch_offset_x -y $bunch_offset_y -angle_x $bunch_offset_xp -angle_y $bunch_offset_yp

FirstOrder 1

####################################
# Bpms and quadrupole misalignment #
####################################

proc my_survey {} {
    global seed sigma
    Octave {
        randn("seed", $seed );
	BI = placet_get_number_list("ATF2_v5.2", "bpm");
	QI = placet_get_number_list("ATF2_v5.2", "quadrupole");	
	placet_element_set_attribute("ATF2_v5.2", BI, "x", randn(size(BI)) * $sigma);
	placet_element_set_attribute("ATF2_v5.2", BI, "y", randn(size(BI)) * $sigma);
	placet_element_set_attribute("ATF2_v5.2", QI, "x", randn(size(QI)) * $sigma);
	placet_element_set_attribute("ATF2_v5.2", QI, "y", randn(size(QI)) * $sigma);
      if $sigma > 0
           printf("BPMs and quadrupole misalignment is implemented \n ");
        end 
	
   }
}

 
##############################
# Apply incoming beam jitter #  
##############################

#Octave {
#    if ($use_beam_jitter)
#    Tcl_Eval("BeamAddOffset -beam electron -x $beam_jitter_x -y $beam_jitter_y -angle_x $beam_jitter_xp -angle_y $beam_jitter_yp")
#    printf("Incoming beam jitter is considered \n");  
#    else 
#    end
    
#}
    	     
############
# Tracking #
############

#######################################
# Tracking                            #
#######################################
set offset2 0
DispersionFreeEmittance -on
## To add vertical offset to the collimator element
    Octave {
   ##  Coffset=placet_get_name_number_list("ATF2_v5.2", "C3WAKE1");
    ## placet_element_set_attribute("ATF2_v5.2", Coffset, "y", ${offset2});
    disp("Performing particle tracking ...\n") 
    
    [E,B] = placet_test_no_correction("ATF2_v5.2", "electron","my_survey"); # $sigma is in um    
    printf("FINAL HORIZONTAL EMITTANCE = %g um*rad\n",   E(end-1,2) / 10);
    printf("FINAL VERTICAL   EMITTANCE = %g um*rad\n\n", E(end-1,6) / 10);
    Ex = E(end-1,2) / 10;
    Ey = E(end-1,6) / 10; # The IP index correspond to end-1 
    s=load('IPparticles.out');
    Emean=mean(s(:,1))
    xmean=mean(s(:,2))
    ymean=mean(s(:,3))
    sigy=std(s(:,3))
    sigx=std(s(:,2))
    sigpy=std(s(:,6))
    sigpx=std(s(:,5))
    a2=cov([s(:,3),s(:,6)]);
    a2x=cov([s(:,2),s(:,5)]);
    emitty=sqrt((sigy)^2*(sigpy)^2-(a2(1,2))^2)*2544*10^(-6);
    emittx=sqrt((sigx)^2*(sigpx)^2-(a2x(1,2))^2)*2544*10^(-6);
    deltay_var=(-ymean*10^(3))-5
    delta_sigma=sqrt(sigy^2-0.037897^2)*10^3
    }




 
