# Input: charge file, energy file, correctors strength
# Output: Tracking results and BPMs readings
 
# Possible beam dynamics set up: Misalignment and beam incoming jitter

####################
# Directory set up #
####################
set base_dir [ pwd ]
set script_dir $base_dir/../../../ATF2
set lattice_dir $script_dir/Lattices/ATF2
set script_common_dir $script_dir/Common
source $script_common_dir/scripts/tcl_procs.tcl


set e0 1.3

array set args {
    sigma  30
    machine all
    nm 1
    wgt1 -1
    wgt2 -1
		deltae 0.13
		dcharge 8e9
    beta0 5
    beta1 6
    beta2 6
    noverlap 0.0
}   

array set args $argv

set nm $args(nm)
set sigma $args(sigma)
set deltae $args(deltae)
set dcharge $args(dcharge)
set machine $args(machine)
set wgt1 $args(wgt1)
set wgt2 $args(wgt2)
set beta0 $args(beta0)
set beta1 $args(beta1)
set beta2 $args(beta2)
set noverlap $args(noverlap)

set bpmres 1

if { $wgt1 == -1 } {
    set wgt1 [expr round(sqrt(($bpmres * $bpmres + $sigma * $sigma) / (2 * $bpmres * $bpmres)))]
    puts "Using DFS theoretical weight = $wgt1"
}

if { $wgt2 == -1 } {
    set wgt2 [expr round(sqrt(($bpmres * $bpmres + $sigma * $sigma) / (2 * $bpmres * $bpmres)))]
    puts "Using WFS theoretical weight = $wgt2"
}


#######################
# Tracking Output dir #
#######################

set beamline_name "ATF2_v5.2"

exec mkdir -p RESULTS_Coll_sigma_${sigma}_quad
cd RESULTS_Coll_sigma_${sigma}_quad


###################
# Beam Parameters #
###################
# Bunch offset
set use_bunch_offset 0
set bunch_offset_x 0; #[um]
set bunch_offset_y 0; #[um]
set bunch_offset_xp 0; #[urad]
set bunch_offset_yp 0; #[urad]

# total number of simulated particles and slices
set n_slice 10
set n 2000

# copied mostly from Mad8 file
# emitance normalized in e-7 m
set match(emitt_x) 50
set match(emitt_y) 0.3
# energy spread normalized in %, negative means flat distribution, positive gaussian distribution, maxed at 3 sigma
set match(e_spread) 0.0
# beam long in e-6 m
set match(sigma_z) 300
set match(phase) 0.0
set e_initial 1.3 ;# design beam energy [GeV]
set e0_start 1.3 ;# [GeV]
set match(charge)  6e9
set charge 6e9

#######################
#Add beam line set up #
#######################
set beamline_name "ATF2_v5.2"
##########  10x1 optics  ########
#array set match {beta_x 6.848560987444 alpha_x 1.108024744392 beta_y 2.935758991906 alpha_y -1.907222941526}
##########  10x0.5 optics  ########
array set match {beta_x 6.54290 alpha_x 0.99638 beta_y 3.23785 alpha_y -1.9183}

# Use beam incoming jitter
set use_beam_jitter 0
set beam_jitter_x 10; #[um]
set beam_jitter_y 10; #[um]
set beam_jitter_xp 1; #[urad]
set beam_jitter_yp 1; #[urad]

# synchrotron on/off in certain elements
set quad_synrad 0
set mult_synrad 0
set sbend_synrad 0

# CBPM Wakefield
set use_wakefield 0
# use short bunch approximation - 
set use_wakefield_shortbunch 0
set wakefield_shortbunch 300 ;# [um]

# Options for bpms and quadrupoles misalignments
######## COLLIMATOR PARAMETERS ###########
# metode to calculate collimators: 0 Giovani's method
set wmetode 0
### Example collimator number 3
# Maximum half gap
set paramb 12.0e-3
# Minimum half gap
set parama 12.0e-3
# Collimator half width 
set colwidth 12e-3
# Taper part length
set tlength  100e-3
# Flat part length
set flength 100.0e-3
# Material conductivity (for Stainless Steel, Aluminium, Cooper)
#set conduct 1.45e6
#set conduct 4.8e7
set conduct 5.8e7
set offset 0
#######################
# Add beam line       #
#######################
Girder
ReferencePoint -name "START" -angle 1 -sens 1
proc HCORRECTOR { a b c d } { Dipole $a $b $c $d }
proc VCORRECTOR { a b c d } { Dipole $a $b $c $d }
#source $lattice_dir/ATF2_v5.2-MadX-Coll2.tcl
source $lattice_dir/ATF2_v5.2-MadX-Coll_10x0.5.tcl

ReferencePoint -sense -1
BeamlineSet -name $beamline_name

###########################
# Add beam line wakefields#
###########################
set use_wakefield 0

if {$use_wakefield} {
    source $script_dir/Frameworks/ATF2_studies/scripts/wakefield.tcl
}
# use short bunch approximation - 
set use_wakefield_shortbunch 0
set wakefield_shortbunch 300 ;# [um]
#######################
# Setting up the beam #
#######################

set scripts $script_common_dir/scripts  ; # This is necessary for compatibility

# load RF and injection scripts:
source $script_common_dir/scripts/init.tcl;

# loading of the beam making scripts
source $script_common_dir/scripts/ilc_beam.tcl;

# Total number of particles
set n_total [expr $n * $n_slice]

# make initial perfect beam
make_beam_many beam0 $n_slice $n
FirstOrder 1
make_beam_many beam1 $n_slice $n
FirstOrder 1

set charge $dcharge
make_beam_many beam2 $n_slice $n
FirstOrder 1

BeamAddOffset -beam beam0 -x $bunch_offset_x -y $bunch_offset_y -angle_x $bunch_offset_xp -angle_y $bunch_offset_yp

Octave {
    B1 = placet_get_beam("beam1");
    B1(:,1) -= ($deltae) / 100.0;
    placet_set_beam("beam1", B1);
}
 
##############################
# Apply incoming beam jitter #  
##############################

#Octave {
#    if ($use_beam_jitter)
#    Tcl_Eval("BeamAddOffset -beam electron -x $beam_jitter_x -y $beam_jitter_y -angle_x $beam_jitter_xp -angle_y $beam_jitter_yp")
#    printf("Incoming beam jitter is considered \n");  
#    else 
#    end
    
#}
    	     
############
# Tracking #
############

#######################################
# Tracking                            #
#######################################
set offset2 0
#DispersionFreeEmittance -on
 
Octave {
    if exist('../R_quad_${deltae}_${dcharge}.dat', "file")
    disp('Loading Response matrix ...');
    load '../R_quad_${deltae}_${dcharge}.dat';
    else
    disp('Computing Response matrix ...');
    Response.Cx = placet_get_name_number_list("$beamline_name", "ZH*");
    Response.Cy = placet_get_name_number_list("$beamline_name", "ZV*");
    Response.Cqx = placet_get_number_list("$beamline_name", "quadrupole")(end-43:end); % only the last 44 quads are on movers (I believe...)
    Response.Cqy = placet_get_number_list("$beamline_name", "quadrupole")(end-43:end);
    Response.Bpms = placet_get_number_list("$beamline_name", "bpm");
    
    # picks all correctors preceeding the last bpm, and all bpms following the first corrector
    Response.Cx = Response.Cx(Response.Cx < Response.Bpms(end));
    Response.Cy = Response.Cy(Response.Cy < Response.Bpms(end));
    Response.Cqx = Response.Cqx(Response.Cqx < Response.Bpms(end));
    Response.Cqy = Response.Cqy(Response.Cqy < Response.Bpms(end));
    Response.Bpms = Response.Bpms(Response.Bpms > max(Response.Cx(1), Response.Cy(1)));
    Response.Bpms = Response.Bpms(Response.Bpms > max(Response.Cqx(1), Response.Cqy(1)));

    oldBpmRes = placet_element_get_attribute("$beamline_name", Response.Bpms, "resolution");
    placet_element_set_attribute("$beamline_name", Response.Bpms, "resolution", 0.0);
    
     # gets the energy at each corrector
    Response.Ex = placet_element_get_attribute("$beamline_name", Response.Cx, "e0");
    Response.Ey = placet_element_get_attribute("$beamline_name", Response.Cy, "e0");
    
    # compute response matrices
    placet_test_no_correction("$beamline_name", "beam0", "Zero");
    Response.B0 = placet_get_bpm_readings("$beamline_name", Response.Bpms);
    Response.R0x = placet_get_response_matrix_attribute ("$beamline_name", "beam0", Response.Bpms, "x", Response.Cx, "strength_x", "Zero");
    Response.R0y = placet_get_response_matrix_attribute ("$beamline_name", "beam0", Response.Bpms, "y", Response.Cy, "strength_y", "Zero");
    Response.R0qx = placet_get_response_matrix_attribute ("$beamline_name", "beam0", Response.Bpms, "x", Response.Cqx, "x", "Zero");
    Response.R0qy = placet_get_response_matrix_attribute ("$beamline_name", "beam0", Response.Bpms, "y", Response.Cqy, "y", "Zero");
    
    placet_test_no_correction("$beamline_name", "beam1", "Zero");
    Response.B1 = placet_get_bpm_readings("$beamline_name", Response.Bpms) - Response.B0;
    Response.R1x = placet_get_response_matrix_attribute ("$beamline_name", "beam1", Response.Bpms, "x", Response.Cx, "strength_x", "Zero") - Response.R0x;
    Response.R1y = placet_get_response_matrix_attribute ("$beamline_name", "beam1", Response.Bpms, "y", Response.Cy, "strength_y", "Zero") - Response.R0y;
    Response.R1qx = placet_get_response_matrix_attribute ("$beamline_name", "beam1", Response.Bpms, "x", Response.Cqx, "x", "Zero") - Response.R0qx;
    Response.R1qy = placet_get_response_matrix_attribute ("$beamline_name", "beam1", Response.Bpms, "y", Response.Cqy, "y", "Zero") - Response.R0qy;
    
		placet_test_no_correction("$beamline_name", "beam2", "Zero");
    Response.B2 = placet_get_bpm_readings("$beamline_name", Response.Bpms) - Response.B0;
    Response.R2x = placet_get_response_matrix_attribute ("$beamline_name", "beam2", Response.Bpms, "x", Response.Cx, "strength_x", "Zero") - Response.R0x;
    Response.R2y = placet_get_response_matrix_attribute ("$beamline_name", "beam2", Response.Bpms, "y", Response.Cy, "strength_y", "Zero") - Response.R0y;
    Response.R2qx = placet_get_response_matrix_attribute ("$beamline_name", "beam2", Response.Bpms, "x", Response.Cqx, "x", "Zero") - Response.R0qx;
    Response.R2qy = placet_get_response_matrix_attribute ("$beamline_name", "beam2", Response.Bpms, "y", Response.Cqy, "y", "Zero") - Response.R0qy;

    placet_element_set_attribute("$beamline_name", Response.Bpms, "resolution", oldBpmRes);
		
    save -text '../R_quad_${deltae}_${dcharge}.dat' Response;
    end
}

proc my_survey {} {
  global machine sigma sigmap bpmres beamline_name nm
  Octave {
    randn("seed", $nm * 12345);
      
    BPMs = placet_get_number_list("$beamline_name", "bpm");
    placet_element_set_attribute("$beamline_name", BPMs, "x", randn(size(BPMs)) * $sigma);
    placet_element_set_attribute("$beamline_name", BPMs, "y", randn(size(BPMs)) * $sigma);	
    
    CORs = placet_get_number_list("$beamline_name", "dipole");
    placet_element_set_attribute("$beamline_name", CORs, "strength_x", 0.0);
    placet_element_set_attribute("$beamline_name", CORs, "strength_y", 0.0);
    
    QUAs = placet_get_number_list("$beamline_name", "quadrupole");
    placet_element_set_attribute("$beamline_name", QUAs, "x", randn(size(QUAs)) * $sigma);
    placet_element_set_attribute("$beamline_name", QUAs, "y", randn(size(QUAs)) * $sigma);    
  }
}

if { $machine == "all" } {
    set machine_start 1
    set machine_end $nm
} {
    set machine_start $machine
    set machine_end $machine
}

Octave {
    E0=0;
    E1=0;
    E2=0;
    E3=0;
    Bpm0=0;
    Bpm1=0;
    EmittX_hist=zeros($nm, 4);
    EmittY_hist=zeros($nm, 4);
}

Octave {
    [E,B] = placet_test_no_correction("$beamline_name", "beam0", "Zero", "%s %E %dE %ex %ey %sx %sy");
}


#  save beam on different positions
proc reset_global_vars {} {
    global dump_index
    set dump_index 0
}

proc dump_beam {} {
  global dump_index
  set dumpnames {inj_in.dump inj_out.dump corr_out.dump bc1_out.dump lin1_out.dump  bc2_out.dump lin2_out.dump}
  BeamDump -file [lindex  $dumpnames $dump_index]
  incr dump_index
}

for {set machine $machine_start} {$machine <= $machine_end} {incr machine} {
    
    puts "MACHINE: $machine/$nm"
    
    my_survey
    
    Octave {
	disp("TRACKING...");
	[E,B] = placet_test_no_correction("$beamline_name", "beam0", "None", "%s %E %dE %ex %ey %sx %sy");
	E0 += E;
	EmittX_hist($machine, 1) = E(end-1,6);
	EmittY_hist($machine, 1) = E(end-1,7);
	 
	disp("1-TO-1 CORRECTION");
	  nCx = length(Response.Cx)+length(Response.Cqx);
	  nCy = length(Response.Cy)+length(Response.Cqy);
 	  R0x = [ Response.R0x Response.R0qx ];
 	  R0y = [ Response.R0y Response.R0qy ];
	  for i=1:3
	    B0 = placet_get_bpm_readings("$beamline_name", Response.Bpms);
	    B0 -= Response.B0;
   	  Cx = -[ R0x ; $beta0 * eye(nCx) ] \ [ B0(:,1) ; zeros(nCx,1) ];
	    Cy = -[ R0y ; $beta0 * eye(nCy) ] \ [ B0(:,2) ; zeros(nCy,1) ];
	    placet_element_vary_attribute("$beamline_name", Response.Cx, "strength_x", Cx(1:length(Response.Cx)));
	    placet_element_vary_attribute("$beamline_name", Response.Cy, "strength_y", Cy(1:length(Response.Cy)));
	    placet_element_vary_attribute("$beamline_name", Response.Cqx, "x", Cx(length(Response.Cx)+(1:length(Response.Cqx))));
	    placet_element_vary_attribute("$beamline_name", Response.Cqy, "y", Cy(length(Response.Cy)+(1:length(Response.Cqy))));
	    [E,B] = placet_test_no_correction("$beamline_name", "beam0", "None", 1, 0, Response.Bpms(end), "%s %E %dE %ex %ey %sx %sy");
	  end

	E1 += E;
	EmittX_hist($machine, 2) = E(end-1,6);
	EmittY_hist($machine, 2) = E(end-1,7);

	disp("DFS CORRECTION");
	  nCx = length(Response.Cx)+length(Response.Cqx);
	  nCy = length(Response.Cy)+length(Response.Cqy);
 	  R0x = [ Response.R0x Response.R0qx ];
 	  R0y = [ Response.R0y Response.R0qy ];
 	  R1x = [ Response.R1x Response.R1qx ];
 	  R1y = [ Response.R1y Response.R1qy ];
	  for i=1:3
	    B0 = placet_get_bpm_readings("$beamline_name", Response.Bpms);
	    placet_test_no_correction("$beamline_name", "beam1", "None", 1, 0, Response.Bpms(end));
	    B1 = placet_get_bpm_readings("$beamline_name", Response.Bpms) - B0;
	    B0 -= Response.B0;
	    B1 -= Response.B1;
      Cx = -[ R0x ; $wgt1 * R1x ; $beta1 * eye(nCx) ] \ [ B0(:,1) ; $wgt1 * B1(:,1) ; zeros(nCx,1) ];
	    Cy = -[ R0y ; $wgt1 * R1y ; $beta1 * eye(nCy) ] \ [ B0(:,2) ; $wgt1 * B1(:,2) ; zeros(nCy,1) ];
	    placet_element_vary_attribute("$beamline_name", Response.Cx, "strength_x", Cx(1:length(Response.Cx)));
	    placet_element_vary_attribute("$beamline_name", Response.Cy, "strength_y", Cy(1:length(Response.Cy)));
	    placet_element_vary_attribute("$beamline_name", Response.Cqx, "x", Cx(length(Response.Cx)+(1:length(Response.Cqx))));
	    placet_element_vary_attribute("$beamline_name", Response.Cqy, "y", Cy(length(Response.Cy)+(1:length(Response.Cqy))));
	    [E,B] = placet_test_no_correction("$beamline_name", "beam0", "None", 1, 0, Response.Bpms(end), "%s %E %dE %ex %ey %sx %sy");
	  end

	E2 += E;
	EmittX_hist($machine, 3) = E(end-1,6);
	EmittY_hist($machine, 3) = E(end-1,7);

	disp("WFS CORRECTION");
	  nCx = length(Response.Cx)+length(Response.Cqx);
	  nCy = length(Response.Cy)+length(Response.Cqy);
 	  R0x = [ Response.R0x Response.R0qx ];
 	  R0y = [ Response.R0y Response.R0qy ];
 	  R1x = [ Response.R1x Response.R1qx ];
 	  R1y = [ Response.R1y Response.R1qy ];
 	  R2x = [ Response.R2x Response.R2qx ];
 	  R2y = [ Response.R2y Response.R2qy ];
	  for i=1:3
	    B0 = placet_get_bpm_readings("$beamline_name", Response.Bpms);
	    % DFS
	    placet_test_no_correction("$beamline_name", "beam1", "None", 1, 0, Response.Bpms(end));
	    B1 = placet_get_bpm_readings("$beamline_name", Response.Bpms) - B0;
      % WFS
	    placet_test_no_correction("$beamline_name", "beam2", "None", 1, 0, Response.Bpms(end));
	    B2 = placet_get_bpm_readings("$beamline_name", Response.Bpms) - B0;
	    %
	    B0 -= Response.B0;
	    B1 -= Response.B1;
	    B2 -= Response.B2;
      Cx = -[ R0x ; $wgt1 * R1x ; $wgt2 * R2x ; $beta2 * eye(nCx) ] \ [ B0(:,1) ; $wgt1 * B1(:,1) ; $wgt2 * B2(:,1) ; zeros(nCx,1) ];
	    Cy = -[ R0y ; $wgt1 * R1y ; $wgt2 * R2y ; $beta2 * eye(nCy) ] \ [ B0(:,2) ; $wgt1 * B1(:,2) ; $wgt2 * B2(:,2) ; zeros(nCy,1) ];
	    placet_element_vary_attribute("$beamline_name", Response.Cx, "strength_x", Cx(1:length(Response.Cx)));
	    placet_element_vary_attribute("$beamline_name", Response.Cy, "strength_y", Cy(1:length(Response.Cy)));
	    placet_element_vary_attribute("$beamline_name", Response.Cqx, "x", Cx(length(Response.Cx)+(1:length(Response.Cqx))));
	    placet_element_vary_attribute("$beamline_name", Response.Cqy, "y", Cy(length(Response.Cy)+(1:length(Response.Cqy))));
	    [E,B] = placet_test_no_correction("$beamline_name", "beam0", "None", 1, 0, Response.Bpms(end), "%s %E %dE %ex %ey %sx %sy");
	  end

  save -text ${beamline_name}_beam_${machine}.dat B;

	E3 += E;
	EmittX_hist($machine, 4) = E(end-1,6);
	EmittY_hist($machine, 4) = E(end-1,7);
    }
}

set nm [expr $machine_end - $machine_start + 1]

Octave {
    E0 /= $nm;
    E1 /= $nm;
    E2 /= $nm;
    E3 /= $nm;
		save -text ${beamline_name}_sigma_histx_w_${wgt1}_${wgt2}_b_${beta0}_${beta1}_${beta2}_c_${dcharge}_n_${nm}.dat EmittX_hist
		save -text ${beamline_name}_sigma_histy_w_${wgt1}_${wgt2}_b_${beta0}_${beta1}_${beta2}_c_${dcharge}_n_${nm}.dat EmittY_hist
    save -text ${beamline_name}_emitt_no_n_${nm}.dat E0
    save -text ${beamline_name}_emitt_simple_b_${beta0}_n_${nm}.dat E1
    save -text ${beamline_name}_emitt_dfs_w_${wgt1}_b_${beta0}_${beta1}_n_${nm}.dat E2
    save -text ${beamline_name}_emitt_wfs_w_${wgt1}_${wgt2}_b_${beta0}_${beta1}_${beta2}_c_${dcharge}_n_${nm}.dat E3

}

exec echo "set grid lt 0 \n \\
plot '${beamline_name}_emitt_simple_b_${beta0}_n_${nm}.dat' u 1:4 w l ti 'simple cor', \\
     '${beamline_name}_emitt_dfs_w_${wgt1}_b_${beta0}_${beta1}_n_${nm}.dat' u 1:4 w l ti 'dfs cor', \\
     '${beamline_name}_emitt_wfs_w_${wgt1}_${wgt2}_b_${beta0}_${beta1}_${beta2}_c_${dcharge}_n_${nm}.dat' u 1:4 w l ti 'wfs cor' " | gnuplot -persist


