function ICM = incremental_covariance_matrix(x, ICM)
    diff = x - ICM.average.average;
    ICM.average = incremental_average(x, ICM.average);
    ICM.variance = incremental_average(diff * diff', ICM.variance);
    A = ICM.variance.average;
    ICM.covariance = (A+A')/2;
endfunction 

