function [Ibend,Itrim,Iquad,Isext,Icorr]=get_ATF2_I(setfile)

global script_dir

%
% [Ibend,Iquad,Isext,Icorr]=get_ATF2_I(setfile)
%
% Get ATF2 magnet currents
%
% INPUT:
%
%   setfile = SETDATA file (i.e. 'set09feb20_0025.dat')
%
% OUTPUTs:
%
%   Ibend  = bend currents (amps)
%   Itrim  = bend trim currents (amps)
%   Iquad  = quadrupole currents (amps)
%   Isext  = sextupole + skew sextupole currents (amps)
%   Icorr  = corrector currents (amps)

% device names

bname=[ ...
  'BH1R   ';'BS1X   ';'BS2X   ';'BS3X   ';'BH1X   ';'BH2X   '; ...
  'BH3X   ';'B5FF   ';'B2FF   ';'B1FF   ';'BDUMP  '];
tname=[ ...
  'QM6R.1 ';'QM7R.1 ';'BS1X.1 '];
qname=[ ...
  'QM6R   ';'QM7AR  ';'QS1X   ';'QF1X   ';'QD2X   ';'QF3X   '; ...
  'QF4X   ';'QD5X   ';'QF6X   ';'QS2X   ';'QF7X   ';'QD8X   '; ...
  'QF9X   ';'QK1X   ';'QD10X  ';'QF11X  ';'QK2X   ';'QD12X  '; ...
  'QF13X  ';'QD14X  ';'QF15X  ';'QK3X   ';'QD16X  ';'QF17X  '; ...
  'QK4X   ';'QD18X  ';'QF19X  ';'QD20X  ';'QF21X  ';'QM16FF '; ...
  'QM15FF ';'QM14FF ';'QM13FF ';'QM12FF ';'QM11FF ';'QD10BFF'; ...
  'QD10AFF';'QF9BFF ';'QF9AFF ';'QD8FF  ';'QF7FF  ';'QD6FF  '; ...
  'QF5BFF ';'QF5AFF ';'QD4BFF ';'QD4AFF ';'QF3FF  ';'QD2BFF '; ...
  'QD2AFF ';'QF1FF  ';'QD0FF  '];
sname=[ ...
  'SF6FF';'SK4FF';'SK3FF';'SF5FF';'SD4FF';'SK2FF';'SK1FF';'SF1FF';'SD0FF'];
cname=[ ...
  'ZH100R ';'ZH101R ';'ZX1X   ';'ZH1X   ';'ZH2X   ';'ZX2X   '; ...
  'ZX3X   ';'ZH3X   ';'ZH4X   ';'ZH5X   ';'ZH6X   ';'ZH7X   '; ...
  'ZH8X   ';'ZH9X   ';'ZH10X  ';'ZH1FF  '; ...
  'ZV100R ';'ZV1X   ';'ZV2X   ';'ZV3X   ';'ZV4X   ';'ZV5X   '; ...
  'ZV6X   ';'ZV7X   ';'ZV8X   ';'ZV9X   ';'ZV10X  ';'ZV11X  '; ...
  'ZV1FF  '];
names=[bname;tname;qname;sname;cname];

[Nb,dummy]=size(bname);
[Nt,dummy]=size(tname);
[Nq,dummy]=size(qname);
[Ns,dummy]=size(sname);
[Nc,dummy]=size(cname);

Ibend=zeros(Nb,1);
Itrim=zeros(Nt,1);
Iquad=zeros(Nq,1);
Isext=zeros(Ns,1);
Icorr=zeros(Nc,1);
I=zeros(Nb+Nt+Nq+Ns+Nc,1);

% READ the setdata file

fid=fopen(setfile,'rt');
while (1)
  s=fgetl(fid);
  if (~isstr(s)),break,end
  [t,r]=strtok(s);
  if (strcmp(t,'FFMAG'))
    [t,r]=strtok(r);
  end
  n=strmatch(t,names,'exact');
  if (~isempty(n))
    I(n)=str2num(strtok(r));
  end
end
fclose(fid);

% assign currents

Ibend=I(1:Nb);
Itrim=I(Nb+1:Nb+Nt);
Iquad=I(Nb+Nt+1:Nb+Nt+Nq);
Isext=I(Nb+Nt+Nq+1:Nb+Nt+Nq+Ns);
Icorr=I(Nb+Nt+Nq+Ns+1:Nb+Nt+Nq+Ns+Nc);

% assign currents to family members

id1=strmatch('BS1X   ',bname);
id2=strmatch('BS2X   ',bname);
Ibend(id2)=Ibend(id1);

id1=strmatch('BH1X   ',bname);
id2=strmatch('BH2X   ',bname);
Ibend(id2)=Ibend(id1);

id1=strmatch('QF15X  ',qname);
id2=strmatch('QF13X  ',qname);
Iquad(id2)=Iquad(id1);

return
