function [hcorr,hkick,hcoef,vcorr,vkick,vcoef]=ATFDRbump3(K,N,S,energy,fflag)

%[tt,K,N,L,P,A,T,E,FDN,rmat,S]=xtffr2mat('ATFDR_rmat.tape');
idm=strmatch('MONI',K);
Nm=length(idm);
idh=strmatch('HKIC',K);
Nh=length(idh);
idv=strmatch('VKIC',K);
Nv=length(idv);
r=S(end)/(2*pi);

Icorr=ones(Nh+Nv,1);
KL=get_DR_corrs(1,energy,Icorr,fflag); % rad/amp
hccal=KL(1:Nh);
vccal=KL(Nh+1:end);

% remove "special" correctors

id=strmatch('ZH100R',N);id=find(idh==id);idh(id)=[];hccal(id)=[];
id=strmatch('ZH101R',N);id=find(idh==id);idh(id)=[];hccal(id)=[];
id=strmatch('ZH102R',N);id=find(idh==id);idh(id)=[];hccal(id)=[];
id=strmatch('ZV100R',N);id=find(idv==id);idv(id)=[];vccal(id)=[];
Nh=length(idh);
Nv=length(idv);

hcorr=zeros(Nm,3); % ZH unit numbers
hkick=zeros(Nm,3); % horizontal kicks (mrad/mm)
hcoef=zeros(Nm,3); % horizontal bump coefficients (amp/mm)
idhc=zeros(1,3);
disp(' ')
for n=1:Nm
  id0=idm(n);
  s=sin(0.5*(S(idh)-S(id0))/r);
  [t,id2]=min(abs(s));
  id1=id2-1;if (id1<1),id1=Nh;end
  idhc(1)=idh(id1);
  idhc(2)=idh(id2);
  id3=id2+1;if (id3>Nh),id3=1;end
  idhc(3)=idh(id3);
  [corr,kick]=MADbump3(id0,idhc,1,N);
  hcorr(n,:)=corr;
  hkick(n,:)=kick;
  hcoef(n,1)=kick(1)/hccal(id1);
  hcoef(n,2)=kick(2)/hccal(id2);
  hcoef(n,3)=kick(3)/hccal(id3);
  disp(sprintf('   %2d %3d %7.4f %3d %7.4f %3d %7.4f', ...
    n,corr(1),hcoef(n,1),corr(2),hcoef(n,2),corr(3),hcoef(n,3)))
end

vcorr=zeros(Nm,3); % ZV unit numbers
vkick=zeros(Nm,3); % vertical kicks (mrad/mm)
vcoef=zeros(Nm,3); % vertical bump coefficients (amp/mm)
idvc=zeros(1,3);
disp(' ')
for n=1:Nm
  id0=idm(n);
  s=sin(0.5*(S(idv)-S(id0))/r);
  [t,id2]=min(abs(s));
  id1=id2-1;if (id1<1),id1=Nv;end
  idvc(1)=idv(id1);
  idvc(2)=idv(id2);
  id3=id2+1;if (id3>Nv),id3=1;end
  idvc(3)=idv(id3);
  [corr,kick]=MADbump3(id0,idvc,0,N);
  vcorr(n,:)=corr;
  vkick(n,:)=kick;
  vcoef(n,1)=kick(1)/vccal(id1);
  vcoef(n,2)=kick(2)/vccal(id2);
  vcoef(n,3)=kick(3)/vccal(id3);
  disp(sprintf('   %2d %3d %7.4f %3d %7.4f %3d %7.4f', ...
    n,corr(1),vcoef(n,1),corr(2),vcoef(n,2),corr(3),vcoef(n,3)))
end
disp(' ')

return
