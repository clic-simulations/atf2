function [name, strength] = ATF2model(setfile,fflag)
%
% ATF2model(setfile,QMFFrev,fflag)
%
% Generate MAD model for ATF EXT from a SETDATA file
%
% INPUTs:
%
%   setfile = SETDATA file (i.e. 'SET07FEB2_0949.DAT')
%   fflag   = 1 (use Kubo's quadrupole fudge factors) or 0 (don't use fudges)

% get magnet currents from SETDATA file

[Ibend,Itrim,Iquad,Isext,Icorr]=get_ATF2_I(setfile);
energy=DR_energy(1,Ibend(1));

% compute magnet strengths

KL=get_ATF2_quads(1,energy,Iquad,fflag);
sKL=get_ATF2_sexts(1,energy,Isext,fflag);

% write MAD patch file

qname=[ ...
  'QM6RX  ';'QM7RX  ';'QS1X   ';'QF1X   ';'QD2X   ';'QF3X   '; ...
  'QF4X   ';'QD5X   ';'QF6X   ';'QS2X   ';'QF7X   ';'QD8X   '; ...
  'QF9X   ';'QK1X   ';'QD10X  ';'QF11X  ';'QK2X   ';'QD12X  '; ...
  'QF13X  ';'QD14X  ';'QF15X  ';'QK3X   ';'QD16X  ';'QF17X  '; ...
  'QK4X   ';'QD18X  ';'QF19X  ';'QD20X  ';'QF21X  ';'QM16FF '; ...
  'QM15FF ';'QM14FF ';'QM13FF ';'QM12FF ';'QM11FF ';'QD10BFF'; ...
  'QD10AFF';'QF9BFF ';'QF9AFF ';'QD8FF  ';'QF7FF  ';'QD6FF  '; ...
  'QF5BFF ';'QF5AFF ';'QD4BFF ';'QD4AFF ';'QF3FF  ';'QD2BFF '; ...
  'QD2AFF ';'QF1FF  ';'QD0FF  '];
[Nq,dummy]=size(qname);

sname=[ ...
  'SF6FF';'SK4FF';'SK3FF';'SF5FF';'SD4FF';'SK2FF';'SK1FF';'SF1FF';'SD0FF'];
[Ns,dummy]=size(sname);

%fid=fopen(strcat(substr(setfile,1,15),'_conv.dat'),'wt');
%fprintf(fid,'! %s\n',deblank(setfile));
%for n=1:Nq
%  fprintf(fid,'  KL%s %10.6f\n',deblank(qname(n,:)),KL(n));
%end
%for n=1:Ns
%  fprintf(fid,'  KL%s %10.6f\n',deblank(sname(n,:)),sKL(n));
%end

%fprintf(fid,'  SET, E0, %f\n',energy);
%fprintf(fid,'  RETURN\n');
%fclose(fid);

% output
name = [(qname(:,:)); (sname(:,:))];
strength = [KL; sKL];

% run MAD
% [status,result]=system('..\MAD8\RunMAD ATF2.mad');

end
