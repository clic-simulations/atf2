function ATFDRmodel(setfile,htune,vtune,fflag)
%
% ATFDRmodel(setfile,htune,vtune,fflag)
%
% Generate MAD model for ATF DR from a SETDATA file
%
% INPUTs:
%
%   setfile = SETDATA file (i.e. 'SET09FEB17_1126.DAT')
%   htune   = horizontal tune (i.e. 15.180)
%   vtune   = horizontal tune (i.e. 8.570)
%   fflag   = 1 (use Kubo's quadrupole fudge factors) or 0 (don't use fudges)

idq=[1:100]';    % pointers to normal quads in trim list
idsq=[101:168]'; % pointers to skew quads in trim list

% get magnet currents from SETDATA file

[Ibend,Iquad,Isext,Itrim,Icorr]=get_DR_I(setfile);
energy=DR_energy(1,Ibend);

% compute magnet strengths

[dummy,qKL]=get_DR_quads(1,energy,Iquad,Itrim(idq),fflag);
sKL=get_DR_sexts(1,energy,Isext,fflag);
sqKL=get_DR_skews(1,energy,Itrim(idsq),fflag);

% write MAD patch file

qparm=[];
Nqf1=28;
for n=1:Nqf1
  name=sprintf('KLQF1R_%d',n);
  if (n<10),name=[name,' '];end
  qparm=[qparm;name];
end
Nqf2=26;
for n=1:Nqf2
  name=sprintf('KLQF2R_%d',n);
  if (n<10),name=[name,' '];end
  qparm=[qparm;name];
end
Nqm=23;
for n=1:Nqm
  name1=sprintf('KLQM%dR_1',n);
  name2=sprintf('KLQM%dR_2',n);
  if (n<10)
    name1=[name1,' '];
    name2=[name2,' '];
  end
  qparm=[qparm;name1;name2];
end

sparm=['KLSF1R';'KLSD1R'];

sqparm=[];
Nsqf=34;
for n=1:Nsqf
  name=sprintf('KLSQSF_%d',n);
  if (n<10),name=[name,' '];end
  sqparm=[sqparm;name];
end
Nsqd=34;
for n=1:Nsqd
  name=sprintf('KLSQSD_%d',n);
  if (n<10),name=[name,' '];end
  sqparm=[sqparm;name];
end

fid=fopen('SETDATA.mad','wt');
fprintf(fid,'! %s\n',deblank(setfile));
[Nq,dummy]=size(qparm);
for n=1:Nq
  fprintf(fid,'  SET, %s, %10.6f\n',qparm(n,:),qKL(n));
end
[Ns,dummy]=size(sparm);
for n=1:Ns
  fprintf(fid,'  SET, %s,    %10.6f\n',sparm(n,:),sKL(n));
end
[Nsq,dummy]=size(sqparm);
for n=1:Nsq
  fprintf(fid,'  SET, %s, %10.6f\n',sqparm(n,:),sqKL(n));
end
fprintf(fid,'  SET, E0, %f\n',energy);
if (htune>0),fprintf(fid,'  SET, XTUNE, %f\n',htune);end
if (vtune>0),fprintf(fid,'  SET, YTUNE, %f\n',vtune);end
fprintf(fid,'  MTUNES\n');
fprintf(fid,'  RETURN\n');
fclose(fid);

% run MAD

[status,result]=system('..\MAD8\RunMAD ATFDR.mad');

ans=prompt(' Create 3-bump coefficients (~10 minutes)?','yn','n');
if (strcmp(ans,'y'))
  
% get the model

  [tt,K,N,L,P,A,T,E,FDN,rmat,S]=xtffr2mat('rmat.tape');

% compute the bump coefficients (takes ~7-8 minutes)

  tic
  [hcorr,hkick,hcoef,vcorr,vkick,vcoef]=ATFDRbump3(K,N,S,energy,fflag);
  t=toc;
  disp(sprintf('   Elapsed time: %.1f seconds',t))

% save the bump coefficients

  ans=prompt(' Create MADin.mat?','yn','y');
  if (strcmp(ans,'y'))
    [pathstr,name,ext,versn]=fileparts(setfile);
    fname=strcat('MADin-',name,'.mat');
    cmd=['save ',fname,' hcorr hkick hcoef vcorr vkick vcoef'];
    eval(cmd)
    disp(['   Created: ',fname])
    disp(' ')
  end
end

return
