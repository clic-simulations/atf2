function [corr,kick]=MADbump3(idm,idc,hflag,N)

corr=zeros(1,3);
for n=1:3
  m=idc(n);
  name=deblank(N(m,:));
  corr(n)=str2double(name(3:end-1));
end

mname=deblank(N(idm,:));
cname1=deblank(N(idc(1),:));
cname2=deblank(N(idc(2),:));
cname3=deblank(N(idc(3),:));
if (hflag)
  cxy='X';
else
  cxy='Y';
end

fid=fopen('bump.mad','wt');
fprintf(fid,'%s\n','ASSIGN, PRINT="bump.print"');
fprintf(fid,'%s\n','ASSIGN, ECHO="bump.echo"');
fprintf(fid,'%s\n','OPTION, -INTER, -ECHO, DOUBLE, VERIFY');
fprintf(fid,'%s\n','CALL, FILENAME="ATFDR.saveline"');
fprintf(fid,'SET, %s[KICK], 1.E-6\n',cname1);
fprintf(fid,'SET, %s[KICK], 1.E-6\n',cname2);
fprintf(fid,'SET, %s[KICK], 1.E-6\n',cname3);
fprintf(fid,'%s\n','USE, ATFDR');
fprintf(fid,'%s\n','CELL, ORBIT');
fprintf(fid,'  VARY, %s[KICK], STEP=1.E-5\n',cname1);
fprintf(fid,'  VARY, %s[KICK], STEP=1.E-5\n',cname2);
fprintf(fid,'  VARY, %s[KICK], STEP=1.E-5\n',cname3);
fprintf(fid,'  CONSTR, "%s", %s=0.001\n',mname,cxy);
fprintf(fid,'  CONSTR, %s, %s=0, P%s=0\n',cname3,cxy,cxy);
fprintf(fid,'%s\n','  LMDIF');
fprintf(fid,'%s\n','  MIGRAD');
fprintf(fid,'%s\n','ENDMATCH');
fprintf(fid,'VALUE, %s[KICK]\n',cname1);
fprintf(fid,'VALUE, %s[KICK]\n',cname2);
fprintf(fid,'VALUE, %s[KICK]\n',cname3);
fprintf(fid,'%s\n','STOP');
fclose(fid);

% run MAD

[status,result]=system('..\MAD8\RunMAD bump.mad');

% get the corrector kicks

[status,result]=system('grep "Value of expression" bump.echo');
kick=zeros(1,3);
for n=1:3
  for m=1:6
    [t,result]=strtok(result);
  end
  kick(n)=str2double(t);
end

return
