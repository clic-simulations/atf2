function [names,x,y,roll]=get_ATF2_pos(setfile)

global script_dir

% sign conventions for setfile
% looking downstream (IP direction):
% +x is right
% +y is down
% +roll is clockwise

names = [ ...
  'QM16FF '; ...
  'QM15FF ';'QM14FF ';'QM13FF ';'QM12FF ';'QM11FF ';'QD10BFF'; ...
  'QD10AFF';'QF9BFF ';'SF6FF  ';'QF9AFF ';'QD8FF  ';'QF7FF  ';'QD6FF  '; ...
  'QF5BFF ';'SF5FF  ';'QF5AFF ';'QD4BFF ';'SD4FF  ';'QD4AFF ';'QF3FF  ';'QD2BFF '; ...
  'QD2AFF ';'SF1FF  ';'QF1FF  ';'SD0FF  ';'QD0FF  '];

% READ the setdata file

fid=fopen(setfile,'rt');
a=1;
i=1;
while (1)
  s=fgetl(fid);
  if (~isstr(s)),break,end
  [t,r]=strtok(s);
  if (strcmp(t,'QMOV'))
      [t,r]=strtok(r);
      r = str2num(r);
      if (mod(a,3)==1)
          x(i)=r;
      elseif (mod(a,3)==2)
          y(i)=r;
      else
          roll(i)=r;
          i++;
      end
      a++;
  end
end
