% Function to find the initial covariance matrix P for the experiment at
% FACET
%
% Juergen Pfingstner
% 17th of January 2012

function [ P ] = find_P_init( scaling, lambda )
%
% Function to find the initial covariance matrix P for the experiment at
% FACET
%
% Input: 
%      scaling ... scaling factors of the excitation 
%      lambda  ... learning factor of the RLS algorithm
%
% Output:
%       P      ... Initial covariance matrix
%

%%%%%%%%%%%%%%%%%%%%
% Define parameter %
%%%%%%%%%%%%%%%%%%%%

epsilon = 0.01;
P = 0.1.*eye(length(scaling)+1);
norm_P_old = norm(P,'fro');
run = 1;
n = length(scaling);
j = 1;

while(run == 1)
    % Update P for 100 time_steps
    for i = 1:100
        % Create new excitations
        phi = [rand(n,1).*scaling; 1];
        
        % Calc K
        b = lambda + phi' * P * phi;
        K = (P * phi) ./ b;

        % Calc  P
        P = ((eye(n+1) - K * phi')*P)./lambda;

    end
    
    norm_P = norm(P,'fro');
    change_P = abs(norm_P-norm_P_old)/norm_P_old;
    fprintf(1, 'Iteration %d, value: %g\n', 100*j, change_P);
    if(change_P < epsilon)
        run = 0;
    else
        norm_P_old = norm_P;
        j = j+1;
    end
    
    % Stop if calculation takes to long
    if(j > 100)
        run = 0;
    end
end

