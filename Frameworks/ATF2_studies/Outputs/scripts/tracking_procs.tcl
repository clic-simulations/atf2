
proc save_beam_ff_start { } {
    global time_step_index bunch_index beam_name
    BeamDump -file ${beam_name}_ff_start${time_step_index}.dat
    array set beamparams [BeamMeasure]
    save_array beamparams "electron_beampar_ff_start${time_step_index}_$bunch_index"
}

proc save_beam_ff_end { } {
    global time_step_index bunch_index beam_name
    BeamDump -file ${beam_name}${time_step_index}.dat
    array set beamparams [BeamMeasure]
    save_array beamparams "electron_beampar_ff_end${time_step_index}_$bunch_index"
}

proc save_beam_mfb2ff { } {
    global time_step_index bunch_index beam_name
    BeamDump -file ${beam_name}${time_step_index}.dat
    array set beamparams [BeamMeasure]
    save_array beamparams "electron_beampar_mfb2ff${time_step_index}_$bunch_index"
}

proc save_beam_ip { } {
    global time_step_index bunch_index beam_name
    BeamDump -file ${beam_name}_ip${time_step_index}.dat
    array set beamparams [BeamMeasure]
    save_array beamparams "electron_beampar_ip${time_step_index}_$bunch_index"
}

proc add_dispersion_ff_start { } {
    global dispersion_ff_x dispersion_ff_y dispersion_ff_xp dispersion_ff_yp e0
    BeamAddDispersion -eref $e0 -dx ${dispersion_ff_x} -dxprime ${dispersion_ff_xp} -dy ${dispersion_ff_yp} -dyprime ${dispersion_ff_yp}
}

proc nanobpmsetup_add_beam_offset {} {
    global nanoBPMSetupBeamOffsetX nanoBPMSetupBeamOffsetXp nanoBPMSetupBeamOffsetY nanoBPMSetupBeamOffsetYp
    BeamAddOffset -x $nanoBPMSetupBeamOffsetX -y $nanoBPMSetupBeamOffsetY -angle_x $nanoBPMSetupBeamOffsetXp -angle_y $nanoBPMSetupBeamOffsetYp
}
