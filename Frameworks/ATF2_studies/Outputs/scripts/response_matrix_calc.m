% picks all correctors preceeding the last bpm, and all bpms following the first corrector
% In ATF this is already the case (though perhaps not use first two correctors)

Response.Cx = hcorr_index(hcorr_index < bpm_index(end));
Response.Cy = vcorr_index(vcorr_index < bpm_index(end));

% gets the energy at each corrector
Response.Ex = placet_element_get_attribute(beamline_name, Response.Cx, "e0");
Response.Ey = placet_element_get_attribute(beamline_name, Response.Cy, "e0");

%response_bpm_index = bpm_index(bpm_index > max(hcorr_index(1), vcorr_index(1)))
Response.Bpms = bpm_index(bpm_index > max(Response.Cx(1), Response.Cy(1)));

% bpms perfect orbit
Response.B = placet_get_bpm_readings(beamline_name, Response.Bpms);

printf("Calculate the horizontal response matrix\n");
Response.Rx = calc_response_matrix(beamline_name,beam_name,Response.Bpms,Response.Cx,step_size_R_x, 'x');

printf("Calculate the vertical response matrix\n");
Response.Ry = calc_response_matrix(beamline_name,beam_name,Response.Bpms,Response.Cy,step_size_R_y, 'y');

if (use_dfs)
    % setup beam with lower charge and track
    if (dynamic_beam)
        change_energy(dfs_dE);
        source (strcat(script_dir,"/scripts/tracking.m"));
        beam_name_dfs = beam_name;
    else
        placet_test_no_correction(beamline_name,beam_name_dfs,"None");
    end

    Response.BDFS = placet_get_bpm_readings(beamline_name, Response.Bpms) - Response.B;
    
    printf("Calculate the horizontal response matrix with reduced energy\n");
    Response.RxDFS = calc_response_matrix(beamline_name,beam_name_dfs,Response.Bpms,Response.Cx,step_size_R_x, 'x');
    
    printf("Calculate the vertical response matrix with reduced energy\n");
    Response.RyDFS = calc_response_matrix(beamline_name,beam_name_dfs,Response.Bpms,Response.Cy,step_size_R_y, 'y');

    %put charge back
    if (dynamic_beam)
        change_energy(1./dfs_dE); 
    end
end

if (use_wfs)
    
    % setup beam with lower charge and track
    if (dynamic_beam)
        change_charge(wfs_dcharge);
        source (strcat(script_dir,"/scripts/tracking.m"));
        beam_name_wfs = beam_name;
    else
        placet_test_no_correction(beamline_name,beam_name_wfs,"None");
    end

    Response.BWFS = placet_get_bpm_readings(beamline_name, Response.Bpms) - Response.B;
    
    printf("Calculate the horizontal response matrix with reduced charge\n");
    Response.RxWFS = calc_response_matrix(beamline_name,beam_name_wfs,Response.Bpms,Response.Cx,step_size_R_x, 'x');
    
    printf("Calculate the vertical response matrix with reduced charge\n");
    Response.RyWFS = calc_response_matrix(beamline_name,beam_name_wfs,Response.Bpms,Response.Cy,step_size_R_y, 'y');

    %put charge back
    if (dynamic_beam)
        change_charge(1./wfs_dcharge); 
    end
end
save -text "Response.dat" Response;
