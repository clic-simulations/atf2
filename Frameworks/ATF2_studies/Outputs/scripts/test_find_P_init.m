% Script to test the functionality of find_P_init
%
% Juergen Pfingstner
% 17th of January 2012

%%%%%%%%%%%%%
% Parameter %
%%%%%%%%%%%%%

alpha_x = 12;
alpha_y = 6;
lambda_x = 0.9999;
lambda_y = 0.9999;

%%%%%%%%%%%%%%%%%%%%%%%%
% Load scaling factors %
%%%%%%%%%%%%%%%%%%%%%%%%

beta_x_corr = load('beta_x_corr.dat', '-ascii');
beta_y_corr = load('beta_y_corr.dat', '-ascii');

%%%%%%%%%%%%%%%%%
% Create matrix %
%%%%%%%%%%%%%%%%%

P_x = find_P_init(alpha_x./beta_x_corr, lambda_x);
P_y = find_P_init(alpha_y./beta_y_corr, lambda_y);

%%%%%%%%%%%%%%%%%%%%%%
% Analyse the result %
%%%%%%%%%%%%%%%%%%%%%%

figure(1);
plot(diag(P_x), 'b');
grid on;
hold on;
plot(beta_x_corr./500, 'r');

P_x_sim = load('../sysident_run1/seed_1_lambda_0.9999/P_x.dat');
norm(P_x, 'fro')
norm(P_x_sim, 'fro')
norm(P_x_sim-P_x, 'fro')

figure(2);
plot(diag(P_x), 'b');
grid on;
hold on;
plot(diag(P_x_sim), 'r');

