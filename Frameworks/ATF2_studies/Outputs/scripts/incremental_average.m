function IA = incremental_average(x, IA)
    IA.average = IA.average + (x - IA.average) / (IA.N);
    IA.N = IA.N + 1;
end