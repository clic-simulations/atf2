function ICM = init_ICM()
    ICM.average = init_IA();
    ICM.variance = init_IA();
    ICM.covariance = 1;
endfunction