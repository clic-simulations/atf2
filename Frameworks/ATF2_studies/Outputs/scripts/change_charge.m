function change_charge(dcharge)
global charge

reduce_charge = charge*dcharge;
% charge can be a const in Tcl, so unset and reset
Tcl_Eval("unset charge");
Tcl_SetVar("charge",reduce_charge);
charge = reduce_charge;

end