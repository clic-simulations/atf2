# Init the data needed for the system identification
#
# Juergen Pfingstner
# 27th of March 2012

addpath(strcat("./scripts"));

% Choice of the used correctors

if(load_corr == 1)
    corr_index = load(load_corr_name);
    hcorr_index = corr_index.Index.x;
    vcorr_index = corr_index.Index.y;
    [hcorr_index,sort_order_h] = sort(hcorr_index(1:67));
    [vcorr_index,sort_order_v] = sort(vcorr_index(1:67));
else
    hcorr_index = 1:13;
    vcorr_index = 1:13;
end

fid1 = fopen(ident_results_file_name, 'wt');

if(detailed_eval == 1)

    ident_detail_col_x_file_name = sprintf("%s/ident_results_col_x_%s.dat", eval_dir,eval_type_string);
    f_col_x = fopen(ident_detail_col_x_file_name, 'wt');

    ident_detail_col_y_file_name = sprintf("%s/ident_results_col_y_%s.dat", eval_dir,eval_type_string);
    f_col_y = fopen(ident_detail_col_y_file_name, 'wt');

    ident_detail_row_x_file_name = sprintf("%s/ident_results_row_x_%s.dat", eval_dir,eval_type_string);
    f_row_x = fopen(ident_detail_row_x_file_name, 'wt');

    ident_detail_row_y_file_name = sprintf("%s/ident_results_row_y_%s.dat", eval_dir,eval_type_string);
    f_row_y = fopen(ident_detail_row_y_file_name, 'wt');

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Init and load the data for the system identification %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(1, "Load and init data for system identification ... ");

excit_x_data = load(excit_x_file_name);
excit_y_data = load(excit_y_file_name);
meas_x_data = load(meas_x_file_name);
meas_y_data = load(meas_y_file_name);
[nr_time_steps, temp] = size(meas_x_data);

R_x = load(R_x_file_name);
R_x = R_x(:,hcorr_index);
norm2_R_x = norm(R_x, 'fro');
%file_name_disp = "$script_dir/data/disp_x.dat";
disp_orbit = load(disp_file_name);

disp_orbit_x = disp_orbit(:,1);
R_x = [R_x, disp_orbit_x];
norm_R_x = norm(R_x, 'fro');

R_y = load(R_y_file_name);
R_y = R_y(:,vcorr_index);
norm2_R_y = norm(R_y, 'fro');
disp_orbit_y = disp_orbit(:,2);
R_y = [R_y, disp_orbit_y];
norm_R_y = norm(R_y, 'fro');

[m,n_x] = size(R_x); 
R_x_ident = zeros(m,n_x);
[m,n_y] = size(R_y); 
R_y_ident = zeros(m,n_y);

P_x = eye(n_x)*init_P_value;
P_y = eye(n_y)*init_P_value;

% Create delete_matrices

delete_x = (R_x != 0);
delete_y = (R_y != 0);

if((rls_algo == 1) || (rls_algo == 2) || (rls_algo == 4) || (rls_algo == 6))

    if(rls_algo == 6)

        %R_ident_x = zeros(m,n_x,2);
        %Ident_data_y = zeros(m,n_y,2);
        D_x_ident = zeros(m,n_x);
        D_y_ident = zeros(m,n_y);

        P_inv_col_x = zeros(n_x,1);
        P_inv_col_x(end) = 1./init_P_value;
        P_inv_diag_x = ones(n_x,1)./init_P_value;
    
        P_inv_col_y = zeros(n_y,1);
        P_inv_col_y(end) = 1./init_P_value;
        P_inv_diag_y = ones(n_y,1)./init_P_value;

    end
elseif(rls_algo == 3)

    p_x = diag(P_x);
    p_y = diag(P_y);
           
    ident_data_x = struct('r', [], 'P', [], 'lambda', 1);
    for i=1:m
        %if(bpm_hcorr(i) < 0)
        %    nr_elem = n_x;
        %else
        %    nr_elem = bpm_hcorr(i)+1;
        %end
        nr_elem = sum(delete_x(i,:));
        ident_data_x(i).r = zeros(nr_elem, 1); 
        ident_data_x(i).P = diag([p_x(1:nr_elem-1)', p_x(end)])*eye(nr_elem); 
        ident_data_x(i).lambda = lambda_x;
    end

    ident_data_y = struct('r', [], 'P', [], 'lambda', 1);
    for i=1:m
        %if(bpm_vcorr(i) < 0)
        %    nr_elem = n_y;
        %else
        %    nr_elem = bpm_vcorr(i)+1;
        %end
        nr_elem = sum(delete_y(i,:));
        ident_data_y(i).r = zeros(nr_elem, 1); 
        ident_data_y(i).P = diag([p_y(1:nr_elem-1)', p_y(end)])*eye(nr_elem);
        ident_data_y(i).lambda = lambda_y;
    end
elseif(rls_algo == 5)

    ident_data_x = struct('r', [], 'P', []);
    for i=1:m
        %if(bpm_hcorr(i) < 0)
        %nr_elem = n_x;
        %else
        %nr_elem = bpm_hcorr(i)+1;
        %end
        nr_elem = sum(delete_x(i,:));
        ident_data_x(i).r = zeros(nr_elem, 1); 
        ident_data_x(i).P = init_P_value;
    end

    ident_data_y = struct('r', [], 'P', []);
    for i=1:m
        %if(bpm_vcorr(i) < 0)
        %nr_elem = n_y;
        %else
        %nr_elem = bpm_vcorr(i)+1;
        %end
        nr_elem = sum(delete_y(i,:));
        ident_data_y(i).r = zeros(nr_elem, 1); 
        ident_data_y(i).P = init_P_value;
    end
elseif((rls_algo == 7) || (rls_algo == 8))
        
    % Incremental covariance matrix
        
    ICM_x = cell(m,1);
    ICM_y = cell(m,1);
    for i = 1:m
        ICM_x{i} = init_ICM();
        ICM_y{i} = init_ICM();
    end

else
    fprintf(1, 'Unknown RLS algo type\n');
end

fprintf(1, "finished\n");



