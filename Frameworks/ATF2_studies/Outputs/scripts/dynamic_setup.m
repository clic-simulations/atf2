% Script to adjust settings, properties or imperfections specific
% for a certain time step

%%%%%%%%%%%%%%%%%%%%%%%
% Apply ground motion %
%%%%%%%%%%%%%%%%%%%%%%%

if (use_ground_motion==1)
  Tcl_Eval("ground_motion $beamline_name $groundmotion(type)");
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Apply Quadrupole strength jitter %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (use_quad_strength_jitter==1)
    disp('Apply Quad Strength jitter')
    placet_element_set_attribute(beamline_name,qp_index,"strength",qp_strength.*(1+quad_strength_sigma*normrnd(0,1,length(qp_index),1)));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Apply Quadrupole position jitter %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (use_quad_position_jitter==1)
    disp('Apply Quad position jitter (y only)')
    placet_element_set_attribute(beamline_name,qp_index,"y",quad_position_sigma*normrnd(0,1,1,length(qp_index)));
end

%%%%%%%%%
% Scans %
%%%%%%%%%

% ref cavity mover
if (wakeFieldSetup && wakeFieldSetup_scan==1)
    move = 1000; % um
    wakeFieldSetupPosition = 1000*wakeFieldSetup_pos+move*(time_step_index-1);
    placet_element_set_attribute(beamline_name,wakeFieldSetup_index,"y",wakeFieldSetupPosition);
    % bellow
    if (wakeFieldSetupTiltedBellow && !use_wakefield_shortbunch)
        % assume 0 position has no tilt
        tilt = round(wakeFieldSetupPosition/1000);
        wakename = strcat("bellow_tilted",int2str(tilt),"mm");
        placet_element_set_attribute(beamline_name,wakeFieldSetup_bellow_index,"short_range_wake",wakename);
        % set to 1mm as wakes are then normalised
        placet_element_set_attribute(beamline_name,wakeFieldSetup_bellow_index,"y",1000);

        clear wakename tilt;
    else
        if(!twoRefCavityFullSetup)
            % move bellow halfway
            placet_element_set_attribute(beamline_name,wakeFieldSetup_bellow_index,"y",0.5*wakeFieldSetupPosition);
        end
    end
    if (wakeFieldSetupQuadMove)
        % according to measurement qd10aff might move up to
        % 1/1000. of setup
        placet_element_vary_attribute(beamline_name,qd10aff_index,"y",move/1000.);
        placet_element_vary_attribute(beamline_name,qd10bff_index,"y",-move/1000.);
    end
end

if (wakeFieldSetup && wakeFieldDipole_scan==1)
    if (wakeFieldDipole)
        placet_element_vary_attribute(beamline_name,wakefielddipole_index,"strength_y",0.2);
    end
end

% sd4ff cavity mover
if (sd4ff_mover_scan_x==1)
    placet_element_vary_attribute(beamline_name,msd4ff_index,"x",300);
    %placet_element_vary_attribute(beamline_name,bellow_sd4ff_index,"x",0.5*300);
end

% sd4ff cavity mover
if (sd4ff_mover_scan_y==1)
    placet_element_vary_attribute(beamline_name,msd4ff_index,"y",300);
    %placet_element_vary_attribute(beamline_name,bellow_sd4ff_index,"y",0.5*300);
end

if (use_qd0ff_current_scan==1)
    placet_element_set_attribute(beamline_name,qd0ff_index,"strength",(qd0ff_current+qd0ff_current_scan_step*(time_step_index-1))*qd0ff_current_conversion_factor);
end

if (use_qf1ff_current_scan==1)
    placet_element_set_attribute(beamline_name,qf1ff_index,"strength",(qf1ff_current+qf1ff_current_scan_step*(time_step_index-1))*qf1ff_current_conversion_factor);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%
% Apply IP kicker jitter %
%%%%%%%%%%%%%%%%%%%%%%%%%%

if (use_font==1 && use_font_jitter==1)

    disp('Apply font kicker jitter')
    placet_element_set_attribute(beamline_name,fontk1_index,"strength_x",fontk1_strengthx*(1+fontk1_strengthx_sigma*normrnd(0,1,1,length(fontk1_index))));
    placet_element_set_attribute(beamline_name,fontk1_index,"strength_y",fontk1_strengthy*(1+fontk1_strengthy_sigma*normrnd(0,1,1,length(fontk1_index))));
    placet_element_set_attribute(beamline_name,fontk2_index,"strength_x",fontk2_strengthx*(1+fontk2_strengthx_sigma*normrnd(0,1,1,length(fontk2_index))));
    placet_element_set_attribute(beamline_name,fontk2_index,"strength_y",fontk2_strengthy*(1+fontk2_strengthy_sigma*normrnd(0,1,1,length(fontk2_index))));
    placet_element_set_attribute(beamline_name,ipkicker_index,"strength_x",ipkicker_strengthx*(1+ipkicker_strengthx_sigma*normrnd(0,1,1,length(ipkicker_index))));
    placet_element_set_attribute(beamline_name,ipkicker_index,"strength_y",ipkicker_strengthy*(1+ipkicker_strengthy_sigma*normrnd(0,1,1,length(ipkicker_index))));
end

%%%%%%%%%%%%%%%%%%%%%%%%%
% Apply incoming jitter %
%%%%%%%%%%%%%%%%%%%%%%%%%

if (use_beam_jitter)
  a = normrnd(0,1);
  b = normrnd(0,1);
  c = normrnd(0,1);
  d = normrnd(0,1);
  %placet_element_set_attribute(beamline_name,start_index,"x",beam_jitter_x*normrnd(0,1));
  %placet_element_set_attribute(beamline_name,start_index,"y",beam_jitter_y*normrnd(0,1));
  %placet_element_set_attribute(beamline_name,start_index,"xp",beam_jitter_xp*normrnd(0,1));
  %placet_element_set_attribute(beamline_name,start_index,"yp",beam_jitter_yp*normrnd(0,1));
  placet_element_set_attribute(beamline_name,start_index,"x",beam_jitter_x*a);
  placet_element_set_attribute(beamline_name,start_index,"y",beam_jitter_y*b);
  placet_element_set_attribute(beamline_name,start_index,"xp",beam_jitter_xp*c);
  placet_element_set_attribute(beamline_name,start_index,"yp",beam_jitter_yp*d);
end

if (use_steering)
    source (strcat(script_dir,"/scripts/steering.m"))
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set BPM resolution dependent on charge %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (use_bpm_charge_dependence)
    cband_res = cband*sqrt(cband_charge_dependence*cband_charge_dependence/((charge/1e10)*(charge/1e10))+1.0);
    placet_element_set_attribute(beamline_name,bpm_cband_att_index,"resolution",cband_res);
    cband_noatt_res = cband_noatt*sqrt(cband_noatt_charge_dependence*cband_noatt_charge_dependence/((charge/1e10)*(charge/1e10))+1.0);
    placet_element_set_attribute(beamline_name,bpm_cband_noatt_index,"resolution",cband_noatt_res);
    sband_res = sband*sqrt(sband_charge_dependence*sband_charge_dependence/((charge/1e10)*(charge/1e10))+1.0);
    placet_element_set_attribute(beamline_name,bpm_sband_index,"resolution",sband_res);
    stripline_res = stripline*sqrt(stripline_charge_dependence*stripline_charge_dependence/((charge/1e10)*(charge/1e10))+1.0);
    placet_element_set_attribute(beamline_name,bpm_stripline_index,"resolution",stripline_res);
    ipbpm_res = ipbpm*sqrt(ipbpm_charge_dependence*ipbpm_charge_dependence/((charge/1e10)*(charge/1e10))+1.0);
    placet_element_set_attribute(beamline_name,bpm_ipbpm_index,"resolution",ipbpm_res);
end
