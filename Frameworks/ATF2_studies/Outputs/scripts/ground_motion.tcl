proc ground_motion_init {beamlinename type model sign} {
    global script_dir delta_T ground_motion_x ground_motion_y groundmotion
    
    if {$type==2} {
	puts -nonewline "Init ground motion generator in "
	puts $beamlinename
	BeamlineUse -name $beamlinename
	
	GroundMotionInit -file $script_dir/data/GM/harm.$model.300 -x $ground_motion_x -y $ground_motion_y -t_step $delta_T -s_sign $sign -s_abs 0 -last_start 1 -s_start 0 -t_start $groundmotion(t_start)
	# stabilisation for the machine
	if {$groundmotion(filtertype)} {
	    # add all filters at once
	    AddGMFilter -file $script_dir/data/GM/harm.$model.300 -filter_x $script_dir/data/TF/$groundmotion(filtertype_x) -filter_y $script_dir/data/TF/$groundmotion(filtertype_y)
	}
    } elseif {$type==1} {
	# ATL motion
	puts -nonewline "Init ATL motion in "
	puts $beamlinename
	ground_motion_long $beamlinename $groundmotion(t_start)
    } elseif {$type==0} {
	puts -nonewline "No ground motion applied "
	puts $beamlinename
    } else {
	puts -nonewline "Unknown ground motion type "
	puts $beamlinename
	exit
    }
}

proc ground_motion {beamlinename type } {
    global delta_T
    if {$type==1} {
	ground_motion_long $beamlinename $delta_T
    } elseif {$type==2} {
	ground_motion_short $beamlinename
    }
}

proc ground_motion_short {beamlinename} {
    global groundmotion
    BeamlineUse -name $beamlinename
    GroundMotion -ip0 $groundmotion(ip0)
}

proc ground_motion_long {beamlinename {time_step -1} } {
    global ground_motion_x ground_motion_y groundmotion

    # ATL motion, time is given by tcl variable atl_time
    if {$time_step == -1} {
	set time_step $atl_time
    }

    puts "ATL motion applied $time_step"
    
    # move girders
    BeamlineUse -name $beamlinename
    GroundMotionATL -t $time_step -x $ground_motion_x -y $ground_motion_y -end_fixed $groundmotion(ip0)
}
