% Response matrix calculation for sextupole knobs
%
% Adapted to ATF2 from Andrea Latina

puts("Response Knob Matrix calculation")

offset = step_size_R_knob % um
N = 5; % number of sextupoles
NAME = [ "SF6FF"; "SF5FF"; "SD4FF"; "SF1FF"; "SD0FF" ];
KBI = zeros(N, 2); % sextupoles split in 2
for i = 1:N
    KBI(i,:) = placet_get_name_number_list(beamline_name, NAME(i,:))
end
Knobs.I = [ KBI; KBI ];
Knobs.L = [ repmat("x", length(KBI), 1); repmat("y", length(KBI), 1) ];
Knobs.K = zeros(15,length(Knobs.I));
[E,B] = placet_test_no_correction(beamline_name, beam_name, "None");
Cov0 = cov(B(:,[1 2 3 5 6 ]));
S0 = std(B(:,[ 1 2 3 5 6 ]));
% proper format
CovLin0 = zeros(15,1)
index = 1
for i = 1:5
    for j = i:5
        % normalise by beam size
        CovLin0(index++,1) = Cov0(i,j) / (S0(i)*S0(j));
    end
end
Knobs.CovLin0 = CovLin0;
Knobs.S0 = S0;
% 
sizeX = S0(2)
sizeY = S0(3)
for k = 1:length(Knobs.I)
    placet_element_set_attribute(beamline_name, Knobs.I(k,:), Knobs.L(k), +offset);
    [E,B] = placet_test_no_correction(beamline_name, beam_name, "None");
    Cov = cov(B(:,[ 1 2 3 5 6 ]));
    placet_element_set_attribute(beamline_name, Knobs.I(k,:), Knobs.L(k), -offset);
    [E,B] = placet_test_no_correction(beamline_name, beam_name, "None");
    Cov -= cov(B(:,[ 1 2 3 5 6 ]));
    Cov /= 2 * offset
    index = 1;
    for i = 1:5
        for j = i:5
            Knobs.K(index++,k) = Cov(i,j)/(S0(i)*S0(j));
        end
    end
    placet_element_set_attribute(beamline_name, Knobs.I(k), Knobs.L(k), 0.0);
end
[U,S,V] = svd(Knobs.K);
Knobs.S = diag(S);
Knobs.V = V;

Knobs.Ix = KBI;
[Ux,Sx,Vx] = svd(Knobs.K(:,1:5));
Knobs.Ux = Ux
Knobs.Sx = diag(Sx);
Knobs.Vx = Vx;

Knobs.Iy = KBI;
[Uy,Sy,Vy] = svd(Knobs.K(:,6:10));
Knobs.Uy = Uy
Knobs.Sy = diag(Sy);
Knobs.Vy = Vy;

N = rows(Knobs.K);
if (S(end) / S(1) / N < eps) % check working?
    warning("some singular values might be too small!")
end

save -text Knobs.dat Knobs;
