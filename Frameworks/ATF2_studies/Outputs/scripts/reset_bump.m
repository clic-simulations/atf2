
% Reset excitation

corr_setting_old_x = placet_element_get_attribute("beamline_name", corr_used_x_index, "strength_x");
corr_setting_new_x = corr_setting_old_x - excit_x';
placet_element_set_attribute("beamline_name", corr_used_x_index, "strength_x", corr_setting_new_x);

corr_setting_old_y = placet_element_get_attribute("beamline_name", corr_used_y_index, "strength_y");
corr_setting_new_y = corr_setting_old_y - excit_y';
placet_element_set_attribute("beamline_name", corr_used_y_index, "strength_y", corr_setting_new_y);