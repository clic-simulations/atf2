% Implementation of the setup of the system identification which
% means basicly the bump setup

if(identification_switch == 1)
  
    % Calculate excitation
    
    if(excit_type == 1)
	
	% Create limited random excitation

	excit_x = randn(length(corr_used_x_index), 1);
	excit_y = randn(length(corr_used_y_index), 1);

	%for i=1:length(excit_x)
	%    if(excit_x(i) > 3)
	%        excit_x(i) = 3;
	%    elseif(excit_x(i) < -3)
	%        excit_x(i) = -3;
	%    end
	%end

	%for i=1:length(excit_y)
	%    if(excit_y(i) > 3)
	%        excit_y(i) = 3;
	%    elseif(excit_y(i) < -3)
	%        excit_y(i) = -3;
	%    end
	%end

        %excit_x = excit_x.*alpha_x./sqrt(beta_x_corr);
        %excit_y = excit_y.*alpha_y./sqrt(beta_y_corr);

	%% To create an emittance growth of delta_emitt_wish alltogether, it has to be determined
	%% how much each corrector has to be excited. Since each corrector is calibrated to create the 
	%% same emittance growth and since the emittance growth of different sources adds linearly, we
	%% get the wanted result if each corrector creates an emittance growth of delta_emitt_wish/N,
	%% where N is the number of quadrupoles. 
	%delta_emitt_temp = abs(delta_emitt_x_wish./(n_x-1).*excit_x);
	delta_emitt_temp = delta_emitt_x_wish./(n_x-1);
	excit_x = sign(excit_x).*sqrt(delta_emitt_temp./emitt_fact_x);
	%delta_emitt_temp = abs(delta_emitt_y_wish./(n_y-1).*excit_y);
	delta_emitt_temp = delta_emitt_y_wish./(n_y-1);
	excit_y = sign(excit_y).*sqrt(delta_emitt_temp./emitt_fact_y);
    elseif(excit_type == 2)
        corr_excit_nr_x = mod(time_step_index, n_x); 
        cycle_nr_x = round((time_step_index-corr_excit_nr_x)/(n_x));
	corr_excit_nr_y = mod(time_step_index, n_y); 	
	cycle_nr_y = round((time_step_index-corr_excit_nr_y)/(n_y));
        
	excit_x = zeros(length(corr_used_x_index), 1);
        if(corr_excit_nr_x < 0.5)
	    % zero excitation
	    excit_x_local = 0;
	else
	    % local excitation
	    sign_excit_x = (-1)^mod(cycle_nr_x, 2);
	    excit_x_local = sign_excit_x*sqrt(delta_emitt_x_wish/emitt_fact_x(corr_excit_nr_x));
	    %excit_x_local = 10;
	    excit_x(corr_excit_nr_x) = excit_x_local;
	end	

	excit_y = zeros(length(corr_used_y_index), 1);
        if(corr_excit_nr_y < 0.5)
	    % zero excitation
	    excit_y_local = 0;
	else
	    % local excitation
	    sign_excit_y = (-1)^mod(cycle_nr_y, 2);
	    excit_y_local = sign_excit_y*sqrt(delta_emitt_y_wish/emitt_fact_y(corr_excit_nr_y));
	    %excit_y_local = 10;
	    excit_y(corr_excit_nr_y) = excit_y_local;
	end
    elseif(excit_type == 3)
	corr_excit_nr_x = mod(time_step_index, n_x); 
        cycle_nr_x = round((time_step_index-corr_excit_nr_x)/(n_x));
	corr_excit_nr_y = mod(time_step_index, n_y); 	
	cycle_nr_y = round((time_step_index-corr_excit_nr_y)/(n_y));
        
	excit_x = zeros(length(corr_used_x_index), 1);
        if(corr_excit_nr_x < 0.5)
	    % zero excitation
	    excit_x_local = 0;
	else
	    % local excitation
	    sign_excit_x = (-1)^mod(cycle_nr_x, 2);
	    excit_x_local = sign_excit_x*hexcit_amp(corr_excit_nr_x)*alpha_x;
	    excit_x(corr_excit_nr_x) = excit_x_local;
	end	

	excit_y = zeros(length(corr_used_y_index), 1);
        if(corr_excit_nr_y < 0.5)
	    % zero excitation
	    excit_y_local = 0;
	else
	    % local excitation
	    sign_excit_y = (-1)^mod(cycle_nr_y, 2);
	    excit_y_local = sign_excit_y*vexcit_amp(corr_excit_nr_y)*alpha_y;
	    excit_y(corr_excit_nr_y) = excit_y_local;
	end
    elseif(excit_type == 4)
	% Excitation scheme in which each corrector is performing an excitation in the pos. and 
	% the neg. direction before the next corrector is excited. Therefore on cycle is twice as long. 
	% This is implemented at the moment only to get the necessary excitation for the calculation  
	% of the emittance scaling factors for the excitation itself.
	% The identification does not work at the moment with this option!!!
        corr_excit_nr_x = mod(time_step_index, 2*n_x);
        cycle_nr_x = round((time_step_index-corr_excit_nr_x)/(2*n_x));
	corr_excit_nr_y = mod(time_step_index, 2*n_y); 	
	cycle_nr_y = round((time_step_index-corr_excit_nr_y)/(2*n_y));
        
	excit_x = zeros(length(corr_used_x_index), 1);
        if((corr_excit_nr_x < 0.5) || (corr_excit_nr_x > (2*n_x - 1.5)))
	    % zero excitation
	    fprintf(1,"no excitation\n");
	    excit_x_local = 0;
	    corr_excit_nr_x = 1;
	elseif(mod(corr_excit_nr_x,2)==1)
   	    % positive local excitation
	    fprintf(1,"pos excitation\n");
	    corr_excit_nr_x = round((corr_excit_nr_x+1)/2)
	    excit_x_local = sqrt(delta_emitt_x_wish/emitt_fact_x(corr_excit_nr_x));
	    excit_x(corr_excit_nr_x) = excit_x_local;
	else
	    % negative local excitation
	    fprintf(1,"neg excitation\n");
	    corr_excit_nr_x = round(corr_excit_nr_x/2)
	    excit_x_local = -sqrt(delta_emitt_x_wish/emitt_fact_x(corr_excit_nr_x));
	    excit_x(corr_excit_nr_x) = excit_x_local;
	end	

	excit_y = zeros(length(corr_used_y_index), 1);
        if((corr_excit_nr_y < 0.5) || (corr_excit_nr_y > (2*n_y - 1.5)))
	    % zero excitation
	    excit_y_local = 0;
	    corr_excit_nr_y = 1;
	elseif(mod(corr_excit_nr_y,2)==1)
   	    % positive local excitation
	    corr_excit_nr_y = round((corr_excit_nr_y+1)/2);
	    excit_y_local = sqrt(delta_emitt_y_wish/emitt_fact_y(corr_excit_nr_y));
	    excit_y(corr_excit_nr_y) = excit_y_local;
	else
	    % negative local excitation
	    corr_excit_nr_y = round(corr_excit_nr_y/2);
	    excit_y_local = -sqrt(delta_emitt_y_wish/emitt_fact_y(corr_excit_nr_y));
	    excit_y(corr_excit_nr_y) = excit_y_local;
	end	

    end

    % Apply excitation
	
    %corr_setting_old_x = placet_element_get_attribute("beamline_name", corr_used_x_index, "strength_x");
    %corr_setting_new_x = corr_setting_old_x + excit_x';
    placet_element_set_attribute(beamline_name, corr_used_x_index, "strength_x", excit_x');

    %corr_setting_old_y = placet_element_get_attribute("beamline_name", corr_used_y_index, "strength_y");
    %corr_setting_new_y = corr_setting_old_y + excit_y';
    placet_element_set_attribute(beamline_name, corr_used_y_index, "strength_y", excit_y');

end
