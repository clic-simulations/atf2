%%%%%%%%%%%%%%%%%%
% 1-1 correction %
%%%%%%%%%%%%%%%%%%

% initialise
if (time_step_index == 1)
    iter_one_to_one = 1;
end

% start at step 2
if (use_one_to_one_steering)

if (time_step_index >= 2 && iter_one_to_one <= one_to_one_steering_iter)
    printf('121 Steering - iteration %d\n',iter_one_to_one);

    disp('One to one steering');
    iter_one_to_one++;

    diff_readings = bpm_readings - orbit_perf;

    Cx = -[ Response.Rx ; one_to_one_steering_weight * eye(nr_hcorr) ] \ [ diff_readings(:,1) ; zeros(nr_hcorr,1) ];
    Cy = -[ Response.Ry ; one_to_one_steering_weight * eye(nr_vcorr) ] \ [ diff_readings(:,2) ; zeros(nr_vcorr,1) ];
    placet_element_vary_attribute(beamline_name, hcorr_index, "strength_x", Cx);
    placet_element_vary_attribute(beamline_name, vcorr_index, "strength_y", Cy);

    %BPMreadingsafter121 = placet_get_bpm_readings(beamline_name, DFSWFS_Bpms);
end

end % iter

%%%%%%%%%%%%%%%
% DFS and WFS %
%%%%%%%%%%%%%%%

%%%%%%%
% DFS %
%%%%%%%

if (use_dfs || use_wfs)

if (use_one_to_one_steering)
    dfs_wfs_start = 2 + one_to_one_steering_iter;
else
    dfs_wfs_start = 2; 
end

if (time_step_index == 1) % better to initialise earlier
    iter_dfs_wfs = 1;
    dfs_onoff=0; %DFS steering state: 0 neutral, 1 reduced energy,
                 %2 reset energy and store bpm reading, 3 done, 4 apply correction
    wfs_onoff=0; %DFS steering state: 0 neutral, 1 reduced charge,
                 %2 reset charge and store bpm reading, 3 done, 4 apply correction
end

% DFS, alternating nominal and reduced energy
if (time_step_index >= dfs_wfs_start && iter_dfs_wfs <= dfs_wfs_iter)

    printf('DFS/WFS Steering - iteration %d\n',iter_dfs_wfs);
    % save BPM readings nominal beam
    if (dfs_onoff == 0 && wfs_onoff == 0)
        B = placet_get_bpm_readings(beamline_name, DFSWFS_Bpms);
    end
    
    if (use_dfs && dfs_onoff == 0)
        dfs_onoff = 1; % iteration with reduced energy
    elseif (use_wfs && wfs_onoff == 0)
        wfs_onoff = 1; % iteration with reduced charge
    end
    
    if (dfs_onoff==1) 
        dfs_onoff = 2;
        disp('reduced energy beam')
        % beam with reduced energy
        % reduce energy
        change_energy(dfs_dE);
        if (!dynamic_beam)
            beam_name_orig = beam_name;
            beam_name = beam_name_dfs;
        end
    elseif (dfs_onoff==2)
        dfs_onoff = 3;
        % save BPM readings
        B_DFS = placet_get_bpm_readings(beamline_name, DFSWFS_Bpms);
        % reset energy
        change_energy(1./dfs_dE);
        if (!dynamic_beam)
            %nominal beam
            beam_name = beam_name_orig;
        end
    end
    
    if(wfs_onoff==1)
        wfs_onoff = 2;
        disp('reduced charge beam')
        % beam with reduced charge
        % reduce charge
        change_charge(wfs_dcharge);
        if (!dynamic_beam)
            beam_name_orig = beam_name;
            beam_name = beam_name_wfs;
        end
    elseif (wfs_onoff==2)
        wfs_onoff = 3;
        % save BPM readings
        B_WFS = placet_get_bpm_readings(beamline_name, DFSWFS_Bpms);
        % reset charge
        change_charge(1./wfs_dcharge);
        if (!dynamic_beam)
            %nominal beam
            beam_name = beam_name_orig;
        end
    end

    if (dfs_onoff == 3 && use_wfs==0)
        disp('DFS steering')
        dfs_onoff = 4;
        % get readings from reduced energy beam
        % differential readings compared to nominal reduced energy beam
        BDFS = B_DFS - DFSWFS_B - DFSWFS_BDFS;
        % differential readings from 'nominal orbit'
        B -= DFSWFS_B;
        
        %B -= BPMreadingsafter121;
        
        % cut for nbins:
        %B = B(Bin.Bpms,:);
        %BDFS = BDFS(Bin.Bpms,:);
        Cx = -[ Rx ; dfs_weight_x * RxDFS ; steering_beta * eye(nCx) ] \ [ B(:,1) ; dfs_weight_x * BDFS(:,1) ; zeros(nCx,1) ];
        Cy = -[ Ry ; dfs_weight_y * RyDFS ; steering_beta * eye(nCy) ] \ [ B(:,2) ; dfs_weight_y * BDFS(:,2) ; zeros(nCy,1) ];

        %figure(1)
        %plot(bpm_readings(:,1))

        %figure(2)
        %plot(bpm_readings(:,2))
        
    elseif (wfs_onoff == 3 && use_dfs == 0)
        disp('WFS steering')
        wfs_onoff = 4;
        % get readings from reduced charge beam
        % differential readings compared to nominal reduced charge beam
        BWFS = B_WFS - DFSWFS_B - DFSWFS_BWFS;
        % differential readings from 'nominal orbit'
        B -= DFSWFS_B;
        
        Cx = -[ Rx ; wfs_weight_x * RxWFS ; steering_beta * eye(nCx) ] \ [ B(:,1) ; wfs_weight_x * BWFS(:,1) ; zeros(nCx,1) ];
        Cy = -[ Ry ; wfs_weight_y * RyWFS ; steering_beta * eye(nCy) ] \ [ B(:,2) ; wfs_weight_y * BWFS(:,2) ; zeros(nCy,1) ];
    elseif (dfs_onoff == 3 && wfs_onoff == 3)
        disp('DFS and WFS steering')
        dfs_onoff = 4;
        wfs_onoff = 4;
        % differential readings compared to nominal reduced charge beam
        BDFS = B_DFS - DFSWFS_B - DFSWFS_BDFS;
        BWFS = B_WFS - DFSWFS_B - DFSWFS_BWFS;
        % differential readings from 'nominal orbit'
        B -= DFSWFS_B;
        
        Cx = -[ Rx ; dfs_weight_x * RxDFS ; wfs_weight_x * RxWFS ; steering_beta * eye(nCx) ] \ [ B(:,1) ; dfs_weight_x * BDFS(:,1) ; wfs_weight_x * BWFS(:,1) ; zeros(nCx,1) ];
        Cy = -[ Ry ; dfs_weight_y * RyDFS ; wfs_weight_y * RyWFS ; steering_beta * eye(nCy) ] \ [ B(:,2) ; dfs_weight_y * BDFS(:,2) ; wfs_weight_y * BWFS(:,2) ; zeros(nCy,1) ];
    end
end

% apply correction
if (dfs_onoff==4 || wfs_onoff==4)
    disp('Apply correction')
    iter_dfs_wfs+=1;
    dfs_onoff = 0;
    wfs_onoff = 0;

    placet_element_vary_attribute(beamline_name, Response.Cx(Bin.Cx), "strength_x", Cx);
    placet_element_vary_attribute(beamline_name, Response.Cy(Bin.Cy), "strength_y", Cy);

    %strength_x = placet_element_get_attribute(beamline_name, Response.Cx(Bin.Cx), "strength_x")
    %strength_y = placet_element_get_attribute(beamline_name, Response.Cy(Bin.Cy), "strength_y")

end % iteration

end % dfs/wfs

% correct higher order imperfections with sextupole knobs
if (use_knobs && use_perfect_knobs)
    if (time_step_index == nr_time_steps - 1 )
        % get BEAM AT IP!!
        Cov = cov(Beam(:,[1 2 3 5 6 ]))
        S = std(B(:,[ 1 2 3 5 6 ]));
        sizeX = S(2)
        sizeY = S(3)
        % proper format
        CovLin = zeros(15,1)
        index = 1
        for i = 1:5
            for j = i:5
                % normalise by beam size
                CovLin(index++,1) = Cov(i,j) / (S(i)*S(j));
            end
        end
        % calculate knobs
        [U,S,V] = svd(Knobs.K);

        % include Cov target!
        knob_values = inv(V) * inv(S) * inv(U) * Cov

        % apply knobs
        for k = 1:length(Knobs.I)
            placet_element_vary_attribute(beamline_name,Knobs.I(k,:),Knobs.L(k),knob_values);
        end 
    end
    if (time_step_index == nr_time_steps - 1 )
        % check
        Cov = cov(Beam(:,[1 2 3 5 6 ]))
    end
end
