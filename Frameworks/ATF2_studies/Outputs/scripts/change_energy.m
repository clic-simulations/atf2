function change_energy(dE)
global e_initial

reduce_E = e_initial*dE;
% charge can be a const in Tcl, so unset and reset
Tcl_Eval("unset e_initial");
Tcl_SetVar("e_initial",reduce_E);
e_initial = reduce_E;

end