# This file loads the settings for the job and initialises the run environment

# Include general default settings:
source $script_dir/settings.tcl

# Add computer specific paths (if existing)
if [file exists $script_dir/my_computer.tcl] {
  source $script_dir/my_computer.tcl
}

# Include private run-specific settings file (if given as command line option, else only default is used)
if {$argc >= 1} {
    set settings_name [lindex $argv 0]
    puts "load run settings: $settings_name"

    # check if private variables are existing and issue a warning if not
    check_file ${script_dir}/${settings_name}
    source ${script_dir}/${settings_name}
}

# test for incompatible settings and override them or abort
source $script_dir/scripts/test_settings.tcl

# export tcl variables to Octave and vice versa, create directories: 
source $script_dir/scripts/prepare_environment.tcl
