% Script to evaluate and store the results of the last time steps

%%%%%%%%%%%%%%%%%%%%%%
% General parameters %
%%%%%%%%%%%%%%%%%%%%%%

orbit_x = bpm_readings(:,1) - orbit_perf(:,1);
orbit_x_std = sqrt(var(orbit_x));
orbit_x_max = max(orbit_x);
orbit_y = bpm_readings(:,2) - orbit_perf(:,2);
orbit_y_std = sqrt(var(orbit_y));
orbit_y_max = max(orbit_y);


%%%%%%%%%%%%%%%%%%%%%%%%%
% Emittance calculation %
%%%%%%%%%%%%%%%%%%%%%%%%%

% Determine files to be processed

file_stop_index = time_step_index;
file_start_index = file_stop_index - multipulse_nr + 1;
if(file_start_index < 1)
  file_start_index = 1;
end
Tcl_SetVar('file_stop_index', file_stop_index);
Tcl_SetVar('file_start_index', file_start_index);

% Load the single pulse results

Tcl_Eval('read_array electron_beampar_ff_end${time_step_index}_$bunch_index');
source(strcat("electron_beampar_ff_end",num2str(time_step_index),"_",num2str(bunch_index),".m"));

offset_x = x_tcl - x0;
offset_y = y_tcl - y0;

sps_x = sqrt(sxx_axis_tcl) - sps_x_perf;
sps_y = sqrt(syy_axis_tcl) - sps_y_perf;

spe_x = emitt_x_tcl - spe_x_perf;
spe_y = emitt_y_tcl - spe_y_perf;

%% Calculate the approximated multi-pulse emittance
%
%beam_file_name = sprintf("%s_beampar", beam_name);
%Tcl_SetVar("beam_file_name", beam_file_name);
%Tcl_Eval('array set mpe_data [calc_emitt_approx $beam_file_name $file_start_index $file_stop_index $eval_beam_shift]')
%Tcl_Eval('export_array_2_octave mpe_data');
%spea_x = emitt_s_x_tcl/100 - spe_x_perf;
%
%% There could be still a problem with the emitt_..._tcl scaling
%j0e_x = emitt_j0_x_tcl;
%jce_x = emitt_c_x_tcl;
%je_x = sqrt( j0e_x*j0e_x + jce_x*jce_x );
%mpea_x = emitt_j_x_tcl - spe_x_perf;
%mpeas_x = spea_x + je_x^2/spe_x_perf/2;
%
%spea_y = emitt_s_y_tcl/100 - spe_y_perf;
%j0e_y = emitt_j0_y_tcl;
%jce_y = emitt_c_y_tcl;
%je_y = sqrt( j0e_y*j0e_y + jce_y*jce_y );
%mpea_y = emitt_j_y_tcl - spe_y_perf;
%mpeas_y = spea_y + je_y^2/spe_y_perf/2;

% Calculate the real multipulse emittance if the time is right 

if(detailed_eval==1 && mod(time_step_index, multipulse_nr)==0)
     
    %beam_file_name = sprintf("%s_", beam_name);
    %Tcl_SetVar("beam_file_name", beam_file_name);
    Tcl_Eval('array set mpe_data [calc_emitt_particle $beam_name $file_start_index $file_stop_index $eval_beam_shift]')
    %Tcl_Eval('array set mpe_data [calc_emitt_particle2 $beam_name $file_start_index $file_stop_index $x0 $xp0 $y0 $yp0]')
    Tcl_Eval('save_array mpe_data mpe_data');
    %Tcl_Eval('export_array_2_octave mpe_data');
    source("mpe_data.m");
      
    mpe_x = emitt_x_tcl/100 - spe_x_perf;
    mpe_y = emitt_y_tcl/100 - spe_y_perf;
	
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Eval identification result %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(identification_switch == 1)
	
    if((rls_algo == 3) || (rls_algo == 5))
	% Restore the orbit response matrix
	for i=1:m
            nr_elem = length(ident_data_x(i).r);

	    line_vector = [(ident_data_x(i).r(1:end-1))', zeros(1, n_x - nr_elem), ident_data_x(i).r(end)];
	    R_x_ident(i,:) = line_vector;
	end
	for i=1:m
       	    nr_elem = length(ident_data_y(i).r);

	    line_vector = [(ident_data_y(i).r(1:end-1))', zeros(1, n_y - nr_elem), ident_data_y(i).r(end)];
	    R_y_ident(i,:) = line_vector;
	end
    end

    if(rls_algo == 6)

	% The dispersion has to be handled

	disp_x_ident = zeros(m,1);
	disp_y_ident = zeros(m,1);
	if(disp_reconst_algo == 1)
	    disp_x_ident = D_x_ident(:,end);
	    disp_y_ident = D_y_ident(:,end);
	elseif(disp_reconst_algo == 2)
	    for i=1:m
	        if(bpm_hcorr(i)<0)
	            disp_x_ident(i) = mean(D_x_ident(i,1:end-1));	
	        else
	            disp_x_ident(i) = mean(D_x_ident(i,1:bpm_hcorr(i)));
	        end
	        if(bpm_vcorr(i)<0)
	            disp_y_ident(i) = mean(D_y_ident(i,1:end-1));	
	        else
	            disp_y_ident(i) = mean(D_y_ident(i,1:bpm_vcorr(i)));
	        end
	    end
	else
	    for i=1:m
	        if(bpm_hcorr(i)<0)
	            disp_x_ident(i) = mean(D_x_ident(i,:));	
	        else
	            disp_x_ident(i) = mean([D_x_ident(i,1:bpm_hcorr(i)), D_x_ident(i,end)]);
	        end
	        if(bpm_vcorr(i)<0)
	            disp_y_ident(i) = mean(D_y_ident(i,:));	
	        else
	            disp_y_ident(i) = mean([D_y_ident(i,1:bpm_vcorr(i)), D_y_ident(i,end)]);
	        end
	    end
	end

	R_x_ident(:,end) = disp_x_ident;
	R_y_ident(:,end) = disp_y_ident;

    end

    error1_ident_x = norm(R_x_ident-R_x, 'fro')./norm_R_x;
    error1_ident_y = norm(R_y_ident-R_y, 'fro')./norm_R_y;

    error2_ident_x = norm(R_x_ident(:,1:end-1)-R_x(:,1:end-1), 'fro')./norm2_R_x;
    error2_ident_y = norm(R_y_ident(:,1:end-1)-R_y(:,1:end-1), 'fro')./norm2_R_y;

    if(rls_algo == 3)
	P_x = ident_data_x(end).P;
        P_y = ident_data_y(end).P;
    end
    norm_P_x = norm(P_x, 'fro');
    norm_P_y = norm(P_y, 'fro');

end

if (wakeFieldSetup && size(wakeFieldSetup_index)>0)
    wakeFieldSetup_pos_temp = placet_element_get_attribute(beamline_name,wakeFieldSetup_index,"y");
end

%%%%%%%%%%%%%%%%%%%%%
% Store the results %
%%%%%%%%%%%%%%%%%%%%%

fid1 = fopen(meas_station_file_name, 'at');
fprintf(fid1, '%g\t %g\t %g\t', time, orbit_x_std, orbit_y_std);
	fprintf(fid1, '%g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t', spe_x, spe_y, offset_x, offset_y, orbit_x_max, orbit_y_max, sps_x, sps_y, mpe_x, mpe_y);
if (wakeFieldSetup && size(wakeFieldSetup_index)>0)
    fprintf(fid1, '%g\t', wakeFieldSetup_pos_temp(1));
end
if(identification_switch == 1)
    fprintf(fid1, '%g\t %g\t %g\t %g\t %g\t %g\t', error1_ident_x, error1_ident_y, norm_P_x, norm_P_y, error2_ident_x, error2_ident_y);
end
fprintf(fid1, '\n');
%fprintf(fid1, '%g   %g   %g   %g   %g   %g\n', mpeas_x, mpeas_y, orbit_x_max, orbit_y_max, sps_x, sps_y);
fclose(fid1);

if (save_beam_ff_start == 1)
    Tcl_Eval('read_array electron_beampar_ff_start${time_step_index}_$bunch_index');
    source(strcat("electron_beampar_ff_start",num2str(time_step_index),"_",num2str(bunch_index),".m"));

    fid11 = fopen(meas_station_ff_start_file_name, 'at');
    fprintf(fid11, '%g\t', time);
    fprintf(fid11, '%g\t %g\t %g\t %g\t', x_tcl, xp_tcl, y_tcl, yp_tcl);
    
    fprintf(fid11, '%g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t', sxx_axis_tcl, sxpxp_axis_tcl, syy_axis_tcl,sypyp_axis_tcl, szz_axis_tcl,see_tcl, sxy_axis_tcl, sxe_axis_tcl, sxpe_axis_tcl, sye_axis_tcl, sype_axis_tcl);
    fprintf(fid11, '%g\t %g\t %g\t %g\t %g\t %g\t',
    emitt_x_tcl, emitt_y_tcl, disp_x_tcl,disp_xp_tcl,disp_y_tcl,disp_yp_tcl);
    fprintf(fid11, '%g\t %g\t %g\t %g\t %g\t %g\t', alpha_x_tcl,alpha_y_tcl,beta_x_tcl,beta_y_tcl,e_tcl,e_spread_tcl);
    fprintf(fid11, '\n');
    %fprintf(fid11, '%g   %g   %g   %g   %g   %g\n', mpeas_x, mpeas_y, orbit_x_max, orbit_y_max, sps_x, sps_y);
    fclose(fid11);
end

if (save_beam_mfb2ff == 1)    
    Tcl_Eval('read_array electron_beampar_mfb2ff${time_step_index}_$bunch_index');
    source(strcat("electron_beampar_mfb2ff",num2str(time_step_index),"_",num2str(bunch_index),".m"));

    fid11 = fopen(meas_station_mfb2ff_file_name, 'at');
    fprintf(fid11, '%g\t', time);
    fprintf(fid11, '%g\t %g\t %g\t %g\t', x_tcl, xp_tcl, y_tcl, yp_tcl);
    
    fprintf(fid11, '%g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t', sxx_axis_tcl, sxpxp_axis_tcl, syy_axis_tcl,sypyp_axis_tcl, szz_axis_tcl,see_tcl, sxy_axis_tcl, sxe_axis_tcl, sxpe_axis_tcl, sye_axis_tcl, sype_axis_tcl);
    fprintf(fid11, '%g\t %g\t %g\t %g\t %g\t %g\t',
    emitt_x_tcl, emitt_y_tcl, disp_x_tcl,disp_xp_tcl,disp_y_tcl,disp_yp_tcl);
    fprintf(fid11, '%g\t %g\t %g\t %g\t %g\t %g\t', alpha_x_tcl,alpha_y_tcl,beta_x_tcl,beta_y_tcl,e_tcl,e_spread_tcl);
    fprintf(fid11, '\n');
    %fprintf(fid11, '%g   %g   %g   %g   %g   %g\n', mpeas_x, mpeas_y, orbit_x_max, orbit_y_max, sps_x, sps_y);
    fclose(fid11);
end

%if (save_beam_ip == 1)
%    Tcl_Eval('read_array electron_beampar_ip${time_step_index}_$bunch_index');
%    source(strcat("electron_beampar_ip",num2str(time_step_index),"_",num2str(bunch_index),".m"));

%    fid11(ii) = fopen(meas_station_ip_file_name, 'at');
%    fprintf(fid11(ii), '%g\t', time);
%    fprintf(fid11(ii), '%g\t %g\t %g\t %g\t', x_tcl, xp_tcl, y_tcl, yp_tcl);
    
%    fprintf(fid11(ii), '%g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t', sxx_axis_tcl, sxpxp_axis_tcl, syy_axis_tcl,sypyp_axis_tcl, szz_axis_tcl,see_tcl, sxy_axis_tcl, sxe_axis_tcl, sxpe_axis_tcl, sye_axis_tcl, sype_axis_tcl);
%    fprintf(fid11(ii), '%g\t %g\t %g\t %g\t %g\t %g\t',
%    emitt_x_tcl, emitt_y_tcl, disp_x_tcl,disp_xp_tcl,disp_y_tcl,disp_yp_tcl);
%    fprintf(fid11(ii), '%g\t %g\t %g\t %g\t %g\t %g\t', %alpha_x_tcl,alpha_y_tcl,beta_x_tcl,beta_y_tcl,e_tcl,e_spread_tcl);
%    if (wakeFieldSetup && size(wakeFieldSetup_index)>0)
%        fprintf(fid11(ii), '%g\t', wakeFieldSetup_pos_temp(1));
%    end
%    fprintf(fid11(ii), '\n');
    %fprintf(fid11(ii), '%g   %g   %g   %g   %g   %g\n', mpeas_x, mpeas_y, orbit_x_max, orbit_y_max, sps_x, sps_y);
%    fclose(fid11(ii));
%end

if (save_beam_ip == 1)
    Tcl_Eval('read_array electron_beampar_ip${time_step_index}_$bunch_index');
    source(strcat("electron_beampar_ip",num2str(time_step_index),"_",num2str(bunch_index),".m"));

    fid11 = fopen(meas_station_ip_file_name, 'at');
    fprintf(fid11, '%g\t', time);
    fprintf(fid11, '%g\t %g\t %g\t %g\t', x_tcl, xp_tcl, y_tcl, yp_tcl);
    
    fprintf(fid11, '%g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t', sxx_axis_tcl, sxpxp_axis_tcl, syy_axis_tcl,sypyp_axis_tcl, szz_axis_tcl,see_tcl, sxy_axis_tcl, sxe_axis_tcl, sxpe_axis_tcl, sye_axis_tcl, sype_axis_tcl);
    fprintf(fid11, '%g\t %g\t %g\t %g\t %g\t %g\t',
    emitt_x_tcl, emitt_y_tcl, disp_x_tcl,disp_xp_tcl,disp_y_tcl,disp_yp_tcl);
    fprintf(fid11, '%g\t %g\t %g\t %g\t %g\t %g\t', alpha_x_tcl,alpha_y_tcl,beta_x_tcl,beta_y_tcl,e_tcl,e_spread_tcl);
    if (wakeFieldSetup && size(wakeFieldSetup_index)>0)
        fprintf(fid11, '%g\t', wakeFieldSetup_pos_temp(1));
    end
    fprintf(fid11, '\n');
    %fprintf(fid11, '%g   %g   %g   %g   %g   %g\n', mpeas_x, mpeas_y, orbit_x_max, orbit_y_max, sps_x, sps_y);
    fclose(fid11);
end

if (bunch_index==1)
    temp_name_x = bpm_readings_file_name_x;
    temp_name_y = bpm_readings_file_name_y;
    if (bpm_direction == 1)
        temp_name_xp = bpm_readings_file_name_xp;
        temp_name_yp = bpm_readings_file_name_yp;
    end
else
    temp_name_x = strcat(bpm_readings_file_name_x(1:end-4),int2str(bunch_index),'.dat');
    temp_name_y = strcat(bpm_readings_file_name_y(1:end-4),int2str(bunch_index),'.dat');       
    if (bpm_direction == 1)
        temp_name_xp = strcat(bpm_readings_file_name_xp(1:end-4),int2str(bunch_index),'.dat');
        temp_name_yp = strcat(bpm_readings_file_name_yp(1:end-4),int2str(bunch_index),'.dat');
    end
end

fid2 = fopen(temp_name_x, 'at');
if (save_bpm_readings_nonoise)
    fprintf(fid2,'%g \t', bpm_readings_true(:,1));
else
    fprintf(fid2,'%g \t', bpm_readings(:,1));
end
fprintf(fid2,'\n');
fclose(fid2);
fid3 = fopen(temp_name_y, 'at');
if (save_bpm_readings_nonoise)
    fprintf(fid3,'%g \t', bpm_readings_true(:,2));
else
    fprintf(fid3,'%g \t', bpm_readings(:,2));
end
fprintf(fid3,'\n');
fclose(fid3);

if (bpm_direction == 1)
    fid2 = fopen(temp_name_xp, 'at');
    fprintf(fid2,'%g \t', transpose(bpm_readingsxp));
    fprintf(fid2,'\n');
    fclose(fid2);
    fid3 = fopen(temp_name_yp, 'at');
    fprintf(fid3,'%g \t', transpose(bpm_readingsyp));
    fprintf(fid3,'\n');
    fclose(fid3);
end

fid4 = fopen(magnetPosition_readings_file_name_x, 'at');
fprintf(fid4,'%g \t', magnetPosition_readings(:,1));
fprintf(fid4,'\n');
fclose(fid4);
fid5 = fopen(magnetPosition_readings_file_name_y, 'at');
fprintf(fid5,'%g \t', magnetPosition_readings(:,2));
fprintf(fid5,'\n');
fclose(fid5);

if (use_font || IPFeedback || MFB2FFFeedback)
    fid6 = fopen(ip_feedback_file_name, 'at');
    fprintf(fid6,'%g \t %d \t %d \t %g \t %g \t %g \t %g \t %g \t %g \t %g \t %g \t %g \t %g \t %g \t %g \t %g \t %g \t %g \t %g \t %g \t, %g \t %g \t %g \t', time, time_step_index, ...
            bunch_index, fontk1_strengthx, fontk1_strengthy, ...
            fontk2_strengthx, fontk2_strengthy, ipkicker_strengthx, ...
            ipkicker_strengthy, fontp1_readingx, fontp1_readingy, ...
            fontp2_readingx, fontp2_readingy, fontp3_readingx, ...
            fontp3_readingy, mfb2ff_readingx, mfb2ff_readingy, ...
            preip_readingx, preip_readingy, ipa_readingx, ipa_readingy, ...
            ipb_readingx, ipb_readingy);
    if (beamline_version < 52)
        fprintf(fid6,postip_readingx, postip_readingy);
    else
        fprintf(fid6,ipc_readingx, ipc_readingy);
    end        
    if (use_mfb2ffkicker)
        fprintf(fid6,'%g \t %g', MFB2FF_x, MFB2FF_y);
    end
    fprintf(fid6,'\n');
    fclose(fid6);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Eval the ident results in detail %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(detailed_eval == 1)
    
    for i = 1:eval_interval:n_x
	error_local = norm(R_x_ident(:,i)-R_x(:,i), 'fro')./norm(R_x(:,i), 'fro');
	fprintf(f_col_x, '%g   ', error_local);
    end
    error_local = norm(R_x_ident(:,end)-R_x(:,end), 'fro')./norm(R_x(:,end), 'fro');
    fprintf(f_col_x, '%g\n', error_local);

    for i = 1:eval_interval:n_y
	error_local = norm(R_y_ident(:,i)-R_y(:,i), 'fro')./norm(R_y(:,i), 'fro');
	fprintf(f_col_y, '%g   ', error_local);
    end
    error_local = norm(R_y_ident(:,end)-R_y(:,end), 'fro')./norm(R_y(:,end), 'fro');
    fprintf(f_col_y, '%g\n', error_local);

    for i = 1:eval_interval:m
	error_local = norm(R_x_ident(i,:)-R_x(i,:), 'fro')./norm(R_x(i,:), 'fro');
	fprintf(f_row_x, '%g   ', error_local);
    end
    error_local = norm(R_x_ident(end,:)-R_x(end,:), 'fro')./norm(R_x(end,:), 'fro');
    fprintf(f_row_x, '%g\n', error_local);

    for i = 1:eval_interval:m
	error_local = norm(R_y_ident(i,:)-R_y(i,:), 'fro')./norm(R_y(i,:), 'fro');
	fprintf(f_row_y, '%g   ', error_local);
    end
    error_local = norm(R_y_ident(end,:)-R_y(end,:), 'fro')./norm(R_y(end,:), 'fro');
    fprintf(f_row_y, '%g\n', error_local);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Save the old excitations and measurements %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(save_excit_meas == 1)

    for i = 1:length(excit_x)
	fprintf(f_excit_x, '%g   ', excit_x(i));
    end
    fprintf(f_excit_x, '1 \n');

    for i = 1:length(excit_y)
	fprintf(f_excit_y, '%g   ', excit_y(i));
    end
    fprintf(f_excit_y, '1 \n');

    for i = 1:length(bpm_readings(:,1))
	fprintf(f_meas_x, '%g   ', bpm_readings(i,1));
    end
    fprintf(f_meas_x, '\n');

    for i = 1:length(bpm_readings(:,2))
	fprintf(f_meas_y, '%g   ', bpm_readings(i,2));
    end
    fprintf(f_meas_y, '\n');
end

%%%%%%%%%%%%%%%%%%%%%%%
% Delete unused files %
%%%%%%%%%%%%%%%%%%%%%%%

if (delete_files)
    if ( time_step_index <= multipulse_nr)
        % nothing to do yet
    else 
        useless_file_index = time_step_index - multipulse_nr;
        useless_file_name = sprintf('electron%d.dat', useless_file_index);
        delete(useless_file_name);
        if (save_beam_ff_start)
            useless_file_name = sprintf('electron_beampar_ff_start%d_%d.dat', useless_file_index,bunch_index);
            delete(useless_file_name);
        end
        if (save_beam_mfb2ff)
            useless_file_name = sprintf('electron_beampar_mfb2ff%d_%d.dat', useless_file_index,bunch_index);
            delete(useless_file_name);
        end
        if (save_beam_ip)
            useless_file_name = sprintf('electron_beampar_ip%d_%d.dat', useless_file_index,bunch_index);
            delete(useless_file_name);
        end
        if (response_matrix_calc==0)
            useless_file_name = sprintf('electron_beampar_ff_end%d_%d.dat', useless_file_index,bunch_index);
            delete(useless_file_name);
        end
    end    
end
