# Init the data needed for the system identification
#
# Juergen Pfingstner
# 9th of January 2012

% Positions of important elements

% First BPM has no excitation from the correctors
bpm_pos = placet_element_get_attribute(beamline_name, bpm_index, 's')';
bpm_length = placet_element_get_attribute(beamline_name, bpm_index, 'length')';
bpm_center = bpm_pos - bpm_length./2;
clear bpm_length;

dipole_pos = placet_element_get_attribute(beamline_name, dipole_index, 's')';
dipole_length = placet_element_get_attribute(beamline_name, dipole_index, 'length')';
dipole_center = dipole_pos - dipole_length./2;
clear dipole_length;

hcorr_pos = placet_element_get_attribute(beamline_name, hcorr_index, 's')';
hcorr_length = placet_element_get_attribute(beamline_name, hcorr_index, 'length')';
hcorr_center = hcorr_pos - hcorr_length./2;
clear hcorr_length;

vcorr_pos = placet_element_get_attribute(beamline_name, vcorr_index, 's')';
vcorr_length = placet_element_get_attribute(beamline_name, vcorr_index, 'length')';
vcorr_center = vcorr_pos - vcorr_length./2;
clear vcorr_length;

qp_pos = placet_element_get_attribute(beamline_name, qp_index, 's')';
qp_length = placet_element_get_attribute(beamline_name, qp_index, 'length')';
qp_center = qp_pos - qp_length./2;
clear qp_length;

% Choice of the used correctors

% If this is changed also the beta function of the correctors have to
% be recalculated
if(load_corr == 1)
    #hcorr_index_index = load(load_hcorr_name);
    #vcorr_index_index = load(load_vcorr_name);
    corr_index_index = load(load_corr_name);
    hcorr_index_index = corr_index_index.Index.x;
    vcorr_index_index = corr_index_index.Index.y;
    [hcorr_index_index,sort_order_h] = sort(hcorr_index_index(1:67));
    [vcorr_index_index,sort_order_v] = sort(vcorr_index_index(1:67));
else
    hcorr_index_index = 1:hcorr_modulo:length(hcorr_index);
    vcorr_index_index = 1:vcorr_modulo:length(vcorr_index);
end

corr_used_x_index = hcorr_index(hcorr_index_index);
corr_used_x_center = hcorr_center(hcorr_index_index);
corr_used_y_index = vcorr_index(vcorr_index_index);
corr_used_y_center = vcorr_center(vcorr_index_index);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Create correspondance between Correctors and BPMs %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

bpm_hcorr = zeros(length(bpm_index), 1);
pos_last_hcorr = corr_used_x_center(end);
for i=1:length(bpm_hcorr)
    pos_bpm = bpm_center(i);
    if(pos_bpm >= pos_last_hcorr)
        % No more correctors behind the BPM
        bpm_hcorr(i) = -1;
    else
        j = 1;
        while(pos_bpm > corr_used_x_center(j))
            j=j+1;
        end
        bpm_hcorr(i) = j-1;
    end
end

bpm_vcorr = zeros(length(bpm_index), 1);
pos_last_vcorr = corr_used_y_center(end);
for i=1:length(bpm_vcorr)
    pos_bpm = bpm_center(i);
    if(pos_bpm >= pos_last_vcorr)
        % No more correctors behind the BPM
        bpm_vcorr(i) = -1;
    else
        j = 1;
        while(pos_bpm > corr_used_y_center(j))
            j=j+1;
        end
        bpm_vcorr(i) = j-1;
    end
end

%save('bpm_hcorr.dat', 'bpm_hcorr');
%save('bpm_vcorr.dat', 'bpm_vcorr');

if(detailed_eval == 1)
    
    ident_detail_col_x_file_name = "ident_results_col_x.dat";
    f_col_x = fopen(ident_detail_col_x_file_name, 'wt');
    
    ident_detail_col_y_file_name = "ident_results_col_y.dat";
    f_col_y = fopen(ident_detail_col_y_file_name, 'wt');
    
    ident_detail_row_x_file_name = "ident_results_row_x.dat";
    f_row_x = fopen(ident_detail_row_x_file_name, 'wt');
    
    ident_detail_row_y_file_name = "ident_results_row_y.dat";
    f_row_y = fopen(ident_detail_row_y_file_name, 'wt');

end

if(save_excit_meas == 1)
    excit_x_file_name = "excit_x.dat";
    f_excit_x = fopen(excit_x_file_name, 'wt');
    
    excit_y_file_name = "excit_y.dat";
    f_excit_y = fopen(excit_y_file_name, 'wt');
    
    meas_x_file_name = "meas_x.dat";
    f_meas_x = fopen(meas_x_file_name, 'wt');
    
    meas_y_file_name = "meas_y.dat";
    f_meas_y = fopen(meas_y_file_name, 'wt');
end

if(identification_switch == 1)

    fprintf(1, "Load and init data for system identification ... ");
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Create scaling factors for the excitation %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if(excit_type == 3)
	% Not yet supported

        %% Andrea's excitation for constant max. BPM reading
        %excit_amp = load(load_amp_name);
        %hexcit_amp = excit_amp.Amplitudes.x;
        %vexcit_amp = excit_amp.Amplitudes.y;
        %hexcit_amp = hexcit_amp(1:67);
        %vexcit_amp = vexcit_amp(1:67);
        %hexcit_amp = hexcit_amp(sort_order_h);
        %vexcit_amp = vexcit_amp(sort_order_v);
    elseif(excit_type == 2)
        %% Excitation for constant emittance growth
	file_name_temp = strcat(data_dir,'/emitt_factor_x.dat');
        emitt_fact_x = load(file_name_temp);
	file_name_temp = strcat(data_dir,'/emitt_factor_y.dat');
        emitt_fact_y = load(file_name_temp);
        %delta_emitt_x_wish = alpha_x/100*spe_x_perf;
        %delta_emitt_y_wish = alpha_y/100*spe_y_perf;

	%% Excitation for constant growth of the projected beam size.
	%% Here we have to explain a few things. Note that the estimation of the 
	%% growth of the projected beam size is based on the scaling factors for the 
	%% projected emittance growth for the corrector. The scaling factors are based
	%% on emittances, since otherwise an oscillation originating at a corrector that has 
	%% a zeros phase at the IP would lead to an scaling factor that creates a huge oscillation along the 
	%% beam line, since actually no beam offset is created (assuming that there is no significant beam 
	%% size growth). Therefore, some correctors will not create a large offset and the beam size
	%% will only growth on average with the wanted value. It is well possible however that 
	%% there will develope problems due to this excitation, if to small oscillation are induces and the
	%% algorithm learns not fast enough. But lets see :)
	%%
	%% One more thing: the calculation of the exciatation !!!
	%% The excitation is calibrated for a certain emittance growth, due to reasons explained above. 
	%% Here, a constant growth of the projected beam size is wanted however. The wanted beam size growth
	%% in percent has to be tconverted to an equivalent growth of the projected emittance. The follwing 
	%% relation is used:
	%%
	%% sig_w/sig_0 = sqrt(eps_w/eps_0)
	%% 1+alpha/100 = sqrt(1+delta_eps_rel)
	%% => delta_eps_rel = (1+alpha/100)^2 -1
	%% => delta_eps = delta_eps_rel*eps0

	delta_eps_x_rel = (1+alpha_x/100)^2 - 1;
	delta_emitt_x_wish = delta_eps_x_rel*spe_x_perf

	delta_eps_y_rel = (1+alpha_y/100)^2 - 1;
	delta_emitt_y_wish = delta_eps_y_rel*spe_y_perf

    elseif(excit_type == 4)
	file_name_temp = strcat(data_dir,'/emitt_factor_x.dat');
        emitt_fact_x = load(file_name_temp);
	file_name_temp = strcat(data_dir,'/emitt_factor_y.dat');
        emitt_fact_y = load(file_name_temp);
	% Excitation for constant emittance growth calibration
        %emitt_fact_x = ones(size(corr_used_x_index));
        %emitt_fact_y = ones(size(corr_used_y_index));
        delta_emitt_x_wish = alpha_x/100*spe_x_perf;
        delta_emitt_y_wish = alpha_y/100*spe_y_perf;
    else
        %% For the first creation of the data
	%match_alpha_x = str2double(Tcl_GetVar('match(alpha_x)'));
	%match_alpha_y = str2double(Tcl_GetVar('match(alpha_y)'));
	%match_beta_x = str2double(Tcl_GetVar('match(beta_x)'));
	%match_beta_y = str2double(Tcl_GetVar('match(beta_y)'));
        %[s, beta_twiss_x_, beta_twiss_y, alpha_twiss_x, alpha_twiss_y, mu_x, mu_y, Dx, Dy] = placet_evolve_beta_function(beamline_name, match_beta_x, match_alpha_x, match_beta_y, match_alpha_y, all_index(1), all_index(end), 0, 0, 0, 0);
        %beta_x_corr = beta_twiss_x(hcorr_index);
        %beta_y_corr = beta_twiss_y(vcorr_index);
        %clear s beta_twiss_x beta_twiss_y alpha_twiss_x alpha_twiss_y mu_x mu_y Dx Dy;
	%file_name_temp = strcat(data_dir,'/beta_x_corr.dat');
        %save(file_name_temp, "beta_x_corr", "-ascii");
	%file_name_temp = strcat(data_dir,'/beta_y_corr.dat');
        %save(file_name_temp, "beta_y_corr", "-ascii");
        
        %% Random excitation scaled with the beta function
	%file_name_temp = strcat(data_dir,'/beta_x_corr.dat');
        %beta_x_corr = load('-ascii', file_name_temp);
	%file_name_temp = strcat(data_dir,'/beta_y_corr.dat');
        %beta_y_corr = load('-ascii', file_name_temp);
        
        %beta_x_corr = beta_x_corr(hcorr_index_index);
        %beta_y_corr = beta_y_corr(vcorr_index_index);

	%% Random excitation with a constant emittance
	%% For an explantation please see case excit_type = 2
	
	file_name_temp = strcat(data_dir,'/emitt_factor_x.dat');
        emitt_fact_x = load(file_name_temp);
	file_name_temp = strcat(data_dir,'/emitt_factor_y.dat');
        emitt_fact_y = load(file_name_temp);

	delta_eps_x_rel = (1+alpha_x/100)^2 - 1;
	delta_emitt_x_wish = delta_eps_x_rel*spe_x_perf;

	delta_eps_y_rel = (1+alpha_y/100)^2 - 1;
	delta_emitt_y_wish = delta_eps_y_rel*spe_y_perf;
	
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Init and load the data for the system identification %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    file_name_R = strcat(data_dir,'/R_x.dat');
    R_x = load(file_name_R);
    R_x = R_x(:,hcorr_index_index);
    norm2_R_x = norm(R_x, 'fro');
    disp_orbit = placet_get_bpm_readings(beamline_name, bpm_index, true);
    R_x = [R_x, disp_orbit(:,1)];
    norm_R_x = norm(R_x, 'fro');
    
    file_name_R = strcat(data_dir,'/R_y.dat');
    R_y = load(file_name_R);
    R_y = R_y(:,vcorr_index_index);
    norm2_R_y = norm(R_y, 'fro');
    R_y = [R_y, disp_orbit(:,2)];
    norm_R_y = norm(R_y, 'fro');
    %save('disp.dat', 'disp_orbit');
    
    [m,n_x] = size(R_x); 
    R_x_ident = zeros(m,n_x);
    %R_x_ident(116, end) = -160;
    [m,n_y] = size(R_y); 
    R_y_ident = zeros(m,n_y);
    
    if(init_P_algo == 1)
        P_x = eye(n_x)*init_P_value;
        P_y = eye(n_y)*init_P_value;
    else
        P_x = find_P_init(alpha_x./sqrt(beta_x_corr), lambda_x);
        P_y = find_P_init(alpha_y./sqrt(beta_y_corr), lambda_y);
    end
    
    if((rls_algo == 1) | (rls_algo == 2) | (rls_algo == 4) | (rls_algo == 6))
        
        % Create delete_matrices
        
        delete_x = zeros(size(R_x)); 
        delete_x(:,n_x) = ones(m,1);
        for i=1:m
            if(bpm_hcorr(i)<0)
                delete_x(i,:) = ones(1,n_x);
            else
                delete_x(i,1:bpm_hcorr(i)) = ones(1,length(bpm_vcorr(i))); 
            end
        end
        
        delete_y = zeros(size(R_y)); 
        delete_y(:,n_y) = ones(m,1);
        for i=1:m
            if(bpm_vcorr(i)<0)
                delete_y(i,:) = ones(1,n_y);
            else
                delete_y(i,1:bpm_vcorr(i)) = ones(1,length(bpm_vcorr(i))); 
            end
        end
        
        if(rls_algo == 6)
            
            %R_ident_x = zeros(m,n_x,2);
            %Ident_data_y = zeros(m,n_y,2);
            D_x_ident = zeros(m,n_x);
            D_y_ident = zeros(m,n_y);
            
            P_inv_col_x = zeros(n_x,1);
            P_inv_col_x(end) = 1./init_P_value;
            P_inv_diag_x = ones(n_x,1)./init_P_value;
            
            P_inv_col_y = zeros(n_y,1);
            P_inv_col_y(end) = 1./init_P_value;
            P_inv_diag_y = ones(n_y,1)./init_P_value;
            
            hcorr_bpm = zeros(n_x, 1);
            for i=1:n_x
                j=1;
                stop_it = 0;
                while(stop_it < 0.5)
                    if(delete_x(j,i) > 0.5)
                        hcorr_bpm(i) = j;
                        stop_it = 1;
                    else
                        j=j+1;
                    end
                end
            end
            
            vcorr_bpm = zeros(n_y, 1);
            for i=1:n_y
                j=1;
                stop_it = 0;
                while(stop_it < 0.5)
                    if(delete_y(j,i) > 0.5)
                        vcorr_bpm(i) = j;
                        stop_it = 1;
                    else
                        j=j+1;
                    end
                end
            end
        end
        
    elseif(rls_algo == 3)
        
        p_x = diag(P_x);
        p_y = diag(P_y);
        
        ident_data_x = struct('r', [], 'P', [], 'lambda', 1);
        for i=1:m
            if(bpm_hcorr(i) < 0)
                nr_elem = n_x;
            else
                nr_elem = bpm_hcorr(i)+1;
            end
            ident_data_x(i).r = zeros(nr_elem, 1); 
	        ident_data_x(i).P = diag([p_x(1:nr_elem-1)', p_x(end)])*eye(nr_elem);
                ident_data_x(i).lambda = lambda_x;
        end
        
        ident_data_y = struct('r', [], 'P', [], 'lambda', 1);
        for i=1:m
            if(bpm_vcorr(i) < 0)
                nr_elem = n_y;
            else
                nr_elem = bpm_vcorr(i)+1;
            end
            ident_data_y(i).r = zeros(nr_elem, 1); 
            ident_data_y(i).P = diag([p_y(1:nr_elem-1)', p_y(end)])*eye(nr_elem);
            ident_data_y(i).lambda = lambda_y;
        end
    elseif(rls_algo == 5)
        
        ident_data_x = struct('r', [], 'P', []);
        for i=1:m
            if(bpm_hcorr(i) < 0)
                nr_elem = n_x;
            else
                nr_elem = bpm_hcorr(i)+1;
            end
            ident_data_x(i).r = zeros(nr_elem, 1); 
            ident_data_x(i).P = init_P_value;
        end
        
        ident_data_y = struct('r', [], 'P', []);
        for i=1:m
            if(bpm_vcorr(i) < 0)
                nr_elem = n_y;
            else
                nr_elem = bpm_vcorr(i)+1;
            end
            ident_data_y(i).r = zeros(nr_elem, 1); 
            ident_data_y(i).P = init_P_value;
        end
    elseif((rls_algo == 7) | (rls_algo == 8))
        
        % Incremental covariance matrix
        
        ICM_x = cell(m,1);
        ICM_y = cell(m,1);
        for i = 1:m
            ICM_x{i} = init_ICM();
            ICM_y{i} = init_ICM();
        end
        
    else
        fprintf(1, 'Unknown RLS algo type\n');
    end
    
    fprintf(1, "finished\n");
    
end
