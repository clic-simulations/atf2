%randn("seed", $machine * 1e4);
%placet_element_set_attribute("test", DI, "strength_x", 0.0);
%placet_element_set_attribute("test", DI, "strength_y", 0.0);
%placet_element_set_attribute("test", BI, "resolution", $bpmres);
if (alignment_bpm)
    placet_element_set_attribute(beamline_name, bpm_index, "x", randn(size(bpm_index)) * alignment_sigma);
    placet_element_set_attribute(beamline_name, bpm_index, "y", randn(size(bpm_index)) * alignment_sigma);
end
if (alignment_qp)
    placet_element_set_attribute(beamline_name, qp_index, "x", randn(size(qp_index)) * alignment_sigma);
    placet_element_set_attribute(beamline_name, qp_index, "y", randn(size(qp_index)) * alignment_sigma);
end
if (alignment_drift)
    placet_element_set_attribute(beamline_name, drift_index, "x", randn(size(drift_index)) * alignment_sigma);
    placet_element_set_attribute(beamline_name, drift_index, "y", randn(size(drift_index)) * alignment_sigma);
end

if (use_quad_strength_error)
    quad_strength_orig = placet_element_get_attribute(beamline_name, qp_index, "strength");
    quad_strength = (1 + randn(size(qp_index))*quad_strength_error).*transpose(quad_strength_orig);
    placet_element_set_attribute(beamline_name, qp_index, "strength", quad_strength);
end
