% File to postprocess all results of one machine run

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Delete the useless files %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (delete_files)
    % Matlab was not capable of deleting more than one file (do not ask
    % why). Therefore the task is performed in Tcl
    %useless_file_name = sprintf('electron_linac_*.dat');
    %delete(useless_file_name); Did not work due to some strange reason
    
    Tcl_Eval("eval exec rm -f [glob -nocomplain electron*]");
    Tcl_Eval("eval exec rm -f [glob -nocomplain particles*]");
    Tcl_Eval("eval exec rm -f [glob -nocomplain beam.dat]");
    %Tcl_Eval("eval exec rm -f [glob -nocomplain pbhfacet*]");
end

%%%%%%%%%%%%%%%%%%%%%%%%
% Close the open files %
%%%%%%%%%%%%%%%%%%%%%%%%

if(detailed_eval == 1)
    fclose(f_col_x);
    fclose(f_col_y);
    fclose(f_row_x);
    fclose(f_row_y);
end

if(save_excit_meas == 1)
    fclose(f_excit_x);
    fclose(f_excit_y);
    fclose(f_meas_x);
    fclose(f_meas_y);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%
% Save the final results %
%%%%%%%%%%%%%%%%%%%%%%%%%%

if (identification_switch == 1)
    save("R_x.dat", "R_x_ident");
    save("R_y.dat", "R_y_ident");

    %if(rls_algo == 1)
    %    P_x = ident_data_x(end).P;
    %    P_y = ident_data_y(end).P;
    %end

    save("P_x.dat", "P_x");
    save("P_y.dat", "P_y");
end

if (save_tracking)
    % copy last file to generic name
    copyfile(strcat(substr(tracking_file_name,1,-4),num2str(time_step_index),".dat"),tracking_file_name);
end