% Script to initialise machine and do one time initialisations

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get data of BPMs, QPs and correctors %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Get indices of important elements

all_index = placet_get_name_number_list(beamline_name,'*');
start_index = placet_get_name_number_list(beamline_name,'START');
bpm_index = placet_get_number_list(beamline_name, "bpm");
bpm_name = placet_element_get_attribute(beamline_name, bpm_index,'name');
bpm_s = placet_element_get_attribute(beamline_name, bpm_index,'s');

mfb2ff_index = placet_get_name_number_list(beamline_name,"MFB2FF");
dipole_index = placet_get_number_list(beamline_name, 'dipole');
dipole_s = placet_element_get_attribute(beamline_name, dipole_index,'s');
dipole_name = placet_element_get_attribute(beamline_name, dipole_index,'name');

drift_index = placet_get_number_list(beamline_name, 'drift');

qp_index = placet_get_number_list(beamline_name, 'quadrupole');
sbend_index = placet_get_number_list(beamline_name, 'sbend');
multipole_index = placet_get_number_list(beamline_name, 'multipole');
magnet_index = sort([qp_index multipole_index]);
sf6ff_index = placet_get_name_number_list(beamline_name,'SF6FF*');
sf5ff_index = placet_get_name_number_list(beamline_name,'SF5FF*');
sd4ff_index = placet_get_name_number_list(beamline_name,'SD4FF*');
sf1ff_index = placet_get_name_number_list(beamline_name,'SF1FF*');
sd0ff_index = placet_get_name_number_list(beamline_name,'SD0FF*');
sextupole_index = sort([sf6ff_index sf5ff_index sd4ff_index sf1ff_index sd0ff_index]);
skewsext_index = placet_get_name_number_list(beamline_name,'SK*');
qd0ff_index = placet_get_name_number_list(beamline_name,'QD0FF');
qf1ff_index = placet_get_name_number_list(beamline_name,'QF1FF');
hcorr_index = placet_get_name_number_list(beamline_name,'ZH*');
vcorr_index = placet_get_name_number_list(beamline_name,'ZV*');
ipkicker_index = placet_get_name_number_list(beamline_name,"IPKICKER");
preip_index = placet_get_name_number_list(beamline_name,"MPREIP");
ipa_index = placet_get_name_number_list(beamline_name,"IPBPMA");
ipb_index = placet_get_name_number_list(beamline_name,"IPBPMB");
if (beamline_version < 52)
    postip_index = placet_get_name_number_list(beamline_name,"POSTIP");
else
    ipc_index = placet_get_name_number_list(beamline_name,"IPBPMC");
endif
ip_index = placet_get_name_number_list(beamline_name,"IP");
otr_index = placet_get_name_number_list(beamline_name,'OTR*');

% determine stripline, cavity bpms with/without attenuation 
% based on comparing with resolution (not guaranteed to work!)
bpm_resolution = placet_element_get_attribute(beamline_name,bpm_index,"resolution");
nr_stripline=0;
bpm_stripline_index=[];
nr_cband_att=0;
bpm_cband_att_index=[];
nr_cband_noatt=0;
bpm_cband_noatt_index=[];
nr_sband=0;
bpm_sband_index=[];
nr_ipbpm=0;
bpm_ipbpm_index=[];

for i=1:length(bpm_index)
    if (bpm_resolution(i) == stripline || bpm_resolution(i) == stripline2)
        nr_stripline+=1;
        bpm_stripline_index(nr_stripline) = bpm_index(i);
    elseif (bpm_resolution(i) == cband)
        nr_cband_att+=1;
        bpm_cband_att_index(nr_cband_att) = bpm_index(i);
    elseif (bpm_resolution(i) == cband_noatt)
        nr_cband_noatt+=1;
        bpm_cband_noatt_index(nr_cband_noatt) = bpm_index(i);
    elseif (bpm_resolution(i) == sband)
        nr_sband+=1;
        bpm_sband_index(nr_sband) = bpm_index(i);
    elseif (bpm_resolution(i) == ipbpm)
        nr_ipbpm+=1;
        bpm_ipbpm_index(nr_ipbpm) = bpm_index(i);
    end
end

if (use_font || IPFeedback || MFB2FFFeedback)
    bpmfont_index = placet_get_name_number_list(beamline_name,"FONTP*");
    fontp1_index = placet_get_name_number_list(beamline_name,"FONTP1");
    fontp2_index = placet_get_name_number_list(beamline_name,"FONTP2");
    fontp3_index = placet_get_name_number_list(beamline_name,"FONTP3");
    fontk1_index = placet_get_name_number_list(beamline_name,"FONTK1");
    fontk2_index = placet_get_name_number_list(beamline_name,"FONTK2");
end
if (use_mfb2ffkicker)
    mfb2ffkicker_index = placet_get_name_number_list(beamline_name,"MFB2FFKICKER");
end

element_index = sort([qp_index bpm_index dipole_index sextupole_index]);
element_s = placet_element_get_attribute(beamline_name,element_index,"s");
element_name = placet_element_get_attribute(beamline_name,element_index,"name");

bpmcorr_index = sort([bpm_index dipole_index]);
bpmcorr_name =  placet_element_get_attribute(beamline_name,bpmcorr_index,"name");
bpmcorr_s =  placet_element_get_attribute(beamline_name,bpmcorr_index,"s");

fid = fopen("bpmcorr.txt","wt");
for i=1:size(bpmcorr_s)
    fprintf(fid,'%s %g \n',bpmcorr_name(i,:),bpmcorr_s(i));
end
fclose(fid);

nr_bpms = length(bpm_index);
nr_hcorr = length(hcorr_index);
nr_vcorr = length(vcorr_index);

%f_temp = fopen("element_s.txt", 'wt');
%for i=1:size(element_s)
%    fprintf(f_temp, "%s %f \n", element_name(i,:), element_s(i))
%end
%fclose(f_temp);

if (nanoBPMSetup)
    ipt1_index = placet_get_name_number_list(beamline_name,"IPTBPM1");
    ipt2_index = placet_get_name_number_list(beamline_name,"IPTBPM2");
    ipt3_index = placet_get_name_number_list(beamline_name,"IPTBPM3");
end

%% Add Wakes
if (use_otr_wakes)
  placet_element_set_attribute(beamline_name,otr_index,"short_range_wake","cavbpm_cref");
end

if (use_sbend_wakes)
  placet_element_set_attribute(beamline_name,sbend_index,"short_range_wake","cavbpm_cref");
end

if (use_corrector_wakes)
  placet_element_set_attribute(beamline_name,hcorr_index,"short_range_wake","cavbpm_cref");
  placet_element_set_attribute(beamline_name,vcorr_index,"short_range_wake","cavbpm_cref");
end

if (use_drift_wakes)
    placet_element_set_attribute(beamline_name,drift_index,"short_range_wake","cavbpm_cref");
end

%%%


if (wakeFieldSetup)
    if (twoRefCavityFullSetup)
        fullwake = placet_get_name_number_list(beamline_name,"FULLWAKESETUP");
        wakeFieldSetup_index = fullwake;
    else
        bellow_mref1_index = placet_get_name_number_list(beamline_name,"BELLOW_MREF_1");
        flange_mref1_index = placet_get_name_number_list(beamline_name,"FLANGE_MREF_1");
        flange_mref2_index = placet_get_name_number_list(beamline_name,"FLANGE_MREF_2");
        bellow_mref2_index = placet_get_name_number_list(beamline_name,"BELLOW_MREF_2");

        if (oneRefCavity)
            aperture_step1_index = placet_get_name_number_list(beamline_name,"APERTURESTEP_1");
            mref3ff_index = placet_get_name_number_list(beamline_name,"MREF3FF");
            aperture_step2_index = placet_get_name_number_list(beamline_name,"APERTURESTEP_2");
            wakeFieldSetup_index = sort([flange_mref1_index ...
                                aperture_step1_index mref3ff_index ...
                                aperture_step2_index flange_mref2_index]);
        elseif (twoRefCavity)
            aperture_step1_index = placet_get_name_number_list(beamline_name,"APERTURESTEP_1");
            mref2ff_index = placet_get_name_number_list(beamline_name,"MREF2FF");
            aperture_step2_index = placet_get_name_number_list(beamline_name,"APERTURESTEP_2");
            flange_mref15_index = placet_get_name_number_list(beamline_name,"FLANGE_MREF_15");
            aperture_step3_index = placet_get_name_number_list(beamline_name,"APERTURESTEP_3");
            mref3ff_index = placet_get_name_number_list(beamline_name,"MREF3FF");
            aperture_step4_index = placet_get_name_number_list(beamline_name,"APERTURESTEP_4");
            wakeFieldSetup_index = sort([flange_mref1_index ...
                                aperture_step1_index mref2ff_index ...
                                aperture_step2_index flange_mref15_index ...
                                aperture_step3_index mref3ff_index ...
                                aperture_step4_index flange_mref2_index]);
        elseif (bellowWakeStudy || oneBellow || oneBellowShielded)
            bellow_mover_index = placet_get_name_number_list(beamline_name,"BELLOW_MOVER*");
            wakeFieldSetup_index = sort([flange_mref1_index bellow_mover_index flange_mref2_index]);

        else 
            wakeFieldSetup_index = [];
        end
        wakeFieldSetup_bellow_index = sort([bellow_mref1_index bellow_mref2_index]);
    end
    if (wakeFieldDipole)
        wakefielddipole_index = placet_get_name_number_list(beamline_name,"WAKE_DIPOLE")
    end
    if (wakeFieldSetupQuadMove)
       qd10aff_index = placet_get_name_number_list(beamline_name,'QD10AFF');
       qd10bff_index = placet_get_name_number_list(beamline_name,'QD10BFF');
    end
end

if (sd4ff_mover_scan_x || sd4ff_mover_scan_y)
    msd4ff_index = placet_get_name_number_list(beamline_name,'MSD4FF');
end

% s positions of BPMs
bpm_s = placet_element_get_attribute(beamline_name,bpm_index,"s");
bpm_name = placet_element_get_attribute(beamline_name,bpm_index,"name");
if (use_font)
    bpmfont_s = placet_element_get_attribute(beamline_name,bpmfont_index,"s");
end
if (mfb2ff_index)
    mfb2ff_s = placet_element_get_attribute(beamline_name,mfb2ff_index,"s");
end
qp_name = placet_element_get_attribute(beamline_name,qp_index,"name");

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Change lattice settings %
%%%%%%%%%%%%%%%%%%%%%%%%%%%

placet_element_set_attribute(beamline_name,ipa_index,"resolution",ipbpma);
placet_element_set_attribute(beamline_name,ipb_index,"resolution",ipbpmb);

if (beamline_version > 51)
    placet_element_set_attribute(beamline_name,ipc_index,"resolution",ipbpmc);
end

if (strcmp(beamline_name,"ATF2") || strcmp(beamline_name,"ATF2_v5.1"))
    qd20x_index = placet_get_name_number_list(beamline_name,'QD20X');
    qf21x_index = placet_get_name_number_list(beamline_name,'QF21X');
    qm16ff_index = placet_get_name_number_list(beamline_name,'QM16FF');
    qm15ff_index = placet_get_name_number_list(beamline_name,'QM15FF');
    qm14ff_index = placet_get_name_number_list(beamline_name,'QM14FF');
    qm13ff_index = placet_get_name_number_list(beamline_name,'QM13FF');
    qm12ff_index = placet_get_name_number_list(beamline_name,'QM12FF');
    qm11ff_index = placet_get_name_number_list(beamline_name,'QM11FF');
    
    % values from ATF2.mad8
    if (strcmp(optics,"nominal") || strcmp(optics,"BX1BY1m"))
        printf("Optics: BX1BY1m\n")
        placet_element_set_attribute(beamline_name,qd20x_index,"strength",0.5*e0_start*-0.266022963369);
        placet_element_set_attribute(beamline_name,qf21x_index,"strength",0.5*e0_start*0.55);
        placet_element_set_attribute(beamline_name,qm16ff_index,"strength",0.5*e0_start*0.0);
        placet_element_set_attribute(beamline_name,qm15ff_index,"strength",0.5*e0_start*0.197460336891);
        placet_element_set_attribute(beamline_name,qm14ff_index,"strength",0.5*e0_start*-1.364012087245);
        placet_element_set_attribute(beamline_name,qm13ff_index,"strength",0.5*e0_start*0.949014876758);
        placet_element_set_attribute(beamline_name,qm12ff_index,"strength",0.5*e0_start*-0.233774057387);
        placet_element_set_attribute(beamline_name,qm11ff_index,"strength",0.5*e0_start*0.159982882393);
    
        % special sextupole settings? 
    
    elseif (strcmp(optics,"BX10BY1m"))
        printf("Optics: BX10BY1m\n")
        placet_element_set_attribute(beamline_name,qd20x_index,"strength",0.5*e0_start*-0.304096730505);
        placet_element_set_attribute(beamline_name,qf21x_index,"strength",0.5*e0_start*0.45);
        placet_element_set_attribute(beamline_name,qm16ff_index,"strength",0.5*e0_start*0.0);
        placet_element_set_attribute(beamline_name,qm15ff_index,"strength",0.5*e0_start*0.644607295568);
        placet_element_set_attribute(beamline_name,qm14ff_index,"strength",0.5*e0_start*-1.517448781982);
        placet_element_set_attribute(beamline_name,qm13ff_index,"strength",0.5*e0_start*0.923014954517);
        placet_element_set_attribute(beamline_name,qm12ff_index,"strength",0.5*e0_start*-0.232657890125);
        placet_element_set_attribute(beamline_name,qm11ff_index,"strength",0.5*e0_start*0.159982882393);
    
        % special sextupole settings? 

    elseif (strcmp(optics,"BX10BY1nl"))
        printf("Optics: BX10BY1nl\n")
        % nothing to do, default value
    else 
        printf("WARNING Optics settings not known!\n")
        exit(1)
    end
end

if (use_set_file == 1)
    set_file = strcat(data_dir,set_file_name);
    [fid,msg] = fopen(set_file);
    if (fid == -1) 
        printf("%s\n",msg);
        printf("Set file %s not found!\n",set_file_name);
        exit(1)
    end
    fclose(fid);
    
    addpath(strcat(script_dir,'/scripts/convertsetfile'))
    % magnet strength
    [magnetName, magnetStrength] = ATF2model(set_file,1);

    if (any(isnan(magnetStrength)))
       puts "Conversion to set file was not succesful!";
       magnetName
       magnetStrength
       exit(1)
    end
    
    for i=1:length(magnetName)
        index = placet_get_name_number_list(beamline_name,cellstr(magnetName(i,:)));
        % convert Mad parameters to PLACET (* e0)
        % divide by number of magnets with same name
        % (length(index)), usually 2
        placetStrength = magnetStrength(i)*e0_start/length(index);
        
        % sbends:
        if ((strcmp(cellstr(magnetName(i,:)),"QM6RX")) || strcmp(cellstr(magnetName(i,:)),"QM7RX"))
            placet_element_set_attribute(beamline_name,index,"K",placetStrength);
        % sextupoles:
        elseif ((strcmp(cellstr(magnetName(i,:)),"SF6FF")) || ...
                (strcmp(cellstr(magnetName(i,:)),"SF5FF")) || ...
                (strcmp(cellstr(magnetName(i,:)),"SD4FF")) || ...
                (strcmp(cellstr(magnetName(i,:)),"SF1FF")) || ...
                (strcmp(cellstr(magnetName(i,:)),"SD0FF")) || ...
                (strcmp(cellstr(magnetName(i,:)),"SK1FF")) || ...
                (strcmp(cellstr(magnetName(i,:)),"SK2FF")) || ...
                (strcmp(cellstr(magnetName(i,:)),"SK3FF")) || ...
                (strcmp(cellstr(magnetName(i,:)),"SK4FF"))
            )

            placet_element_set_attribute(beamline_name,index,"strength",complex(placetStrength+0.0i));
        % quads:
        else
            placet_element_set_attribute(beamline_name,index,"strength",placetStrength);
            
        end
    end
    % magnet position (x [um], y [um], tilt [urad])
    [magnetName,xpos,ypos,roll] = get_ATF2_pos(set_file);
    for i=1:length(magnetName)
        index = placet_get_name_number_list(beamline_name,cellstr(magnetName(i,:)));
        % sign convention of set file
        % looking downstream (IP direction):
        % +x is right
        % +y is down
        % +roll is clockwise
        % sign convention of PLACET
        % +x is right
        % +y is up
        % +roll is counterclockwise
        placet_element_set_attribute(beamline_name,index,"x",xpos(i));
        placet_element_set_attribute(beamline_name,index,"y",-ypos(i));
        placet_element_set_attribute(beamline_name,index,"roll",roll(i));
    end
end
    
if (use_sextupole == 0)
    placet_element_set_attribute(beamline_name, sextupole_index, "strength", complex(0.0+0.0i));
end

if (use_skewsextupoles == 0)
    placet_element_set_attribute(beamline_name, skewsext_index, "strength", complex(0.0+0.0i));
end

if (use_multipole_components == 0)
    % set all multipoles with length 0 to strength 0:
    multipole_length = placet_element_get_attribute(beamline_name, multipole_index, "length");
    for i=1:length(multipole_index)
        if (multipole_length(i)==0)
            placet_element_set_attribute(beamline_name, multipole_index(1,i), "strength", complex(0.0+0.0i));
        end
    end
end

if (use_qd0ff_strength == 1)
    placet_element_set_attribute(beamline_name,qd0ff_index,"strength",qd0ff_strength);
end

if (use_qf1ff_strength == 1)
    %qf1ff_strength_nom = placet_element_get_attribute(beamline_name,qf1ff_index,"strength")
    placet_element_set_attribute(beamline_name,qf1ff_index,"strength",qf1ff_strength);
end

if (use_qd0ff_current == 1)
    addpath(strcat(script_dir,'/scripts/convertsetfile'));
    Iquad = zeros(51);
    Iquad(51) = qd0ff_current;
    qd0ff_atf2strength = get_ATF2_quads(1,e0_start,Iquad,1)(51);
    % convert to PLACET units:
    qd0ff_strength = qd0ff_atf2strength*e0_start/length(qd0ff_index);
    placet_element_set_attribute(beamline_name,qd0ff_index,"strength",qd0ff_strength);
end

if (use_qf1ff_current == 1)
    addpath(strcat(script_dir,'/scripts/convertsetfile'))
    Iquad = zeros(51);
    Iquad(50) = qf1ff_current;
    qf1ff_atf2strength = get_ATF2_quads(1,e0_start,Iquad,1)(50);
    % convert to PLACET units:
    qf1ff_strength = qf1ff_atf2strength*e0_start/length(qf1ff_index);
    placet_element_set_attribute(beamline_name,qf1ff_index,"strength",qf1ff_strength);
end

if (use_qd0ff_position == 1)
    placet_element_set_attribute(beamline_name,qd0ff_index,"x",qd0ff_positionx);
    placet_element_set_attribute(beamline_name,qd0ff_index,"y",qd0ff_positiony);
    placet_element_set_attribute(beamline_name,qd0ff_index,"roll",qd0ff_roll);
end

if (use_qf1ff_position == 1)
    placet_element_set_attribute(beamline_name,qf1ff_index,"x",qf1ff_positionx);
    placet_element_set_attribute(beamline_name,qf1ff_index,"y",qf1ff_positiony);
    placet_element_set_attribute(beamline_name,qf1ff_index,"roll",qf1ff_roll);
end

%if (identification_switch == 1) 
%    source (strcat(script_dir,"/scripts/init_sysident.m"));
%end

if (nanoBPMSetup)
    placet_element_set_attribute(beamline_name,ipt1_index,"resolution",iptbpm1);
    placet_element_set_attribute(beamline_name,ipt2_index,"resolution",iptbpm2);
    placet_element_set_attribute(beamline_name,ipt3_index,"resolution",iptbpm3);
end

if (wakeFieldSetup)
    if (!twoRefCavityFullSetup)
        % move bellows half way
        placet_element_set_attribute(beamline_name,wakeFieldSetup_bellow_index,"y",0.5*1000*wakeFieldSetup_pos);
    end
    placet_element_set_attribute(beamline_name,wakeFieldSetup_index,"y",1000*wakeFieldSetup_pos);

    if (wakeFieldDipole)
        placet_element_set_attribute(beamline_name,wakefielddipole_index,"strength_y",wakeFieldDipole_strength);
    end
end

if (sd4ff_mover_scan_x == 1)
    % bellows?
    placet_element_set_attribute(beamline_name,msd4ff_index,"x",-1500.0);
end

if (sd4ff_mover_scan_y == 1)
    %bellows?
    placet_element_set_attribute(beamline_name,msd4ff_index,"y",-1500.0);
end
    
if(use_font == 1)
    
    placet_element_set_attribute(beamline_name, fontk1_index, "strength_x", fontk1_strengthx);
    placet_element_set_attribute(beamline_name, fontk1_index, "strength_y", fontk1_strengthy);
    placet_element_set_attribute(beamline_name, fontk2_index, "strength_x", fontk2_strengthx);
    placet_element_set_attribute(beamline_name, fontk2_index, "strength_y", fontk2_strengthy);
    placet_element_set_attribute(beamline_name, ipkicker_index, "strength_x", ipkicker_strengthx);
    placet_element_set_attribute(beamline_name, ipkicker_index, "strength_y", ipkicker_strengthy);
end

if(use_bunch_correlation == 1)

    r = bunchTobunchCorrelation;
    bunch_jitter_x  = sqrt((1-r^2)/r^2)*beam_jitter_x;
    bunch_jitter_xp = sqrt((1-r^2)/r^2)*beam_jitter_xp;
    bunch_jitter_y  = sqrt((1-r^2)/r^2)*beam_jitter_y;
    bunch_jitter_yp = sqrt((1-r^2)/r^2)*beam_jitter_yp;

    % switch on bunch jitter, if that wasn't the case yet. (print warning?)
    use_bunch_jitter = 1;
    
    if(use_beam_jitter == 0)
        puts("correlation calculation for bunch correlation is not possible when beam jitter is off, switch on beam jitter, or bunch correlation off");
        exit(1);
    end
end

if (bpm_direction == 1)
    placet_element_set_attribute(beamline_name,bpm_index,"direction",true);
end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run the first time to get some initial values %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

printf("Calculate the perfect parameters\n");

source (strcat(script_dir,"/scripts/tracking.m"))

% Twiss parameters for each quadrupole based on tracking
%Tcl_Eval("TwissPlot -beam $beam_name -file twiss.dat");
%Tcl_Eval("TwissPlotStep -beam $beam_name -step 0.01 -file twiss_step.dat");

%BI = placet_get_number_list(beamline_name, "bpm");

%out = placet_get_transfer_matrix_elements_fit(beamline_name, beam_name, ...
%                                              "None", { "11", "12", ...
%                    "16", "43", "44" }, 0, BI);

%save -text matrix_elements.dat out;
%save -text bpm_names.dat bpm_name;

% Twiss parameters based on first order transfer matrices
[evolve_s, evolve_beta_x, evolve_beta_y, evolve_alpha_x, evolve_alpha_y, ...
 evolve_mu_x, evolve_mu_y, evolve_Dx, evolve_Dy, evolve_E] = placet_evolve_beta_function(beamline_name, match_beta_x, match_alpha_x, match_beta_y, match_alpha_y);

fid_evolve = fopen('twiss_evolve.dat', 'wt');
if (add_header)
    fprintf(fid_evolve, "# s\t beta_x\t beta_y\t alpha_x\t alpha_y\t mu_x\t mu_y\t disp_x\t disp_y E\n")
end
for i=1:size(evolve_s)
    fprintf(fid_evolve,'%g\t %g\t %g\t %g\t %g\t %g\t %g\t %g\t %g \n', evolve_s(i), evolve_beta_x(i), evolve_beta_y(i), ...
            evolve_alpha_x(i), evolve_alpha_y(i), evolve_mu_x(i), ...
            evolve_mu_y(i), evolve_Dx(i), evolve_Dy(i), evolve_E(i))
end
fclose(fid_evolve);
clear fid_evolve;

orbit_perf = bpm_readings;
spe_x_perf = Emitt(end,2);
spe_y_perf = Emitt(end,6);
mpe_x = 0;
mpe_y = 0;

x0 = mean(Beam(:,2));
xp0 = mean(Beam(:,5));
y0 = mean(Beam(:,3));
yp0 = mean(Beam(:,6));
sps_x_perf = std(Beam(:,2)); 
sps_y_perf = std(Beam(:,3));
Tcl_SetVar('x0', x0);
Tcl_SetVar('xp0', xp0);
Tcl_SetVar('y0', y0);
Tcl_SetVar('yp0', yp0);

if (use_quad_strength_jitter==1)
    qp_strength = placet_element_get_attribute(beamline_name,qp_index,"strength");
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate response matrices %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (response_matrix_calc == 1)
    source (strcat(script_dir,"/scripts/response_matrix_calc.m"));
end

if (response_knob_matrix_calc == 1)
    source (strcat(script_dir,"/scripts/response_knob_matrix_calc.m"));
end

if (response_matrix_load == 1)
    response_matrix_name = strcat(script_dir,'/scripts/',response_matrix_file);
    if exist(response_matrix_name, "file")
        load(response_matrix_name);
    else
        printf("WARNING Response matrix %s not found!\n", response_matrix_file);
        exit(1);
    end    
end

if (response_knob_matrix_load == 1)
    response_matrix_name = strcat(script_dir,'/scripts/',response_knob_matrix_file);
    if exist(response_matrix_name, "file")
        load(response_matrix_name);
    else
        printf("WARNING Response matrix %s not found!\n", response_matrix_file);
        exit(1);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%
% Identification switch %
%%%%%%%%%%%%%%%%%%%%%%%%%

if (identification_switch == 1) 
    init_sysident
end

%%%%%%%%%%%%%%%%%
% Ground motion %
%%%%%%%%%%%%%%%%%

if (use_ground_motion == 1)
  Tcl_Eval("ground_motion_init $beamline_name $groundmotion(type) $groundmotion(model) 1");

  % apply first ground motion (no moving as first motion is zero)
  if(groundmotion_type == 2)
    Tcl_Eval("ground_motion $beamline_name $groundmotion(type)");
  end
else
  fprintf(1,'No ground motion will be applied\n');
end

%%%%%%%%%%%%%%%%
% Misalignment %
%%%%%%%%%%%%%%%%

if(use_misalignment)
    source (strcat(script_dir,"/scripts/my_survey.m"));
    %placet_element_get_attribute(beamline_name, qp_index, "y")
end

%%%%%%%%%%%
% DFS WFS %
%%%%%%%%%%%

if(use_dfs || use_wfs)
    % Binning
    steering_nbins = 1; % adapt if chosen for more than one bin
    noverlap = 0;
    nBpms = length(Response.Bpms);
    Blen = nBpms / (steering_nbins * (1 - noverlap) + noverlap);
    for i=1:steering_nbins
        Bmin = floor((i - 1) * Blen - (i - 1) * Blen * noverlap) + 1;
        Bmax = floor((i)     * Blen - (i - 1) * Blen * noverlap);
        Cxmin = find(Response.Cx < Response.Bpms(Bmin))(end);
        Cxmax = find(Response.Cx < Response.Bpms(Bmax))(end);
        Cymin = find(Response.Cy < Response.Bpms(Bmin))(end);
        Cymax = find(Response.Cy < Response.Bpms(Bmax))(end);
        if (steering_select_bpms)
            % MQF1X, MQF3X, MQF4X, MQD5X, MQF7X, MQF9X, MQF11X,
            % MQD14X, MQD16X, MQD18X,
            % MQD20X, FB2FF, QD10BFF,
            % QD2BFF
            Bins(i).Bpms = [2,4,5,6,8,10,13,17,20,22,24,29,33,48];
        else
            Bins(i).Bpms = Bmin:Bmax;
        end
        Bins(i).Cx = Cxmin:Cxmax;
        Bins(i).Cy = Cymin:Cymax;
    end
    printf("Each steering bin contains approx %g bpms.\n", round(Blen));
    
    % make indices for more than one bin!
    for bin = 1:steering_nbins
        Bin = Bins(bin);
        nCx = length(Bin.Cx);
        nCy = length(Bin.Cy);
        DFSWFS_Bpms = Response.Bpms(Bin.Bpms);
        DFSWFS_B = Response.B(Bin.Bpms,:);
        Rx = Response.Rx(Bin.Bpms,Bin.Cx);
        Ry = Response.Ry(Bin.Bpms,Bin.Cy);
        if (use_dfs)
            DFSWFS_BDFS = Response.BDFS(Bin.Bpms,:);
            RxDFS = Response.RxDFS(Bin.Bpms,Bin.Cx);
            RyDFS = Response.RyDFS(Bin.Bpms,Bin.Cy);
        end
        if (use_wfs)
            DFSWFS_BWFS = Response.BWFS(Bin.Bpms,:);
            RxWFS = Response.RxWFS(Bin.Bpms,Bin.Cx);
            RyWFS = Response.RyWFS(Bin.Bpms,Bin.Cy);
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%
% Open storage files %
%%%%%%%%%%%%%%%%%%%%%%

f_temp = fopen(meas_station_file_name, 'wt');
if (add_header == 1)
    fprintf(f_temp, "# time\t orbit_x_std\t orbit_y_std\t spe_x\t spe_y\t offset_x\t offset_y\t orbit_x_max\t orbit_y_max\t sps_x\t sps_y\t mpe_x\t mpe_y\t")
    if (wakeFieldSetup && size(wakeFieldSetup_index)>0)
        fprintf(f_temp, "wakeFieldSetup_pos\t")
    end
    if(identification_switch == 1)
        fprintf(f_temp, "error1_ident_x\t error1_ident_y\t norm_P_x norm_P_y\t error2_ident_x\t error2_ident_y\t")
    end
end
fprintf(f_temp,'\n')
fclose(f_temp);

if (save_beam_ip == 1)
    f_temp = fopen(meas_station_ip_file_name, 'wt');
    if (add_header == 1)
        fprintf(f_temp, "# time\t x\t xp\t y\t yp\t sxx\t sxpxp\t syy\t sypyp\t szz\t see\t sxy\t sxe\t sxpe\t sye\t sype\t emitt_x\t emitt_y\t disp_x\t disp_xp\t disp_y\t disp_yp\t alpha_x\t alpha_y\t beta_x\t beta_y\t e\t e_spread")
        if (wakeFieldSetup && size(wakeFieldSetup_index)>0)
            fprintf(f_temp, "\t wakeFieldSetup_pos")
        end
    end
    fprintf(f_temp,'\n')
    fclose(f_temp);
end

if (save_beam_ff_start == 1)
    f_temp = fopen(meas_station_ff_start_file_name, 'wt');
    if (add_header == 1) 
        fprintf(f_temp, "# time\t x\t xp\t y\t yp\t sxx\t sxpxp\t syy\t sypyp\t szz\t see\t sxy\t sxe\t sxpe\t sye\t sype\t emitt_x\t emitt_y\t disp_x\t disp_xp\t disp_y\t disp_yp\t alpha_x\t alpha_y\t beta_x\t beta_y\t e\t e_spread")
    end
    fprintf(f_temp,'\n')
    fclose(f_temp);
end

if (save_beam_mfb2ff == 1)
    f_temp = fopen(meas_station_mfb2ff_file_name, 'wt');
    if (add_header == 1) 
        fprintf(f_temp, "# time\t x\t xp\t y\t yp\t sxx\t sxpxp\t syy\t sypyp\t szz\t see\t sxy\t sxe\t sxpe\t sye\t sype\t emitt_x\t emitt_y\t disp_x\t disp_xp\t disp_y\t disp_yp\t alpha_x\t alpha_y\t beta_x\t beta_y\t e\t e_spread")
    end
    fprintf(f_temp,'\n')
    fclose(f_temp);
end

% BPM readings
f_temp = fopen(bpm_readings_file_name_x, 'wt');
fclose(f_temp);
f_temp = fopen(bpm_readings_file_name_y, 'wt');
fclose(f_temp);
if (bpm_direction == 1)
    f_temp = fopen(bpm_readings_file_name_xp, 'wt');
    fclose(f_temp);
    f_temp = fopen(bpm_readings_file_name_yp, 'wt');
    fclose(f_temp);
end

% One file for each bunch:
for i=2:n_bunches
    temp_name = strcat(bpm_readings_file_name_x(1:end-4),int2str(i),'.dat');
    f_temp = fopen(temp_name, 'wt');
    fclose(f_temp);
    temp_name = strcat(bpm_readings_file_name_y(1:end-4),int2str(i),'.dat');
    f_temp = fopen(temp_name, 'wt');
    fclose(f_temp);
    if (bpm_direction == 1)
        temp_name = strcat(bpm_readings_file_name_xp(1:end-4),int2str(i),'.dat');
        f_temp = fopen(temp_name, 'wt');
        fclose(f_temp);
        temp_name = strcat(bpm_readings_file_name_yp(1:end-4),int2str(i),'.dat');
        f_temp = fopen(temp_name, 'wt');
        fclose(f_temp);
    end
end

% Magnet Positions
f_temp = fopen(magnetPosition_readings_file_name_x, 'wt');
fclose(f_temp);
f_temp = fopen(magnetPosition_readings_file_name_y, 'wt');
fclose(f_temp);

% FONT/IP feedback file
if (use_font || IPFeedback || MFB2FFFeedback)
    f_temp = fopen(ip_feedback_file_name, 'wt');
    if (add_header == 1) 
        fprintf(f_temp, "# time\t train\t bunch\t FONTK1X\t FONTK1Y\t FONTK2X\t FONTK2Y\t IPkickerX\t IPkickerY\t FONTBPM1X\t FONTBPM1Y\t FONTBPM2X\t FONTBPM2Y\t FONTBPM3X\t FONTBPM3Y\t MFB2FFX\t MFB2FFY\t PREIPx\t PREIPy\t IPAx\t IPAy\t IPBx\t IPBy\t ");
        if (beamline_version < 52)
            fprintf(f_temp,"POSTIPx\t POSTIPy");
        else
            fprintf(f_temp,"IPCx\t IPCy");
        end
        if (use_mfb2ffkicker)
           fprintf(f_temp,"\t MFB2FFkickerX\t MFB2FFkickerY"); 
        end
        fprintf(f_temp,'\n');
    end
    fclose(f_temp);
end
