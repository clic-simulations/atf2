% Implements the function rls_full.m
% 
% Function: It implements one step of the recursive least square algorithm (RLS),
% for the full orbit response matrix.
%
% Juergen Pfingstner
% 13. Januar 2012

function [R_new] = lms_full(bpm, phi, R_old, lambda, delete_matrix)
% Usage of [R_new] = lms_full(bpm, phi, R_old, lambda, delete_matrix)
% 
% Variables:
%
%      Input:
%            bpm:     Measurement values of the BPMs
%            phi:     Excitation values
%            R_old:   Last estimated R
%            lambda:  Forgetting factor
%	     delete_matrix: matrix to create the right update
%
%      Output:
%            R_new:   Actual estimated R
%

% Calculate the error of the last measurement
error = bpm - R_old*phi;

% Calculate the new estimated values
R_new = R_old + (lambda .* diag(error)) * delete_matrix * diag(phi);

end


