#####################
# Define parameters #
#####################

set time_step_index 0 

Octave {
    rand("state", seed_nr);
    randn("state", seed_nr);
    beamline_name = Tcl_GetVar("beamline_name");
    beam_name = Tcl_GetVar("beam_name");
    time_step_index = Tcl_GetVar("time_step_index");
    global time = 0; %start time
}

# set all streams
if {$PLACETNewSeed} {
    RandomReset -seed $seed_nr
} else {
    RandomReset -seed "$seed_nr $seed_nr $seed_nr $seed_nr"
}

# load ground motion scripts
if {$use_ground_motion == 1} {
  source $script_dir/scripts/ground_motion.tcl
}

# Tcl procedures for TclCalls in the beamline
source $script_dir/scripts/tracking_procs.tcl

# Set appropriate wakefield
source $script_dir/scripts/wakefield.tcl

set e0 $e0_start
SetReferenceEnergy $e0_start

# add beamline
Girder
ReferencePoint -name "START" -angle 1 -sens 1
if { $beamline_name == "ATF2" || $beamline_name == "ATF2_v5.2"} {
    set beamline_version 52
    source $lattice_dir/ATF2_v5.2.tcl
} elseif { $beamline_name == "ATF2_v5.2-MadX"} {
    set beamline_version 52
    proc HCORRECTOR { a b c d } { Dipole $a $b $c $d }
    proc VCORRECTOR { a b c d } { Dipole $a $b $c $d }
    source $lattice_dir/ATF2_v5.2-MadX.tcl
    #source $lattice_dir/ATF2_v5.2-MadX-ATF2BPMNames.tcl
} elseif { $beamline_name == "ATF2_v5.1"} {
    set beamline_version 51
    source $lattice_dir/ATF2_v5.1.tcl
} elseif { $beamline_name == "ATF2_v4.0" } {
    set beamline_version 40
    source $lattice_dir/ATF2_v4.0.tcl;
} elseif { $beamline_name == "UL" } {
    set beamline_version 42
    source $lattice_dir/ATF2_UltraLow_v4.2_noMULTS.tcl;
} else {
    puts "lattice unknown, check beamline option"
    exit
}
Octave {
    beamline_version = Tcl_GetVar("beamline_version");
}

ReferencePoint -sense -1
#if {$response_matrix_calc==0} {
    TclCall -name "FF_END" -script "save_beam_ff_end"
#}

puts "Final beam energy at IP: $e0 GeV"

BeamlineSet -name $beamline_name
#BeamlineSaveFootprint -file $beamline_name.dat

#######################
# Setting up the beam #
#######################

set scripts $script_common_dir/scripts  ; # This is necessary for compatibility (bad though)

# load RF and injection scripts:
source $script_common_dir/scripts/init.tcl;

# loading of the beam making scripts
source $script_common_dir/scripts/ilc_beam.tcl;
# Total number of particles
set n_total [expr $n * $n_slice]

# make initial perfect beam
if {$particle_beam} {
    make_beam_many_energy $beam_name $n_slice $n 1
} else {
    make_beam_slice $beam_name $n_slice 100
}

if {$use_dfs && !$dynamic_beam } {
    Octave {
	change_energy(dfs_dE);
	beam_name_dfs = strcat(beam_name,'_dfs')
    }
    if {$particle_beam} {
	make_beam_many_energy ${beam_name}_dfs $n_slice $n 1
	BeamAddOffset -beam ${beam_name}_dfs -x $x_offset_dfs -y $y_offset_dfs -angle_x $beam_xangle_dfs -angle_y $beam_yangle_dfs
        BeamDump -beam ${beam_name}_dfs -file beam_dfs_1.in
    } else {
	make_beam_slice ${beam_name}_dfs $n_slice 100
    }
    Octave {
	#put energy back
	change_energy(1./dfs_dE);    
    }
}

if {($use_wfs || $use_one_to_one_steering) && !$dynamic_beam } {
    Octave {
	change_charge(wfs_dcharge);
        change_energy(wfs_dE); # Energy change of the initial wfs beam
	beam_name_wfs = strcat(beam_name,'_wfs')
    }
    if {$particle_beam} {
	make_beam_many_energy ${beam_name}_wfs $n_slice $n 1
        BeamAddOffset -beam ${beam_name}_wfs -x $x_offset_wfs -y $y_offset_wfs -angle_x $beam_xangle_wfs -angle_y $beam_yangle_wfs
        BeamDump -beam ${beam_name}_wfs -file beam_wfs_1.in
    } else {
	make_beam_slice ${beam_name}_wfs $n_slice 100
    }
    Octave {
	#put charge back
	change_charge(1./wfs_dcharge); 
	change_energy(1./wfs_dE);   
    }
}

if {$use_disp_free_emittance} {
    DispersionFreeEmittance -on
}

#puts "phase advance"

#set bunch_index 1

#PhaseAdvance -beamline $beamline_name -beam $beam_name -file_result phaseadvance.txt -file_detail phaseadvancedetail.txt

# align to zero
Zero

