if {!$use_wakefield} {
    set bellow_wake ""
    set flange_wake ""
    set cbandref_wake ""
    set cband_wake ""
    set sband_wake ""
    set aperture_2024_wake ""
    set aperture_2420_wake ""
    set resistive_wall_wake_3cm ""
} else {
    set bellow_wake "bellow"
    set flange_wake "flange"
    set cbandref_wake "cavbpm_cref"
    set cband_wake "cavbpm_cband"
    set sband_wake "cavbpm_sband"
    set aperture_2024_wake "aperture_2024_wake"
    set aperture_2420_wake "aperture_2420_wake"
    set resistive_wall_wake_3cm "resistive_wall_3cm"

    if {$use_wakefield_shortbunch} {
	# cband reference cavity
	SplineCreate "wake_cavbpm_cref" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfCrefWakeTBL03.dat"
	ShortRangeWake "cavbpm_cref" -wx "wake_cavbpm_cref" -wy "wake_cavbpm_cref" -type 0 -wake_bunch_length $wakefield_shortbunch
	# cband cavity bpm
	SplineCreate "wake_cavbpm_cband" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfCbpmWakeTBL03.dat"
	ShortRangeWake "cavbpm_cband" -wx "wake_cavbpm_cband" -wy "wake_cavbpm_cband" -type 0 -wake_bunch_length $wakefield_shortbunch
	# sband cavity bpm
	SplineCreate "wake_cavbpm_sband" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfSbpmWakeTBL03.dat"
	ShortRangeWake "cavbpm_sband" -wx "wake_cavbpm_sband" -wy "wake_cavbpm_sband" -type 0 -wake_bunch_length $wakefield_shortbunch
	# bellow
	SplineCreate "wake_bellow" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfBellowWakeReal03mm.dat"
	ShortRangeWake "bellow" -wx "wake_bellow" -wy "wake_bellow" -type 0 -wake_bunch_length $wakefield_shortbunch

	puts "WARNING: No Flange or Aperture Wake description for short bunch description"
	set flange_wake ""
	set aperture_2024_wake ""
	set aperture_2420_wake ""
    } else {
	# wakefield for CavityBPM
	set bunch_length_mm [expr round($match(sigma_z)/1000.)]
	# cband reference cavity
	if {$bunch_length_mm < 10} {
	    SplineCreate "wake_cavbpm_cref" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfCrefWy0${bunch_length_mm}mm.dat"
	} else {
	    SplineCreate "wake_cavbpm_cref" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfCrefWy${bunch_length_mm}mm.dat"
	}
	SplineCreate "wake_cavbpm_cref_long" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfCrefWLy0${bunch_length_mm}mm.dat"
	ShortRangeWake "cavbpm_cref" -wx "wake_cavbpm_cref" -wy "wake_cavbpm_cref" -wz "wake_cavbpm_cref_long" -type 1
	#ShortRangeWake "cavbpm_cref" -wx "wake_cavbpm_cref" -wy "wake_cavbpm_cref" -type 1
	# only 7 mm available so far with C-band and S-band
	set bunch_length_mm 7
	# cband cavity bpm
	SplineCreate "wake_cavbpm_cband" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfCbpmrefWy0${bunch_length_mm}mm.dat"
	#ShortRangeWake "cavbpm_cband" -wx "wake_cavbpm_cband" -wy "wake_cavbpm_cband" -type 1
	ShortRangeWake "cavbpm_cband" -wx "wake_cavbpm_cband" -wy "wake_cavbpm_cband" -wz "wake_cavbpm_cref_long" -type 1

	SplineCreate "wake_resistive_wall_z" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/wake_resistive_wall_ILC_z.dat"
	SplineCreate "wake_resistive_wall_3cm" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/wake_resistive_wall_ILC_3cm.dat"
	ShortRangeWake "resistive_wall_3cm" -wx "wake_resistive_wall_3cm" -wy "wake_resistive_wall_3cm" -wz "wake_resistive_wall_z" -type 2

	# sband cavity bpm
	SplineCreate "wake_cavbpm_sband" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfSbpmrefWy0${bunch_length_mm}mm.dat"
	ShortRangeWake "cavbpm_sband" -wx "wake_cavbpm_sband" -wy "wake_cavbpm_sband" -type 1
	# aperture steps 20-24 mm and 24-20 mm
	SplineCreate "wake_aperture_2024" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfStepWakeTBL${bunch_length_mm}.dat"
	ShortRangeWake "aperture_2024_wake" -wx "wake_aperture_2024" -wy "wake_aperture_2024" -type 1
	SplineCreate "wake_aperture_2420" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfStepWakeRevTBL${bunch_length_mm}.dat"
	ShortRangeWake "aperture_2420_wake" -wx "wake_aperture_2420" -wy "wake_aperture_2420" -type 1

	# bellow
	# bellow tilted
	if {$wakeFieldSetupTiltedBellow} {
	    SplineCreate "wake_tiltedbellow_-1mm" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfBellowWakeTiltTOff1.dat" -sign 0
	    SplineCreate "wake_tiltedbellow_-2mm" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfBellowWakeTiltTOff2.dat" -sign 0
	    SplineCreate "wake_tiltedbellow_-3mm" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfBellowWakeTiltTOff3.dat" -sign 0
	    SplineCreate "wake_tiltedbellow_-4mm" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfBellowWakeTiltTOff4.dat" -sign 0
	    SplineCreate "wake_tiltedbellow_-5mm" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfBellowWakeTiltTOff5.dat" -sign 0
	    SplineCreate "wake_tiltedbellow_1mm" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfBellowWakeTiltTOff1.dat" -sign 1
	    SplineCreate "wake_tiltedbellow_2mm" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfBellowWakeTiltTOff2.dat" -sign 1
	    SplineCreate "wake_tiltedbellow_3mm" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfBellowWakeTiltTOff3.dat" -sign 1
	    SplineCreate "wake_tiltedbellow_4mm" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfBellowWakeTiltTOff4.dat" -sign 1
	    SplineCreate "wake_tiltedbellow_5mm" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfBellowWakeTiltTOff5.dat" -sign 1

	    SplineCreate "zero_wake" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/zeroWake.dat"
	    
	    ShortRangeWake "bellow_tilted-1mm" -wx "wake_tiltedbellow_-1mm" -wy "wake_tiltedbellow_-1mm" -type 1
	    ShortRangeWake "bellow_tilted-2mm" -wx "wake_tiltedbellow_-2mm" -wy "wake_tiltedbellow_-2mm" -type 1
	    ShortRangeWake "bellow_tilted-3mm" -wx "wake_tiltedbellow_-3mm" -wy "wake_tiltedbellow_-3mm" -type 1
	    ShortRangeWake "bellow_tilted-4mm" -wx "wake_tiltedbellow_-4mm" -wy "wake_tiltedbellow_-4mm" -type 1
	    ShortRangeWake "bellow_tilted-5mm" -wx "wake_tiltedbellow_-5mm" -wy "wake_tiltedbellow_-5mm" -type 1

	    ShortRangeWake "bellow_tilted0mm" -wx "zero_wake" -wy "zero_wake" -type 1
	    ShortRangeWake "bellow" -wx "zero_wake" -wy "zero_wake" -type 1

	    ShortRangeWake "bellow_tilted1mm" -wx "wake_tiltedbellow_1mm" -wy "wake_tiltedbellow_1mm" -type 1
	    ShortRangeWake "bellow_tilted2mm" -wx "wake_tiltedbellow_2mm" -wy "wake_tiltedbellow_2mm" -type 1
	    ShortRangeWake "bellow_tilted3mm" -wx "wake_tiltedbellow_3mm" -wy "wake_tiltedbellow_3mm" -type 1
	    ShortRangeWake "bellow_tilted4mm" -wx "wake_tiltedbellow_4mm" -wy "wake_tiltedbellow_4mm" -type 1
	    ShortRangeWake "bellow_tilted5mm" -wx "wake_tiltedbellow_5mm" -wy "wake_tiltedbellow_5mm" -type 1
	} else {

        #### bellows in the machine ####
	#    SplineCreate "wake_bellow" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfBellowWakeReal${bunch_length_mm}mm.dat"
	#    ShortRangeWake "bellow" -wx "wake_bellow" -wy "wake_bellow" -type 1

	#### bellow on the mover ###
	    SplineCreate "wake_bellow" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfBellowLongWakeTBL${bunch_length_mm}.dat"
	    ShortRangeWake "bellow" -wx "wake_bellow" -wy "wake_bellow" -type 1

	    #SplineCreate "zero_wake" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/zeroWake.dat"
	    #ShortRangeWake "bellow" -wx "zero_wake" -wy "zero_wake" -type 1
	}
	# flange
	SplineCreate "wake_flange" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfFlangeWakeTBL${bunch_length_mm}mm.dat"
	ShortRangeWake "flange" -wx "wake_flange" -wy "wake_flange" -type 1

	# full setup , 2 ref cavity
	SplineCreate "wake_fullsetup" -file "${script_dir}/Frameworks/ATF2_studies/data/WakeData/atfWfSetupWakeT01.dat"
	ShortRangeWake "full2refsetup" -wx "wake_fullsetup" -wy "wake_fullsetup" -type 1
    }
}
