% Implements the function rls_full.m
% 
% Function: It implements one step of the recursive least square algorithm (RLS),
% for the full orbit response matrix.
%
% Juergen Pfingstner
% 13. Januar 2012

function [R_new, P_new] = rls_full(bpm, phi, R_old, P_old, lambda)
% Usage of [R_new, P_new] = rls_full(bpm, phi, R_old, P_old, lambda)
% 
% Variables:
%
%      Input:
%            bpm:     Measurement values of the BPMs
%            u:       Excitation values
%            R_old:   Last estimated R
%            P_old:   Last estimated covarinace matrix of the parameters
%            lambda:   Forgetting factor
%
%      Output:
%            R_new:   Actual estimated R
%            P_new:   Actual estimated covariance matrix of the parameters
%

[m,n] = size(R_old);

% Calculate the error of the last measurement
error = bpm - R_old*phi;

% Calculate K
b = lambda + phi' * P_old * phi;
b = 1 / b;
K = P_old * phi;
K = K .* b;

% Calculate the updated matrix P_new
P_new = eye(n) - K * phi';
P_new = P_new * P_old;
P_new = P_new ./ lambda;

% Calculate the new estimated values
R_new = R_old + error * K';

end


