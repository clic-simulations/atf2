######################################
# Section to export/import variables #
######################################

# make output directory:
exec mkdir -p $dir_name
# make link to output directory (for easy analysis), but only if not a directory or regular file
# proc to test on file
proc is_broken {linkname} {return [expr { ! [file exists [file readlink $linkname]] }]}
set rc [catch {is_broken output} result] 
if {$rc == 0 && $result} { 
    #puts "output is a broken link" 
    exec rm output
    exec ln -s $dir_name output
} elseif {$rc == 0} { 
    #puts "link output is OK" 
    exec rm output
    exec ln -s $dir_name output
} elseif {![file exists output]} {
    #puts "output is not a link: $result"
    exec ln -s $dir_name output
}

unset rc
unset result

cd $dir_name

set data_dir $script_dir/data/

# export whole tcl environment into Octave:
export_env_2_octave ""

Octave {
  addpath(strcat(script_dir,"/scripts"))
}

# print time:
set timestamp [clock format [clock seconds]]
puts "$timestamp"

# Save PLACET version:
set PLACET_version [Version]

# save current settings:
save_state settings.dat

# fix current environment
const_env
