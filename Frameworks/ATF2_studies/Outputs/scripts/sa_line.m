% Implements the function rls_full.m
% 
% Function: It implements one step of the recursive least square algorithm (RLS),
% for the full orbit response matrix.
%
% Juergen Pfingstner
% 13. Januar 2012

function [r_new, p_new] = sa_line(bpm, phi, r_old, p_old)
% Usage of [r_new, p_new] = sa_line(bpm, phi, r_old, p_old)
% 
% Variables:
%
%      Input:
%            bpm:     Measurement values of the BPMs
%            phi:     Excitation values
%            r_old:   Last estimated r
%            p_old:   Last estimated covarinace matrix of the parameters
%
%      Output:
%            r_new:   Actual estimated r
%            p_new:   Actual estimated covariance matrix of the parameters
%

% Calculate the error of the last measurement

error = bpm - r_old'*phi;

p_new = 1/p_old + phi'*phi;
p_new = 1/p_new;

% Calculate the new estimated values
r_new = r_old + p_new*error*phi;

end


