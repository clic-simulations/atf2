% Script to make and track the beam, store the result and read the BPMs 
%

%%%%%%%%%%%%%
% Make beam %
%%%%%%%%%%%%%

% determine initial energy
if (use_energy_jitter==1)
    Tcl_Eval("set de_initial [expr 1+$energy_jitter*[gauss]]")
    Tcl_Eval("puts \"initial beam energy is [expr $de_initial * $e_initial] GeV\" ")

else
    Tcl_Eval("set de_initial 1")
end

if (dynamic_beam == 1)
    Tcl_Eval("BeamDelete -beam $beam_name")

    printf("beam charge %f\n", charge);

    if (particle_beam)
        Tcl_Eval("make_beam_many_energy $beam_name $n_slice $n $de_initial");
    else
        Tcl_Eval("make_beam_slice $beam_name $n_slice 100");
    end
end
    
% add again dispersion
if (use_dispersion==1)
    Tcl_Eval("BeamAddDispersion -beam $beam_name -eref $e0 -dx $dispersion_x -dxprime $dispersion_xp -dy $dispersion_y -dyprime $dispersion_yp")
end

% add again xy coupling
if (use_xycoupling==1)
    Tcl_Eval("BeamAddOffset -beam $beam_name -rotate $xycoupling_angle")
end

% if bunch_index not yet defined, define to 0
if (exist("bunch_index", "var") == 0)
   bunch_index = 1;
end
% bunch feedback + bunch effects 
source(strcat(script_dir,"/scripts/bunch_effects.m"))

Tcl_SetVar('time_step_index',time_step_index);
Tcl_SetVar('bunch_index',bunch_index);
Tcl_SetVar('name', beam_name);

%%%%%%%%%%%%%%%%%%
% Track the beam %
%%%%%%%%%%%%%%%%%%

if (save_tracking)
    [Emitt,Beam]=placet_test_no_correction(beamline_name, beam_name, ...
                                           "None", tracking_output_string );
    file_temp = strcat(substr(tracking_file_name,1,-4),num2str(time_step_index),".dat");
    save("-ascii",file_temp,"Emitt");
else
    [Emitt,Beam]=placet_test_no_correction(beamline_name, beam_name, ...
                                           "None");
end


%if (save_beam_ip)
%Tcl_Eval("read_array electron_beampar_ip${time_step_index}_$bunch_index")
%Tcl_Eval("print_beam_params electron_beampar_ip${time_step_index}_$bunch_index")
%end

%%%%%%%%%%%%%%%%%
% Read BPM data %
%%%%%%%%%%%%%%%%%

if (save_bpm_readings_nonoise)
    bpm_readings_true = placet_get_bpm_readings(beamline_name, bpm_index, true);
end

if (bpm_noise == 1)
    bpm_readings = placet_get_bpm_readings(beamline_name, bpm_index, false);
else
    if (save_bpm_readings_nonoise)
        bpm_readings = bpm_readings_true;
    else
        bpm_readings = placet_get_bpm_readings(beamline_name, bpm_index, true);
    end
end

if (bpm_direction == 1)
    bpm_readingsxp = placet_element_get_attribute(beamline_name,bpm_index,"reading_xp");
    bpm_readingsyp = placet_element_get_attribute(beamline_name,bpm_index,"reading_yp");
end

magnetPosition_readings(:,1) = placet_element_get_attribute(beamline_name, magnet_index, "x");
magnetPosition_readings(:,2) = placet_element_get_attribute(beamline_name, magnet_index, "y");

if (use_font || IPFeedback || MFB2FFFeedback)
    fontk1_strengthx = placet_element_get_attribute(beamline_name,fontk1_index,"strength_x");
    fontk1_strengthy = placet_element_get_attribute(beamline_name,fontk1_index,"strength_y");
    fontk2_strengthx = placet_element_get_attribute(beamline_name,fontk2_index,"strength_x");
    fontk2_strengthy = placet_element_get_attribute(beamline_name,fontk2_index,"strength_y");
    ipkicker_strengthx = placet_element_get_attribute(beamline_name,ipkicker_index,"strength_x");
    ipkicker_strengthy = placet_element_get_attribute(beamline_name,ipkicker_index,"strength_y");

    fontp1_readingx = placet_element_get_attribute(beamline_name,fontp1_index,"reading_x");
    fontp1_readingy = placet_element_get_attribute(beamline_name,fontp1_index,"reading_y");
    fontp2_readingx = placet_element_get_attribute(beamline_name,fontp2_index,"reading_x");
    fontp2_readingy = placet_element_get_attribute(beamline_name,fontp2_index,"reading_y");
    fontp3_readingx = placet_element_get_attribute(beamline_name,fontp3_index,"reading_x");
    fontp3_readingy = placet_element_get_attribute(beamline_name,fontp3_index,"reading_y");
    mfb2ff_readingx = placet_element_get_attribute(beamline_name,mfb2ff_index,"reading_x");
    mfb2ff_readingy = placet_element_get_attribute(beamline_name,mfb2ff_index,"reading_y");
    preip_readingx = placet_element_get_attribute(beamline_name,preip_index,"reading_x");
    preip_readingy = placet_element_get_attribute(beamline_name,preip_index,"reading_y");
    ipa_readingx = placet_element_get_attribute(beamline_name,ipa_index,"reading_x");
    ipa_readingy = placet_element_get_attribute(beamline_name,ipa_index,"reading_y");
    ipb_readingx = placet_element_get_attribute(beamline_name,ipb_index,"reading_x");
    ipb_readingy = placet_element_get_attribute(beamline_name,ipb_index,"reading_y");
    if (beamline_version < 52)
        postip_readingx = placet_element_get_attribute(beamline_name,postip_index,"reading_x");
        postip_readingy = placet_element_get_attribute(beamline_name,postip_index,"reading_y");
    else
        ipc_readingx = placet_element_get_attribute(beamline_name,ipc_index,"reading_x");
        ipc_readingy = placet_element_get_attribute(beamline_name,ipc_index,"reading_y");
    end
    
end

%% Debug
%figure(11);
%plot(bpm_readings(:,1), 'b');
%hold on;
%grid on;

%figure(12);
%plot(bpm_readings(:,2), 'r');
%hold on;
%grid on;

%figure(100);
%keyboard;
%% dEBUG
