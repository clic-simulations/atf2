if (use_bunch_offset)
#    if (bunch_index!=1)
    if (bunch_index!=1)
        Tcl_Eval("BeamAddOffset -beam $beam_name -x $bunch_offset_x -y $bunch_offset_y -angle_x $bunch_offset_xp -angle_y $bunch_offset_yp")
    end
end

if (use_bunch_jitter)
  % no bunch jitter added for first bunch. jitter is always added on top of position of first bunch.
  if (bunch_index!=1)
      bunch_jitter_add_x = bunch_jitter_x*normrnd(0,1);
      bunch_jitter_add_y = bunch_jitter_y*normrnd(0,1);
      bunch_jitter_add_xp = bunch_jitter_xp*normrnd(0,1);
      bunch_jitter_add_yp = bunch_jitter_yp*normrnd(0,1);
      Tcl_SetVar('bunch_jitter_add_x',bunch_jitter_add_x);
      Tcl_SetVar('bunch_jitter_add_y',bunch_jitter_add_y);
      Tcl_SetVar('bunch_jitter_add_xp',bunch_jitter_add_xp);
      Tcl_SetVar('bunch_jitter_add_yp',bunch_jitter_add_yp);
      Tcl_Eval("BeamAddOffset -beam $beam_name -x $bunch_jitter_add_x -y $bunch_jitter_add_y -angle_x $bunch_jitter_add_xp -angle_y $bunch_jitter_add_yp")
  end
end

if (use_font && FONTFeedback)
    %disp('FONT Feedback')
    fontReading_x = placet_element_get_attribute(beamline_name,bpmfont_index,"reading_x");
    fontReading_y = placet_element_get_attribute(beamline_name,bpmfont_index,"reading_y");
    if (bunch_index==1)
        % first bunch kickers off
        placet_element_set_attribute(beamline_name,fontk1_index,"strength_x",0.0);
        placet_element_set_attribute(beamline_name,fontk1_index,"strength_y",0.0);
        placet_element_set_attribute(beamline_name,fontk2_index,"strength_x",0.0);
        placet_element_set_attribute(beamline_name,fontk2_index,"strength_y",0.0);
    else
        if (FONTFeedbackP1)
            % feedback algorithm, simple 1-1:
            if (FONTFeedbackX)
                fontk1_strengthx = -fontReading_x(1)*FONTP1slope;
                placet_element_vary_attribute(beamline_name,fontk1_index,"strength_x",fontk1_strengthx*(1+fontk1_strengthx_sigma*normrnd(0,1,1,length(fontk1_index))));
            end
            if (FONTFeedbackY)
                fontk1_strengthy = -fontReading_y(1)*FONTP1slope;
                placet_element_vary_attribute(beamline_name,fontk1_index,"strength_y",fontk1_strengthy*(1+fontk1_strengthy_sigma*normrnd(0,1,1,length(fontk1_index))));
            end
        end
        if (FONTFeedbackP2)
            if (FONTFeedbackX)
                fontk2_strengthx = -fontReading_x(2)*FONTP2slope;
                placet_element_vary_attribute(beamline_name,fontk2_index,"strength_x",fontk2_strengthx*(1+fontk2_strengthx_sigma*normrnd(0,1,1,length(fontk2_index))));
            end
            if (FONTFeedbackY)
                fontk2_strengthy = -fontReading_y(2)*FONTP2slope;
                placet_element_vary_attribute(beamline_name,fontk2_index,"strength_y",fontk2_strengthy*(1+fontk2_strengthy_sigma*normrnd(0,1,1,length(fontk2_index))));
            end
        end
    end
end

if (IPFeedback)
    IPA_x = placet_element_get_attribute(beamline_name,ipa_index,"reading_x");
    IPA_y = placet_element_get_attribute(beamline_name,ipa_index,"reading_y");
    IPB_x = placet_element_get_attribute(beamline_name,ipb_index,"reading_x");
    IPB_y = placet_element_get_attribute(beamline_name,ipb_index,"reading_y");
    if (bunch_index == 1)
        % first bunch: kickers off
        placet_element_set_attribute(beamline_name,ipkicker_index,"strength_x",0.0);
        placet_element_set_attribute(beamline_name,ipkicker_index,"strength_y",0.0);
    else
        if (IPFeedbackIPA)
            % feedback algorithm (to be added, use IPA/IPB):
            if (IPFeedbackX)
                ipkicker_strengthx = -(IPA_x-IPFeedbackXObjective)*IPAslope;
            end
            if (IPFeedbackY)
                ipkicker_strengthy = -(IPA_y-IPFeedbackYObjective)*IPAslope;
            end
        elseif (IPFeedbackIPB)
            if (IPFeedbackX)
                ipkicker_strengthx = -(IPB_x-IPFeedbackXObjective)*IPBslope;
            end
            if (IPFeedbackY)
                ipkicker_strengthy = -(IPB_y-IPFeedbackYObjective)*IPBslope;
            end
        end
        if (IPFeedbackIPA || IPFeedbackIPB)
            if (IPFeedbackX)
                placet_element_vary_attribute(beamline_name,ipkicker_index,"strength_x",ipkicker_strengthx*(1+ipkicker_strengthx_sigma*normrnd(0,1)));
            end
            if (IPFeedbackY)
                placet_element_vary_attribute(beamline_name,ipkicker_index,"strength_y",ipkicker_strengthy*(1+ipkicker_strengthy_sigma*normrnd(0,1)));
            end
        end
    end
end

if (use_mfb2ffkicker && MFB2FFFeedback)
    % last reading
    MFB2FF_x = placet_element_get_attribute(beamline_name,mfb2ff_index,"reading_x");
    MFB2FF_y = placet_element_get_attribute(beamline_name,mfb2ff_index,"reading_y");
    if (bunch_index == 1)
        % first bunch: kickers off
        placet_element_set_attribute(beamline_name,mfb2ffkicker_index,"strength_x",0.0);
        placet_element_set_attribute(beamline_name,mfb2ffkicker_index,"strength_y",0.0);
    else
        if (MFB2FFFeedbackX)
            mfb2ffkicker_strengthx = -(MFB2FF_x)*MFB2FFslope;
            placet_element_vary_attribute(beamline_name,mfb2ffkicker_index,"strength_x",mfb2ff_strengthx*(1+mfb2ffkicker_strengthx_sigma*normrnd(0,1)));
        end
        if (MFB2FFFeedbackY)
            mfb2ffkicker_strengthy = -(MFB2FF_y)*MFB2FFslope;
            placet_element_vary_attribute(beamline_name,mfb2ffkicker_index,"strength_y",mfb2ffkicker_strengthy*(1+mfb2ffkicker_strengthy_sigma*normrnd(0,1)));
        end
    end
end
