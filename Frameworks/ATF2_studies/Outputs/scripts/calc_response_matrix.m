% Function to calculate the response matrix
%
% Juergen Pfingstner

function [R] = calc_response_matrix(line_name, beam_name, bpm_index, corr_index, step_size, direction)
%

corr_name = placet_element_get_attribute(line_name, corr_index, 'name');
R = zeros(length(bpm_index), length(corr_index));
placet_test_no_correction(line_name, beam_name, 'None');
B=placet_get_bpm_readings(line_name, bpm_index, true);
if(strcmp(direction, 'y'))
  % y direction
  b0 = B(:,2);
  corr0 = placet_element_get_attribute(line_name, corr_index, 'strength_y');
else
  % x direction
  b0 = B(:,1);
  corr0 = placet_element_get_attribute(line_name, corr_index, 'strength_x');
end

for i=1:length(corr_index)

  fprintf(1, "Corrector %d of %d: %s\n", i, length(corr_index), corr_name(i,:));

  if(strcmp(direction, 'y'))

    placet_element_set_attribute(line_name, corr_index(i), 'strength_y', corr0(i) + step_size);
    placet_test_no_correction(line_name, beam_name, 'None');
    placet_element_set_attribute(line_name, corr_index(i), 'strength_y', corr0(i));
    B=placet_get_bpm_readings(line_name, bpm_index, true);
    b1 = B(:,2);

  else
 
    placet_element_set_attribute(line_name, corr_index(i), 'strength_x', corr0(i) + step_size);
    placet_test_no_correction(line_name, beam_name, 'None');
    placet_element_set_attribute(line_name, corr_index(i), 'strength_x', corr0(i));
    B=placet_get_bpm_readings(line_name, bpm_index, true);
    b1 = B(:,1);
    
  end
  R(:,i)=(b1-b0);

  %% Debug
  %figure(1);
  %plot(R(:,i),'b');
  %%hold on;
  %%grid on;
  %figure(100);
  %keyboard;
  %% dEBUG
end

R = R ./ step_size; 

