% Implementation of the second part of the system identification,
% which is everything after the excitation for FACET
%
% Juergen Pfingstner
% 10th December 2012

    %%%%%%%%%%%%%%%%%%%%
    % Extract the data %
    %%%%%%%%%%%%%%%%%%%%

    excit_x = excit_x_data(time_step_index,:)';
    excit_y = excit_y_data(time_step_index,:)';
    bpm_readings_x = meas_x_data(time_step_index,:)';
    bpm_readings_y = meas_y_data(time_step_index,:)';

    %%%%%%%%%%%%%%%%%%%%%%%
    % Feed the algorithms %
    %%%%%%%%%%%%%%%%%%%%%%%
 
    if(rls_algo == 1)	
	
      % Full RLS at once

      [R_x_ident, P_x] = rls_full(bpm_readings_x, [excit_x; 1], R_x_ident, P_x, lambda_x);
      [R_y_ident, P_y] = rls_full(bpm_readings_y, [excit_y; 1], R_y_ident, P_y, lambda_y);
      R_x_ident = R_x_ident .* delete_x;
      R_y_ident = R_y_ident .* delete_y;	
    elseif(rls_algo == 2)

      % Simplified version of the full RLS at once

      [R_x_ident, P_x] = rls_mod(bpm_readings_x, [excit_x; 1], R_x_ident, P_x, lambda_x, delete_x);
      [R_y_ident, P_y] = rls_mod(bpm_readings_y, [excit_y; 1], R_y_ident, P_y, lambda_y, delete_y);
    elseif(rls_algo == 3)

      % RLS for every line individual

      for i=1:m

	%if(bpm_hcorr(i) < 0)
	%    nr_elem = n_x-1;
	%else
        %    nr_elem = bpm_hcorr(i);
	%end
	nr_elem = sum(delete_x(i,1:(end-1)));
	[ident_data_x(i).r, ident_data_x(i).P] = rls_line(bpm_readings_x(i), [excit_x(1:nr_elem); 1], ident_data_x(i).r, ident_data_x(i).P, ident_data_x(i).lambda);

	%if(bpm_vcorr(i) < 0)
	%    nr_elem = n_y-1;
	%else
        %    nr_elem = bpm_vcorr(i);
	%end
	nr_elem = sum(delete_y(i,1:(end-1)));
	[ident_data_y(i).r, ident_data_y(i).P] = rls_line(bpm_readings_y(i), [excit_y(1:nr_elem); 1], ident_data_y(i).r, ident_data_y(i).P, ident_data_y(i).lambda);
      end
    elseif(rls_algo == 4)

	%% LMS algorithm	

	[R_x_ident] = lms_full(bpm_readings_x, excit_x, R_x_ident, lambda_x, delete_x);
	[R_y_ident] = lms_full(bpm_readings_y, excit_y, R_y_ident, lambda_y, delete_y);
    elseif(rls_algo == 5)
	
	% SA algorithm (stochastic approximation)

	for i=1:m	
	  %if(bpm_hcorr(i) < 0)
	  %    nr_elem = n_x-1;
	  %else
          %    nr_elem = bpm_hcorr(i);
	  %end
	  nr_elem = sum(delete_x(i,1:(end-1)));
          [ident_data_x(i).r, ident_data_x(i).P] = sa_line( bpm_readings_x(i), [excit_x(1:nr_elem); 1], ident_data_x(i).r, ident_data_x(i).P);
 
	  %if(bpm_vcorr(i) < 0)
	  %    nr_elem = n_y-1;
	  %else
          %    nr_elem = bpm_vcorr(i);
	  %end
	  nr_elem = sum(delete_y(i,1:(end-1)));
	  [ident_data_y(i).r, ident_data_y(i).P] = sa_line(bpm_readings_y(i), [excit_y(1:nr_elem); 1], ident_data_y(i).r, ident_data_y(i).P);
	end

    elseif(rls_algo == 6)

	% RLS for each element individual

	% Determine the excitation position and the cycle number
	
	corr_excit_nr_x = mod(time_step_index, n_x); 
	cycle_nr_x = round((time_step_index-corr_excit_nr_x)/(n_x));
	corr_excit_nr_y = mod(time_step_index, n_y); 	
	cycle_nr_y = round((time_step_index-corr_excit_nr_y)/(n_y));

	% Update the P_inv and calculate the P for the local update
		
	if(corr_excit_nr_x == 0)
	    P_local_x = 1 / (cycle_nr_x+1+1/init_P_value);
	    K_x_local = P_local_x;

	    for i=1:m
	        param_x_local = D_x_ident(i,end);
		error_x_local = bpm_readings_x(i) - param_x_local; 
	        D_x_ident(i,end) = param_x_local + K_x_local*error_x_local;
	    end
	else
	    excit_x_local = excit_x(corr_excit_nr_x);
	    P_inv_diag_x(corr_excit_nr_x) = P_inv_diag_x(corr_excit_nr_x) + excit_x_local^2;
	    P_inv_col_x(corr_excit_nr_x) = P_inv_col_x(corr_excit_nr_x) + excit_x_local;
	
	    p_11 = P_inv_diag_x(corr_excit_nr_x);
	    p_12 = P_inv_col_x(corr_excit_nr_x);
	    p_22 = cycle_nr_x+1+1/init_P_value;

	    P_local_x = 1/(p_11*p_22 - p_12^2)*[p_22, -p_12; -p_12, p_11];
	    K_x_local = P_local_x*[excit_x_local; 1];

            start_index_bpm = 1+sum(delete_x(:,corr_excit_nr_x)==0);
	    for i = start_index_bpm:m
		param_x_local = [R_x_ident(i, corr_excit_nr_x); D_x_ident(i, corr_excit_nr_x)];
	        error_x_local = bpm_readings_x(i) - param_x_local'*[excit_x_local; 1]; 
	        param_x_local = param_x_local + K_x_local*error_x_local;
	        R_x_ident(i, corr_excit_nr_x) = param_x_local(1);
		D_x_ident(i, corr_excit_nr_x) = param_x_local(2);
	    end
        end

	if(corr_excit_nr_y == 0)
	    P_local_y = 1 / (cycle_nr_y+1+1/init_P_value);
	    K_y_local = P_local_y;

	    for i=1:m
	        param_y_local = D_y_ident(i,end);
		error_y_local = bpm_readings_y(i) - param_y_local; 
	        D_y_ident(i,end) = param_y_local + K_y_local*error_y_local;
	    end
	else
	    excit_y_local = excit_y(corr_excit_nr_y);
	    P_inv_diag_y(corr_excit_nr_y) = P_inv_diag_y(corr_excit_nr_y) + excit_y_local^2;
	    P_inv_col_y(corr_excit_nr_y) = P_inv_col_y(corr_excit_nr_y) + excit_y_local;
	
	    p_11 = P_inv_diag_y(corr_excit_nr_y);
	    p_12 = P_inv_col_y(corr_excit_nr_y);
	    p_22 = cycle_nr_y+1+1/init_P_value;

	    P_local_y = 1/(p_11*p_22 - p_12^2)*[p_22, -p_12; -p_12, p_11];
	    K_y_local = P_local_y*[excit_y_local; 1];

	    start_index_bpm = 1+sum(delete_y(:,corr_excit_nr_y)==0);
	    for i = start_index_bpm:m
	        param_y_local = [R_y_ident(i, corr_excit_nr_y); D_y_ident(i, corr_excit_nr_y)];
	        error_y_local = bpm_readings_y(i) - param_y_local'*[excit_y_local; 1]; 
	        param_y_local = param_y_local + K_y_local*error_y_local;
	        R_y_ident(i, corr_excit_nr_y) = param_y_local(1);
		D_y_ident(i, corr_excit_nr_y) = param_y_local(2);
	    end
        end

   elseif(rls_algo == 7)

	% Incremental covariance matrix

	% Loop through all rows
	for i=1:m

	    %%%%%%%%%
	    % For x %
	    %%%%%%%%%

	    %if(bpm_hcorr(i) < 0)
	    %    nr_elem = n_x-1;
	    %else
            %    nr_elem = bpm_hcorr(i);
	    %end
            nr_elem = sum(delete_x(i,1:(end-1)));
	    icm_data_x = [bpm_readings_x(i); excit_x(1:nr_elem)];

	    % Calculate the covariance matrix
	    if sum(excit_x(1:nr_elem) != 0)
	        % Increment only if there is information also in the Correctors' part of the vector x
	        ICM_x{i} = incremental_covariance_matrix(icm_data_x, ICM_x{i});
	    end	

	    % Calculate the elements of R
	    A = ICM_x{i}.covariance;
            maskA = eye(size(A));
            maskA(:,1) = 1;
            maskA(1,:) = 1;
            [V,L] = eig(ICM_x{i}.covariance .* maskA);
            [L,idx] = sort(-abs(diag(L)));
            V = V(:,idx);
            %Cy = Response.Cy(Response.Cy < Response.Bpms(i));
            %R_icm(i,1:length(Cy)) = -V(2:end,end)' / V(1,end);

	    %[V,L]=eig(ICM_x{i}.covariance);	
	    %[L,idx] = sort(-abs(diag(L)));
 	    %V = V(:,idx);

	    % Test for the first few runs
            if(V(1,end)==0)
                V(1,end)=1;
	    end
	    if(length(V)>1.5)
		R_x_ident(i,1:nr_elem) = -V(2:end, end)' / V(1,end);
            end            

	    %%%%%%%%%	
	    % For y %
	    %%%%%%%%%

	    %if(bpm_vcorr(i) < 0)
	    %    nr_elem = n_y-1;
	    %else
            %    nr_elem = bpm_vcorr(i);
	    %end
	    nr_elem = sum(delete_y(i,1:(end-1)));
	    icm_data_y = [bpm_readings_y(i); excit_y(1:nr_elem)];		

	    % Calculate the covariance matrix
	    if sum(excit_y(1:nr_elem) != 0)
	        % Increment only if there is information also in the Correctors' part of the vector x
	        ICM_y{i} = incremental_covariance_matrix(icm_data_y, ICM_y{i});
	    end

	    % Calculate the elements of R
	    A = ICM_y{i}.covariance;
            maskA = eye(size(A));
            maskA(:,1) = 1;
            maskA(1,:) = 1;
            [V,L] = eig(ICM_y{i}.covariance .* maskA);
            [L,idx] = sort(-abs(diag(L)));
            V = V(:,idx);
            %Cy = Response.Cy(Response.Cy < Response.Bpms(i));
            %R_icm(i,1:length(Cy)) = -V(2:end,end)' / V(1,end);

	    %[V,L]=eig(ICM_y{i}.covariance);	
	    %[L,idx] = sort(-abs(diag(L)));
 	    %V = V(:,idx);
         
	    % Test for the first few runs
            if(V(1,end)==0)
                V(1,end)=1;
	    end
	    if(length(V)>1.5)
	        R_y_ident(i,1:nr_elem) = -V(2:end, end)' / V(1,end);
            end

	end
   else
	% Not known
	fprintf(1, 'Unkown RLS type\n');
    end
