%%%%%%%%%%%%%
% Paramters %
%%%%%%%%%%%%%

%eval_dir = "/afs/cern.ch/../sysident_excit_y";

excit_x_file_name = sprintf("%s/excit_x.dat", eval_dir);
excit_y_file_name = sprintf("%s/excit_y.dat", eval_dir);
meas_x_file_name = sprintf("%s/meas_x.dat", eval_dir);
meas_y_file_name = sprintf("%s/meas_y.dat", eval_dir);
ident_results_file_name = sprintf("%s/ident_result_%s.dat", eval_dir, eval_type_string);

%rls_algo = 7;
%excit_type = 2;

disp_reconst_algo = 3;
init_P_value = 1e6;
detailed_eval = 1;
eval_interval = 1;
load_corr = 0;
%lambda_x = 0.1;
%lambda_y = 0.1;

R_x_file_name = "./data/R_x.dat";
R_y_file_name = "./data/R_y.dat";
disp_file_name = "./data/disp_orbit.dat";
delta_T = 1./3;
load_corr_name = "./data/excit_corr_1mum.dat";
load_amp_name = "./data/excit_amp_1mum.dat";

%%%%%%%%%%%%%%%%%%%%%%
% Load and init data %
%%%%%%%%%%%%%%%%%%%%%%

source "./scripts/init_sysident_offline.m"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Go through all time steps %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
for time_step_index = 1:nr_time_steps
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Apply ground motion and other disturbances %
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
        %tic;
	%fprintf(1, 'Time: %fs of %fs\n', time_step_index*delta_T, nr_time_steps*delta_T);
		
	%%%%%%%%%%%%%%%%%%%%%%%%%
	% System Identification %
	%%%%%%%%%%%%%%%%%%%%%%%%%
	
        % updating of the parameters (response matrix, etc.)
        source "./scripts/sysident_update_offline.m"
      
	%%%%%%%%%%%%%%%%%%%%%
	% Store the results %
	%%%%%%%%%%%%%%%%%%%%%
	
        % store results and end process
        source "./scripts/postprocess_time_step_offline.m"
        %toc
	
 end
 
 fclose(fid1);
 fclose(f_row_x);
 fclose(f_row_y); 
 fclose(f_col_x);
 fclose(f_col_y);

