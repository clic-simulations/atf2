# global variables
# directory structure
set script_dir [pwd]
source $script_dir/scripts/tcl_procs.tcl
source $script_dir/scripts/load_settings.tcl
set save_excit_meas 1
Octave {
    save_excit_meas = 1;
}

#setting up beam, beamline
source $script_dir/scripts/accelerator_setup.tcl
source $script_dir/scripts/postprocess_procs.tcl
source $script_dir/scripts/init_sysident.tcl

Octave {

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % Apply initial imperfections %
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
      source "$script_dir/scripts/machine_setup.m";

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % Caclulate response matrix %
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
      if(response_matrix_calc == 1)
          fprintf(1, "Calc R_x\n");
          R_x = calc_response_matrix(beamline_name, beam_name, bpm_index, corr_used_x_index, step_size_R_x, 'x');
          save("R_x.dat","R_x", "-ascii");
          fprintf(1, "Calc R_y\n");
          R_y = calc_response_matrix(beamline_name, beam_name, bpm_index, corr_used_y_index, step_size_R_y, 'y');
          save("R_y.dat","R_y", "-ascii");
          exit;
      end

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % Go through all time steps %
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
      for time_step_index = 1:nr_time_steps
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Apply ground motion and other disturbances %
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
        tic;
	fprintf(1, 'Time: %fs of %fs\n', time_step_index*delta_T, nr_time_steps*delta_T);
		
        %source "$script_dir/scripts/dynamic_setup.m";
	
	%%%%%%%%%%%%%%%%%%%%%%%%%
	% System Identification %
	%%%%%%%%%%%%%%%%%%%%%%%%%
	
        % system identification setup
        source "$script_dir/scripts/sysident_setup.m"

	%%%%%%%%%%%%
	% Tracking %
	%%%%%%%%%%%%

        source "$script_dir/scripts/tracking.m"

	%%%%%%%%%%%%%%%%%
	% System Update %
	%%%%%%%%%%%%%%%%%

        % updating of the parameters (response matrix, etc.)
        source "$script_dir/scripts/sysident_update.m"
      
	%%%%%%%%%%%%%%%%%%%%%
	% Store the results %
	%%%%%%%%%%%%%%%%%%%%%
	
        % store results and end process
        source "$script_dir/scripts/postprocess_time_step.m"
        toc
	
      end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Delete unused files for a clean sim environment %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

source "$script_dir/scripts/postprocess_sim.m"

}

