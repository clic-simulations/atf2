% Script to evaluate and store the results of the last time steps
%
% Juergen Pfingstner
% 27th of March 2012


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Eval identification result %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
    if((rls_algo == 3) || (rls_algo == 5))
	% Restore the orbit response matrix
	for i=1:m
            nr_elem = length(ident_data_x(i).r);

	    line_vector = [(ident_data_x(i).r(1:end-1))', zeros(1, n_x - nr_elem), ident_data_x(i).r(end)];
	    R_x_ident(i,:) = line_vector;
	end
	for i=1:m
       	    nr_elem = length(ident_data_y(i).r);

	    line_vector = [(ident_data_y(i).r(1:end-1))', zeros(1, n_y - nr_elem), ident_data_y(i).r(end)];
	    R_y_ident(i,:) = line_vector;
	end
    end

    if(rls_algo == 6)

	% The dispersion has to be handled

	disp_x_ident = zeros(m,1);
	disp_y_ident = zeros(m,1);
	if(disp_reconst_algo == 1)
	    disp_x_ident = D_x_ident(:,end);
	    disp_y_ident = D_y_ident(:,end);
	elseif(disp_reconst_algo == 2)
	    for i=1:m
	        %if(bpm_hcorr(i)<0)
	        %    disp_x_ident(i) = mean(D_x_ident(i,1:end-1));	
	        %else
	        %    disp_x_ident(i) = mean(D_x_ident(i,1:bpm_hcorr(i)));
	        %end
	        %if(bpm_vcorr(i)<0)
	        %    disp_y_ident(i) = mean(D_y_ident(i,1:end-1));	
	        %else
	        %    disp_y_ident(i) = mean(D_y_ident(i,1:bpm_vcorr(i)));
	        %end
	        nr_elem = sum(delete_x(i,1:end-1));
	        disp_x_ident(i) = mean(D_x_ident(i,1:nr_elem));
	        nr_elem = sum(delete_y(i,1:end-1));
	        disp_y_ident(i) = mean(D_y_ident(i,1:nr_elem));
	    end
	else
	    for i=1:m
	        %if(bpm_hcorr(i)<0)
	        %    disp_x_ident(i) = mean(D_x_ident(i,:));	
	        %else
	        %    disp_x_ident(i) = mean([D_x_ident(i,1:bpm_hcorr(i)), D_x_ident(i,end)]);
	        %end
	        %if(bpm_vcorr(i)<0)
	        %    disp_y_ident(i) = mean(D_y_ident(i,:));	
	        %else
	        %    disp_y_ident(i) = mean([D_y_ident(i,1:bpm_vcorr(i)), D_y_ident(i,end)]);
	        %end
	        nr_elem = sum(delete_x(i,1:end-1));
	        disp_x_ident(i) = mean([D_x_ident(i,1:nr_elem), D_x_ident(i,end)]);
	        nr_elem = sum(delete_y(i,1:end-1));
	        disp_y_ident(i) = mean([D_y_ident(i,1:nr_elem), D_y_ident(i,end)]);
	    end
	end

	R_x_ident(:,end) = disp_x_ident;
	R_y_ident(:,end) = disp_y_ident;

    end
    
    error1_ident_x = norm(R_x_ident-R_x, 'fro')./norm_R_x;
    error1_ident_y = norm(R_y_ident-R_y, 'fro')./norm_R_y;

    error2_ident_x = norm(R_x_ident(:,1:end-1)-R_x(:,1:end-1), 'fro')./norm2_R_x;
    error2_ident_y = norm(R_y_ident(:,1:end-1)-R_y(:,1:end-1), 'fro')./norm2_R_y;

    if(rls_algo == 3)
	P_x = ident_data_x(end).P;
        P_y = ident_data_y(end).P;
    end
    norm_P_x = norm(P_x, 'fro');
    norm_P_y = norm(P_y, 'fro');

%%%%%%%%%%%%%%%%%%%%%
% Store the results %
%%%%%%%%%%%%%%%%%%%%%

fprintf(fid1, '%g   %g   %g   %g   %g   %g   %g   ', time_step_index.*delta_T, error1_ident_x, error1_ident_y, norm_P_x, norm_P_y, error2_ident_x, error2_ident_y);
fprintf(fid1, '\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Eval the ident results in detail %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(detailed_eval == 1)
    
    for i = 1:eval_interval:n_x
	error_local = norm(R_x_ident(:,i)-R_x(:,i), 'fro')./norm(R_x(:,i), 'fro');
	fprintf(f_col_x, '%g   ', error_local);
    end
    error_local = norm(R_x_ident(:,end)-R_x(:,end), 'fro')./norm(R_x(:,end), 'fro');
    fprintf(f_col_x, '%g\n', error_local);

    for i = 1:eval_interval:n_y
	error_local = norm(R_y_ident(:,i)-R_y(:,i), 'fro')./norm(R_y(:,i), 'fro');
	fprintf(f_col_y, '%g   ', error_local);
    end
    error_local = norm(R_y_ident(:,end)-R_y(:,end), 'fro')./norm(R_y(:,end), 'fro');
    fprintf(f_col_y, '%g\n', error_local);

    for i = 1:eval_interval:m
	error_local = norm(R_x_ident(i,:)-R_x(i,:), 'fro')./norm(R_x(i,:), 'fro');
	fprintf(f_row_x, '%g   ', error_local);
    end
    error_local = norm(R_x_ident(end,:)-R_x(end,:), 'fro')./norm(R_x(end,:), 'fro');
    fprintf(f_row_x, '%g\n', error_local);

    for i = 1:eval_interval:m
	error_local = norm(R_y_ident(i,:)-R_y(i,:), 'fro')./norm(R_y(i,:), 'fro');
	fprintf(f_row_y, '%g   ', error_local);
    end
    error_local = norm(R_y_ident(end,:)-R_y(end,:), 'fro')./norm(R_y(end,:), 'fro');
    fprintf(f_row_y, '%g\n', error_local);
end

