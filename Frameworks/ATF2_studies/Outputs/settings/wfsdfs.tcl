set particle_beam 1
set dynamic_beam 0

set use_misalignment 1
set alignment_qp 1
set alignment_bpm 1
set alignment_drift 1
set alignment_sigma 100 ;#[um]
#set alignment_sigma [lindex $argv 1]

set use_quad_strength_error 1
set quad_strength_error 0.01

set seed_nr 2
#set seed_nr [lindex $argv 1]

set use_sextupole 1
set use_multipole_components 1

set add_header 1

set use_wakefield 1
set use_wakefield_shortbunch 1

## wakes in addition to BPMs
set use_otr_wakes 0
set use_sbend_wakes 0
set use_corrector_wakes 0

set response_matrix_calc 0
set response_matrix_load 1
#####################
# RESPONSE MATRICES #
#####################
set beamline_name "ATF2_v5.2-MadX"
if {$use_sextupole} {
    #set response_matrix_file "../test/Response.dat"
    set response_matrix_file "../ResponseMatrices/Response.dat"
    #set response_matrix_file "../ResponseMatrices/Response_DriftasCREF_seed1.dat"
} else {
    #set response_matrix_file "../ResponseMatrices/ResponseSextOff.dat"
    #set response_matrix_file "../ResponseMatrices/ResponseSextOffWake100.dat"
    #set response_matrix_file "../ResponseMatrices/ResponseSextOffWake2Charge0.8-0.5.dat"
    #set response_matrix_file "../ResponseMatrices/ResponseSextOffWake2.dat"
    set response_matrix_file "../ResponseMatrices/ResponseSextOffNov2014.dat"
}

# STEERING
set use_steering 1
set steering_select_bpms 0

set steering_beta 5

set nr_time_steps 15 ;# 3 one-to-one + 3*3 DFS-WFS

set use_one_to_one_steering 1
set one_to_one_steering_iter 3
set one_to_one_steering_weight 5

set charge 0.8e10

set use_wfs 1
set wfs_dcharge [expr 0.5/0.8]

#set cband [lindex $argv 1]

# theoretical weight
#set wfs_weight [expr round(sqrt(($stripline * $stripline + $alignment_sigma * $alignment_sigma) / (2 * $stripline * $stripline)))]
set wfs_weight [expr round(sqrt(($cband * $cband + $alignment_sigma * $alignment_sigma) / (2 * $cband * $cband)))]
#set wfs_weight [lindex $argv 2]
#set wfs_weight 26 for 10um
#set wfs_weight 78
set wfs_weight_x $wfs_weight
set wfs_weight_y $wfs_weight

set use_dfs 1
set dfs_wfs_iter 3
set dfs_dE 0.995

# theoretical weight
#set dfs_weight [expr round(sqrt(($stripline * $stripline + $alignment_sigma * $alignment_sigma) / (2 * $stripline * $stripline)))]
set dfs_weight [expr round(sqrt(($cband * $cband + $alignment_sigma * $alignment_sigma) / (2 * $cband * $cband)))]
#set dfs_weight [lindex $argv 2]
set dfs_weight_x $dfs_weight
set dfs_weight_y $dfs_weight

puts "weight: $dfs_weight"

set bpm_noise 1

set use_bpm_charge_dependence 1

set save_bpm_readings_nonoise 1

set multipulse_nr 1

#set dir_name "WFS/WFS_ResponseSextOffBPMOff_100Wake_NoOneToOne"
set dir_name "DFSWFSNov2014/DFSWFS_align${alignment_sigma}_wakefield${use_wakefield}_charge${charge}_dE${dfs_dE}_weight${dfs_weight}_dcharge${wfs_dcharge}_weight${wfs_weight}_beta${steering_beta}_dynamic${dynamic_beam}_sext${use_sextupole}_bpm${bpm_noise}_select${steering_select_bpms}_cbpm_res${cband}_bpm_charge_dep${use_bpm_charge_dependence}_multoff${use_multipole_components}_wakeotr${use_otr_wakes}_wakesbend${use_sbend_wakes}_wakecorr${use_corrector_wakes}_seed${seed_nr}"

set dir_name "test"

# For new lattice
set save_beam_ip 0
set save_beam_ff_start 0
set save_beam_mfb2ff 0
