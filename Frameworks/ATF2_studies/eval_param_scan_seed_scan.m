% Script to combine the results of simulations with different seeds. This 
% is just the starter script for a universally usable function. Only the 
% parameters have to be changed to use it for any simulation of that type.
%
% Juergen Pfingstner
% 22th of April 2013

%addpath('/Users/jpfingst/Work/Matlab/Data_analysis/');

%%%%%%%%%%%%%
% Parameter %
%%%%%%%%%%%%%

random_or_orthogonal = 'random';
x_or_y = 'y';
rls_algo = 3;
lambda_x = 1;
lambda_y = 1;


start_seed = 1;
stop_seed = 10;
param_values = [0.01 0.02 0.05 0.1 0.2 0.5 1 2 5 10];
param_values_string = {'0.01' '0.02' '0.05' '0.1' '0.2' '0.5' '1' '2' '5' '10'};
%exception_param = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
basic_path = '/Users/jpfingst/Work/sim_results/ATF2/sysident_excit_data';
for j = start_seed:stop_seed
    for i = 1 : length(param_values)
    
        eval_dir = sprintf('%s/%s_%s/seed_%d_%s', basic_path, random_or_orthogonal, x_or_y, j, param_values_string{i});
        eval_type_string = sprintf('%d_%d_%d', rls_algo, lambda_x, lambda_y);
   
        % check if directory exists
        if(exist(eval_dir, 'dir'))
            eval_dir
            %eval_type_string
            run_atf2_sysident_offline
        else
            fprintf('Shit, there is no directory like that: %s\n', eval_dir);
        end
    end
end
fprintf(1,'\n');

