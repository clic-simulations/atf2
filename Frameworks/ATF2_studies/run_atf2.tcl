# global variables
# directory structure
set script_dir [pwd]
set lattice_dir $script_dir/../../Lattices/ATF2
set script_common_dir $script_dir/../../Common
source $script_common_dir/scripts/tcl_procs.tcl
source $script_dir/scripts/load_settings.tcl

#setting up beam, beamline
source $script_dir/scripts/accelerator_setup.tcl

source $script_dir/scripts/postprocess_procs.tcl



Octave {

#for ii=1:5

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Apply initial imperfections %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
    source "$script_dir/scripts/machine_setup.m";


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Go through all time steps %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    for time_step_index = 1:nr_time_steps
        tic;
        time = delta_T*time_step_index;
        fprintf(1, 'Pulse %d, Time: %fs of %fs\n', time_step_index, time, nr_time_steps*delta_T);

	%%%%%%%%%%%%%%%%%%%%%%
	% Centering the BEAM %
	%%%%%%%%%%%%%%%%%%%%%%

#    B0 = placet_get_beam("electron");
#    B0(:,2) -= mean(B0(:,2));
#    B0(:,3) -= mean(B0(:,3));
#    B0(:,5) -= mean(B0(:,5));
#    B0(:,6) -= mean(B0(:,6));
#    placet_set_beam("electron", B0);

	%%%%%%%%%%%%%%%%%%%%%%
	%   Adding offset    %
	%%%%%%%%%%%%%%%%%%%%%%


#   B0(:,3) = B0(:,3)+5.8827;
#   B0(:,6) = B0(:,6)+20.0381; 
#   placet_set_beam("electron", B0);

#    Blist=placet_get_number_list("ATF2_v5.2-MadX", "bpm");
#    placet_element_set_attribute("ATF2_v5.2-MadX", Blist, "resolution", 0);
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Apply ground motion and other disturbances %
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


	%%%%%%%%%%%%%%%%%%%%%%%%
	%     Misalignment     %
	%%%%%%%%%%%%%%%%%%%%%%%%

bpm_index = placet_get_number_list(beamline_name, "bpm");
qp_index = placet_get_number_list(beamline_name, 'quadrupole');
drift_index = placet_get_number_list(beamline_name, 'drift');
mult_index = placet_get_number_list(beamline_name, 'multipole');
#for ii=1:5

alignment_sigma = 10 ;#10 [um]

placet_element_set_attribute(beamline_name, bpm_index, "x", randn(size(bpm_index)) * alignment_sigma);
placet_element_set_attribute(beamline_name, bpm_index, "y", randn(size(bpm_index)) * alignment_sigma);
placet_element_set_attribute(beamline_name, qp_index, "x", randn(size(qp_index)) * alignment_sigma);
placet_element_set_attribute(beamline_name, qp_index, "y", randn(size(qp_index)) * alignment_sigma);
placet_element_set_attribute(beamline_name, mult_index, "x", randn(size(mult_index)) * alignment_sigma);
placet_element_set_attribute(beamline_name, mult_index, "y", randn(size(mult_index)) * alignment_sigma);


Matrice = [];


for ii=1:1


        source "$script_dir/scripts/dynamic_setup.m";

	%%%%%%%%%%%%%%%%%%%%%%%%%
	% System Identification %
	%%%%%%%%%%%%%%%%%%%%%%%%%
	
        % system identification setup
        source "$script_dir/scripts/sysident_setup.m"

	%%%%%%%%%%%%
	% Tracking %
	%%%%%%%%%%%%

        for bunch_index = 1:n_bunches
            if (n_bunches > 1) 
                fprintf(1, 'Pulse %d Bunch %d, Time: %fs of %fs\n', time_step_index, bunch_index, time, nr_time_steps*delta_T);
            end
            % bunch feedback + bunch effects
            % now inside tracking.m
            % source "$script_dir/scripts/bunch_effects.m"

	%%%%%%%%%%%%%%%%%%%%%%%%
	%  Randn quad strength %
	%%%%%%%%%%%%%%%%%%%%%%%%


#QI = placet_get_number_list("ATF2_v5.2-MadX", "quadrupole");
#SQ = placet_element_get_attribute("ATF2_v5.2-MadX", QI, "strength");

                           #for ii=1:1000

#placet_element_set_attribute("ATF2_v5.2-MadX", QI, "strength", SQ + 1e-4 * transpose(randn(size(QI))));


            source "$script_dir/scripts/tracking.m"

            %%%%%%%%%%%%%%%%%%%%%
	    % Store the results %
	    %%%%%%%%%%%%%%%%%%%%%
            
            source "$script_dir/scripts/postprocess_time_step.m"
            time += bunch_spacing;


            %%%%%%%%%%%%%%%%%%%%%
	    %     Beam size     %
	    %%%%%%%%%%%%%%%%%%%%%



#HSy(ii) = 1e3 * std(Beam(:,3)); # beamsize at IP in nm
#disp("Random strength for all quad number ");disp(ii);
#disp("Random misalignment number ");disp(ii);
#disp(ii);
#gamma_ = 1 + e_tcl*1000000000/511000;
#emitt_ = emitt_y_tcl * 0.0000001;
#beam_size_(ii) = sqrt(beta_y_tcl * emitt_ / gamma_)*1000000000
#beam_size_ = sqrt(beta_y_tcl * emitt_ / gamma_)*1000000000;


Blist=placet_get_number_list("ATF2_v5.2-MadX", "bpm");
Bname=placet_element_get_attribute("ATF2_v5.2-MadX", Blist, "name"); 
Bpos=placet_element_get_attribute("ATF2_v5.2-MadX", Blist, "s");
Bmeas=placet_get_bpm_readings("ATF2_v5.2-MadX",Blist); 

Matrice = [Matrice;Bpos,Bmeas];


E0 = 1.3;
X = Beam(:,2);
Y = Beam(:,3);
XP = Beam(:,5);
YP = Beam(:,6);
D = (Beam(:,1) - E0) / E0;
I = ones(size(X));
beamsize1 = std(Y)

save -text DATA_BPMs.dat Matrice;

save -text Beam1.dat Beam

Ay = [ X.^3 X.^2 X.*XP XP.^2 D.^2 X XP YP D I ];
Py = Ay \ Y;
Y1 = Y - Ay * Py;
Beam(:,3) = Y1;
beamsize2 = std(Y1)

save -text Beam2.dat Beam

#Ayp = [ X.^3 X.^2 X.*XP XP.^2 D.^2 X Y XP D I ];
#Pyp = Ayp \ YP;
#YP1 = YP - Ayp * Pyp;
#Beam(:,6) = YP1;

#save -text Beam3.dat Beam



     
#Bsig=[Bpos Bmeas];
#fid = fopen('DATA_BPMs.txt', 'w');
#if fid
#for i=1:length(Blist)   
#fprintf(fid, '%s %f %f %f \n', deblank(Bname(i,:)), Bpos(i), Bmeas(i,1), Bmeas(i,2));
#end
#fclose(fid);
#end   

end

        end

        %%%%%%%%%%%%%%%%%
        % System Update %
        %%%%%%%%%%%%%%%%%
      
        % updating of the parameters (response matrix, etc.)
        source "$script_dir/scripts/sysident_update.m"

      
        % store results and end process
        %source "$script_dir/scripts/postprocess_time_step.m"



#beam_size = transpose(beam_size_);
save -text beam_size.dat beam_size;
      
        toc
      
    end
      
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Delete unused files for a clean sim environment %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    source "$script_dir/scripts/postprocess_sim.m"

}




# Python post analysis
if {$python_analysis} {
    cd "$script_dir/analysis"
    Python {
# no indent!
execfile("PythonPostAnalysis.py")
print "End of Python Analysis"
    }
}


puts "Tracking ..."


Octave {

[E,B] = placet_test_no_correction("ATF2_v5.2-MadX", "electron", "None", 1);

E0 = 1.3;

X = B(:,2);
Y = B(:,3);
XP = B(:,5);
YP = B(:,6);
D = (B(:,1) - E0) / E0;
I = ones(size(X));

Ay = [ X.^3 X.^2 X.*XP XP.^2 D.^2 X XP YP D I ];

Py = Ay \ Y;

Y1 = Y - Ay * Py;

B(:,3) = Y1;


	nbelement=size(placet_get_number_list("ATF2_v5.2-MadX","*"))(2);
	sig_0=placet_get_sigma_matrix(placet_get_beam("electron"));
	emit_0=placet_get_emittance(placet_get_beam("electron"));
	#size.x = std(B(:,2));
	#size.y = std(B(:,3));
	pos.x = mean(B(:,2));
	pos.y = mean(B(:,3));
	delta = mean(B(:,1).*B(:,5));
	#matrix=[size.x*1e-6 size.y*1e-6 pos.x*1e-6 pos.y*1e-6];
	sig_ip=placet_get_sigma_matrix(B);
	twiss_ip=placet_get_twiss_matrix(B);
	emit_ip=placet_get_emittance(B);
	%disp("E_spread (%)=");disp(std(B(:,1))/mean(B(:,1))*100);
	%disp("sigma_0=");disp(sig_0);
	%disp("sigma_ip=");disp(sig_ip);
	%disp("emittance_ip=");disp(emit_ip);
	%disp("disp_ip=");disp(delta);
	%disp("emittance_0=");disp(emit_0);
	%disp("twiss_ip=");disp(twiss_ip);
	%disp("size.x(m) size.y(m) pos.x(m) pos.y(m)");
	%disp(matrix);
	save -text ${beamline_name}_Beam.dat B
	save -text ${beamline_name}_E.dat E
}

TwissPlot -beam electron -file twiss.dat

#Octave {   
#    Blist=placet_get_number_list("ATF2", "bpm");
#    Bname=placet_element_get_attribute("ATF2", Blist, "name"); 
#    Bpos=placet_element_get_attribute("ATF2", Blist, "s");
#    Bmeas=placet_get_bpm_readings("ATF2",Blist);  

#    Blist=placet_get_number_list("ATF2_v5.2-MadX", "bpm");
#    Bname=placet_element_get_attribute("ATF2_v5.2-MadX", Blist, "name"); 
#    Bpos=placet_element_get_attribute("ATF2_v5.2-MadX", Blist, "s");
#    Bmeas=placet_get_bpm_readings("ATF2_v5.2-MadX",Blist); 
          
#    Bsig=[Bpos Bmeas];
#    fid = fopen('placet_BBA_bpms.txt', 'w');
#    if fid
#    for i=1:length(Blist)   
#    fprintf(fid, '%s %f %f %f \n', deblank(Bname(i,:)), Bpos(i), Bmeas(i,1), Bmeas(i,2));
#    end
#    fclose(fid);
#    end     

#}


#Octave {

#    bpm_pos = placet_element_get_attribute(beamline_name, bpm_index, "s");
#    bpm_length = placet_element_get_attribute(beamline_name, bpm_index, "length");
#    bpm_center = bpm_pos - bpm_length/2;

#    bpm_readings_x = placet_element_get_attribute(beamline_name,bpm_index,"reading_x");

#    bpm_readings_y = placet_element_get_attribute(beamline_name,bpm_index,"reading_y");

#    data = [bpm_center,bpm_readings_x,bpm_readings_y];
#    save -text BPM_data.dat data;

#    bpm_name = placet_element_get_attribute(beamline_name, bpm_index, "name");
    
#    disp("bunch_index=");disp(bunch_index);
#}


#Octave {
#     B0 = placet_get_beam("electron");
#     disp("beam3=");disp(mean(B0(:,2)));
#     B0(:,2) -= mean(B0(:,2));
#     B0(:,3) -= mean(B0(:,3));
#     B0(:,5) -= mean(B0(:,5));
#     B0(:,6) -= mean(B0(:,6));
#     placet_set_beam("electron", B0);
#     disp("beam4=");disp(mean(B0(:,2)));
#}




puts "ATF2 run has ended"
