##########################################################################################
#                                     ATF2 Beam Studies                                  
#
# Pierre Korysko (pierre.korysko@cern.ch, CERN/University of Oxford)
# Jochem Snuverink (jochem.snuverink@rhul.ac.uk, Royal Holloway, University of London)
# Young-Im Kim (Youngim.Kim@physics.ox.ac.uk, University of Oxford) 
#
# Last update : 2019/11/12
#
##########################################################################################

1) Description 
* Beam tracking simulation for the ATF2, ATF2_v5.2 optics (numerous wakefield sources removed in Nov 2016)
* Features : Ground motion, dispersion, jitter, multi-bunch, bunch-to-bunch correlation, one-to-one, DFS, WFS, Wakefield Knobs
* BPMs : stripline, cbpm_noatt (c-band bpm resolution w/o 20 dB attenuation), cbpm sbpm, ipbpm

2) Installation and usage
* ATF2 beam studies requires placet-octave to be installed. To install placet-octave, See below 
https://clicsw.web.cern.ch/clicsw/
http://www-pnp.physics.ox.ac.uk/~kimy/beamSim.html
$svn co http://svnweb.cern.ch/guest/clicsim/trunk/ATF2
Once placet-octave is installed, tou lauch the ATF2_Study simulation, do :
$placet-octave run_atf2.tcl 

3) Options are in the settings.tcl 
It is recommended NOT to change this file, but instead write your own
settings for example in a second optional 'ipfeedback.tcl' file. An example is given in the repository. 
Then run as below :

$placet run_atf2.tcl ipfeedback.tcl

4) analysis is an example to read output files and make a plot, written as Python. 
