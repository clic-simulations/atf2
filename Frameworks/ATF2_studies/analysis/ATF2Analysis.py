import pylab as pl
import matplotlib as mpl

from pylab import *
from scipy.optimize import leastsq

PRSTABstyle = True
#default plotting

if not PRSTABstyle:
    mpl.rc('axes', grid=True)
mpl.rc('legend',numpoints=1,fontsize=20)
mpl.rc('axes',labelsize=20)
mpl.rc('axes',titlesize=20)
mpl.rc('axes',linewidth=1.5)
mpl.rc('lines',linewidth=1.5)     # line width in points

mpl.rc('xtick',labelsize=18) # fontsize of the tick labels
mpl.rc('ytick',labelsize=18) # fontsize of the tick labels
mpl.rc('xtick.major',width=1.5)
mpl.rc('ytick.major',width=1.5)

def Murnaghan(parameters,vol):
    '''
    given a vector of parameters and volumes, return a vector of energies.
    equation From PRB 28,5480 (1983)
    '''
    E0 = parameters[0]
    B0 = parameters[1]
    BP = parameters[2]
    V0 = parameters[3]
    
    E = E0 + B0*vol/BP*(((V0/vol)**BP)/(BP-1)+1) - V0*B0/(BP-1.)

    return E

# and we define an objective function that will be minimized
def objective(pars,y,x):
    #we will minimize this function
    err =  y - Murnaghan(pars,x)
    return err

class twiss2 : 
    def __init__(self, directory="../output/",fileName="twiss_evolve.dat") :

        self.dir  = directory
        self.fileName  = fileName

        # plot until s=end
        self.end  = 90.5 # IP
        # self.end  = 92 # full
        
        self.d1   = [] # s
        self.d2   = [] # beta_x
        self.d3   = [] # beta_y
        self.d4   = [] # alpha_x
        self.d5   = [] # alpha_y
        self.d6   = [] # phase_x
        self.d7   = [] # phase_y
        self.d8   = [] # disp_x
        self.d9   = [] # disp_y
        # self.d10  = [] # E
        
        self.readFile()
        self.controlPlot()

    def readFile(self) :
        f = open(self.dir+self.fileName)
        for l in f : 
            t = l.split()
            if t [0] =='#' :
                continue
            d1    = float(t[0])
            d2    = float(t[1])
            d3    = float(t[2])
            d4    = float(t[3])
            d5    = float(t[4])
            d6    = float(t[5])
            d7    = float(t[6])
            d8    = float(t[7])
            d9    = float(t[8])
            #d10   = float(t[9])
            
            self.d1.append(d1)
            self.d2.append(d2)
            self.d3.append(d3)
            self.d4.append(d4)
            self.d5.append(d5)
            self.d6.append(d6)
            self.d7.append(d7)
            self.d8.append(d8)
            self.d9.append(d9)
            #self.d10.append(d10)

        self.d1 = pl.array(self.d1)
        self.d2 = pl.array(self.d2)
        self.d3 = pl.array(self.d3)
        self.d4 = pl.array(self.d4)
        self.d5 = pl.array(self.d5)
        self.d6 = pl.array(self.d6)
        self.d7 = pl.array(self.d7)
        self.d8 = pl.array(self.d8)
        self.d9 = pl.array(self.d9)
        #self.d10 = pl.array(self.d10)

    def controlPlot(self) :
        pl.figure(20)
        pl.clf()
        pl.subplot(2,1,1)
        pl.plot(self.d1, pl.sqrt(self.d2), 'b-', label='$\sqrt{\\beta_x}$')
        pl.plot(self.d1, pl.sqrt(self.d3), 'g-.',label='$\sqrt{\\beta_y}$')
        pl.xlim(0,90.5)
        pl.xlabel('$s$ [$\mathrm{m}$]')
        pl.ylabel('$\sqrt{\\beta}$ [$\sqrt{\mathrm{m}}$]')
        pl.legend(loc=0, numpoints=1, prop={'size':20})
        pl.subplot(2,1,2)
        pl.plot(self.d1, self.d8, 'b-', label='$\eta_x$')
        pl.plot(self.d1, self.d9, 'g-.', label='$\eta_y$')
        pl.xlim(0,90.5)
        pl.xlabel('$s$ [$\mathrm{m}$]')
        pl.ylabel('Dispersion ($\eta$) [$\mathrm{m}$]')
        pl.legend(loc=0, numpoints=1, prop={'size':20})
        pl.subplots_adjust(0.15, 0.12, 0.95, 0.95, 0.20, 0.30)
        pl.savefig(self.dir+"twiss.png")
	
        pl.show()

        #pl.figure(21, figsize=(8,5))
        pl.figure(21)
        pl.clf()
        pl.plot(self.d1, pl.sqrt(self.d2), 'b-', label='$\sqrt{\\beta_x}$')
        pl.plot(self.d1, pl.sqrt(self.d3), 'g-.',label='$\sqrt{\\beta_y}$')
        pl.xlim(0,90.5)
        pl.xlabel('$s$ [m]')
        pl.ylabel('Beta function ($\sqrt{\\beta}$) [$\sqrt{m}$]')
        pl.legend(loc=0, numpoints=1, prop={'size':18})
        pl.savefig(self.dir+"beta.pdf")

class twiss : 
    def __init__(self, directory="../output/", fileName="twiss.dat") :

        self.dir       = directory
	self.fileName  = fileName
	self.eleNum    = []
	self.s         = []
	self.Es        = []
	self.betaXm    = []
	self.alphaXm   = []
	self.betaXi    = []
	self.alphaXi   = []
	self.betaYm    = []
	self.alphaYm   = []
	self.betaYi    = []
	self.alphaYi   = []
	self.dispX     = []
	self.dispXp    = []
	self.dispY     = []
	self.dispYp    = []

	self.readFile()
	self.controlPlot()

    def readFile(self) :
	f = open(self.dir + self.fileName)
	for l in f : 
	    t = l.split()
	    if t [0] =='#' :
		continue
	    eleNum    = float(t[0])
	    s         = float(t[1])
	    Es        = float(t[2])
	    betaXm    = float(t[3])
	    alphaXm   = float(t[4])
	    betaXi    = float(t[5])
	    alphaXi   = float(t[6])
	    betaYm    = float(t[7])
	    alphaYm   = float(t[8])
	    betaYi    = float(t[9])
	    alphaYi   = float(t[10])
	    dispX     = float(t[11])
	    dispXp    = float(t[12])
	    dispY     = float(t[13])
	    dispYp    = float(t[14])

	    self.eleNum.append(eleNum)
	    self.s.append(s)
	    self.Es.append(Es)
	    self.betaXm.append(betaXm)
	    self.alphaXm.append(alphaXm)
	    self.betaXi.append(betaXi)
	    self.alphaXi.append(alphaXi)
	    self.betaYm.append(betaYm)
	    self.alphaYm.append(alphaYm)
	    self.betaYi.append(betaYi)
	    self.alphaYi.append(alphaYi)
	    self.dispX.append(dispX)
	    self.dispXp.append(dispXp)
	    self.dispY.append(dispY)
	    self.dispYp.append(dispYp)

	self.eleNum    = pl.array(self.eleNum)
	self.s         = pl.array(self.s)
	self.Es        = pl.array(self.Es)
	self.betaXm    = pl.array(self.betaXm)
	self.alphaXm   = pl.array(self.alphaXm)
	self.betaXi    = pl.array(self.betaXi)
	self.alphaXi   = pl.array(self.alphaXi)
	self.betaYm    = pl.array(self.betaYm)
	self.alphaYm   = pl.array(self.alphaYm)
	self.betaYi    = pl.array(self.betaYi)
	self.alphaYi   = pl.array(self.alphaYi)
	self.dispX     = pl.array(self.dispX)
	self.dispXp    = pl.array(self.dispXp)
	self.dispY     = pl.array(self.dispY)
	self.dispYp    = pl.array(self.dispYp)


    def controlPlot(self) :
	pl.figure(21)
        pl.clf()
        pl.subplot(2,1,1)
        pl.plot(self.s, pl.sqrt(self.betaXi), 'b-', label='$\sqrt{\\beta_x}$')
        pl.plot(self.s, pl.sqrt(self.betaYi), 'g-.',label='$\sqrt{\\beta_y}$')
        pl.xlabel('$s$ [m]')
        pl.ylabel('Beta function ($\sqrt{\\beta}$) [$\sqrt{m}$]')
        pl.legend(loc=0, numpoints=1)
        pl.subplot(2,1,2)
        pl.plot(self.s, self.dispX, 'b-', label='$\eta_x$')
        pl.plot(self.s, self.dispY, 'g-.', label='$\eta_y$')
        pl.xlabel('$s$ [m]')
        pl.ylabel('Dispersion ($\eta$)')
        pl.legend(loc=0, numpoints=1)
	
	pl.show()

class beamStudy : 
    def __init__(self, fileName="../ipfeedback_0_0_0_0_0_0/meas_station.dat") :

	self.fileName       = fileName
	self.time           = []
	self.orbit_x_std    = []
	self.orbit_y_std    = []
	self.spe_x          = [] # single pulse emittance
	self.spe_y          = []
	self.offset_x       = []
	self.offset_y       = []
	self.orbit_x_max    = []
	self.orbit_y_max    = []
	self.sps_x          = []
	self.sps_y          = []
	self.mpe_x          = [] # multi pulse emittance
	self.mpe_y          = []
	self.mref3_pos      = [] # mover position [um] (optional)

	self.readFile()
	self.controlPlot()

    def readFile(self) :
	f = open(self.fileName)
	for l in f : 
	    t = l.split()
	    if t [0] =='#' :
		continue
	    time           = t[0]
	    orbit_x_std    = t[1]
	    orbit_y_std    = t[2]
	    spe_x          = t[3]
	    spe_y          = t[4]
	    offset_x       = t[5]
	    offset_y       = t[6]
	    orbit_x_max    = t[7]
	    orbit_y_max    = t[8]
	    sps_x          = t[9]
	    sps_y          = t[10]
	    mpe_x          = t[11]
	    mpe_y          = t[12]
	    mref3_pos      = t[13]

	    self.time.append(time)
	    self.orbit_x_std.append(orbit_x_std)
	    self.orbit_y_std.append(orbit_y_std)
	    self.spe_x.append(spe_x)
	    self.spe_y.append(spe_y)
	    self.offset_x.append(offset_x)
	    self.offset_y.append(offset_y)
	    self.orbit_x_max.append(orbit_x_max)
	    self.orbit_y_max.append(orbit_y_max)
	    self.sps_x.append(sps_x)
	    self.sps_y.append(sps_y)
	    self.mpe_x.append(mpe_x)
	    self.mpe_y.append(mpe_y)
	    self.mref3_pos.append(mref3_pos)

	self.time           = pl.array(self.time)
	self.orbit_x_std    = pl.array(self.orbit_x_std)
	self.orbit_y_std    = pl.array(self.orbit_y_std)
	self.spe_x          = pl.array(self.spe_x)
	self.spe_y          = pl.array(self.spe_y)
	self.offset_x       = pl.array(self.offset_x) 
	self.offset_y       = pl.array(self.offset_y) 
	self.orbit_x_max    = pl.array(self.orbit_x_max)
	self.orbit_y_max    = pl.array(self.orbit_y_max)
	self.sps_x          = pl.array(self.sps_x)
	self.sps_y          = pl.array(self.sps_y)
	self.mpe_x          = pl.array(self.mpe_x)
	self.mpe_y          = pl.array(self.mpe_y)
	self.mref3_pos      = pl.array(self.mref3_pos)

    def controlPlot(self) :
	pl.figure(1)
	pl.clf()
	pl.plot(self.mref3_pos, self.spe_y, '+-')
	pl.xlabel("Kick")
	pl.ylabel("Beam size [$\\mu$m]")

class qf1ffStrengthScan :
    def __init__(self, fileList='files/qf1ffScan.txt') :

	dir = 'simData/QF1FFStrengthScan/'
	self.qf1ffstrength = pl.array([0.18034, 0.28034, 0.38034, 0.48034, 0.58034, 0.68034, 0.78034, 0.88034])
	self.fileList = fileList
	f = open(self.fileList)
	self.ipaJitterx = []
	self.ipaJittery = []
	self.ipaMeanx = []
	self.ipaMeany = []

	self.ipbJitterx = []
	self.ipbJittery = []
	self.ipbMeanx = []
	self.ipbMeany = []
	for l in f :
	    t = l.split()
	    if t [0][0] =='#' :
		continue
	    c = t[1]
	    file = dir+t[0]

	    q = orbitStudy(file, c)
	    ipaJitterx = q.fx[:,-3].std()
	    ipaJittery = q.fy[:,-3].std()
	    ipaMeanx   = q.fx[:,-3].mean()
	    ipaMeany   = q.fy[:,-3].mean()

	    ipbJitterx = q.fx[:,-2].std()
	    ipbJittery = q.fy[:,-2].std()
	    ipbMeanx   = q.fx[:,-2].mean()
	    ipbMeany   = q.fy[:,-2].mean()

	    self.ipaJitterx.append(ipaJitterx)
	    self.ipaJittery.append(ipaJittery)
	    self.ipaMeanx.append(ipaMeanx)
	    self.ipaMeany.append(ipaMeany)

	    self.ipbJitterx.append(ipbJitterx)
	    self.ipbJittery.append(ipbJittery)
	    self.ipbMeanx.append(ipbMeanx)
	    self.ipbMeany.append(ipbMeany)

	self.ipaJitterx = pl.array(self.ipaJitterx)
	self.ipaJittery = pl.array(self.ipaJittery)
	self.ipaMeanx   = pl.array(self.ipaMeanx)
	self.ipaMeany   = pl.array(self.ipaMeany)

	self.ipbJitterx = pl.array(self.ipbJitterx)
	self.ipbJittery = pl.array(self.ipbJittery)
	self.ipbMeanx   = pl.array(self.ipbMeanx)
	self.ipbMeany   = pl.array(self.ipbMeany)

	self.controlPlot()

    def controlPlot(self) :

	vfit = np.linspace(min(self.qf1ffstrength), max(self.qf1ffstrength), 100)
	a1,b1,c1 = pl.polyfit(self.qf1ffstrength, self.ipaJitterx**2, 2)
	a2,b2,c2 = pl.polyfit(self.qf1ffstrength, self.ipbJitterx**2, 2)
	d1,e1,f1 = pl.polyfit(self.qf1ffstrength, self.ipaJittery**2, 2)
	d2,e2,f2 = pl.polyfit(self.qf1ffstrength, self.ipbJittery**2, 2)

	v01 = -b1/(2*a1)
	e01 = a1*v01**2 + b1*v01 + c1
	b01 = 2*a1*v01
	bP1 = 4
	x01 = [e01, b01, bP1, v01] #initial guesses in the same order used in the Murnaghan function
	murnpars1, ier1 = leastsq(objective, x01, args=(self.ipaJitterx**2,self.qf1ffstrength)) #this is from scipy

	v02 = -b2/(2*a2)
	e02 = a2*v02**2 + b2*v02 + c2
	b02 = 2*a2*v02
	bP2 = 4
	x02 = [e02, b02, bP2, v02] #initial guesses in the same order used in the Murnaghan function
	murnpars2, ier2 = leastsq(objective, x02, args=(self.ipbJitterx**2,self.qf1ffstrength)) #this is from scipy

	v03 = -e1/(2*d1)
	e03 = d1*v03**2 + e1*v03 + f1
	b03 = 2*d1*v03
	bP3 = 4
	x03 = [e03, b03, bP3, v03] #initial guesses in the same order used in the Murnaghan function
	murnpars3, ier3 = leastsq(objective, x03, args=(self.ipaJittery**2,self.qf1ffstrength)) #this is from scipy

	v04 = -e2/(2*d2)
	e04 = d2*v04**2 + e2*v04 + f2
	b04 = 2*d2*v04
	bP4 = 4
	x04 = [e04, b04, bP4, v04] #initial guesses in the same order used in the Murnaghan function
	murnpars4, ier4 = leastsq(objective, x04, args=(self.ipbJittery**2,self.qf1ffstrength)) #this is from scipy

	pl.figure(7)
	pl.clf()
	ax1 = pl.subplot(2,2,1)
	pl.plot(self.qf1ffstrength, self.ipaJitterx**2, 'b.', label='IPA $x$')
	pl.plot(vfit,a1*vfit**2+b1*vfit+c1,'r-', label='Fit') 
	#pl.plot(vfit, Murnaghan(murnpars1,vfit))
	ax1.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.legend(loc=0, numpoints=1)
	pl.xlabel('QF1FF Strength [GeV/m]')
	pl.ylabel('Jitter $\sigma_x^2$ [$\mu$m$^2$]')
	ax2 = pl.subplot(2,2,2)
	pl.plot(self.qf1ffstrength, self.ipaJittery**2, 'b.', label='IPA $y$')
	pl.plot(vfit,d1*vfit**2+e1*vfit+f1,'r-', label='Fit') 
	#pl.plot(vfit, Murnaghan(murnpars3,vfit))
	ax2.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.legend(loc=0, numpoints=1)
	pl.xlabel('QF1FF Strength [GeV/m]')
	pl.ylabel('Jitter $\sigma_y^2$ [$\mu$m$^2$]')

	ax3 = pl.subplot(2,2,3, sharex=ax1)
	pl.plot(self.qf1ffstrength, self.ipbJitterx**2, 'b.', label='IPB $x$')
	pl.plot(vfit,a2*vfit**2+b2*vfit+c2,'r-', label='Fit') 
	#pl.plot(vfit, Murnaghan(murnpars2,vfit))
	ax3.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.legend(loc=0, numpoints=1)
	pl.xlabel('QF1FF Strength [GeV/m]')
	pl.ylabel('Jitter $\sigma_x^2$ [$\mu$m$^2$]')
	pl.subplots_adjust(0.13, 0.10, 0.93, 0.93, 0.30, 0.20)
	ax4 = pl.subplot(2,2,4, sharex=ax1)
	pl.plot(self.qf1ffstrength, self.ipbJittery**2, 'b.', label='IPB $y$')
	pl.plot(vfit,d2*vfit**2+e2*vfit+f2,'r-', label='Fit') 
	#pl.plot(vfit, Murnaghan(murnpars4,vfit))
	ax4.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.legend(loc=0, numpoints=1)
	pl.xlabel('QF1FF Strength [GeV/m]')
	pl.ylabel('Jitter $\sigma_y^2$ [$\mu$m$^2$]')
	pl.subplots_adjust(0.13, 0.10, 0.95, 0.93, 0.30, 0.30)

	pl.figure(13)
	pl.clf()
	ax1 = pl.subplot(1,2,1)
	pl.plot(self.qf1ffstrength, self.ipaJittery**2, 'b.', label='IPA')
	pl.plot(vfit,d1*vfit**2+e1*vfit+f1,'r-', label='Fit') 
	pl.plot(vfit, Murnaghan(murnpars3,vfit))
	ax1.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.legend(loc=0, numpoints=1)
	pl.xlabel('QF1FF Strength [GeV/m]')
	pl.ylabel('Jitter $\sigma_y^2$ [$\mu$m$^2$]')
	ax2 = pl.subplot(1,2,2, sharex=ax1)
	pl.plot(self.qf1ffstrength, self.ipbJittery**2, 'b.', label='IPB')
	pl.plot(vfit,d2*vfit**2+e2*vfit+f2,'r-', label='Fit') 
	pl.plot(vfit, Murnaghan(murnpars4,vfit))
	ax2.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.legend(loc=0, numpoints=1)
	pl.xlabel('QF1FF Strength [GeV/m]')
	pl.ylabel('Jitter $\sigma_y^2$ [$\mu$m$^2$]')
	pl.subplots_adjust(0.13, 0.10, 0.93, 0.93, 0.30, 0.20)

	pl.figure(11)
	pl.clf()
	ax1 = pl.subplot(1,2,1)
	pl.errorbar(self.qf1ffstrength, self.ipaMeanx, yerr=self.ipaJitterx, fmt='-o',ecolor='b', label='IPA $x$')
	pl.errorbar(self.qf1ffstrength, self.ipaMeany, yerr=self.ipaJittery, fmt='-o',ecolor='g', label='IPA $y$')
	ax1.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.legend(loc=0, numpoints=1)
	pl.grid()
	pl.xlabel('QF1FF Strength [GeV/m]')
	pl.ylabel('IPA position $x/y$ [$\mu$m]')

	ax2 = pl.subplot(1,2,2, sharex=ax1)
	pl.errorbar(self.qf1ffstrength, self.ipbMeanx, yerr=self.ipbJitterx, fmt='-o',ecolor='b', label='IPB $x$')
	pl.errorbar(self.qf1ffstrength, self.ipbMeany, yerr=self.ipbJittery, fmt='-o',ecolor='g', label='IPB $y$')
	ax2.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.legend(loc=0, numpoints=1)
	pl.grid()
	pl.xlabel('QF1FF Strength [GeV/m]')
	pl.ylabel('IPB position $x/y$ [$\mu$m]')
	pl.subplots_adjust(0.13, 0.10, 0.93, 0.93, 0.30, 0.20)

	pl.figure(12)
	pl.clf()
	ax1 = pl.subplot(2,2,1)
	pl.errorbar(self.qf1ffstrength, self.ipaMeanx, yerr=self.ipaJitterx, fmt='-o',ecolor='b')
	ax1.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.grid()
	pl.xlabel('QF1FF Strength [GeV/m]')
	pl.ylabel('IPA position $x$ [$\mu$m]')

	ax2 = pl.subplot(2,2,2)
	pl.errorbar(self.qf1ffstrength, self.ipaMeany, yerr=self.ipaJittery, fmt='-o',ecolor='b')
	ax2.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.grid()
	pl.xlabel('QF1FF Strength [GeV/m]')
	pl.ylabel('IPA position $y$ [$\mu$m]')

	ax3 = pl.subplot(2,2,3)
	pl.errorbar(self.qf1ffstrength, self.ipbMeanx, yerr=self.ipbJitterx, fmt='-o',ecolor='b')
	ax3.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.grid()
	pl.xlabel('QF1FF Strength [GeV/m]')
	pl.ylabel('IPB position $x$ [$\mu$m]')

	ax4 = pl.subplot(2,2,4)
	pl.errorbar(self.qf1ffstrength, self.ipbMeany, yerr=self.ipbJittery, fmt='-o',ecolor='b')
	ax4.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.grid()
	pl.xlabel('QF1FF Strength [GeV/m]')
	pl.ylabel('IPB position $y$ [$\mu$m]')

	pl.subplots_adjust(0.12, 0.10, 0.95, 0.95, 0.30, 0.30)

class qd0ffStrengthScan :
    def __init__(self, fileList='files/qd0ffStrengthScan.txt') :

	dir = 'simData/QD0FFStrengthScan/'
	self.qd0ffstrength = pl.array([-1.08554,-1.03544,-0.98554,-0.93544,-0.88554,-0.83554,-0.78554,-0.73544,-0.68554])
	self.fileList = fileList
	f = open(self.fileList)
	self.ipaJitterx = []
	self.ipaJittery = []
	self.ipaMeanx = []
	self.ipaMeany = []

	self.ipbJitterx = []
	self.ipbJittery = []
	self.ipbMeanx = []
	self.ipbMeany = []

	for l in f :
	    t = l.split()
	    if t [0][0] =='#' :
		continue
	    c = t[1]
	    file = dir+t[0]

	    q = orbitStudy(file, c)
	    ipaJitterx = q.fx[:,-3].std()
	    ipaJittery = q.fy[:,-3].std()
	    ipaMeanx   = q.fx[:,-3].mean()
	    ipaMeany   = q.fy[:,-3].mean()

	    ipbJitterx = q.fx[:,-2].std()
	    ipbJittery = q.fy[:,-2].std()
	    ipbMeanx   = q.fx[:,-2].mean()
	    ipbMeany   = q.fy[:,-2].mean()

	    self.ipaJitterx.append(ipaJitterx)
	    self.ipaJittery.append(ipaJittery)
	    self.ipaMeanx.append(ipaMeanx)
	    self.ipaMeany.append(ipaMeany)

	    self.ipbJitterx.append(ipbJitterx)
	    self.ipbJittery.append(ipbJittery)
	    self.ipbMeanx.append(ipbMeanx)
	    self.ipbMeany.append(ipbMeany)

	self.ipaJitterx = pl.array(self.ipaJitterx)
	self.ipaJittery = pl.array(self.ipaJittery)
	self.ipaMeanx   = pl.array(self.ipaMeanx)
	self.ipaMeany   = pl.array(self.ipaMeany)

	self.ipbJitterx = pl.array(self.ipbJitterx)
	self.ipbJittery = pl.array(self.ipbJittery)
	self.ipbMeanx   = pl.array(self.ipbMeanx)
	self.ipbMeany   = pl.array(self.ipbMeany)

	self.controlPlot()

    def controlPlot(self) :

	vfit = np.linspace(min(self.qd0ffstrength), max(self.qd0ffstrength), 100)
	a1,b1,c1 = pl.polyfit(self.qd0ffstrength, self.ipaJitterx**2, 2)
	a2,b2,c2 = pl.polyfit(self.qd0ffstrength, self.ipbJitterx**2, 2)
	d1,e1,f1 = pl.polyfit(self.qd0ffstrength, self.ipaJittery**2, 2)
	d2,e2,f2 = pl.polyfit(self.qd0ffstrength, self.ipbJittery**2, 2)

	v01 = -b1/(2*a1)
	e01 = a1*v01**2 + b1*v01 + c1
	b01 = 2*a1*v01
	bP1 = 4
	x01 = [e01, b01, bP1, v01] #initial guesses in the same order used in the Murnaghan function
	murnpars1, ier1 = leastsq(objective, x01, args=(self.ipaJitterx**2,self.qd0ffstrength)) #this is from scipy

	v02 = -b2/(2*a2)
	e02 = a2*v02**2 + b2*v02 + c2
	b02 = 2*a2*v02
	bP2 = 4
	x02 = [e02, b02, bP2, v02] #initial guesses in the same order used in the Murnaghan function
	murnpars2, ier2 = leastsq(objective, x02, args=(self.ipbJitterx**2,self.qd0ffstrength)) #this is from scipy

	v03 = -e1/(2*d1)
	e03 = d1*v03**2 + e1*v03 + f1
	b03 = 2*d1*v03
	bP3 = 4
	x03 = [e03, b03, bP3, v03] #initial guesses in the same order used in the Murnaghan function
	murnpars3, ier3 = leastsq(objective, x03, args=(self.ipaJittery**2,self.qd0ffstrength)) #this is from scipy

	v04 = -e2/(2*d2)
	e04 = d2*v04**2 + e2*v04 + f2
	b04 = 2*d2*v04
	bP4 = 4
	x04 = [e04, b04, bP4, v04] #initial guesses in the same order used in the Murnaghan function
	murnpars4, ier4 = leastsq(objective, x04, args=(self.ipbJittery**2,self.qd0ffstrength)) #this is from scipy

	pl.figure(5)
	pl.clf()
	ax1 = pl.subplot(2,2,1)
	pl.plot(self.qd0ffstrength, self.ipaJitterx**2, 'b.', label='IPA')
	pl.plot(vfit,a1*vfit**2+b1*vfit+c1,'r-', label='Fit') 
	#pl.plot(vfit, Murnaghan(murnpars1,vfit))
	ax1.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.legend(loc=0, numpoints=1)
	pl.xlabel('QD0FF Strength [GeV/m]')
	pl.ylabel('Jitter $\sigma_x^2$ [$\mu$m$^2$]')
	ax2 = pl.subplot(2,2,2)
	pl.plot(self.qd0ffstrength, self.ipaJittery**2, 'b.', label='IPA')
	pl.plot(vfit,d1*vfit**2+e1*vfit+f1,'r-', label='Fit') 
	#pl.plot(vfit, Murnaghan(murnpars3,vfit))
	ax2.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.legend(loc=0, numpoints=1)
	pl.xlabel('QD0FF Strength [GeV/m]')
	pl.ylabel('Jitter $\sigma_y^2$ [$\mu$m$^2$]')

	ax3 = pl.subplot(2,2,3, sharex=ax1)
	pl.plot(self.qd0ffstrength, self.ipbJitterx**2, 'b.', label='IPB')
	pl.plot(vfit,a2*vfit**2+b2*vfit+c2,'r-', label='Fit') 
	#pl.plot(vfit, Murnaghan(murnpars2,vfit))
	ax3.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.legend(loc=0, numpoints=1)
	pl.xlabel('QD0FF Strength [GeV/m]')
	pl.ylabel('Jitter $\sigma_x^2$ [$\mu$m$^2$]')
	ax4 = pl.subplot(2,2,4, sharex=ax1)
	pl.plot(self.qd0ffstrength, self.ipbJittery**2, 'b.', label='IPB')
	pl.plot(vfit,d2*vfit**2+e2*vfit+f2,'r-', label='Fit') 
	#pl.plot(vfit, Murnaghan(murnpars4,vfit))
	ax4.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.legend(loc=0, numpoints=1)
	pl.xlabel('QD0FF Strength [GeV/m]')
	pl.ylabel('Jitter $\sigma_y^2$ [$\mu$m$^2$]')
	pl.subplots_adjust(0.13, 0.10, 0.95, 0.93, 0.30, 0.30)

	pl.figure(14)
	pl.clf()
	ax1 = pl.subplot(1,2,1)
	pl.plot(self.qd0ffstrength, self.ipaJittery**2, 'b.', label='IPA')
	pl.plot(vfit,d1*vfit**2+e1*vfit+f1,'r-', label='Fit') 
	#pl.plot(vfit, Murnaghan(murnpars3,vfit))
	ax1.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.legend(loc=0, numpoints=1)
	pl.xlabel('QD0FF Strength [GeV/m]')
	pl.ylabel('Jitter $\sigma_y^2$ [$\mu$m$^2$]')
	ax2 = pl.subplot(1,2,2, sharex=ax1)
	pl.plot(self.qd0ffstrength, self.ipbJittery**2, 'b.', label='IPB')
	pl.plot(vfit,d2*vfit**2+e2*vfit+f2,'r-', label='Fit') 
	#pl.plot(vfit, Murnaghan(murnpars4,vfit))
	ax2.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.legend(loc=0, numpoints=1)
	pl.xlabel('QD0FF Strength [GeV/m]')
	pl.ylabel('Jitter $\sigma_y^2$ [$\mu$m$^2$]')
	pl.subplots_adjust(0.13, 0.10, 0.93, 0.93, 0.30, 0.20)


	pl.figure(10)
	pl.clf()
	ax1 = pl.subplot(1,2,1)
	pl.errorbar(self.qd0ffstrength, self.ipaMeanx, yerr=self.ipaJitterx, fmt='-o',ecolor='b', label='IPA $x$')
	pl.errorbar(self.qd0ffstrength, self.ipaMeany, yerr=self.ipaJittery, fmt='-o',ecolor='g', label='IPA $y$')
	ax1.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.legend(loc=0, numpoints=1)
	pl.grid()
	#pl.plot(self.qd0ffstrength, self.ipbMeanx, 'b*')
	pl.xlabel('QD0FF Strength [GeV/m]')
	pl.ylabel('IPA position $x/y$ [$\mu$m]')

	ax2 = pl.subplot(1,2,2, sharex=ax1)
	pl.errorbar(self.qd0ffstrength, self.ipbMeanx, yerr=self.ipbJitterx, fmt='-o',ecolor='b', label='IPB $x$')
	pl.errorbar(self.qd0ffstrength, self.ipbMeany, yerr=self.ipbJittery, fmt='-o',ecolor='g', label='IPB $y$')
	ax2.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.legend(loc=0, numpoints=1)
	pl.grid()
	#pl.plot(self.qd0ffstrength, self.ipbMeanx, 'b*')
	pl.xlabel('QD0FF Strength [GeV/m]')
	pl.ylabel('IPB position $x/y$ [$\mu$m]')
	pl.subplots_adjust(0.10, 0.10, 0.93, 0.93, 0.30, 0.20)

	pl.figure(6)
	pl.clf()
	ax1 = pl.subplot(2,2,1)
	pl.errorbar(self.qd0ffstrength, self.ipaMeanx, yerr=self.ipaJitterx, fmt='-o',ecolor='b')
	ax1.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.grid()
	#pl.plot(self.qd0ffstrength, self.ipbMeanx, 'b*')
	pl.xlabel('QD0FF Strength [GeV/m]')
	pl.ylabel('IPA position $x$ [$\mu$m]')

	ax2 = pl.subplot(2,2,2)
	pl.errorbar(self.qd0ffstrength, self.ipaMeany, yerr=self.ipaJittery, fmt='-o',ecolor='b')
	ax2.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.grid()
	#pl.plot(self.qd0ffstrength, self.ipbMeany, 'b*')
	pl.xlabel('QD0FF Strength [GeV/m]')
	pl.ylabel('IPA position $y$ [$\mu$m]')

	ax3 = pl.subplot(2,2,3)
	pl.errorbar(self.qd0ffstrength, self.ipbMeanx, yerr=self.ipbJitterx, fmt='-o',ecolor='b')
	ax3.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.grid()
	#pl.plot(self.qd0ffstrength, self.ipbMeanx, 'b*')
	pl.xlabel('QD0FF Strength [GeV/m]')
	pl.ylabel('IPB position $x$ [$\mu$m]')

	ax4 = pl.subplot(2,2,4)
	pl.errorbar(self.qd0ffstrength, self.ipbMeany, yerr=self.ipbJittery, fmt='-o',ecolor='b')
	ax4.xaxis.set_major_locator(pl.MaxNLocator(5))
	pl.grid()
	#pl.plot(self.qd0ffstrength, self.ipbMeany, 'b*')
	pl.xlabel('QD0FF Strength [GeV/m]')
	pl.ylabel('IPB position $y$ [$\mu$m]')

	pl.subplots_adjust(0.12, 0.10, 0.95, 0.95, 0.30, 0.30)


class qd0ffMoverScan :
    def __init__(self, fileList='files/qd0ffMoverScan.txt') :

	dir = 'simData/QD0FFMoverScan/'
	#dir = 'simData/QD0FFMoverScan_Coupling0.8_doubleJitter_Roll3000_1umResol_Offset200_ipb/'
	self.qd0ffMover = pl.array([-250, -200, -150, -100, -50, 0, 50, 100, 150, 200, 250])
	#self.qd0ffMover = pl.array([-450, -400, -350, -300, -250, -200, -150, -100, -50, 0])
	self.fileList = fileList
	f = open(self.fileList)
	self.ipaJitterx = []
	self.ipaJittery = []
	self.ipaMeanx = []
	self.ipaMeany = []

	self.ipbJitterx = []
	self.ipbJittery = []
	self.ipbMeanx = []
	self.ipbMeany = []

	for l in f :
	    t = l.split()
	    if t [0][0] =='#' :
		continue
	    c = t[1]
	    file = dir+t[0]

	    q = orbitStudy(file, c)
	    ipaJitterx = q.fx[:,-3].std()
	    ipaJittery = q.fy[:,-3].std()
	    ipaMeanx   = q.fx[:,-3].mean()
	    ipaMeany   = q.fy[:,-3].mean()

	    ipbJitterx = q.fx[:,-2].std()
	    ipbJittery = q.fy[:,-2].std()
	    ipbMeanx   = q.fx[:,-2].mean()
	    ipbMeany   = q.fy[:,-2].mean()

	    self.ipaJitterx.append(ipaJitterx)
	    self.ipaJittery.append(ipaJittery)
	    self.ipaMeanx.append(ipaMeanx)
	    self.ipaMeany.append(ipaMeany)

	    self.ipbJitterx.append(ipbJitterx)
	    self.ipbJittery.append(ipbJittery)
	    self.ipbMeanx.append(ipbMeanx)
	    self.ipbMeany.append(ipbMeany)

	self.ipaJitterx = pl.array(self.ipaJitterx)
	self.ipaJittery = pl.array(self.ipaJittery)
	self.ipaMeanx   = pl.array(self.ipaMeanx)
	self.ipaMeany   = pl.array(self.ipaMeany)

	self.ipbJitterx = pl.array(self.ipbJitterx)
	self.ipbJittery = pl.array(self.ipbJittery)
	self.ipbMeanx   = pl.array(self.ipbMeanx)
	self.ipbMeany   = pl.array(self.ipbMeany)

	self.controlPlot()

    def controlPlot(self) :
	pl.figure(8)
	pl.clf()
	pl.subplot(1,2,1)
	pl.plot(self.qd0ffMover, self.ipbJitterx, 'b*')
	pl.xlabel('QD0FF mover position [$\mu$m]')
	pl.ylabel('IPB jitter $x$ [$\mu$m]')
	pl.subplot(1,2,2)
	pl.plot(self.qd0ffMover, self.ipbJittery, 'b*')
	pl.xlabel('QD0FF mover position [$\mu$m]')
	pl.ylabel('IPB jitter $y$ [$\mu$m]')

	pl.figure(9)
	pl.clf()
	pl.subplot(2,2,1)
	pl.errorbar(self.qd0ffMover, self.ipaMeanx, yerr=self.ipaJitterx, fmt='-*',ecolor='b')
	pl.grid()
	#pl.plot(self.qd0ffstrength, self.ipbMeanx, 'b*')
	pl.xlabel('QD0FF mover position [$\mu$m]')
	pl.ylabel('IPA position $x$ [$\mu$m]')
	pl.subplot(2,2,2)
	pl.errorbar(self.qd0ffMover, self.ipaMeany, yerr=self.ipaJittery, fmt='-*',ecolor='b')
	pl.grid()
	#pl.plot(self.qd0ffstrength, self.ipbMeany, 'b*')
	pl.xlabel('QD0FF mover position [$\mu$m]')
	pl.ylabel('IPA position $y$ [$\mu$m]')
	pl.subplot(2,2,3)
	pl.errorbar(self.qd0ffMover, self.ipbMeanx, yerr=self.ipbJitterx, fmt='-*',ecolor='b')
	pl.grid()
	#pl.plot(self.qd0ffstrength, self.ipbMeanx, 'b*')
	pl.xlabel('QD0FF mover position [$\mu$m]')
	pl.ylabel('IPB position $x$ [$\mu$m]')
	pl.subplot(2,2,4)
	pl.errorbar(self.qd0ffMover, self.ipbMeany, yerr=self.ipbJittery, fmt='-*',ecolor='b')
	pl.grid()
	#pl.plot(self.qd0ffstrength, self.ipbMeany, 'b*')
	pl.xlabel('QD0FF mover position [$\mu$m]')
	pl.ylabel('IPB position $y$ [$\mu$m]')
	pl.subplots_adjust(0.12, 0.10, 0.95, 0.95, 0.30, 0.30)
	

class kickerScan :
    def __init__(self, fileList='files/perfectBeamlineWithSextupoleList.txt') :

	dir = 'simData/perfectBeamlineWithSextupole/'
	self.fileList = fileList
	f = open(self.fileList)
	for l in f :
	    t = l.split()
	    if t [0][0] =='#' :
		continue
	    c = t[1]
	    file = dir+t[0]

	    self.k = orbitStudy(file, c)

class ipfeedback :
    def __init__(self, directory="../output/", color='b', mb = 2) :

	self.directory = directory
        self.color = color
	self.mb = mb

	bunch1x     = directory+'bpmreadingsx.dat'
	bunch1y     = directory+'bpmreadingsy.dat'
	bunch1xp    = directory+'bpmreadingsxp.dat'
	bunch1yp    = directory+'bpmreadingsyp.dat'

	self.fx1  = pl.np.loadtxt(bunch1x)
	self.fy1  = pl.np.loadtxt(bunch1y)

	bunch2x     = directory+'bpmreadingsx2.dat'
	bunch2y     = directory+'bpmreadingsy2.dat'
	bunch2xp    = directory+'bpmreadingsxp2.dat'
	bunch2yp    = directory+'bpmreadingsyp2.dat'

	self.fx2  = pl.np.loadtxt(bunch2x)
	self.fy2  = pl.np.loadtxt(bunch2y)

	if mb == 3 :
	    bunch3x     = directory+'bpmreadingsx3.dat'
	    bunch3y     = directory+'bpmreadingsy3.dat'
	    bunch3xp    = directory+'bpmreadingsxp3.dat'
	    bunch3yp    = directory+'bpmreadingsyp3.dat'

	    self.fx3  = pl.np.loadtxt(bunch3x)
	    self.fy3  = pl.np.loadtxt(bunch3y)

	self.m = orbitStudy(self.directory, self.color)

	print 'IPA 1st, 2nd bunch correlation :', pl.corrcoef(self.fy1[:,-3], self.fy2[:,-3])[0][1]
	print 'IPB 1st, 2nd bunch correlation :', pl.corrcoef(self.fy1[:,-2], self.fy2[:,-2])[0][1]

	if mb == 3 :
            print 'IPA 1st, 3rd bunch correlation :', pl.corrcoef(self.fy1[:,-3], self.fy3[:,-3])[0][1]
            print 'IPB 1st, 3rd bunch correlation :', pl.corrcoef(self.fy1[:,-2], self.fy3[:,-2])[0][1]

	self.makePlot()

    def makePlot(self) :
	pl.figure(5)
	pl.clf()
	pl.subplot(2,1,1)
	pl.plot(self.m.s, self.fx1.transpose(), 'b')
	pl.plot(self.m.s, self.fx2.transpose(), 'g')
	#pl.plot(self.m.s, self.fx3.transpose(), 'r')
	pl.xlim(6, self.end)
	pl.xlabel("$s$ [m]")
	pl.ylabel("Horizontal Orbit [$\\mu$m]")
	pl.subplot(2,1,2)
	#pl.plot(self.f.transpose(), '+-')
	pl.plot(self.m.s, self.fy1.transpose(), 'b')
	pl.plot(self.m.s, self.fy2.transpose(), 'g')
	#pl.plot(self.m.s, self.fy3.transpose(), 'r')
	pl.xlim(6, self.end)
	pl.xlabel("$s$ [m]")
	pl.ylabel("Vertical Orbit [$\\mu$m]")

	pl.figure(16)
	pl.clf()
	pl.subplot(2,2,1)
	pl.hist(self.fx1[:,52],bins=10,histtype='step', label='IPA $x$, FB off')
	pl.hist(self.fx2[:,52],bins=10,histtype='step', label='IPA $x$, FB on')
	pl.ylim(0,50)
	pl.legend(loc=1, numpoints=1, prop={'size':14})
	pl.subplot(2,2,2)
	pl.hist(self.fy1[:,52],bins=10,histtype='step', label='IPA $y$, FB off')
	pl.hist(self.fy2[:,52],bins=10,histtype='step', label='IPA $y$, FB on')
	pl.ylim(0,50)
	pl.legend(loc=1, numpoints=1, prop={'size':14})
	pl.subplot(2,2,3)
	pl.hist(self.fx1[:,53],bins=10,histtype='step', label='IPB $x$, FB off')
	pl.hist(self.fx2[:,53],bins=10,histtype='step', label='IPB $x$, FB on')
	pl.ylim(0,50)
	pl.legend(loc=1, numpoints=1, prop={'size':14})
	pl.subplot(2,2,4)
	pl.hist(self.fy1[:,53],bins=10,histtype='step', label='IPB $y$, FB off')
	pl.hist(self.fy2[:,53],bins=10,histtype='step', label='IPB $y$, FB on')
	pl.ylim(0,50)
	pl.legend(loc=1, numpoints=1, prop={'size':14})
	pl.subplots_adjust(0.10,0.10,0.93,0.93,0.25,0.25)


class orbitStudy :   
    def __init__(self, directory="../output/", color='b', beamlineVersion="5.2", begin=None, end=None, firstInstance=True, silent=False, signFlip=False) :

        self.directory = directory
        self.color     = color
        self.beamlineVersion = beamlineVersion
        self.begin     = begin # begin pulse
        self.end       = end   # end pulse
        self.fileNamex      = 'bpmreadingsx.dat'
        self.fileNamey      = 'bpmreadingsy.dat'
        self.fileNamexp     = 'bpmreadingsxp.dat'
        self.fileNameyp     = 'bpmreadingsyp.dat'
        # firstInstance check for overlaying plots
        self.firstInstance  = firstInstance
        self.silent         = silent
        self.signFlip       = signFlip
        
        # v 5.1
        if self.beamlineVersion == "5.1":
            self.s = pl.array([6.3001,8.4134,10.4120,11.3951,15.7006,16.4197,18.4303,
                           21.8702,24.1287,26.0076,28.4204,28.9454,30.3954,
                           31.3234,31.8454,32.9138,33.9493,34.2253,34.9848,
                           35.9832,37.4332,39.8157,41.2604,45.0217,49.0184,52.7797,
                           54.5177,56.0177,56.8217,57.5177,59.0177,60.6177,62.1177,
                           63.0177,64.3177,64.9086,65.4377,67.2377,69.1377,
                           71.0378,72.8378,73.4286,73.9578,75.2578,75.8486,76.3778,
                           79.1779,80.8780,82.5780,87.1850,88.5500,90.0720,
                        90.3478,90.4288,91.1990])
            self.bpmNames = (["MB2X","MQF1X","MQD2X","MQF3X","MQF4X","MQD5X","MQF6X","MQF7X","MQD8X","MQF9X","FONTP1","MQD10X","MQF11X","FONTP2","MQD12X","MQF13X","MQD14X","FONTP3","MQF15X","MQD16X","MQF17X","MQD18X","MQF19X","MQD20X","MQF21X","MQM16FF","MQM15FF","MQM14FF","MFB2FF","MQM13FF","MQM12FF","MQM11FF","MQD10BFF","MQD10AFF","MQF9BFF","MSF6FF","MQF9AFF","MQD8FF","MQF7FF","MQD6FF","MQF5BFF","MSF5FF","MQF5AFF","MQD4BFF","MSD4FF","MQD4AFF","MQF3FF","MQD2BFF","MQD2AFF","MSF1FF","MSD0FF","MPREIP","IPBPMA","IPBPMB","POSTIP"])

        elif self.beamlineVersion == "5.2" :
            # v.5.2
            self.s = pl.array([5.1001,    7.2134,    9.2120,   10.1951,   14.5006,   15.2197,   17.2303,   20.6702,   22.9287,   24.8076,   27.2204,   27.7454,   29.1954,   30.1234,   30.6454,   31.7138,   32.7493,   33.0253,   33.7848,   34.7832,   36.2332,   38.6157,   40.0604,   43.8217,   47.8184,     51.5797,   53.3177,   54.8177,   55.6217,   56.3177,   57.8177,   59.4177, 60.9177,  61.8177,   63.1177,    63.7086,   64.2377,   66.0377,   67.9377,   69.8378,   71.6378,   72.2286,   72.7578,   74.0578,   74.6486,   75.1778,   77.9779,   79.6780,   81.3780,   85.9850,   87.3500,   88.8720,   89.1478,   89.2288,   89.3872,   89.9240])

            self.bpmNames = (["MB2X","MQF1X","MQD2X","MQF3X","MQF4X","MQD5X","MQF6X","MQF7X","MQD8X","MQF9X","FONTP1","MQD10X","MQF11X","FONTP2","MQD12X","MQF13X","MQD14X","FONTP3","MQF15X","MQD16X","MQF17X","MQD18X","MQF19X","MQD20X","MQF21X","MQM16FF","MQM15FF","MQM14FF","MFB2FF","MQM13FF","MQM12FF","MQM11FF","MQD10BFF","MQD10AFF","MQF9BFF","MSF6FF","MQF9AFF","MQD8FF","MQF7FF","MQD6FF","MQF5BFF","MSF5FF","MQF5AFF","MQD4BFF","MSD4FF","MQD4AFF","MQF3FF","MQD2BFF","MQD2AFF","MSF1FF","MSD0FF","MPREIP","IPBPMA","IPBPMB","IPBPMC","MPIP"])
        
        self.nrBpms=len(self.bpmNames)

        font_s = (28.420, 31.323, 34.225)

        self.readFile()
        #self.controlPlot()
        #self.ipac2013ControlPlot()

    def readFile(self) :
        self.fx  = pl.np.loadtxt(self.directory + self.fileNamex)
        self.fy  = pl.np.loadtxt(self.directory + self.fileNamey)
        if self.signFlip:
            self.fx = -self.fx
            self.fy = -self.fy
        
        self.nrPulses = len(self.fy)
        if not self.silent:
            print 'Number of Pulses :', self.nrPulses
        #self.fxp = pl.np.loadtxt(self.directory + self.fileNamexp)
        #self.fyp = pl.np.loadtxt(self.directory + self.fileNameyp)

        if not self.silent:
            #print pl.corrcoef(self.fy[:,11], self.fy[:,-3])[0][1]
            print 'MFB2FF and IPA correlation :', pl.corrcoef(self.fy[:,28], self.fy[:,52])[0][1]
            print 'MFB2FF and IPB correlation :', pl.corrcoef(self.fy[:,28], self.fy[:,53])[0][1]
            print 'IPA and B correlation :', pl.corrcoef(self.fy[:,-3], self.fy[:,-2])[0][1]
            print 'P1/P2/P3 and IP A correlation :', pl.corrcoef(self.fy[:,10], self.fy[:,-3])[0][1], pl.corrcoef(self.fy[:,13], self.fy[:,-3])[0][1], pl.corrcoef(self.fy[:,17], self.fy[:,-3])[0][1]
            #print 'P2 and IP A correlation :', pl.corrcoef(self.fy[:,13], self.fy[:,-3])[0][1]
            #print 'P3 and IP A correlation :', pl.corrcoef(self.fy[:,17], self.fy[:,-3])[0][1]
            print 'P1/P2/P3 and IP B correlation :', pl.corrcoef(self.fy[:,10], self.fy[:,-2])[0][1], pl.corrcoef(self.fy[:,13], self.fy[:,-2])[0][1], pl.corrcoef(self.fy[:,17], self.fy[:,-2])[0][1]
            #print 'P2 and IP B correlation :', pl.corrcoef(self.fy[:,13], self.fy[:,-2])[0][1]
            #print 'P3 and IP B correlation :', pl.corrcoef(self.fy[:,17], self.fy[:,-2])[0][1]

            print 'Jitter P1/P2/P3/IPA/IPB x :', self.fx[:,10].std(), self.fx[:,13].std(), self.fx[:,17].std(), self.fx[:,52].std(), self.fx[:,53].std()
            print 'Jitter P1/P2/P3/IPA/IPB y :', self.fy[:,10].std(), self.fy[:,13].std(), self.fy[:,17].std(), self.fy[:,52].std(), self.fy[:,53].std()

    def ipac2013ControlPlot(self) :
        corre1 = pl.corrcoef(self.fy[:,28], self.fy[:,52])[0][1]
        corre2 = pl.corrcoef(self.fy[:,28], self.fy[:,53])[0][1]
        pl.figure(2013)
        #pl.clf()
        ax1 = pl.subplot(2,1,1)
        pl.plot(self.fy[:,28], self.fy[:,52], '.', label='Correlation coeff. = %0.00000009f'%corre1)
        ax1.xaxis.set_major_locator(pl.MaxNLocator(5))
        ax1.yaxis.set_major_locator(pl.MaxNLocator(5))
        #pl.legend(loc=1, numpoints=1, prop={'size':14})
        pl.xlabel('MFB2FF $y$ [$\\mu$m]')
        pl.ylabel('IPA $y$ [$\\mu$m]')
        ax2 = pl.subplot(2,1,2)
        pl.plot(self.fy[:,28], self.fy[:,53], '.',label='Correlation coeff. = %0.000000009f'%corre2)
        ax2.xaxis.set_major_locator(pl.MaxNLocator(5))
        ax2.yaxis.set_major_locator(pl.MaxNLocator(5))
        #pl.legend(loc=1, numpoints=1, prop={'size':14})
        pl.xlabel('MFB2FF $y$ [$\\mu$m]')
        pl.ylabel('IPB $y$ [$\\mu$m]')
        pl.subplots_adjust(0.12, 0.10, 0.94, 0.94, 0.20, 0.30)

    def controlPlot(self) :
        pl.figure(2)
        #pl.clf()
        ax1 = pl.subplot(2,1,1)
        if len(self.s)==len(self.fx.transpose()) :
            pl.plot(self.s, self.fx.transpose()[:,self.begin:self.end],self.color)
            #pl.plot(self.s, self.fxp.transpose(), 'r')
            pl.xlim(6, 92)
            pl.xlabel("$s$ [m]")
        else :
            pl.plot(self.fx.transpose()[:,self.begin:self.end], self.color)
            pl.xlabel("BPM number")
        pl.ylabel("Horizontal Orbit [$\\mu$m]")

        ax2 = pl.subplot(2,1,2, sharex=ax1)
        #pl.plot(self.f.transpose(), '+-')
        if len(self.s)==len(self.fy.transpose()) :
            pl.plot(self.s, self.fy.transpose()[:,self.begin:self.end],self.color)
            #pl.plot(self.s, self.fyp.transpose(), 'r')
            pl.xlim(6, 92)
            pl.xlabel("$s$ [m]")
        else :
            pl.plot(self.fy.transpose()[:,self.begin:self.end], self.color)
            pl.xlabel("BPM number")
        pl.ylabel("Vertical Orbit [$\\mu$m]")

        pl.savefig(self.directory+"orbit.png")

        pl.figure(3)
        #pl.clf()
        pl.plot(self.fy[self.begin:self.end,52], self.fy[self.begin:self.end,53], self.color+'+')
        pl.xlabel('IPA y [$\mu$m]')
        pl.ylabel('IPB y [$\mu$m]')

        pl.figure(4)
        pl.ylabel('IPB y [$\mu$m]')
        pl.hist(self.fy[self.begin:self.end,53], color=self.color)

        pl.figure(5)
        ax1 = pl.subplot(2,1,1)
        if len(self.s)==len(self.fx.transpose()) :
            pl.plot(self.s, self.fx.transpose()[:,-1],self.color)
            #pl.plot(self.s, self.fxp.transpose(), 'r')
            pl.xlim(6, 92)
            pl.xlabel("$s$ [m]")
        else :
            pl.plot(self.fx.transpose()[:,-1], self.color)
            pl.xlabel("BPM number")
        pl.ylabel("Horizontal Orbit [$\\mu$m]")

        ax2 = pl.subplot(2,1,2, sharex=ax1)
        #pl.plot(self.f.transpose(), '+-')
        if len(self.s)==len(self.fy.transpose()) :
            pl.plot(self.s, self.fy.transpose()[:,-1],self.color)
            #pl.plot(self.s, self.fyp.transpose(), 'r')
            pl.xlim(6, 92)
            pl.xlabel("$s$ [m]")
        else :
            pl.plot(self.fy.transpose()[:,-1], self.color)
            pl.xlabel("BPM number")
        pl.ylabel("Vertical Orbit [$\\mu$m]")

        pl.savefig(self.directory+"lastorbit.png")


    def MREFscan(self, factor=1.0) :
        mrefpos = [-4,-3,-2,-1,0,1,2,3,4,5]
        mrefpos = mrefpos[0:self.nrPulses]
        pl.figure(10)
        #pl.clf()
        # QD2AFF
        pl.plot(mrefpos,self.fy[:,50]/factor, label=factor);
        pl.plot(mrefpos,self.fy[:,50]/factor,'o');
        pl.legend()
        pl.xlabel("MREF position [mm]")
        pl.ylabel("QD2AFF bpm reading [um]")
        pl.title("Simulation")
        pl.savefig(self.directory+"QD2AFF.png")
        pl.figure(11)
        pl.plot(mrefpos,self.fy[:,49]/factor,linestyle='-',marker='o', label=factor);
        pl.legend()
        pl.xlabel("MREF position [mm]")
        pl.ylabel("QD2BFF bpm reading [um]")
        pl.title("Simulation")
        pl.savefig(self.directory+"QD2BFF.png")
    def SF6FFscan(self) :
        pl.figure(10)
        #pl.clf()
        # QD2AFF
        sf6ffpos = [-1.5,-1.2,-0.9,-0.6,-0.3,0.0,0.3,0.6,0.9,1.2,1.5]
        pl.plot(sf6ffpos,self.fx[:,50],linestyle='-',marker='o');
        pl.xlabel("SF6FF x-position [mm]")
        pl.ylabel("QD2AFF bpm reading [um]")
        pl.title("Simulation")
        pl.savefig(self.directory+"SFF6FFxscan.png")

        pl.figure(11)
        pl.plot(sf6ffpos,self.fy[:,48],linestyle='-',marker='o');
        pl.xlabel("SF6FF y-position [mm]")
        pl.ylabel("QF3FF bpm reading [um]")
        pl.title("Simulation")
        pl.savefig(self.directory+"SFF6FFyscan.png")
    def SD4FFscan(self) :
        pl.figure(10)
        #pl.clf()
        # QD2AFF
        sd4ffpos = [-1.5,-1.2,-0.9,-0.6,-0.3,0.0,0.3,0.6,0.9,1.2,1.5]
        pl.plot(sd4ffpos,self.fy[:,50],linestyle='-',marker='o');
        pl.xlabel("SD4FF y-position [mm]")
        pl.ylabel("QD2AFF bpm reading [um]")
        pl.title("Simulation")
        pl.savefig(self.directory+"SFD4FFyscan.png")
    def BeamlineResponseMREFFit(self,plotlabel="Simulation"):
        # counting so that fits to measurement
        self.label = plotlabel
        count=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57]
        self.begin = 33
        #begin = 47
        self.end = 49
        # better to get from simulation file!
        mrefpos = [-4,-3,-2,-1,0,1,2,3,4,5]
        #mrefpos = [-1.5,-1.2,-0.9,-0.6,-0.3,0.0,0.3,0.6,0.9,1.2,1.5]
        mrefpos = mrefpos[0:self.nrPulses]

        nr_bpms=max(np.shape(self.fy[:,:]))
        self.names = ["MQF1X", "MQD2X", "MQF3X", "MQF4X", "MQD5X", "MQF6X", "MQF7X", "MQD8X", "MQF9X", "QD10X", "QF11X", "QD12X", "MQF13X", "MQD14X", "MQF15X", "QD16X", "QF17X", "QD18X", "QF19X", "QD20X", "QF21X", "IPT1", "IPT2", "IPT3", "IPT4", "QM16FF", "QM15FF", "QM14FF", "FB2FF", "QM13FF", "QM12FF", "QM11FF", "QD10BFF", "QD10AFF", "QF9BFF", "",#"SF6FF",
         "QF9AFF", "QD8FF", "QF7FF", "QD6FF", "QF5BFF", "",#"SF5FF",
"QF5AFF", "QD4BFF", "", #"SD4FF",
"QD4AFF", "QF3FF", "QD2BFF", "QD2AFF", "SF1FF", "QF1FF", "SD0FF", "QD0FF", "PREIP", "IPA", "IPB", "M-PIP"]
        self.s = pl.array([8.4134,10.4120,11.3951,15.7006,16.4197,18.4303, #QF6X
                           21.8702,24.1287,26.0076,28.9454,30.3954, #QF11X
                           31.8454,32.9138,33.9493,34.9848, #QF15X
                           35.9832,37.4332,39.8157,41.2604,45.0217,49.0184, #QM15FF
                           49.1,49.2,49.3,49.4, #IPTs guess
                           52.7797, # QM16FF
                           54.5177,56.0177,56.8217,57.5177,59.0177,60.6177,62.1177, #QM10BFF
                           63.0177,64.3177,64.9086,65.4377,67.2377,69.1377,
                           71.0378,72.8378,73.4286,73.9578,75.2578,75.8486,76.3778,
                           79.1779,80.8780,82.5780,87.1850,
                           87.3, #QF1 guess
                           88.5500, #SDOFF
                           88.7, #QD0, guess
                           90.0720,90.3478,90.4288,91.1990]) #IPBPMs
        
        residual_fit=np.zeros((nr_bpms,1))
        self.ar=np.zeros((nr_bpms,1))
        self.br=np.zeros((nr_bpms,1))

        pl.figure(19)
        #pl.clf()
        for i in range(0,nr_bpms):
            (self.ar[i],self.br[i]),residual_fit[i],a,(b,c),d=np.polyfit(mrefpos,self.fx[:,i],1,None,True)
        pl.title("Orbit response - Simulation - x")
        # shift by 1 to adapt to real data analysis!! MREF cavity vs 2 BPM ref cavities
        #pl.errorbar(count[self.begin:self.end],ar[self.begin:self.end,0],sqrt(residual_fit[self.begin:self.end,0]/nr_meas),label=label1[k],color=colors[k])
        pl.plot(count[self.begin:self.end],self.ar[self.begin:self.end,0],linestyle='-',marker='o')
        pl.xlabel("BPM nr")
        pl.ylabel("BPM reading (orbit subtracted) [um]/mover pos. [mm]")
        pl.savefig(self.directory+"MREFOrbitResponse_x.png")

        fig20 = pl.figure(20) #, figsize=(12,8))
        #ax = pl.subplot(1,1,1)
        pl.subplots_adjust(top=0.79)
        pl.subplots_adjust(bottom=0.12)

        #pl.clf()
        for i in range(0,nr_bpms):
            (self.ar[i],self.br[i]),residual_fit[i],a,(b,c),d=np.polyfit(mrefpos,self.fy[:,i],1,None,True)
        #pl.title("Orbit response")
        # shift by 1 to adapt to real data analysis!! MREF cavity vs 2 BPM ref cavities
        #pl.errorbar(count[self.begin:self.end],ar[self.begin:self.end,0],sqrt(residual_fit[self.begin:self.end,0]/nr_meas),label=label1[k],color=colors[k])
        # ticks with BPM names equidistant
        #pl.xticks(range(self.begin,self.end),self.names[self.begin:self.end],rotation=50)
        
        if self.firstInstance:
            #pl.xlabel("BPM name")
            pl.xlabel("s [m]")
            pl.ylabel("$\Delta y$ [$\mu$m]")
            fig20ax = fig20.add_subplot(111)

            #fig20ax.xaxis.label.set_color('red')
            #fig20ax.yaxis.label.set_color('red')
            fig20ax.set_xlim(self.s[self.begin],self.s[self.end])
        
            #pl.xticks(self.s[self.begin:self.end],self.names[self.begin:self.end],rotation=50)

            # extra axis on top
            fig20ax2 = fig20ax.twiny()
            # with s distance
            fig20ax2.set_xticks(self.s[self.begin:self.end])
            fig20ax2.set_xticklabels(self.names[self.begin:self.end])#,color='r')
            for tick in fig20ax2.xaxis.iter_ticks():
                tick[0].label2On = True
                tick[0].label1On = False
                tick[0].label2.set_rotation(50)
        else:
            fig20ax2 = fig20.axes[1]

        #pl.tick_params(axis='x', which='major', labelsize=16)
        #pl.ticklabel_format(useOffset=True)
                
        #pl.ylabel("Differential orbit response [$\mu$m]")
        #pl.ylabel("BPM reading (orbit subtracted) [um]/mover pos. [mm]")
        #pl.plot(count[self.begin:self.end],self.ar[self.begin:self.end,0],linestyle='-',marker='o',label=self.label)

        #if self.firstInstance:
        #    self.splot = self.s * self.s + self.s
        #else:
        #    self.splot = self.s
            
        fig20ax2.plot(self.s[self.begin:self.end],self.ar[self.begin:self.end,0],linestyle='-',marker='o',label=self.label)
        
        #dummy=1
        #for tick in ax.xaxis.get_major_ticks():
        #    if (dummy==1):
        #        tick.verticalalignment='top'
        #        dummy=-1
        #    else:
        #        tick.verticalalignment='bottom'
        #        dummy=1
        pl.legend(loc=0)

        pl.savefig(self.directory+"MREFOrbitResponse.png")

        # to overlap with measurement plot
        pl.figure(21)
        for i in range(0,nr_bpms):
            (self.ar[i],self.br[i]),residual_fit[i],a,(b,c),d=np.polyfit(mrefpos,self.fy[:,i],1,None,True)
        pl.plot(count[self.begin-2:self.end-2],self.ar[self.begin:self.end,0],linestyle='-',marker='o',label=self.label, color='k')
        pl.legend()

    def SVD(self, svdCut = 1e-4, IPBPMs=True, directorySVD='None'):
        self.svdCut = svdCut

        if directorySVD=='None':
            fx_svd = self.fx
            fy_svd = self.fy
        else:
            fx_svd = pl.np.loadtxt(directorySVD + self.fileNamex)
            fy_svd = pl.np.loadtxt(directorySVD + self.fileNamey)

        # SVD
        if IPBPMs:
            fxMod = fx_svd
            fyMod = fy_svd

        else:
            fxMod = fx_svd[:,:-4]
            fyMod = fy_svd[:,:-4]

        self.Ux,self.sx,self.Vx = np.linalg.svd(fxMod)
        self.Uy,self.sy,self.Vy = np.linalg.svd(fyMod)
        
        self.sxInv = zeros((max(pl.shape(self.Ux)),max(pl.shape(self.Vx))))
        self.syInv = zeros((max(pl.shape(self.Uy)),max(pl.shape(self.Vy))))

        for i in range(max(pl.shape(self.sx))):
            # skip too small values for stability
            if (self.sx[i] > self.sx[0]*self.svdCut):
                self.sxInv[i,i]=1./self.sx[i]

        for i in range(max(pl.shape(self.sy))):
            if (self.sy[i] > self.sy[0]*self.svdCut):
                self.syInv[i,i]=1./self.sy[i]

        # pseudo-inverse 
        self.fxInv = np.dot(np.dot((self.Vx).T,(self.sxInv).T),(self.Ux).T)
        self.fyInv = np.dot(np.dot((self.Vy).T,(self.syInv).T),(self.Uy).T)

        # calculate IP position and angle 
        #self.ipx  = self.fx[:,52] + 1.8667*(self.fx[:,53]-self.fx[:,52])
        #self.ipy  = self.fy[:,52] + 1.8667*(self.fy[:,53]-self.fy[:,52])

        # or rather get it from simulation? -> yes since bpms might have noise
        
        beamIP = beam(fileName=directorySVD+"meas_station_ip.dat")

        ipx = np.zeros((self.nrPulses,1))
        ipxp = np.zeros((self.nrPulses,1))
        ipy = np.zeros((self.nrPulses,1))
        ipyp = np.zeros((self.nrPulses,1))

        ipx[:,0] = beamIP.x
        ipy[:,0] = beamIP.y
        ipxp[:,0] = beamIP.xp
        ipyp[:,0] = beamIP.yp

        ipXXp = np.hstack((ipx,ipxp))
        ipYYp = np.hstack((ipy,ipyp))

        self.corrx = np.dot(self.fxInv, ipXXp)
        self.corry = np.dot(self.fyInv, ipYYp)

    def IPPrediction(self):

        # don't take IPBPMs into account
        self.IPBPMs = False
        
        #closely linked to SVD function
        # use no BPM resolution data for SVD
        #self.SVD(IPBPMs=False, directorySVD=self.directory + "../jitter_nores/")
        # Use data with BPM resolution for SVD
        self.SVD(IPBPMs=False, directorySVD=self.directory + "../jitter_default/")
        # Use data with sextupoles off
        #self.SVD(IPBPMs=self.IPBPMs, directorySVD=self.directory + "../jitter_seed15_nores_no_sext/")

        # SVD
        if self.IPBPMs:
            self.fxMod = self.fx
            self.fyMod = self.fy

        else:
            self.fxMod = self.fx[:,:-4]
            self.fyMod = self.fy[:,:-4]

        # prediction
        self.ipxSVD = np.dot(self.fxMod,self.corrx)
        self.ipySVD = np.dot(self.fyMod,self.corry) 

        # real IP position:
        self.beamIP = beam(fileName=self.directory+"meas_station_ip.dat")
        self.ipx = np.zeros((self.nrPulses,1))
        self.ipxp = np.zeros((self.nrPulses,1))
        self.ipy = np.zeros((self.nrPulses,1))
        self.ipyp = np.zeros((self.nrPulses,1))

        self.ipx[:,0] = self.beamIP.x
        self.ipy[:,0] = self.beamIP.y
        self.ipxp[:,0] = self.beamIP.xp
        self.ipyp[:,0] = self.beamIP.yp

        self.ipXXp = np.hstack((self.ipx,self.ipxp))
        self.ipYYp = np.hstack((self.ipy,self.ipyp))

        # residual
        self.residualx = self.ipxSVD - self.ipXXp
        self.residualy = self.ipySVD - self.ipYYp

        self.residualIPx = self.residualx[:,0]
        self.residualIPy = self.residualy[:,0]
        self.residualIPxp = self.residualx[:,1]
        self.residualIPyp = self.residualy[:,1]

        ipJitterx = self.residualIPx.std()
        ipJittery = self.residualIPy.std()
        ipMeanx = self.residualIPx.mean()
        ipMeany = self.residualIPy.mean()
        ipJitterxp = self.residualIPxp.std()
        ipJitteryp = self.residualIPyp.std()
        ipMeanxp = self.residualIPxp.mean()
        ipMeanyp = self.residualIPyp.mean()

        if not self.silent:
            print "IP jitter (x,y) ", ipJitterx, " " , ipJittery 
            print "IP mean (x,y) ", ipMeanx, " " , ipMeany 

            print "IP jitter (xp,yp) ", ipJitterxp, " " , ipJitteryp 
            print "IP mean (xp,yp) ", ipMeanxp, " " , ipMeanyp

        pl.figure(100)
        pl.plot(self.ipx[:,0],self.ipxSVD[:,0],'+',label= pl.corrcoef(self.ipx[:,0],self.ipxSVD[:,0])[0][1])
        pl.xlabel("IP x [$\mu$m]")
        pl.ylabel("IP x predicted [$\mu$m]")
        pl.legend()
        pl.savefig(self.directory+"IPxSVD.pdf")

        pl.figure(101)
        pl.plot(self.ipxp[:,0],self.ipxSVD[:,1],'+',label= pl.corrcoef(self.ipxp[:,0],self.ipxSVD[:,1])[0][1])
        pl.xlabel("IP x' [$\mu$rad]")
        pl.ylabel("IP x' predicted [$\mu$rad]")
        pl.legend()
        pl.savefig(self.directory+"IPxpSVD.pdf")

        pl.figure(102)
        pl.plot(self.ipy[:,0],self.ipySVD[:,0],'+',label= pl.corrcoef(self.ipy[:,0],self.ipySVD[:,0])[0][1])
        pl.xlabel("IP y [$\mu$m]")
        pl.ylabel("IP y predicted [$\mu$m]")
        pl.legend()
        pl.savefig(self.directory+"IPySVD.pdf")

        pl.figure(103)
        pl.plot(self.ipyp[:,0],self.ipySVD[:,1],'+',label= pl.corrcoef(self.ipyp[:,0],self.ipySVD[:,1])[0][1])
        pl.xlabel("IP y' [$\mu$rad]")
        pl.ylabel("IP y' predicted [$\mu$rad]")
        pl.legend()
        pl.savefig(self.directory+"IPypSVD.pdf")

        pl.figure(104)
        pl.hist(self.ipx[:,0]-self.ipx.mean(), label="IP position (av subtr.)")
        pl.hist(self.residualIPx, label="residual")
        pl.xlabel("x [$\mu$m]")
        pl.legend()
        pl.savefig(self.directory+"IPxRes.pdf")

        pl.figure(105)
        pl.hist(self.ipy[:,0]-self.ipy.mean(),label="IP position (av subtr.)")
        pl.hist(self.residualIPy, label="residual")
        pl.xlabel("y [$\mu$m]")
        pl.legend()
        pl.savefig(self.directory+"IPyRes.pdf")

        pl.figure(106)
        pl.hist(self.ipxp[:,0]-self.ipxp.mean(),label="IP position (av subtr.)")
        pl.hist(self.residualIPxp, label="residual")
        pl.xlabel("x' [$\mu$rad]")
        pl.legend()
        pl.savefig(self.directory+"IPxpRes.pdf")

        pl.figure(107)
        pl.hist(self.ipyp[:,0]-self.ipyp.mean(),label="IP position (av subtr.)")
        pl.hist(self.residualIPyp, label="residual")
        pl.xlabel("y' [$\mu$rad]")
        pl.legend()
        pl.savefig(self.directory+"IPypRes.pdf")

class beam:
    def __init__(self,fileName="../output/meas_station_ip.dat") :

	self.fileName       = fileName
	self.time           = []
	self.x              = []
	self.xp             = []
	self.y              = []
	self.yp             = []
	self.sxx            = []
	self.sxpxp          = []
	self.syy            = []
	self.sypyp          = []
	self.szz            = []
	self.see            = []
	self.sxy            = []
	self.sxe            = []
	self.sxpe           = []
        self.ye             = []
        # more to come

        self.readFile()
        #self.controlPlot()

    def readFile(self) :
	f = open(self.fileName)
	for l in f : 
	    t = l.split()
	    if t [0] =='#' :
		continue
	    time   = t[0]
	    x      = t[1]
	    xp     = t[2]
	    y      = t[3]
	    yp     = t[4]
	    sxx    = t[5]
	    sxpxp  = t[6]
	    syy    = t[7]
	    sypyp  = t[8]
	    szz    = t[9]
	    see    = t[10]
	    sxy    = t[11]
	    sxe    = t[12]
	    sxpe   = t[13]
            ye     = t[14]

	    self.time.append(time)
	    self.x.append(x)
	    self.xp.append(xp)
	    self.y.append(y)
	    self.yp.append(yp)

	self.time           = pl.array(self.time)
	self.x              = pl.array(self.x)
	self.xp             = pl.array(self.xp)
	self.y              = pl.array(self.y)
	self.yp             = pl.array(self.yp)


        #ipaJitterx = self.residualx[:,-3].std()
        #ipaJittery = self.residualy[:,-3].std()
        #ipaMeanx   = self.residualx[:,-3].mean()
        #ipaMeany   = self.residualy[:,-3].mean()
    
        #ipbJitterx = self.residualx[:,-2].std()
        #ipbJittery = self.residualy[:,-2].std()
        #ipbMeanx   = self.residualx[:,-2].mean()
        #ipbMeany   = self.residualy[:,-2].mean()
        
        #if not self.silent:
        #print "IPA jitter (x,y) ", ipaJitterx, " " , ipaJittery 
        #print "IPB jitter (x,y) ", ipbJitterx, " " , ipbJittery 
        #print "IPA mean (x,y) ", ipaMeanx, " " , ipaMeany 
        #print "IPB mean (x,y) ", ipbMeanx, " " , ipbMeany 

        #pl.figure(100)
        #pl.hist(self.residualx[:,-3])
        #pl.xlabel("IPA reading x - residual [$\mu$m]")

        #pl.figure(101)
        #pl.hist(self.residualx[:,-2])
        #pl.xlabel("IPB reading x - residual [$\mu$m]")

        #pl.figure(102)
        #pl.hist(self.residualy[:,-3])
        #pl.xlabel("IPA reading y - residual [$\mu$m]")

        #pl.figure(103)
        #pl.hist(self.residualy[:,-2])
        #pl.xlabel("IPB reading y - residual [$\mu$m]")

        #self.ipxSVD = self.fxSVD[:,52] + 1.8667*(self.fxSVD[:,53]-self.fxSVD[:,52])
        #self.ipySVD = self.fySVD[:,52] + 1.8667*(self.fySVD[:,53]-self.fySVD[:,52])

        #self.ipxpSVD = (self.fxSVD[:,53]-self.fxSVD[:,52])/0.081
        #self.ipypSVD = (self.fySVD[:,53]-self.fySVD[:,52])/0.081

class trackingStudy:
    def __init__(self,directory="../output/", fileName="tracking.dat", label="tracking", scanValue=1, fileNameNominal="") :

        self.dir             = directory
        self.fileName        = fileName
        self.fileNameNominal = fileNameNominal
        self.label           = label
        self.scanValue       = scanValue
        self.s               = []
        self.E               = []
        self.dE              = []
        self.ex              = []
        self.ey              = []
        self.sx              = []
        self.sy              = []
        self.sz              = []        

        self.readFile()

        if self.fileNameNominal:
            self.sN              = []
            self.EN              = []
            self.dEN             = []
            self.exN             = []
            self.eyN             = []
            self.sxN             = []
            self.syN             = []
            self.szN             = []

            self.readNominalFile()
            
        #self.controlPlot()
        #self.orbitPlot()

    def readFile(self) :
        print 'read file ',self.dir+self.fileName
        f = open(self.dir+self.fileName)
        for l in f : 
            t = l.split()
            if t [0] =='#' :
                continue
            s      = t[0]
            E      = t[1]
            dE     = t[2]
            ex     = t[3]
            ey     = t[4]
            sz     = t[5]

            self.s.append(s)
            self.E.append(E)
            self.dE.append(dE)
            self.ex.append(ex)
            self.ey.append(ey)
            self.sz.append(sz)
    
            if np.size(t) > 6:
                sx = t[6]
                sy = t[7]
                self.sx.append(sx)
                self.sy.append(sy)
    
        self.s              = pl.array(self.s, dtype=float)
        self.E              = pl.array(self.E, dtype=float)
        self.dE             = pl.array(self.dE, dtype=float)
        self.ex             = pl.array(self.ex, dtype=float)
        self.ey             = pl.array(self.ey, dtype=float)
        self.sx             = pl.array(self.sx, dtype=float)
        self.sy             = pl.array(self.sy, dtype=float)
        self.sz             = pl.array(self.sz, dtype=float)
    
    def readNominalFile(self) :
        f = open(self.fileNameNominal)
        for l in f : 
            t = l.split()
            if t [0] =='#' :
                continue
            sN      = t[0]
            EN      = t[1]
            dEN     = t[2]
            exN     = t[3]
            eyN     = t[4]
            szN     = t[5]

            self.sN.append(sN)
            self.EN.append(EN)
            self.dEN.append(dEN)
            self.exN.append(exN)
            self.eyN.append(eyN)
            self.szN.append(szN)
    
            if np.size(t) > 6:
                sxN = t[6]
                syN = t[7]
                self.sxN.append(sxN)
                self.syN.append(syN)
            
        self.sN              = pl.array(self.sN, dtype=float)
        self.EN              = pl.array(self.EN, dtype=float)
        self.dEN             = pl.array(self.dEN, dtype=float)
        self.exN             = pl.array(self.exN, dtype=float)
        self.eyN             = pl.array(self.eyN, dtype=float)
        self.sxN             = pl.array(self.sxN, dtype=float)
        self.syN             = pl.array(self.syN, dtype=float)
        self.szN             = pl.array(self.szN, dtype=float)

    def controlPlot(self) :
        fig10=pl.figure(10)
        #pl.clf()
        pl.xlabel('$s$ [m]')
        pl.ylabel('Norm. emittance y [nm]')
        pl.xlim([0,93])
        #pl.xlim([0,87])
        ax10=fig10.axes[0];
        ax10.plot(self.s,100*self.ey, label=self.label)
        ax10.legend(loc="upper left")

        # this is an inset axes over the main axes
        #ax10sub = pl.axes([.4, .4, .4, .4])
        #pl.xlim([75,85])
        #pl.ylim([100,140])
        #pl.ylabel('')
        #ax10sub.plot(self.s,100*self.ey)
        # pl.setp(a, yticks=[])
        #pl.show()
        pl.savefig("emittance.png")

        pl.figure(11)
        pl.xlabel('$s$ [m]')
        #pl.xlim([0,85])
        pl.ylabel('Norm. emittance x [nm]')
        pl.plot(self.s,100*self.ex, label=self.label)
        pl.legend(loc='upper left')
        pl.savefig("emittancex.png")
        
        beamsize = 0
        if beamsize:
            pl.figure(16)
            pl.xlabel('$s$ [m]')
            #pl.xlim([0,85])
            pl.ylabel('beam size x [um]')
            pl.plot(self.s,self.sx, label=self.label)
            pl.legend()
            pl.savefig("beamx.png")

            pl.figure(17)
            pl.xlabel('$s$ [m]')
            #pl.xlim([0,85])
            pl.ylabel('beam size y [um]')
            pl.plot(self.s,self.sy, label=self.label)
            pl.legend()
            pl.savefig("beamy.png")
        
        if self.fileNameNominal:
            pl.figure(12)
            pl.xlabel('$s$ [m]')
            #pl.xlim([0,85])
            pl.ylabel('$\Delta$ $\epsilon_y$ [nm]')
            pl.plot(self.s,100*(self.ey-self.eyN), label=self.label)
            pl.legend(loc="upper left")
            pl.savefig("DemittanceY.png")

            pl.figure(121)
            pl.xlabel('$s$ [m]')
            pl.xlim([60,93])
            pl.ylabel('$\Delta$ $\epsilon_y$ [nm]')
            pl.plot(self.s,100*(self.ey-self.eyN), label=self.label)
            pl.legend(loc="upper left")
            pl.savefig("DemittanceYZoom.png")
           
            pl.figure(13)
            pl.xlabel('$s$ [m]')
            #pl.xlim([0,85])
            pl.ylabel('$\Delta$ $\epsilon_y$ / $\epsilon_y$ [\%]')
            #pl.plot(self.s,100*(self.ey-self.eyN)/self.eyN, label=self.label)
            pl.semilogy(self.s,100*(self.ey-self.eyN)/self.eyN, label=self.label)
            pl.legend(loc="upper left")
            pl.savefig("DemittanceYRel.png")

            pl.figure(131)
            pl.xlabel('$s$ [m]')
            pl.xlim([60,93])
            pl.ylabel('$\Delta$ $\epsilon_y$ / $\epsilon_y$ [\%]')
            #pl.plot(self.s,100*(self.ey-self.eyN)/self.eyN, label=self.label)
            pl.semilogy(self.s,100*(self.ey-self.eyN)/self.eyN, label=self.label)
            pl.legend(loc="lower right")
            pl.savefig("DemittanceYRelZoom.png")

            pl.figure(14)
            pl.xlabel('$s$ [m]')
            #pl.xlim([0,85])
            pl.ylabel('$\Delta$ $\epsilon_x$ [nm]')
            pl.plot(self.s,100*(self.ex-self.exN), label=self.label)
            pl.legend(loc="upper left")
            pl.savefig("DemittanceX.png")

            pl.figure(141)
            pl.xlabel('$s$ [m]')
            pl.xlim([60,93])
            pl.ylabel('$\Delta$ $\epsilon_x$ [nm]')
            pl.plot(self.s,100*(self.ex-self.exN), label=self.label)
            pl.legend(loc="upper left")
            pl.savefig("DemittanceXZoom.png")

            pl.figure(15)
            pl.xlabel('$s$ [m]')
            #pl.xlim([0,85])
            pl.ylabel('$\Delta$ $\epsilon_x$ / $\epsilon_x$ [\%]')
            pl.plot(self.s,100*(self.ex-self.exN)/self.exN, label=self.label)
            pl.legend(loc="upper left")
            pl.savefig("DemittanceXRel.png")

            pl.figure(151)
            pl.xlabel('$s$ [m]')
            pl.xlim([60,93])
            pl.ylabel('$\Delta$ $\epsilon_x$ / $\epsilon_x$ [\%]')
            pl.plot(self.s,100*(self.ex-self.exN)/self.exN, label=self.label)
            pl.legend(loc="upper left")
            pl.savefig("DemittanceXRelZoom.png")

    def orbitPlot(self, shift=4, pulse1=0, pulse2=0) :
        self.orbit = orbitStudy(self.dir,silent=True)

        # if pulse 1 and 2 defined then those two (counting from 0!)
        if pulse1!=0 and pulse2!=0:
            self.pulse1=pulse1
            self.pulse2=pulse2
        else:
            # for orbit plot subtract reduced charge orbit from nominal charge orbit, in WFS typically pulse two to last and next to last
            self.pulse1 = min(pl.shape(self.orbit.fx))-shift
            self.pulse2 = self.pulse1 + 1
            
        #print 'Differential orbit between pulse ', self.pulse1, ' and ', self.pulse2

        pl.figure(20)
        pl.xlabel("$s$ [m]")
        pl.ylabel("Horizontal Differential Orbit [$\\mu$m]")
        pl.plot(self.orbit.s,self.orbit.fx.transpose()[:,self.pulse1:self.pulse1+1]-self.orbit.fx.transpose()[:,self.pulse2:self.pulse2+1], label=self.label)
        pl.legend(loc="upper left")
        pl.savefig("HorOrbitDiff.png")

        self.maxdistx = max(self.orbit.fx.transpose()[:,self.pulse1:self.pulse1+1]-self.orbit.fx.transpose()[:,self.pulse2:self.pulse2+1])
        self.meandistx = np.mean(self.orbit.fx.transpose()[:,self.pulse1:self.pulse1+1]-self.orbit.fx.transpose()[:,self.pulse2:self.pulse2+1])
        #print 'Max distance hor. differential orbit:', self.maxdistx
        
        pl.figure(21)
        pl.xlabel("$s$ [m]")
        pl.ylabel("Vertical Differential Orbit [$\\mu$m]")
        pl.plot(self.orbit.s,self.orbit.fy.transpose()[:,self.pulse1:self.pulse1+1]-self.orbit.fy.transpose()[:,self.pulse2:self.pulse2+1], label=self.label)
        pl.legend(loc="upper left")
        pl.savefig("VertOrbitDiff.png")

        self.maxdisty = max(self.orbit.fy.transpose()[:,self.pulse1:self.pulse1+1]-self.orbit.fy.transpose()[:,self.pulse2:self.pulse2+1])
        self.meandisty = np.mean(self.orbit.fy.transpose()[:,self.pulse1:self.pulse1+1]-self.orbit.fy.transpose()[:,self.pulse2:self.pulse2+1])
        #print 'Max distance vert. differential orbit:', self.maxdisty

        pl.figure(2)
        #pl.clf()
        ax1 = pl.subplot(2,1,1)
        if len(self.orbit.s)==len(self.orbit.fx.transpose()) :
            pl.plot(self.orbit.s, self.orbit.fx.transpose()[:,-1],label=self.label)
            pl.xlim(6, 92)
            pl.xlabel("$s$ [m]")
        else :
            pl.plot(self.orbit.fx.transpose()[:,-1], label=self.label)
            pl.xlabel("BPM number")
        pl.ylabel("Horizontal Orbit [$\\mu$m]")
        pl.legend(loc="upper left")
        
        ax2 = pl.subplot(2,1,2, sharex=ax1)
        #pl.plot(self.orbit.f.transpose(), '+-')
        if len(self.orbit.s)==len(self.orbit.fy.transpose()) :
            pl.plot(self.orbit.s, self.orbit.fy.transpose()[:,-1],label=self.label)
            pl.plot(self.orbit.s, self.orbit.fy.transpose()[:,0],label=self.label)
            pl.plot(self.orbit.s, self.orbit.fy.transpose()[:,3],label=self.label)
            pl.xlim(6, 92)
            pl.xlabel("$s$ [m]")
        else :
            pl.plot(self.orbit.fy.transpose()[:,-1], label=self.label)
            pl.xlabel("BPM number")
        pl.ylabel("Vertical Orbit [$\\mu$m]")
        pl.legend(loc="upper left")

        pl.savefig(self.orbit.directory+"orbit.png")

    def scanPlot(self,scanParameter="a"):

        pl.figure(22)
        pl.xlabel(scanParameter)
        pl.ylabel('Norm. emittance y [nm]')
        #pl.ylim([3,4])

        pl.semilogx(self.scanValue,100*self.ey[-1],'o',color='b')

class trackingStudyArray() :
    ''' trackingStudyArray class, array for trackingStudy; mainly for plotting'''
    def __init__(self, label="",color="b",linestyle='-',marker='o',parameter=1,fileNameNominal="") :
        self.anaList   = []
        self.size      = 0
        self.label     = label
        self.color     = color
        self.linestyle = linestyle
        self.marker    = marker
        self.parameter = parameter
        self.fileNameNominal = fileNameNominal

        if self.fileNameNominal:
            self.sN              = []
            self.EN              = []
            self.dEN             = []
            self.exN             = []
            self.eyN             = []
            self.sxN             = []
            self.syN             = []
            self.szN             = []

            self.readNominalFile()

    def readNominalFile(self) :
        f = open(self.fileNameNominal)
        for l in f : 
            t = l.split()
            if t [0] =='#' :
                continue
            sN      = t[0]
            EN      = t[1]
            dEN     = t[2]
            exN     = t[3]
            eyN     = t[4]
            szN     = t[5]

            self.sN.append(sN)
            self.EN.append(EN)
            self.dEN.append(dEN)
            self.exN.append(exN)
            self.eyN.append(eyN)
            self.szN.append(szN)
    
            if np.size(t) > 6:
                sxN = t[6]
                syN = t[7]
                self.sxN.append(sxN)
                self.syN.append(syN)
            
        self.sN              = pl.array(self.sN, dtype=float)
        self.EN              = pl.array(self.EN, dtype=float)
        self.dEN             = pl.array(self.dEN, dtype=float)
        self.exN             = pl.array(self.exN, dtype=float)
        self.eyN             = pl.array(self.eyN, dtype=float)
        self.sxN             = pl.array(self.sxN, dtype=float)
        self.syN             = pl.array(self.syN, dtype=float)
        self.szN             = pl.array(self.szN, dtype=float)
            
    def add(self,trackStudy):
        self.anaList.append(trackStudy)
        self.size += 1

    def analyse(self):

        self.ex = 0
        self.ey = 0
        self.nrSuccessfulSeeds = 0
        for k in range(0,self.size):
            if np.isnan(self.anaList[k].ex).any() == False:
                self.ex += self.anaList[k].ex
                self.ey += self.anaList[k].ey
                self.nrSuccessfulSeeds+=1
        self.s = self.anaList[0].s
        self.ex = self.ex/self.nrSuccessfulSeeds
        self.ey = self.ey/self.nrSuccessfulSeeds

        # final emittance
        self.exF=[]
        self.eyF=[]
        for k in range(0,self.size):
            self.exF.append(self.anaList[k].ex[-1])
            self.eyF.append(self.anaList[k].ey[-1])
        self.exF = array(self.exF)
        self.eyF = array(self.eyF)

        # clean nans:
        self.exF = self.exF[~np.isnan(self.exF)]
        self.eyF = self.eyF[~np.isnan(self.eyF)]
        print 'nr of seeds lost ', self.size-size(self.exF)
        
        # emittance before sextupoles (QM15FF)
        self.QM15FFindex = 60
        self.exH=[]
        self.eyH=[]
        for k in range(0,self.size):
            self.exH.append(self.anaList[k].ex[self.QM15FFindex])
            self.eyH.append(self.anaList[k].ey[self.QM15FFindex])
        self.exH = array(self.exH)
        self.eyH = array(self.eyH)
        # clean nans:
        self.exH = self.exH[~np.isnan(self.exH)]
        self.eyH = self.eyH[~np.isnan(self.eyH)]

        self.plot()
        # orbit
        if hasattr(self.anaList[0],'maxdistx'):
            self.analyseOrbit()
            self.plotOrbit()

    def analyseOrbit(self):
        self.maxdistx = []
        self.maxdisty = []
        self.meandistx = []
        self.meandisty = []
        for k in range(0,self.size):
            self.maxdistx.append(self.anaList[k].maxdistx)
            self.maxdisty.append(self.anaList[k].maxdisty)
            self.meandistx.append(self.anaList[k].meandistx)
            self.meandisty.append(self.anaList[k].meandisty)
        self.maxdistx = array(self.maxdistx)
        self.maxdisty = array(self.maxdisty)
        self.meandistx = array(self.meandistx)
        self.meandisty = array(self.meandisty)
                      
    def plot(self):

        end = 87

        fig1000=pl.figure(1000)
        pl.xlabel('$s$ [m]')
        pl.xlim([0,end])
        #pl.xlim([0,87])
        pl.ylim([0,15])
        ax10=fig1000.axes[0];
        
        if self.fileNameNominal:
            #pl.ylabel('$\Delta$ $\epsilon_$ [nm]')
            #ey = 100*(self.ey-self.eyN)
            pl.ylabel('$\Delta$ $\epsilon_y$ / $\epsilon_y$ [\%]')
            ey = 100*(self.ey-self.eyN) / self.eyN
        else:
            pl.ylabel('norm. emittance y [nm]')
            ey = 100*self.ey

        ax10.plot(self.s,ey, label=self.label,marker=self.marker,linestyle=self.linestyle,color=self.color)
        ax10.legend(loc=0)

        inset = 0
        if inset:
            # this is an inset axes over the main axes
            ax10sub = pl.axes([.4, .4, .4, .4])
            pl.xlim([75,85])
            #pl.xlim([60,100])
            pl.ylim([1.0,1.4])
            pl.ylabel('')
            ax10sub.plot(self.s,self.ey)
            # pl.setp(a, yticks=[])
        pl.show()
        pl.savefig("emittancey.png")

        pl.figure(1001)
        pl.title("IP")
        pl.hist(100*self.eyF,bins=10, label=self.label,color=self.color)

        pl.xlabel('norm. emittance y [nm]')
        pl.show()
        pl.savefig("finalemittanceyhist.png") 
        pl.legend(loc=0)

        
        fig1002=pl.figure(1002)
        pl.title("IP")
        pl.xlabel('$s$ [m]')
        pl.xlim([0,end])
        #pl.xlim([0,87])
        ax10=fig1002.axes[0];
        if self.fileNameNominal:
            #pl.ylabel('$\Delta$ $\epsilon_x$ [nm]')
            #ex = 100*(self.ex-self.exN)
            
            pl.ylabel('$\Delta$ $\epsilon_x$ / $\epsilon_x$ [\%]')
            ex = 100*(self.ex-self.exN)/self.exN
        else:
            pl.ylabel('norm. emittance x [nm]')
            ex = 100*self.ex
            
        ax10.plot(self.s,ex, label=self.label,marker=self.marker,linestyle=self.linestyle,color=self.color)
        ax10.legend(loc=0)

        # this is an inset axes over the main axes
        if inset:
            ax10sub = pl.axes([.4, .4, .4, .4])
            pl.xlim([75,85])
            #pl.xlim([60,100])
            pl.ylim([1.0,1.4])
            pl.ylabel('')
            ax10sub.plot(self.s,self.ex)
            # pl.setp(a, yticks=[])
        pl.show()
        pl.savefig("emittancex.png")

        pl.figure(1003)
        pl.title("IP")
        pl.hist(100*self.exF,bins=10, label=self.label,color=self.color)

        pl.xlabel('norm. emittance x [nm]')
        pl.show()
        pl.legend(loc=0)
        pl.savefig("finalemittancexhist.png") 

        pl.figure(1004)
        #pl.title("QM15FF")
        pl.hist(100*self.eyH,bins=10, label=self.label,color=self.color)

        pl.xlabel('norm. emittance y [nm]')
        pl.show()
        pl.legend(loc=0)
        pl.savefig("halfemittanceyhist.png") 

        pl.figure(1005)
        #pl.title("QM15FF")
        pl.hist(100*self.exH,bins=10,label=self.label,color=self.color)

        pl.xlabel('norm. emittance x [nm]')
        pl.show()
        pl.legend(loc=0)
        pl.savefig("halfemittancexhist.png") 

    def plotOrbit(self) :
        pl.figure(1006)
        pl.title('max orbit x')
        pl.hist(self.maxdistx,bins=10, label=self.label,color=self.color)
        pl.xlabel('max orbit [um]')
        pl.legend(loc=0)
        pl.savefig("maxorbitx.png") 

        pl.figure(1007)
        pl.title('max orbit y')
        pl.hist(self.maxdisty,bins=10, label=self.label,color=self.color)
        pl.xlabel('max orbit [um]')
        pl.legend(loc=0)
        pl.savefig("maxorbity.png") 

        pl.figure(1008)
        pl.title('mean orbit x')
        pl.hist(self.meandistx,bins=10, label=self.label,color=self.color)
        pl.legend(loc=0)
        pl.savefig("meanorbitx.png") 

        pl.figure(1009)
        pl.title('mean orbit y')
        pl.hist(self.meandisty,bins=10, label=self.label,color=self.color)
        pl.legend(loc=0)
        pl.savefig("meanorbity.png") 

class trackingStudyArrayArray() :
    ''' trackingStudyArrayArray class, array of arrays for trackingStudy (e.g. weight scan for several seeds); mainly for plotting'''
    def __init__(self, color="b",linestyle='-',marker='o',label='') :
        self.color = color
        self.linestyle = linestyle
        self.marker = marker
        self.anaList = []
        self.size = 0
        self.label = label
        
    def add(self,trackingStudyArray):
        self.anaList.append(trackingStudyArray)
        self.size += 1

    def analyse(self):

        self.parameter = []
        self.exF = []
        self.eyF = []
        self.exH = []
        self.eyH = []

        for k in range(0,self.size):
            arr = self.anaList[k]
            self.parameter.append(arr.parameter)
            self.exF.append(arr.ex[-1])
            self.eyF.append(arr.ey[-1])
            self.exH.append(arr.ex[arr.QM15FFindex])
            self.eyH.append(arr.ey[arr.QM15FFindex])
        self.parameter = array(self.parameter)
        self.exF = array(self.exF)
        self.eyF = array(self.eyF)
        self.exH = array(self.exH)
        self.eyH = array(self.eyH)

        self.plot()
        
        if hasattr(self.anaList[0],'maxdistx'):
            self.analyseOrbit()
            self.plotOrbit()            

    def analyseOrbit(self):

        self.meanmaxdistx = []
        self.meanmaxdisty = []
        self.meanmeandistx = []
        self.meanmeandisty = []
        for k in range(0,self.size):
            arr = self.anaList[k]
            self.meanmaxdistx.append(np.mean(arr.maxdistx))
            self.meanmaxdisty.append(np.mean(arr.maxdisty))
            self.meanmeandistx.append(np.mean(arr.meandistx))
            self.meanmeandisty.append(np.mean(arr.meandisty))
        self.meanmaxdistx = array(self.meanmaxdistx)
        self.meanmaxdisty = array(self.meanmaxdisty)
        self.meanmeandistx = array(self.meanmeandistx)
        self.meanmeandisty = array(self.meanmeandisty)

    def plot(self):
            
        pl.figure(10001)
        pl.title("IP")
        pl.semilogx(self.parameter, 100*self.eyF,'o-',marker=self.marker,linestyle=self.linestyle,color=self.color,label=self.label)
        pl.xlim([0.01,10000])
        pl.xlabel('weight')
        pl.ylabel('norm. emittance y [nm]')
        pl.legend(loc=0)
        pl.savefig("finalemittyscan.png") 

        pl.figure(10002)
        pl.title("IP")
        pl.semilogx(self.parameter, 100*self.exF,'o-',marker=self.marker,linestyle=self.linestyle,color=self.color,label=self.label)
        pl.xlim([0.01,10000])
        pl.xlabel('weight')
        pl.ylabel('norm. emittance x [nm]')
        pl.legend(loc=0)
        pl.savefig("emittxscan.png") 

        pl.figure(10003)
        #pl.title("QM15FF")

        pl.semilogx(self.parameter, 100*self.eyH,'o-',marker=self.marker,linestyle=self.linestyle,color=self.color,label=self.label)
        pl.xlim([0.01,10000])
        pl.xlabel('weight')
        pl.ylabel('norm. emittance y [nm]')
        pl.legend(loc=0)
        pl.savefig("halfemittyscan.png") 

        pl.figure(10004)
        #pl.title("QM15FF")
        pl.semilogx(self.parameter, 100*self.exH,'o-',marker=self.marker,linestyle=self.linestyle,color=self.color,label=self.label)
        pl.xlim([0.01,10000])
        pl.xlabel('weight')
        pl.ylabel('norm. emittance x [nm]')
        pl.legend(loc=0)
        pl.savefig("halfemittxscan.png") 

    def plotOrbit(self):

        pl.figure(10005)
        pl.title("Mean Max Orbit x")
        pl.semilogx(self.parameter, self.meanmaxdistx,'o-',marker=self.marker,linestyle=self.linestyle,color=self.color,label=self.label)
        pl.xlim([0.01,10000])
        pl.xlabel('weight')
        pl.ylabel('mean max orbit [um]')
        pl.legend(loc=0)
        pl.savefig("meanmaxorbitxscan.png") 

        pl.figure(10006)
        pl.title("Mean Max Orbit y")
        pl.semilogx(self.parameter, self.meanmaxdisty,'o-',marker=self.marker,linestyle=self.linestyle,color=self.color,label=self.label)
        pl.xlim([0.01,10000])
        pl.xlabel('weight')
        pl.ylabel('mean max orbit [um]')
        pl.legend(loc=0)
        pl.savefig("meanmaxorbityscan.png") 

        pl.figure(10007)
        pl.title("Mean Mean Orbit y")
        pl.semilogx(self.parameter, self.meanmeandistx,'o-',marker=self.marker,linestyle=self.linestyle,color=self.color,label=self.label)
        pl.xlim([0.01,10000])
        pl.xlabel('weight')
        pl.ylabel('mean mean orbit [um]')
        pl.legend(loc=0)
        pl.savefig("meanmeanorbitxscan.png") 

        pl.figure(10008)
        pl.title("Mean Max Orbit y")
        pl.semilogx(self.parameter, self.meanmeandisty,'o-',marker=self.marker,linestyle=self.linestyle,color=self.color,label=self.label)
        pl.xlim([0.01,10000])
        pl.xlabel('weight')
        pl.ylabel('mean mean orbit [um]')
        pl.legend(loc=0)
        pl.savefig("meanmeanorbityscan.png") 
        
