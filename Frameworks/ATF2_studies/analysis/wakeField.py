import pylab as pl
import numpy as np
import matplotlib as mpl

from scipy.optimize import curve_fit

mpl.rc('legend',numpoints=1,fontsize=18)
mpl.rc('axes',labelsize=20)
mpl.rc('axes',titlesize=20)
mpl.rc('axes',linewidth=1.5)
mpl.rc('lines',linewidth=1.5)     # line width in points

mpl.rc('xtick',labelsize=20)
mpl.rc('ytick',labelsize=20)
mpl.rc('xtick.major',width=1.5)
mpl.rc('ytick.major',width=1.5)

mpl.rc('figure.subplot',left=0.15)
mpl.rc('figure.subplot',bottom=0.15)

class wakeStudy : 
    def __init__(self, fileName="../data/WakeData/atfCbpmrefWy07mm.dat", bunchLength = 7.0, offset = 1.0, signFlip = False, label = "") :

        self.colors = ['b','r','g','k','c']
        
        self.fileName  = fileName
        self.bunchLength = bunchLength # in mm
        self.offset    = offset # in mm
        self.d1        = []
        self.d2        = []
        self.signFlip  = signFlip
        self.label     = label

        print self.fileName

        if label=="":
            self.label = str(self.bunchLength) + ' mm'

        self.readFile()
        self.controlPlot()

    def readFile(self) :
        f = open(self.fileName)
        for l in f : 
            t = l.split()
            if not t or t [0] =='#' :
                continue
            d1    = float(t[0])
            d2    = float(t[1])

            self.d1.append(d1)
            if self.signFlip:
                self.d2.append(-d2)
            else:
                self.d2.append(d2)

        self.d1           = pl.array(self.d1)
        self.d2           = pl.array(self.d2)
        self.mrefPosition = self.d1 * 1000

    def eval(self, x) :
        '''evaluate value of x'''

        # not so fast as doesn't use fact that self.d1 is increasing
        # index idx
        idx = (np.abs(self.d1-x)).argmin()
        return self.d2[idx]

    def controlPlot(self) :
        pl.figure(1)
        #pl.clf()
        pl.plot(self.d1,self.d2, label=self.label)
        #pl.xlim(self.d1.min(), self.d1.max())
        pl.xlim(-0.04,0.02)
        pl.xlabel('z [m]')
        pl.ylabel('$W$ [V/pC]')
        #pl.ylabel('Transverse wake potential [V/pC/mm]')
        pl.legend(loc="upper left")

        # Normal distribution
        def normal(x, *p):
            mu, sigma = p
            return 1/sigma/np.sqrt(2*3.14159265)*np.exp(-(x-mu)**2/(2.*sigma**2))

        self.y = normal(self.d1, 0, self.bunchLength*0.001)
        
        pl.figure(2)
        #pl.clf()
        pl.plot(self.d1,self.y,label=self.label)
        pl.xlim(self.d1.min(), self.d1.max())
        pl.xlabel('z [m]')
        pl.ylabel('Charge distribution [pC/m]')
        pl.legend()

        self.convolute = self.d2*self.y
        pl.figure(3)
        #pl.clf()
        pl.xlabel('z [m]')
        pl.ylabel('Wake-charge convolution [V/mm/m]')
        pl.xlim(-0.04,0.03)
        pl.plot(self.d1,self.convolute,label=self.label)
        pl.legend(loc=0)

        self.conv = sum(self.d2*self.y) * (self.d1.max()-self.d1.min()) / np.size(self.d1)
        print 'convoluted wake of', self.label, 'is',self.conv,'V/mm'

class wakeStudyArray:
    ''' wakeStudyArray class, array for wakeStudy; mainly for plotting'''
    def __init__(self, directory=".") :
        self.dir = directory
        self.anaList = []
        self.size = 0
        self.anaCount = []
                
    def Add(self,wakeStudy, count = 1):
        self.anaList.append(wakeStudy)
        self.size += 1
        self.anaCount.append(count)
        
    def Analyse(self):
        self.offsets = np.zeros(self.size)
        self.conv = np.zeros(self.size)
        for k in range(0,self.size):
            wakeAna = self.anaList[k]
            self.offsets[k] = wakeAna.offset
            self.conv[k] = wakeAna.conv
        
    def Plot(self):
        self.Analyse()
        fig=pl.figure(4)
        pl.xlabel('Offset [mm]')
        #pl.ylabel('Weighted average wake potential [V/pC]')
        pl.ylabel('$W_{\\rm avg}$ [V/pC]')
        figax = fig.add_subplot(111)
        #figax.yaxis.label.set_color('red')

        pl.plot(self.offsets,self.conv,'o')
        pl.ylim([-2,2])

        def odd3rdorderSimple(x, a, b):
            return a*(x)**3 + b*x

        (ar3,cr3),covar=curve_fit(odd3rdorderSimple,self.offsets,self.conv)

        x = np.arange(min(0,min(self.offsets)),max(0,max(self.offsets))+0.1,0.1)
        y  = np.polyval((ar3,0,cr3,0),x)
        pl.plot(x,y)
        print 'ar3',ar3,'cr3',cr3,'ratio',cr3/ar3

    def CombinedPlot(self):
        '''Combined wakefield plot'''
        s = np.arange(-0.04,0.03,0.0005)
        combinedWake = np.zeros(np.size(s))
        for k in range(0,self.size) :
            wakeAna = self.anaList[k]
            count = self.anaCount[k]
            for i in range(0,np.size(s)):
                combinedWake[i] += wakeAna.eval(s[i]) * count


        pl.figure(1)
        #pl.figure(5)
        pl.xlabel('z [m]')
        #pl.ylabel('Transverse wake potential [V/pC/mm]')
        pl.plot(s,combinedWake,'c--',label="Linear Combination")
        pl.legend(loc="upper left")

        # Normal distribution
        def normal(x, *p):
            mu, sigma = p
            return 1/sigma/np.sqrt(2*3.14159265)*np.exp(-(x-mu)**2/(2.*sigma**2))

        # Convoluted wake assumes same bunch length!
        y = normal(s, 0, self.anaList[0].bunchLength*0.001)
        convolutedWake = combinedWake * y

        pl.figure(7)
        pl.plot(s,y)
                          
        pl.figure(6)
        pl.xlabel('z [m]')
        pl.ylabel('Wake-charge convolution [V/mm/m]')
        pl.xlim(-0.04,0.03)
        pl.plot(s,convolutedWake)

        self.conv = sum(combinedWake*y) * (s.max()-s.min()) / np.size(s)
        print 'convoluted wake of combined setup is',self.conv,'V/mm'
