#!/bin/bash
#
# This script takes 4 parameters. If the parameter number is not 4 it simply stops.
#
# usage:
#    submit_jobs_G_k [job_name] [queue_name] [gm_seed] [corr_noise_seed]
#


if [ $# != 2 ]; then
   echo -e "\nThere have to be 3 parameters passed to this script"
   echo -e "Script stopped with an error!\n"
   exit
fi

dir_name=$1
queue_name=$2

param1=(1.0 2.0 3.0)
param2=(1.0 2.0 3.0)
size_param1=${#param1[*]}
size_param2=${#param2[*]}

i=0
while [ $i -lt $size_param1 ]; do
    j=0
    while [ $j -lt $size_param2 ]; do 
	echo "Submit $dir_name with seed $seed, param1 ${param1[$i]} and param2 ${param2[$j]} to queue $queue_name"
	bsub -q $queue_name job_universal.sh $dir_name ${param1[$i]}  ${param2[$j]}
	let j=j+1
    done
    let i=i+1
done






