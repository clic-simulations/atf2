#!/bin/bash

if [ $# != 4 ]; then
   echo -e "\nThere have to be 4 parameters passed to this script"
   echo -e "Script stopped with an error!\n"
   exit
fi

dir_name=$1
queue_name=$2
start_seed=$3
stop_seed=$4

param=(1.0 2.0 3.0)
size_param=${#param[*]}

j=0
let stop_seed=stop_seed+1;
while [ $j -lt $size_param ]; do
    i=$start_seed
    while [ $i -lt $stop_seed ]; do
	echo "Submit $dir_name with seed $i and param ${param[$j]} to queue $queue_name"
	bsub -q $queue_name job_universal.sh $dir_name $i ${param[$j]}
	let i=i+1
    done
    let j=j+1
done

