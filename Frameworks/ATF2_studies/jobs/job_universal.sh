#!/bin/bash

#################
# Copy the data #
#################

export workdir=$( pwd )
export basedir=/afs/cern.ch/work/j/jpfingst/clicsim/trunk/ATF2
export scriptdir=$basedir/Frameworks/ATF2_studies
#export homedir=$scriptir
export homedir=jpfingst@macbe13127.cern.ch:/Users/jpfingst/Work/sim_results/ATF2

mkdir -p Lattices
mkdir -p Common
mkdir -p Frameworks

scp -r ${basedir}/Lattices/* ${workdir}/Lattices/
scp -r ${basedir}/Common/* ${workdir}/Common/

cd Frameworks
mkdir -p ATF2_studies
cd ATF2_studies
export workdir=$( pwd )

mkdir -p scripts
mkdir -p data
sim_dir=$1
mkdir -p $sim_dir

scp -r ${scriptdir}/scripts/* ${workdir}/scripts/
scp -r ${scriptdir}/data/* ${workdir}/data/
scp ${scriptdir}/run_atf2.tcl ${workdir}
scp ${scriptdir}/settings.tcl ${workdir}
scp ${scriptdir}/$sim_dir/settings_$sim_dir.tcl ${workdir}/$sim_dir/

source /afs/cern.ch/eng/sl/clic-code/setup.sh
echo "Test setup finished"

###############
# Run the job #
###############

if [ $# == 0 ]; then
    echo "No test directory has been specified"
    exit
elif [ $# == 1 ]; then
    echo "No seed was received"
    placet run_atf2.tcl ./$sim_dir/settings_$sim_dir.tcl |tee test.log
elif [ $# == 2 ]; then 
    echo "The seed $2 was received"
    placet run_atf2.tcl ./$sim_dir/settings_$sim_dir.tcl $2 |tee test.log
elif [ $# == 3 ]; then 
    echo "The seed $2 and parameter $3 were received"
    placet run_atf2.tcl ./$sim_dir/settings_$sim_dir.tcl $2 $3 |tee test.log
elif [ $# == 4 ]; then 
    echo "The seed $2 and he parameters $3 and $4 were received"
    placet run_atf2.tcl ./$sim_dir/settings_$sim_dir.tcl $2 $3 $4 |tee test.log
else
    echo "Too many parameters specified"
    exit
fi
echo "Simulation finished"

#####################
# Copy results back #
#####################

if [ $# == 1 ]; then
    seed_dir=seed
elif [ $# == 2 ]; then
    seed_dir=seed_$2
elif [ $# == 3 ]; then
    seed_dir=seed_$2_$3
else
    seed_dir=seed_$2_$3_$4
fi

cd ${workdir}/
mkdir -p copy_dir
cd copy_dir
mkdir -p $seed_dir
cd ..
   
scp ${workdir}/$sim_dir/meas_station* ${workdir}/copy_dir/$seed_dir/
scp ${workdir}/$sim_dir/settings.dat ${workdir}/copy_dir/$seed_dir/
scp ${workdir}/test.log ${workdir}/copy_dir/$seed_dir/

scp ${workdir}/$sim_dir/bpm_meas* ${workdir}/copy_dir/$seed_dir/
scp ${workdir}/$sim_dir/gm* ${workdir}/copy_dir/$seed_dir/
scp ${workdir}/$sim_dir/ident_results* ${workdir}/copy_dir/$seed_dir/
scp ${workdir}/$sim_dir/excit_* ${workdir}/copy_dir/$seed_dir/
scp ${workdir}/$sim_dir/P_* ${workdir}/copy_dir/$seed_dir/
scp ${workdir}/$sim_dir/R_* ${workdir}/copy_dir/$seed_dir/
scp ${workdir}/$sim_dir/meas_* ${workdir}/copy_dir/$seed_dir/


scp -r ${workdir}/copy_dir/* ${homedir}/$sim_dir/
echo "Copying of results finished" 
