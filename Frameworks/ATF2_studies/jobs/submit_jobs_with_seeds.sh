#!/bin/bash
#
# This shript takes 4 parameters. If the parameter number is not 4 it simply stops.
#
# usage:
#    submit_jobs_with_seeds [job_name] [queue_name] [start_seed] [stop_seed]
#
# example
#    submit_jobs_with_seeds job_full_gm_no_contr.sh 1 10
#

if [ $# != 4 ]; then
   echo -e "\nThere have to be 4 parameters passed to this script"
   echo -e "Script stopped with an error!\n"
   exit
fi

job_name=$1
queue_name=$2
start_seed=$3
stop_seed=$4

i=$start_seed
let stop_seed=stop_seed+1;
while [ $i -lt $stop_seed ]; do
  echo "Submitt $job_name with seed $i to queue $queue_name"
  bsub -q $queue_name $job_name $i
  let i=i+1
done

