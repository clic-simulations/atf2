#!/bin/bash
#
# Submit using bsub -q <queuename> lsfjob_simple.sh
# Since this job script is for single CPU jobs you can use
# any machine provided by IT, i.e. you can use any queue you like.
# The only thing you have to take care of is that currently (Oct 2010)
# some machines run SLC4 and some run SLC5. It depends on your
# binary if it runs on one or the other or even on both.
# If your binary runs only on SLC4 you should use the following:
# bsub -q <queuename> -R "select[type==SLC4_64]" lsfjobsimple.sh
# If your binary runs only on SLC5 you should use the following:
# bsub -q <queuename> -R "select[type==SLC5_64]" lsfjobsimple.sh
# It might also depend on the queue if the job can run on SLC4 or SLC5.
# For example, the queue "lxscdmulti" currently (Oct 2010) only
# submits to SLC4 machines. So, requesting type==SLC5_64 on lxscdmulti
# should block the job.
#
# The working directory will be automatically set by the batch system
# to a local directory on the machine which runs the job.
# If your job needs several input files you first have to copy them
# to this working dirctory.
# Since this directory will be delete after finishing the job
# one has to copy all data to some persistent place, e.g. AFS,
# at the end of this script.

# A simple solution is to set the working directory to the AFS directory
# where the simulation files are located. Then the job runs in this
# directory and one does not have to take care of copying files.
# e.g. "cd $LS_SUBCWD"
# LS_SUBCWD is set by the batch system and contains the path of the directory
# from where the job was submitted, e.g. where the command "bsub" was run.
#
# The above solution is simple but not prefered. It is better to really run
# on the local disks and copy data back to AFS or any other place at the end.
export workdir=$( pwd )
export basedir=/afs/cern.ch/work/j/jpfingst/clicsim/trunk/ATF2/
export scriptdir=$basedir/Frameworks/ATF2_jpfingst/
export homedir=$scriptdir
#export homedir=jpfingst@macbe13127.cern.ch:/Users/jpfingst/Work/sim_results/FACET/

mkdir -p Lattices
mkdir -p Common
mkdir -p Frameworks

scp -r ${basedir}/Lattices/* ${workdir}/Lattices/
scp -r ${basedir}/Common/* ${workdir}/Common/

cd Frameworks
mkdir -p ATF2_jpfingst
cd ATF2_jpfingst
export workdir=$( pwd )

mkdir -p scripts
#mkdir -p data
mkdir -p sysident_run

scp -r ${scriptdir}/scripts/* ${workdir}/scripts/
#scp -r ${scriptdir}/data/* ${workdir}/data/
scp ${scriptdir}/run_atf2.tcl ${workdir}
scp ${scriptdir}/settings.tcl ${workdir}
scp ${scriptdir}/my_computer.tcl ${workdir}
scp ${scriptdir}/sysident_run/settings_sysident_run.tcl ${workdir}/sysident_run/

# run the job
#<put your job here>
# e.g. placet-octave placet.tcl
#source /afs/cern.ch/eng/sl/clic-code/software/env_slc5.sh
source /afs/cern.ch/eng/sl/clic-code/setup.sh
if [ $# == 0 ]; then
    echo "No seed was received"
    placet-octave run_atf2.tcl ./sysident_run/settings_sysident_run.tcl |tee test.log
else
    echo "The seed $1 was received"
    placet-octave run_atf2.tcl ./sysident_run/settings_sysident_run.tcl $1 $2 |tee test.log
fi 

# Now we copy our data back to a directory with a unique name
# which contains the job id.
#cp -r ${workdir} ${LS_SUBCWD}/LSFJOB_${LSB_JOBID}_Data/

if [ $# == 0 ]; then
    scp ${workdir}/sysident_run/meas_station* ${homedir}/sysident_run/
    scp ${workdir}/sysident_run/settings.dat ${homedir}/sysident_run/
    scp ${workdir}/test.log ${homedir}/sysident_run/
else
    cd ${workdir}/
    mkdir -p copy_dir
    cd copy_dir
    mkdir -p seed_$1_$2
    cd ..
   
    scp ${workdir}/sysident_run/meas_station* ${workdir}/copy_dir/seed_$1_$2/
    scp ${workdir}/sysident_run/settings.dat ${workdir}/copy_dir/seed_$1_$2/
    scp ${workdir}/sysident_run/P_x.dat ${workdir}/copy_dir/seed_$1_$2/
    scp ${workdir}/sysident_run/P_y.dat ${workdir}/copy_dir/seed_$1_$2/
    scp ${workdir}/sysident_run/R_x.dat ${workdir}/copy_dir/seed_$1_$2/
    scp ${workdir}/sysident_run/R_y.dat ${workdir}/copy_dir/seed_$1_$2/
    scp ${workdir}/sysident_run/ident_results* ${workdir}/copy_dir/seed_$1_$2/
    scp ${workdir}/sysident_run/excit_* ${workdir}/copy_dir/seed_$1_$2/
    scp ${workdir}/sysident_run/meas_* ${workdir}/copy_dir/seed_$1_$2/
    scp ${workdir}/test.log ${workdir}/copy_dir/seed_$1_$2/

    scp -r ${workdir}/copy_dir/* ${homedir}/sysident_run/
fi

