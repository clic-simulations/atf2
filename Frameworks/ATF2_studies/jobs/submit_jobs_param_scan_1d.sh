#!/bin/bash
#
# This script takes 4 parameters. If the parameter number is not 4 it simply stops.
#
# usage:
#    submit_jobs_G_k [job_name] [queue_name] [gm_seed] [corr_noise_seed]
#


if [ $# != 3 ]; then
   echo -e "\nThere have to be 3 parameters passed to this script"
   echo -e "Script stopped with an error!\n"
   exit
fi

dir_name=$1
queue_name=$2
seed=$3

param=(1.0 2.0 3.0)
size_param=${#param[*]}

i=0
while [ $i -lt $size_param ]; do
    echo "Submit $dir_name with seed $seed and param ${param[$i]} to queue $queue_name"
    bsub -q $queue_name job_universal.sh $dir_name $seed ${param[$i]}
    let i=i+1
done






