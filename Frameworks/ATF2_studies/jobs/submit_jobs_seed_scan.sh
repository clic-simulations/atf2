#!/bin/bash

if [ $# != 4 ]; then
   echo -e "\nThere have to be 4 parameters passed to this script"
   echo -e "Script stopped with an error!\n"
   exit
fi

dir_name=$1
queue_name=$2
start_seed=$3
stop_seed=$4

i=$start_seed
let stop_seed=stop_seed+1;
while [ $i -lt $stop_seed ]; do
  echo "Submit $dir_name with seed $i to queue $queue_name"
  bsub -q $queue_name job_universal.sh $dir_name $i
  let i=i+1
done

