set nr_time_steps 30
set use_sextupole 0
# Set beam jitter
set use_beam_jitter 1
set beam_jitter_x 5; #[um]
set beam_jitter_y 1; #[um]
set beam_jitter_xp 0; #[urad]
set beam_jitter_yp 0; #[urad]
# Set font system
set use_font 1
set use_font_jitter 1
set fontk1_strengthx [lindex $argv 1]
set fontk1_strengthy [lindex $argv 2]
set fontk2_strengthx [lindex $argv 3]
set fontk2_strengthy [lindex $argv 4]
# Set ip kicker
set ipkicker_strengthx [lindex $argv 5]
set ipkicker_strengthy [lindex $argv 6]
#set ipkicker_strengthy_sigma [expr $ipkicker_strengthy*0.01] 
# multi bunch!
set n_bunches 1
# set bunch to bunch correlation 
set use_bunch_correlation 1
set bunchTobunchCorrelation 1.0
# Set directory name
set dir_name "ipfeedback_${fontk1_strengthx}_${fontk1_strengthy}_${fontk2_strengthx}_${fontk2_strengthy}_${ipkicker_strengthx}_${ipkicker_strengthy}"
