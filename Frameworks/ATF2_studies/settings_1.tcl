##########################
# Default parameter file #
##########################

######################
# General parameters #
######################

set nr_time_steps 10
# 3Hz beam rep rate
set delta_T [expr 1./3.]
set dir_name "default_output"
set seed_nr 2

#######################
# Beamline parameters #
#######################

#bpm resolution [um]
set stripline 1.0
set stripline2 1.0
set font_stripline_resolution 0.5
set cband 0.2
set cband_noatt 0.05 
set sband 1.0
# nominal 0.002 for ipbpm
set ipbpm 0.1
set ipbpma $ipbpm
set ipbpmb $ipbpm
set ipbpmc $ipbpm

# BPM resolution is dependent on charge with the formula R*sqrt(A^2/q^2+1), where R is the usual resolution and A [1e10 nr part] the charge dependent part.
# This behaviour can be added
set use_bpm_charge_dependence 0
set stripline_charge_dependence 0.69 ;# for 1um resolution
set font_stripline_charge_dependence 0;# unknown
set cband_charge_dependence 0.46 ;# for 200 nm resolution
set cband_noatt_charge_dependence 0.16 ;# for 30nm resolution 
set sband_charge_dependence 0 ;# unknown
set ipbpm_charge_dependence 0 ;# unknown

# Add 3 IPBPMs between QF21X and QM16FF (nanoBPM study)
set nanoBPMSetup 0
set iptbpm1 $ipbpm
set iptbpm2 $ipbpm
set iptbpm3 $ipbpm

# Add (artificial) BPM at IP
set add_bpm_at_ip 0

# tilt angle of skew quadrupoles (only for v.4.0)
set skew_tilt [expr 3.1415926535897932/180*45]

# beamline version 
# options are: "ATF2" (v5.2) "ATF2-v5.2-MadX" (MadX new conversion) "ATF2_v5.2" (v5.2), "ATF2_v5.1", "ATF2_v4.0", "UL" (UltraLow (v4.2)
set beamline_name "ATF2_v5.2-MadX"
# beam name
set beam_name electron

# optics
# only can be changed for v5
# options are (analog to Mad8): "nominal" or "BX1BY1m" (beta* 40mm x 0.1mm), BX10BY1m (beta* 4mm x 0.1mm), BX10BY1nl (beta* 4mm x 0.1mm, nonlinear optimisation by G. White)
# TODO: BX1BY1m needs matching of beam, not done yet
set optics "BX10BY1nl"

#sextupole on (1) or off (0): zero strength (only for lattice v.4.0)
set sext 1
# Switch on/off sextupoles (only for lattice v5)
set use_sextupole 1
# Switch on/off skew sextupoles (default set to 0 strength)
set use_skewsextupoles 1

# Switch on/off multipole components (all multipoles with length 0)
set use_multipole_components 1

# Reference beamline energy (can be different from beam energy)
set e0_start 1.3 ;# [GeV]

# set magnet setting using ATF2 'set' file
set use_set_file 0
# set file name e.g. "set13feb22_0718_1.dat"
# to be put in directory 'data'
set set_file_name ""

###################
# Beam Parameters #
###################

# copied mostly from Mad8 file

#emitance normalized in e-7 m
set match(emitt_x) 52
set match(emitt_y) 0.3
#energy spread normalized in %, negative means flat distribution, positive gaussian distribution, maxed at 3 sigma
#set match(e_spread) 0.08
set match(e_spread) 0.0
#beam long in e-6 m
set match(sigma_z) 8000
set match(phase) 0.0
set e_initial 1.3 ;# design beam energy [GeV]
# for DR RF -2 kHz
#set e_initial [expr 1.3+0.004]
#set e_initial 1.269 ;# operating beam energy? [GeV], from Mad8 file
set charge 1e10
if { $beamline_name == "ATF2" || $beamline_name == "ATF2_v5.1" || $beamline_name == "ATF2_v5.2"} {
    # ATF2 v5 beta in m 
    # (by Hector Garcia Morales)
    array set match {beta_x 8.158262183 alpha_x 0.0 beta_y 0.7917879269 alpha_y 0.0}
} elseif { $beamline_name == "ATF2_v5.2-MadX" } { 
    # Mad8
    array set match {beta_x 6.848560987444 alpha_x 1.108024744392 beta_y 2.935758991906 alpha_y -1.907222941526}
} elseif { $beamline_name == "ATF2_v4.0" || $beamline_name == "UL"} {
    # ATF2 v4.0 beta in m
    array set match {beta_x 6.54290 alpha_x 0.99638 beta_y 3.23785 alpha_y -1.9183 }
}

# beam representation: particle/sliced
set particle_beam 1
# remake beam after each iteration
set dynamic_beam 1
# total number of simulated particles and slices
set n_slice 10
set n 3000
# number of bunches (simulated as pulses)
set n_bunches 1
set bunch_spacing 150e-9 ;#[s]

# add initial dispersion to beam
set use_dispersion 0
set dispersion_x 0 ;# [um]
set dispersion_xp 0 ;# [urad]
set dispersion_y 0 ;# [um]
set dispersion_yp 0 ;# [urad]

# add dispersion at start of FF
set use_dispersion_ff 0
set dispersion_ff_x 0 ;# [um]
set dispersion_ff_xp 0 ;# [urad]
set dispersion_ff_y 0 ;# [um]
set dispersion_ff_yp 0 ;# [urad]

# add initial xy-coupling
set use_xycoupling 0
set xycoupling_angle 0 ;# [rad]

# energy jitter 
set use_energy_jitter 0
set energy_jitter 2e-4 ;# relative

#######################
# Tracking Parameters #
#######################

# synchrotron on/off in certain elements
set quad_synrad 0
set mult_synrad 0
set sbend_synrad 0

################
# FONT setting #
################

set use_font 0
# kicker strength in GeV*urad (for fontk1 10um change in direction fontp1 bpm corresponds to about 23.4 urad*GeV (keV))
set fontk1_strengthx 0.0
set fontk1_strengthy 0.0
set fontk2_strengthx 0.0
set fontk2_strengthy 0.0
set ipkicker_strengthx 0.0
set ipkicker_strengthy 0.0
# jitter on the kickers (relative to the strength)
set use_font_jitter 0
set fontk1_strengthx_sigma 0.0
set fontk1_strengthy_sigma 0.0
set fontk2_strengthx_sigma 0.0
set fontk2_strengthy_sigma 0.0
set mfb2ffkicker_strengthx_sigma 0.0
set mfb2ffkicker_strengthy_sigma 0.0
set ipkicker_strengthx_sigma 0.0
set ipkicker_strengthy_sigma 0.0

set use_qd0ff_strength 0
set qd0ff_strength -0.88554 ;# [GeV/m]
# input qd0 strength in current, overrides qd0 strength setting
set use_qd0ff_current 0
set qd0ff_current 131.631 ;# [A]

set use_qd0ff_current_scan 0
set qd0ff_current_scan_step 0.1 ;# [A]

set use_qd0ff_position 0
set qd0ff_positionx 0.0 ;# [um]
set qd0ff_positiony 0.0 ;# [um]
set qd0ff_roll 0.0 ;# [urad]

set use_qf1ff_strength 0
set qf1ff_strength 0.48034 ;# [GeV/m]
# input qf1ff strength in current, overrides qf1ff strength setting
set use_qf1ff_current 0
set qf1ff_current 124.376 ;# [A]
set use_qf1ff_current_scan 0
set qf1ff_current_scan_step 0.1 ;# [A]

set use_qf1ff_position 0
set qf1ff_positionx 0.0 ;# [um]
set qf1ff_positiony 0.0 ;# [um]
set qf1ff_roll 0.0 ;# [urad]

####################
# FONT/IP feedback #
####################

# FONT feedback in FONT region, needs use_font 1
set FONTFeedback 0
# FONT feedback for X/Y
set FONTFeedbackX 0
set FONTFeedbackY 1

# Feedback algorithm (select one):
# simple FONTfeedback on BPM FONTP1
set FONTFeedbackP1 1
# simple FONTfeedback on BPM FONTP2
set FONTFeedbackP2 0

# feedback relation between bpm reading and kicker strengths
# FONT kicker 1 to FONTP1 [urad*GeV/um]
set FONTP1slope [expr $e_initial/0.548] ;# e0 / distance K1 - P1 
# FONT kicker 2 to FONTP2  
set FONTP2slope [expr $e_initial/0.525] ;# e0 / distance K2 - P2

# MFB2FF kicker
# put kicker in beamline
set use_mfb2ffkicker 0
# MFB2FF feedback, y only
set MFB2FFFeedback 0
set MFB2FFFeedbackX 0
set MFB2FFFeedbackY 1
set MFB2FFslope [expr $e_initial/0.803955] ; #e0 / distance kicker - MFB2FF

# IP feedback with kicker at IP using IPA and IPB
set IPFeedback 0
# IP feedback for X/Y
set IPFeedbackX 0
set IPFeedbackY 1

# feedback objective, can also be used to correct systematic bunch offset
set IPFeedbackXObjective 0.0 ;# [um]
set IPFeedbackYObjective 0.0 ;# [um]

# IP kicker gain to IPA/IPB [urad*GeV/um]
if { $beamline_name == "ATF2" || $beamline_name == "ATF2_v5.2"} {
    set IPAslope [expr $e_initial/0.3838] ;# e0 / distance kicker - IPA
    # IP kicker to IPB  
    set IPBslope [expr $e_initial/0.4648] ;# e0 / distance kicker - IPB
} else {
    set IPAslope [expr $e_initial/0.7298] ;# e0 / distance kicker - IPA
    # IP kicker to IPB  
    set IPBslope [expr $e_initial/0.8108] ;# e0 / distance kicker - IPB
}

# Feedback algorithms (select one):
# simple IPfeedback on BPM IPA
set IPFeedbackIPA 1
# simple IPfeedback on BPM IPB
set IPFeedbackIPB 0

######################################
# Imperfections - Dynamic and Static #
######################################

# Ground motion
set use_ground_motion 0
set ground_motion_x 1 
set ground_motion_y 1

# Misalignment 
set use_misalignment 0
set alignment_qp 1
set alignment_bpm 1
set alignment_drift 0
set alignment_sigma 10 ;#[um]

# quad strength error (static)
set use_quad_strength_error 0
# relative error
set quad_strength_error 0.01

# type: 0: none, 1: ATL, 2: ground motion generator
# model (files in /data/GM) A: LEP (CERN), B: Fermilab, C: DESY, D: KEK, "ATF2_Daniels": ?
# filtertype (stabilisation): 0: no filter, 1: use filters specified in x and y for the whole beamline
# stabilisation is not tested for ATF studies! - JS
# t_start: start after perfect beamline in s
# ip0: move with respect to IP (keep IP at 0) (true) or keep start of beamline at 0 (false)
array set groundmotion {
    type 2
    model "D"
    filtertype 0
    filtertype_x "local_FB_lateral_v2_1.dat"
    filtertype_y "local_FB_vertical_v2_1.dat"
    t_start 0
    ip0 1
}

# Beam jitter
set use_beam_jitter 0
set beam_jitter_x 0; #[um]
set beam_jitter_y 0; #[um]
set beam_jitter_xp 0; #[urad]
set beam_jitter_yp 0; #[urad]

# Bunch jitter
set use_bunch_jitter 0
set bunch_jitter_x 0; #[um]
set bunch_jitter_y 0; #[um]
set bunch_jitter_xp 0; #[urad]
set bunch_jitter_yp 0; #[urad]

# Bunch to bunch correlation
set use_bunch_correlation 0
set bunchTobunchCorrelation 1.0

# Bunch offset
set use_bunch_offset 0
set bunch_offset_x 0; #[um]
set bunch_offset_y 0; #[um]
set bunch_offset_xp 0; #[urad]
set bunch_offset_yp 0; #[urad]

# NanoBPMSetupBeamOffset
set nanoBPMSetupBeamOffsetX 0 ;# [um]
set nanoBPMSetupBeamOffsetXp 0 ;# [urad]
set nanoBPMSetupBeamOffsetY 0 ;# [um]
set nanoBPMSetupBeamOffsetYp 0 ;# [urad]

# CBPM Wakefield
set use_wakefield 0
# use short bunch approximation - 
set use_wakefield_shortbunch 0
set wakefield_shortbunch 300 ;# [um]

# BPM resolution on:
set bpm_noise 1

# add OTR wakes
set use_otr_wakes 0
# add CREF wakes in sbends
set use_sbend_wakes 0
# add CREF wakes in correctors
set use_corrector_wakes 0
# add CREF wakes in drifts
set use_drift_wakes 0

# add wake field setup between QD10BFF and QD10AFF
set wakeFieldSetup 0
# select one of the following options:
# 1 reference cavities (November 2012)
set oneRefCavity 0
## 2 reference cavities (December 2012 - April 2013)
# individual components
set twoRefCavity 0
# wake from single simulation
set twoRefCavityFullSetup 0
# bellow setup instead of 2 reference cavities
# http://atf.kek.jp/twiki/bin/view/ATFlogbook/Log20130419s
set bellowWakeStudy 0
# 1 bellow (May - June 2013)
set oneBellow 0 
# 1 bellow (June 2013) - no wakefield
set oneBellowShielded 0
# use tilted bellow description instead of moving bellows halfway
set wakeFieldSetupTiltedBellow 0
# set move subsequent quadrupoles also a little
set wakeFieldSetupQuadMove 0

# add Dipole Kick
set wakeFieldDipole 0
set wakeFieldDipole_strength 0
set wakeFieldDipole_scan 0

set wakeFieldSetup_pos 0; # [mm] between -4.6mm and +4.6mm

# increase position by 1.0 mm per time step in y
set wakeFieldSetup_scan 0
# increase position by 1.0 mm per time step
set sd4ff_mover_scan_x 0
set sd4ff_mover_scan_y 0

# Quadrupole strength jitter (relative)
set use_quad_strength_jitter 0
set quad_strength_sigma 0
# Quadrupole position jitter (y only)
set use_quad_position_jitter 0
set quad_position_sigma 0; # [um]

###############################
# Response matrix calculation #
###############################

set response_matrix_calc 0
set step_size_R_x 1
set step_size_R_y 1
set response_matrix_load 0
set response_matrix_file "../Response/Response_1.dat"

set response_knob_matrix_calc 0
set response_knob_matrix_load 0
set step_size_R_knob 1e4 ;#[um]
set response_knob_matrix_file ""

############
# Steering #
############

# apply any steering (need to choose one or more)
set use_steering 0
# option to select a subset of bpms used for DFS or WFS (see machine_setup.m)
set steering_select_bpms 0
# beta parameter, cuts on singular values (the higher, the harder the cut)
set steering_beta 6

# one to one steering
set use_one_to_one_steering 0
# number of iterations
set one_to_one_steering_iter 3
# 1-to-1 steering weight
set one_to_one_steering_weight 5

# dispersion free steering - not implemented yet
set use_dfs 0
# relative energy of second beam
set dfs_dE 0.995
set dfs_weight 5
set dfs_weight_x ${dfs_weight}
set dfs_weight_y ${dfs_weight}
# offset and angle of second beam
set x_offset_dfs 0
set y_offset_dfs 0
set beam_yangle_dfs 0
set beam_xangle_dfs 0
# wakefield free steering
set use_wfs 0
# relative charge of second beam
set wfs_dcharge 0.8
# omega weight, dependent on bpm_resolution and alignment
set wfs_weight 5
set wfs_weight_y ${wfs_weight}
set wfs_weight_x ${wfs_weight}
# offset and angle of second beam
set x_offset_wfs 0
set y_offset_wfs 0
set beam_yangle_wfs 0
set beam_xangle_wfs 0
# relative energy second beam
set wfs_dE 1.0
# number of iterations
set dfs_wfs_iter 3

# knob correction
set use_knobs 0
# only option now is perfect knob correction (only possible in simulation)
set use_perfect_knobs 0

#########################
# System identification #
#########################

set identification_switch 0
# alpha has for the ATF2 simulations the meaning of a projected beam size growth in percent.
set alpha_x 5
set alpha_y 5
set lambda_x 0.997
set lambda_y 0.997
# 1 ... standard algo, 2 ... modified algo, 3 ... full algo
set rls_algo 3
# 1 ... diag(0.1), 2 ... find_init_P function
set init_P_algo 2
set init_P_value 0.1
# 1 ... random excit, 2 ... one corrector after the other excit (necessary for rls_algo 6), note the excit_type 3
# which is an excitation with constant maximal BPM offset due to the excitation, is not supported so far. 
# 4 ... corresponds to a test case for the initial scaling of the emittacne growth (determination of factors)
set excit_type 1
# only necessary for rls_algo 6
set disp_reconst_algo 1

set hcorr_modulo 1
set vcorr_modulo 1
set load_corr 0
#set load_hcorr_name "hcorr_index_1.dat"
#set load_vcorr_name "vcorr_index_1.dat"
set load_corr_name "excit_corr_1mum_1.dat"
set load_amp_name "excit_amp_1mum_1.dat"

#############################
# Postprocessing Parameters #
#############################

# output files
# BPM readings
# save BPM readings without noise
set save_bpm_readings_nonoise 0
set bpm_readings_file_name_x "bpmreadingsx_1.dat"
set bpm_readings_file_name_y "bpmreadingsy_1.dat"
set bpm_readings_file_name_xp "bpmreadingsxp_1.dat"
set bpm_readings_file_name_yp "bpmreadingsyp_1.dat"
set magnetPosition_readings_file_name_x "magnetPositionreadingsx_1.dat"
set magnetPosition_readings_file_name_y "magnetPositionreadingsy_1.dat"
set ip_feedback_file_name "IPFeedback_1.dat"
# add header to output file
set add_header 0
set meas_station_file_name "meas_station_1.dat"
# save beam at start of Final Focus (before MQM16FF)
set save_beam_ff_start 0
set meas_station_ff_start_file_name "meas_station_ff_start_1.dat"
# save beam at MFB2FF
set save_beam_mfb2ff 0
set meas_station_mfb2ff_file_name "meas_station_mfb2ff_1.dat"
# save beam at IP
set save_beam_ip 0
set meas_station_ip_file_name "meas_station_ip_1.dat"
# number of pulses taken into account for multipulse emittance calculation
set multipulse_nr 10
set eval_beam_shift 1
set detailed_eval 0
set save_excit_meas 0
set eval_interval 10

# output string for tracking
set save_tracking 1 
set tracking_file_name "tracking_1_1.dat"
set tracking_output_string "%s %E %dE %ex %ey %sz %sx %sy" ;# position, energy, dispersion, emittx, emitty, bunch_length, beam size x, beam size y

# emittance calculated as if dispersion is corrected
set use_disp_free_emittance 0

# python analysis (use a non-interactive backend to return from Python)
set python_analysis 0

# delete intermediate files
set delete_files 1

##########
# PLACET #
##########

# BPM directional reading
set bpm_direction 0

# New Seed Handling from M. Blaha
set PLACETNewSeed 1
