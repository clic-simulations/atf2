% Script to combine the results of simulations with different seeds. This 
% is just the starter script for a universally usable function. Only the 
% parameters have to be changed to use it for any simulation of that type.
%
% Juergen Pfingstner
% 22th of April 2013

%addpath('/Users/jpfingst/Work/Matlab/Data_analysis/');

%%%%%%%%%%%%%
% Parameter %
%%%%%%%%%%%%%

random_or_orthogonal = 'random';
x_or_y = 'x';
rls_algo = 6;
lambda_x = 1;
lambda_y = 1;


start_seed = 1;
stop_seed = 10;
param_values = [0.01 0.02 0.05 0.1 0.2 0.5 1 2 5 10];
param_values_string = {'0.01' '0.02' '0.05' '0.1' '0.2' '0.5' '1' '2' '5' '10'};
%exception_param = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
basic_path = '/Users/jpfingst/Work/sim_results/ATF2/sysident_excit_data';
eval_type_string = sprintf('%d_%d_%d', rls_algo, lambda_x, lambda_y);
data1 = [];
for i = 1 : length(param_values)
    
    % Go through all seed an average the results
    nr_valid_seed = 0;
    for j = start_seed:stop_seed  
        eval_dir = sprintf('%s/%s_%s/seed_%d_%s', basic_path, random_or_orthogonal, x_or_y, j, param_values_string{i});
   
        % check if directory exists
        if(exist(eval_dir, 'dir'))
            eval_dir
            meas_file_name = sprintf("%s/meas_station.dat", eval_dir);
            ident_results_file_name = sprintf("%s/ident_result_%s.dat", eval_dir, eval_type_string);
            ident_detail_col_x_file_name = sprintf("%s/ident_results_col_x_%s.dat", eval_dir,eval_type_string);        
            ident_detail_col_y_file_name = sprintf("%s/ident_results_col_y_%s.dat", eval_dir,eval_type_string);
            ident_detail_row_x_file_name = sprintf("%s/ident_results_row_x_%s.dat", eval_dir,eval_type_string);
            ident_detail_row_y_file_name = sprintf("%s/ident_results_row_y_%s.dat", eval_dir,eval_type_string);
            
            data_temp0 = load(meas_file_name);
            data_temp1 = load(ident_results_file_name);
            data_temp2 = load(ident_detail_col_x_file_name);
            data_temp3 = load(ident_detail_col_y_file_name);
            data_temp4 = load(ident_detail_row_x_file_name);
            data_temp5 = load(ident_detail_row_y_file_name);
            % Since many simulations stopped at about 450
            % iterations I make a fix here :)
            data_temp0 = data_temp0(1:300,:);
            data_temp1 = data_temp1(1:300,:);
            data_temp2 = data_temp2(1:300,:);
            data_temp3 = data_temp3(1:300,:);
            data_temp4 = data_temp4(1:300,:);
            data_temp5 = data_temp5(1:300,:);
            %if(j==10)
            %    keyboard;
            %end
            if(isempty(data1))
                data0 = data_temp0;
                data1 = data_temp1;
                data2 = data_temp2;
                data3 = data_temp3;
                data4 = data_temp4;
                data5 = data_temp5;
            else
                data0 = data0+data_temp0;
                data1 = data1+data_temp1;
                data2 = data2+data_temp2;
                data3 = data3+data_temp3;
                data4 = data4+data_temp4;
                data5 = data5+data_temp5;
            end
            nr_valid_seed = nr_valid_seed+1;
        else
            fprintf('Shit, there is no directory like that: %s\n', eval_dir);
        end
    end
    data0=data0./nr_valid_seed;
    data1=data1./nr_valid_seed;
    data2=data2./nr_valid_seed;
    data3=data3./nr_valid_seed;
    data4=data4./nr_valid_seed;
    data5=data5./nr_valid_seed;
    meas_file_name = sprintf("%s/meas_station_%s_%s_%s.dat", basic_path,random_or_orthogonal, x_or_y, param_values_string{i});
    ident_results_file_name = sprintf("%s/ident_result_%s_%s_%s_%s.dat", basic_path,random_or_orthogonal, x_or_y, eval_type_string, param_values_string{i});
    ident_detail_col_x_file_name = sprintf("%s/ident_results_col_x_%s_%s_%s_%s.dat", basic_path, random_or_orthogonal, x_or_y, eval_type_string,param_values_string{i});        
    ident_detail_col_y_file_name = sprintf("%s/ident_results_col_y_%s_%s_%s_%s.dat", basic_path, random_or_orthogonal, x_or_y, eval_type_string);
ident_detail_row_x_file_name = sprintf("%s/ident_results_row_x_%s_%s_%s_%s.dat", basic_path, random_or_orthogonal, x_or_y, eval_type_string,param_values_string{i});        
    ident_detail_row_y_file_name = sprintf("%s/ident_results_row_y_%s_%s_%s_%s.dat", basic_path, random_or_orthogonal, x_or_y, eval_type_string,param_values_string{i});

    fid_temp = fopen(meas_file_name,'wt');
    [nr_row, nr_col] = size(data0);
    for row_index=1:nr_row
        for col_index=1:nr_col
            fprintf(fid_temp, '%g  ',data0(row_index, col_index));
        end
        fprintf(fid_temp, '\n');
    end
    fclose(fid_temp);
    save(ident_results_file_name, 'data1');
    save(ident_detail_col_x_file_name, 'data2');
    save(ident_detail_col_y_file_name, 'data3');
    save(ident_detail_row_x_file_name, 'data4');
    save(ident_detail_row_y_file_name, 'data5');
    data1 = [];
    data2 = [];
    data3 = [];
    data4 = [];
    data5 = [];
end
fprintf(1,'\n');

