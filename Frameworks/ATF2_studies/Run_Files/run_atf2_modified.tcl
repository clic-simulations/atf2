# global variables
# directory structure
set script_dir [pwd]
set lattice_dir $script_dir/../../Lattices/ATF2
set script_common_dir $script_dir/../../Common
source $script_common_dir/scripts/tcl_procs.tcl
source $script_dir/scripts/load_settings.tcl

#setting up beam, beamline
source $script_dir/scripts/accelerator_setup.tcl
source $script_dir/scripts/postprocess_procs.tcl


# Python post analysis
if {$python_analysis} {
    cd "$script_dir/analysis"
    Python {
# no indent!
execfile("PythonPostAnalysis.py")
print "End of Python Analysis"
    }
}

puts "ATF2 run has ended"
