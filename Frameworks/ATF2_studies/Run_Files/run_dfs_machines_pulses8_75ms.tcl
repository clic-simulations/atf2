# Input: charge file, energy file, correctors strength
# Output: Tracking results and BPMs readings
 
# Possible beam dynamics set up: Misalignment and beam incoming jitter

####################
# Directory set up #
###### #############
set base_dir [ pwd ]
set script_dir $base_dir/../../../ATF2
set lattice_dir $script_dir/Lattices/ATF2
set script_common_dir $script_dir/Common
source $script_common_dir/scripts/tcl_procs.tcl

set e0 1.3

array set args {
    sigma 75
    machine all
    nm 100
    wgt1 1
    wgt2 1
    deltae 0.13
    dcharge 2e9
    beta0 0
    beta1 0
    beta2 0
    nbins 1
    noverlap 0.0
    jitter 1
}   

array set args $argv

set nm $args(nm)
set sigma $args(sigma)
set deltae $args(deltae)
set dcharge $args(dcharge)
set machine $args(machine)
set wgt1 $args(wgt1)
set wgt2 $args(wgt2)
set beta0 $args(beta0)
set beta1 $args(beta1)
set beta2 $args(beta2)
set nbins $args(nbins)
set noverlap $args(noverlap)
set jitter $args(jitter)


proc randn {nsigma} {
    set r 1e12
    while {abs($r)>$nsigma} {
        set v1 [expr { 2. * rand() - 1 }]
        set v2 [expr { 2. * rand() - 1 }]
        set rsq [expr { $v1*$v1 + $v2*$v2 }]
        while { $rsq > 1. } {
 	    set v1 [expr { 2. * rand() - 1 }]
 	    set v2 [expr { 2. * rand() - 1 }]
 	    set rsq [expr { $v1*$v1 + $v2*$v2 }]
        }
        set fac [expr { sqrt( -2. * log( $rsq ) / $rsq ) }]
        set r [expr { $v1 * $fac }]
	if {abs($r)>$nsigma} {
            set r [expr { $v2 * $fac }]
	}
    }
    return $r
}

#######################
# Tracking Output dir #
#######################

set beamline_name "ATF2_v5.2-MadX_nov2016"

exec mkdir -p Output8_01
cd Output8_01

#######################
# BPM resolution [um] #
#######################
#1.0
#1.0
#0.5
#0.2
#0.05
#1.0

set stripline 1*5.0 
set stripline2 1*5.0
set font_stripline_resolution 0.0
set cband 1*1.0
set cband_noatt 0*0.0
set sband 1*1.0

###################
# Beam Parameters #
###################
# Bunch offset
set use_bunch_offset 0
set bunch_offset_x 0; #[um]
set bunch_offset_y 0; #[um]
set bunch_offset_xp 0; #[urad]
set bunch_offset_yp 0; #[urad]

# total number of simulated particles and slices
set n_slice 10
set n 3000

# copied mostly from Mad8 file
# emitance normalized in e-7 m
set match(emitt_x) 52
set match(emitt_y) 0.3
# energy spread normalized in %, negative means flat distribution, positive gaussian distribution, maxed at 3 sigma
set match(e_spread) 0.08
#set match(e_spread) 0.00001
# beam long in e-6 m
set match(sigma_z) 7000
set match(phase) 0.0
set e_initial 1.3;# design beam energy [GeV]
set e0_start 1.3 ;# [GeV]
set charge 8.0e9
set charge_exp 8.0e9
set seed_exp 6
#set match(charge) $charge

#######################
#Add beam line set up #
#######################
set beamline_name "ATF2_v5.2-MadX_nov2016"
##########  10x1 optics  ########
#array set match {beta_x 6.848560987444 alpha_x 1.108024744392 beta_y 2.935758991906 alpha_y -1.907222941526}
##########  10x0.5 optics  ########
array set match {beta_x 6.848560987444 alpha_x 1.108024744392 beta_y 2.935758991906 alpha_y -1.907222941526}

set match_beta_x 6.848560987444
set match_alpha_x 1.108024744392
set match_beta_y 2.93575899190
set match_alpha_y -1.907222941526

# Use beam incoming jitter
set use_beam_jitter 1
set beam_jitter_x 0; #[um]
set beam_jitter_y [expr 0*0.1*29.6771]; #[um] # [expr 3*29.6771]   296.771 - 1 sigma
set beam_jitter_xp 0; #1.7273; #[urad]
set beam_jitter_yp [expr 1*0.1*101.0882]; #[urad] [expr 3*10.10882]   101.0882 - 1 sigma

# synchrotron on/off in certain elements
set quad_synrad 0
set mult_synrad 0
set sbend_synrad 0

# CBPM Wakefield
#set use_wakefield 1
# use short bunch approximation - 
#set use_wakefield_shortbunch 0
#set wakefield_shortbunch 300 ;# [um]

# Options for bpms and quadrupoles misalignments
######## COLLIMATOR PARAMETERS ###########
# metode to calculate collimators: 0 Giovani's method
set wmetode 0
### Example collimator number 3
# Maximum half gap
set paramb 12.0e-3
# Minimum half gap
set parama 12.0e-3
# Collimator half width 
set colwidth 12e-3
# Taper part length
set tlength  100e-3
# Flat part length
set flength 100.0e-3
# Material conductivity (for Stainless Steel, Aluminium, Cooper)
#set conduct 1.45e6
#set conduct 4.8e7
set conduct 5.8e7
set offset 0

###########################
# Add beam line wakefields#
###########################

set use_wakefield 1
# use short bunch approximation - 
set use_wakefield_shortbunch 0
set wakefield_shortbunch 300 ;# [um]

# add wake field setup between QD10BFF and QD10AFF
#set wakeFieldSetup 0
# select one of the following options:
# 1 reference cavities (November 2012)
#set oneRefCavity 0
## 2 reference cavities (December 2012 - April 2013)
# individual components
#set twoRefCavity 0
# wake from single simulation
#set twoRefCavityFullSetup 0
# bellow setup instead of 2 reference cavities
# http://atf.kek.jp/twiki/bin/view/ATFlogbook/Log20130419s
#set bellowWakeStudy 0
# 1 bellow (May - June 2013)
#set oneBellow 0 
# 1 bellow (June 2013) - no wakefield
#set oneBellowShielded 0
# use tilted bellow description instead of moving bellows halfway
set wakeFieldSetupTiltedBellow 0
# set move subsequent quadrupoles also a little
#set wakeFieldSetupQuadMove 0

if {$use_wakefield} {
    source $script_dir/Frameworks/ATF2_studies/scripts/wakefield.tcl

}

#######################
# Add beam line       #
#######################
Girder
ReferencePoint -name "START" -angle 1 -sens 1
proc HCORRECTOR { a b c d } { Dipole $a $b $c $d }
proc VCORRECTOR { a b c d } { Dipole $a $b $c $d }
#source $lattice_dir/ATF2_v5.2-MadX-Coll2.tcl
#source $lattice_dir/ATF2_v5.2-MadX-Coll_10x0.5.tcl
source $lattice_dir/ATF2_v5.2-MadX_fev2018.tcl
#source $lattice_dir/ATF2_v5.2-MadX_nov2016_onewake.tcl

ReferencePoint -sense -1
BeamlineSet -name $beamline_name

#######################
# Setting up the beam #
#######################

set scripts $script_common_dir/scripts  ; # This is necessary for compatibility

# load RF and injection scripts:
source $script_common_dir/scripts/init.tcl;

# loading of the beam making scripts
source $script_common_dir/scripts/ilc_beam.tcl;

# Total number of particles
set n_total [expr $n * $n_slice]

# make initial perfect beam
make_beam_many beam0 $n_slice $n
FirstOrder 1

make_beam_many beam1 $n_slice $n
FirstOrder 1

set charge $dcharge
make_beam_many beam2 $n_slice $n
FirstOrder 1

BeamAddOffset -beam beam0 -x $bunch_offset_x -y $bunch_offset_y -angle_x $bunch_offset_xp -angle_y $bunch_offset_yp


Octave {
    Beam0 = placet_get_beam("beam0");
    B1 = placet_get_beam("beam1");
    B1(:,1) -= ($deltae) / 100.0;
    placet_set_beam("beam1", B1);

	Matrice = [];
	SIZE_Y_NO_CORR = [];
	SIZE_Y_1TO1 = [];
	SIZE_Y_1TO1_DFS = [];
	SIZE_Y_1TO1_DFS_WFS = [];
	SIZE_Y_1TO1_DFS_WFS_KNOBS = [];
	SIZE_Y_JITTER = [];
	Matrice_no_corr = [];
	Matrice_jitter = [];
	CHARGE = [];
	BEAM_SIZE = [];
	BEAM_YP = [];

}

#Octave {
# Centering the beam
#B_centered = placet_get_beam("beam0");
#B_centered(:,2) -= mean(B_centered(:,2));
#B_centered(:,3) -= mean(B_centered(:,3));
#B_centered(:,5) -= mean(B_centered(:,5));
#B_centered(:,6) -= mean(B_centered(:,6));
#placet_set_beam("beam0", B_centered);
#}

##############################
# Apply incoming beam jitter #  
##############################

Octave {

#    if ($use_beam_jitter)
#    Tcl_Eval("BeamAddOffset -beam beam0 -x $beam_jitter_x -y $beam_jitter_y -angle_x $beam_jitter_xp -angle_y $beam_jitter_yp")
#    printf("Incoming beam jitter is considered \n");  
#    else 
#    end

    }
    	     
############
# Tracking #
############

set offset2 0
#DispersionFreeEmittance -on
 
Octave {

    if exist('../R_${deltae}_${dcharge}.dat', "file")
    disp('Loading Response matrix ...');
    load '../R_${deltae}_${dcharge}.dat';
    else
    disp('Computing Response matrix ...');
    Response.Cx = placet_get_name_number_list("$beamline_name", "ZH*");
    Response.Cy = placet_get_name_number_list("$beamline_name", "ZV*");
    Response.Bpms = placet_get_number_list("$beamline_name", "bpm");
    
    # picks all correctors preceeding the last bpm, and all bpms following the first corrector
    Response.Cx = Response.Cx(Response.Cx < Response.Bpms(end));
    Response.Cy = Response.Cy(Response.Cy < Response.Bpms(end));
    Response.Bpms = Response.Bpms(Response.Bpms > max(Response.Cx(1), Response.Cy(1)));

    oldBpmRes = placet_element_get_attribute("$beamline_name", Response.Bpms, "resolution");
    placet_element_set_attribute("$beamline_name", Response.Bpms, "resolution", 0.0);
    
     # gets the energy at each corrector
    Response.Ex = placet_element_get_attribute("$beamline_name", Response.Cx, "e0");
    Response.Ey = placet_element_get_attribute("$beamline_name", Response.Cy, "e0");
    
    # compute response matrices
    placet_test_no_correction("$beamline_name", "beam0", "Zero");
    Response.B0 = placet_get_bpm_readings("$beamline_name", Response.Bpms);
    Response.R0x = placet_get_response_matrix_attribute ("$beamline_name", "beam0", Response.Bpms, "x", Response.Cx, "strength_x", "Zero");
    Response.R0y = placet_get_response_matrix_attribute ("$beamline_name", "beam0", Response.Bpms, "y", Response.Cy, "strength_y", "Zero");
    
    placet_test_no_correction("$beamline_name", "beam1", "Zero");
    Response.B1 = placet_get_bpm_readings("$beamline_name", Response.Bpms) - Response.B0;
    Response.R1x = placet_get_response_matrix_attribute ("$beamline_name", "beam1", Response.Bpms, "x", Response.Cx, "strength_x", "Zero") - Response.R0x;
    Response.R1y = placet_get_response_matrix_attribute ("$beamline_name", "beam1", Response.Bpms, "y", Response.Cy, "strength_y", "Zero") - Response.R0y;
    
    placet_test_no_correction("$beamline_name", "beam2", "Zero");
    Response.B2 = placet_get_bpm_readings("$beamline_name", Response.Bpms) - Response.B0;
    Response.R2x = placet_get_response_matrix_attribute ("$beamline_name", "beam2", Response.Bpms, "x", Response.Cx, "strength_x", "Zero") - Response.R0x;
    Response.R2y = placet_get_response_matrix_attribute ("$beamline_name", "beam2", Response.Bpms, "y", Response.Cy, "strength_y", "Zero") - Response.R0y;

    placet_element_set_attribute("$beamline_name", Response.Bpms, "resolution", oldBpmRes);
		
    save -text '../R_${deltae}_${dcharge}.dat' Response;
    end
}

TwissPlot -beam beam0 -file twiss_good.dat


proc my_survey {} {
  global machine sigma sigmap beamline_name nm
  Octave {

	%%%%%%%%%%%%%%%%%%%%%%%%
	%     Misalignment     %
	%%%%%%%%%%%%%%%%%%%%%%%%


    randn("seed", $machine * 200); #12345
      
    BPMs = placet_get_number_list("$beamline_name", "bpm");
    placet_element_set_attribute("$beamline_name", BPMs, "x", randn(size(BPMs)) * $sigma);
    placet_element_set_attribute("$beamline_name", BPMs, "y", randn(size(BPMs)) * $sigma);
    
    CORs = placet_get_number_list("$beamline_name", "dipole");
    placet_element_set_attribute("$beamline_name", CORs, "strength_x", 0.0);
    placet_element_set_attribute("$beamline_name", CORs, "strength_y", 0.0);
    
    QUAs = placet_get_number_list("$beamline_name", "quadrupole");
    placet_element_set_attribute("$beamline_name", QUAs, "x", randn(size(QUAs)) * $sigma);
    placet_element_set_attribute("$beamline_name", QUAs, "y", randn(size(QUAs)) * $sigma); 

    MULTs = placet_get_number_list("$beamline_name", "multipole");
    placet_element_set_attribute("$beamline_name", MULTs, "x", randn(size(MULTs)) * $sigma);
    placet_element_set_attribute("$beamline_name", MULTs, "y", randn(size(MULTs)) * $sigma);   
  }
}

if { $machine == "all" } {
    set machine_start 1
    set machine_end $nm
} {
    set machine_start $machine
    set machine_end $machine
}

Octave {
    E0=0;
    E1=0;
    E2=0;
    E3=0;
    Bpm0=0;
    Bpm1=0;
    EmittX_hist=zeros($nm, 4);
    EmittY_hist=zeros($nm, 4);

    global BPM_head 
    BPM_head = [];

    global Beta_head 
    Beta_head = [];

[evolve_s, evolve_beta_x, evolve_beta_y, evolve_alpha_x, evolve_alpha_y, ...
 evolve_mu_x, evolve_mu_y, evolve_Dx, evolve_Dy, evolve_E] = placet_evolve_beta_function("$beamline_name", $match_beta_x, $match_alpha_x, $match_beta_y, $match_alpha_y);

phase_advance = [evolve_s, evolve_mu_x, evolve_mu_y];

save -text twiss_evolve.dat phase_advance;
}

#Octave {
#    [E,B] = placet_test_no_correction("$beamline_name", "beam0", "Zero", "%s %E %dE %ex %ey %sx %sy");
#}


#  save beam on different positions
proc reset_global_vars {} {
    global dump_index
    set dump_index 0
}

proc dump_beam {} {
  global dump_index
  set dumpnames {inj_in.dump inj_out.dump corr_out.dump bc1_out.dump lin1_out.dump  bc2_out.dump lin2_out.dump}
  BeamDump -file [lindex  $dumpnames $dump_index]
  incr dump_index
}

# Binning
Octave {
    nBpms = length(Response.Bpms);
    printf("total number of bpms %g \n", nBpms);
    Blen = nBpms / ($nbins * (1 - $noverlap) + $noverlap);
    for i=1:$nbins
      Bmin = floor((i - 1) * Blen - (i - 1) * Blen * $noverlap) + 1;
      Bmax = floor((i)     * Blen - (i - 1) * Blen * $noverlap);
      Cxmin = find(Response.Cx < Response.Bpms(Bmin))(end);
      Cxmax = find(Response.Cx < Response.Bpms(Bmax))(end);
      Cymin = find(Response.Cy < Response.Bpms(Bmin))(end);
      Cymax = find(Response.Cy < Response.Bpms(Bmax))(end);
      Bins(i).Bpms = Bmin:Bmax;
      Bins(i).Cx = Cxmin:Cxmax;
      Bins(i).Cy = Cymin:Cymax;
    end
    
    printf("Each bin contains approx %g bpms.\n", round(Blen));

}


proc save_head {} {
  Octave {
     global T Energie Bet Energy

     B_1 = placet_get_beam();
     M = B_1(:,4) < -7000; # head
#     M = abs(B_1(:,4)) < 7000; # body
#     M = B_1(:,4) > 7000; # tail
     [beta_x, beta_y]=placet_get_twiss_parameters(B_1(M,:));

     T = [ T ; mean(B_1(M,2)) mean(B_1(M,3)) ];
     Bet = [ Bet ; beta_x beta_y ];
#     Energie = [ Energie; mean(B_1(:,1)) ];
     Energy = [Energy; mean(B_1(M,1))];

  }
}

TwissPlot -beam beam0 -file twiss_good.dat

for {set machine $machine_start} {$machine <= $machine_end} {incr machine} {

puts "MACHINE: $machine/$nm"
my_survey
    
Octave {

	disp("TRACKING...");

	[E,B] = placet_test_no_correction("$beamline_name", "beam0", "None", "%s %E %dE %ex %ey %sx %sy");
	E0 += E;
	EmittX_hist($machine, 1) = E(end-1,6);
	EmittY_hist($machine, 1) = E(end-1,7);
	B = load('ip_beam.dat');
	sizey_no_corr = std(B(:,3))*1000;
	size_y = std(B(:,3))*1000
        save -text no_correction_distribution.dat B;
	SIZE_Y_NO_CORR = [SIZE_Y_NO_CORR;sizey_no_corr];
        save -text no_correction_size.dat SIZE_Y_NO_CORR;


    Blist_no_corr=placet_get_number_list("$beamline_name", "bpm");
    Bname_no_corr=placet_element_get_attribute("$beamline_name", Blist_no_corr, "name"); 
    Bpos_no_corr=placet_element_get_attribute("$beamline_name", Blist_no_corr, "s");
    Bmeas_no_corr=placet_get_bpm_readings("$beamline_name",Blist_no_corr);  
         
    Matrice_no_corr = [Matrice_no_corr;Bpos_no_corr,Bmeas_no_corr];
    save -text DATA_BPMs_no_corr.dat Matrice_no_corr;



	disp("1-TO-1 CORRECTION");
        for bin = 1:$nbins
	  Bin = Bins(bin);
	  nCx = length(Bin.Cx);
  	  nCy = length(Bin.Cy);
   	  R0x = Response.R0x(Bin.Bpms,Bin.Cx);
   	  R0y = Response.R0y(Bin.Bpms,Bin.Cy);
	  for i=1:3
	    B0 = placet_get_bpm_readings("$beamline_name", Response.Bpms);
	    B0 -= Response.B0;
   	    B0 = B0(Bin.Bpms,:);
            Cx = -[ R0x ; $beta0 * eye(nCx) ] \ [ B0(:,1) ; zeros(nCx,1) ];
	    Cy = -[ R0y ; $beta0 * eye(nCy) ] \ [ B0(:,2) ; zeros(nCy,1) ];
	    placet_element_vary_attribute("$beamline_name", Response.Cx(Bin.Cx), "strength_x", Cx);
	    placet_element_vary_attribute("$beamline_name", Response.Cy(Bin.Cy), "strength_y", Cy);
	    [E,B] = placet_test_no_correction("$beamline_name", "beam0", "None", "%s %E %dE %ex %ey %sx %sy");
	    
	  end
	end

	E1 += E;
	EmittX_hist($machine, 2) = E(end-1,6);
	EmittY_hist($machine, 2) = E(end-1,7);
	B = load('ip_beam.dat');
	sizey_1to1 = std(B(:,3))*1000;
	size_y = std(B(:,3))*1000
        save -text 1to1_distribution.dat B;
	SIZE_Y_1TO1 = [SIZE_Y_1TO1;sizey_1to1];
        save -text 1to1_size.dat SIZE_Y_1TO1;

	disp("DFS CORRECTION");
        for bin = 1:$nbins
	  Bin = Bins(bin);
	  nCx = length(Bin.Cx);
  	  nCy = length(Bin.Cy);
   	  R0x = Response.R0x(Bin.Bpms,Bin.Cx);
   	  R0y = Response.R0y(Bin.Bpms,Bin.Cy);
   	  R1x = Response.R1x(Bin.Bpms,Bin.Cx);
   	  R1y = Response.R1y(Bin.Bpms,Bin.Cy);
	  for i=1:3
	    B0 = placet_get_bpm_readings("$beamline_name", Response.Bpms);
	    placet_test_no_correction("$beamline_name", "beam1", "None");
	    B1 = placet_get_bpm_readings("$beamline_name", Response.Bpms) - B0;
	    B0 -= Response.B0;
	    B1 -= Response.B1;
            B0 = B0(Bin.Bpms,:);
            B1 = B1(Bin.Bpms,:);
	    Cx = -[ R0x ; $wgt1 * R1x ; $beta1 * eye(nCx) ] \ [ B0(:,1) ; $wgt1 * B1(:,1) ; zeros(nCx,1) ];
	    Cy = -[ R0y ; $wgt1 * R1y ; $beta1 * eye(nCy) ] \ [ B0(:,2) ; $wgt1 * B1(:,2) ; zeros(nCy,1) ];
	    placet_element_vary_attribute("$beamline_name", Response.Cx(Bin.Cx), "strength_x", Cx);
	    placet_element_vary_attribute("$beamline_name", Response.Cy(Bin.Cy), "strength_y", Cy);
	    [E,B] = placet_test_no_correction("$beamline_name", "beam0", "None", "%s %E %dE %ex %ey %sx %sy");
	    
	  end
	end

	E2 += E;
	EmittX_hist($machine, 3) = E(end-1,6);
	EmittY_hist($machine, 3) = E(end-1,7);
	B = load('ip_beam.dat');
	sizey_1to1_dfs = std(B(:,3))*1000;
	size_y = std(B(:,3))*1000
        save -text 1to1_DFS_distribution.dat B;
	SIZE_Y_1TO1_DFS = [SIZE_Y_1TO1_DFS;sizey_1to1_dfs];
        save -text 1to1_DFS_size.dat SIZE_Y_1TO1_DFS;

	disp("WFS CORRECTION");
        for bin = 1:$nbins
	  Bin = Bins(bin);
	  nCx = length(Bin.Cx);
  	  nCy = length(Bin.Cy);
   	  R0x = Response.R0x(Bin.Bpms,Bin.Cx);
   	  R0y = Response.R0y(Bin.Bpms,Bin.Cy);
   	  R1x = Response.R1x(Bin.Bpms,Bin.Cx);
   	  R1y = Response.R1y(Bin.Bpms,Bin.Cy);
   	  R2x = Response.R2x(Bin.Bpms,Bin.Cx);
   	  R2y = Response.R2y(Bin.Bpms,Bin.Cy);
	  for i=1:3
	    B0 = placet_get_bpm_readings("$beamline_name", Response.Bpms);
	    % DFS
	    placet_test_no_correction("$beamline_name", "beam1", "None");
	    B1 = placet_get_bpm_readings("$beamline_name", Response.Bpms) - B0;
            % WFS
	    placet_test_no_correction("$beamline_name", "beam2", "None");
	    B2 = placet_get_bpm_readings("$beamline_name", Response.Bpms) - B0;
	    %
	    B0 -= Response.B0;
	    B1 -= Response.B1;
	    B2 -= Response.B2;
            B0 = B0(Bin.Bpms,:);
            B1 = B1(Bin.Bpms,:);
            B2 = B2(Bin.Bpms,:);
	    Cx = -[ R0x ; $wgt1 * R1x ; $wgt2 * R2x ; $beta2 * eye(nCx) ] \ [ B0(:,1) ; $wgt1 * B1(:,1) ; $wgt2 * B2(:,1) ; zeros(nCx,1) ];
	    Cy = -[ R0y ; $wgt1 * R1y ; $wgt2 * R2y ; $beta2 * eye(nCy) ] \ [ B0(:,2) ; $wgt1 * B1(:,2) ; $wgt2 * B2(:,2) ; zeros(nCy,1) ];
	    placet_element_vary_attribute("$beamline_name", Response.Cx(Bin.Cx), "strength_x", Cx);
	    placet_element_vary_attribute("$beamline_name", Response.Cy(Bin.Cy), "strength_y", Cy);
	    [E,B] = placet_test_no_correction("$beamline_name", "beam0", "None", "%s %E %dE %ex %ey %sx %sy");
	    
	  end
	end

#        save -text ${beamline_name}_beam_${machine}.dat B;

	E3 += E;
	EmittX_hist($machine, 4) = E(end-1,6);
	EmittY_hist($machine, 4) = E(end-1,7);
	B = load('ip_beam.dat');
	sizey_1to1_dfs_wfs = std(B(:,3))*1000;
	size_y = std(B(:,3))*1000
        save -text 1to1_DFS_WFS_distribution.dat B;
	SIZE_Y_1TO1_DFS_WFS = [SIZE_Y_1TO1_DFS_WFS;sizey_1to1_dfs_wfs];
        save -text 1to1_DFS_WFS_size.dat SIZE_Y_1TO1_DFS_WFS;

	disp("KNOBS");
	E0 = 1.3;
	E = B(:,1);
	X = B(:,2);
	Y = B(:,3);
	XP = B(:,5);
	YP = B(:,6);
	D = (B(:,1) - E0) / E0;
	I = ones(size(X));

#	Ax  = [ XP D I ]; % 
#	Axp = [ D I ]; %
#	Ay  = [ XP YP D XP.*YP YP.*D XP.*XP XP.*D D.*D YP.*YP I ]; % alpha_y eta_n
	Ay  = [ YP D XP XP.*XP XP.*YP XP.*D I ];
#	Px  = Ax  \ X;
#	Pxp = Axp \ XP;
	Py  = Ay  \ Y;
#	B(:,2) = X  .- Ax  * Px;
#	B(:,5) = XP .- Axp * Pxp;
	B(:,3) = Y  .- Ay  * Py;

	sizey_1to1_dfs_wfs_knobs = std(B(:,3))*1000;
	size_x = std(B(:,2))*1000
	size_y = std(B(:,3))*1000
        save -text 1to1_DFS_WFS_KNOBS_distribution.dat B;
	SIZE_Y_1TO1_DFS_WFS_KNOBS = [SIZE_Y_1TO1_DFS_WFS_KNOBS;sizey_1to1_dfs_wfs_knobs];
        save -text 1to1_DFS_WFS_KNOBS_size.dat SIZE_Y_1TO1_DFS_WFS_KNOBS;


	CHARGE = [CHARGE;$charge_exp];
        save -text CHARGE.dat CHARGE;

#	 BEAM_SIZE = [BEAM_SIZE;size_y];
#        save -text BEAM_SIZE.dat BEAM_SIZE;

#	BEAM_YP = [BEAM_YP; YP];
#	save -text BEAM_YP.dat BEAM_YP;



	disp("Applying jitter");

        }

	RandomReset -seed [expr $seed_exp]

	
    for {set jitter [expr {$machine*1000}]} {$jitter <= [expr {$machine*1000+100}]} {incr jitter} {
  
         Random g -type gaussian 

#	 set charge_ex [expr {abs($charge_exp + [g]*$charge_exp * 1)}];	 
#	 set charge_ex [expr {$charge_exp + rand()*$charge_exp * 0 * 0.15}];
	 set charge_ex [expr {$charge_exp}];
	 set charge $charge_ex;
	 make_beam_many beam$jitter $n_slice $n
	 FirstOrder 1

	 Octave {

	 disp("Beam jitter number ");disp($jitter);
	 Beam0_jitter = Beam0;
	 Beam0_jitter(:,2) += randn * $beam_jitter_x;
	 Beam0_jitter(:,3) += randn * $beam_jitter_y;
	 Beam0_jitter(:,5) += randn * $beam_jitter_xp;
	 Beam0_jitter(:,6) += randn * $beam_jitter_yp;
#	 placet_set_beam("beam3", Beam0_jitter);
	 placet_set_beam("beam$jitter", Beam0_jitter);

	 disp("Beam charge ");disp($charge_ex);

	 % adding jitter
	 % tracking the beam
	 % reading bpms and beamsize


	 placet_test_no_correction("$beamline_name", "beam$jitter", "None");
	 B = load('ip_beam.dat'); #REMOVE?
	 X = B(:,2);
	 Y = B(:,3);
	 XP = B(:,5);
	 YP = B(:,6);
	 D = (B(:,1) - E0) / E0;
	 
#	 Ax  = [ XP D I ]; % 
#	 Axp = [ D I ]; %
#	 Ay  = [ XP YP D XP.*YP YP.*D XP.*XP XP.*D D.*D YP.*YP I ]; % alpha_y eta_n
	 Ay  = [ YP D XP XP.*XP XP.*YP XP.*D I ];
#	 B(:,2) = X  .- Ax  * Px;
#	 B(:,5) = XP .- Axp * Pxp;
	 B(:,3) = Y  .- Ay  * Py;

	 B_ip_jitter = B;


	 sizey_jitter = std(B_ip_jitter(:,3))*1000;
	 size_x = std(B_ip_jitter(:,2))*1000
	 size_y = std(B_ip_jitter(:,3))*1000
	 size_yp = std(B_ip_jitter(:,5))*1000;
         save -text jitter_distribution.dat B_ip_jitter;
	 SIZE_Y_JITTER = [SIZE_Y_JITTER;sizey_jitter];
         save -text jitter_size_y.dat SIZE_Y_JITTER;


	 Blist_jitter=placet_get_number_list("$beamline_name", "bpm");
	 Bname_jitter=placet_element_get_attribute("$beamline_name", Blist_jitter, "name"); 
	 Bpos_jitter=placet_element_get_attribute("$beamline_name", Blist_jitter, "s");
	 Bmeas_jitter=placet_get_bpm_readings("$beamline_name",Blist_jitter);  
         
	 Bsig_jitter=[Bpos_jitter Bmeas_jitter];
	 Matrice_jitter = [Matrice_jitter;Bpos_jitter,Bmeas_jitter];
	 save -text DATA_BPMs_jitter.dat Matrice_jitter;

	 CHARGE = [CHARGE;$charge_ex];
         save -text CHARGE.dat CHARGE;

	 BEAM_SIZE = [BEAM_SIZE;size_y];
         save -text BEAM_SIZE.dat BEAM_SIZE;

#	 BEAM_YP = [BEAM_YP; YP];
#	 save -text BEAM_YP.dat BEAM_YP;



#	 global T
 #	 T = [];

#  	 placet_element_set_attribute("$beamline_name", Blist_jitter, "tclcall_entrance", "save_head");

	 }

 #   TestNoCorrection -beam beam0 -emitt emitt_final.dat

#	Octave {

#	 BPM_head = [BPM_head;T];
#	 save -text bpm_head.txt BPM_head;

#	 Beta_head = [Beta_head;Bet];
#	 Beta_head1 = [Bpos_jitter,Beta_head];
#	 save -text Beta_head.txt Beta_head1;

#	 BPMs_energy = [Bpos_jitter,Energy];
#	 save -text BPMs_energy_head.txt BPMs_energy;


#	 Energie_s = [Energie, Bpos_jitter];
#	 save -text ENERGIE.dat Energie_s
#	 distrib_cut = [B_1(M,3),B_1(M,4)];
#	 save -text distrib_cut.dat distrib_cut

#	}
   }
}


set nm [expr $machine_end - $machine_start + 1]

Octave {
    E0 /= $nm;
    E1 /= $nm;
    E2 /= $nm;
    E3 /= $nm;
#    save -text ${beamline_name}_sigma_histx_w_${wgt1}_${wgt2}_b_${beta0}_${beta1}_${beta2}_c_${dcharge}_n_${nm}.dat EmittX_hist
#    save -text ${beamline_name}_sigma_histy_w_${wgt1}_${wgt2}_b_${beta0}_${beta1}_${beta2}_c_${dcharge}_n_${nm}.dat EmittY_hist
#    save -text ${beamline_name}_emitt_no_n_${nm}.dat E0
#    save -text ${beamline_name}_emitt_simple_b_${beta0}_n_${nm}.dat E1
#    save -text ${beamline_name}_emitt_dfs_w_${wgt1}_b_${beta0}_${beta1}_n_${nm}.dat E2
#    save -text ${beamline_name}_emitt_wfs_w_${wgt1}_${wgt2}_b_${beta0}_${beta1}_${beta2}_c_${dcharge}_n_${nm}.dat E3
}


Octave {

Blist=placet_get_number_list("$beamline_name", "bpm");
Bname=placet_element_get_attribute("$beamline_name", Blist, "name"); 
Bpos=placet_element_get_attribute("$beamline_name", Blist, "s");
Bmeas=placet_get_bpm_readings("$beamline_name",Blist);  
         

bpm_index = placet_get_number_list("$beamline_name", "bpm");


#Y_bpms = Bmeas(:,2);

#for i = bpm_index

diff_calc_meas = [];
Y0_calculated = [];
Y_bpms_out = [];
R = [];

#for j = 1:size(Blist)(2)

#	R = [];

#	Y_bpms_out = Bmeas(1:j,2);
	
#	for i = bpm_index(1:j)

#	Y_bpms = Bmeas(1:j,2);
	
#	R_{i} = placet_get_transfer_matrix("ATF2_v5.2-MadX_nov2016", 0, i);

#	R = [R;R_{1,i}(3,3), R_{1,i}(3,4), R_{1,i}(3,6)];
	
#	end

#	Y_bpms = Bmeas(:,2);	

#	Y0 = R\Y_bpms;

#	Y0_calculated = [Y0_calculated;Y0(1)];

#	diff_calc_meas = [diff_calc_meas;Y_bpms_out - Y0_calculated];

#end

#	for i = bpm_index
	
#	R_{i} = placet_get_transfer_matrix("ATF2_v5.2-MadX_nov2016", 0, i);

#	R = [R;R_{1,i}(3,3), R_{1,i}(3,4), R_{1,i}(3,6)];
	
#	end

#	Y_bpms = Bmeas(:,2);	
#	Y0 = R\Y_bpms;



#disp(diff_calc_meas(end-size(Blist)(2)+1:end))

#diff_calc_meas_last = diff_calc_meas(end-size(Blist)(2)+1:end)
#save -text diff_calc_meas.dat diff_calc_meas_last


}



#Octave {


#Matrix_M = reshape(Matrice_jitter(:,3), 46,100);

#Matrix_M = Matrix_M';

#save -text Matrix_M.dat Matrix_M;

#Matrix_M_charge = [Matrix_M,CHARGE];

#}

#exec echo "set grid lt 0 \n \\
#plot '${beamline_name}_emitt_simple_b_${beta0}_n_${nm}.dat' u 1:4 w l ti 'simple cor', \\
#     '${beamline_name}_emitt_dfs_w_${wgt1}_b_${beta0}_${beta1}_n_${nm}.dat' u 1:4 w l ti 'dfs cor', \\
#     '${beamline_name}_emitt_wfs_w_${wgt1}_${wgt2}_b_${beta0}_${beta1}_${beta2}_c_${dcharge}_n_${nm}.dat' u 1:4 w l ti 'wfs cor' " | gnuplot -persist




