% Script to track with certain imperfections in order to locate certain the origin of the observed beam jitter sources
%
% Juergen Pfingstner
% 8th of February 2012

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialisation and parameters %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global INST
global BEAMLINE
global FL

%FL.simTrackMode = 'sparse';
%FL.simBeamID = '3';
%FL.simTrackMode = 'macro';
%FL.simBeamID = '1';

[nr_bpms_ring, bpms_index, raw_index] = get_BPM_index_FL();

bpms_index = bpms_index(108:end-1);
bpms_index(17) = [];
bpms_index(8) = [];

bpm_numbers = 108:135;
bpm_numbers(17) = [];
bpm_numbers(8) = [];

raw_index = raw_index(108:end-1);
raw_index(17) = [];
raw_index(8) = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Introduce the imperfection %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% BPM 112 (MQM16FF) is located at BEAMLINE index 1589 
% BPM 118 (MQM10FF) is located at BEAMLINE index 1672
% Offset data structure is [x xz y yz s xy] in [m rad m rad m rad]

%offset_x = 1e-3;
%offset_y = 2e-7;
offset_x = 0;
offset_y = 8e-7;

% QD18X
%BEAMLINE{1524}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1525}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1526}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1527}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1528}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1529}.Offset = [offset_x 0 offset_y 0 0 0];

% QF19X
%BEAMLINE{1539}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1540}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1541}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1542}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1543}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1544}.Offset = [offset_x 0 offset_y 0 0 0];

% QD20X
%BEAMLINE{1562}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1563}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1564}.Offset = [offset_x 0 offset_y 0 0 0];

% QF21X
BEAMLINE{1572}.Offset = [offset_x 0 offset_y 0 0 0];
BEAMLINE{1573}.Offset = [offset_x 0 offset_y 0 0 0];

% QM16FF
%BEAMLINE{1591}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1592}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1593}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1594}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1595}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1596}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1597}.Offset = [offset_x 0 offset_y 0 0 0];

% QM15FF
%BEAMLINE{1603}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1604}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1605}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1606}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1607}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1608}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1608}.Offset = [offset_x 0 offset_y 0 0 0];

% QM14FF
%BEAMLINE{1613}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1614}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1615}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1616}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1617}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1618}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1618}.Offset = [offset_x 0 offset_y 0 0 0];

% QM13FF
%BEAMLINE{1632}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1633}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1634}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1635}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1636}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1637}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1638}.Offset = [offset_x 0 offset_y 0 0 0];

% QM12FF
%BEAMLINE{1642}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1643}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1644}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1645}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1646}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1647}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1648}.Offset = [offset_x 0 offset_y 0 0 0];

% QM11FF
%BEAMLINE{1654}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1655}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1656}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1657}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1658}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1659}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1660}.Offset = [offset_x 0 offset_y 0 0 0];

% QD10BFF
%BEAMLINE{1664}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1665}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1676}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1677}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1668}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1669}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1670}.Offset = [offset_x 0 offset_y 0 0 0];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Track the beam and extract the BPM readings %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[stat,raw]=FlHwUpdate();
[stat,raw]=FlHwUpdate('readbuffer',1);
bpm_readings_x = raw(:,raw_index*3-1);
bpm_readings_y = raw(:,raw_index*3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Remove the imperfections %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% QD18X
%BEAMLINE{1524}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1525}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1526}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1527}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1528}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1529}.Offset = [0 0 0 0 0 0];

% QF19X
%BEAMLINE{1539}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1540}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1541}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1542}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1543}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1544}.Offset = [0 0 0 0 0 0];

% QD20X
%BEAMLINE{1562}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1563}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1564}.Offset = [0 0 0 0 0 0];

% QF21X
BEAMLINE{1572}.Offset = [0 0 0 0 0 0];
BEAMLINE{1573}.Offset = [0 0 0 0 0 0];

% QM16FF
%BEAMLINE{1591}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1592}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1593}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1594}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1595}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1596}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1597}.Offset = [0 0 0 0 0 0];

% QM15FF
%BEAMLINE{1603}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1604}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1605}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1606}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1607}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1608}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1609}.Offset = [0 0 0 0 0 0];

% QM14FF
%BEAMLINE{1613}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1614}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1615}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1616}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1617}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1618}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1619}.Offset = [0 0 0 0 0 0];

% QM13FF
%BEAMLINE{1632}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1633}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1634}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1635}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1636}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1637}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1638}.Offset = [0 0 0 0 0 0];

% QM12FF
%BEAMLINE{1642}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1643}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1644}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1645}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1646}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1647}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1648}.Offset = [0 0 0 0 0 0];

% QM11FF
%BEAMLINE{1654}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1655}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1656}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1657}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1658}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1659}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1660}.Offset = [0 0 0 0 0 0];

% QD10BFF
%BEAMLINE{1664}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1665}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1666}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1667}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1668}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1669}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1670}.Offset = [0 0 0 0 0 0];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot the result together with the detected beam motion %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

source_17 = load('source_motion_17_V2.dat');
source_18 = load('source_motion_18_V1.dat');

sim_data = bpm_readings_y'./source_17(:,4);
sim_data = sim_data./norm(sim_data);

meas_data2_17 = source_17(:,2)./source_17(:,4);
meas_data2_17 = meas_data2_17./norm(meas_data2_17);
meas_data3_17 = source_17(:,3)./source_17(:,4);
meas_data3_17 = meas_data3_17./norm(meas_data3_17);
meas_data_xy_17 = source_17(:,5)./source_17(:,4);
meas_data_xy_17 = meas_data_xy_17./norm(meas_data_xy_17);

meas_data2_18 = source_18(:,2)./source_18(:,4);
meas_data2_18 = meas_data2_18./norm(meas_data2_18);
meas_data3_18 = source_18(:,3)./source_18(:,4);
meas_data3_18 = meas_data3_18./norm(meas_data3_18);
meas_data_xy_18 = source_18(:,5)./source_18(:,4);
meas_data_xy_18 = meas_data_xy_18./norm(meas_data_xy_18);

figure(3);
plot(bpm_numbers, meas_data2_17, 'x-b');
hold on;
grid on;
plot(bpm_numbers, meas_data2_18, 'x--b');
plot(bpm_numbers, meas_data3_17, 'x-g');
plot(bpm_numbers, meas_data3_18, 'x--g');
plot(bpm_numbers, meas_data_xy_17, 'x-k');
plot(bpm_numbers, meas_data_xy_18, 'x--k');
plot(bpm_numbers, sim_data, 'x-r');
legend('source 2, 17th', 'source 2, 18th', 'source 3, 17th', 'source3,18th', 'source xy, 17th', 'source xy, 18th','sim data');
hold off;

figure(4);
plot(bpm_numbers, source_17(:,2), 'x-b');
%plot(bpm_numbers, source_17(:,2), 'x-b');
hold on;
grid on;
plot(bpm_numbers, source_18(:,2), 'x--b');
plot(bpm_numbers, source_17(:,3), 'x-g');
plot(bpm_numbers, source_18(:,3), 'x--g');
plot(bpm_numbers, source_17(:,5), 'x-k');
plot(bpm_numbers, source_18(:,5), 'x--k');
%plot(bpm_numbers, source_17(:,3), 'x-g');
plot(bpm_numbers, bpm_readings_y', 'x-r');
legend('source 2, 17th', 'source 2, 18th', 'source 3, 17th', 'source3,18th', 'source xy, 17th', 'source xy, 18th','sim data');
xlabel('BPM nr. [1]');
ylabel('y [\mum]');
axis([108 135 -40e-6 40e-6]);
hold off;

%figure(4);
%plot(bpm_numbers, source_17(:,2).*5.22e-5, 'x-b');
%hold on;
%grid on;
%plot(bpm_numbers, source_17(:,3).*6.087e-5, 'x-g');
%plot(bpm_numbers, bpm_readings_y', 'x-r');
%legend('source 2, 17th', 'source 3, 17th', 'sim data');
%hold off;

%figure(5);
%plot(bpm_numbers, source(:,2), 'x-b');
%hold on;
%grid on;
%plot(bpm_numbers, source(:,3), 'x-g');
%plot(bpm_numbers, bpm_readings_y'./6e-5, 'x-r');
%legend('meas data (source 2)', 'meas data (source 3)', 'sim data');
%hold off;
