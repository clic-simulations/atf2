% Script to track with certain imperfections in order to locate certain the origin of the observed beam jitter sources
%
% Juergen Pfingstner
% 8th of February 2012

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialisation and parameters %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global INST
global BEAMLINE
global FL

%FL.simTrackMode = 'sparse';
%FL.simBeamID = '3';
%FL.simTrackMode = 'macro';
%FL.simBeamID = '1';

[nr_bpms_ring, bpms_index, raw_index] = get_BPM_index_FL();

bpms_index = bpms_index(108:end-1);
bpms_index(17) = [];
bpms_index(8) = [];

bpm_numbers = 108:135;
bpm_numbers(17) = [];
bpm_numbers(8) = [];

raw_index = raw_index(108:end-1);
raw_index(17) = [];
raw_index(8) = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Introduce the imperfection %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% BPM 112 (MQM16FF) is located at BEAMLINE index 1589 
% BPM 118 (MQM10FF) is located at BEAMLINE index 1672
% Start of extraction is located at BEAMLINE index 1284

nr_elements = length(BEAMLINE);
source_17 = load('source_motion_17_V2.dat');
source_18 = load('source_motion_18_V1.dat');
sigma_y = source_17(:,4);

%delta_yp = -4e-7;
%delta_yp = -1e-6;
delta_yp = 0;
delta_x = 6e-6;

%offset_x = 1e-3;
%offset_y = 2e-7;
offset_x = 0;
offset_y = 0;
%roll = -1/180*pi;
%roll = 4/180*pi;
roll = 1/180*pi;

[~, nr_part] = size(FL.SimBeam{3}.Bunch.x);
beam_in = FL.SimBeam{3};

%[stat,raw]=FlHwUpdate();
[stat, beam_out1, raw_data] = TrackThru(1,nr_elements, beam_in, 1, 1);
raw_index = [raw_data{1}.Index];
bpm_index_correspondence = zeros(length(bpms_index),2);
bpm_index_correspondence(:,1) = bpms_index;

for i=1:length(bpms_index)
    j = find(raw_index==bpm_index_correspondence(i,1),1);
    bpm_index_correspondence(i,2) = j;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Introduce the imperfection %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% BPM 112 (MQM16FF) is located at BEAMLINE index 1589 
% BPM 118 (MQM10FF) is located at BEAMLINE index 1672
% Offset data structure is [x xz y yz s xy] in [m rad m rad m rad]

% QD18X
%BEAMLINE{1524}.Offset = [offset_x 0 offset_y 0 0 roll];
%BEAMLINE{1525}.Offset = [offset_x 0 offset_y 0 0 roll];
%BEAMLINE{1526}.Offset = [offset_x 0 offset_y 0 0 roll];
%BEAMLINE{1527}.Offset = [offset_x 0 offset_y 0 0 roll];
%BEAMLINE{1528}.Offset = [offset_x 0 offset_y 0 0 roll];
%BEAMLINE{1529}.Offset = [offset_x 0 offset_y 0 0 roll];

% QF19X
BEAMLINE{1539}.Offset = [offset_x 0 offset_y 0 0 roll];
BEAMLINE{1540}.Offset = [offset_x 0 offset_y 0 0 roll];
BEAMLINE{1541}.Offset = [offset_x 0 offset_y 0 0 roll];
BEAMLINE{1542}.Offset = [offset_x 0 offset_y 0 0 roll];
BEAMLINE{1543}.Offset = [offset_x 0 offset_y 0 0 roll];
BEAMLINE{1544}.Offset = [offset_x 0 offset_y 0 0 roll];

% QD20X
%BEAMLINE{1562}.Offset = [offset_x 0 offset_y 0 0 roll];
%BEAMLINE{1563}.Offset = [offset_x 0 offset_y 0 0 roll];
%BEAMLINE{1564}.Offset = [offset_x 0 offset_y 0 0 roll];

% QF21X
%BEAMLINE{1572}.Offset = [offset_x 0 offset_y 0 0 roll];
%BEAMLINE{1573}.Offset = [offset_x 0 offset_y 0 0 roll];


%for i=2:nr_elements-1
%for i=1284:5:nr_elements-11

start_index = 1538;
%start_index = 1586;
increment = 1;
stop_index = 1710;
nr_element = length(start_index:increment:stop_index);
%sim_orbits = zeros(length(bpm_numbers),nr_elements);

%for i=start_index:increment:stop_index
for i=start_index:1:start_index

    % Some output
    %fprintf(1,'Tracking element %i\n' i);
    i
    
    % Track the beam to the end of the ith element

    [stat, beam_out1, raw_data1] = TrackThru(1,i-1, beam_in, 1, 1);
    
    % Apply a kick or other stuff to the beam
    
    for j=1:nr_part
        beam_out1.Bunch.x(4,j) = beam_out1.Bunch.x(4,j) + delta_yp; 
        beam_out1.Bunch.x(1,j) = beam_out1.Bunch.x(1,j) + delta_x; 
    end
    
    % Track to the end of the beam line
    [stat, beam_out2, raw_data2] = TrackThru(i,nr_elements,beam_out1, 1, 1);
    
    % Read BPM data
    raw_data = [[raw_data1{1}.y]'; [raw_data2{1}.y]'];
    save_data = raw_data(bpm_index_correspondence(:,2));
    %keyboard;
    %[stat,raw]=FlHwUpdate('readbuffer',1);
    %%bpm_readings_x = raw(:,raw_index*3-1);
    %bpm_readings_y = raw(:,raw_index*3);

    %save_data = bpm_readings_y'./sigma_y;
    save_data_original = save_data;
    save_data = save_data./source_17(:,4);
    save_data = save_data./norm(save_data);
    %save_data = bpm_readings_y';
    %save_data = bpm_readings_y./norm(save_data);
    %sim_orbits(:,i) = save_data;
    sim_orbits = save_data;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Remove the imperfections %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% QD18X
%BEAMLINE{1524}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1525}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1526}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1527}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1528}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1529}.Offset = [0 0 0 0 0 0];

% QF19X
BEAMLINE{1539}.Offset = [0 0 0 0 0 0];
BEAMLINE{1540}.Offset = [0 0 0 0 0 0];
BEAMLINE{1541}.Offset = [0 0 0 0 0 0];
BEAMLINE{1542}.Offset = [0 0 0 0 0 0];
BEAMLINE{1543}.Offset = [0 0 0 0 0 0];
BEAMLINE{1544}.Offset = [0 0 0 0 0 0];

% QD20X
%BEAMLINE{1562}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1563}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1564}.Offset = [0 0 0 0 0 0];

% QF21X
%BEAMLINE{1572}.Offset = [0 0 0 0 0 0];
%BEAMLINE{1573}.Offset = [0 0 0 0 0 0];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Detect which was the closest kick %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

meas_data2_17 = source_17(:,2)./source_17(:,4);
meas_data2_17 = meas_data2_17./norm(meas_data2_17);
meas_data3_17 = source_17(:,3)./source_17(:,4);
meas_data3_17 = meas_data3_17./norm(meas_data3_17);
meas_data_xy_17 = source_17(:,5)./source_17(:,4);
meas_data_xy_17 = meas_data_xy_17./norm(meas_data_xy_17);

meas_data2_18 = source_18(:,2)./source_18(:,4);
meas_data2_18 = meas_data2_18./norm(meas_data2_18);
meas_data3_18 = source_18(:,3)./source_18(:,4);
meas_data3_18 = meas_data3_18./norm(meas_data3_18);
meas_data_xy_18 = source_18(:,5)./source_18(:,4);
meas_data_xy_18 = meas_data_xy_18./norm(meas_data_xy_18);

%figure(1);
%plot(bpm_numbers, meas_data2_17, 'x-b');
%hold on;
%grid on;
%plot(bpm_numbers, meas_data2_18, 'x--b');
%plot(bpm_numbers, meas_data3_17, 'x-g');
%plot(bpm_numbers, meas_data3_18, 'x--g');
%plot(bpm_numbers, meas_data_xy_17, 'x-k');
%plot(bpm_numbers, meas_data_xy_18, 'x--k');
%plot(bpm_numbers, sim_orbits, 'x-r');
%legend('source 2, 17th', 'source 2, 18th', 'source 3, 17th', 'source3,18th', 'source xy, 17th', 'source xy, 18th','sim data');
%hold off;

figure(2);
plot(bpm_numbers, source_17(:,2), 'x-b');
hold on;
grid on;
plot(bpm_numbers, source_18(:,2), 'x--b');
plot(bpm_numbers, source_17(:,3), 'x-g');
plot(bpm_numbers, source_18(:,3), 'x--g');
plot(bpm_numbers, source_17(:,5), 'x-k');
plot(bpm_numbers, source_18(:,5), 'x--k');
%plot(bpm_numbers, source_17(:,3), 'x-g');
plot(bpm_numbers, save_data_original', 'x-r');
legend('source 2, 17th', 'source 2, 18th', 'source 3, 17th', 'source3,18th', 'source xy, 17th', 'source xy, 18th','sim data');
xlabel('BPM nr. [1]');
ylabel('y [\mum]');
axis([108 135 -40e-6 40e-6]);
hold off;
