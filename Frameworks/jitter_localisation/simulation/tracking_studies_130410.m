% Script to track with certain imperfections in order to locate certain the origin of the observed beam jitter sources
%
% Juergen Pfingstner
% 8th of February 2012

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialisation and parameters %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global INST
global BEAMLINE
global FL

%FL.simTrackMode = 'sparse';
%FL.simBeamID = '3';
%FL.simTrackMode = 'macro';
%FL.simBeamID = '1';

[nr_bpms_ring, bpms_index, raw_index] = get_BPM_index_FL();

% Set the BPM resolution to zero. Otherwise we get noise
% artefacts. 
for i=1:length(bpms_index)
    BEAMLINE{bpms_index(i)}.Resolution = 0.0;
end

bpms_index = bpms_index(108:end-3);
bpms_index(11) = [];
bpms_index(1) = [];

bpm_numbers = 108:133;
bpm_numbers(11) = [];
bpm_numbers(6) = [];

raw_index_full = raw_index;
raw_index = raw_index(108:end-3);
raw_index(11) = [];
raw_index(6) = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initial simulation with no imperfections to be able to form the %
% differential data                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[stat,raw]=FlHwUpdate();
[stat,raw]=FlHwUpdate('readbuffer',1);
bpm_readings_before_x = raw(:,raw_index*3-1);
bpm_readings_before_y = raw(:,raw_index*3);
bpm_readings_full_before_y = raw(:,raw_index_full*3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Introduce the imperfection %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% BPM 112 (MQM16FF) is located at BEAMLINE index 1589 
% BPM 118 (MQM10FF) is located at BEAMLINE index 1672
% Offset data structure is [x xz y yz s xy] in [m rad m rad m rad]

%offset_x = 1e-3;
%offset_y = 2e-7;
offset_x = 0;
%offset_y = 8e-7*0.1157; %18X, gets worse and worse in the begin
%offset_y = -8e-7*0.1910; %19X, fits in general good apart from the begin
offset_y = 8e-7*0.2264; %20X
%offset_y = -8e-7*0.52; %21X
%offset_y = -8e-7*1.0; %16FF, kein Effekt da Magnet ausgeschaltet ist
%offset_y = -8e-7*5.0135; %15FF, passt nicht mehr so gut 

% QD18X
%BEAMLINE{1524}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1525}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1526}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1527}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1528}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1529}.Offset = [offset_x 0 offset_y 0 0 0];

% QF19X
%BEAMLINE{1539}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1540}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1541}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1542}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1543}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1544}.Offset = [offset_x 0 offset_y 0 0 0];

% QD20X
BEAMLINE{1562}.Offset = [offset_x 0 offset_y 0 0 0];
BEAMLINE{1563}.Offset = [offset_x 0 offset_y 0 0 0];
BEAMLINE{1564}.Offset = [offset_x 0 offset_y 0 0 0];

% QF21X
%BEAMLINE{1572}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1573}.Offset = [offset_x 0 offset_y 0 0 0];

% QM16FF
%BEAMLINE{1591}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1592}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1593}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1594}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1595}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1596}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1597}.Offset = [offset_x 0 offset_y 0 0 0];

% QM15FF
%BEAMLINE{1603}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1604}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1605}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1606}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1607}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1608}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1608}.Offset = [offset_x 0 offset_y 0 0 0];

% QM14FF
%BEAMLINE{1613}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1614}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1615}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1616}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1617}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1618}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1618}.Offset = [offset_x 0 offset_y 0 0 0];

% QM13FF
%BEAMLINE{1632}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1633}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1634}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1635}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1636}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1637}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1638}.Offset = [offset_x 0 offset_y 0 0 0];

% QM12FF
%BEAMLINE{1642}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1643}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1644}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1645}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1646}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1647}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1648}.Offset = [offset_x 0 offset_y 0 0 0];

% QM11FF
%BEAMLINE{1654}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1655}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1656}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1657}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1658}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1659}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1660}.Offset = [offset_x 0 offset_y 0 0 0];

% QD10BFF
%BEAMLINE{1664}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1665}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1676}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1677}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1668}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1669}.Offset = [offset_x 0 offset_y 0 0 0];
%BEAMLINE{1670}.Offset = [offset_x 0 offset_y 0 0 0];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Track the beam and extract the BPM readings %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[stat,raw]=FlHwUpdate();
[stat,raw]=FlHwUpdate('readbuffer',1);
bpm_readings_after_x = raw(:,raw_index*3-1);
bpm_readings_after_y = raw(:,raw_index*3);
bpm_readings_full_after_y = raw(:,raw_index_full*3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Remove the imperfections %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% QD18X
BEAMLINE{1524}.Offset = [0 0 0 0 0 0];
BEAMLINE{1525}.Offset = [0 0 0 0 0 0];
BEAMLINE{1526}.Offset = [0 0 0 0 0 0];
BEAMLINE{1527}.Offset = [0 0 0 0 0 0];
BEAMLINE{1528}.Offset = [0 0 0 0 0 0];
BEAMLINE{1529}.Offset = [0 0 0 0 0 0];

% QF19X
BEAMLINE{1539}.Offset = [0 0 0 0 0 0];
BEAMLINE{1540}.Offset = [0 0 0 0 0 0];
BEAMLINE{1541}.Offset = [0 0 0 0 0 0];
BEAMLINE{1542}.Offset = [0 0 0 0 0 0];
BEAMLINE{1543}.Offset = [0 0 0 0 0 0];
BEAMLINE{1544}.Offset = [0 0 0 0 0 0];

% QD20X
BEAMLINE{1562}.Offset = [0 0 0 0 0 0];
BEAMLINE{1563}.Offset = [0 0 0 0 0 0];
BEAMLINE{1564}.Offset = [0 0 0 0 0 0];

% QF21X
BEAMLINE{1572}.Offset = [0 0 0 0 0 0];
BEAMLINE{1573}.Offset = [0 0 0 0 0 0];

% QM16FF
BEAMLINE{1591}.Offset = [0 0 0 0 0 0];
BEAMLINE{1592}.Offset = [0 0 0 0 0 0];
BEAMLINE{1593}.Offset = [0 0 0 0 0 0];
BEAMLINE{1594}.Offset = [0 0 0 0 0 0];
BEAMLINE{1595}.Offset = [0 0 0 0 0 0];
BEAMLINE{1596}.Offset = [0 0 0 0 0 0];
BEAMLINE{1597}.Offset = [0 0 0 0 0 0];

% QM15FF
BEAMLINE{1603}.Offset = [0 0 0 0 0 0];
BEAMLINE{1604}.Offset = [0 0 0 0 0 0];
BEAMLINE{1605}.Offset = [0 0 0 0 0 0];
BEAMLINE{1606}.Offset = [0 0 0 0 0 0];
BEAMLINE{1607}.Offset = [0 0 0 0 0 0];
BEAMLINE{1608}.Offset = [0 0 0 0 0 0];
BEAMLINE{1609}.Offset = [0 0 0 0 0 0];

% QM14FF
BEAMLINE{1613}.Offset = [0 0 0 0 0 0];
BEAMLINE{1614}.Offset = [0 0 0 0 0 0];
BEAMLINE{1615}.Offset = [0 0 0 0 0 0];
BEAMLINE{1616}.Offset = [0 0 0 0 0 0];
BEAMLINE{1617}.Offset = [0 0 0 0 0 0];
BEAMLINE{1618}.Offset = [0 0 0 0 0 0];
BEAMLINE{1619}.Offset = [0 0 0 0 0 0];

% QM13FF
BEAMLINE{1632}.Offset = [0 0 0 0 0 0];
BEAMLINE{1633}.Offset = [0 0 0 0 0 0];
BEAMLINE{1634}.Offset = [0 0 0 0 0 0];
BEAMLINE{1635}.Offset = [0 0 0 0 0 0];
BEAMLINE{1636}.Offset = [0 0 0 0 0 0];
BEAMLINE{1637}.Offset = [0 0 0 0 0 0];
BEAMLINE{1638}.Offset = [0 0 0 0 0 0];

% QM12FF
BEAMLINE{1642}.Offset = [0 0 0 0 0 0];
BEAMLINE{1643}.Offset = [0 0 0 0 0 0];
BEAMLINE{1644}.Offset = [0 0 0 0 0 0];
BEAMLINE{1645}.Offset = [0 0 0 0 0 0];
BEAMLINE{1646}.Offset = [0 0 0 0 0 0];
BEAMLINE{1647}.Offset = [0 0 0 0 0 0];
BEAMLINE{1648}.Offset = [0 0 0 0 0 0];

% QM11FF
BEAMLINE{1654}.Offset = [0 0 0 0 0 0];
BEAMLINE{1655}.Offset = [0 0 0 0 0 0];
BEAMLINE{1656}.Offset = [0 0 0 0 0 0];
BEAMLINE{1657}.Offset = [0 0 0 0 0 0];
BEAMLINE{1658}.Offset = [0 0 0 0 0 0];
BEAMLINE{1659}.Offset = [0 0 0 0 0 0];
BEAMLINE{1660}.Offset = [0 0 0 0 0 0];

% QD10BFF
BEAMLINE{1664}.Offset = [0 0 0 0 0 0];
BEAMLINE{1665}.Offset = [0 0 0 0 0 0];
BEAMLINE{1666}.Offset = [0 0 0 0 0 0];
BEAMLINE{1667}.Offset = [0 0 0 0 0 0];
BEAMLINE{1668}.Offset = [0 0 0 0 0 0];
BEAMLINE{1669}.Offset = [0 0 0 0 0 0];
BEAMLINE{1670}.Offset = [0 0 0 0 0 0];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot the result together with the detected beam motion %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

remaining_oscillation = load('oscillation_130410.dat');
remaining_oscillation_130524 = load('oscillation_130524.dat');

t = remaining_oscillation(:,1);
meas_data1 = remaining_oscillation(:,2);
meas_data2 = remaining_oscillation(:,3);
sigma_y = remaining_oscillation(:,4);
meas_data1_norm = meas_data1./sigma_y;
meas_data2_norm = meas_data2./sigma_y;
sim_data = bpm_readings_after_y' - bpm_readings_before_y';
sim_data_norm = sim_data./sigma_y;

t_130524 = remaining_oscillation_130524(:,1);
meas_data1_130524 = remaining_oscillation_130524(:,2);
meas_data2_130524 = remaining_oscillation_130524(:,3);
sigma_y_130524 = remaining_oscillation_130524(:,4);
meas_data1_norm_130524 = meas_data1_130524./sigma_y_130524;
meas_data2_norm_130524 = meas_data2_130524./sigma_y_130524;
sim_data = bpm_readings_after_y' - bpm_readings_before_y';
sim_data_norm_130524 = sim_data./sigma_y_130524;


%figure(3);
%plot(bpm_numbers, meas_data, 'x-b');
%hold on;
%grid on;
%plot(bpm_numbers, sim_data, 'x-r');
%legend('meas', 'sim');
%hold off;

figure(4);
set(gca,'FontSize',18);
plot(bpm_numbers, meas_data1_norm.*100, 'x-b', 'Linewidth', 1.5);
hold on;
grid on;
plot(bpm_numbers, sim_data_norm.*100, 'x-r', 'Linewidth', 1.5);
plot(bpm_numbers, meas_data2_norm.*100, 'x-g', 'Linewidth', 1.5);
plot(bpm_numbers, -meas_data1_norm_130524.*100, 'x--b', 'Linewidth', 1.5);
plot(bpm_numbers, -meas_data2_norm_130524.*100, 'x--g', 'Linewidth', 1.5);
legend('meas. data', 'sim. data');
xlabel('BPM nr. [1]');
ylabel('$$y_{S2}/\sigma_{y0}$$ [\%]');
axis([bpm_numbers(1)-2, bpm_numbers(end)+2, -100, 100]);
hold off;

%figure(5);
%plot(bpm_readings_full_after_y - bpm_readings_full_before_y, 'x-b');
%grid on;

%figure(6);
%plot(bpm_readings_full_after_y, 'x-r');
%grid on;
%hold on;
%plot(bpm_readings_full_before_y, 'x-b');
%hold off;

proposed_factor1 = mean(abs(meas_data1))/mean(abs(sim_data))
proposed_factor2 = mean(abs(meas_data2))/mean(abs(sim_data))
