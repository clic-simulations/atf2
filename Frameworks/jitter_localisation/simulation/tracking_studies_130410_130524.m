% Script to track with certain imperfections in order to locate certain the origin of the observed beam jitter sources
%
% Juergen Pfingstner
% 8th of February 2012

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialisation and parameters %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global INST
global BEAMLINE
global FL

%FL.simTrackMode = 'sparse';
%FL.simBeamID = '3';
%FL.simTrackMode = 'macro';
%FL.simBeamID = '1';

[nr_bpms_ring, bpms_index, raw_index] = get_BPM_index_FL();
bpms_index_ip = findcells(BEAMLINE,'Name','IP');
raw_index_ip = findcells(INSTR, 'Index', bpms_index_ip); 

% Set the BPM resolution to zero. Otherwise we get noise
% artefacts. 
for i=1:length(bpms_index)
    BEAMLINE{bpms_index(i)}.Resolution = 0.0;
end
BEAMLINE{bpms_index_ip}.Resolution = 0.0;

bpms_index_long = bpms_index(91:end-3);
bpms_index_long(11+17) = [];
bpms_index_long(6+17) = [];

bpms_index = bpms_index(108:end-3);
bpms_index(11) = [];
bpms_index(6) = [];

bpm_numbers_long = 91:133;
bpm_numbers_long(11+17) = [];
bpm_numbers_long(6+17) = [];

bpm_numbers = 108:133;
bpm_numbers(11) = [];
bpm_numbers(6) = [];

raw_index_long = raw_index(91:end-3);
raw_index_long(11+17) = [];
raw_index_long(6+17) = [];

raw_index_full = raw_index;
raw_index = raw_index(108:end-3);
raw_index(11) = [];
raw_index(6) = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initial simulation with no imperfections to be able to form the %
% differential data                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[stat,raw]=FlHwUpdate();
[stat,raw]=FlHwUpdate('readbuffer',1);
bpm_readings_before_x = raw(:,raw_index_long*3-1);
bpm_readings_before_y = raw(:,raw_index_long*3);
bpm_readings_full_before_y = raw(:,raw_index_full*3);
ip_offset_before_y = raw(:,raw_index_ip*3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Introduce the imperfection %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% BPM 112 (MQM16FF) is located at BEAMLINE index 1589 
% BPM 118 (MQM10FF) is located at BEAMLINE index 1672
% Offset data structure is [x xz y yz s xy] in [m rad m rad m rad]

offset_x = 0;
offset_y = 0;
tilt = 0;
roll = 0;
PS_ampl = 0;

% Possible names: 
%       ExtKick, QM6RX, ZH100RX, ZH100RX, ZH101RX, ZV100RX, QM7RX, 
%       BS1XA, BS2XA, BS3XA, ZV1X, QS1X, Q1X, Q2X, Q3X, Q4X, Q5X, 
%       Q6X, Q7X, Q8X, Q9X, Q10X, Q11X, Q12X, Q13X, Q14X, Q15X, ZV9X, 
%       Q16X, Q17X, ZV10X, Q18X, Q19X, ZV11X, Q20X, Q21X, Q16FF, ZV1FF,
%       Q15FF, Q14FF, Q13FF, Q12FF, Q11FF, Q10FF

element_name = 'Q9X';

%roll = pi/180*0.01; % Extraction kicker: Fits quite well with the
                    % new data and source 1
%offset_y = -5e-7; % QM6RX
%roll = pi/180*1; % ZH100RX Phase is correct but, too large in the beginning
%PS_ampl = -2e-5; % ZH100RX
%roll = pi/180*1; % ZH101RX Phase is correct but, too large in the beginning
%PS_ampl = -2.5e-5; % ZH101RX
%PS_ampl = -1e-6; % ZV100RX: Wrong phase
%offset_y = 14e-7; % QM7RX
%roll = pi/180*0.001; % BS1XA
%roll = pi/180*0.0004; % BS2XA
%roll = pi/180*0.00015; % BS3XA
%PS_ampl = -5e-7; % ZV1X: Wrong phase
%PS_ampl = -0.1; % QS1X
%offset_y = 8e-7*0.5; %1X
%offset_y = -8e-7*0.2; %2X
%offset_y = 8e-7*0.5; %3X
%offset_y = -8e-7*0.4; %4X
%offset_y = 8e-7*0.15; %5X
%offset_y = -8e-7*0.5; %6X
%offset_y = -8e-7*2.2; %7X The only one that fits to old data!!!
%offset_y = 8e-7*1.2; %8X Not so good anymore, but also not bad for old data
%offset_y = 8e-7*15.0; %9X Changed sign but far to large in the beginning 
%offset_y = -8e-7*0.4; %10X No changed sign
%offset_y = -8e-7*4.0; %11X Changed sign but too large in the beginning
%offset_y = 8e-7*0.4; %12X Changed sign but too small in the beginning
%offset_y = -8e-7*0.5; %13X Changed sign but too small in the beginning
%offset_y = 8e-7*0.3; %14X Changed sign but too small in the beginning
%offset_y = -8e-7*0.5; %15X no changed sig
%PS_ampl = 0.75e-7; % ZV10X
%offset_y = 8e-7*0.55; %16X no changed sign
%offset_y = 8e-7*4.0; %17X changed sign but too large in the beginning 
%PS_ampl = 0.75e-7; % ZV10X
%offset_y = 8e-7*0.1157; %18X, gets worse and worse in the begin
%offset_y = -8e-7*0.1910; %19X, fits in general good apart from the begin
PS_ampl = 0.75e-7; % ZV11X:
%offset_y = 8e-7*0.2264; %20X
%offset_y = -8e-7*0.52; %21X
%offset_y = -8e-7*1.0; %16FF, kein Effekt da Magnet ausgeschaltet ist
%PS_ampl = 1.3e-6; % ZV1FF
%offset_y = -8e-7*5.0135; %15FF, passt nicht mehr so gut 

if(strcmp(element_name, 'ExtKick')==1)
    %Extraction kicker: KEX1A, KEX1B
    BEAMLINE{1286}.Tilt = tilt;
    BEAMLINE{1290}.Tilt = tilt;
    BEAMLINE{1286}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1290}.Offset = [offset_x 0 offset_y 0 0 roll];
%elseif(strcmp(element_name, 'SBEND')==1)
%    % SBEND tests
%    BEAMLINE{1310}.Offset = [1e-4 0 1e-4 0 0 0];
%    BEAMLINE{1311}.Offset = [1e-4 0 1e-4 0 0 0];
elseif(strcmp(element_name, 'QM6RX')==1)
    % QM6RX
    BEAMLINE{1293}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1294}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'ZH100RX')==1)
    % ZH100RX
    BEAMLINE{1296}.Offset = [offset_x 0 offset_y 0 0 roll];
    PS(345).Ampl = PS_ampl;
elseif(strcmp(element_name, 'ZH101RX')==1)
    % ZH101RX
    BEAMLINE{1298}.Offset = [offset_x 0 offset_y 0 0 roll];
    PS(346).Ampl = PS_ampl;
elseif(strcmp(element_name, 'ZV100RX')==1)
    % ZV100RX
    BEAMLINE{1300}.Offset = [offset_x 0 offset_y 0 0 roll];
    PS(410).Ampl = PS_ampl;
elseif(strcmp(element_name, 'QM7RX')==1)
    % QM7RX
    BEAMLINE{1304}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1305}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'BS1XA')==1)
    % BS1XA
    BEAMLINE{1307}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1308}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'BS2XA')==1)
    % BS2XA
    BEAMLINE{1310}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1311}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'BS3XA')==1)
    % BS3XA
    BEAMLINE{1315}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1316}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'ZV1X')==1)
    % ZV1X
    BEAMLINE{1318}.Offset = [offset_x 0 offset_y 0 0 roll];
    PS(462).Ampl = PS_ampl;
elseif(strcmp(element_name, 'QS1X')==1)
    % QS1X
    BEAMLINE{1320}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1321}.Offset = [offset_x 0 offset_y 0 0 roll];
    PS(1).Ampl = PS_ampl;
elseif(strcmp(element_name, 'Q1X')==1)
    % QF1X
    BEAMLINE{1323}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1324}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q2X')==1)
    % QD2X
    BEAMLINE{1340}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1341}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q3X')==1)
    % QF3X
    BEAMLINE{1345}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1346}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q4X')==1)
    % QF4X
    BEAMLINE{1356}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1357}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q5X')==1)
    % QD5X
    BEAMLINE{1365}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1366}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q6X')==1)
    % QF6X
    BEAMLINE{1382}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1383}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q7X')==1)
    % QF7X
    BEAMLINE{1393}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1394}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q8X')==1)
    % QD8X
    BEAMLINE{1400}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1401}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q9X')==1)
    % QF9X
    BEAMLINE{1417}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1418}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q10X')==1)
    % Q10X
    BEAMLINE{1431}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1432}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1433}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1434}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1435}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1436}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q11X')==1)
    % Q11X
    BEAMLINE{1442}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1443}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1444}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1445}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1446}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1447}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1448}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q12X')==1)
    % Q12X
    BEAMLINE{1461}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1462}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1463}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1464}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1465}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1466}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q13X')==1)
    % Q13X
    BEAMLINE{1472}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1473}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q14X')==1)
    % QD14X
    BEAMLINE{1477}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1478}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1479}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q15X')==1)
    % Q15X
    BEAMLINE{1487}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1488}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'ZV9X')==1)
    % ZV9X
    BEAMLINE{1492}.Offset = [offset_x 0 offset_y 0 0 roll];
    PS(470).Ampl = PS_ampl;
elseif(strcmp(element_name, 'Q16X')==1)
    % Q16X
    BEAMLINE{1497}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1498}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1499}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1500}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1501}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1502}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q17X')==1)
    % Q17X
    BEAMLINE{1508}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1509}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1510}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1511}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1512}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1513}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'ZV10X')==1)
    % ZV10X
    BEAMLINE{1522}.Offset = [offset_x 0 offset_y 0 0 roll];
    PS(471).Ampl = PS_ampl;
elseif(strcmp(element_name, 'Q18X')==1)
    % QD18X
    BEAMLINE{1524}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1525}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1526}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1527}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1528}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1529}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q19X')==1)
    % QF19X
    BEAMLINE{1539}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1540}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1541}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1542}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1543}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1544}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'ZV11X')==1)
    % ZV11X
    BEAMLINE{1554}.Offset = [offset_x 0 offset_y 0 0 roll];
    PS(472).Ampl = PS_ampl;
elseif(strcmp(element_name, 'Q20X')==1)
    % QD20X
    BEAMLINE{1562}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1563}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1564}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q21X')==1)
    % QF21X
    BEAMLINE{1572}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1573}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q16FF')==1)
    % QM16FF
    BEAMLINE{1591}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1592}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1593}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1594}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1595}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1596}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1597}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'ZV1FF')==1)
    % ZV1FF
    BEAMLINE{1601}.Offset = [offset_x 0 offset_y 0 0 roll];
    PS(473).Ampl = PS_ampl;
elseif(strcmp(element_name, 'Q15FF')==1)
    % QM15FF
    BEAMLINE{1603}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1604}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1605}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1606}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1607}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1608}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1608}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q14FF')==1)
    % QM14FF
    BEAMLINE{1613}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1614}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1615}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1616}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1617}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1618}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1618}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q13FF')==1)
    % QM13FF
    BEAMLINE{1632}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1633}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1634}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1635}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1636}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1637}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1638}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q12FF')==1)
    % QM12FF
    BEAMLINE{1642}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1643}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1644}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1645}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1646}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1647}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1648}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q11FF')==1)
    % QM11FF
    BEAMLINE{1654}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1655}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1656}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1657}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1658}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1659}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1660}.Offset = [offset_x 0 offset_y 0 0 roll];
elseif(strcmp(element_name, 'Q10FF')==1)
    % QD10BFF
    BEAMLINE{1664}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1665}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1676}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1677}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1668}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1669}.Offset = [offset_x 0 offset_y 0 0 roll];
    BEAMLINE{1670}.Offset = [offset_x 0 offset_y 0 0 roll];
end

% ZV1FF compensation
%PS(473).Ampl = -1.5e-6; % for ZV11X
%PS(473).Ampl = -1.5e-6; % for ZV10X

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Track the beam and extract the BPM readings %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[stat,raw]=FlHwUpdate();
[stat,raw]=FlHwUpdate('readbuffer',1);
bpm_readings_after_x = raw(:,raw_index_long*3-1);
bpm_readings_after_y = raw(:,raw_index_long*3);
bpm_readings_full_after_y = raw(:,raw_index_full*3);
ip_offset_after_y = raw(:,raw_index_ip*3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Remove the imperfections %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Extraction kicker: KEX1A, KEX1B
BEAMLINE{1286}.Tilt = 0;
BEAMLINE{1290}.Tilt = 0;
BEAMLINE{1286}.Offset = [0 0 0 0 0 0];
BEAMLINE{1290}.Offset = [0 0 0 0 0 0];

% QM6RX
BEAMLINE{1293}.Offset = [0 0 0 0 0 0];
BEAMLINE{1294}.Offset = [0 0 0 0 0 0];

% ZH100RX
BEAMLINE{1296}.Offset = [0 0 0 0 0 0];
PS(345).Ampl = 0;

% ZH101RX
BEAMLINE{1298}.Offset = [0 0 0 0 0 0];
PS(346).Ampl = 0;

% ZV100RX
BEAMLINE{1300}.Offset = [0 0 0 0 0 0];
PS(410).Ampl = 0;

% QM7RX
BEAMLINE{1304}.Offset = [0 0 0 0 0 0];
BEAMLINE{1305}.Offset = [0 0 0 0 0 0];

% BS1XA
BEAMLINE{1307}.Offset = [0 0 0 0 0 0];
BEAMLINE{1308}.Offset = [0 0 0 0 0 0];

% BS2XA
BEAMLINE{1310}.Offset = [0 0 0 0 0 0];
BEAMLINE{1311}.Offset = [0 0 0 0 0 0];

% BS3XA
BEAMLINE{1315}.Offset = [0 0 0 0 0 0];
BEAMLINE{1316}.Offset = [0 0 0 0 0 0];

% ZV1X
BEAMLINE{1318}.Offset = [0 0 0 0 0 0];
PS(462).Ampl = 0;

% QS1X
BEAMLINE{1320}.Offset = [0 0 0 0 0 0];
BEAMLINE{1321}.Offset = [0 0 0 0 0 0];
PS(1).Ampl = 0;


% QF1X
BEAMLINE{1323}.Offset = [0 0 0 0 0 0];
BEAMLINE{1324}.Offset = [0 0 0 0 0 0];

% QD2X
BEAMLINE{1340}.Offset = [0 0 0 0 0 0];
BEAMLINE{1341}.Offset = [0 0 0 0 0 0];

% QF3X
BEAMLINE{1345}.Offset = [0 0 0 0 0 0];
BEAMLINE{1346}.Offset = [0 0 0 0 0 0];

% QF4X
BEAMLINE{1356}.Offset = [0 0 0 0 0 0];
BEAMLINE{1357}.Offset = [0 0 0 0 0 0];

% QD5X
BEAMLINE{1365}.Offset = [0 0 0 0 0 0];
BEAMLINE{1366}.Offset = [0 0 0 0 0 0];

% QF6X
BEAMLINE{1382}.Offset = [0 0 0 0 0 0];
BEAMLINE{1383}.Offset = [0 0 0 0 0 0];

% QF7X
BEAMLINE{1393}.Offset = [0 0 0 0 0 0];
BEAMLINE{1394}.Offset = [0 0 0 0 0 0];

% QD8X
BEAMLINE{1400}.Offset = [0 0 0 0 0 0];
BEAMLINE{1401}.Offset = [0 0 0 0 0 0];

% QF9X
BEAMLINE{1417}.Offset = [0 0 0 0 0 0];
BEAMLINE{1418}.Offset = [0 0 0 0 0 0];

% Q10X
BEAMLINE{1431}.Offset = [0 0 0 0 0 0];
BEAMLINE{1432}.Offset = [0 0 0 0 0 0];
BEAMLINE{1433}.Offset = [0 0 0 0 0 0];
BEAMLINE{1434}.Offset = [0 0 0 0 0 0];
BEAMLINE{1435}.Offset = [0 0 0 0 0 0];
BEAMLINE{1436}.Offset = [0 0 0 0 0 0];

% Q11X
BEAMLINE{1442}.Offset = [0 0 0 0 0 0];
BEAMLINE{1443}.Offset = [0 0 0 0 0 0];
BEAMLINE{1444}.Offset = [0 0 0 0 0 0];
BEAMLINE{1445}.Offset = [0 0 0 0 0 0];
BEAMLINE{1446}.Offset = [0 0 0 0 0 0];
BEAMLINE{1447}.Offset = [0 0 0 0 0 0];
BEAMLINE{1448}.Offset = [0 0 0 0 0 0];

% Q12X
BEAMLINE{1461}.Offset = [0 0 0 0 0 0];
BEAMLINE{1462}.Offset = [0 0 0 0 0 0];
BEAMLINE{1463}.Offset = [0 0 0 0 0 0];
BEAMLINE{1464}.Offset = [0 0 0 0 0 0];
BEAMLINE{1465}.Offset = [0 0 0 0 0 0];
BEAMLINE{1466}.Offset = [0 0 0 0 0 0];

% Q13X
BEAMLINE{1472}.Offset = [0 0 0 0 0 0];
BEAMLINE{1473}.Offset = [0 0 0 0 0 0];

% QD14X
BEAMLINE{1477}.Offset = [0 0 0 0 0 0];
BEAMLINE{1478}.Offset = [0 0 0 0 0 0];
BEAMLINE{1479}.Offset = [0 0 0 0 0 0];

% Q15X
BEAMLINE{1487}.Offset = [0 0 0 0 0 0];
BEAMLINE{1488}.Offset = [0 0 0 0 0 0];

% ZV9X
BEAMLINE{1492}.Offset = [0 0 0 0 0 0];
PS(470).Ampl = 0;

% Q16X
BEAMLINE{1497}.Offset = [0 0 0 0 0 0];
BEAMLINE{1498}.Offset = [0 0 0 0 0 0];
BEAMLINE{1499}.Offset = [0 0 0 0 0 0];
BEAMLINE{1500}.Offset = [0 0 0 0 0 0];
BEAMLINE{1501}.Offset = [0 0 0 0 0 0];
BEAMLINE{1502}.Offset = [0 0 0 0 0 0];

% Q17X
BEAMLINE{1508}.Offset = [0 0 0 0 0 0];
BEAMLINE{1509}.Offset = [0 0 0 0 0 0];
BEAMLINE{1510}.Offset = [0 0 0 0 0 0];
BEAMLINE{1511}.Offset = [0 0 0 0 0 0];
BEAMLINE{1512}.Offset = [0 0 0 0 0 0];
BEAMLINE{1513}.Offset = [0 0 0 0 0 0];

% ZV10X
BEAMLINE{1522}.Offset = [0 0 0 0 0 0];
PS(471).Ampl = 0;

% QD18X
BEAMLINE{1524}.Offset = [0 0 0 0 0 0];
BEAMLINE{1525}.Offset = [0 0 0 0 0 0];
BEAMLINE{1526}.Offset = [0 0 0 0 0 0];
BEAMLINE{1527}.Offset = [0 0 0 0 0 0];
BEAMLINE{1528}.Offset = [0 0 0 0 0 0];
BEAMLINE{1529}.Offset = [0 0 0 0 0 0];

% QF19X
BEAMLINE{1539}.Offset = [0 0 0 0 0 0];
BEAMLINE{1540}.Offset = [0 0 0 0 0 0];
BEAMLINE{1541}.Offset = [0 0 0 0 0 0];
BEAMLINE{1542}.Offset = [0 0 0 0 0 0];
BEAMLINE{1543}.Offset = [0 0 0 0 0 0];
BEAMLINE{1544}.Offset = [0 0 0 0 0 0];

% ZV11X
BEAMLINE{1554}.Offset = [0 0 0 0 0 0];
PS(472).Ampl = 0;

% QD20X
BEAMLINE{1562}.Offset = [0 0 0 0 0 0];
BEAMLINE{1563}.Offset = [0 0 0 0 0 0];
BEAMLINE{1564}.Offset = [0 0 0 0 0 0];

% QF21X
BEAMLINE{1572}.Offset = [0 0 0 0 0 0];
BEAMLINE{1573}.Offset = [0 0 0 0 0 0];

% QM16FF
BEAMLINE{1591}.Offset = [0 0 0 0 0 0];
BEAMLINE{1592}.Offset = [0 0 0 0 0 0];
BEAMLINE{1593}.Offset = [0 0 0 0 0 0];
BEAMLINE{1594}.Offset = [0 0 0 0 0 0];
BEAMLINE{1595}.Offset = [0 0 0 0 0 0];
BEAMLINE{1596}.Offset = [0 0 0 0 0 0];
BEAMLINE{1597}.Offset = [0 0 0 0 0 0];

% ZV1FF
BEAMLINE{1601}.Offset = [0 0 0 0 0 0];
PS(473).Ampl = 0;

% QM15FF
BEAMLINE{1603}.Offset = [0 0 0 0 0 0];
BEAMLINE{1604}.Offset = [0 0 0 0 0 0];
BEAMLINE{1605}.Offset = [0 0 0 0 0 0];
BEAMLINE{1606}.Offset = [0 0 0 0 0 0];
BEAMLINE{1607}.Offset = [0 0 0 0 0 0];
BEAMLINE{1608}.Offset = [0 0 0 0 0 0];
BEAMLINE{1609}.Offset = [0 0 0 0 0 0];

% QM14FF
BEAMLINE{1613}.Offset = [0 0 0 0 0 0];
BEAMLINE{1614}.Offset = [0 0 0 0 0 0];
BEAMLINE{1615}.Offset = [0 0 0 0 0 0];
BEAMLINE{1616}.Offset = [0 0 0 0 0 0];
BEAMLINE{1617}.Offset = [0 0 0 0 0 0];
BEAMLINE{1618}.Offset = [0 0 0 0 0 0];
BEAMLINE{1619}.Offset = [0 0 0 0 0 0];

% QM13FF
BEAMLINE{1632}.Offset = [0 0 0 0 0 0];
BEAMLINE{1633}.Offset = [0 0 0 0 0 0];
BEAMLINE{1634}.Offset = [0 0 0 0 0 0];
BEAMLINE{1635}.Offset = [0 0 0 0 0 0];
BEAMLINE{1636}.Offset = [0 0 0 0 0 0];
BEAMLINE{1637}.Offset = [0 0 0 0 0 0];
BEAMLINE{1638}.Offset = [0 0 0 0 0 0];

% QM12FF
BEAMLINE{1642}.Offset = [0 0 0 0 0 0];
BEAMLINE{1643}.Offset = [0 0 0 0 0 0];
BEAMLINE{1644}.Offset = [0 0 0 0 0 0];
BEAMLINE{1645}.Offset = [0 0 0 0 0 0];
BEAMLINE{1646}.Offset = [0 0 0 0 0 0];
BEAMLINE{1647}.Offset = [0 0 0 0 0 0];
BEAMLINE{1648}.Offset = [0 0 0 0 0 0];

% QM11FF
BEAMLINE{1654}.Offset = [0 0 0 0 0 0];
BEAMLINE{1655}.Offset = [0 0 0 0 0 0];
BEAMLINE{1656}.Offset = [0 0 0 0 0 0];
BEAMLINE{1657}.Offset = [0 0 0 0 0 0];
BEAMLINE{1658}.Offset = [0 0 0 0 0 0];
BEAMLINE{1659}.Offset = [0 0 0 0 0 0];
BEAMLINE{1660}.Offset = [0 0 0 0 0 0];

% QD10BFF
BEAMLINE{1664}.Offset = [0 0 0 0 0 0];
BEAMLINE{1665}.Offset = [0 0 0 0 0 0];
BEAMLINE{1666}.Offset = [0 0 0 0 0 0];
BEAMLINE{1667}.Offset = [0 0 0 0 0 0];
BEAMLINE{1668}.Offset = [0 0 0 0 0 0];
BEAMLINE{1669}.Offset = [0 0 0 0 0 0];
BEAMLINE{1670}.Offset = [0 0 0 0 0 0];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot the result together with the detected beam motion %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sim_data = bpm_readings_after_y' - bpm_readings_before_y';

remaining_oscillation_130410 = load('oscillation_130410.dat');
remaining_oscillation_130524 = load('oscillation_130524.dat');
remaining_oscillation_130410_long = load('oscillation_130410_long.dat');
remaining_oscillation_130524_long = load('oscillation_130524_long.dat');
remaining_oscillation_130618 = load('oscillation_130618_0453_long.dat');

t_130410 = remaining_oscillation_130410(:,1);
meas_data1_130410 = remaining_oscillation_130410(:,2);
meas_data2_130410 = remaining_oscillation_130410(:,3);
sigma_y_130410 = remaining_oscillation_130410(:,4);
meas_data1_norm_130410 = meas_data1_130410./sigma_y_130410;
meas_data2_norm_130410 = meas_data2_130410./sigma_y_130410;
sim_data_norm_130410 = sim_data(18:end)./sigma_y_130410;

t_130524 = remaining_oscillation_130524(:,1);
meas_data1_130524 = remaining_oscillation_130524(:,2);
meas_data2_130524 = remaining_oscillation_130524(:,3);
sigma_y_130524 = remaining_oscillation_130524(:,4);
meas_data1_norm_130524 = meas_data1_130524./sigma_y_130524;
meas_data2_norm_130524 = meas_data2_130524./sigma_y_130524;
sim_data_norm_130524 = sim_data(18:end)./sigma_y_130524;

t_130410_long = remaining_oscillation_130410_long(:,1);
meas_data1_130410_long = remaining_oscillation_130410_long(:,2);
meas_data2_130410_long = remaining_oscillation_130410_long(:,3);
sigma_y_130410_long = remaining_oscillation_130410_long(:,4);
meas_data1_norm_130410_long = meas_data1_130410_long./sigma_y_130410_long;
meas_data2_norm_130410_long = meas_data2_130410_long./sigma_y_130410_long;
sim_data_norm_130410_long = sim_data./sigma_y_130410_long;

t_130524_long = remaining_oscillation_130524_long(:,1);
meas_data1_130524_long = remaining_oscillation_130524_long(:,2);
meas_data2_130524_long = remaining_oscillation_130524_long(:,3);
sigma_y_130524_long = remaining_oscillation_130524_long(:,4);
meas_data1_norm_130524_long = meas_data1_130524_long./sigma_y_130524_long;
meas_data2_norm_130524_long = meas_data2_130524_long./sigma_y_130524_long;
sim_data_norm_130524_long = sim_data./sigma_y_130524_long;

t_130618 = remaining_oscillation_130618(:,1);
meas_data1_130618 = remaining_oscillation_130618(:,2);
meas_data2_130618 = remaining_oscillation_130618(:,3);
sigma_y_130618 = remaining_oscillation_130618(:,4);
meas_data1_norm_130618 = meas_data1_130618./sigma_y_130618;
meas_data2_norm_130618 = meas_data2_130618./sigma_y_130618;
sim_data_norm_130618 = sim_data./sigma_y_130618;

figure(3);
set(gca,'FontSize',18);
plot(bpm_numbers_long, meas_data2_norm_130410_long.*100, 'x-b', 'Linewidth', 1.5);
hold on;
grid on;
plot(bpm_numbers_long, sim_data_norm_130410_long.*100, 'x-r', 'Linewidth', 1.5);
legend('meas. data', 'sim. data');
xlabel('BPM nr. [1]');
ylabel('$$y_{S2}/\sigma_{y0}$$ [\%]');
axis([bpm_numbers(1)-2, bpm_numbers(end)+2, -10, 10]);
hold off;

figure(4);
set(gca,'FontSize',18);
%plot(bpm_numbers, sim_data_norm_130410.*100, 'x-r', 'Linewidth', 1.5);
hold on;
grid on;
%plot(bpm_numbers, meas_data1_norm_130410.*100, 'x-b', 'Linewidth', 1.5);
%plot(bpm_numbers, meas_data2_norm_130410.*100, 'x-k', 'Linewidth', 1.5);
%plot(bpm_numbers, -meas_data1_norm_130524.*100, 'x--b', 'Linewidth', 1.5);
%plot(bpm_numbers, -meas_data2_norm_130524.*100, 'x--k', 'Linewidth', 1.5);
plot(bpm_numbers_long, -meas_data1_norm_130410_long.*100, 'x-r', 'Linewidth', 1.5);
plot(bpm_numbers_long, meas_data2_norm_130410_long.*100, 'x-b', 'Linewidth', 1.5);
plot(bpm_numbers_long, -meas_data1_norm_130524_long.*100, 'x--m', 'Linewidth', 1.5);
plot(bpm_numbers_long, -meas_data2_norm_130524_long.*100, 'x--c', 'Linewidth', 1.5);
plot(bpm_numbers_long, -meas_data1_norm_130618.*100, 'x-g', 'Linewidth', 1.5);
plot(bpm_numbers_long, meas_data2_norm_130618.*100, 'x--g', 'Linewidth', 1.5);
plot(bpm_numbers_long, sim_data_norm_130410_long.*100, 'x-k', 'Linewidth', 1.5);
legend('meas. data', 'sim. data');
xlabel('BPM nr. [1]');
ylabel('$$y_{S2}/\sigma_{y0}$$ [\%]');
%axis([bpm_numbers_long(1)-2, bpm_numbers_long(end)+2, -100, 100]);
hold off;

%figure(5);
%plot(bpm_readings_full_after_y - bpm_readings_full_before_y, 'x-b');
%grid on;

%figure(6);
%plot(bpm_readings_full_after_y, 'x-r');
%grid on;
%hold on;
%plot(bpm_readings_full_before_y, 'x-b');
%hold off;

proposed_factor1 = mean(abs(meas_data1_130410))/mean(abs(sim_data))
proposed_factor2 = mean(abs(meas_data2_130410))/mean(abs(sim_data))

beam_size_ip = 37e-9;
ip_offset = (ip_offset_after_y - ip_offset_before_y);
ip_offset_relative = ip_offset/beam_size_ip*100
expected_beam_size = sqrt(beam_size_ip^2 + ip_offset^2)*10^9