% Script to analyse the data from ATF in more detail 
%
% Juergen Pfingstner
% 28. February 2013


%%%%%%%%%%%%%
% Parameter %
%%%%%%%%%%%%%

%file_name_base = 'epics_all_orbit_130514_1130';
%file_name_base = 'epics_all_orbit_130410_0040';
%file_name_base = 'epics_all_orbit_130409_1840';
%file_name_base = 'epics_all_orbit_130315_0545';
%file_name_base = 'epics_all_orbit_fb_130307_V1';
%file_name_base = 'epics_all_orbit_fb_121218_V1_new';
%file_name_base = 'epics_all_orbit_fb_121218_V1_delete';
%file_name_base = 'epics_all_orbit_fb_121217_V2';
%string_color = 'r';

use_x_or_y = 'y';

use_initialisation = 1;
use_data_inspection = 0;
use_noise_level_calc = 1;
use_svd_cleaning = 0;
use_deg_freedom_plot = 1;
use_correlation_detection = 1;

use_removed_motion_analysis_space = 0;
use_removed_motion_analysis_time = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_initialisation == 1)

    file_name_x= sprintf('./data/%s_x_mod.dat', file_name_base);
    file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
    x0 = load(file_name_x, '-ASCII');
    y0 = load(file_name_y, '-ASCII');
    
    file_name_model= sprintf('./data/model_%s_mod.dat', file_name_base);
    model_data = load(file_name_model, '-ASCII');
    beta_x = model_data(:,1)';
    beta_y = model_data(:,2)';
    eta_x = model_data(:,3)';
    eta_y = model_data(:,4)';
    nu_x = model_data(:,5)';
    nu_y = model_data(:,6)';
    sigma_x = model_data(:,7)';
    sigma_y = model_data(:,8)';
    s_bpm = model_data(:,9)';
    ref_noise = model_data(:,10)';
    
    t=[1:size(y0,2)];

    %Let's only take BPMs in the ATF2 beam line
    x0 = x0(:,91:end-2);
    y0 = y0(:,91:end-2);
    sigma_x = sigma_x(91:end-2)';
    sigma_y = sigma_y(91:end-2)';
    ref_noise = ref_noise(:,91:end-2)';
    nu_y = nu_y(91:end-2');
    beta_y = beta_y(91:end-2)';
    index_bpm = 91:134;
    scale_bpm_min = 90;
    scale_bpm_max = 137;
    
    % Debug
    save_data = [std(x0)', std(y0)'];
    save('jitter_save_2013June.dat','save_data','-ascii');
    % dEBUG 
    
    x0(:,34) = [];
    y0(:,34) = [];
    sigma_x(34) = [];
    sigma_y(34) = [];
    nu_y(34) = [];
    beta_y(34) = [];
    ref_noise(34) = [];
    index_bpm(34) = [];
    
    x0(:,23) = [];
    y0(:,23) = [];
    nu_y(23) = [];
    beta_y(23) = [];
    sigma_x(23) = [];
    sigma_y(23) = [];
    ref_noise(23) = [];
    index_bpm(23) = [];
    
    % This is an emergency solution since at at the measurement
    % 20/27 November 2013 the stripline BPMs did not work 
    %x0 = x0(:,106:end-2);
    %y0 = y0(:,106:end-2);
    %sigma_x = sigma_x(106:end-2)';
    %sigma_y = sigma_y(106:end-2)';
    %ref_noise = ref_noise(:,106:end-2)';
    %nu_y = nu_y(106:end-2)';
    %beta_y = beta_y(106:end-2)';
    %index_bpm = 106:134;
    %scale_bpm_min = 106;
    %scale_bpm_max = 137;
    
    %Let's only take BPMs in the cavity BPM section of the ATF2 beam line
    %x0 = x0(:,108:end-2);
    %y0 = y0(:,108:end-2);
    %sigma_x = sigma_x(108:end-2)';
    %sigma_y = sigma_y(108:end-2)';
    %nu_y = nu_y(108:end-2)';
    %beta_y = beta_y(108:end-2)';
    %ref_noise = ref_noise(:,108:end-2)';
    %index_bpm = 108:134;
    
    %x0(:,17) = [];
    %y0(:,17) = [];
    %sigma_x(17) = [];
    %sigma_y(17) = [];
    %nu_y(17) = [];
    %beta_y(17) = [];
    %ref_noise(17) = [];
    %index_bpm(17) = [];
    
    %x0(:,11) = [];
    %y0(:,11) = [];
    %sigma_x(11) = [];
    %sigma_y(11) = [];
    %nu_y(11) = [];
    %beta_y(11) = [];
    %ref_noise(11) = [];
    %index_bpm(11) = [];
    
    %x0(:,7) = [];
    %y0(:,7) = [];
    %sigma_x(7) = [];
    %sigma_y(7) = [];
    %nu_y(7) = [];
    %beta_y(7) = [];
    %ref_noise(7) = [];
    %index_bpm(7) = [];
    
    %x0(:,6) = [];
    %y0(:,6) = [];
    %sigma_x(6) = [];
    %sigma_y(6) = [];
    %nu_y(6) = [];
    %beta_y(6) = [];
    %ref_noise(6) = [];
    %index_bpm(6) = [];
    
    scale_bpm_min = 106;
    scale_bpm_max = 136;
    %y0 = y0*diag(1./sigma_y);

    if(strcmp(use_x_or_y,'x')==1)
        y0 = x0;
        sigma_y = sigma_x;
        beta_y = beta_x;
        eta_y = eta_x;
        nu_y = nu_x;
    end
    
     % Some initial quantities

    R_y0 = corr(y0);
    R_xy = corr(x0,y0);
    std_y0 = std(y0);
    [U_y0, S_y0, V_y0] = svd(y0);
    [nr_time_steps, nr_bpms] = size(y0);
    
    % Debug
    %for i_loop = 1:nr_bpms
    %    y0(:,i_loop) = y0(:,i_loop)./std(y0(:,i_loop));
    %end
    % dEBUG
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_data_inspection == 1)
    
    %figure(1);
    %%plot(index_bpm, y0(1,:)./sigma_y', 'x-b');
    %y0_mod = y0*diag(1./sigma_y);
    %plot(index_bpm, y0_mod(1:10,:), 'x-');
    %grid on;
    
    % Initial conclusion (strange word combination): It is hardly possible
    % to conclude something from the rare data. MIA or other analysis
    % techniques will be necessary.
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_noise_level_calc == 1)

    % noise determination in two ways

    % 1.) alla Young-Im

    Res1 = zeros(size(y0));
    data1_temp = zeros(length(index_bpm), 1);
    %data2_temp = zeros(length(index_bpm), length(index_bpm)-1);    
    for i=1:length(index_bpm);
        data1_temp = y0(:,i);
        data2_temp = y0;
        data2_temp(:,i) = [];
        V = pinv(data2_temp)*data1_temp;
        Res1(:,i) = data1_temp - data2_temp*V;
    end
    Res1 = std(Res1);

    %% 2.) alla myself -> did not work
    Temp_matrix = ((std_y0)'*(1./std_y0)).*R_y0;
    %Temp_matrix = cov(y0)*diag(1./(std_y0.^2));
    for i=1:length(index_bpm)
        Temp_matrix(i,i) = 0;
    end
    %Temp_matrix = sum(Temp_matrix,2);
    %Temp_matrix = Temp_matrix.*std_y0';
    %Temp_matrix = ones(size(Temp_matrix)) - Temp_matrix;
    %Res2 = y0*diag(Temp_matrix);

    %Temp_matrix = eye(length(index_bpm)) - Temp_matrix;
    Res2 = y0-y0*Temp_matrix;
    Res2 = std(Res2);

    % 3.) alla myself -> the second try: Remove on correlation after the other
    Res3 = y0;
    for i=1:nr_bpms
    %for i=(112-index_bpm(1)+1)
        % plot the change
        %plot(index_bpm, std(Res3), 'x-c');
        %keyboard
    
        % Subtract the correlation of the ith BPM
        C_temp = cov(Res3);
        k_temp = C_temp(i,:)./(std(Res3(:,i)).^2);
        k_temp(i) = 0;
        subdata1 = Res3(:,i)*k_temp;
        %Res3 = Res3 - Res3(:,i)*k_temp;
        %C_temp = cov(y0);
        %k_temp = C_temp(i,:)./(std(y0(:,i)).^2);
        %k_temp(i) = 0;
        %Res3 = Res3 - y0(:,i)*k_temp;
    
        % The correlated motion is taken out from all other BPMs, but it
        % remains in the BPM itself. So also here it has to be taken out. 
        data1_temp = Res3(:,i);
        data2_temp = Res3;
        data2_temp(:,i) = [];
        V = pinv(data2_temp)*data1_temp;
        subdata2 = data2_temp*V;
        %Res3(:,i) = data1_temp - data2_temp*V;
    
        Res3 = Res3 - subdata1;
        %Res3(:,i) = Res3(:,i) - subdata2;
    end

    %figure(102);
    %plot(index_bpm, Res1, 'x-r');
    %grid on;
    %hold on;
    %plot(index_bpm, std_y0, 'x-b');
    %%plot(index_bpm, Res2, 'x-m')
    %plot(index_bpm, std(Res3), 'x-m');
    
    % For the plot in a loop
    figure(1020);
    plot_style_temp = strcat('x-',string_color);
    plot(index_bpm, std_y0, plot_style_temp);
    grid on;
    hold on;
    plot_style_temp = strcat('x--',string_color);
    plot(index_bpm, Res1, plot_style_temp);
    %plot(index_bpm, Res2, 'x-m')
    %plot_style_temp = strcat('x:',string_color);
    %plot(index_bpm, std(Res3), plot_style_temp);
    xlabel('BPM nr. [1]');
    ylabel('$$\sigma_y$$ [m]');
    %legend('BPM signal', 'BPM noise Young-Im', 'BPM noise me');
    legend('BPM signal', 'BPM noise');
    %axis([scale_bpm_min scale_bpm_max 0 1e-5]);
    
    % For the plot in a loop
    figure(1021);
    plot_style_temp = strcat('x-',string_color);
    plot(index_bpm, std_y0./sigma_y'.*100, plot_style_temp);
    grid on;
    hold on;
    plot_style_temp = strcat('x--',string_color);
    plot(index_bpm, Res1./sigma_y'.*100, plot_style_temp);
    %plot(index_bpm, Res2./sigma_y, 'x-m')
    %plot_style_temp = strcat('x:',string_color);
    %plot(index_bpm, std(Res3), plot_style_temp);
    xlabel('BPM nr. [1]');
    ylabel('$$\sigma_y$$ [m]');
    %legend('BPM signal', 'BPM noise Young-Im', 'BPM noise me');
    legend('BPM signal', 'BPM noise');
    %axis([scale_bpm_min scale_bpm_max 0 6e-5]);
    
    figure(2000);
    plot(index_bpm, nu_y);
    
    figure(2001);
    plot(index_bpm, beta_y);
    
    % Plot for paper
    %figure(3);
    %set(gca,'FontSize',18);
    %plot(index_bpm, std_y0*1e6, 'x-b', 'Linewidth', 1.5);
    %grid on;    
    %hold on;
    %plot(index_bpm, Res1*1e6, 'x-r', 'Linewidth', 1.5);
    %xlabel('BPM nr. [1]');
    %ylabel('$$\sigma_y$$ [$$\mu$$m]');
    %legend('BPM signal', 'BPM noise');
    %axis([90 136 0 70]);
    
    % First conclusion: By taking out the motion Hector and me are doing it,
    % the motion at the BPM itself if not take out. Also after the motion in
    % the other BPMs is removed, this correlation is missing for the removal of
    % the motion in the current BPM. Also the fix I tried is not really
    % working (). Do not really know why, but do not want to spend more time 
    % on it. However, the error for our evaluation BPMs is small (compare red
    % and pink curve). 
    %
    % Also note the the correlation has to be taken out for each BPM
    % individual, since otherwise the same correlation is removed several
    % times, for the different BPMs. 
    %
    % Everything is not completely clear, but it should work for our analysis!
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_svd_cleaning == 1)
    
    % Clean the matrix from BPMs with large noise
    
    S = S_y0;
    V = V_y0;
    U = U_y0;
    
    treshhold = 0.8;
    s = diag(S);
    for i=1:nr_bpms
        if(max(abs(V(:,i))>treshhold))
           fprintf(1,'Removing BPM SVD direction %i\n',i);
           s(i) = 0; 
        end
    end    
    S = zeros(nr_time_steps, nr_bpms);
    S(1:nr_bpms,1:nr_bpms) = diag(s);
    y1 = U*S*V';
    [U_y1, S_y1, V_y1] = svd(y1);
    
    % Cut the noise floor (where exactly is difficult)
    
    S = S_y1;
    s = diag(S);
    V = V_y1;
    U = U_y1;
    
    %nr_sv_cut = 11;
    nr_sv_cut = 11;
    s(nr_sv_cut:end) = zeros(1,nr_bpms-nr_sv_cut+1);
    S = zeros(nr_time_steps, nr_bpms);
    S(1:nr_bpms,1:nr_bpms) = diag(s);
    y2 = U*S*V';
    [U_y2, S_y2, V_y2] = svd(y2);    
    
    % Take a look at the directions
    
    print_col = 3;
    nr_plot = 1;
   
    %S = S_y0;
    %V = V_y0;
    %U = U_y0;
    
    %S = S_y1;
    %V = V_y1;
    %U = U_y1;
    
    S = S_y2;
    V = V_y2;
    U = U_y2;
    
    %figure(nr_plot);
    %plot(diag(S), 'x-b');
    %grid on;
    
    %figure(nr_plot+1);
    %plot(index_bpm, V(:,1)./sigma_y,'-b');
    %grid on;
    %hold on;
    %plot(index_bpm, V(:,2)./sigma_y,'-r');
    
    %figure(nr_plot+2);
    %plot(index_bpm, V(:,print_col)./sigma_y,'-b');
    %grid on;
    %hold on;
    %plot(index_bpm, V(:,print_col+1)./sigma_y,'-r');  
    
    %figure(nr_plot);
    %plot(diag(S), 'x-b');
    %grid on;
    
    %figure(nr_plot+1);
    %plot(index_bpm, V(:,1),'-b');
    %grid on;
    %hold on;
    %plot(index_bpm, V(:,2),'-r');
    
    %figure(nr_plot+2);
    %plot(index_bpm, V(:,print_col),'-b');
    %grid on;
    %hold on;
    %plot(index_bpm, V(:,print_col+1),'-r');
    
end

if(use_deg_freedom_plot == 1)

    if(use_svd_cleaning == 1)
        [DoF_data] = calc_DoF_data(y2);
    else
        [DoF_data] = calc_DoF_data(y0);
    end
    
    nr_sv_plot = 10;
    %ref_sv = DoF_data(1, :);
    ref_sv = sum(DoF_data);
    figure(40);
    plot_style_temp = strcat('x-',string_color);
    plot(index_bpm, DoF_data(1:nr_sv_plot,:)*diag(1./ref_sv), plot_style_temp);
    grid on;
    hold on;
    
    figure(50);
    plot_style_temp = strcat('x-',string_color);
    plot(index_bpm, DoF_data(1:nr_sv_plot,:)*1000, plot_style_temp);
    grid on;
    hold on;
    ylabel('Unnorm. singular values [mm]');
    xlabel('BPM nr. [1]');
    %axis([108 135 0 7]);
    
    % Second conclusion: One can see changes in the SV around the
    % identified sources, but the location is not at a clear location.
    % This does not make the analysis much clearer but adds other
    % indications. We will have to determine the location also with the
    % displacement plot to check. But at least it somehow fits with the
    % correlation results, even though there seems to be some beam motion
    % for higher BPMs. 
    
end

if(use_correlation_detection == 1)

    if(use_svd_cleaning == 1)
        R_y2 = corr(y2);
    end
    
    % Making the matrix artificially triangular
    %for i=1:nr_bpms-1
    %   R_y2(i,i+1:end) = zeros(1,nr_bpms-i); 
    %end
    
    %figure(6);
    %surf(R_y2);
    
    if(use_svd_cleaning == 1)
        [U_r2, S_r2, V_r2] = svd(R_y2);
    else
        [U_r2, S_r2, V_r2] = svd(R_y0);
    end
    
    %figure(7);
    %plot(diag(S_r2), 'x-b');
    %grid on;
    
    %figure(108);
    %plot(index_bpm, V_r2(:,1),'x-b');
    %grid on;
    %hold on;
    %plot(index_bpm, V_r2(:,2),'x-r');
    %plot(index_bpm, V_r2(:,3),'x-g');
    %%plot(index_bpm, V_r2(:,4),'x-k');
    %%plot(index_bpm, V_r2(:,5),'x-m');
    
    if(use_svd_cleaning == 1)
        [DoF_data] = calc_DoF_data(R_y2);
    else
        [DoF_data] = calc_DoF_data(R_y0);
    end
    %[DoF_data] = calc_DoF_data(R_y0);
    %[DoF_data] = calc_DoF_data(R_xy);
    
    nr_sv_plot = 10;
    %ref_sv = DoF_data(1, :);
    ref_sv = sum(DoF_data);
    %figure(9);
    %plot(index_bpm, DoF_data(1:nr_sv_plot,:)*diag(1./ref_sv), 'x-m');
    %%plot(index_bpm, DoF_data(1,:)*diag(1./ref_sv), 'x-m');
    %grid on;
    %hold on;
    %%plot(index_bpm, sum(DoF_data(2:nr_sv_plot,:)*diag(1./ref_sv)), 'x-m');
    
    %figure(10);
    %plot(index_bpm, DoF_data(1:nr_sv_plot,:), 'x-m');
    %%plot(index_bpm, DoF_data(1,:), 'x-m');
    %grid on;
    %hold on;
    %%plot(index_bpm, sum(DoF_data(2:nr_sv_plot,:)), 'x-m');
    %ylabel('Unnorm. singular values [1]');
    %xlabel('BPM nr. [1]');
    %axis([108 135 0 18]);
    
    %figure(900);
    %plot_style_temp = strcat('x-',string_color);
    %plot(index_bpm, DoF_data(1:nr_sv_plot,:)*diag(1./ref_sv), plot_style_temp);
    %grid on;
    %hold on;
    %ylabel('Norm. singular values [1]');
    %xlabel('BPM nr. [1]');
    %axis([scale_bpm_min scale_bpm_max 0 1.05]);
    
    %figure(901);
    %plot_style_temp = strcat('x-',string_color);
    %plot(index_bpm, DoF_data(1,:)*diag(1./ref_sv), plot_style_temp);
    %grid on;
    %hold on;
    %plot(index_bpm, sum(DoF_data(2:nr_sv_plot,:))*diag(1./ref_sv), plot_style_temp);
    %ylabel('Norm. singular values [1]');
    %xlabel('BPM nr. [1]');
    %axis([scale_bpm_min scale_bpm_max 0 1.05]);
    
    figure(1000);
    plot_style_temp = strcat('x-',string_color);
    plot(index_bpm, DoF_data(1:nr_sv_plot,:), plot_style_temp);
    %plot(index_bpm, DoF_data(1,:), 'x-m');
    grid on;
    hold on;
    %plot(index_bpm, sum(DoF_data(2:nr_sv_plot,:)), 'x-m');
    ylabel('Unnorm. singular values [1]');
    xlabel('BPM nr. [1]');
    axis([scale_bpm_min scale_bpm_max 0 20]);
    
    % For presentation about charge dependence measurement
    %figure(1001);
    %set(gca,'FontSize',18);
    %plot_style_temp = strcat('x-',string_color);
    %plot(index_bpm, DoF_data(1,:), plot_style_temp, 'Linewidth', 1.5);
    %grid on;
    %hold on;
    %plot_style_temp = strcat('x--',string_color);
    %plot(index_bpm, sum(DoF_data(2:nr_sv_plot,:)), plot_style_temp, 'Linewidth', 1.5);
    %ylabel('DoF singular values [$$\mu$$m]');
    %xlabel('BPM nr. [1]');
    %%axis([106 136 0 15]);
    %axis([scale_bpm_min scale_bpm_max 0 20]);
    %legend('$$s_1, N=3\times 10^9$$','$$\sum_{i=2}^{10} s_i,
    %N=3\times 10^9$$','$$s_1, N=4\times 10^9$$','$$\sum_{i=2}^{10}
    %s_i, N=4\times 10^9$$','$$s_1, N=5\times
    %10^9$$','$$\sum_{i=2}^{10} s_i, N=5\times 10^9$$','$$s_1,
    %N=6\times 10^9$$','$$\sum_{i=2}^{10} s_i, N=6\times 10^9$$');
    
    % For presentation about source exchange 18th of June 2013
    figure(1001);
    set(gca,'FontSize',18);
    plot_style_temp = strcat('x-',string_color);
    %plot(index_bpm, DoF_data(1,:), plot_style_temp, 'Linewidth', 1.5);
    grid on;
    hold on;
    plot_style_temp = strcat('x-',string_color);
    plot(index_bpm, sum(DoF_data(2:nr_sv_plot,:)), plot_style_temp, 'Linewidth', 1.5);
    ylabel('DoF singular values');
    xlabel('BPM nr. [1]');
    %axis([106 136 0 15]);
    axis([scale_bpm_min scale_bpm_max 0 12]);
    %axis([90 140 0 20]);
    %legend('before, $$s_1$$','before, $$\sum_{i=2}^{10} s_i$$', ...
    %       'exchanged, $$s_1$$','exchanged, $$\sum_{i=2}^{10} s_i$$', ...
    %       'changed back, $$s_1$$','changed back, $$\sum_{i=2}^{10} s_i$$');
    
    % Third conclusion: The location of the correlation change can be 
    % clearly located and they are as proposed. The form of the correlation
    % pattern is less interesting, since they do not really look physical.
end

if(use_removed_motion_analysis_space == 1)
    %diff1_mod = diff1(:,91:end);
    diff1_mod = diff1(:,108:end-1);
    diff1_mod(:,17) = [];
    diff1_mod(:,8) = [];
    
    %diff2_mod = diff2(:,91:end);
    diff2_mod = diff2(:,108:end-1);
    diff2_mod(:,17) = [];
    diff2_mod(:,8) = [];
    
    %diff3_mod = diff3(:,91:end); 
    diff3_mod = diff3(:,108:end-1); 
    diff3_mod(:,17) = [];
    diff3_mod(:,8) = [];
    
    %diff_xy_mod = diff_xy(:,91:end); 
    diff_xy_mod = diff_xy(:,108:end-1); 
    diff_xy_mod(:,17) = [];
    diff_xy_mod(:,8) = [];
    
    [U_diff1, S_diff1, V_diff1] = svd(diff1_mod);
    [U_diff2, S_diff2, V_diff2] = svd(diff2_mod);
    [U_diff3, S_diff3, V_diff3] = svd(diff3_mod);
    [U_diff_xy, S_diff_xy, V_diff_xy] = svd(diff_xy_mod);
    
    %figure(11);
    %plot(diag(S_diff1), 'x-b');
    %grid on;
    
    %figure(12);
    %plot(index_bpm, V_diff1(:,1)./sigma_y,'x-b');
    %grid on;
    %hold on;
    %plot(index_bpm, V_diff1(:,2)./sigma_y,'x-r');
    
    %figure(13);
    %plot(index_bpm, diff1_mod(1:10,:)*diag(1./sigma_y),'x-');
    %%plot(index_bpm, diff2_mod(1:10,:),'x-');
    %grid on;
    
    %figure(14);
    %plot(diag(S_diff2), 'x-b');
    %grid on;
    
    %figure(15);
    %plot(index_bpm, V_diff2(:,1)./sigma_y,'x-b');
    %grid on;
    %hold on;
    %%plot(index_bpm, V_diff2(:,2)./sigma_y,'x-r');
    
    mean_diff2 = mean(abs(diff2_mod));
    
    %figure(16);
    %plot(index_bpm, diff2_mod(1:10,:)*diag(1./sigma_y),'x-');
    %%plot(index_bpm, diff2_mod(1:10,:),'x-');
    %grid on;
    
    %figure(17);
    %plot(diag(S_diff3), 'x-b');
    %grid on;
    
    %figure(18);
    %plot(index_bpm, V_diff3(:,1)./sigma_y,'x-b');
    %grid on;
    %hold on;
    %%plot(index_bpm, V_diff3(:,2)./sigma_y,'x-r');
    
    %figure(19);
    %plot(index_bpm, diff3_mod(1:10,:)*diag(1./sigma_y),'x-');
    %%plot(index_bpm, diff2_mod(1:10,:),'x-');
    %grid on;
    
    %figure(20);
    %plot(index_bpm, V_diff_xy(:,1)./sigma_y,'x-b');
    %grid on;
    
    mean_diff3 = mean(abs(diff3_mod));
    
    % Calculate scaling factors for the beam motion
    std_diff1 = (std(diff1_mod)').*(sign(diff1_mod(1,:))');
    factor_diff1 = norm(std_diff1)/norm(V_diff1(:,1));
    std_diff2 = (std(diff2_mod)').*(sign(diff2_mod(1,:))');
    factor_diff2 = norm(std_diff2)/norm(V_diff2(:,1));
    std_diff3 = (std(diff3_mod)').*(sign(diff3_mod(1,:))');
    factor_diff3 = norm(std_diff3)/norm(V_diff3(:,1));
    std_diff_xy = (std(diff_xy_mod)').*(sign(diff_xy_mod(1,:))');
    factor_diff_xy = norm(std_diff_xy)/norm(V_diff_xy(:,1));
    
    save_data = [V_diff1(:,1)*factor_diff1, V_diff2(:,1)*factor_diff2, V_diff3(:,1)*factor_diff3, sigma_y, V_diff_xy(:,1)*factor_diff_xy];
    save('source_motion_QD20X_bump1.dat', 'save_data', '-ascii');
    
    % Debug
    %figure(21);
    %plot(mean_diff2, 'x-b');
    %grid on;
    %hold on;
    %plot(V_diff2(:,1).*S_diff2(1)./26./3,'x-r');
    
    %figure(22);
    %plot(mean_diff3, 'x-b');
    %grid on;
    %hold on;
    %plot(V_diff3(:,1).*S_diff3(1)./26./3,'x-r');
    
end

if(use_removed_motion_analysis_time == 1)
    
    %figure(23);
    %plot(U_diff1(:,1));
    %grid on;
    %hold on;
    
    PSD_diff1 = abs(fft(U_diff1(:,1))).^2;
    f_scale_fact = 3.12./length(PSD_diff1);
    f_values = (1:length(PSD_diff1))*f_scale_fact;
    PSD_diff1 = PSD_diff1(1:(end/2));
    f_values = f_values(1:(end/2));
    irms_diff1 = cumsum(PSD_diff1(end:-1:1));
    irms_diff1 = irms_diff1(end:-1:1);
    
    PSD_diff2 = abs(fft(U_diff2(:,1))).^2;
    PSD_diff2 = PSD_diff2(1:(end/2));
    irms_diff2 = cumsum(PSD_diff2(end:-1:1));
    irms_diff2 = irms_diff2(end:-1:1);
    
    PSD_diff3 = abs(fft(U_diff3(:,1))).^2;
    PSD_diff3 = PSD_diff3(1:(end/2));
    irms_diff3 = cumsum(PSD_diff3(end:-1:1));
    irms_diff3 = irms_diff3(end:-1:1);
    
    %figure(24)
    %plot(f_values, PSD_diff1./irms_diff1(1), 'b');
    %grid on;
    %hold on;
    %plot(f_values, PSD_diff2./irms_diff1(1), 'r');
    %plot(f_values, PSD_diff3./irms_diff1(1), 'k');
    %xlabel('frequency [Hz]');
    %ylabel('normalised PSD [1]');
    %legend('source 1', 'source 2', 'source 3');
    %axis([0 3.12/2, 0 0.02]);
    
    %figure(25)
    %plot(f_values, irms_diff1./irms_diff1(1), 'b');
    %grid on;
    %hold on;
    %plot(f_values, irms_diff2./irms_diff1(1), 'r');
    %plot(f_values, irms_diff3./irms_diff1(1), 'k');
    %xlabel('frequency [Hz]');
    %ylabel('normalised IRMS [1]');
    %legend('source 1', 'source 2', 'source 3');
    %axis([0 3.12/2, 0 1]);
    
end



