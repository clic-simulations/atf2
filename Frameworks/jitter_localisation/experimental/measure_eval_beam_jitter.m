% Script to measure, process the data and evaluate the beam jitter
% in per cent at the locations of the cavity bpms.
%
% Juergen Pfingstner
% 20th of February 2012

%%%%%%%%%%%%%
% Parameter %
%%%%%%%%%%%%%

filename_prefix = 'bump2';
filename_suffix = 'MQD8X_neg3100_corr1_v3';
nr_trains = 1*60*3;
%nr_trains = 10;

external_parameter = 1;
t = clock();
file_name_base = sprintf('%s_%i%i%i_%ih%i_%s', filename_prefix,t(1),t(2),t(3),t(4),t(5), filename_suffix);


%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read corrector strength %
%%%%%%%%%%%%%%%%%%%%%%%%%%%

[corr_pv_x, corr_pv_y, corr_index_x, corr_index_y] = get_corr_pv_ATF2('read');
[corr_I_x, corr_I_y] = get_pv_xy(corr_pv_x, corr_pv_y);
%[beam_status] = get_pv('atf:beam')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get BPM readings and lattice information and store the data %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

acquire_BPM_data

%%%%%%%%%%%%%%%%%%%%
% Process the data %
%%%%%%%%%%%%%%%%%%%%

preprocess_data

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluate the jitter level at the downstream cavity BPMs %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

x_std=std(x_data);
y_std=std(y_data);
sigma_x = model_data(:,7)';
sigma_y = model_data(:,8)';
s_bpm = model_data(:,9)';
ref_noise = model_data(:,10)';
bpm_index = [1:size(y_data,2)];

% initial jitter
for i=1:136;
    x_rel(i) = x_std(i)/sigma_x(i);
    y_rel(i) = y_std(i)/sigma_y(i);
    noise_x_rel(i) = ref_noise(i)/sigma_x(i);
    noise_y_rel(i) = ref_noise(i)/sigma_y(i);
end

figure(1);
%title('Standard deviation of the differential signal levels in ATF2');
plot(bpm_index, x_rel*100, '-xb');
grid on;
hold on;
plot(bpm_index, y_rel*100, '-xr');
plot(bpm_index, noise_x_rel*100,'-xc');
plot(bpm_index, noise_y_rel*100,'-xm');
xlabel('BPM nr. [1]');
%ylabel('$$\sigma_s/\sigma_y$$ [\%]');
%ylabel('\sigma_s \ sigma_y [per cent]');
axis([115 132 0 50]);
legend('x jitter', 'y jitter', 'x noise', 'y noise');

% Take out some noisy BPMs and calculate the average jitter level
x_rel_ff=[x_rel(119:123) x_rel(125:132)];
y_rel_ff=[y_rel(119:123) y_rel(125:132)];
x_rel_mean=mean(x_rel_ff);
y_rel_mean=mean(y_rel_ff);

print_string = sprintf('Rel. beam jitter level in per cent: x=%g, y=%g\n', x_rel_mean*100, y_rel_mean*100);
fprintf(1,print_string)

file_name_average = ['./data/' file_name_base '_averaged.dat'];
fid_average = fopen(file_name_average,'w');
fprintf(fid_average, print_string);
fclose(fid_average);

file_name_corr = ['./data/' file_name_base '_I_corr_x.dat'];
save(file_name_corr, 'corr_I_x','-ascii');
file_name_corr = ['./data/' file_name_base '_I_corr_y.dat'];
save(file_name_corr, 'corr_I_y','-ascii');

