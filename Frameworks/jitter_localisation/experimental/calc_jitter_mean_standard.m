% Script to calculate and plot the mean and std of 
% stored data                                      
% 
% Juergen Pfingstner
% 6th of December 2012 

%%%%%%%%%%%%%%
% Parameters %
%%%%%%%%%%%%%%

%file_name_base = 'epicsnew_ring_first_fb_121211_V1.dat';
file_name_base = 'fs_fb_121211_V1.dat';
%file_name_base = 'test.dat';
bpm_noise_subtraction = 'none'; % 'none', 'simple', 'svd'
nr_sv_filt = 20;
store_svd_matrix = 1;
use_disp_subtraction = 0;
figure_start_nr = 10;
add_bba_offset = 1;
analysis_area = 'all'; %'atf', 'atf2', 'all'

%%%%%%%%%%%%%%%%%%%%%%%%
% Init data structures %
%%%%%%%%%%%%%%%%%%%%%%%%

global BEAMLINE
global INSTR

file_name = sprintf('./data/%s', file_name_base);
if(store_svd_matrix)
  file_name_base_x= sprintf('x_%s', file_name_base);
  file_name_base_y= sprintf('y_%s', file_name_base);
else
  file_name_base_x = 'no';
  file_name_base_y = 'no';
end

fprintf('Find the BPM indices\n');
[nr_bpms_ring, bpms_index, raw_index] = get_BPM_index_FL();

nr_total = length(bpms_index);
s_bpm = zeros(1,nr_total);
ref_noise = zeros(1,nr_total);
for i=1:nr_total
    s_bpm(i) = BEAMLINE{bpms_index(i)}.S;
    ref_noise(i) = BEAMLINE{bpms_index(i)}.Resolution;
end

% Debug
%figure(10);
%plot(ref_noise);
%grid on;
%keyboard;
% dEBUG

fprintf('Get the beta and dispersion functions at the BPM positions\n');
beta_x = FL.SimModel.Twiss.betax(bpms_index);
beta_y = FL.SimModel.Twiss.betay(bpms_index);
eta_x = FL.SimModel.Twiss.etax(bpms_index);
eta_y = FL.SimModel.Twiss.etay(bpms_index);
gamma = 1300/0.511;
epsilon_y = 0.3e-7./gamma;
epsilon_x = 50e-7./gamma;

%%%%%%%%%%%%%%%%%
% Load the data %
%%%%%%%%%%%%%%%%%

raw_data = load(file_name, '-ASCII');
[nr_trains, nr_bpms] = size(raw_data);
nr_trains = nr_trains./2;
x_data = raw_data(1:nr_trains,:);
y_data = raw_data(nr_trains+1:end,:);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Extract the data of interest %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(strcmp(analysis_area, 'atf'))
  x_data = x_data(:,1:nr_bpms_ring);
  y_data = y_data(:,1:nr_bpms_ring);
elseif(strcmp(analysis_area, 'atf2'))
  x_data = x_data(:,nr_bpms_ring+1:end);
  y_data = y_data(:,nr_bpms_ring:end);
else
  % Take all data
end

%%%%%%%%%%%%%%%%%%%%
% Process the data %
%%%%%%%%%%%%%%%%%%%%

x_data = clean_bpm_data(x_data);
y_data = clean_bpm_data(y_data);
[nr_trains_x, nr_bpms_x] = size(x_data);
[nr_trains_y, nr_bpms_y] = size(y_data);

if(strcmp(bpm_noise_subtraction,'svd'))
  %x_data = svd_filter((x_data(1:end-1,:)-x_data(2:end,:)), nr_sv_filt, file_name_base_x);
  %y_data = svd_filter((y_data(1:end-1,:)-y_data(2:end,:)), nr_sv_filt, file_name_base_y);
  x_data = svd_filter(x_data, nr_sv_filt, file_name_base_x);
  y_data = svd_filter(y_data, nr_sv_filt, file_name_base_y);
end
    
if(use_disp_subtraction == 1)
  for i=1:nr_trains_x
     x_data(i,:) = x_data(i,:) - (x_data(i,:)*eta_x(1:nr_bpms_x)')*eta_x(1:nr_bpms_x);
  end
  for i=1:nr_trains_y
     y_data(i,:) = y_data(i,:) - (y_data(i,:)*eta_y(1:nr_bpms_y)')*eta_y(1:nr_bpms_y);
  end
end

% Calculate the normalised variance of the jitter 

mean_x = mean(x_data);
mean_y = mean(y_data);
std_x = std(x_data);
std_y = std(y_data);
if(strcmp(bpm_noise_subtraction,'simple'))
  std_x = std_x - ref_noise(1:nr_bpms_x); 
  std_y = std_y - ref_noise(1:nr_bpms_y);
end
std_x_norm = std_x./sqrt(epsilon_x.*beta_x(1:nr_bpms_x));
std_y_norm = std_y./sqrt(epsilon_y.*beta_y(1:nr_bpms_y));

if((add_bba_offset == 1) & (strcmp(analysis_area,'ring') | strcmp(analysis_area,'all')))
  bba_h = load('./data/BBAsummary_2011NOV29_horizontal.txt','-ascii');
  bba_v = load('./data/BBAsummary_2011NOV29_vertical.txt','-ascii');
  [bba_h, bba_v] = rearrange_EPICS_BPM_data(bba_h(:,2)', bba_v(:,2)');
  mean_x(1:length(bba_h)) = mean_x(1:length(bba_h)) - bba_h;
  mean_y(1:length(bba_v)) = mean_y(1:length(bba_v)) - bba_v;
end

%%%%%%%%%%%%%%%%%
% Plot the data %
%%%%%%%%%%%%%%%%%

figure(figure_start_nr);
plot(std_x_norm, '-b');
grid on;
hold on;
plot(std_y_norm, '-r');
s_ring_linac = nr_bpms_ring + 0.5;
yLimits = get(gca,'YLim');
line([s_ring_linac, s_ring_linac], yLimits.*0.8, 'Color', 'k', 'LineStyle', '--');
title('Normalised std of beam offset (last)');
legend('x', 'y');
hold off;

figure(figure_start_nr+1);
plot(std_x, '-b');
grid on;
hold on;
plot(std_y, '-r');
s_ring_linac = nr_bpms_ring + 0.5;
yLimits = get(gca,'YLim');
line([s_ring_linac, s_ring_linac], yLimits.*0.8, 'Color', 'k', 'LineStyle', '--');
title('Std of beam offset');
legend('x', 'y');
hold off;

figure(figure_start_nr+2);
plot(mean_x, '-b');
grid on;
hold on;
plot(mean_y, '-r');
s_ring_linac = nr_bpms_ring + 0.5;
yLimits = get(gca,'YLim');
line([s_ring_linac, s_ring_linac], yLimits.*0.8, 'Color', 'k', 'LineStyle', '--');    
title('Mean of beam offset');
legend('x', 'y');
hold off;
