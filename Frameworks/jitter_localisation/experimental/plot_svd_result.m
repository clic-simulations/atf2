% Script to analyse the directions of a certain 
% SVD decomposition. 
%
% Juergen Pfingstner
% 11. Dec. 2012

function [] = plot_svd_result(direction_nr)

% Parameter 

file_name_base = 'epics_ring_first_no_fb_121207_V1.dat';
%file_name_base = 'fs_fb_121207_V1.dat';
plot_nr = 10;
%direction_nr = 1;
x_or_y = 'y';

%%%%%%%%%%%%%%

file_name_U = sprintf('./data/U_%s_%s', x_or_y, file_name_base);
file_name_s = sprintf('./data/s_%s_%s', x_or_y, file_name_base);

U = load(file_name_U, '-ascii');
s = load(file_name_s, '-ascii');
nr_bpms_ring = 93;

figure(plot_nr);
plot(U(:,direction_nr), '-b');
s_ring_linac = nr_bpms_ring + 0.5;
yLimits = get(gca,'YLim');
line([s_ring_linac, s_ring_linac], yLimits.*0.8, 'Color', 'k', 'LineStyle', '--');
grid on;
hold on;

figure(plot_nr+100);
plot(s);
grid on;
hold on;
