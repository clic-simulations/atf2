% Function to calculate the tf of different
% feedback versions. 
%
% Juergen Pfingstner
% 11.12.2012

function [data_out] = remove_spikes2(data, treshold)

   N = length(data);
   data_out = zeros(N,1);
   j = 1;
   for i=1:N
       if(abs(data(i))>treshold)
           % Nothing to do
       else
         data_out(j) = data(i);
         j = j+1;
       end
   end 
   data_out = data_out(1:(j-1));
   
end 
