% Script to calculate and plot frequency properties
% if the stored data                                       
% 
% Juergen Pfingstner
% 6th of December 2012 

%%%%%%%%%%%%%%
% Parameters %
%%%%%%%%%%%%%%

%file_name_base1 = 'fs_fb_121215_V1';
%file_name_base2 = 'fs_fb_121214_V1';
%file_name_base1 = 'epicsnew_ring_orbit_no_fb_121214_V1';
%file_name_base2 = 'epicsnew_ring_orbit_fb_121214_V1';
file_name_base1 = 'epics_all_orbit_fb_121218_V1';
file_name_base2 = 'epics_all_orbit_fb_121217_V2';
%file_name_base2 = 'epicsnew_ring_orbit_fb_121214_V1.dat';
%file_name_base2 = 'test.dat';
%file_name_base1 = 'test2.dat';
bpm_noise_subtraction = 'none'; % 'none', 'simple', 'svd'
nr_sv_filt = 10;
store_svd_matrix = 1;
use_disp_subtraction = 0;
use_substract_mean = 1; 

figure_start_nr = 30;
bpm_index_plot = 100;
analysis_area = 'all'; % 'atf', 'atf2', 'atf2_clean', 'all'

N_bartlett_blocks = 1;
use_spike_removal = 1;
spike_removal_treshold = 1e-4;
use_jitter_filter = 0;
use_time_stamps_arrange = 0;
use_shift_data = 0;
nr_shift_steps = 0;
use_diff_corr = 0;
use_shift_stripline_data = 1;

%%%%%%%%%%%%%%%%%
% Load the data %
%%%%%%%%%%%%%%%%%

file_name1= sprintf('./data/%s.dat', file_name_base1);
file_name2= sprintf('./data/%s.dat', file_name_base2);
if(store_svd_matrix)
  file_name_base_x_1= sprintf('x_%s', file_name_base1);
  file_name_base_x_2= sprintf('x_%s', file_name_base2);
  file_name_base_y_1= sprintf('y_%s', file_name_base1);
  file_name_base_y_2= sprintf('y_%s', file_name_base2);
else
  file_name_base_x_1 = 'no';
  file_name_base_x_2 = 'no';
  file_name_base_y_1 = 'no';
  file_name_base_y_2 = 'no';
end

raw_data1 = load(file_name1, '-ASCII');
[nr_trains1, nr_bpms1] = size(raw_data1);
nr_trains1 = nr_trains1./2;
x_data1 = raw_data1(1:nr_trains1,:);
y_data1 = raw_data1(nr_trains1+1:end,:);

raw_data2 = load(file_name2, '-ASCII');
[nr_trains2, nr_bpms2] = size(raw_data2);
nr_trains2 = nr_trains2./2;
x_data2 = raw_data2(1:nr_trains2,:);
y_data2 = raw_data2(nr_trains2+1:end,:);

%if(use_time_stamps_arrange)
%  file_name1_time_stamp = ['./data/time_stamp_' file_name_base1 '.dat'];
%  file_name2_time_stamp = ['./data/time_stamp_' file_name_base2 '.dat'];
%
%  raw_time1 = load(file_name1_time_stamp, '-ASCII');
%  x_time_stamp1 = raw_time1(1:nr_trains1,:);
%  y_time_stamp1 = raw_time1(nr_trains1+1:end,:);
%
%  raw_time2 = load(file_name2_time_stamp, '-ASCII');
%  x_time_stamp2 = raw_time2(1:nr_trains2,:);
%  y_time_stamp2 = raw_time2(nr_trains2+1:end,:);
%end

% Debug
%x_data1 = 1.e-6.*randn(size(x_data1));
%x_data1 = x_data1(1:500,:);
%x_data2 = x_data2(1:500,:);
%y_data1 = y_data1(1:500,:);
%y_data2 = y_data2(1:500,:);
% dEBUG

%%%%%%%%%%%%%%%%%%%%%%%
% Get some model data %
%%%%%%%%%%%%%%%%%%%%%%%

fprintf('Find the BPM indices\n');
[nr_bpms_ring, bpms_index, raw_index] = get_BPM_index_FL();

nr_total = length(bpms_index);
s_bpm = zeros(1,nr_total);
ref_noise = zeros(1,nr_total);
for i=1:nr_total
    s_bpm(i) = BEAMLINE{bpms_index(i)}.S;
    ref_noise(i) = BEAMLINE{bpms_index(i)}.Resolution;
end

% Debug
%figure(1);
%plot(ref_noise);
%grid on;
%keyboard;
% dEBUG

fprintf('Get the beta and dispersion functions at the BPM positions\n');
beta_x = FL.SimModel.Twiss.betax(bpms_index);
beta_y = FL.SimModel.Twiss.betay(bpms_index);
eta_x = FL.SimModel.Twiss.etax(bpms_index);
eta_y = FL.SimModel.Twiss.etay(bpms_index);
nux = FL.SimModel.Twiss.nux(bpms_index);
nuy = FL.SimModel.Twiss.nuy(bpms_index);
gamma = 1300/0.511;
epsilon_y = 0.3e-7./gamma;
epsilon_x = 50e-7./gamma;
sigma_x = sqrt(epsilon_x.*beta_x);
sigma_y = sqrt(epsilon_y.*beta_y);

%% Debug
%figure(2);
%plot(beta_x, 'b');
%grid on;
%hold on;
%plot(beta_y, 'c');
%plot(eta_x, 'r');
%plot(eta_y, 'm');
%
%figure(3);
%plot(sigma_x, 'b');
%grid on;
%hold on;
%plot(sigma_y, 'r');
%
figure(4);
plot(nux, 'x-b');
grid on;
hold on;
plot(nuy, 'x-r');
%% dEBUG

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Extract the data of interest %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(strcmp(analysis_area, 'atf'))
  bpm_indices_for_dft = [1:nr_bpms_ring];
  eta_indices = [1:nr_bpms_ring];
elseif(strcmp(analysis_area, 'atf2'))
  bpm_indices_for_dft = [1:nr_bpms1];
  eta_indices = [nr_bpms_ring+1:nr_bpms1];
elseif(strcmp(analysis_area, 'atf2_clean'))
  bpm_indices_for_dft = [108:137];
  eta_indices = [1:nr_bpms1];
elseif(strcmp(analysis_area, 'all'))
bpm_indices_for_dft = [1:nr_bpms1];
  eta_indices = [1:nr_bpms1];
else
  % Take all data
end

%%%%%%%%%%%%%%%%%%%%
% Process the data %
%%%%%%%%%%%%%%%%%%%%

% Debug
%x_data1(:,1:nr_bpms_ring) = zeros(size(x_data1(:,1:nr_bpms_ring)));
%y_data1(:,1:nr_bpms_ring) = zeros(size(y_data1(:,1:nr_bpms_ring)));
%x_data2(:,1:nr_bpms_ring) = zeros(size(x_data2(:,1:nr_bpms_ring)));
%y_data2(:,1:nr_bpms_ring) = zeros(size(y_data2(:,1:nr_bpms_ring)));
% dEBUG

x_data1 = clean_bpm_data(x_data1);
y_data1 = clean_bpm_data(y_data1);
x_data2 = clean_bpm_data(x_data2);
y_data2 = clean_bpm_data(y_data2);

[nr_trains_x1, nr_bpms_x1] = size(x_data1);
[nr_trains_y1, nr_bpms_y1] = size(y_data1);
[nr_trains_x2, nr_bpms_x2] = size(x_data2);
[nr_trains_y2, nr_bpms_y2] = size(y_data2);

% Cut all data to the same size
nr_trains = min([nr_trains_x1, nr_trains_x2, nr_trains_y1, nr_trains_y2]);
x_data1 = x_data1(1:nr_trains,:);
x_data2 = x_data2(1:nr_trains,:);
y_data1 = y_data1(1:nr_trains,:);
y_data2 = y_data2(1:nr_trains,:);

% Shift the stripline BPMs compared to the cavity BPMs
if(use_shift_stripline_data == 1)
    x_data1(:,93:101) = [x_data1(2:end,93:101); zeros(1,length(93:101))]; 
    x_data1(:,105:107) = [x_data1(2:end,105:107); zeros(1,length(105:107))]; 
    x_data2(:,93:101) = [x_data2(2:end,93:101); zeros(1,length(93:101))]; 
    x_data2(:,105:107) = [x_data2(2:end,105:107); zeros(1,length(105:107))]; 
    y_data1(:,93:101) = [y_data1(2:end,93:101); zeros(1,length(93:101))]; 
    y_data1(:,105:107) = [y_data1(2:end,105:107); zeros(1,length(105:107))]; 
    y_data2(:,93:101) = [y_data2(2:end,93:101); zeros(1,length(93:101))]; 
    y_data2(:,105:107) = [y_data2(2:end,105:107); zeros(1,length(105:107))]; 
end

f_rep = 3.12;
T_0 = nr_trains./f_rep;

if(use_substract_mean == 1)
  mean_x1 = mean(x_data1);
  mean_y1 = mean(y_data1);
  mean_x2 = mean(x_data2);
  mean_y2 = mean(y_data2);
  for i=1:nr_trains
      x_data1(i,:) = x_data1(i,:) - mean_x1;
      y_data1(i,:) = y_data1(i,:) - mean_y1;
      x_data2(i,:) = x_data2(i,:) - mean_x2;
      y_data2(i,:) = y_data2(i,:) - mean_y2;
  end
end

if(use_spike_removal == 1)
  x_data1 = remove_spikes(x_data1, spike_removal_treshold);
  x_data2 = remove_spikes(x_data2, spike_removal_treshold);
  y_data1 = remove_spikes(y_data1, spike_removal_treshold);
  y_data2 = remove_spikes(y_data2, spike_removal_treshold);
end

if(strcmp(bpm_noise_subtraction,'svd'))
  %x_data1 = svd_filter(x_data1(1:end-1,:)-x_data1(2:end,:), nr_sv_filt, file_name_base_x_1);
  %y_data1 = svd_filter(y_data1(1:end-1,:)-y_data1(2:end,:), nr_sv_filt, file_name_base_y_1);
  %x_data2 = svd_filter(x_data2(1:end-1,:)-x_data2(2:end,:), nr_sv_filt, file_name_base_x_2);
  %y_data2 = svd_filter(y_data2(1:end-1,:)-y_data2(2:end,:), nr_sv_filt, file_name_base_y_2);
  x_data1 = svd_filter(x_data1, nr_sv_filt, file_name_base_x_1);
  y_data1 = svd_filter(y_data1, nr_sv_filt, file_name_base_y_1);
  x_data2 = svd_filter(x_data2, nr_sv_filt, file_name_base_x_2);
  y_data2 = svd_filter(y_data2, nr_sv_filt, file_name_base_y_2);
end

disp_mode = zeros(nr_trains_x1,1);

if(use_disp_subtraction == 1)
  for i=1:nr_trains
     disp_mode(i) = (x_data1(i,eta_indices)*eta_x(eta_indices)');
     x_data1(i,eta_indices) = x_data1(i,eta_indices) - (x_data1(i,eta_indices)*eta_x(eta_indices)')*eta_x(eta_indices);
     y_data1(i,eta_indices) = y_data1(i,eta_indices) - (y_data1(i,eta_indices)*eta_y(eta_indices)')*eta_y(eta_indices);
     x_data2(i,eta_indices) = x_data2(i,eta_indices) - (x_data2(i,eta_indices)*eta_x(eta_indices)')*eta_x(eta_indices);
     y_data2(i,eta_indices) = y_data2(i,eta_indices) - (y_data2(i,eta_indices)*eta_y(eta_indices)')*eta_y(eta_indices);
  end
end

[psd_x1, f] = psd_bartlett(x_data1(:,bpm_indices_for_dft), T_0, N_bartlett_blocks);
[psd_x2, f] = psd_bartlett(x_data2(:,bpm_indices_for_dft), T_0, N_bartlett_blocks);
[psd_y1, f] = psd_bartlett(y_data1(:,bpm_indices_for_dft), T_0, N_bartlett_blocks);
[psd_y2, f] = psd_bartlett(y_data2(:,bpm_indices_for_dft), T_0, N_bartlett_blocks);
[psd_energy, ~] = psd_bartlett(disp_mode, T_0, N_bartlett_blocks);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create the BPM noise spectrum %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(strcmp(analysis_area,'atf2_clean'))
  psd_noise = ((0.1e-6)^2*T_0/N_bartlett_blocks/length(f)).*ones(size(psd_x1));
elseif(strcmp(analysis_area,'atf'))
  %psd_noise = ((20e-6)^2*T_0/N_bartlett_blocks/length(f)).*ones(size(psd_x1));
  psd_noise = ((20e-6)^2*T_0/N_bartlett_blocks/length(f)).*ones(size(psd_x1));
elseif(strcmp(analysis_area,'atf2'))
  %psd_noise = ((5e-6)^2*T_0/N_bartlett_blocks/length(f)).*ones(size(psd_x1));
  psd_noise = ((0.1e-6)^2*T_0/N_bartlett_blocks/length(f)).*ones(size(psd_x1));
elseif(strcmp(analysis_area,'all'))
  %psd_noise = ((5e-6)^2*T_0/N_bartlett_blocks/length(f)).*ones(size(psd_x1));
  psd_noise = ((0.1e-6)^2*T_0/N_bartlett_blocks/length(f)).*ones(size(psd_x1));
end

%%%%%%%%%%%%%%%%%%%%%%%
% Estimate the jitter %
%%%%%%%%%%%%%%%%%%%%%%%

tf_jitter = calc_tf_fb(2, 1, 1./f_rep, f);
psd_filter_jitter = (abs(tf_jitter).^2)./2;

%figure(3);
%plot(abs(tf_jitter));
%grid on;

if(use_jitter_filter == 1)
  psd_x1 = diag(psd_filter_jitter)*psd_x1;
  psd_x2 = diag(psd_filter_jitter)*psd_x2;
  psd_y1 = diag(psd_filter_jitter)*psd_y1;
  psd_y2 = diag(psd_filter_jitter)*psd_y2;
end

%%%%%%%%%%%%%%%%%%%%%%%
% Calc integrated RMS %
%%%%%%%%%%%%%%%%%%%%%%%

irms_x1 = sqrt(cumsum(psd_x1(end:-1:1,:))./(T_0./N_bartlett_blocks));
irms_x1 = irms_x1(end:-1:1,:);
irms_x2 = sqrt(cumsum(psd_x2(end:-1:1,:))./(T_0./N_bartlett_blocks));
irms_x2 = irms_x2(end:-1:1,:);
irms_y1 = sqrt(cumsum(psd_y1(end:-1:1,:))./(T_0./N_bartlett_blocks));
irms_y1 = irms_y1(end:-1:1,:);
irms_y2 = sqrt(cumsum(psd_y2(end:-1:1,:))./(T_0./N_bartlett_blocks));
irms_y2 = irms_y2(end:-1:1,:);
irms_noise = sqrt(cumsum(psd_noise(end:-1:1,:))./(T_0./N_bartlett_blocks));
irms_noise = irms_noise(end:-1:1,:);

tf_x = sqrt(psd_x2./psd_x1);
tf_y = sqrt(psd_y2./psd_y1);
tf_xy = sqrt(psd_y1./psd_x1);
tf_x_mean = mean(tf_x')';
tf_y_mean = mean(tf_y')';
tf_xy_mean = mean(tf_xy')';
tf_x_std = std(tf_x')';
tf_y_std = std(tf_y')';
tf_xy_std = std(tf_xy')';

% Pulse to pulse integrator
%p_i = 1;
%T_d = 1./f_rep;
%option_fb = 1;

% Kubo's damping ring integrator
p_i = 0.5;
T_d = 15;
option_fb = 1;

%% Okugis's extraction line FB
%p_i = 0.5;
%T_d = 2;
%option_fb = 1;

tf_fb_model = calc_tf_fb(option_fb, p_i, T_d, f(2:end));

%%%%%%%%%%%%%%%%%
% Plot the data %
%%%%%%%%%%%%%%%%%

%handle1 = figure(figure_start_nr);
%%semilogy(f, psd_x1(:,bpm_index_plot), 'b');
%%grid on;
%%hold on;
%%semilogy(f, psd_x2(:,bpm_index_plot), 'c');
%%semilogy(f, psd_y1(:,bpm_index_plot), 'r');
%%semilogy(f, psd_y2(:,bpm_index_plot), 'm');
%%%semilogy(f, psd_energy, 'c');
%%semilogy(f, psd_noise(:,bpm_index_plot), 'k');
%loglog(f, psd_x1(:,bpm_index_plot), 'b');
%grid on;
%hold on;
%loglog(f, psd_x2(:,bpm_index_plot), 'c');
%loglog(f, psd_y1(:,bpm_index_plot), 'r');
%loglog(f, psd_y2(:,bpm_index_plot), 'm');
%%semilogy(f, psd_energy, 'k');
%loglog(f, psd_noise(:,bpm_index_plot), 'k');
%title('PSD of beam jitter');
%legend('x1 no FB', 'x2 FB', 'y1 no FB', 'y2 FB');
%xlabel('Frequency [Hz]');
%ylabel('PSD [m^2/Hz]');
%hold off;

%handle1a = figure(figure_start_nr+1);
%loglog(f, irms_x1(:,bpm_index_plot), 'b');
%grid on;
%hold on;
%loglog(f, irms_x2(:,bpm_index_plot), 'c');
%loglog(f, irms_y1(:,bpm_index_plot), 'r');
%loglog(f, irms_y2(:,bpm_index_plot), 'm');
%%loglog(f, irms_energy, 'c');
%%loglog(f, irms_noise(:,bpm_index_plot), 'k');
%title('IRMS of beam jitter');
%legend('x1 no FB', 'x2 FB', 'y1 no FB', 'y2 FB');
%xlabel('Frequency [Hz]');
%ylabel('IRMS [m]');
%hold off;

t = (1:nr_trains)./f_rep;
%handle1b = figure(figure_start_nr+2);
%plot(x_data1(:,bpm_index_plot), 'b');
%grid on;
%hold on;
%loglog(x_data2(:,bpm_index_plot), 'c');
%loglog(y_data1(:,bpm_index_plot), 'r');
%loglog(y_data2(:,bpm_index_plot), 'm');
%title('Time signal of beam jitter');
%legend('x1 no FB', 'x2 FB', 'y1 no FB', 'y2 FB');
%xlabel('Time [s]');
%ylabel('offset [m]');
%hold off;

%handle2 = figure(figure_start_nr+3);
%loglog(f, tf_x(:,bpm_index_plot), 'b');
%grid on;
%hold on;
%loglog(f, tf_y(:,bpm_index_plot), 'r');
%%loglog(f, tf_xy(:,bpm_index_plot), 'c');
%loglog(f(2:end), abs(tf_fb_model), 'k');
%title('TF of beam jitter');
%legend('x', 'y', 'model');
%xlabel('Frequency [Hz]');
%ylabel('Magnitude [1]');
%hold off;

%handle3 = figure(figure_start_nr+4);
%loglog(f, tf_x_mean, '-b');
%grid on;
%hold on;
%loglog(f, tf_y_mean, '-r');
%%loglog(f, tf_xy_mean, '-g');
%loglog(f(2:end), abs(tf_fb_model), '-k');
%title('Mean TF of beam jitter');
%legend('x', 'y', 'model');
%xlabel('Frequency [Hz]');
%ylabel('Magnitude [1]');
%hold off;

%handle4 = figure(figure_start_nr+5);
%semilogy(f, tf_x_std, '-b');
%grid on;
%hold on;
%semilogy(f, tf_y_std, '-r');
%semilogy(f, tf_xy_std, '-g');
%title('Std TF of beam jitter');
%legend('x', 'y');
%xlabel('Frequency [Hz]');
%ylabel('Magnitude [1]');
%hold off;

%%%%%%%%%%%%%%%%%%%%%%%
% Correlation studies %
%%%%%%%%%%%%%%%%%%%%%%%

if(use_diff_corr == 1) 
  x_data1 = x_data1(1:end-1,:) - x_data1(2:end,:);
  x_data1 = [x_data1; zeros(1,nr_bpms_x1)]; 
  x_data2 = x_data2(1:end-1,:) - x_data2(2:end,:);
  x_data2 = [x_data2; zeros(1,nr_bpms_x2)]; 
  y_data1 = y_data1(1:end-1,:) - y_data1(2:end,:);
  y_data1 = [y_data1; zeros(1,nr_bpms_y1)]; 
  y_data2 = y_data2(1:end-1,:) - y_data2(2:end,:);
  y_data2 = [y_data2; zeros(1,nr_bpms_y2)]; 
end

% Debug
%x_data2 = x_data1;
%y_data2 = y_data1;
%%x_data1 = load('./data/correlation_correction_bpm_119.dat');
%%y_data1 = load('./data/correlation_correction_bpm_119.dat');
%x_data1 = load('./data/correction_bpm_102_after_100_y.dat');
%y_data1 = load('./data/correction_bpm_102_after_100_y.dat');

%[nr_row_corr, nr_col_corr] = size(x_data1);
%x_data1 = [zeros(nr_row_corr, 2), x_data1];
%y_data1 = [zeros(nr_row_corr, 2), y_data1];
%x_data2 = [zeros(nr_row_corr+1, 2), x_data2];
%y_data2 = [zeros(nr_row_corr+1, 2), y_data2];
% dEBUG

%handle5 = figure(figure_start_nr+6);
%plot(irms_x1(1,:)./sigma_x(eta_indices)*100, 'b');
%grid on;
%hold on;
%plot(irms_y1(1,:)./sigma_y(eta_indices)*100, 'r');
%%plot(irms_x1(1,:), 'b');
%%grid on;
%%hold on;
%%plot(irms_y1(1,:), 'r');
%%semilogy(f, tf_y_std, '-r');
%%semilogy(f, tf_xy_std, '-g');
%title('Beam jitter');
%%legend('x1');
%xlabel('BPM number [1]');
%ylabel('Jitter [%]');
%%axis([0 length(eta_indices) 0 60])
%hold off;

%handle6 = figure(figure_start_nr+7);
%plot(y_data1(:,109), y_data1(:,120), 'xb');
%grid on;
%hold on;
%title('Beam correlation');
%xlabel('BPM 18 [m]');
%ylabel('BPM 25 [m]');
%hold off;

%handle7 = figure(figure_start_nr+8);
%plot(y_data1(:,120), y_data1(:,121), 'xb');
%grid on;
%hold on;
%title('Beam correlation x');
%xlabel('BPM 18 [m]');
%ylabel('BPM 25 [m]');
%hold off;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the correlation coefficients %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

corr_coef_x1 = zeros(nr_bpms_x1, 1);
corr_coef_y1 = zeros(nr_bpms_y1, 1);
corr_coef_x2 = zeros(nr_bpms_x2, 1);
corr_coef_y2 = zeros(nr_bpms_y2, 1);

% Data shift
if(nr_shift_steps==0)
  x_data1_shift = x_data1;
  x_data2_shift = x_data2;
  y_data1_shift = y_data1;
  y_data2_shift = y_data2;
elseif(nr_shift_steps < 0)
  x_data1_shift = [x_data1(-nr_shift_steps+1:end,:); zeros(-nr_shift_steps,nr_bpms_x1)];
  x_data2_shift = [x_data2(-nr_shift_steps+1:end,:); zeros(-nr_shift_steps,nr_bpms_x2)];
  y_data1_shift = [y_data1(-nr_shift_steps+1:end,:); zeros(-nr_shift_steps,nr_bpms_y1)];
  y_data2_shift = [y_data2(-nr_shift_steps+1:end,:); zeros(-nr_shift_steps,nr_bpms_y2)];
else
  x_data1_shift = [zeros(nr_shift_steps,nr_bpms_x1); x_data1(1:end-nr_shift_steps,:)];
  x_data2_shift = [zeros(nr_shift_steps,nr_bpms_x2); x_data2(1:end-nr_shift_steps,:)];
  y_data1_shift = [zeros(nr_shift_steps,nr_bpms_y1); y_data1(1:end-nr_shift_steps,:)];
  y_data2_shift = [zeros(nr_shift_steps,nr_bpms_y2); y_data2(1:end-nr_shift_steps,:)];
end

if(use_time_stamps_arrange==1)
  pulse_index_x1 = round(x_time_stamp1.*f_rep);
  pulse_index_y1 = round(y_time_stamp1.*f_rep);
  pulse_index_x2= round(x_time_stamp2.*f_rep);
  pulse_index_y2 = round(y_time_stamp2.*f_rep);

  for i = 1:nr_bpms_x1
    corr_coef_x1(i) = corrcoef_ordered(x_data1(:,bpm_index_plot), x_data1_shift(:,i), pulse_index_x1(:,bpm_index_plot), pulse_index_x1(:,i));
    corr_coef_y1(i) = corrcoef_ordered(y_data1(:,bpm_index_plot), y_data1_shift(:,i), pulse_index_y1(:,bpm_index_plot), pulse_index_y1(:,i));
    corr_coef_x2(i) = corrcoef_ordered(x_data2(:,bpm_index_plot), x_data2_shift(:,i), pulse_index_x2(:,bpm_index_plot), pulse_index_x2(:,i));
    corr_coef_y2(i) = corrcoef_ordered(y_data2(:,bpm_index_plot), y_data2(:,i), pulse_index_y2(:,bpm_index_plot), pulse_index_y2(:,i));
  end
else
  for i = 1:nr_bpms_x1
    corr_temp = corrcoef(x_data1(:,bpm_index_plot), x_data1_shift(:,i));
    corr_coef_x1(i) = corr_temp(1,2);
    corr_temp = corrcoef(y_data1(:,bpm_index_plot), y_data1_shift(:,i));
    corr_coef_y1(i) = corr_temp(1,2);
    corr_temp = corrcoef(x_data2(:,bpm_index_plot), x_data2_shift(:,i));
    corr_coef_x2(i) = corr_temp(1,2);
    corr_temp = corrcoef(y_data2(:,bpm_index_plot), y_data2_shift(:,i));
    corr_coef_y2(i) = corr_temp(1,2);
  end
end

handle8 = figure(figure_start_nr+9);
plot(corr_coef_x1, 'b');
grid on;
hold on;
%plot(corr_coef_x2, 'c');
plot(corr_coef_y1, 'r');
plot(corr_coef_y2, 'm');
title('Correlation coefficient');
axis([93 138 -1 1]);
xlabel('BPM nr. [1]');
ylabel('r [1]');
hold off;

% Save some data

corr_coeff_x1_mod = [corr_coef_x1(1:48); corr_coef_x1(51:end)];
corr_coeff_y1_mod = [corr_coef_y1(1:48); corr_coef_y1(51:end)];
corr_coeff_x2_mod = [corr_coef_x2(1:48); corr_coef_x2(51:end)];
corr_coeff_y2_mod = [corr_coef_y2(1:48); corr_coef_y2(51:end)];
save('correlation_98_full_x1.dat', 'corr_coeff_x1_mod','-ascii');
save('correlation_98_full_y1.dat', 'corr_coeff_y1_mod','-ascii');
save('correlation_98_full_x2.dat', 'corr_coeff_x2_mod','-ascii');
save('correlation_98_full_y2.dat', 'corr_coeff_y2_mod','-ascii');

%return;

% Perform SVD analysis
fprintf(1,'Calculate the SVD of the y data\n');
%[U,S,V] = svd(y_data1_mod(:,91:end));
%index_temp1 = [106:124, 126:133];
index_temp = [108:126, 128:135];
%[U1,S1,V1] = svd([y_data1(:,108:126), y_data1(:,128:135)]);
%[U2,S2,V2] = svd([y_data2(:,108:126), y_data2(:,128:135)]);
[U1,S1,V1] = svd(y_data1(:, index_temp));
[U2,S2,V2] = svd(y_data2(:,index_temp));

svd_plot_index = 1;

figure(100);
plot(diag(S1), 'x-b');
grid on;
hold on;
plot(diag(S2),'x-r');

figure(101);
plot(index_temp-2, V1(:,svd_plot_index)./sigma_y(index_temp)', 'x-b');
grid on;
hold on;
plot(index_temp-2, V2(:,svd_plot_index)./sigma_y(index_temp)', 'x-r');



