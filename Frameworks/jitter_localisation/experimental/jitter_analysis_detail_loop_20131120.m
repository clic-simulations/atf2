% Script to call the jitter_analysis_detail several times, maybe in
% a loop. 
%
% Juergen Pfingstner
% 18th of June 2013

% Comments: The script jitter_analysis_detail.m has to be modified
% a bit:
%        1.) Comment the definition file_name_base, since it is
%        done here
%
%        2.) Only print 4 plots with the numbers 900, 1000, 1001,
%        1020 . Comment out the other ones.
%
%        3.) In these plots use the color specified on
%        color_string, which is defined here. 
%
%        4.) Use as the initial parameters:
%        use_x_or_y = 'y';
%
%        use_initialisation = 1;
%        use_data_inspection = 0;
%        use_noise_level_calc = 1;
%        use_svd_cleaning = 0;
%        use_deg_freedom_plot = 0;
%        use_correlation_detection = 1;
%
%        use_removed_motion_analysis_space = 0;
%        use_removed_motion_analysis_time = 0;

% To be able to check, which BPM we are dealing with.
[nr_bpms_ring, bpms_index, raw_index] = get_BPM_index_FL();

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% June experiment: exchange of power supplies,   %
% shift of Okugi-san, Yves helped, charge 4-5e9  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Old setup 2
file_name_base = 'epics_all_orbit_130618_0250';
string_color = 'k';
jitter_analysis_detail

% New setup (exchanged power supplies Q20X and Q16FF)
%file_name_base = 'epics_all_orbit_130618_0453';
%string_color = 'k';
%jitter_analysis_detail

% Old setup 3
%file_name_base = 'epics_all_orbit_130618_0712';
%string_color = 'r';
%jitter_analysis_detail

% Old setup 4
%file_name_base = 'epics_all_orbit_130618_0740';
%string_color = 'r';
%jitter_analysis_detail

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 20th November experiment: different bumps and dipoles turned off %
% shift with Okugi-san with help from Oskar and Nuria              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Take initial data 
file_name_base = 'epics_all_orbit_131120_1025_nominal';
string_color = 'r';
jitter_analysis_detail

% Turn off ZV11X (current was already 0, but now turned off with V-SYSTEM)
file_name_base = 'epics_all_orbit_131120_1035_ZV11Xoff';
string_color = 'b';
%jitter_analysis_detail

% Create bump at QD20X (with ZV10X, ZV11X, ZV1FF): 1.7mm
file_name_base = 'epics_all_orbit_131120_1045_QD20X_bump1';
string_color = 'c';
%jitter_analysis_detail

% Retuned to machine to get higher charge, redo the measurement
file_name_base = 'epics_all_orbit_131120_1045_QD20X_bump2';
string_color = 'b';
%jitter_analysis_detail

% Smaller bump around QD20X_bump1: charge again at 0.62e9, bump: 0.85
file_name_base = 'epics_all_orbit_131120_1045_QD20X_bump3';
string_color = 'c';
%jitter_analysis_detail

% Create bump at QD20X and going forward to Q21X, Q16FF, Q15FF, Q14FF
% Bump was made with ZV11X, ZV1FF and the movers of Q16FF and
% Q15FF. The bump actually also extents into other magnets in the
% FF but of smaller amplitude. The amplitude is about: 1mm at Q21X and 
% smaller at Q20X and a bit bigger for the other magnets. 
file_name_base = 'epics_all_orbit_131120_1110_QD21X_bump1';
string_color = 'g';
%jitter_analysis_detail

% Same bump of half amplitude
file_name_base = 'epics_all_orbit_131120_1110_QD21X_bump3';
string_color = 'y';
%jitter_analysis_detail

% Turn off ZV10X, steer back with ZV11X
file_name_base = 'epics_all_orbit_131120_1125_ZV10X_off';
string_color = 'm';
%jitter_analysis_detail

% Create a bump around QD16X and QD14X (no BPM at QF15X apparently): 2mm, charge still 6.3
file_name_base = 'epics_all_orbit_131120_1135_QD16X_bump1';
string_color = 'b';
%jitter_analysis_detail

% Same bump with half the amplitude
file_name_base = 'epics_all_orbit_131120_1135_QD16X_bump2';
string_color = 'c';
%jitter_analysis_detail

% Create bump at Q18X (2mm),  and also Q19X (1mm) and Q20X (1mm) 
file_name_base = 'epics_all_orbit_131120_1135_QD18X_bump1';
string_color = 'm';
%jitter_analysis_detail

% Same bump with twice the amplitude
file_name_base = 'epics_all_orbit_131120_1135_QD18X_bump2';
string_color = 'r';
%jitter_analysis_detail

% Turn off ZH10X
file_name_base = 'epics_all_orbit_131120_1155_ZH10X_off';
string_color = 'g';
%jitter_analysis_detail

% Nominal condition again
file_name_base = 'epics_all_orbit_131120_1200_nominal';
string_color = 'y';
%jitter_analysis_detail



       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       %% Shift on the 27th of November 2013 %%
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       
% Nominal condition 
file_name_base = 'epics_all_orbit_131127_0950_nominal';
string_color = 'r';
%jitter_analysis_detail

% QF15FF mover 0.5mm
file_name_base = 'epics_all_orbit_131127_0955_QF15FF_550um';
string_color = 'b';
%jitter_analysis_detail

% QF16FF mover 1mm
file_name_base = 'epics_all_orbit_131127_0955_QF16FF_1000um';
string_color = 'b';
%jitter_analysis_detail

% ZV1FF small
file_name_base = 'epics_all_orbit_131127_1020_ZV1FF_0_5A';
string_color = 'y';
%jitter_analysis_detail

% ZV11X small
file_name_base = 'epics_all_orbit_131127_1020_ZV11X_0_1A';
string_color = 'r';
%jitter_analysis_detail

% Remeasure nominal jitter
file_name_base = 'epics_all_orbit_131127_1025_nominal';
string_color = 'k';
%jitter_analysis_detail

% ZV10X small to 0A
file_name_base = 'epics_all_orbit_131127_1025_ZV10X_0A';
string_color = 'm';
%jitter_analysis_detail

% ZV10X small to 0.2A
file_name_base = 'epics_all_orbit_131127_1025_ZV10X_0_19A';
string_color = 'r';
%jitter_analysis_detail

% ZV09X small to -0.342A
file_name_base = 'epics_all_orbit_131127_1025_ZV09X_neg_0_342A';
string_color = 'c';
%jitter_analysis_detail

% ZV09X small to -0.642A
file_name_base = 'epics_all_orbit_131127_1025_ZV09X_neg_0_642A';
string_color = 'b';
%jitter_analysis_detail

% ZV08X small to -0.495A
file_name_base = 'epics_all_orbit_131127_1025_ZV08X_neg_0_495A';
string_color = 'k';
%jitter_analysis_detail

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% To be able to change the corrector strength more, we steer now %
% back with ZV1FF. The fear is that otherwise the jitter effects %
% are too small                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ZV11X, ZV1FF
file_name_base = 'epics_all_orbit_131127_1105_ZV11X_ZV1FF';
string_color = 'y';
%jitter_analysis_detail

% ZV10X, ZV1FF
file_name_base = 'epics_all_orbit_131127_1120_ZV10X_ZV1FF';
string_color = 'y';
%jitter_analysis_detail

% ZV09X, ZV1FF
file_name_base = 'epics_all_orbit_131127_1120_ZV09X_ZV1FF';
string_color = 'c';
%jitter_analysis_detail

% ZV08X, ZV1FF
file_name_base = 'epics_all_orbit_131127_1120_ZV08X_ZV1FF';
string_color = 'b';
%jitter_analysis_detail

% Bump with ZV09X, ZV10X and ZV1FF to zero orbit at Q20X
file_name_base = 'epics_all_orbit_131127_1120_ZV09X_ZV10X_ZV1FF';
string_color = 'r';
%jitter_analysis_detail

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% I observed that the beam steering was much better this day %
% and wanted to check if the jitter level was changed.       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Nominal with no better beam orbit in the beginning of the beam line
file_name_base = 'epics_all_orbit_131129_1205_nominal';
string_color = 'm';
%jitter_analysis_detail
