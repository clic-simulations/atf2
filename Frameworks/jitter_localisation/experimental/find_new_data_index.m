% Function to determine the indices at which
% the time stamps of a BPM have changed.
%
% Juergen Pfingstner
% 6th of March 2014

function [change_index] = find_new_data_index(time_stamp)

change_index = zeros(size(time_stamp));

for i=2:length(change_index)
    if((time_stamp(i)-time_stamp(i-1))*24*60*60 > 0.001)	
        change_index(i)=1;
    end
end

change_index = find(change_index>0.5);
