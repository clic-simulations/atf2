% Function to align two sets of data according
% to and index number.
%
% Juergen Pfingstner
% 17th of December 2012

function [data_a1, data_a2, index_a1, index_a2] = align_with_index(data1, data2, index1, index2)

  data_a1 = zeros(size(data1));
  index_a1 = zeros(size(data1));
  data_a2 = zeros(size(data1));
  index_a2 = zeros(size(data1));
  row_count = 1;

  for i=1:length(data1)
    % Find the coresponding index
    index_temp = find_index(index2, index1(i));

    if(index_temp > 0.5)
      % An index has been found
      data_a1(row_count) = data1(i);
      index_a1(row_count) = index1(i);
      data_a2(row_count) = data2(index_temp);
      index_a2(row_count) = index_temp;
      row_count = row_count + 1;
    else
      % No index has been found -> nothing to do
    end
  end
  row_count = row_count - 1;

  % Cut the data to the right length
  data_a1 = data_a1(1:row_count);
  data_a2 = data_a2(1:row_count);
  index_a1 = index_a1(1:row_count);
  index_a2 = index_a2(1:row_count);
end
