% Function to read the corrector settings from EPICS
%
% Juergen Pfingstner
% 27th of Februar 2014

function [read_back_x, read_back_y] = set_pv_xy(pv_x, pv_y)

nr_x = length(pv_x);
nr_y = length(pv_y);
pv = [pv_x;pv_y];

% Get the values of the PVs
read_back=lcaPut(pv);

read_back_x = read_back(1:nr_x);
read_back_y = read_back(nr_x+1:nr_x+nr_y);
