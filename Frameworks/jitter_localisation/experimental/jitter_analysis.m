% Script to get the normalised jitter at the last turn of the DR and the
% EXT line.
%
% Juergen Pfingstner
% 6th June 2012
% 4th December 2012: strong modification

%%%%%%%%%%%%%%
% Parameters %
%%%%%%%%%%%%%%

use_real_machine = 1;
file_name_raw = 'raw_data_meas.dat';
file_name_raw_first = 'raw_data_first_meas.dat';

use_epics_reading = 1;
use_fs_reading = 0;
nr_trains = 100;
epics_data_mode = 'first'

use_bpm_noise_subtraction = 0;
use_disp_subtraction = 0;

main_figure_nr = 1;

%%%%%%%%%%%%%%%%%%
% Initialisation %
%%%%%%%%%%%%%%%%%%

global BEAMLINE
global INSTR

fprintf('Find the BPM indices\n');
[nr_bpms_ring, bpms_index, raw_index] = get_BPM_index_FL();

nr_total = length(bpms_index);
s_bpm = zeros(1,nr_total);
name_bpm = {'Bpm1', 'Bpm2'};
ref_noise = zeros(1,nr_total);
for i=1:nr_total
    s_bpm(i) = BEAMLINE{bpms_index(i)}.S;
    name_bpm{i} = BEAMLINE{bpms_index(i)}.Name;
    ref_noise(i) = BEAMLINE{bpms_index(i)}.Resolution;
end

% Debug
%file_id1 = fopen('bpm_name.dat', 'w');
%file_id2 = fopen('bpm_pos.dat', 'w');
%for i=1:nr_total
%    fprintf(file_id1, '%s\n',name_bpm{i});
%    fprintf(file_id2, '%d\n',s_bpm(i));
%end
%fclose(file_id1);
%fclose(file_id2);
% dEBUG

fprintf('Get the beta and dispersion functions at the BPM positions\n');
beta_x = FL.SimModel.Twiss.betax(bpms_index);
beta_y = FL.SimModel.Twiss.betay(bpms_index);
eta_x = FL.SimModel.Twiss.etax(bpms_index);
eta_y = FL.SimModel.Twiss.etay(bpms_index);
gamma = 1300/0.511;
epsilon_y = 0.3e-7./gamma;
epsilon_x = 50e-7./gamma;

% Debug
%figure(4);
%plot(ref_noise);
%grid on;
%hold off;

%figure(2);
%plot(beta_x, 'b');
%grid on;
%hold on;
%plot(beta_y, 'r');
%s_ring_linac = nr_bpms_ring + 0.5;
%line([s_ring_linac, s_ring_linac], [-1, 1], 'Color', 'k', 'LineStyle', '--');
%hold off;

%figure(3);
%plot(eta_x, 'b');
%grid on;
%hold on;
%plot(eta_y, 'r');
%s_ring_linac = nr_bpms_ring + 0.5;
%line([s_ring_linac, s_ring_linac], [-0.6, 0.6], 'Color', 'k', 'LineStyle', '--');
%hold off;
% dEBUG

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Part 1: Read the BPM readings of the last turn and the beam line with the flight simulator %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_fs_reading == 1)
    
    if(use_real_machine == 0)
      % Load the saved data
      raw = load(file_name_raw, '-ASCII');
      x_data = raw(:,raw_index*3-1);
      y_data = raw(:,raw_index*3);
    else
      % Read the measurement device data
      fprintf(1,'Update data structures\n');
      FlHwUpdate('wait',nr_trains);
      [s,raw]=FlHwUpdate('readbuffer',nr_trains);
      x_data = raw(:,raw_index*3-1);
      y_data = raw(:,raw_index*3);
    end    
    x_data = clean_bpm_data(x_data);
    y_data = clean_bpm_data(y_data);

    if(use_disp_subtraction == 1)
      for i=1:nr_trains
	  x_data(i,:) = x_data(i,:) - (x_data(i,:)*eta_x')*eta_x;
          y_data(i,:) = y_data(i,:) - (y_data(i,:)*eta_y')*eta_y;
      end
    end

    fprintf('Calculate the average and standard deviation of the measurements\n');
    mean_x = mean(x_data);
    mean_y = mean(y_data);
    std_x = std(x_data);
    std_y = std(y_data);
    if(use_bpm_noise_subtraction == 1)
	  std_x = std_x - ref_noise; 
          std_y = std_y - ref_noise;
    end
    std_x_norm = std_x./sqrt(epsilon_x.*beta_x);
    std_y_norm = std_y./sqrt(epsilon_y.*beta_y);
    
    % Save the data

    fprintf('Store the data\n');
    if(use_real_machine == 0)
        save_data = [mean_x', mean_y', std_x', std_y', std_x_norm', std_y_norm'];
        save('processed_data_sim.dat', 'save_data', '-ASCII');
        save_data = [x_data', y_data'];
        save('raw_data_sim.dat', 'save_data', '-ASCII');
    else
        save_data = [mean_x', mean_y', std_x', std_y', std_x_norm', std_y_norm'];
        save('processed_data_meas.dat', 'save_data', '-ASCII');
        save_data = [x_data', y_data'];
        save('original_data_meas.dat', 'save_data', '-ASCII');
        save(file_name_raw, 'raw', '-ASCII');
    end
    %ref_data = load('processed_data_sim_ref.dat');
    
    % Plot the data

    figure(main_figure_nr);
    handle1 = subplot(2,2,1);
    plot(std_x_norm, '-b');
    grid on;
    hold on;
    plot(std_y_norm, '-r');
    %plot(ref_data(:,3), '--b');
    %plot(ref_data(:,4), '--r');
    s_ring_linac = nr_bpms_ring + 0.5;
    yLimits = get(handle1,'YLim');
    line([s_ring_linac, s_ring_linac], yLimits.*0.8, 'Color', 'k', 'LineStyle', '--');
    title('Normalised std of beam offset (last)');
    legend('x', 'y');
    hold off;    

    handle3 = subplot(2,2,3);
    plot(std_x, '-b');
    grid on;
    hold on;
    plot(std_y, '-r');
    %plot(ref_data(:,5), '--b');
    %plot(ref_data(:,6), '--r');
    s_ring_linac = nr_bpms_ring + 0.5;
    yLimits = get(handle3,'YLim');
    line([s_ring_linac, s_ring_linac], yLimits.*0.8, 'Color', 'k', 'LineStyle', '--');
    title('Std of beam offset');
    legend('x', 'y');
    hold off;

    handle4 = subplot(2,2,4);
    plot(mean_x, '-b');
    grid on;
    hold on;
    plot(mean_y, '-r');
    %plot(ref_data(:,1), '--b');
    %plot(ref_data(:,2), '--r');
    s_ring_linac = nr_bpms_ring + 0.5;
    yLimits = get(handle4,'YLim');
    line([s_ring_linac, s_ring_linac], yLimits.*0.8, 'Color', 'k', 'LineStyle', '--');    
    title('Mean of beam offset');
    legend('x', 'y');
    hold off;

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Part 2: Read the BPM readings of the first turn %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if(use_epics_reading == 1)

    if(use_real_machine == 0)
        % Load the saved data
        raw_first = load(file_name_raw_first, '-ASCII');
        raw_first_x = raw_first(:,1)';
        raw_first_y = raw_first(:,2)';
        x_data_first = raw_first(:,1)';
        y_data_first = raw_first(:,2)';
    else
      [raw_first_x, raw_first_y] = get_BPM_data_EPICS(nr_trains, epics_data_mode);
      [raw_first_x, raw_first_y] = rearrange_EPICS_BPM_data(raw_first_x, raw_first_y);
      x_data_first = raw_first_x;
      y_data_first = raw_first_y;
    end

    x_data_first = clean_bpm_data(x_data_first);
    y_data_first = clean_bpm_data(y_data_first);
    
    if(use_disp_subtraction == 1)
      for i=1:nr_trains
	 x_data_first(i,:) = x_data_first(i,:) - (x_data_first(i,:)*eta_x(1:nr_bpms_ring)')*eta_x(1:nr_bpms_ring);
         y_data_first(i,:) = y_data_first(i,:) - (y_data_first(i,:)*eta_y(1:nr_bpms_ring)')*eta_y(1:nr_bpms_ring);
      end    
    end
    
    % Calculate the normalised variance of the jitter 

    mean_x_first = mean(x_data_first);
    mean_y_first = mean(y_data_first);
    std_x_first = std(x_data_first);
    std_y_first = std(y_data_first);
    if(use_bpm_noise_subtraction == 1)
	  std_x_first = std_x_first - ref_noise; 
          std_y_first = std_y_first - ref_noise;
    end
    std_x_first_norm = std_x_first./sqrt(epsilon_x.*beta_x(1:length(nr_bpms_ring)));
    std_y_first_norm = std_y_first./sqrt(epsilon_y.*beta_y(1:length(nr_bpms_ring)));

    % Plot the data
    
    figure(main_figure_nr);
    subplot(2,2,2);
    plot(std_x_first_norm, '-b');
    grid on;
    hold on;
    plot(std_y_first_norm, '-r');
    %plot(ref_data(:,3), '--b');
    %plot(ref_data(:,4), '--r');
    title('Normalised std of beam offset (first)');
    legend('x', 'y');
    hold off;
    
    % Save the data

    fprintf('Store the data\n');
    if(use_real_machine == 0)
        % Nothing to save
    else
        save_data = [mean_x_first', mean_y_first', std_x_first', std_y_first', std_x_first_norm', std_y_first_norm'];
        save('processed_data_first.dat', 'save_data', '-ASCII');
        save_data = [x_data_first', y_data_first'];
        save('original_data_first', 'save_data', '-ASCII');
        save_data = [raw_first_x', raw_first_y'];
        save(file_name_raw_first, 'save_data', '-ASCII');
    end
    %ref_data = load('processed_data_sim_ref.dat');


end



