% Function to read the corrector settings from EPICS
%
% Juergen Pfingstner
% 27th of Februar 2014

function [read_back] = get_pv(pv)

% Get the values of the PVs
read_back=lcaPut(pv);

