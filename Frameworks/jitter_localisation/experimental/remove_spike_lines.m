% Function to calculate the tf of different
% feedback versions. 
%
% Juergen Pfingstner
% 11.12.2012

function [data, line_marker] = remove_spike_lines(data, treshold, used_bpms)

   [M,N] = size(data);
   line_marker = [];
   for i=1:M
       if(max(abs(data(i,used_bpms)))>treshold)
         line_marker = [line_marker; i]; 
       end
   end 
   data(line_marker,:) = [];

end 
