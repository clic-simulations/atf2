% Script to call the jitter_analysis_detail several times, maybe in
% a loop. 
%
% Juergen Pfingstner
% 14th of May 2013

% Comments: The script jitter_analysis_detail.m has to be modified
% a bit:
%        1.) Comment the definition file_name_base, since it is
%        done here
%
%        2.) Only print 4 plots with the numbers 900, 1000, 1001,
%        1020 . Comment out the other ones.
%
%        3.) In these plots use the color specified on
%        color_string, which is defined here. 
%
%        4.) Use as the initial parameters:
%        use_x_or_y = 'y';
%
%        use_initialisation = 1;
%        use_data_inspection = 0;
%        use_noise_level_calc = 1;
%        use_svd_cleaning = 1;
%        use_deg_freedom_plot = 0;
%        use_correlation_detection = 1;
%
%        use_removed_motion_analysis_space = 0;
%        use_removed_motion_analysis_time = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test data set to check the scripts %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%file_name_base = 'epics_all_orbit_130514_1130';
%string_color = 'k';
%jitter_analysis_detail

%file_name_base = 'epics_all_orbit_130514_1135';
%string_color = 'b';
%jitter_analysis_detail

%file_name_base = 'epics_all_orbit_130514_1140';
%string_color = 'r';
%jitter_analysis_detail
%return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% December data set (low charge) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%file_name_base = 'epics_all_orbit_fb_121218_V1';
%string_color = 'g';
%jitter_analysis_detail
%return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% April data sets, Jochems shift (charge scan) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% charge ?, BPM calibration
%file_name_base = 'epics_all_orbit_130409_1840';
%string_color = 'g';
%jitter_analysis_detail

% charge 3*10^9
file_name_base = 'epics_all_orbit_130410_0040';
string_color = 'c';
jitter_analysis_detail

% charge 4*10^9
file_name_base = 'epics_all_orbit_130410_0050';
string_color = 'b';
jitter_analysis_detail

% charge 5*10^9
file_name_base = 'epics_all_orbit_130410_0051';
string_color = 'm';
jitter_analysis_detail

% charge 6*10^9
file_name_base = 'epics_all_orbit_130410_0056';
string_color = 'r';
jitter_analysis_detail

% charge 6.5*10^9
%file_name_base = 'epics_all_orbit_130410_0105';
%string_color = 'r';
%jitter_analysis_detail
