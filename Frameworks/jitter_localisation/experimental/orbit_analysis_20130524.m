% Script to analyse and plot the orbits of different measurments
% (mainly changing wakefield conditions (ref cavity scans) and
% charge  scans)
%
% Juergen Pfingstner
% 18th of May 2013

%%%%%%%%%%%%%%
% Parameters %
%%%%%%%%%%%%%%

%use_x_or_y = 'y';
%use_normalised_data = 0;

%%%%%%%%%%%%%%%%%
% Load the data %
%%%%%%%%%%%%%%%%%

file_name_base = 'epics_all_orbit_130524_1500_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y7 = load(file_name_y, '-ASCII');
    
file_name_base = 'epics_all_orbit_130524_1510_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y53 = load(file_name_y, '-ASCII');

file_name_base = 'epics_all_orbit_130524_1515_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y35 = load(file_name_y, '-ASCII');

file_name_base = 'epics_all_orbit_130524_1525_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y21 = load(file_name_y, '-ASCII');

file_name_base = 'epics_all_orbit_130524_1530_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y12 = load(file_name_y, '-ASCII');

file_name_model= sprintf('./data/model_%s_mod.dat', file_name_base);
model_data = load(file_name_model, '-ASCII');
beta_x = model_data(:,1)';
beta_y = model_data(:,2)';
eta_x = model_data(:,3)';
eta_y = model_data(:,4)';
nu_x = model_data(:,5)';
nu_y = model_data(:,6)';
sigma_x = model_data(:,7)';
sigma_y = model_data(:,8)';
s_bpm = model_data(:,9)';
ref_noise = model_data(:,10)';
   
y7 = y7(:,91:end);
y53 = y53(:,91:end);
y35 = y35(:,91:end);
y21 = y21(:,91:end);
y12 = y12(:,91:end);

sigma_x = sigma_x(91:end)';
sigma_y = sigma_y(91:end)';
ref_noise = ref_noise(:,91:end)';
nu_y = nu_y(:,91:end)';
index_bpm = 91:136;
scale_bpm_min = 90;
scale_bpm_max = 137;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cut away obviously noisy data %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%y7 = y7(1:end-1,:);
%y53 = y53(1:end-1,:);
y35 = y35(1:end-1,:);
%y21 = y21(1:end-1,:);
%y12 = y12(1:end-1,:);

y7(end,:) = [];
y7(887:889,:) = [];
y53(end,:) = [];
y53(880,:) = [];
y35(end,:) = [];
y35(552:564,:) = [];
y35(539,:) = [];
y35(413:414,:) = [];
y35(164:165,:) = [];
y21(end,:) = [];
y21(533,:) = [];
y21(129,:) = [];
y21(13:19,:) = [];
y12(end,:) = [];
y12(461:462,:) = [];

%%%%%%%%%%%%%%%%%%%%
% Analyse the data %
%%%%%%%%%%%%%%%%%%%%

avg_y7 = mean(y7);
avg_y53 = mean(y53);
avg_y35 = mean(y35);
avg_y21 = mean(y21);
avg_y12 = mean(y12);

figure(298);
plot(index_bpm, avg_y12.*10^6, 'x-c');
hold on;
grid on;
plot(index_bpm, avg_y21.*10^6,'x-b');
plot(index_bpm, avg_y35.*10^6,'x-m');
plot(index_bpm, avg_y53.*10^6,'x-g');
plot(index_bpm, avg_y7.*10^6,'x-k');

%avg_y7 = avg_y7*1e6;
%avg_y53 = avg_y53*1e6;
%avg_y35 = avg_y35*1e6;
%avg_y21 = avg_y21*1e6;
%avg_y12 = avg_y12*1e6;

avg_y7 = avg_y7./sigma_y';
avg_y53 = avg_y53./sigma_y';
avg_y35 = avg_y35./sigma_y';
avg_y21 = avg_y21./sigma_y';
avg_y12 = avg_y12./sigma_y';

%%%%%%%%%%%%%%%%%%%%
% Plot the results %
%%%%%%%%%%%%%%%%%%%%

figure(299);
set(gca,'FontSize',18);
plot(index_bpm, (avg_y53-avg_y7)*100, 'x-c', 'Linewidth', 1.5);
hold on;
grid on;
plot(index_bpm, (avg_y35-avg_y7)*100,'x-b', 'Linewidth', 1.5);
plot(index_bpm, (avg_y21-avg_y7)*100,'x-m', 'Linewidth', 1.5);
plot(index_bpm, (avg_y12-avg_y7)*100,'x-r', 'Linewidth', 1.5);
ylabel('beam orbit change [\% of $$\sigma_{y0}$$]');
xlabel('BPM nr. [1]');
axis([90 137 -280 180]);
legend('$$y(5.3\times 10^9)$$ - $$y(7\times 10^9)$$',['$$y(3.5\times ' ...
                    '10^9)$$ - $$y(7\times 10^9)$$'],'$$y(2.1\times 10^9)$$ - $$y(7\times 10^9)$$','$$y(1.2\times 10^9)$$ - $$y(7\times 10^9)$$');

figure(301);
plot(index_bpm, sigma_y, 'x-k');
hold on;
grid on;

figure(302);
plot(index_bpm, nu_y, 'x-k');
hold on;
grid on;