% Function to calculate the tf of different
% feedback versions. 
%
% Juergen Pfingstner
% 11.12.2012

function [data, removal_counter] = remove_spikes(data, treshold)

   [M,N] = size(data);
   removal_counter = 0;
   for i=1:M
     for j=1:N
       if(abs(data(i,j))>treshold)
         data(i,j) = 0;
         removal_counter = removal_counter+1;
       end
     end
   end 
   
end 
