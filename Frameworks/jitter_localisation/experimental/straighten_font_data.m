% Correct the often slightly wrong font data (add or delete points)
%
% Juergen Pfingstner
% 21th of March 2014

function [data_corr, ts_corr] = straighten_font_data(data, font_ts, file_name, y_ts)

% Analayse the time stamps
ts_ref = y_ts(1,1);
y_time_stamp = (y_ts-ts_ref)*24*60*60;
font_time_stamp = (font_ts-ts_ref)*24*60*60; 
%figure(666);
%plot(font_time_stamp(:,1)-y_time_stamp(1:end-2,1));

if(strcmp(file_name,'./data/gm_20140307_0745.mat')==1)

  % Data are in general okay, just one element too short
  data_corr = [mean(data(1:10,:)); data];
  ts_corr = [font_ts(1,:)-1/3.12/60/60/24; font_ts]; 

elseif(strcmp(file_name,'./data/gm2_20140307_0805.mat')==1)

  % One data point is missing and the data set is one element too short
  data_corr = [ data(1:449,:); mean(data(445:455,:)); data(450:end,:) ];  
  ts_corr = [ font_ts(1:449,:); mean(font_ts(449:450,:)); font_ts(450:end,:) ];

  data_corr = [mean(data_corr(1:10,:)); data_corr];
  ts_corr = [ts_corr(1,:)-1/3.12/60/60/24; ts_corr]; 

elseif(strcmp(file_name,'./data/beam_on_off_20140307_0825.mat')==1)

  data_corr = data;
  ts_corr = font_ts;

end

% Analayse the time stamps
font_time_stamp_corr = (ts_corr-ts_ref)*24*60*60; 
%figure(667)
%plot(font_time_stamp_corr(:,1)-y_time_stamp(:,1))
