% Script to get the normalised jitter at the last turn of the DR and the
% EXT line.
%
% Juergen Pfingstner
% 6th December 2012

%%%%%%%%%%%%%%
% Parameters %
%%%%%%%%%%%%%%

data_dir_local = 'sync_test5_20140304_2205';
%data_dir_local = 'test1';
nr_trains_local = floor(10*60*3.12*2);
%nr_trains_local = 200;
if(exist('external_parameter')==0)
  external_parameter = 0;
end
if(external_parameter == 0)
  file_name_base = data_dir_local;
  nr_trains = nr_trains_local;
end

%%%%%%%%%%%%%%%%%%
% Initialisation %
%%%%%%%%%%%%%%%%%%

global BEAMLINE
global INSTR
clear x_data ydata raw s;

fprintf(1,'Update the model from the real machine\n');
FlHwUpdate('all');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read the beam on off data %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[beam_on, time_stamp_beam_on] = get_beam_on_off_EPICS(nr_trains);

%%%%%%%%%%%%%%%%%
% Save the data %
%%%%%%%%%%%%%%%%%

fprintf('Store the data\n');
file_name_beam_on = ['./data/' file_name_base '_beam_on.dat'];
save(file_name_beam_on, 'beam_on','-ASCII');

file_name_beam_on_time_stamp = ['./data/time_stamp_beam_on_' file_name_base '.mat'];
save(file_name_beam_on_time_stamp, 'time_stamp_beam_on');

