%%%%%%%%%%%%%%
% Parameters %
%%%%%%%%%%%%%%

file_name_base = 'epics_all_orbit_131120_1045_QD20X_bump2';
%file_name_base = 'epics_all_orbit_fb_121217_V2';

use_x_or_y = 'x';

bpm=99;
%bpm2=100;
bpmv= [91 92 101 102];

%%%%%%%%%%%%%%%%%%
% Initialisation %
%%%%%%%%%%%%%%%%%%

format short

file_name_x= sprintf('./data/%s_x_mod.dat', file_name_base);
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
x0 = load(file_name_x, '-ASCII');
y0 = load(file_name_y, '-ASCII');

file_name_model= sprintf('./data/model_%s_mod.dat', file_name_base);
model_data = load(file_name_model, '-ASCII');
beta_x = model_data(:,1)';
beta_y = model_data(:,2)';
eta_x = model_data(:,3)';
eta_y = model_data(:,4)';
nu_x = model_data(:,5)';
nu_y = model_data(:,6)';
sigma_x = model_data(:,7)';
sigma_y = model_data(:,8)';
s_bpm = model_data(:,9)';
ref_noise = model_data(:,10)';

if(strcmp(use_x_or_y,'x')==1)
    y0 = x0;
    sigma_y = sigma_x;
    beta_y = beta_x;
    eta_y = eta_x;
    nu_y = nu_x;
end

t=[1:size(y0,2)];

%statistics
x0_ave=mean(x0);
y0_ave=mean(y0);
x0_std=std(x0);
y0_std=std(y0);
N=length(y0_ave);

% initial jitter
for i=1:136;
    jitty0(i)=y0_std(i)/sigma_y(i);
end
figure(3);
plot(t,jitty0)
hold on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% correlation coefficient

%figure(2);
%subplot(2,1,1);
%plot(y0(:,bpm),y0(:,bpm2), 'x')


for i=bpm;
    for j=1:N;
     c_y=corrcoef(y0(:,i),y0(:,j));
     c_x=corrcoef(x0(:,i),x0(:,j));
     r_y(j)=c_y(1,2);
     r_x(j)=c_x(1,2);
    end
end

% removing correlation

diff1 = zeros(size(y0));

for j=bpmv;
    for i=1:N;
        if i~=j;
            covar=cov(y0(:,j),y0(:,i));
            c12=covar(1,2);
            c11=std(y0(:,j));
            c22=std(y0(:,i));
            k=(c12/c11/c11);
            if(j == 102)
                diff1(:,i)=k*y0(:,j);
            end
            y0(:,i)=y0(:,i)-k*y0(:,j);
        else
            y0(:,i)=y0(:,j);
            %diff(:,i)=0;
        end
    end
end


%file1='correlation_correction_bpm_118_119.dat';
%save(file1,'y0','-ASCII');

%figure(2);
%subplot(2,1,2);
%plot(y0(:,bpm),y0(:,bpm2), 'x')

% recalculating correlation

for i=bpm;
    for j=1:N;
       covar=cov(y0(:,bpm),y0(:,j));
       c12=covar(1,2);
       c_y=corrcoef(y0(:,bpm),y0(:,j));
       r_y2(j)=c_y(1,2);
       c_x2=corrcoef(x0(:,i),x0(:,j));
       r_x2(j)=c_x2(1,2);
    end
end

figure(1);
%hold on;
title('Correlation coefficient');
xlabel('BPM number');
ylabel('r [1]')
grid;
plot(t,r_y)
hold on;
plot(t,r_y2,'r')
hold off;

%file10='corr_remove_0_x.dat';
%save(file10,'r_x2','-ASCII');
%file11='corr_remove_0_y.dat';
%save(file11,'r_y2','-ASCII');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% jitter analysis

% model
%diff_std=std(diff);
y0_std=std(y0);

for i=1:136;
    noise(i)=ref_noise(i)/sigma_y(i);
    jitty1(i)=y0_std(i)/sigma_y(i);
    %diff1(i)=diff_std(i)/sigma_y(i);
    %jitty1(i)=y02_std(i)/sigma_y(i);
    %jitty2(i)=y03_std(i)/sigma_y(i);
end
figure(3);
title('Standard deviation of the differential signal levels in ATF2');
xlabel('BPM nr. [1]');
ylabel('\sigma_s/\sigma_y');
pbaspect([4 2 1]);
plot(t,jitty1,'r',t,noise,'k')
axis([90 138 0 1]);
hold off;

jitty_ff=[jitty1(112:123) jitty1(126:132)];
jit_mean1=mean(jitty_ff)

%diff_ff=[diff1(112:124) diff1(126:132)];
%diff_mean1=mean(diff_ff)

%file1='./paper/corr_remove_98_99_100_102_112_118_y.dat.dat';
%save(file1,'jitty1','-ASCII');

diff1_x = diff1;

%clear;

