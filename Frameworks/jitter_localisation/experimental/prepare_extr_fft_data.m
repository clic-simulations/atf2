% File to calculate the PSD and the according frequencies out of
% FFT data
%
% Juergen Pfingstner
% 24th of June 2013

function [f1, data_s1, data_s2] = prepare_extr_fft_data(fft_data, ...
                                             Td, mode, remove_first_point);

[nr_f, temp] = size(fft_data);
f1 = 1:nr_f;
f1 = f1';
f1 = f1./nr_f./Td;
f1 = f1(1:round(end/2));

data_s1 = abs(fft_data(1:round(end/2),1)).^2;
data_s2 = abs(fft_data(1:round(end/2),2)).^2;

filt_diff_2 = 2-2.*cos(2*pi*f1*Td);

if(strcmp(mode, 'full')==1)
    data_s1 = data_s1./filt_diff_2;
    data_s2 = data_s2./filt_diff_2;
end

if(remove_first_point == 1)
   f1 = f1(2:end); 
   data_s1 = data_s1(2:end); 
   data_s2 = data_s2(2:end); 
end