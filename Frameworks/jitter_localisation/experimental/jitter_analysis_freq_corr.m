% Script to calculate and plot frequency properties
% if the stored data                                       
% 
% Juergen Pfingstner
% 6th of December 2012 

%%%%%%%%%%%%%%
% Parameters %
%%%%%%%%%%%%%%

file_name_base = 'epics_all_orbit_130410_0105';
%file_name_base = 'epics_all_orbit_130314_1600';
%file_name_base = 'epics_all_orbit_fb_121218_V1';
%file_name_base = 'epics_all_orbit_fb_121217_V2';

bpm_noise_subtraction = 'none'; % 'none', 'simple', 'svd'
nr_sv_filt = 10;
store_svd_matrix = 0;

use_time_stamps_arrange = 0;
use_shift_data = 0;
nr_shift_steps = 0;
figure_start_nr = 10;
bpm_index_plot = 114;
analysis_area = 'all'; % 'atf', 'atf2', 'atf2_clean', 'all'

N_bartlett_blocks = 1;
use_jitter_filter = 0;

%%%%%%%%%%%%%%%%%
% Load the data %
%%%%%%%%%%%%%%%%%

file_name_x= sprintf('./data/%s_x_mod.dat', file_name_base);
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
file_name_time_stamp_x= sprintf('./data/time_stamp_%s_x_mod.dat', file_name_base);
file_name_time_stamp_y= sprintf('./data/time_stamp_%s_y_mod.dat', file_name_base);
if(store_svd_matrix)
  file_name_base_x= sprintf('x_%s', file_name_base);
  file_name_base_y= sprintf('y_%s', file_name_base);
else
  file_name_base_x = 'no';
  file_name_base_y = 'no';
end

x_data = load(file_name_x, '-ASCII');
y_data = load(file_name_y, '-ASCII');
%x_time_stamp = load(file_name_time_stamp_x, '-ASCII');
%y_time_stamp = load(file_name_time_stamp_y, '-ASCII');
[nr_trains, nr_bpms] = size(x_data);

%%%%%%%%%%%%%%%%%%%%%%%
% Get some model data %
%%%%%%%%%%%%%%%%%%%%%%%

file_name_model= sprintf('./data/model_%s_mod.dat', file_name_base);
model_data = load(file_name_model, '-ASCII');
beta_x = model_data(:,1)';
beta_y = model_data(:,2)';
eta_x = model_data(:,3)';
eta_y = model_data(:,4)';
nu_x = model_data(:,5)';
nu_y = model_data(:,6)';
sigma_x = model_data(:,7)';
sigma_y = model_data(:,8)';
s_bpm = model_data(:,9)';
ref_noise = model_data(:,10)';

figure(11);
plot(nu_y, 'x-b');
grid on;
hold on;
%plot(nu_y, 'x-r');

figure(12);
plot(sigma_y, 'x-b');
grid on;
hold on;
%plot(nu_y, 'x-r');

%%%%%%%%%%%%%%%%%%%%
% Process the data %
%%%%%%%%%%%%%%%%%%%%

f_rep = 3.12;
T_0 = nr_trains./f_rep;
t = (1:nr_trains)./f_rep;

if(strcmp(bpm_noise_subtraction,'svd'))
  x_data = svd_filter(x_data, nr_sv_filt, file_name_base_x);
  y_data = svd_filter(y_data, nr_sv_filt, file_name_base_y);
end

disp_mode = zeros(nr_trains,1);

[psd_x, f] = psd_bartlett(x_data, T_0, N_bartlett_blocks);
[psd_y, f] = psd_bartlett(y_data, T_0, N_bartlett_blocks);
[psd_energy, ~] = psd_bartlett(disp_mode, T_0, N_bartlett_blocks);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create the BPM noise spectrum %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(strcmp(analysis_area,'atf2_clean'))
  psd_noise = ((0.1e-6)^2*T_0/N_bartlett_blocks/length(f)).*ones(size(psd_x));
elseif(strcmp(analysis_area,'atf'))
  %psd_noise = ((20e-6)^2*T_0/N_bartlett_blocks/length(f)).*ones(size(psd_x));
  psd_noise = ((20e-6)^2*T_0/N_bartlett_blocks/length(f)).*ones(size(psd_x));
elseif(strcmp(analysis_area,'atf2'))
  %psd_noise = ((5e-6)^2*T_0/N_bartlett_blocks/length(f)).*ones(size(psd_x));
  psd_noise = ((0.1e-6)^2*T_0/N_bartlett_blocks/length(f)).*ones(size(psd_x));
elseif(strcmp(analysis_area,'all'))
  %psd_noise = ((5e-6)^2*T_0/N_bartlett_blocks/length(f)).*ones(size(psd_x));
  psd_noise = ((0.1e-6)^2*T_0/N_bartlett_blocks/length(f)).*ones(size(psd_x));
end

%%%%%%%%%%%%%%%%%%%%%%%
% Estimate the jitter %
%%%%%%%%%%%%%%%%%%%%%%%

tf_jitter = calc_tf_fb(2, 1, 1./f_rep, f);
psd_filter_jitter = (abs(tf_jitter).^2)./2;

%figure(3);
%plot(abs(tf_jitter));
%grid on;

if(use_jitter_filter == 1)
  psd_x = diag(psd_filter_jitter)*psd_x;
  psd_y = diag(psd_filter_jitter)*psd_y;
end

%%%%%%%%%%%%%%%%%%%%%%%
% Calc integrated RMS %
%%%%%%%%%%%%%%%%%%%%%%%

irms_x = sqrt(cumsum(psd_x(end:-1:1,:))./(T_0./N_bartlett_blocks));
irms_x = irms_x(end:-1:1,:);
irms_y = sqrt(cumsum(psd_y(end:-1:1,:))./(T_0./N_bartlett_blocks));
irms_y = irms_y(end:-1:1,:);
irms_noise = sqrt(cumsum(psd_noise(end:-1:1,:))./(T_0./N_bartlett_blocks));
irms_noise = irms_noise(end:-1:1,:);

% Pulse to pulse integrator
%p_i = 1;
%T_d = 1./f_rep;
%option_fb = 1;

% Kubo's damping ring integrator
p_i = 0.5;
T_d = 15;
option_fb = 1;

%% Okugis's extraction line FB
%p_i = 0.5;
%T_d = 2;
%option_fb = 1;

tf_fb_model = calc_tf_fb(option_fb, p_i, T_d, f(2:end));

%%%%%%%%%%%%%%%%%
% Plot the data %
%%%%%%%%%%%%%%%%%

handle1 = figure(figure_start_nr);
%semilogy(f, psd_x(:,bpm_index_plot), 'b');
%grid on;
%hold on;
%semilogy(f, psd_y(:,bpm_index_plot), 'r');
%%semilogy(f, psd_energy, 'c');
%semilogy(f, psd_noise(:,bpm_index_plot), 'k');
loglog(f, psd_x(:,bpm_index_plot), 'b');
grid on;
hold on;
loglog(f, psd_y(:,bpm_index_plot), 'r');
%semilogy(f, psd_energy, 'k');
loglog(f, psd_noise(:,bpm_index_plot), 'k');
title('PSD of beam jitter');
%legend('x no FB', 'y no FB');
xlabel('Frequency [Hz]');
ylabel('PSD [m^2/Hz]');
hold off;

%handle1a = figure(figure_start_nr+1);
%loglog(f, irms_x(:,bpm_index_plot), 'b');
%grid on;
%hold on;
%loglog(f, irms_y(:,bpm_index_plot), 'r');
%%loglog(f, irms_energy, 'c');
%%loglog(f, irms_noise(:,bpm_index_plot), 'k');
%title('IRMS of beam jitter');
%legend('x no FB', 'y no FB');
%xlabel('Frequency [Hz]');
%ylabel('IRMS [m]');
%hold off;

%handle1b = figure(figure_start_nr+2);
%plot(x_data(:,bpm_index_plot), 'b');
%grid on;
%hold on;
%loglog(y_data(:,bpm_index_plot), 'r');
%title('Time signal of beam jitter');
%legend('x no FB', 'y no FB');
%xlabel('Time [s]');
%ylabel('offset [m]');
%hold off;

%%%%%%%%%%%%%%%%%%%%%%%
% Correlation studies %
%%%%%%%%%%%%%%%%%%%%%%%

% Debug
%x_data = load('./data/correction_bpm_102_after_100_y.dat');
%y_data = load('./data/correction_bpm_102_after_100_y.dat');
% dEBUG

%handle5 = figure(figure_start_nr+6);
%plot(irms_x(1,:)./sigma_x(eta_indices)*100, 'b');
%grid on;
%hold on;
%plot(irms_y(1,:)./sigma_y(eta_indices)*100, 'r');
%%plot(irms_x(1,:), 'b');
%%grid on;
%%hold on;
%%plot(irms_y(1,:), 'r');
%%semilogy(f, tf_y_std, '-r');
%%semilogy(f, tf_xy_std, '-g');
%title('Beam jitter');
%%legend('x');
%xlabel('BPM number [1]');
%ylabel('Jitter [%]');
%%axis([0 length(eta_indices) 0 60])
%hold off;

%handle6 = figure(figure_start_nr+7);
%plot(y_data(:,109), y_data(:,120), 'xb');
%grid on;
%hold on;
%title('Beam correlation');
%xlabel('BPM 18 [m]');
%ylabel('BPM 25 [m]');
%hold off;

%handle7 = figure(figure_start_nr+8);
%plot(y_data(:,120), y_data(:,121), 'xb');
%grid on;
%hold on;
%title('Beam correlation x');
%xlabel('BPM 18 [m]');
%ylabel('BPM 25 [m]');
%hold off;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the correlation coefficients %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

corr_coef_x = zeros(nr_bpms, 1);
corr_coef_y = zeros(nr_bpms, 1);
corr_coef_xy = zeros(nr_bpms, 1);

% Data shift
if(nr_shift_steps==0)
  x_data_shift = x_data;
  y_data_shift = y_data;
elseif(nr_shift_steps < 0)
  x_data_shift = [x_data(-nr_shift_steps+1:end,:); zeros(-nr_shift_steps,nr_bpms)];
  y_data_shift = [y_data(-nr_shift_steps+1:end,:); zeros(-nr_shift_steps,nr_bpms)];
else
  x_data_shift = [zeros(nr_shift_steps,nr_bpms); x_data1(1:end-nr_shift_steps,:)];
  y_data_shift = [zeros(nr_shift_steps,nr_bpms); y_data1(1:end-nr_shift_steps,:)];
end

if(use_time_stamps_arrange==1)
  pulse_index_x = round(x_time_stamp.*f_rep);
  pulse_index_y = round(y_time_stamp.*f_rep);

  for i = 1:nr_bpms
    corr_coef_x(i) = corrcoef_ordered(x_data(:,bpm_index_plot), x_data_shift(:,i), pulse_index_x(:,bpm_index_plot), pulse_index_x(:,i));
    corr_coef_y(i) = corrcoef_ordered(y_data(:,bpm_index_plot), y_data_shift(:,i), pulse_index_y(:,bpm_index_plot), pulse_index_y(:,i));
    corr_coef_xy(i) = corrcoef_ordered(x_data(:,bpm_index_plot), y_data_shift(:,i), pulse_index_x(:,bpm_index_plot), pulse_index_y(:,i));
    %corr_coef_xy(i) = corrcoef_ordered(y_data(:,bpm_index_plot), x_data_shift(:,i), pulse_index_y(:,bpm_index_plot), pulse_index_x(:,i));
    %corr_coef_xy(i) = corrcoef_ordered(x_data(:,i), y_data_shift(:,i), pulse_index_x(:,i), pulse_index_y(:,i));  
  end
else
  for i = 1:nr_bpms
    corr_temp = corrcoef(x_data(:,bpm_index_plot), x_data_shift(:,i));
    corr_coef_x(i) = corr_temp(1,2);
    corr_temp = corrcoef(y_data(:,bpm_index_plot), y_data_shift(:,i));
    corr_coef_y(i) = corr_temp(1,2);
    corr_temp = corrcoef(x_data(:,bpm_index_plot), y_data_shift(:,i));
    corr_coef_xy(i) = corr_temp(1,2);
    %corr_temp = corrcoef(y_data(:,bpm_index_plot), x_data_shift(:,i));
    %corr_coef_xy(i) = corr_temp(1,2);
    %corr_temp = corrcoef(x_data(:,i), y_data_shift(:,i));
    %corr_coef_xy(i) = corr_temp(1,2);
  end
end

handle8 = figure(figure_start_nr+9);
plot(corr_coef_x, 'b');
grid on;
hold on;
plot(corr_coef_y, 'r');
plot(corr_coef_xy, 'k');
title('Correlation coefficient');
axis([93 138 -1 1]);
xlabel('BPM nr. [1]');
ylabel('r [1]');
legend('x', 'y', 'xy');
hold off;

% Debug: Estimate the signal in y due to correlation
y_cleaned = zeros(size(y_data));
diff_xy = zeros(size(y_data));
for j=bpm_index_plot;
    for i=1:nr_bpms;
        if i~=j;
            covar=cov(x_data(:,j),y_data(:,i));
            c12=covar(1,2);
            c11=std(x_data(:,j));
            c22=std(y_data(:,i));
            k=(c12/c11/c11);
            diff_xy(:,i)=k*x_data(:,j);
            y_cleaned(:,i)=y_data(:,i)-k*x_data(:,j);
        else
            y_cleaned(:,i)=y_data(:,j);
            diff_xy(:,i)=0;
        end
    end
end
y_cleaned_std = std(y_cleaned);
y_std = std(y_data);
y_cleaned_rel = y_cleaned_std./sigma_y;
y_rel = y_std./sigma_y;
y_cleaned_rel_extracted=[y_cleaned_rel(112:123) y_cleaned_rel(126:132)];
y_rel_extracted=[y_rel(112:123) y_rel(126:132)];
y_rel_mean=mean(y_rel_extracted)
y_cleaned_rel_mean=mean(y_cleaned_rel_extracted)
% dEBUG

% Save some data

%save('correlation_98_full_x.dat', 'corr_coef_x','-ascii');
%save('correlation_98_full_y.dat', 'corr_coef_y','-ascii');
%return;

% Perform SVD analysis
%fprintf(1,'Calculate the SVD of the y data\n');
%[U,S,V] = svd(y_data(:,91:end));
%index_temp = [106:124, 126:133];
index_temp = [108:126, 128:135];
%[U,S,V] = svd([y_data1(:,108:126), y_data1(:,128:135)]);
[U,S,V] = svd(y_data(:, index_temp));

svd_plot_index = 1;

%figure(100);
%plot(diag(S), 'x-b');
%grid on;
%hold on;
%plot(diag(S),'x-r');

%figure(101);
%plot(index_temp-2, V(:,svd_plot_index)./sigma_y(index_temp)', 'x-b');
%grid on;
%hold on;



