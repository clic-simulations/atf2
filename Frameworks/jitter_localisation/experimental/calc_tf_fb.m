% Function to calculate the tf of different
% feedback versions. 
%
% Juergen Pfingstner
% 11.12.2012

function [tf_closed] = calc_tf_fb(option, parameter, Td, f)

z = exp(i*2*pi*f*Td);
tf_G = 1./z;

if(option == 1)
  % Integrator pulse to pulse
  tf_contr = parameter.*(z)./(z-1);
  tf_open = tf_contr.*tf_G;
  tf_closed = tf_G./(1+tf_open);
elseif(option == 2)
  % Jitter calculation TF
  tf_closed = ones(size(z)) - 1./z;
else
  fprintf(1, 'Unknown FB option\n');
  tf_closed = ones(size(z));
end

end 
