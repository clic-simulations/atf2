% Function to bring the acquired data to the wanted for for the
% data analyis. 
%
% Juergen Pfingstner
% 28. Feb. 2013

%%%%%%%%%%%%%%
% Parameters %
%%%%%%%%%%%%%%

%data_dir_local = 'epics_all_orbit_131120_1025_nominal';
%data_dir_local = 'epics_all_orbit_130618_0250';
data_dir_local = 'sync_test2_20140228_0045';
%data_dir_local = 'sync_test1_20140227_2340';
%file_name_otr_data_local = 'emit2dOTRp_20140307T025450.mat';
if(exist('external_parameter')==0)
  external_parameter = 0;
end
if(external_parameter == 0)
  file_name_base = data_dir_local;
  file_name_otr_data = file_name_otr_data;
end
%if(exist('external_parameter')==0)
%    if(external_parameter == 0)
%        file_name_base = 'epics_all_orbit_131129_1205_nominal';
%    end
%end
use_old_or_new_format = 'new'; % Change on the 28th of February 2013

use_disp_subtraction = 0;
use_substract_mean = 0; 
use_spike_removal = 2;     % 0 ... no removal, 
                           % 1 ... set spikes zero (caused problems)
                           % 2 ... remove spike lines (default)
spike_removal_treshold_x = 5e-4; % For diff data
spike_removal_treshold_y = 5e-4; % For diff data
%spike_removal_treshold_x = 0.2e-4; % For diff data
%spike_removal_treshold_y = 0.2e-4; % For diff data
%spike_removal_treshold_x = 5e-2; % For full data
%spike_removal_treshold_y = 1e-2; % For full data
%spike_removal_treshold_y = 2e-3; % For full data
use_diff_data = 1;
use_shift_stripline_data = 0;
use_mat_time_stamp = 1;
use_font_bpm = 1;
font_bpm_index = [1,3,5];
use_data_limits = 0;
limit_start_index = 0;
limit_end_index = 0;
use_beam_on_off = 1;
use_bug_correction_20140307 = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load data and separate them %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_new_data_format == 1)
  file_name_offset = ['./data/' file_name_base '.mat'];
  raw_data = load(file_name_offset);
  x_data = raw_data.x_data;
  y_data = raw_data.y_data;
  [nr_trains, nr_bpms] = size(x_data);
  beam_on_off = raw_data.beam_on;
  x_time_stamp = raw_data.time_stamp_x;
  y_time_stamp = raw_data.time_stamp_y;

  % Correct a bug in the data conversion on y direction
  if(use_bug_correction_20140307 == 1)
    y_data = y_data.*1.6./1e6;
    y_data(:,font_bpm_index) = y_data(:,font_bpm_index)./1.6.*1e6;

    % There was a bug in the data acquisition. Therefore there is sometimes 
    % one time step missing in x and instead there are made to time steps in 
    % the next line. This causes some problems and therefore the the same time 
    % steps are substituted with interpolated values. 
    ts_test = x_time_stamp(:,20);
    ts_test = (ts_test-ts_test(1))*24*3600;
    change_marker = [];

    for i=2:length(ts_test)
      if(abs(ts_test(i)-ts_test(i-1)) < 0.01)
	change_marker=[change_marker; i];
      end
    end

    nr_interpolations = length(change_marker);
    if(nr_interpolations ~= 0)
      for i=1:nr_interpolations
	change_index = change_marker(i);
	x_data(change_index,:) = mean([x_data(change_index-1,:); x_data(change_index+1,:)]);
	x_time_stamp(change_index,:) = mean([x_time_stamp(change_index-1,:); x_time_stamp(change_index+1,:)]);
      end
    end
  end

  if(use_beam_on_off == 1)
    beam_on_off = raw_data.beam_on;
    beam_on_off_time_stamp = raw_data.time_stamp_beam_on;
  end

  if(use_font_bpm == 1)
    font_data = raw_data.font_data;
    font_time_stamp = raw_data.time_stamp_font;
    
    [font_data, font_time_stamp] = straighten_font_data(font_data, font_time_stamp, file_name_offset, y_time_stamp);
    y_data(:,font_bpm_index) = font_data;
  end

  % Debug
  %x_ts_should_be = (0:(length(x_time_stamp(:,20))-1))./3.1243;
  %figure(30);
  %plot(((x_time_stamp(:,20)-x_time_stamp(1,20))*24*3600)' - x_ts_should_be, '-b');
  %grid on;
  %hold on; 
  %title('BPM x data timing with respect to perfect reference');

  %figure(31);
  %plot((x_time_stamp(2:end,20)-x_time_stamp(1:end-1,20))*24*3600,'-xb');
  %grid on;
  %hold on;
  %title('BPM x time stamp difference');

  %y_ts_should_be = (0:(length(y_time_stamp(:,20))-1))./3.1243;
  %figure(32);
  %plot(((y_time_stamp(:,20)-y_time_stamp(1,20))*24*3600)' - y_ts_should_be, '-b');
  %grid on;
  %hold on; 
  %title('BPM y data timing with respect to perfect reference');

  %figure(33);
  %plot((y_time_stamp(2:end,20)-y_time_stamp(1:end-1,20))*24*3600,'-xb');
  %grid on;
  %hold on;
  %title('BPM y time stamp difference');

  %figure(34);
  %plot((x_time_stamp(:,20)-y_time_stamp(:,20))*24*3600,'-xb');
  %grid on;
  %hold on;
  %title('BPM x and y time stamp difference');

  % dEBUG


else
  file_name_offset = ['./data/' file_name_base '.dat'];
  if(use_mat_time_stamp == 1)
    file_name_time_stamp = ['./data/time_stamp_' file_name_base '.mat'];
  else
    file_name_time_stamp = ['./data/time_stamp_' file_name_base '.dat'];
  end
  file_name_model = ['./data/model_' file_name_base '.dat'];

  raw_data = load(file_name_offset, '-ASCII');
  [nr_trains, nr_bpms] = size(raw_data)
  nr_trains = nr_trains./2;
  x_data = raw_data(1:nr_trains,:);
  y_data = raw_data(nr_trains+1:end,:);

  if(use_beam_on_off == 1)
    file_name_beam_on_off = ['./data/' file_name_base '_beam_on.dat'];
    beam_on_off = load(file_name_beam_on_off, '-ASCII');
    beam_on_off = beam_on_off(1:nr_trains);
  end

  if(use_mat_time_stamp == 1)
    raw_time = load(file_name_time_stamp);
    raw_time = raw_time.save_data;
    TOL=1e-10;
    if(sum(sum(abs(imag(raw_time))))<TOL)
      % Already in the newest version, were data are stored in
      % Matlab format and with full and not just differential information.
      x_time_stamp = raw_time(1:nr_trains,:);
      y_time_stamp = raw_time(nr_trains+1:end,:);      
    else
      % Data were saved as complex numbers 
      x_time_stamp = convert_epics_time(raw_time(1:nr_trains,:));
      y_time_stamp = convert_epics_time(raw_time(nr_trains+1:end,:));      
    end
  else
    raw_time = load(file_name_time_stamp, '-ASCII');
    x_time_stamp = raw_time(1:nr_trains,:);
    y_time_stamp = raw_time(nr_trains+1:end,:);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create indices of the different BPM types %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create BPM indices

if(use_new_data_format == 1)
  dr_bpm_index = [];

  stripline_bpm_index = [1:9,13:15];

  cavity_bpm_index = 1:nr_bpms;
  cavity_bpm_index(stripline_bpm_index) = [];

  if(use_font_bpm == 1)
    stripline_bpm_index(font_bpm_index) = [];
    nr_font_bpm = length(font_bpm_index);
  end

else
  dr_bpm_index = 1:90;

  stripline_bpm_index = [1:99,103:105];

  cavity_bpm_index = 1:nr_bpms;
  cavity_bpm_index(stripline_bpm_index) = [];

  if(use_font_bpm == 1)
    stripline_bpm_index(font_bpm_index+90) = [];
    nr_font_bpm = length(font_bpm_index);
  end
  stripline_bpm_index(dr_bpm_index)=[];
end

nr_cavity_bpm = length(cavity_bpm_index);
nr_stripline_bpm = length(stripline_bpm_index);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bring old data to the new format %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(strcmp(use_old_or_new_format, 'old')==1)
    x_data = [x_data(:,1:48), x_data(:,51:end)];
    y_data = [y_data(:,1:48), y_data(:,51:end)];
    x_time_stamp = [x_time_stamp(:,1:48), x_time_stamp(:,51:end)];
    y_time_stamp = [y_time_stamp(:,1:48), y_time_stamp(:,51:end)];
    
    % Create model data, since they where not stored at that time
    global BEAMLINE
    global INSTR
    [nr_bpms_ring, bpms_index, raw_index] = get_BPM_index_FL();

    nr_total = length(bpms_index);
    s_bpm = zeros(1,nr_total);
    ref_noise = zeros(1,nr_total);
    for i=1:nr_total
        s_bpm(i) = BEAMLINE{bpms_index(i)}.S;
        ref_noise(i) = BEAMLINE{bpms_index(i)}.Resolution;
    end
    beta_x = FL.SimModel.Twiss.betax(bpms_index);
    beta_y = FL.SimModel.Twiss.betay(bpms_index);
    eta_x = FL.SimModel.Twiss.etax(bpms_index);
    eta_y = FL.SimModel.Twiss.etay(bpms_index);
    nu_x = FL.SimModel.Twiss.nux(bpms_index);
    nu_y = FL.SimModel.Twiss.nuy(bpms_index);
    gamma = 1300/0.511;
    epsilon_y = 0.3e-7./gamma;
    epsilon_x = 50e-7./gamma;
    sigma_x = sqrt(epsilon_x.*beta_x);
    sigma_y = sqrt(epsilon_y.*beta_y);
    model_data = [beta_x', beta_y', eta_x', eta_y', nu_x', nu_y', sigma_x', ...
             sigma_y', s_bpm', ref_noise'];
else
  if(use_new_data_format == 1)
    % Here there is a small problem. There are a few different elements 
    % in the stored data structures. So we copy only a subset, which 
    % represents the whole accelerator though. 
    for copy_index=1:length(raw_data.PS)
        if(copy_index == length(raw_data.PS))
    	  PS(copy_index) = PS(copy_index-1);
          PS(copy_index).Element = raw_data.PS(copy_index).Element;
    	end
        PS(copy_index).Ampl = raw_data.PS(copy_index).Ampl;
    end
    for copy_index=1:1933 
    	BEAMLINE{copy_index} = raw_data.BEAMLINE{copy_index};
    end
    otr_data = load(file_name_otr_data);
    otr0_twiss_x = struct('alpha', otr_data.data.alphx);
    otr0_twiss_x.beta = otr_data.data.betax;
    otr0_twiss_x.eta = 0;
    otr0_twiss_x.etap = 0;
    otr0_twiss_x.nu = 0;
    otr0_twiss_y = struct('alpha', otr_data.data.alphy);
    otr0_twiss_y.beta = otr_data.data.betay;
    otr0_twiss_y.eta = 0;
    otr0_twiss_y.etap = 0;
    otr0_twiss_y.nu = 0;
    %[stat, iex_twiss] = GetTwiss(1535, 1284, otr0_twiss_x, otr0_twiss_y);
    [stat, iex_twiss] = GetTwiss(1535, 1, otr0_twiss_x, otr0_twiss_y);

    iex_twiss_x = struct('alpha', iex_twiss.alphax(1));
    iex_twiss_x.beta = iex_twiss.betax(1);
    iex_twiss_x.eta = iex_twiss.etax(1);
    iex_twiss_x.etap = iex_twiss.etapx(1);
    iex_twiss_x.nu = 0;
    iex_twiss_y = struct('alpha', iex_twiss.alphay(1));
    iex_twiss_y.beta = iex_twiss.betay(1);
    iex_twiss_y.eta = iex_twiss.etax(1);
    iex_twiss_y.etap = iex_twiss.etapx(1);
    iex_twiss_y.nu = 0;
    %[stat, full_twiss] = GetTwiss(1284, 1933, iex_twiss_x, iex_twiss_y);
    [stat, full_twiss] = GetTwiss(1, 1933, iex_twiss_x, iex_twiss_y);

    %otr0_twiss_y = otr_data.data.betay;
    %compareModel('ATF2lat.mat','QUAD')
    %INSTR = raw_data.INSTR;
    %global BEAMLINE
    %global INSTR
    [nr_bpms_ring, bpms_index, raw_index] = get_BPM_index_FL();

    nr_total = length(bpms_index);
    s_bpm = zeros(1,nr_total);
    ref_noise = zeros(1,nr_total);
    for i=1:nr_total
        s_bpm(i) = BEAMLINE{bpms_index(i)}.S;
        ref_noise(i) = BEAMLINE{bpms_index(i)}.Resolution;
    end
    beta_x = full_twiss.betax(bpms_index);
    beta_y = full_twiss.betay(bpms_index);
    eta_x = full_twiss.etax(bpms_index);
    eta_y = full_twiss.etay(bpms_index);
    nu_x = full_twiss.nux(bpms_index);
    nu_y = full_twiss.nuy(bpms_index);

    beta_x_init = raw_data.FL_SimModel.Twiss.betax(bpms_index);
    beta_y_init = raw_data.FL_SimModel.Twiss.betay(bpms_index);
    eta_x_init = full_twiss.etax(bpms_index);
    eta_y_init = full_twiss.etay(bpms_index);
    nu_x_init = raw_data.FL_SimModel.Twiss.nux(bpms_index);
    nu_y_init = raw_data.FL_SimModel.Twiss.nuy(bpms_index);

    gamma = 1300/0.511;
    epsilon_y = 0.3e-7./gamma;
    epsilon_x = 50e-7./gamma;
    sigma_x = sqrt(epsilon_x.*beta_x);
    sigma_y = sqrt(epsilon_y.*beta_y);
    sigma_x_init = sqrt(epsilon_x.*beta_x_init);
    sigma_y_init = sqrt(epsilon_y.*beta_y_init);
    model_data = [beta_x', beta_y', eta_x', eta_y', nu_x', nu_y', sigma_x', ...
             sigma_y', s_bpm', ref_noise'];
  else
    model_data = load(file_name_model, '-ASCII');
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%
% Clean the data simple %
%%%%%%%%%%%%%%%%%%%%%%%%%

%x_data = clean_bpm_data(x_data);
%y_data = clean_bpm_data(y_data);

% Cut all data to the same size
[nr_trains_x, nr_bpms_x] = size(x_data);
[nr_trains_y, nr_bpms_y] = size(y_data);

nr_trains = min([nr_trains_x, nr_trains_y]);
x_data = x_data(1:nr_trains,:);
y_data = y_data(1:nr_trains,:);
x_time_stamp = x_time_stamp(1:nr_trains,:);
y_time_stamp = y_time_stamp(1:nr_trains,:);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Shift the ATF2 strip-line data compared to the cavity BPMs %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_shift_stripline_data == 1)
    % Shift the data
    x_data(:,stripline_index) = [x_data(2:end,stripline_index); zeros(1,nr_stripline_bpm)];
    y_data(:,stripline_index) = [y_data(2:end,stripline_index); zeros(1,nr_stripline_bpm)];
end

if(use_font_bpm == 1)
    % Scale data from um to m to fit with the other data
    x_data(:,font_bpm_index) = x_data(:,font_bpm_index)./1e6;
    y_data(:,font_bpm_index) = y_data(:,font_bpm_index)./1e6;
end

if(use_data_limits == 1)
    x_data = x_data(limit_start_index:limit_end_index,:);
    y_data = y_data(limit_start_index:limit_end_index,:);
    x_time_stamp = x_time_stamp(limit_start_index:limit_end_index,:);
    y_time_stamp = y_time_stamp(limit_start_index:limit_end_index,:);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the differential data %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_diff_data == 1) 
  %x_data = x_data(1:end-1,:) - x_data(2:end,:);
  %x_data = [x_data; zeros(1,nr_bpms_x)]; 
  %y_data = y_data(1:end-1,:) - y_data(2:end,:);
  %y_data = [y_data; zeros(1,nr_bpms_y)];
  x_data = x_data(2:end,:) - x_data(1:end-1,:);
  y_data = y_data(2:end,:) - y_data(1:end-1,:);
  x_time_stamp = x_time_stamp(2:end,:);
  y_time_stamp = y_time_stamp(2:end,:);
end

figure(100);
plot(y_data(:,cavity_bpm_index),'-b');
hold on;
grid on;
plot(y_data(:,stripline_bpm_index),'-g');
if(use_font_bpm)
  plot(y_data(:,font_bpm_index),'-y');
end
if(use_beam_on_off == 1)
  plot(beam_on_off*1e-3,'-r');
end
title('Data BPM y before cleaning');

%%%%%%%%%%%%%%%%%
% Spike removal %
%%%%%%%%%%%%%%%%%

if(use_spike_removal == 1)
  [x_data, removed_items_x] = remove_spikes(x_data, spike_removal_treshold_x);
  [y_data, removed_items_y] = remove_spikes(y_data, spike_removal_treshold_y);
fprintf(1, 'Data points set to zero: %i in x and %i in y\n', removed_items_x, removed_items_y);
elseif(use_spike_removal == 2)
    %y_data(:,118) = zeros(size(y_data(:,118)));
    %y_data(:,130) = zeros(size(y_data(:,130)));
    %max(y_data);
    %[value,index] = max(max(y_data))
    %figure(102);
    %plot(y_data(:,91:end));
    if(use_new_data_format == 1)
      used_bpms_spike_removal = 1:nr_bpms;
    else
      used_bpms_spike_removal = 91:nr_bpms;
    end
  [x_data, index_remove_x] = remove_spike_lines(x_data, ...
                        spike_removal_treshold_x, used_bpms_spike_removal);
  [y_data, index_remove_y] = remove_spike_lines(y_data, ...
                        spike_removal_treshold_y, used_bpms_spike_removal);
  x_time_stamp(index_remove_x,:) = [];
  y_time_stamp(index_remove_y,:) = [];
fprintf(1, 'Data lines removed: %i in x and %i in y\n', length(index_remove_x), length(index_remove_y));
end

figure(101);
plot(y_data(:,cavity_bpm_index),'-b');
hold on;
grid on;
plot(y_data(:,stripline_bpm_index),'-g');
if(use_font_bpm)
  plot(y_data(:,font_bpm_index),'-y');
end
title('Data BPM y after cleaning');

%if(use_beam_on_off == 1)
%  plot(beam_on_off*1e-3,'-r');
%end

%% Cut all data to the same size
[nr_trains_x, nr_bpms_x] = size(x_data);
[nr_trains_y, nr_bpms_y] = size(y_data);

nr_trains = min([nr_trains_x, nr_trains_y]);
x_data = x_data(1:nr_trains,:);
y_data = y_data(1:nr_trains,:);
x_time_stamp = x_time_stamp(1:nr_trains,:);
y_time_stamp = y_time_stamp(1:nr_trains,:);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Make a dispersion subtraction %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_disp_subtraction == 1)
  disp_mode = zeros(nr_trains_x1,1);
  for i=1:nr_trains
     disp_mode(i) = x_data(i,:)*eta_x';
     x_data(i,:) = x_data(i,:) - (x_data(i,:)*eta_x')*eta_x;
     y_data(i,:) = y_data(i,:) - (y_data(i,:)*eta_y')*eta_y;
  end
end

%%%%%%%%%%%%%%%%%%%%%%%
% Subtract mean value %
%%%%%%%%%%%%%%%%%%%%%%%

if(use_substract_mean == 1)
  mean_x = mean(x_data);
  mean_y = mean(y_data);
  for i=1:nr_trains
      x_data(i,:) = x_data(i,:) - mean_x;
      y_data(i,:) = y_data(i,:) - mean_y;
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Store the modified data %
%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_diff_data == 1)
    file_name_x_mod = ['./data/' file_name_base '_x_mod.dat'];
    file_name_y_mod = ['./data/' file_name_base '_y_mod.dat'];
    file_name_time_stamp_x_mod = ['./data/time_stamp_' file_name_base '_x_mod.dat'];
    file_name_time_stamp_y_mod = ['./data/time_stamp_' file_name_base '_y_mod.dat'];
    file_name_model_mod = ['./data/model_' file_name_base '_mod.dat'];
else
    file_name_x_mod = ['./data/' file_name_base '_full_x_mod.dat'];
    file_name_y_mod = ['./data/' file_name_base '_full_y_mod.dat'];
    file_name_time_stamp_x_mod = ['./data/time_stamp_' file_name_base '_full_x_mod.dat'];
    file_name_time_stamp_y_mod = ['./data/time_stamp_' file_name_base '_full_y_mod.dat'];
    file_name_model_mod = ['./data/model_' file_name_base '_full_mod.dat'];
end
    
save(file_name_x_mod, 'x_data', '-ASCII');
save(file_name_y_mod, 'y_data', '-ASCII');
save(file_name_time_stamp_x_mod, 'x_time_stamp', '-ASCII');
save(file_name_time_stamp_y_mod, 'y_time_stamp', '-ASCII');
save(file_name_model_mod, 'model_data', '-ASCII');
