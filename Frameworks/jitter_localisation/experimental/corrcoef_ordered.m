% Function to align two sets of data according
% to and index number.
%
% Juergen Pfingstner
% 17th of December 2012

function [corr_coef] = corrcoef_ordered(data1, data2, index1, index2)

  [data_a1, data_a2, index_a1, index_a2] = align_with_index(data1, data2, index1, index2);
  corr_temp = corrcoef(data_a1, data_a2);
  corr_coef = corr_temp(1,2);

end
