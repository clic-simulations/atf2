% Script to acquire the BPM readings of ATF2 and 
% the beam on/off signal.
%
% Juergen Pfingstner
% 6th March 2014

%%%%%%%%%%%%%%
% Parameters %
%%%%%%%%%%%%%%

%file_name_local = './data/jitter_study6_20140307_0440.mat';
file_name_local = './data/test1.mat';
%nr_trains_local = floor(2*60*3.12);
nr_trains_local = 20;
sampling_factor_local = 4;

use_font = 0;
font_bpm_index = [1;3;5];

if(exist('external_parameter')==0)
  external_parameter = 0;
end
if(external_parameter == 0)
  file_name = file_name_local;
  nr_trains = nr_trains_local;
  sampling_factor = sampling_factor_local;
end

%%%%%%%%%%%%%%%%%%
% Initialisation %
%%%%%%%%%%%%%%%%%%

global BEAMLINE
global INSTR

fprintf(1,'Update the model from the real machine\n');
FlHwUpdate('all');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read the measurement data %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_font == 1)	
  [x_data, y_data, charge_data, font_data, time_stamp_x, time_stamp_y, time_stamp_charge, time_stamp_font] = get_BPM_data_ATF2_FONT(nr_trains, sampling_factor);
    new_data_index = find_new_data_index(time_stamp_font);
    font_data = font_data(new_data_index,:);
    time_stamp_font = time_stamp_font(new_data_index,:);
else
  [x_data, y_data, charge_data, time_stamp_x, time_stamp_y, time_stamp_charge] = get_BPM_data_ATF2(nr_trains, sampling_factor);
end

new_data_index = find_new_data_index(time_stamp_x(:,20));
x_data = x_data(new_data_index,:);
time_stamp_x = time_stamp_x(new_data_index,:);

new_data_index = find_new_data_index(time_stamp_y(:,20));
y_data = y_data(new_data_index,:);
time_stamp_y = time_stamp_y(new_data_index,:);

new_data_index = find_new_data_index(time_stamp_charge(:,20));
charge_data = charge_data(new_data_index,:);
time_stamp_charge = time_stamp_charge(new_data_index,:);

%%%%%%%%%%%%%%%%%
% Save the data %
%%%%%%%%%%%%%%%%%

FL_HwInfo=FL.HwInfo;
FL_SimModel=FL.SimModel;
if(use_font == 1)
    save(file_name,...
	 'x_data', 'y_data', 'charge_data', 'font_data', 'time_stamp_x', 'time_stamp_y', 'time_stamp_charge', 'time_stamp_font', 'font_bpm_index',...
         'INSTR','BEAMLINE','PS','FL_HwInfo','FL_SimModel','GIRDER');
else
    save(file_name,...
	 'x_data', 'y_data', 'charge_data', 'time_stamp_x', 'time_stamp_y', 'time_stamp_charge', 'INSTR','BEAMLINE','PS','FL_HwInfo','FL_SimModel','GIRDER');
end
