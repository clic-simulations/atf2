% Script to get the normalised jitter at the last turn of the DR and the
% EXT line.
%
% Juergen Pfingstner
% 6th December 2012

%%%%%%%%%%%%%%
% Parameters %
%%%%%%%%%%%%%%

data_dir_local = 'sync_test2_20140228_0045';
%data_dir_local = 'font_test1';
nr_trains_local = 10*60*3;
%nr_trains_local = 10;
if(exist('external_parameter')==0)
  external_parameter = 0;
end
if(external_parameter == 0)
  file_name_base = data_dir_local;
  nr_trains = nr_trains_local;
end

use_epics_or_fs_data = 'epics_all'; % 'epics' or 'fs', 'epics_all'
epics_data_mode = 'orbit'; % 'first', 'last' or 'orbit'
use_FONT = 1;
merge_FONT_data = 1;
FONT_BPM_index=[1,3,5];

%%%%%%%%%%%%%%%%%%
% Initialisation %
%%%%%%%%%%%%%%%%%%

global BEAMLINE
global INSTR
clear x_data ydata raw s;

file_name_offset = ['./data/' file_name_base '.dat'];
file_name_time_stamp = ['./data/time_stamp_' file_name_base '.mat'];
file_name_model = ['./data/model_' file_name_base '.dat'];
file_name_model_FL = ['./data/model_FL_' file_name_base '.dat'];

fprintf(1,'Update the model from the real machine\n');
FlHwUpdate('all');

fprintf('Find the BPM indices\n');
[nr_bpms_ring, bpms_index, raw_index] = get_BPM_index_FL();

if(strcmp(use_epics_or_fs_data, 'epics'))
  bpms_index = bpms_index(1:nr_bpms_ring);
end

%%%%%%%%%%%%%%%%%%%%%%
% Get the model data %
%%%%%%%%%%%%%%%%%%%%%%

nr_total = length(bpms_index);
s_bpm = zeros(1,nr_total);
ref_noise = zeros(1,nr_total);
for i=1:nr_total
    s_bpm(i) = BEAMLINE{bpms_index(i)}.S;
    ref_noise(i) = BEAMLINE{bpms_index(i)}.Resolution;
end

fprintf('Get the beta and dispersion functions at the BPM positions\n');
beta_x = FL.SimModel.Twiss.betax(bpms_index);
beta_y = FL.SimModel.Twiss.betay(bpms_index);
eta_x = FL.SimModel.Twiss.etax(bpms_index);
eta_y = FL.SimModel.Twiss.etay(bpms_index);
nu_x = FL.SimModel.Twiss.nux(bpms_index);
nu_y = FL.SimModel.Twiss.nuy(bpms_index);
gamma = 1300/0.511;
epsilon_y = 0.3e-7./gamma;
epsilon_x = 50e-7./gamma;
sigma_x = sqrt(epsilon_x.*beta_x);
sigma_y = sqrt(epsilon_y.*beta_y);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read the measurement data %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(strcmp(use_epics_or_fs_data,'fs'))

    fprintf(1,'Update data structures\n');
    FlHwUpdate('wait',1);
    FlHwUpdate('readbuffer',1);
    FlHwUpdate('wait',nr_trains);
    [s,raw]=FlHwUpdate('readbuffer',nr_trains);
    x_data = raw(:,raw_index*3-1);
    y_data = raw(:,raw_index*3);

elseif(strcmp(use_epics_or_fs_data,'epics'))

    %[x_data, y_data] = get_BPM_data_EPICS(nr_trains, epics_data_mode);
    [x_data, y_data] = get_BPM_data_EPICS_V2(nr_trains, epics_data_mode);
    [x_data, y_data] = rearrange_EPICS_BPM_data(x_data, y_data);

elseif(strcmp(use_epics_or_fs_data,'epics_all'))
    if(use_FONT==1)
      [x_data, y_data, time_stamp_x, time_stamp_y, x_data_FONT, y_data_FONT, time_stamp_FONT_x, time_stamp_FONT_y, time_stamp_FONT_local_y, beam_on, time_stamp_beam_on] = get_BPM_data_all_FONT_EPICS(nr_trains, epics_data_mode);
    else
        [x_data, y_data, time_stamp_x, time_stamp_y, beam_on, time_stamp_beam_on] = get_BPM_data_all_EPICS(nr_trains, epics_data_mode);
    end
else
  fprintf(1, 'Unknown BPM read mode! No data acquired.\n');                                                                   
end

%%%%%%%%%%%%%%%%%%%%%%%%%%
% Merge in the FONT data %
%%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_FONT == 1)
    last_ring_bpm_index = 90;
    if(merge_FONT_data == 1)
        x_data(:,last_ring_bpm_index+FONT_BPM_index) = x_data_FONT;
        y_data(:,last_ring_bpm_index+FONT_BPM_index) = y_data_FONT;
        time_stamp_x(:,last_ring_bpm_index+FONT_BPM_index) = time_stamp_FONT_x;
        time_stamp_y(:,last_ring_bpm_index+FONT_BPM_index) = time_stamp_FONT_y;
    end
end

%%%%%%%%%%%%%%%%%
% Save the data %
%%%%%%%%%%%%%%%%%

fprintf('Store the data\n');
if(strcmp(use_epics_or_fs_data,'epics_all'))
  save_data = [time_stamp_x; time_stamp_y];
  save(file_name_time_stamp, 'save_data');
end
save_data = [x_data; y_data];
save(file_name_offset, 'save_data', '-ASCII');

save_data = [beta_x', beta_y', eta_x', eta_y', nu_x', nu_y', sigma_x', ...
             sigma_y', s_bpm', ref_noise'];
save(file_name_model, 'save_data', '-ASCII');

FL_HwInfo=FL.HwInfo;
FL_SimModel=FL.SimModel;
save(file_name_model_FL,'INSTR','BEAMLINE','PS','FL_HwInfo','FL_SimModel','GIRDER');

file_name_beam_on = ['./data/' file_name_base '_beam_on.dat'];
save(file_name_beam_on, 'beam_on','-ASCII');

file_name_beam_on_time_stamp = ['./data/time_stamp_beam_on_' file_name_base '.mat'];
save(file_name_beam_on_time_stamp, 'time_stamp_beam_on');

if(use_FONT == 1)
    % Save the FONT data also separate

    file_name_time_stamp_FONT = ['./data/time_stamp_FONT_' file_name_base '.mat'];
    save_data = [time_stamp_FONT_x; time_stamp_FONT_y];
    save(file_name_time_stamp_FONT, 'save_data');

    file_name_time_stamp_FONT_local = ['./data/time_stamp_FONT_local_' file_name_base '.mat'];
    save_data = [time_stamp_FONT_local_y];
    save(file_name_time_stamp_FONT_local, 'save_data');

    file_name_data_FONT = ['./data/' file_name_base '_FONT.dat'];
    save_data = [x_data_FONT; y_data_FONT];
    save(file_name_data_FONT, 'save_data', '-ASCII');
end
