function [time_conv]=convert_epics_time(time_epics)

% At the moment here only the difference time is formed.
% The different BPMs are assume to be in columns 
  
time_conv = zeros(size(time_epics));
t0_real = mean(real(time_epics(1,:)));
t0_imag = mean(imag(time_epics(1,:)));

t_real = real(time_epics)-t0_real;
t_imag = (imag(time_epics)-t0_imag)./1e9;
time_conv = t_real + t_imag;
