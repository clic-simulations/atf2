% Function to read BPM data through the flight simulator
%
% Juergen Pfingstner
% 5. Dec. 2012

function [x_data, y_data] = get_BPM_data_EPICS(nr_trains, epics_data_mode)

%global BEAMLINE
%global INSTR

%%%%%%%%%%%%%
% Init data %
%%%%%%%%%%%%%

lcaSetSeverityWarnLevel(14);    % labCA settings
lcaSetSeverityWarnLevel(4);
lcaSetSeverityWarnLevel(14);
lcaSetTimeout(0.2);
lcaSetRetryCount(20);
    
x_data = zeros(nr_trains,24*4);
y_data = zeros(nr_trains,24*4);

if(strcmp(epics_data_mode,'orbit'))
  field_length = 6;
  ending_string = '.VAL';
elseif(strcmp(epics_data_mode,'first'))
  field_length = 7;
  ending_string = '.FRST';
elseif(strcmp(epics_data_mode,'last'))
  field_length = 7;
  ending_string = '.LAST';
else
  fprintf(1, 'Unknown EPICS data mode, mode set to orbit');
  field_length = 6;
  ending_string = '.VAL';
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set the monitors for the PVs to observe %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%monitorPV={'bpm:orbit1';'bpm:orbit2';'bpm:orbit3';'bpm:orbit4'};
%try
%  lcaSetMonitor(monitorPV,1);
%catch
%  try
%      pause(1)
%      lcaSetMonitor(monitorPV,1);
%  catch
%      fprintf(1,'The EPICs monitors for the PVs of the BPM crates could not be set.\n');
%  end
%end

% Wait for the next pulse
%[stat,output]=FlHwUpdate('wait',1, 5);
%[stat,pulse_nr_init]=FlHwUpdate('getpulsenum');

%%%%%%%%%%%%%%%%%
% Read the data %
%%%%%%%%%%%%%%%%%

tic;
for i=1:nr_trains

  fprintf(1,'   Train nr %i\n', i);

  run_loop = 1; 
  cycle_time = 1./3.12;
  while(run_loop == 1)
    if(toc<(i*cycle_time))
      pause(0.001); % Wait 1 millisec.
    else
      run_loop = 0;
    end
  end

  %for j=1:4
  %  try
  %    lcaNewMonitorWait(monitorPV{j});
  %    fprintf(1, 'Wait okay\n');
  %  catch
  %    fprintf(1, 'Error when waiting for the monitor of the BPM crate PVs\n');
  %  end
  %end

  for j=1:4
    bpms_crate_name = sprintf('bpm:orbit%i%s', j, ending_string);
    [read_buffer] = lcaGet(bpms_crate_name);
    %[read_buffer time_stamp] = lcaGet(bpms_crate_name);
    %time_stamp
    x_data(i,(1+(j-1)*24):(j*24)) = read_buffer(3:field_length:end); 
    y_data(i,(1+(j-1)*24):(j*24)) = read_buffer(5:field_length:end); 
  end
end

end
