% Function to rearrange read in but misordered BPM data
%
% Juergen Pfingstner
% 17. Dec. 2012

function [data_mod_x, data_mod_y] = rearrange_EPICS_all_BPM_data(data_x, data_y)

   % Remove BPMs that are not working

   data_mod_x = [data_x(:,1:9)];
   data_mod_x = [data_mod_x, data_x(:,13:15)];
   data_mod_x = [data_mod_x, data_x(:,10:21)];
   data_mod_x = [data_mod_x, data_x(:,26:28)];
   data_mod_x = [data_mod_x, data_x(:,30:48)];
   data_mod_x = [data_mod_x, data_x(:,54:56)];

   data_mod_y = [data_ATF2_y(:,1:9)];
   data_mod_y = [data_mod_y, data_y(:,13:15)];
   data_mod_y = [data_mod_y, data_y(:,10:21)];
   data_mod_y = [data_mod_y, data_y(:,26:28)];
   data_mod_y = [data_mod_y, data_y(:,30:48)];
   data_mod_y = [data_mod_y, data_y(:,54:56)];

   keyboard;

end
