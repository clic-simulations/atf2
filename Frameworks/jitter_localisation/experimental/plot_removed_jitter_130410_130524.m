% Plot the removed jitter from the 10th of April and the 24th of
% May 2013 to see if they are consistant
%
% Juergen Pfingstner
% 4th of June 2013

data_130410_0040 = load('oscillation_130410_0040_long.dat');
data_130410_0050 = load('oscillation_130410_0050_long.dat');
data_130410_0051 = load('oscillation_130410_0051_long.dat');
data_130410_0056 = load('oscillation_130410_0056_long.dat');
data_130410_0105 = load('oscillation_130410_0105_long.dat');

data_130524_1500 = load('oscillation_130524_1500_long.dat');
data_130524_1510 = load('oscillation_130524_1510_long.dat');
data_130524_1515 = load('oscillation_130524_1515_long.dat');
data_130524_1525 = load('oscillation_130524_1525_long.dat');
data_130524_1530 = load('oscillation_130524_1530_long.dat');

data_130618_0100 = load('oscillation_130618_0100_long.dat');
data_130618_0250 = load('oscillation_130618_0250_long.dat');
data_130618_0453 = load('oscillation_130618_0453_long.dat');
data_130618_0712 = load('oscillation_130618_0712_long.dat');
data_130618_0740 = load('oscillation_130618_0740_long.dat');

t = data_130410_0040(:,1);
sigma_y = data_130410_0040(:,4);

source1_130410_0040 = data_130410_0040(:,2);
source2_130410_0040 = data_130410_0040(:,3);
source1_130410_0040_norm = source1_130410_0040./sigma_y;
source2_130410_0040_norm = source2_130410_0040./sigma_y;

source1_130410_0050 = data_130410_0050(:,2);
source2_130410_0050 = data_130410_0050(:,3);
source1_130410_0050_norm = source1_130410_0050./sigma_y;
source2_130410_0050_norm = source2_130410_0050./sigma_y;

source1_130410_0051 = data_130410_0051(:,2);
source2_130410_0051 = data_130410_0051(:,3);
source1_130410_0051_norm = source1_130410_0051./sigma_y;
source2_130410_0051_norm = source2_130410_0051./sigma_y;

source1_130410_0056 = data_130410_0056(:,2);
source2_130410_0056 = data_130410_0056(:,3);
source1_130410_0056_norm = source1_130410_0056./sigma_y;
source2_130410_0056_norm = source2_130410_0056./sigma_y;

source1_130410_0105 = data_130410_0105(:,2);
source2_130410_0105 = data_130410_0105(:,3);
source1_130410_0105_norm = source1_130410_0105./sigma_y;
source2_130410_0105_norm = source2_130410_0105./sigma_y;

source1_130524_1500 = data_130524_1500(:,2);
source2_130524_1500 = data_130524_1500(:,3);
source1_130524_1500_norm = source1_130524_1500./sigma_y;
source2_130524_1500_norm = source2_130524_1500./sigma_y;

source1_130524_1510 = data_130524_1510(:,2);
source2_130524_1510 = data_130524_1510(:,3);
source1_130524_1510_norm = source1_130524_1510./sigma_y;
source2_130524_1510_norm = source2_130524_1510./sigma_y;

source1_130524_1515 = data_130524_1515(:,2);
source2_130524_1515 = data_130524_1515(:,3);
source1_130524_1515_norm = source1_130524_1515./sigma_y;
source2_130524_1515_norm = source2_130524_1515./sigma_y;

source1_130524_1525 = data_130524_1525(:,2);
source2_130524_1525 = data_130524_1525(:,3);
source1_130524_1525_norm = source1_130524_1525./sigma_y;
source2_130524_1525_norm = source2_130524_1525./sigma_y;

source1_130524_1530 = data_130524_1530(:,2);
source2_130524_1530 = data_130524_1530(:,3);
source1_130524_1530_norm = source1_130524_1530./sigma_y;
source2_130524_1530_norm = source2_130524_1530./sigma_y;

source1_130618_0100 = data_130618_0100(:,2);
source2_130618_0100 = data_130618_0100(:,3);
source1_130618_0100_norm = source1_130618_0100./sigma_y;
source2_130618_0100_norm = source2_130618_0100./sigma_y;

source1_130618_0250 = data_130618_0250(:,2);
source2_130618_0250 = data_130618_0250(:,3);
source1_130618_0250_norm = source1_130618_0250./sigma_y;
source2_130618_0250_norm = source2_130618_0250./sigma_y;

source1_130618_0453 = data_130618_0453(:,2);
source2_130618_0453 = data_130618_0453(:,3);
source1_130618_0453_norm = source1_130618_0453./sigma_y;
source2_130618_0453_norm = source2_130618_0453./sigma_y;

source1_130618_0712 = data_130618_0712(:,2);
source2_130618_0712 = data_130618_0712(:,3);
source1_130618_0712_norm = source1_130618_0712./sigma_y;
source2_130618_0712_norm = source2_130618_0712./sigma_y;

source1_130618_0740 = data_130618_0740(:,2);
source2_130618_0740 = data_130618_0740(:,3);
source1_130618_0740_norm = source1_130618_0740./sigma_y;
source2_130618_0740_norm = source2_130618_0740./sigma_y;

figure(3);
plot(t, source2_130410_0040_norm, 'k');
grid on;
hold on;
plot(t, source2_130410_0050_norm, '--k');
plot(t, -source2_130410_0051_norm, 'b');
plot(t, source2_130410_0056_norm, '--b');
plot(t, source2_130410_0105_norm, 'c');

plot(t, -source2_130524_1500_norm, 'r');
plot(t, source2_130524_1510_norm, '--r');
plot(t, -source2_130524_1515_norm, 'm');
plot(t, source2_130524_1525_norm, '--m');
plot(t, source2_130524_1530_norm, '--g');

%plot(t, -source2_130618_0100_norm, 'r');
plot(t, -source2_130618_0250_norm, '--r');
plot(t, source2_130618_0453_norm, 'g');
plot(t, -source2_130618_0712_norm, 'm');
plot(t, source2_130618_0740_norm, '--m');

hold off; 

figure(4);
plot(t, source1_130410_0040_norm, 'k');
grid on;
hold on;
plot(t, source1_130410_0050_norm, '--k');
plot(t, source1_130410_0051_norm, 'b');
plot(t, source1_130410_0056_norm, '--b');
plot(t, -source1_130410_0105_norm, 'c');

plot(t, source1_130524_1500_norm, 'r');
plot(t, source1_130524_1510_norm, '--r');
plot(t, source1_130524_1515_norm, 'm');
plot(t, source1_130524_1525_norm, '--m');
plot(t, source1_130524_1530_norm, '--g');

%plot(t, -source1_130618_0100_norm, 'r');
plot(t, source1_130618_0250_norm, '--r');
plot(t, source1_130618_0453_norm, 'g');
plot(t, source1_130618_0712_norm, 'm');
plot(t, -source1_130618_0740_norm, '--m');
hold off; 

figure(5);
plot(t, source2_130618_0250_norm, '--r');
grid on;
hold on;
plot(t, -source2_130618_0453_norm, 'g');
plot(t, source2_130618_0712_norm, 'm');
plot(t, -source2_130618_0740_norm, '--m');
hold off; 

figure(6);
plot(t, source1_130618_0250_norm, '--r');
grid on;
hold on;
plot(t, source1_130618_0453_norm, 'g');
plot(t, source1_130618_0712_norm, 'm');
plot(t, -source1_130618_0740_norm, '--m');
hold off; 


