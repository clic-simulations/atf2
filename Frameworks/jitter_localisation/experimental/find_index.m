% Function to find the index of a certain 
% integer value on an array
%
% Juergen Pfingstner
% 17th of December 2012

function [index] = find_index(data, number)

   % To slow
   %N = lenth(data);
   %for i=1:N
   %  if(abs((data(i)-number)) < 0.01)
   %    index = i;
   %    return;
   %  end
   %end
   %
   %index = 0;
   %return;

   index = find(data == number,1);
   if(isempty(index))
     index = 0;
   %else
   %  index = index(1);
   end
end

