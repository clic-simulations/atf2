% Function to read BPM data through the flight simulator
%
% Juergen Pfingstner
% 5. Dec. 2012

function [data_mod_x, data_mod_y] = rearrange_EPICS_BPM_data(data_x, data_y)

   data_mod_x = data_x./1e6;
   data_mod_y = data_y./1e6;

  % Rearrange the BPMs according to the injection/extraction point

   data_mod_x = [data_mod_x(:,20:end), data_mod_x(:,1:19)];
   data_mod_y = [data_mod_y(:,20:end), data_mod_y(:,1:19)];

   % Remove BPMs that are not working

   %data_mod_x = [data_mod_x(:,3:27), data_mod_x(:,29:86), data_mod_x(:,88:end)];
   %data_mod_y = [data_mod_y(:,3:27), data_mod_y(:,29:86), data_mod_y(:,88:end)];

   % Remove another 2 BPMs that are not working
   %data_mod_x = [data_mod_x(:,1:48), data_mod_x(:,51:end)];
   %data_mod_y = [data_mod_y(:,1:48), data_mod_y(:,51:end)];

   data_mod_x = [data_mod_x(:,1), data_mod_x(:,4:27), data_mod_x(:,29:66), data_mod_x(:,70:end)];
   data_mod_y = [data_mod_y(:,1), data_mod_y(:,4:27), data_mod_y(:,29:66), data_mod_y(:,70:end)];
   
end
