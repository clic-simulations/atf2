% Function to implement the calculation of the 
% PSD via the averaging method of Bartlett.
%
% Juergen Pfingstner
% 10th of Dec. 2012

function [psd, f] = psd_bartlett(data, t_0, N)

  % Cut the data into N equal sized blocks in time,
  % which is assumed to be the rows

  [nr_trains_full, nr_meas] = size(data);
  mod_data = mod(nr_trains_full,N);
  block_length = (nr_trains_full - mod_data)/N;
  if(mod(block_length,2))
    block_length = block_length - 1;
  end
  data = data(1:block_length*N,:);
 
  % Calc for each block the PSD and add the data

  psd = zeros(block_length,nr_meas);
  for i=1:N
      start_index = 1+(i-1)*block_length;
      stop_index = i*block_length;
      psd = psd + abs(fft(data(start_index:stop_index,:))).^2;
  end

  % Scale the data to make it a PSD

  psd = t_0./((block_length*N)^2).*psd;

  % Combine the data to only get one side of the 
  % spectrum

  psd(2:block_length/2,:) = psd(2:block_length/2,:) + psd(end:-1:(block_length/2+2),:);
  psd(block_length/2+2:end, :) = [];

  % Create according frequency data

  temp_index = 0:block_length/2;
  f = temp_index.*N./t_0; 

  % Test
  %power1 = mean(std(data))
  %power2 = mean(sqrt(sum(psd)./(t_0./N)))

end
