% Script to call the jitter_analysis_detail several times, maybe in
% a loop. 
%
% Juergen Pfingstner
% 18th of June 2013

% Comments: The script jitter_analysis_detail.m has to be modified
% a bit:
%        1.) Comment the definition file_name_base, since it is
%        done here
%
%        2.) Only print 4 plots with the numbers 900, 1000, 1001,
%        1020 . Comment out the other ones.
%
%        3.) In these plots use the color specified on
%        color_string, which is defined here. 
%
%        4.) Use as the initial parameters:
%        use_x_or_y = 'y';
%
%        use_initialisation = 1;
%        use_data_inspection = 0;
%        use_noise_level_calc = 1;
%        use_svd_cleaning = 0;
%        use_deg_freedom_plot = 0;
%        use_correlation_detection = 1;
%
%        use_removed_motion_analysis_space = 0;
%        use_removed_motion_analysis_time = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test data set to check the scripts %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%file_name_base = 'epics_all_orbit_130514_1130';
%string_color = 'k';
%jitter_analysis_detail

%file_name_base = 'epics_all_orbit_130514_1135';
%string_color = 'b';
%jitter_analysis_detail

%file_name_base = 'epics_all_orbit_130514_1140';
%string_color = 'r';
%jitter_analysis_detail
%return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% December data set (low charge) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%file_name_base = 'epics_all_orbit_fb_121218_V1';
%string_color = 'g';
%jitter_analysis_detail
%return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% June experiment: exchange of power supplies,   %
% shift of Okugi-san, Yves helped, charge 4-5e9  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Old setup 1
%file_name_base = 'epics_all_orbit_130618_0100';
%string_color = 'b';
%jitter_analysis_detail

% Old setup 2
file_name_base = 'epics_all_orbit_130618_0250';
string_color = 'b';
jitter_analysis_detail

% New setup (exchanged power supplies Q20X and Q16FF)
file_name_base = 'epics_all_orbit_130618_0453';
string_color = 'k';
jitter_analysis_detail

% Old setup 3
file_name_base = 'epics_all_orbit_130618_0712';
string_color = 'r';
jitter_analysis_detail

% Old setup 4
%file_name_base = 'epics_all_orbit_130618_0740';
%string_color = 'r';
%jitter_analysis_detail
