
% Script to create out of the remaining jitter (after the main
% oscillation has been removed) a wave form that can be compared
% with simulations.
%
% Juergen Pfingstner
% 20. June 2013

%%%%%%%%%%%%%
% Parameter %
%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%
% Load the data %
%%%%%%%%%%%%%%%%%

%data0 = load('./data/epics_all_orbit_130618_0100_y_mod.dat');
%data1 = load('./paper/corr_remove_91_to_109_20130618_0100_y.dat');
%data2 = load('./paper/corr_remove_91_to_113_20130618_0100_y.dat');

%data0 = load('./data/epics_all_orbit_130618_0250_y_mod.dat');
%data1 = load('./paper/corr_remove_91_to_109_20130618_0250_y.dat');
%data2 = load('./paper/corr_remove_91_to_113_20130618_0250_y.dat');

%data0 = load('./data/epics_all_orbit_130618_0453_y_mod.dat');
%data1 = load('./paper/corr_remove_91_to_109_20130618_0453_y.dat');
%data2 = load('./paper/corr_remove_91_to_113_20130618_0453_y.dat');

%data0 = load('./data/epics_all_orbit_130618_0712_y_mod.dat');
%data1 = load('./paper/corr_remove_91_to_109_20130618_0712_y.dat');
%data2 = load('./paper/corr_remove_91_to_113_20130618_0712_y.dat');

data0 = load('./data/epics_all_orbit_130618_0740_y_mod.dat');
data1 = load('./paper/corr_remove_91_to_109_20130618_0740_y.dat');
data2 = load('./paper/corr_remove_91_to_113_20130618_0740_y.dat');

model_data = load('./data/model_epics_all_orbit_130618_0100_mod.dat', '-ASCII');
sigma_y = model_data(:,8)';

% Remove noisy BPMs
t = 91:133;
t(11+17) = [];
t(6+17) = [];

sigma_y = sigma_y(91:end-3);
sigma_y(11+17) = [];
sigma_y(6+17) = [];

data0 = data0(:,91:end-3);
data0(:,11+17) = [];
data0(:,6+17) = [];

data1 = data1(:,91:end-3);
data1(:,11+17) = [];
data1(:,6+17) = [];

data2 = data2(:,91:end-3);
data2(:,11+17) = [];
data2(:,6+17) = [];

data0 = data0 - data1;
data1 = data1 - data2;

%%%%%%%%%%%%%%%%
% Process data %
%%%%%%%%%%%%%%%%

[U0, S0, V0] = svd(data0);
[U1, S1, V1] = svd(data1);
[U2, S2, V2] = svd(data2);

std_data0 = std(data0);
std_data1 = std(data1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Store and plot the data %
%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(1);
plot(t, data0');
grid on;

figure(2);
plot(t, data1');
grid on;

figure(3);
plot(t, data2');
grid on;

figure(4);
plot(t, V1(:,1).*S1(1,1),'-b')
grid on;
hold on;
plot(t, V0(:,1).*S0(1,1),'-r');

figure(5);
plot(t, diag(S0), 'x-k');
grid on;
hold on;
plot(t, diag(S1), 'x-b');
plot(t, diag(S2), 'x-r');

factor0 = mean(std_data0)/mean(abs(V0(:,1)));
factor1 = mean(std_data1)/mean(abs(V1(:,1)));
figure(6);
plot(t, abs(V1(:,1)*factor1), 'b');
grid on;
hold on;
plot(t, abs(V0(:,1)*factor0), 'g');
plot(t, std_data0, 'm');
plot(t, std_data1, 'r');

fft_U0 = fft(U0(:,1));
fft_U1 = fft(U1(:,1));

figure(7);
plot(abs(fft_U1), '-b');
grid on;
hold on;
plot(abs(fft_U0), '-r');

store_data = [ t', V0(:,1).*factor0, V1(:,1).*factor1, sigma_y'];

%save('oscillation_130618_0100_long.dat', 'store_data', '-ascii');
%save('oscillation_130618_0250_long.dat', 'store_data', '-ascii');
%save('oscillation_130618_0453_long.dat', 'store_data', '-ascii');
%save('oscillation_130618_0712_long.dat', 'store_data', '-ascii');
save('oscillation_130618_0740_long.dat', 'store_data', '-ascii');

store_data = [abs(fft_U0), abs(fft_U1)];
save('./data/fft_extracted_130618_0740.dat', 'store_data', '-ascii');