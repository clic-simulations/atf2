% Function to read BPM data through the flight simulator
%
% Juergen Pfingstner
% 5. Dec. 2012

function [x_data, y_data, time_stamp_x, time_stamp_y, beam_on, time_stamp_beam_on] = get_BPM_data_all_EPICS(nr_trains, epics_data_mode)

%global BEAMLINE
%global INSTR

%%%%%%%%%%%%%
% Init data %
%%%%%%%%%%%%%

lcaSetSeverityWarnLevel(14);    % labCA settings
lcaSetSeverityWarnLevel(4);
lcaSetSeverityWarnLevel(14);
lcaSetTimeout(0.2);
lcaSetRetryCount(20);
    
x_data_atf = zeros(nr_trains,96);
y_data_atf = zeros(nr_trains,96);
time_stamp_x_atf = zeros(nr_trains,96);
time_stamp_y_atf = zeros(nr_trains,96);

%atf2_bpm_name_list = get_atf2_bpm_name_list;
nr_atf2_bpms = 57;
x_data_atf2 = zeros(nr_trains,nr_atf2_bpms);
y_data_atf2 = zeros(nr_trains,nr_atf2_bpms);
time_stamp_x_atf2 = zeros(nr_trains,nr_atf2_bpms);
time_stamp_y_atf2 = zeros(nr_trains,nr_atf2_bpms);

if(strcmp(epics_data_mode,'orbit'))
  proc_val = 6;
elseif(strcmp(epics_data_mode,'first'))
  proc_val = 9;
elseif(strcmp(epics_data_mode,'last'))
  proc_val = 8;
else
  fprintf(1, 'Unknown EPICS data mode, mode set to orbit');
  proc_val = 7;
end

% Debug
%[x_data_temp time_stamp_x_temp] = lcaGet('atf2:name', 1024);
%keyboard;
% dEBUG

beam_on = zeros(nr_trains, 1);
time_stamp_beam_on = zeros(nr_trains, 1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set the crates to the proper configuration %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

bpm_crate_name = {'bpm:crate1.', 'bpm:crate2.', 'bpm:crate3.', 'bpm:crate4.'};

for j = 1:4
  lcaPut([bpm_crate_name{j} 'TURN'], 1);
end

%%%%%%%%%%%%%%%%%
% Read the data %
%%%%%%%%%%%%%%%%%

% Get initial timing
bpm_name = 'bpm01.';
[data_temp time_ref_x_ATF] = lcaGet([bpm_name 'POSH'], 112);
[data_temp time_ref_y_ATF] = lcaGet([bpm_name 'POSV'], 112);
[data_temp time_ref_x_ATF2] = lcaGet('atf2:xpos', 1024);
[data_temp time_ref_y_ATF2] = lcaGet('atf2:ypos', 1024);
tic;
for i=1:nr_trains
  %keyboard;
  fprintf(1,'   Train nr %i\n', i);

  run_loop = 1; 
  cycle_time = 1./3.12;
  while(run_loop == 1)
    if(toc<(i*cycle_time))
      pause(0.001); % Wait 1 millisec.
    else
      run_loop = 0;
    end
  end

  %%%%%%%%%%%%%%%%%%%%
  % Get ATF BPM data %
  %%%%%%%%%%%%%%%%%%%%

  for j=1:4
    lcaPut([bpm_crate_name{j} 'SNAP'], 1);
  end
  for j=1:96
    if(j<10)
      bpm_name = sprintf('bpm0%i.', j);
    else
      bpm_name = sprintf('bpm%i.', j);
    end

    lcaPut([bpm_name 'PROC'], proc_val);
    if(strcmp(epics_data_mode, 'orbit'))
      [read_buffer_n time_stamp_n] = lcaGet([bpm_name 'INTN'], 112);
      [read_buffer_h time_stamp_h] = lcaGet([bpm_name 'POSH'], 112);
      [read_buffer_v time_stamp_v] = lcaGet([bpm_name 'POSV'], 112);
      %keyboard;
      x_data_atf(i,j) = mean(read_buffer_h);
      y_data_atf(i,j) = mean(read_buffer_v);
      %x_data_atf(i,j) = read_buffer_h(1);
      %y_data_atf(i,j) = read_buffer_v(1);
      %time_stamp_x_atf(i,j) = imag(time_stamp_h)/1e9 - time_0;
      %time_stamp_y_atf(i,j) = imag(time_stamp_v)/1e9 - time_0;
    else
      [read_buffer_n time_stamp_n] = lcaGet([bpm_name 'WINT'], 3);
      [read_buffer_h time_stamp_h] = lcaGet([bpm_name 'WPSH'], 3);
      [read_buffer_v time_stamp_v] = lcaGet([bpm_name 'WPSV'], 3);
      if(strcmp(epics_data_mode, 'first'))
        x_data(i,j) = read_buffer_h(2);
        y_data(i,j) = read_buffer_v(2);
      elseif(strcmp(epics_data_mode, 'last'))
        x_data(i,j) = read_buffer_h(2);
        y_data(i,j) = read_buffer_v(2);
      else
        x_data(i,j) = 0;
        y_data(i,j) = 0;
      end
    end
    %time_stamp_x_atf(i,j) = get_EPICS_time_diff(time_stamp_h,time_ref_x_ATF);
    %time_stamp_y_atf(i,j) = get_EPICS_time_diff(time_stamp_v,time_ref_y_ATF);
    time_stamp_x_atf(i,j) = time_stamp_h;
    time_stamp_y_atf(i,j) = time_stamp_v;
  end

  %%%%%%%%%%%%%%%%%%%%%
  % Get ATF2 BPM data %
  %%%%%%%%%%%%%%%%%%%%%

  [x_data_temp time_stamp_x_temp] = lcaGet('atf2:xpos', 1024);
  [y_data_temp time_stamp_y_temp] = lcaGet('atf2:ypos', 1024);
  x_data_atf2(i,:) = x_data_temp;
  y_data_atf2(i,:) = y_data_temp;
  %time_stamp_x_atf2(i,:) = get_EPICS_time_diff(time_stamp_x_temp, time_ref_x_ATF2).*ones(size(time_stamp_x_atf2(i,:)));
  %time_stamp_y_atf2(i,:) = get_EPICS_time_diff(time_stamp_y_temp, time_ref_y_ATF2).*ones(size(time_stamp_y_atf2(i,:)));
  time_stamp_x_atf2(i,:) = time_stamp_x_temp;
  time_stamp_y_atf2(i,:) = time_stamp_y_temp;
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%
  % Get Beam on/off signal %
  %%%%%%%%%%%%%%%%%%%%%%%%%%
  
  [beam_on_temp time_stamp_temp] = lcaGet('atf:beam',1024);
  if(strcmp(beam_on_temp,'ON'))
      beam_on(i) = 1;
  else
      beam_on(i) = 0;
  end
  %time_stamp_beam_on(i) = real(time_stamp_temp)+imag(time_stamp_temp);
  time_stamp_beam_on(i) = time_stamp_temp;

end

[x_data_atf, y_data_atf] = rearrange_EPICS_BPM_data(x_data_atf, y_data_atf);
[time_stamp_x_atf, time_stamp_y_atf] = rearrange_EPICS_BPM_data(time_stamp_x_atf, time_stamp_y_atf);
[x_data_atf2, y_data_atf2] = rearrange_EPICS_ATF2_BPM_data(x_data_atf2, y_data_atf2);
[time_stamp_x_atf2, time_stamp_y_atf2] = rearrange_EPICS_ATF2_BPM_data(time_stamp_x_atf2, time_stamp_y_atf2);
x_data = [x_data_atf, x_data_atf2]./1e6;
y_data = [y_data_atf, y_data_atf2]./1e6;
time_stamp_x = epicsts2mat([time_stamp_x_atf, time_stamp_x_atf2]);
time_stamp_y = epicsts2mat([time_stamp_y_atf, time_stamp_y_atf2]);
time_stamp_beam_on = epicsts2mat(time_stamp_beam_on);

end
