% Function to read BPM data through the flight simulator
%
% Juergen Pfingstner
% 5. Dec. 2012

function [x_data, y_data] = get_BPM_data_EPICS(nr_trains, epics_data_mode)

%global BEAMLINE
%global INSTR

%%%%%%%%%%%%%
% Init data %
%%%%%%%%%%%%%

lcaSetSeverityWarnLevel(14);    % labCA settings
lcaSetSeverityWarnLevel(4);
lcaSetSeverityWarnLevel(14);
lcaSetTimeout(0.2);
lcaSetRetryCount(20);
    
x_data = zeros(nr_trains,96);
y_data = zeros(nr_trains,96);

if(strcmp(epics_data_mode,'orbit'))
  proc_val = 6;
elseif(strcmp(epics_data_mode,'first'))
  proc_val = 9;
elseif(strcmp(epics_data_mode,'last'))
  proc_val = 8;
else
  fprintf(1, 'Unknown EPICS data mode, mode set to orbit');
  proc_val = 7;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set the crates to the proper configuration %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

bpm_crate_name = {'bpm:crate1.', 'bpm:crate2.', 'bpm:crate3.', 'bpm:crate4.'};

for j = 1:4
  lcaPut([bpm_crate_name{j} 'TURN'], 1);
end

%%%%%%%%%%%%%%%%%
% Read the data %
%%%%%%%%%%%%%%%%%

tic;
for i=1:nr_trains
  %keyboard;
  fprintf(1,'   Train nr %i\n', i);

  run_loop = 1; 
  cycle_time = 1./3.12;
  while(run_loop == 1)
    if(toc<(i*cycle_time))
      pause(0.001); % Wait 1 millisec.
    else
      run_loop = 0;
    end
  end

  for j=1:4
    lcaPut([bpm_crate_name{j} 'SNAP'], 1);
  end
  for j=1:96
    if(j<10)
      bpm_name = sprintf('bpm0%i.', j);
    else
      bpm_name = sprintf('bpm%i.', j);
    end

    lcaPut([bpm_name 'PROC'], proc_val);
    if(strcmp(epics_data_mode, 'orbit'))
      [read_buffer_n time_stamp] = lcaGet([bpm_name 'INTN'], 112);
      [read_buffer_h time_stamp] = lcaGet([bpm_name 'POSH'], 112);
      [read_buffer_v time_stamp] = lcaGet([bpm_name 'POSV'], 112);
      x_data(i,j) = mean(read_buffer_h);
      y_data(i,j) = mean(read_buffer_v);
    else
      [read_buffer_n time_stamp] = lcaGet([bpm_name 'WINT'], 3);
      [read_buffer_h time_stamp] = lcaGet([bpm_name 'WPSH'], 3);
      [read_buffer_v time_stamp] = lcaGet([bpm_name 'WPSV'], 3);
      if(strcmp(epics_data_mode, 'first'))
        x_data(i,j) = read_buffer_h(2);
        y_data(i,j) = read_buffer_v(2);
      elseif(strcmp(epics_data_mode, 'last'))
        x_data(i,j) = read_buffer_h(2);
        y_data(i,j) = read_buffer_v(2);
      else
        x_data(i,j) = 0;
        y_data(i,j) = 0;
      end
    end
  end
end

end
