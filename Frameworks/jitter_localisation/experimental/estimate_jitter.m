% File to estimate the jitter due to different magnet location
%
% Juergen Pfingstner
% 23th of May 2013

% Define the magnets

jitter_magnet2 = 5;


Q1X_index = 1323; % Magnet has two elements in the flight simulator
Q2X_index = 1340;
Q3X_index = 1345;
Q4X_index = 1356;
Q5X_index = 1365;
Q6X_index = 1382;
Q7X_index = 1393;
Q8X_index = 1400;
Q9X_index = 1417;

B_values1 = [ 2.2769*2 -1.9930*2 1.4194*2 1.4365*2 -1.9504*2 2.3918*2 0.8107*2  -1.2488*2 1.6148*2];
beta_y_values1 = [10.2789 140.1013 37.7977 32.3614 138.7225 15.9291 17.9458 65.2490 4.3685];
magnet_scaling_factor1 = [1 1 1 1 1 1 1 1 1];

Q20X_index = 1562;
Q21X_index = 1572;

B_values2 = [-0.6397*2 0.9412*2];
beta_y_values2 = [12.73  3.38];
magnet_scaling_factor2 = [1 1];

magnet1_id = [Q1X_index, Q2X_index, Q3X_index, Q4X_index, Q5X_index, Q6X_index, Q7X_index, Q8X_index, Q9X_index];
magnet2_id = [Q20X_index Q21X_index];

%use_indices1 = 1:9;
%use_indices2 = 1:2;
use_indices1 = [1];
use_indices2 = [2];

% Collect the necessary values and calculate the jitter estimate
% (only make sense proportionally)

jitter_estimate1 = 0;
jitter_estimate2 = 0;
for i=1:length(use_indices1)
    index_temp = use_indices1(i);
    jitter_estimate1 = jitter_estimate1 + (sqrt(beta_y_values1(index_temp))*abs(B_values1(index_temp))*magnet_scaling_factor1(index_temp))^2;
end

for i=1:length(use_indices2)
    index_temp = use_indices2(i);
    jitter_estimate2 = jitter_estimate2 + (sqrt(beta_y_values2(index_temp))*abs(B_values2(index_temp))*magnet_scaling_factor2(index_temp))^2;
end
    
% Calcate the jitter factor

jitter_estimate1 = sqrt(jitter_estimate1);
jitter_estimate2 = sqrt(jitter_estimate2);
jitter_factor = jitter_estimate1./jitter_estimate2
jitter_magnet1 = jitter_magnet2*jitter_factor

% Debug

gamma = 1300/0.511;
epsilon_y = 0.3e-7./gamma;
sigma_y1 = sqrt(epsilon_y.*beta_y_values1);
sigma_y2 = sqrt(epsilon_y.*beta_y_values2);

%figure(4);
%plot(sigma_y1, 'x-r');
%grid on;
%hold on;
%plot(sigma_y2, 'x-b');