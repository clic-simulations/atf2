% Function to implement the SVD filtering on data
%
% Juergen Pfingstner
% 6th of December

function [data_filt] = svd_filter(data, nr_sv, file_name)
  data = data'; 
  [nr_row, nr_col] = size(data);

  [U, S, V] = svd(data);
  s = diag(S);
  s_f = s;
  if(nr_sv < length(s))
    s_f = s(1:nr_sv);
  end
  S_f = zeros(nr_row, nr_col);
  for i=1:length(s_f)
    S_f(i,i) = s_f(i);
  end
  data_filt = U*S_f*V';
  data_filt = data_filt';

  if(strcmp(file_name, 'no'))
    % Do not save
  else
    % Save the directions and singular values
    svd_file_name = sprintf('./data/U_%s', file_name);
    save(svd_file_name, 'U', '-ascii');
    svd_file_name = sprintf('./data/s_%s', file_name);
    save(svd_file_name, 's', '-ascii');
  end
end
