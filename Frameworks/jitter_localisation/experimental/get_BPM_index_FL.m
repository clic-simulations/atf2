% Function to read BPM data through the flight simulator
%
% Juergen Pfingstner
% 5. Dec. 2012

function [nr_bpms_ring, bpms_index, raw_index] = get_BPM_index_FL()

global BEAMLINE
global INSTR

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Finde the correct indices %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mring= findcells(BEAMLINE,'Name','MB*R'); % BPMs in the ring
%mring = [mring(3:27), mring(29:end)];
%keyboard;
%mring = [mring(3:27), mring(29:86), mring(88:end)]; % Remove six not working BPMs: MB20R, MB21R, MB47R and MB10R
%mring = [mring(1:48), mring(51:end)];
mring = [mring(1), mring(4:27), mring(29:66), mring(70:end)];
% With this modification also the order of the BPMs in the ring fits to the injection/extraction point
%mbendEX=findcells(BEAMLINE,'Name','MB*X'); % button bpms in EX
%mbendEX = mbendEX(3:end); % Remove the not working BPMs: MB1X and MB2X
mquadEX=findcells(BEAMLINE,'Name','MQ*X'); % quad bpms in EX
mquadFF=findcells(BEAMLINE,'Name','MQ*FF'); % quad bpms in FF
mquadFF = mquadFF(1:end-2); % Remove the not working BPMs: MQF1FF and MQD0FF
msextFF=findcells(BEAMLINE,'Name','MS*FF'); % sext bpms in FF
msextFF = [msextFF(2:4), msextFF(6:end)]; % Remove two profile monitors from the indices
%mIP1=findcells(BEAMLINE,'Name','MIP*'); % IP BPM
%mIP2=findcells(BEAMLINE,'Name','M*IP'); % IP BPM
%bpms_index=sort([mring mbendEX mquadEX mquadFF msextFF]);
%bpms_index=sort([mring mquadEX mquadFF msextFF 1925 1927 1929]);
bpms_index=sort([mring mquadEX mquadFF msextFF 1925]);
nr_bpms_ring = length(mring);
nr_total = length(bpms_index);
nr_bpms_bl = nr_total - nr_bpms_ring;

%s_bpm = zeros(1,nr_total);
raw_index = zeros(1,nr_total);
%ref_noise = zeros(1,nr_total);
for i=1:nr_total
    %s_bpm(i) = BEAMLINE{bpms_index(i)}.S;
    %ref_noise(i) = BEAMLINE{bpms_index(i)}.Resolution;
    raw_index(i) = findcells(INSTR, 'Index', bpms_index(i));
end

end
