% Script to analyse and plot the orbits of different measurments
% (mainly changing wakefield conditions (ref cavity scans) and
% charge  scans)
%
% Juergen Pfingstner
% 18th of May 2013

%%%%%%%%%%%%%%
% Parameters %
%%%%%%%%%%%%%%

%use_x_or_y = 'y';
%use_normalised_data = 0;

%%%%%%%%%%%%%%%%%
% Load the data %
%%%%%%%%%%%%%%%%%

file_name_base = 'epics_all_orbit_130410_0040_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y3 = load(file_name_y, '-ASCII');
    
file_name_base = 'epics_all_orbit_130410_0050_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y4 = load(file_name_y, '-ASCII');

file_name_base = 'epics_all_orbit_130410_0051_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y5 = load(file_name_y, '-ASCII');

file_name_base = 'epics_all_orbit_130410_0056_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y6 = load(file_name_y, '-ASCII');

file_name_base = 'epics_all_orbit_130410_0105_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y6_5 = load(file_name_y, '-ASCII');

file_name_model= sprintf('./data/model_%s_mod.dat', file_name_base);
model_data = load(file_name_model, '-ASCII');
beta_x = model_data(:,1)';
beta_y = model_data(:,2)';
eta_x = model_data(:,3)';
eta_y = model_data(:,4)';
nu_x = model_data(:,5)';
nu_y = model_data(:,6)';
sigma_x = model_data(:,7)';
sigma_y = model_data(:,8)';
s_bpm = model_data(:,9)';
ref_noise = model_data(:,10)';
   
y3 = y3(:,91:end);
y4 = y4(:,91:end);
y5 = y5(:,91:end);
y6 = y6(:,91:end);
y6_5 = y6_5(:,91:end);

sigma_x = sigma_x(91:end)';
sigma_y = sigma_y(91:end)';
ref_noise = ref_noise(:,91:end)';
nu_y = nu_y(:,91:end)';
index_bpm = 91:136;
scale_bpm_min = 90;
scale_bpm_max = 137;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cut away obviously noisy data %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

y3(end,:) = [];
y4(end,:) = [];
y4(454:456,:) = [];
y5(end,:) = [];
y6(end,:) = [];
y6(46,:) = [];
y6(98,:) = [];
y6(28,:) = [];
y6_5(end,:) = [];
y6_5(389,:) = [];

%%%%%%%%%%%%%%%%%%%%
% Analyse the data %
%%%%%%%%%%%%%%%%%%%%

avg_y3 = mean(y3);
avg_y4 = mean(y4);
avg_y5 = mean(y5);
avg_y6 = mean(y6);
avg_y6_5 = mean(y6_5);

%avg_y3 = avg_y3*1e6;
%avg_y4 = avg_y4*1e6;
%avg_y5 = avg_y5*1e6;
%avg_y6 = avg_y6*1e6;
%avg_y6_5 = avg_y6_5*1e6;

figure(198);
plot(index_bpm, avg_y3.*10^6, 'x-c');
hold on;
grid on;
plot(index_bpm, avg_y3.*10^6,'x-b');
plot(index_bpm, avg_y5.*10^6,'x-m');
plot(index_bpm, avg_y6.*10^6,'x-g');
plot(index_bpm, avg_y6_5.*10^6,'x-k');

avg_y3 = avg_y3./sigma_y';
avg_y4 = avg_y4./sigma_y';
avg_y5 = avg_y5./sigma_y';
avg_y6 = avg_y6./sigma_y';
avg_y6_5 = avg_y6_5./sigma_y';

%%%%%%%%%%%%%%%%%%%%
% Plot the results %
%%%%%%%%%%%%%%%%%%%%

figure(310);
set(gca,'FontSize',18);
plot(index_bpm, (avg_y4-avg_y3)*100, 'x-c', 'Linewidth', 1.5);
hold on;
grid on;
plot(index_bpm, (avg_y5-avg_y3)*100,'x-b', 'Linewidth', 1.5);
plot(index_bpm, (avg_y6-avg_y3)*100,'x-m', 'Linewidth', 1.5);
plot(index_bpm, (avg_y6_5-avg_y3)*100,'x-r', 'Linewidth', 1.5);
ylabel('beam orbit change [\% of $$\sigma_{y0}$$]');
xlabel('BPM nr. [1]');
axis([90 137 -420 120]);
legend('$$y(4\times 10^9)$$ - $$y(3\times 10^9)$$','$$y(5\times 10^9)$$ - $$y(3\times 10^9)$$','$$y(6\times 10^9)$$ - $$y(3\times 10^9)$$','$$y(6.5\times 10^9)$$ - $$y(3\times 10^9)$$');

figure(311);
set(gca,'FontSize',18);
plot(index_bpm, sigma_y*10^6, 'x-b', 'Linewidth', 1.5);
hold on;
grid on;
ylabel('beam size $$\sigma_{y0}$$ [$$\mu$$m]');
xlabel('BPM nr. [1]');
axis([90 137 0 320]);

figure(312);
set(gca,'FontSize',18);
plot(index_bpm, nu_y, 'x-b', 'Linewidth', 1.5);
hold on;
grid on;
ylabel('phase advance $$\phi_{y}$$ [turns]');
xlabel('BPM nr. [1]');
axis([90 137 8.8 12.8]);