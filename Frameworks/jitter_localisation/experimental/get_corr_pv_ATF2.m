function [pv_x, pv_y, index_x, index_y] = get_corr_pv_ATF2(direction)
% Direction can be read or write to determine the mode of operation


global BEAMLINE

if(direction == 'read')
    suffix = ':currentRead';
elseif(direction == 'write')
    suffix = ':currentWrite';
else
    fprintf(1, 'Not valid direction %s. Please specify!\n', direction);
    pv_x = 0;
    pv_y = 0;
    index_x = 0;
    index_y = 0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the magnet indices %
%%%%%%%%%%%%%%%%%%%%%%%%%%

ZH_index=findcells(BEAMLINE,'Name','ZH*X');
index_x=[ZH_index(3:end)];
nr_x = length(index_x);

ZV_index=findcells(BEAMLINE,'Name','ZV*X');
index_y=[ZV_index(2:end)];
nr_y = length(index_y);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the PVs to the magnets %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

pv_x = cell(nr_x,1);
for i=1:nr_x
    pv_x{i}=[BEAMLINE{index_x(i)}.Name, suffix];
end

pv_y = cell(nr_y,1);
for i=1:nr_y
    pv_y{i}=[BEAMLINE{index_y(i)}.Name, suffix];
end
