% Function to read the corrector settings from EPICS
%
% Juergen Pfingstner
% 27th of Februar 2014

function [value] = get_pv(pv)

   % Get the values of the PVs
   value=lcaGet(pv);

end

