% Get the time diff from the EPICS time stamp
%
% Juergen Pfingstner
% 17th of December 2012

function [time_diff] = get_EPICS_time_diff(time_1, time_ref);

  a = real(time_1) - real(time_ref); 
  b = imag(time_1)/1e9 - imag(time_ref)/1e9;

  time_diff = a+b;

end
