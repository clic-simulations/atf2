% Script to analyse and plot the orbits of different measurments
% (mainly changing wakefield conditions (ref cavity scans) and
% charge  scans)
%
% Juergen Pfingstner
% 18th of May 2013

%%%%%%%%%%%%%%
% Parameters %
%%%%%%%%%%%%%%

%use_x_or_y = 'y';
%use_normalised_data = 0;

%%%%%%%%%%%%%%%%%
% Load the data %
%%%%%%%%%%%%%%%%%

file_name_base = 'epics_all_orbit_130618_0100_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y_b1 = load(file_name_y, '-ASCII');
    
file_name_base = 'epics_all_orbit_130618_0250_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y_b2 = load(file_name_y, '-ASCII');

file_name_base = 'epics_all_orbit_130618_0453_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y_ex = load(file_name_y, '-ASCII');

file_name_base = 'epics_all_orbit_130618_0712_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y_a1 = load(file_name_y, '-ASCII');

file_name_base = 'epics_all_orbit_130618_0740_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y_a2 = load(file_name_y, '-ASCII');

file_name_model= sprintf('./data/model_%s_mod.dat', file_name_base);
model_data = load(file_name_model, '-ASCII');
beta_x = model_data(:,1)';
beta_y = model_data(:,2)';
eta_x = model_data(:,3)';
eta_y = model_data(:,4)';
nu_x = model_data(:,5)';
nu_y = model_data(:,6)';
sigma_x = model_data(:,7)';
sigma_y = model_data(:,8)';
s_bpm = model_data(:,9)';
ref_noise = model_data(:,10)';
   
y_b1 = y_b1(:,91:end);
y_b2 = y_b2(:,91:end);
y_ex = y_ex(:,91:end);
y_a1 = y_a1(:,91:end);
y_a2 = y_a2(:,91:end);

sigma_x = sigma_x(91:end)';
sigma_y = sigma_y(91:end)';
ref_noise = ref_noise(:,91:end)';
nu_y = nu_y(:,91:end)';
index_bpm = 91:136;
scale_bpm_min = 90;
scale_bpm_max = 137;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cut away obviously noisy data %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

y_b1(end,:) = [];
y_b2(end,:) = [];
y_ex(end,:) = [];
y_a1(end,:) = [];
y_a2(end,:) = [];

y_b1(935,:) = [];
y_a2(706:end,:) = [];

%figure(3);
%plot(y_a2);
%return;

%%%%%%%%%%%%%%%%%%%%
% Analyse the data %
%%%%%%%%%%%%%%%%%%%%

avg_y_b1 = mean(y_b1);
avg_y_b2 = mean(y_b2);
avg_y_ex = mean(y_ex);
avg_y_a1 = mean(y_a1);
avg_y_a2 = mean(y_a2);

figure(298);
plot(index_bpm, avg_y_b1.*10^6, 'x-c');
hold on;
grid on;
plot(index_bpm, avg_y_b2.*10^6,'x-b');
plot(index_bpm, avg_y_ex.*10^6,'x-k');
plot(index_bpm, avg_y_a1.*10^6,'x-r');
plot(index_bpm, avg_y_a2.*10^6,'x-m');

%avg_y7 = avg_y7*1e6;
%avg_y53 = avg_y53*1e6;
%avg_y35 = avg_y35*1e6;
%avg_y21 = avg_y21*1e6;
%avg_y12 = avg_y12*1e6;

avg_y_b1 = avg_y_b1./sigma_y';
avg_y_b2 = avg_y_b2./sigma_y';
avg_y_ex = avg_y_ex./sigma_y';
avg_y_a1 = avg_y_a1./sigma_y';
avg_y_a2 = avg_y_a2./sigma_y';

%%%%%%%%%%%%%%%%%%%%
% Plot the results %
%%%%%%%%%%%%%%%%%%%%

%figure(299);
%set(gca,'FontSize',18);
%plot(index_bpm, (avg_y53-avg_y7)*100, 'x-c', 'Linewidth', 1.5);
%hold on;
%grid on;
%plot(index_bpm, (avg_y35-avg_y7)*100,'x-b', 'Linewidth', 1.5);
%plot(index_bpm, (avg_y21-avg_y7)*100,'x-m', 'Linewidth', 1.5);
%plot(index_bpm, (avg_y12-avg_y7)*100,'x-r', 'Linewidth', 1.5);
%ylabel('beam orbit change [\% of $$\sigma_{y0}$$]');
%xlabel('BPM nr. [1]');
%axis([90 137 -280 180]);
%legend('$$y(5.3\times 10^9)$$ - $$y(7\times 10^9)$$',['$$y(3.5\times ' ...
%                    '10^9)$$ - $$y(7\times 10^9)$$'],'$$y(2.1\times 10^9)$$ - $$y(7\times 10^9)$$','$$y(1.2\times 10^9)$$ - $$y(7\times 10^9)$$');

figure(301);
plot(index_bpm, sigma_y, 'x-k');
hold on;
grid on;

figure(302);
plot(index_bpm, nu_y, 'x-k');
hold on;
grid on;