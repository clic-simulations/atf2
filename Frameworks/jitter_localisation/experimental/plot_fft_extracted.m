% File to plot the extracted FFT spectra 
%
% Juergen Pfingstner
% 24th of June 2013

lin_or_log_plot = 'log';     % 'lin' or 'log'
remove_first_point = 1;      % 0 or 1
full_or_diff_data = 'diff';  % 'full' or 'diff' data
load_gm_data = 0;            % 0 or 1
sensor_index_plot = 13;      % 1 to 14; 13 ... Q19X, 14 ... QD0

%%%%%%%%%%%%%%%%%
% Load the data %
%%%%%%%%%%%%%%%%%

Td_beam = 1/3;

data_130410_0040 = load('./data/fft_extracted_130410_0040.dat');
data_130410_0050 = load('./data/fft_extracted_130410_0050.dat');
data_130410_0051 = load('./data/fft_extracted_130410_0051.dat');
data_130410_0056 = load('./data/fft_extracted_130410_0056.dat');
data_130410_0105 = load('./data/fft_extracted_130410_0105.dat');

data_130524_1500 = load('./data/fft_extracted_130524_1500.dat');
data_130524_1510 = load('./data/fft_extracted_130524_1510.dat');
data_130524_1515 = load('./data/fft_extracted_130524_1515.dat');
data_130524_1525 = load('./data/fft_extracted_130524_1525.dat');
data_130524_1530 = load('./data/fft_extracted_130524_1530.dat');

data_130618_0100 = load('./data/fft_extracted_130618_0100.dat');
data_130618_0250 = load('./data/fft_extracted_130618_0250.dat');
data_130618_0453 = load('./data/fft_extracted_130618_0453.dat');
data_130618_0712 = load('./data/fft_extracted_130618_0712.dat');
data_130618_0740 = load('./data/fft_extracted_130618_0740.dat');

if(load_gm_data ==1)
    data_gm_130611 = load('./gm_data/ATF2_2013-06-11_06h45m30s_237');
    data_gm_y_130611 = data_gm_130611.interpulse_dposy;
    [nr_sensors, nr_time_steps_gm] = size(data_gm_y_130611);
    Td_gm = (typecast(data_gm_130611.timey(end),'double')-typecast(data_gm_130611.timey(1),'double'))/nr_time_steps_gm;
    clear data_gm_130611;
end

%%%%%%%%%%%%%%%%%%%
% Modify the data %
%%%%%%%%%%%%%%%%%%%

%[f1, data1_1, data1_2] = prepare_extr_fft_data(data_130618_0100, ...
%                         Td_beam, full_or_diff_data, remove_first_point);
%[f2, data2_1, data2_2] = prepare_extr_fft_data(data_130618_0250, ...
%                         Td_beam, full_or_diff_data, remove_first_point);
%[f3, data3_1, data3_2] = prepare_extr_fft_data(data_130618_0453, ...
%                         Td_beam, full_or_diff_data, remove_first_point);
%[f4, data4_1, data4_2] = prepare_extr_fft_data(data_130618_0712, ...
%                         Td_beam, full_or_diff_data, remove_first_point);
%[f5, data5_1, data5_2] = prepare_extr_fft_data(data_130618_0740, ...
%                         Td_beam, full_or_diff_data, remove_first_point);

%[f1, data1_1, data1_2] = prepare_extr_fft_data(data_130524_1500, ...
%                         Td_beam, full_or_diff_data, remove_first_point);
%[f2, data2_1, data2_2] = prepare_extr_fft_data(data_130524_1510, ...
%                         Td_beam, full_or_diff_data, remove_first_point);
%[f3, data3_1, data3_2] = prepare_extr_fft_data(data_130524_1515, ...
%                         Td_beam, full_or_diff_data, remove_first_point);
%[f4, data4_1, data4_2] = prepare_extr_fft_data(data_130524_1525, ...
%                         Td_beam, full_or_diff_data, remove_first_point);
%[f5, data5_1, data5_2] = prepare_extr_fft_data(data_130524_1530, ...
%                         Td_beam, full_or_diff_data, remove_first_point);

[f1, data1_1, data1_2] = prepare_extr_fft_data(data_130410_0040, ...
                         Td_beam, full_or_diff_data, remove_first_point);
[f2, data2_1, data2_2] = prepare_extr_fft_data(data_130410_0050, ...
                         Td_beam, full_or_diff_data, remove_first_point);
[f3, data3_1, data3_2] = prepare_extr_fft_data(data_130410_0051, ...
                         Td_beam, full_or_diff_data, remove_first_point);
[f4, data4_1, data4_2] = prepare_extr_fft_data(data_130410_0056, ...
                         Td_beam, full_or_diff_data, remove_first_point);
[f5, data5_1, data5_2] = prepare_extr_fft_data(data_130410_0105, ...
                         Td_beam, full_or_diff_data, remove_first_point);

fft_gm = fft(data_gm_y_130611(sensor_index_plot,:))';
[f_gm, data_gm, ~] = prepare_extr_fft_data([fft_gm,fft_gm], ...
                         Td_gm, full_or_diff_data, remove_first_point);

data_white = ones(size(data1_1));
f_white = f1;
if(strcmp(full_or_diff_data, 'diff')==1)
    data_white = data_white.*(2-2.*cos(2*pi*f_white*Td_beam));
    data_white = data_white./sum(data_white).*sum(data1_1);
end

%%%%%%%%%%%%%%%%%
% Plot the data %
%%%%%%%%%%%%%%%%%

if(strcmp('log', lin_or_log_plot) == 1)
    figure(1);
    loglog(f1, data1_1, '-c');
    loglog(f2, data2_1, '-b');
    grid on;
    hold on;
    loglog(f3, data3_1, '-g');
    loglog(f4, data4_1, '-r');
    loglog(f5, data5_1, '-m');
    loglog(f_white, data_white, '-k')
    %title('Source 1');
    xlabel('frequency [Hz]');
    ylabel('PSD');
    axis([0.01, 2, 1e-3, 50]);
    legend('before', 'exchanged', 'after');
    hold off;

    figure(2);
    loglog(f1, data1_2, '-c');
    loglog(f2, data2_2, '-b');
    grid on;
    hold on;
    loglog(f3, data3_2, '-g');
    loglog(f4, data4_2, '-r');
    loglog(f5, data5_2, '-m');
    loglog(f_white, data_white, '-k')
    %title('Source 2');
    xlabel('frequency [Hz]');
    ylabel('PSD');
    axis([0.01, 2, 1e-3, 50]);
    legend('before', 'exchanged', 'after');
    hold off;
    
    figure(3);
    loglog(f_gm, data_gm, '-k');
    grid on;
    hold on;
    %title('Ground motion');
    xlabel('frequency [Hz]');
    ylabel('PSD');
    axis([0.001 2, 1e-12 1e-3]);
    legend('GM at QF19X');
    %hold off;
else
    
    figure(1);
    %plot(f1, data1_1, '-c');
    grid on;
    hold on;
    plot(f2, data2_1, '-b');
    plot(f3, data3_1, '-g');
    plot(f4, data4_1, '-r');
    %plot(f5, data5_1, '-m');
    title('Source 1');
    hold off;

    figure(2);
    %plot(f1, data1_2, '-c');
    grid on;
    hold on;
    plot(f2, data2_2, '-b');
    plot(f3, data3_2, '-g');
    plot(f4, data4_2, '-r');
    %plot(f5, data5_2, '-m');
    title('Source 2');
    hold off; 
    
    figure(3);
    plot(f_gm, data_gm, '-k');
    grid on;
    hold on;
    title('Ground motion');
    %hold off;
end