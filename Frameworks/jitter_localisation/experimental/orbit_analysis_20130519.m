% Script to analyse and plot the orbits of different measurments
% (mainly changing wakefield conditions (ref cavity scans) and
% charge  scans)
%
% Juergen Pfingstner
% 18th of May 2013

%%%%%%%%%%%%%%
% Parameters %
%%%%%%%%%%%%%%

%use_x_or_y = 'y';
%use_normalised_data = 0;

%%%%%%%%%%%%%%%%%
% Load the data %
%%%%%%%%%%%%%%%%%

file_name_base = 'epics_all_orbit_130519_1935_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y11_v1 = load(file_name_y, '-ASCII');
    
file_name_base = 'epics_all_orbit_130519_1940_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y11_v2 = load(file_name_y, '-ASCII');

file_name_base = 'epics_all_orbit_130519_1950_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y11_v3 = load(file_name_y, '-ASCII');

file_name_base = 'epics_all_orbit_130519_1925_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y21 = load(file_name_y, '-ASCII');

file_name_base = 'epics_all_orbit_130519_1900_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y35 = load(file_name_y, '-ASCII');

file_name_base = 'epics_all_orbit_130519_1855_full';
file_name_y= sprintf('./data/%s_y_mod.dat', file_name_base);
y43 = load(file_name_y, '-ASCII');

file_name_model= sprintf('./data/model_%s_mod.dat', file_name_base);
model_data = load(file_name_model, '-ASCII');
beta_x = model_data(:,1)';
beta_y = model_data(:,2)';
eta_x = model_data(:,3)';
eta_y = model_data(:,4)';
nu_x = model_data(:,5)';
nu_y = model_data(:,6)';
sigma_x = model_data(:,7)';
sigma_y = model_data(:,8)';
s_bpm = model_data(:,9)';
ref_noise = model_data(:,10)';
   
y11_v1 = y11_v1(:,91:end);
y11_v2 = y11_v2(:,91:end);
y11_v3 = y11_v3(:,91:end);
y21 = y21(:,91:end);
y35 = y35(:,91:end);
y43 = y43(:,91:end);

sigma_x = sigma_x(91:end)';
sigma_y = sigma_y(91:end)';
ref_noise = ref_noise(:,91:end)';
nu_y = nu_y(:,91:end)';
index_bpm = 91:136;
scale_bpm_min = 90;
scale_bpm_max = 137;

%%%%%%%%%%%%%%%%%%%%
% Analyse the data %
%%%%%%%%%%%%%%%%%%%%

avg_y11_v1 = mean(y11_v1);
avg_y11_v2 = mean(y11_v2);
avg_y11_v3 = mean(y11_v3);
avg_y21 = mean(y21);
avg_y35 = mean(y35);
avg_y43 = mean(y43);

%avg_y11_v1 = avg_y11_v1*1e6;
%avg_y11_v2 = avg_y11_v2*1e6;
%avg_y11_v3 = avg_y11_v3*1e6;
%avg_y21 = avg_y21*1e6;
%avg_y35 = avg_y35*1e6;
%avg_y43 = avg_y43*1e6;

avg_y11_v1 = avg_y11_v1./sigma_y';
avg_y11_v2 = avg_y11_v2./sigma_y';
avg_y11_v3 = avg_y11_v3./sigma_y';
avg_y21 = avg_y21./sigma_y';
avg_y35 = avg_y35./sigma_y';
avg_y43 = avg_y43./sigma_y';

%%%%%%%%%%%%%%%%%%%%
% Plot the results %
%%%%%%%%%%%%%%%%%%%%

figure(299);
plot(index_bpm, avg_y11_v2-avg_y11_v1, 'x-c');
hold on;
grid on;
plot(index_bpm, avg_y11_v3-avg_y11_v1,'x-b');
plot(index_bpm, avg_y21-avg_y11_v1,'x-m');
plot(index_bpm, avg_y35-avg_y11_v1,'x-r');
plot(index_bpm, avg_y43-avg_y11_v1,'x-g');

figure(298);
plot(index_bpm, avg_y11_v2.*10^6, 'x-c');
hold on;
grid on;
plot(index_bpm, avg_y11_v3.*10^6,'x-b');
plot(index_bpm, avg_y21.*10^6,'x-m');
plot(index_bpm, avg_y35.*10^6,'x-r');
plot(index_bpm, avg_y43.*10^6,'x-g');
plot(index_bpm, avg_y11_v1.*10^6,'x-k');

figure(301);
plot(index_bpm, sigma_y, 'x-k');
hold on;
grid on;

figure(302);
plot(index_bpm, nu_y, 'x-k');
hold on;
grid on;