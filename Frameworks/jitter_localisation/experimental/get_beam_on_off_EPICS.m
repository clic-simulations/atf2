% Function to read BPM data through the flight simulator
%
% Juergen Pfingstner
% 5. Dec. 2012

function [beam_on, time_stamp_beam_on] = get_beam_on_off_EPICS(nr_trains)

%global BEAMLINE
%global INSTR

%%%%%%%%%%%%%
% Init data %
%%%%%%%%%%%%%

lcaSetSeverityWarnLevel(14);    % labCA settings
lcaSetSeverityWarnLevel(4);
lcaSetSeverityWarnLevel(14);
lcaSetTimeout(0.2);
lcaSetRetryCount(20);
    
beam_on = zeros(nr_trains, 1);
time_stamp_beam_on = zeros(nr_trains, 1);

%%%%%%%%%%%%%%%%%
% Read the data %
%%%%%%%%%%%%%%%%%

tic
for i=1:nr_trains
  fprintf(1,'   Train nr %i\n', i);

  run_loop = 1; 
  cycle_time = 1./3.12/2;
  while(run_loop == 1)
    if(toc<(i*cycle_time))
      pause(0.001); % Wait 1 millisec.
    else
      run_loop = 0;
    end
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%
  % Get Beam on/off signal %
  %%%%%%%%%%%%%%%%%%%%%%%%%%
  
  [beam_on_temp time_stamp_temp] = lcaGet('atf:beam',1024);
  if(strcmp(beam_on_temp,'ON'))
      beam_on(i) = 1;
  else
      beam_on(i) = 0;
  end
  %time_stamp_beam_on(i) = real(time_stamp_temp)+imag(time_stamp_temp);
  time_stamp_beam_on(i) = time_stamp_temp;
end
toc

time_stamp_beam_on = epicsts2mat(time_stamp_beam_on);

end
