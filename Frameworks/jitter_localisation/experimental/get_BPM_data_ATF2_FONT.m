% Function to read BPM data through the flight simulator
%
% Juergen Pfingstner
% 5. Dec. 2012

function [x_data, y_data, charge_data, font_data, time_stamp_x, time_stamp_y, time_stamp_charge, time_stamp_font] = get_BPM_data_ATF2_FONT(nr_trains, sampling_factor)

%%%%%%%%%%%%%
% Init data %
%%%%%%%%%%%%%

lcaSetSeverityWarnLevel(14);    % labCA settings
lcaSetSeverityWarnLevel(4);
lcaSetSeverityWarnLevel(14);
lcaSetTimeout(0.2);
lcaSetRetryCount(20);
    

nr_points = nr_trains*sampling_factor;
nr_atf2_bpms = 57;
x_data_atf2 = zeros(nr_points, nr_atf2_bpms);
y_data_atf2 = zeros(nr_points, nr_atf2_bpms);
charge_data_atf2 = zeros(nr_points, nr_atf2_bpms);
time_stamp_x_atf2 = zeros(nr_points, nr_atf2_bpms);
time_stamp_y_atf2 = zeros(nr_points, nr_atf2_bpms);
time_stamp_charge_atf2 = zeros(nr_points, nr_atf2_bpms);

pv_font_y = 'font:board1:pos';
pv_font_time = 'font:time';
nr_font_bpms = 3;
font_data = zeros(nr_trains,nr_font_bpms);
time_stamp_font = zeros(nr_trains,nr_font_bpms);

%%%%%%%%%%%%%%%%%
% Read the data %
%%%%%%%%%%%%%%%%%
tic
cycle_time = 1./3.12/sampling_factor;
for i=1:nr_points
	fprintf(1,'   Train nr %i\n', ceil(i/sampling_factor));

  run_loop = 1; 
  while(run_loop == 1)
    if(toc<(i*cycle_time))
      pause(0.001); % Wait 1 millisec.
    else
      run_loop = 0;
    end
  end

  %%%%%%%%%%%%%%%%%%%%%
  % Get ATF2 BPM data %
  %%%%%%%%%%%%%%%%%%%%%

  [x_data_temp time_stamp_x_temp] = lcaGet('atf2:xpos', 1024);
  [y_data_temp time_stamp_y_temp] = lcaGet('atf2:ypos', 1024);
  x_data_atf2(i,:) = x_data_temp;
  y_data_atf2(i,:) = y_data_temp;
  time_stamp_x_atf2(i,:) = time_stamp_x_temp;
  time_stamp_y_atf2(i,:) = time_stamp_y_temp;

  %%%%%%%%%%%%%%%%%%%%%%%
  % Get ATF2 BPM charge %
  %%%%%%%%%%%%%%%%%%%%%%%

  [charge_data_temp time_stamp_charge_temp] = lcaGet('REFC1:amp', 1024);
  charge_data_atf2(i,:) = charge_data_temp;
  time_stamp_charge_atf2(i,:) = time_stamp_charge_temp;

  %%%%%%%%%%%%%%%%%
  % Get FONT data %
  %%%%%%%%%%%%%%%%%

  [y_data_temp time_stamp_y_temp] = lcaGet(pv_font_y, 1024);
  font_data(i,:) = y_data_temp;
  time_stamp_font(i,:) = time_stamp_y_temp;
end
toc

[x_data_atf2, y_data_atf2] = rearrange_EPICS_ATF2_BPM_data(x_data_atf2, y_data_atf2);
[time_stamp_x_atf2, time_stamp_y_atf2] = rearrange_EPICS_ATF2_BPM_data(time_stamp_x_atf2, time_stamp_y_atf2);
[charge_data_atf2, time_stamp_charge_atf2] = rearrange_EPICS_ATF2_BPM_data(charge_data_atf2, time_stamp_charge_atf2);
x_data = x_data_atf2./1e6;
% Uhhh that is a tricky bug. The number has to be corrected in the post-processing.
%y_data = y_data_atf2./1.6;
y_data = y_data_atf2./1e6;
time_stamp_x = epicsts2mat(time_stamp_x_atf2);
time_stamp_y = epicsts2mat(time_stamp_y_atf2);
time_stamp_charge = epicsts2mat(time_stamp_charge_atf2);
time_stamp_font = epicsts2mat(time_stamp_font);

end
