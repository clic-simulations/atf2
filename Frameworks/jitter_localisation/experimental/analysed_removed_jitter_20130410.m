% Script to create out of the remaining jitter (after the main
% oscillation has been removed) a wave form that can be compared
% with simulations.
%
% Juergen Pfingstner
% 20. May 2013

%%%%%%%%%%%%%
% Parameter %
%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%
% Load the data %
%%%%%%%%%%%%%%%%%

data0 = load('./data/epics_all_orbit_130410_0050_y_mod.dat');
data1 = load('./paper/corr_remove_91_to_109_20130410_0050_y.dat');
data2 = load('./paper/corr_remove_91_to_113_20130410_0050_y.dat');
model_data = load('./data/model_epics_all_orbit_130410_0050_mod.dat', '-ASCII');
sigma_y = model_data(:,8)';

% Remove noisy BPMs
t = 108:133;
t(11) = [];
t(6) = [];

sigma_y = sigma_y(108:end-3);
sigma_y(11) = [];
sigma_y(6) = [];

data0 = data0(:,108:end-3);
data0(:,11) = [];
data0(:,6) = [];

data1 = data1(:,108:end-3);
data1(:,11) = [];
data1(:,6) = [];

data2 = data2(:,108:end-3);
data2(:,11) = [];
data2(:,6) = [];

data0 = data0 - data1;
data1 = data1 - data2;

%%%%%%%%%%%%%%%%
% Process data %
%%%%%%%%%%%%%%%%

[U0, S0, V0] = svd(data0);
[U1, S1, V1] = svd(data1);
[U2, S2, V2] = svd(data2);

std_data0 = std(data0);
std_data1 = std(data1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Store and plot the data %
%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(1);
plot(t, data0');
grid on;

figure(2);
plot(t, data1');
grid on;

figure(3);
plot(t, data2');
grid on;

figure(4);
plot(t, V1(:,1).*S1(1,1))
grid on;
hold on;
plot(t, V0(:,1).*S0(1,1));

figure(5);
plot(t, diag(S0), 'x-k');
grid on;
hold on;
plot(t, diag(S1), 'x-b');
plot(t, diag(S2), 'x-r');

factor0 = mean(std_data0)/mean(abs(V0(:,1)));
factor1 = mean(std_data1)/mean(abs(V1(:,1)));
figure(6);
plot(t, abs(V1(:,1)*factor1), 'b');
grid on;
hold on;
plot(t, abs(V0(:,1)*factor0), 'g');
plot(t, std_data0, 'm');
plot(t, std_data1, 'r');

store_data = [ t', V0(:,1).*factor0, V1(:,1).*factor1, sigma_y'];
save('oscillation_130410.dat', 'store_data', '-ascii');
