% Function to calculate the data for the degree of freedom plot (DoF) for
% the model independent analysis (MIA)
%
% Juergen Pfingstner
% 6th of February 2013

function [ DoF_data ] = calc_DoF_data( data )

[~, nr_bpms] = size(data);
DoF_data = zeros(nr_bpms, nr_bpms);

for i=1:nr_bpms
    [~, S, ~] = svd(data(:,1:i));
    %s = diag(S);
    %s = [s; zeros(1,nr_bpms-i)];
    if(i==1)
        DoF_data(1,1) = S(1);
    else
        DoF_data(1:i,i) = diag(S);
    end
end

