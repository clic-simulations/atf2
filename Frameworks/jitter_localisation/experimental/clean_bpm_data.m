% Function to remove faulty BPM measurments of a certain type at ATF
% The measurement is removed if any of the measurements gives a value 
% larger than an unrealistic threshold
%
% Juergen Pfingstner
% 4th of December

function [data_clean] = clean_bpm_data(data_raw)

   threshold = 0.5;

   [nr_row_raw, nr_column_raw] = size(data_raw);
   data_clean = zeros(nr_row_raw, nr_column_raw);
   nr_row_clean = 1;
   for i = 1:nr_row_raw
     data_raw_local = data_raw(i,:).^2;
     if(sum(data_raw_local>threshold))
       % The row is not okay -> do nothing
       %fprintf('Remove data point nr %i\n',i);
     else
       % The row is okay -> copy it
       data_clean(nr_row_clean,:) = data_raw(i,:);
       nr_row_clean = nr_row_clean + 1;
     end  
   end
   data_clean = data_clean(1:(nr_row_clean-1),:);
end 
