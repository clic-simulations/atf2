% Script to process the data and evaluate the relative beam jitter
% at the beginning of the beam line. 
%
% Juergen Pfingstner
% 20th of March 2014

%%%%%%%%%%%%%
% Parameter %
%%%%%%%%%%%%%

file_name_base = 'gm_20140307_0745';
%file_name_base = 'gm2_20140307_0805';
file_name_beta_prop = './data/data_20140307/EXTFF.beta';
file_name_otr_data = './data/data_20140307/emit2dOTRp_20140307T025450.mat';

external_parameter = 1;
use_new_data_format = 1;
bpmv = [30];

%%%%%%%%%%%%%%%%%%%%
% Process the data %
%%%%%%%%%%%%%%%%%%%%

preprocess_data
%file_name_offset = ['./data/' file_name_base '.mat'];
%test_data = load(file_name_offset);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluate the jitter level at the downstream cavity BPMs %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fid_beta_prop = fopen(file_name_beta_prop,'r');
name_prop = [];
s_prop = [];
beta_x_prop = [];
beta_y_prop = [];
while(~feof(fid_beta_prop))
  line_string = fgets(fid_beta_prop);
  data_temp = textscan(line_string,'%q %f %f %f');
  name_prop = [name_prop; data_temp{1}];
  s_prop = [s_prop; data_temp{2}];
  beta_x_prop = [beta_x_prop; data_temp{3}];
  beta_y_prop = [beta_y_prop; data_temp{4}];
end
fclose(fid_beta_prop);

s_bpm_inter = s_bpm(91:end) - s_bpm(91) + s_prop(1);
beta_x_prop_inter = interp1(s_prop, beta_x_prop, s_bpm_inter, 'linear', 'extrap');
beta_y_prop_inter = interp1(s_prop, beta_y_prop, s_bpm_inter, 'linear', 'extrap');

x_std=std(x_data)./sqrt(2);
y_std=std(y_data)./sqrt(2);
beta_x = model_data(:,1)';
beta_y = model_data(:,2)';
sigma_x = model_data(:,7)';
sigma_y = model_data(:,8)';
s_bpm = model_data(:,9)';
ref_noise = model_data(:,10)';
bpm_index = [1:size(y_data,2)];

gamma = 1300/0.511;
emitt_geo_x = 50e-7./gamma;
emitt_geo_y = 0.3e-7./gamma;
sigma_x_prop = sqrt(beta_x_prop_inter*emitt_geo_x);
sigma_y_prop = sqrt(beta_y_prop_inter*emitt_geo_y);
sigma_x_test = sqrt(beta_x(91:end)*emitt_geo_x);
sigma_y_test = sqrt(beta_y(91:end)*emitt_geo_y);

figure(1);
plot(s_bpm_inter, beta_y(91:end),'-xb');
grid on;
hold on;
plot(s_bpm_inter, beta_y_init(91:end),'-xr');
plot(s_bpm_inter(1:end-1), beta_y_prop_inter(1:end-1),'-xg');
legend('beta x prop FL', 'beta x init FL', 'beta x prop MAD');

figure(2);
plot(s_bpm_inter, beta_x(91:end),'-xb');
grid on;
hold on;
plot(s_bpm_inter, beta_x_init(91:end),'-xr');
plot(s_bpm_inter(1:end-1), beta_x_prop_inter(1:end-1),'-xg');
legend('beta y prop FL', 'beta y init FL', 'beta y prop MAD');

%figure(3);
%plot(beta_y(91:end),'-xb');
%grid on;
%hold on;
%plot(beta_y_prop,'-xr');
%plot(beta_y_prop_inter,'-xg');

%figure(4);
%plot(beta_x(91:end),'-xb');
%grid on;
%hold on;
%plot(beta_x_prop,'-xr');
%plot(beta_x_prop_inter,'-xg');

%figure(5);
%plot(beta_x(91:end),'-xb');
%grid on;
%hold on;
%plot(beta_x_prop_inter./1,'-xg');

%figure(6);
%plot(beta_y(91:end),'-xb');
%grid on;
%hold on;
%plot(beta_y_prop_inter./1,'-xg');

figure(7);
plot(sigma_x(91:end),'-ob');
grid on;
hold on;
plot(sigma_x_init(91:end),'-xr');
plot(sigma_x_prop(1:end-1),'-xg');
legend('sigma x prop FL', 'sigma x init FL', 'sigma x prop MAD');

figure(8);
plot(sigma_y(91:end),'-ob');
grid on;
hold on;
plot(sigma_y_init(91:end),'-xr');
plot(sigma_y_prop(1:end-1),'-xg');
legend('sigma y prop FL', 'sigma y init FL', 'sigma y prop MAD');

x_data_removed = x_data;
y_data_removed = y_data;

for index=1:length(bpmv);
  j = bpmv(index);
  for i=1:45;
    if i~=j;
      covar=cov(x_data_removed(:,j),x_data_removed(:,i));
      c12=covar(1,2);
      c11=std(x_data(:,j));
      c22=std(x_data(:,i));
      k_x=(c12/c11/c11);

      covar=cov(y_data_removed(:,j),y_data_removed(:,i));
      c12=covar(1,2);
      c11=std(y_data(:,j));
      c22=std(y_data(:,i));
      k_y=(c12/c11/c11);

      x_data_removed(:,i)=x_data_removed(:,i)-k_x*x_data_removed(:,j);
      y_data_removed(:,i)=y_data_removed(:,i)-k_y*y_data_removed(:,j);
    else
      x_data_removed(:,i) = x_data_removed(:,j);
      y_data_removed(:,i) = y_data_removed(:,j);
    end
  end
end

x_rem_std = std(x_data_removed)./sqrt(2);
y_rem_std = std(y_data_removed)./sqrt(2);


% initial jitter
for i=1:45;
    x_rel_nom(i) = x_std(i)/sigma_x(i+90);
    y_rel_nom(i) = y_std(i)/sigma_y(i+90);
    x_rel_init(i) = x_std(i)/sigma_x_init(i+90);
    y_rel_init(i) = y_std(i)/sigma_y_init(i+90);
    x_rel_prop(i) = x_std(i)/sigma_x_prop(i);
    y_rel_prop(i) = y_std(i)/sigma_y_prop(i);

    x_rel_rem_nom(i) = x_rem_std(i)/sigma_x(i+90);
    y_rel_rem_nom(i) = y_rem_std(i)/sigma_y(i+90);
    x_rel_rem_init(i) = x_rem_std(i)/sigma_x_init(i+90);
    y_rel_rem_init(i) = y_rem_std(i)/sigma_y_init(i+90);
    x_rel_rem_prop(i) = x_rem_std(i)/sigma_x_prop(i);
    y_rel_rem_prop(i) = y_rem_std(i)/sigma_y_prop(i);
end

figure(11);
plot(x_rel_init*100, '-xb');
grid on;
hold on;
plot(x_rel_prop*100,'-xr');
plot(x_rel_nom*100,'-xk');

plot(x_rel_rem_init*100,'-xc');
plot(x_rel_rem_prop*100,'-xm');
plot(x_rel_rem_nom*100,'-xg');
xlabel('BPM nr. [1]');
legend('x jitter init FL', 'x jitter prop MAD', 'x jitter prop FL', 'x jitter init rem FL', 'x jitter prop rem MAD', 'x jitter prop rem FL');

figure(12);
plot(y_rel_init*100, '-xb');
grid on;
hold on;
plot(y_rel_prop*100,'-xr');
plot(y_rel_nom*100, '-xk');

plot(y_rel_rem_init*100,'-xc');
plot(y_rel_rem_prop*100,'-xm');
plot(y_rel_rem_nom*100,'-xg');
xlabel('BPM nr. [1]');
legend('y jitter init FL', 'y jitter prop MAD', 'y jitter prop FL', 'y jitter init rem FL', 'y jitter prop rem MAD', 'y jitter prop rem FL');

%figure(13);
%plot(x_std, '-xb');
%grid on;
%hold on;
%plot(x_rem_std,'-xc');
%xlabel('BPM nr. [1]');
%legend('x jitter nom', 'x jitter nom rem');

%figure(14);
%plot(y_std, '-xb');
%grid on;
%hold on;
%plot(y_rem_std,'-xc');
%xlabel('BPM nr. [1]');
%legend('y jitter nom', 'y jitter nom rem');

return

% Take out some noisy BPMs and calculate the average jitter level
x_rel_ff=[x_rel(119:123) x_rel(125:132)];
y_rel_ff=[y_rel(119:123) y_rel(125:132)];
x_rel_mean=mean(x_rel_ff);
y_rel_mean=mean(y_rel_ff);

print_string = sprintf('Rel. beam jitter level in per cent: x=%g, y=%g\n', x_rel_mean*100, y_rel_mean*100);
fprintf(1,print_string)


